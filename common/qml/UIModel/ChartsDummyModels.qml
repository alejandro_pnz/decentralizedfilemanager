import QtQuick 2.15

Item {
    id: dummyModel

    signal tabsModelLoaded()

    property alias windowTabsModel: tabsModel
    property alias arcaSyncStatusModel: arcaSyncStatus
    property alias arcaSyncPropertyModel: arcaSyncProperty
    property alias allQuodTypesModel: allQuodTypes
    property alias quodLockStatusModel: quodLockStatus
    property alias quodSyncStatusModel: quodSyncStatus
    property alias quodSyncPropertyModel: quodSyncProperty
    property alias allFileTypesModel: allFileTypes
    property alias fileSyncStatusModel: fileSyncStatus
    property alias fileLockStatusModel: fileLockStatus
    property alias fileSyncPropertyModel: fileSyncProperty

    function getDummyModel(modelName) {
        if(modelName === arcaSyncStatus)
            return arcaSyncStatusModel
        else if(modelName === arcaSyncProperty)
            return arcaSyncPropertyModel
        else if(modelName === allQuodTypes)
            return allQuodTypesModel
        else if(modelName === quodLockStatus)
            return quodLockStatusModel
        else if(modelName === quodSyncStatus)
            return quodSyncStatusModel
        else if(modelName === quodSyncProperty)
            return quodSyncPropertyModel
        else if(modelName === allFileTypes)
            return allFileTypesModel
        else if(modelName === fileSyncStatus)
            return fileSyncStatusModel
        else if(modelName === fileLockStatus)
            return fileLockStatusModel
        else if(modelName === fileSyncProperty)
            return fileSyncPropertyModel
    }

    ListModel {
        id: tabsModel

        Component.onCompleted: {
            tabsModel.append({title: "Arca",
                                 contentTitle: "All Arca",
                                 componentTitle: "Sync Status",
                                 charts: [{type: "status",
                                         modelName: arcaSyncStatusModel},
                                     {type: "property",
                                         modelName: arcaSyncPropertyModel}]})
            tabsModel.append({title: "Quod",
                                 contentTitle: "All Quod",
                                 componentTitle: "Sync Status",
                                 charts: [{type: "pie",
                                         modelName: allQuodTypesModel},
                                     {type: "status",
                                         modelName: quodSyncStatusModel,
                                         componentTitle: "Sync Status"},
                                     {type: "status",
                                         modelName: quodLockStatusModel,
                                         componentTitle: "Lock Status"},
                                     {type: "property",
                                         modelName: quodSyncPropertyModel}]})
            tabsModel.append({title: "Files",
                                 contentTitle: "All Files",
                                 componentTitle: "Sync Status",
                                 charts: [{type: "pie",
                                         modelName: allFileTypesModel},
                                     {type: "status",
                                         modelName: fileSyncStatusModel,
                                         componentTitle: "Sync Status"},
                                     {type: "status",
                                         modelName: fileLockStatusModel,
                                         componentTitle: "Lock Status"},
                                     {type: "property",
                                         modelName: fileSyncPropertyModel}]})

            dummyModel.tabsModelLoaded()
        }
    }

    /*Arca Dummy Models*/
    ListModel {
        id: arcaSyncStatus

        Component.onCompleted: {
            arcaSyncStatus.append({label: "Bank", color: "#2F3441", upToDate: 0, outOfSync: 21})
            arcaSyncStatus.append({label: "Card", color: "#5C5AE8", upToDate: 100, outOfSync: 1})
        }
    }

    ListModel {
        id: arcaSyncProperty

        Component.onCompleted: {
            arcaSyncProperty.append({label: "Contact", color: "#BD0ADB", publicValue: 51, privateValue: 0, noSyncValue: 0})
            arcaSyncProperty.append({label: "ID", color: "#22D396", publicValue: 0, privateValue: 35, noSyncValue: 0})
            arcaSyncProperty.append({label: "Bank", color: "#2F3441", publicValue: 0, privateValue: 0, noSyncValue: 90})
        }
    }

    /*Quod Dummy Models*/
    ListModel {
        id: allQuodTypes

        Component.onCompleted: {
            allQuodTypes.append({label: "Bank", color: "#2F3441", value: 3.1})
            allQuodTypes.append({label: "Card", color: "#5C5AE8", value: 4.2})
            allQuodTypes.append({label: "Contact", color: "#BD0ADB", value: 5.1})
            allQuodTypes.append({label: "Custom", color: "#E042FB", value: 6.1})
            allQuodTypes.append({label: "Email", color: "#FFB904", value: 7.2})
            allQuodTypes.append({label: "ID", color: "#22D396", value: 8.3})
            allQuodTypes.append({label: "Keys", color: "#1AA7F9", value: 9.5})
            allQuodTypes.append({label: "License", color: "#4CBDFF", value: 10.6})
            allQuodTypes.append({label: "Login", color: "#858A99", value: 11.7})
            allQuodTypes.append({label: "Network", color: "#5A5F6D", value: 12.7})
        }
    }

    ListModel {
        id: quodLockStatus

        Component.onCompleted: {
            quodLockStatus.append({label: "Bank", color: "#2F3441", upToDate: 31, outOfSync: 9})
            quodLockStatus.append({label: "Card", color: "#5C5AE8", upToDate: 10, outOfSync: 0})
            quodLockStatus.append({label: "Contact", color: "#BD0ADB", upToDate: 10, outOfSync: 0})
            quodLockStatus.append({label: "Custom", color: "#E042FB", upToDate: 21, outOfSync: 0})
            quodLockStatus.append({label: "Email", color: "#FFB904", upToDate: 0, outOfSync: 10})
            quodLockStatus.append({label: "ID", color: "#22D396", upToDate: 0, outOfSync: 10})
            quodLockStatus.append({label: "License", color: "#4CBDFF", upToDate: 11, outOfSync: 0})
            quodLockStatus.append({label: "Network", color: "#5A5F6D", upToDate: 0, outOfSync: 19})
        }
    }

    ListModel {
        id: quodSyncStatus

        Component.onCompleted: {
            quodSyncStatus.append({label: "Bank", color: "#2F3441", upToDate: 31, outOfSync: 9})
            quodSyncStatus.append({label: "Card", color: "#5C5AE8", upToDate: 10, outOfSync: 0})
            quodSyncStatus.append({label: "Contact", color: "#BD0ADB", upToDate: 10, outOfSync: 0})
            quodSyncStatus.append({label: "Custom", color: "#E042FB", upToDate: 21, outOfSync: 0})
            quodSyncStatus.append({label: "Email", color: "#FFB904", upToDate: 0, outOfSync: 10})
            quodSyncStatus.append({label: "ID", color: "#22D396", upToDate: 0, outOfSync: 10})
            quodSyncStatus.append({label: "Keys", color: "#1AA7F9", upToDate: 15, outOfSync: 0})
            quodSyncStatus.append({label: "License", color: "#4CBDFF", upToDate: 11, outOfSync: 0})
            quodSyncStatus.append({label: "Login", color: "#858A99", upToDate: 0, outOfSync: 30})
            quodSyncStatus.append({label: "Network", color: "#5A5F6D", upToDate: 0, outOfSync: 19})
        }
    }

    ListModel {
        id: quodSyncProperty

        Component.onCompleted: {
            quodSyncProperty.append({label: "Bank", color: "#2F3441", publicValue: 31, privateValue: 0, noSyncValue: 0})
            quodSyncProperty.append({label: "Card", color: "#5C5AE8", publicValue: 0, privateValue: 0, noSyncValue: 10})
            quodSyncProperty.append({label: "Contact", color: "#BD0ADB", publicValue: 5, privateValue: 0, noSyncValue: 0})
            quodSyncProperty.append({label: "Custom", color: "#E042FB", publicValue: 30, privateValue: 0, noSyncValue: 0})
            quodSyncProperty.append({label: "Email", color: "#FFB904", publicValue: 0, privateValue: 33, noSyncValue: 0})
            quodSyncProperty.append({label: "ID", color: "#22D396", publicValue: 0, privateValue: 5, noSyncValue: 0})
            quodSyncProperty.append({label: "Keys", color: "#1AA7F9", publicValue: 0, privateValue: 0, noSyncValue: 15})
            quodSyncProperty.append({label: "License", color: "#4CBDFF", publicValue: 11, privateValue: 0, noSyncValue: 9})
            quodSyncProperty.append({label: "Login", color: "#858A99", publicValue: 0, privateValue: 13, noSyncValue: 0})
            quodSyncProperty.append({label: "Network", color: "#5A5F6D", publicValue: 0, privateValue: 0, noSyncValue: 9})
        }
    }

    /*Files Dummy Models*/
    ListModel {
        id: allFileTypes

        Component.onCompleted: {
            allFileTypes.append({label: "Other", color: "#BD0ADB", value: 11})
            allFileTypes.append({label: "Text", color: "#22D396", value: 37})
            allFileTypes.append({label: "Media", color: "#5C5AE8", value: 16})
            allFileTypes.append({label: "Office", color: "#5A5F6D", value: 10})
            allFileTypes.append({label: "Images", color: "#2F3441", value: 11})
        }
    }

    ListModel {
        id: fileSyncStatus

        Component.onCompleted: {
            fileSyncStatus.append({label: "Other", color: "#BD0ADB", upToDate: 31, outOfSync: 0})
            fileSyncStatus.append({label: "Text", color: "#22D396", upToDate: 11, outOfSync: 0})
            fileSyncStatus.append({label: "Media", color: "#5C5AE8", upToDate: 21, outOfSync: 0})
            fileSyncStatus.append({label: "Office", color: "#5A5F6D", upToDate: 0, outOfSync: 19})
            fileSyncStatus.append({label: "Images", color: "#2F3441", upToDate: 0, outOfSync: 29})
        }
    }

    ListModel {
        id: fileLockStatus

        Component.onCompleted: {
            fileLockStatus.append({label: "Other", color: "#BD0ADB", upToDate: 20, outOfSync: 9})
            fileLockStatus.append({label: "Text", color: "#22D396", upToDate: 20, outOfSync: 9})
            fileLockStatus.append({label: "Media", color: "#5C5AE8", upToDate: 0, outOfSync: 9})
            fileLockStatus.append({label: "Office", color: "#5A5F6D", upToDate: 11, outOfSync: 0})
            fileLockStatus.append({label: "Images", color: "#2F3441", upToDate: 11, outOfSync: 19})
        }
    }

    ListModel {
        id: fileSyncProperty

        Component.onCompleted: {
            fileSyncProperty.append({label: "Other", color: "#BD0ADB", publicValue: 11, privateValue: 33, noSyncValue: 9})
            fileSyncProperty.append({label: "Text", color: "#22D396", publicValue: 37, privateValue: 55, noSyncValue: 10})
            fileSyncProperty.append({label: "Media", color: "#5C5AE8", publicValue: 10, privateValue: 0, noSyncValue: 0})
            fileSyncProperty.append({label: "Office", color: "#5A5F6D", publicValue: 0, privateValue: 0, noSyncValue: 9})
            fileSyncProperty.append({label: "Images", color: "#2F3441", publicValue: 10, privateValue: 13, noSyncValue: 9})
        }
    }
}
