import QtQuick 2.0
import com.test 1.0
import SortFilterProxyModel 0.2

Item {
  property alias model: sortModel
  property int defaultSorter: UsersSortModel.Sorters.OnlineFirst
  enum Sorters {
    OnlineFirst, BlackListedFirst, AddedMostRecently
  }

  ListModel {
    id: sortModel
  }

  Component.onCompleted: {
    sortModel.append({ name: Strings.onlineFirst, type: UsersSortModel.Sorters.OnlineFirst});
    sortModel.append({ name: Strings.blacklistedFirst, type: UsersSortModel.Sorters.BlackListedFirst});
    sortModel.append({ name: Strings.addedMostRecently, type: UsersSortModel.Sorters.AddedMostRecently});
  }
}
