import QtQuick 2.15
import QtQml.Models 2.12
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2

Item {
  property alias sourceModel: typeModel
  property alias model: typeModel
  property alias ism: ism

  function select(index) {
    ism.select(typeModel.index(index,0), ItemSelectionModel.Toggle)
  }

  ItemSelectionModel {
    id: ism
    model: typeModel
  }

  ListModel {
    id: typeModel
  }

  Component.onCompleted: {
    typeModel.append({ name: Strings.images })
    typeModel.append({ name: Strings.office })
    typeModel.append({ name: Strings.media })
    typeModel.append({ name: Strings.text })
    typeModel.append({ name: Strings.other })
  }
}
