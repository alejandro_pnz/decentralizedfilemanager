import QtQuick 2.0
import com.test 1.0
import SortFilterProxyModel 0.2

Item {
  property alias model: proxy
  property alias sourceModel: sortModel
  property int defaultSorter: Enums.LastUsed
  property int objectType: Enums.Arca | Enums.Quod | Enums.File
  function getIndexOfSorter(sorter) {
    for (var i = 0; i < sortModel.count; ++i) {
      if (sortModel.get(i).type === sorter) return i
    }
    return -1
  }

  SortFilterProxyModel {
    id: proxy
    sourceModel: sortModel
    filters: [
      ExpressionFilter {
        enabled: true
        expression: {
          return visibleFor & objectType
        }
      }
    ]
  }

  ListModel {
    id: sortModel
  }

  Component.onCompleted: {
    sortModel.append({ name: Strings.lastOpened, type: Enums.LastUsed, visibleFor: Enums.Arca | Enums.Quod | Enums.File, });
    sortModel.append({ name: Strings.mostUsed, type: Enums.MostUsed, visibleFor: Enums.Arca | Enums.Quod | Enums.File, });
    sortModel.append({ name: Strings.fromAtoZ, type: Enums.NameAsc, visibleFor: Enums.Arca | Enums.Quod | Enums.File, });
    sortModel.append({ name: Strings.fromZtoA, type: Enums.NameDesc, visibleFor: Enums.Arca | Enums.Quod | Enums.File, });
    sortModel.append({ name: Strings.lastCreated, type: Enums.Created, visibleFor: Enums.Arca | Enums.Quod | Enums.File, });
    sortModel.append({ name: Strings.locked, type: Enums.Locked, visibleFor: Enums.Quod | Enums.File, });
    sortModel.append({ name: Strings.type, type: Enums.Type, visibleFor: Enums.File, });
    sortModel.append({ name: Strings.size, type: Enums.Size, visibleFor: Enums.File, });
    sortModel.append({ name: Strings.syncStatus, type: Enums.Sync, visibleFor: Enums.Arca | Enums.Quod | Enums.File,});
  }
}
