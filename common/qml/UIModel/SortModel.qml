import QtQuick 2.15
import SortFilterProxyModel 0.2
import com.test 1.0
import com.testintl 1.0

Item {
  readonly property alias proxy: sorterModel
  property alias sourceModel: sorterModel.sourceModel
  property int sorter: Enums.LastUsed
  property string searchPattern: ""
  property bool pinnedFilterEnabled: true
  property alias customListSorter: listSorter

  onSorterChanged: {
    console.log("CURRENT SORTER", sorter)
  }

  function syncSort(left, right) {
    return Utility.syncStatusLessThan(left, right)
  }

  SortFilterProxyModel {
    id: sorterModel
    filters: [
      ValueFilter {
        roleName: "pinned"
        value: false
        enabled: pinnedFilterEnabled
      },
      RegExpFilter {
        roleName: "name"
        pattern: searchPattern
        enabled: searchPattern.length > 0
        caseSensitivity: Qt.CaseInsensitive
      },
      ListFilter {
        id: listSorter
        enabled: false
      }

    ]
    sorters: [
      RoleSorter {
        roleName: "lastUsed"
        enabled: sorter == Enums.LastUsed
      },
      RoleSorter {
        roleName: "locked"
        enabled: sorter == Enums.Locked
      },
      ExpressionSorter {
        expression: {
          return syncSort(modelLeft.syncProperty, modelRight.syncProperty)
        }
        enabled: sorter == Enums.Sync
      },
      StringSorter {
        roleName: "name"
        enabled: sorter == Enums.NameAsc || sorter == Enums.NameDesc
        ascendingOrder: sorter == Enums.NameAsc
        // TODO provide proper locale
      },
      RoleSorter {
        roleName: "fileType"
        enabled: sorter == Enums.Type
      },
      RoleSorter {
        roleName: "size"
        enabled: sorter == Enums.Size
        ascendingOrder: false
      },
      RoleSorter {
        roleName: "created"
        enabled: sorter == Enums.Created
        ascendingOrder: false
      },
      RoleSorter {
        roleName: "usedCount"
        enabled: sorter == Enums.MostUsed
      }
    ]
  }
}
