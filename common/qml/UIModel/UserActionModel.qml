import QtQuick 2.15
import QtQml.Models 2.12
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2

import com.testintl 1.0

Item {
  id: root
  property alias sourceModel: actionModel
  property alias model: proxy
  property var actionList: []

  property bool isAO: AuthenticationManager.currentAccount.accountOwner
  property bool isGuest: AuthenticationManager.currentAccount.guest

  property var currentObject: undefined
  property var selection: []
  readonly property var editor: homeManager.userEditor
  readonly property bool hasSelection: selection.length > 0

  property bool inSelectMode: false

  property var givePowerPopup: null
  property bool givePowerRequested: false

  Connections {
    target: givePowerPopup

    function onAccepted() {
      if (givePowerRequested) {
        givePowerRequested = false
        root.givePower()
      }
    }

    function onClosed() {
      givePowerRequested = false
    }
  }

  signal modelLoaded()
  signal cleanSelection()
  signal selectCurrent()

  property bool anyPinned: false
  property bool anyPowerUser: false

  property bool __initialized: false

  // Predefined actions lists
  readonly property var singleUserActions: [
    UserActionModel.Actions.Pin,
    UserActionModel.Actions.Select,
    UserActionModel.Actions.GivePower,
    UserActionModel.Actions.Block,
    UserActionModel.Actions.Delete
  ]
  readonly property var oneUserActions: [
    UserActionModel.Actions.GivePower,
    UserActionModel.Actions.Block,
    UserActionModel.Actions.Delete,
    UserActionModel.Actions.ShowInfo
  ]
  readonly property var userPageActions: [
    UserActionModel.Actions.Select,
    UserActionModel.Actions.InviteUser,
    UserActionModel.Actions.AddContact
  ]

  onSelectionChanged: updateModelValues()
  onModelLoaded: updateModelValues()
  onCurrentObjectChanged: {
    if (!hasSelection && currentObject !== undefined) {
      updateModelValues();
    }
  }
  onInSelectModeChanged: {
    if (app.isMobile) {
      actionModel.setProperty(UserActionModel.Actions.Select, "visible", !inSelectMode)
    }
  }

  // Warning, order matters! Should be the same as element order in model
  enum Actions {
    Pin, EditNickname, GivePower, Block, Delete, Select, InviteUser, AddContact, ShowInfo
  }

  function enumToIndex(e) {
    for (var i = 0; i < actionModel.count; ++i) {
      var item = actionModel.get(i)
      if (item.actionEnum === e) return i;
    }
  }

  function updateModelValues() {
    if (!__initialized) return
    if (selection.length > 0) {
      anyPinned = UserListModel.anyPinned(selection)
      __updatePin(!anyPinned)

      anyPowerUser = UserListModel.anyPowerUser(selection);
      __updatePower(!anyPowerUser)

      if (app.isMobile) {
        actionModel.setProperty(enumToIndex(UserActionModel.Actions.Select), "visible", false)
      }
    } else {
      console.log("UPDATE VALUES", currentObject)
      if (currentObject != undefined) {
        __updatePin(!currentObject.pinned)
        __updatePower(!currentObject.hasPower)

        if (app.isMobile) {
          actionModel.setProperty(enumToIndex(UserActionModel.Actions.Select), "visible", true)
        }
      }
    }
  }

  function __updatePin(show) {
    actionModel.setProperty(enumToIndex(UserActionModel.Actions.Pin), "name",
                            show ? Strings.pinObject : Strings.unpin)
  }

  function __updatePower(show) {
    actionModel.setProperty(enumToIndex(UserActionModel.Actions.GivePower), "name",
                            show ? Strings.givePower : Strings.revokePower)
    actionModel.setProperty(enumToIndex(UserActionModel.Actions.GivePower), "iconSource",
                            show ? "qrc:/power-icon.svg" : "qrc:/revoke-power.svg")
  }

  function givePower() {
    if (hasSelection) {
      UserListModel.givePowerList(selection, !anyPowerUser)
      cleanSelection()
    } else {
      UserListModel.givePowerOne(currentObject.id)
    }
  }

  ListModel {
    id: actionModel
  }

  SortFilterProxyModel {
    id: proxy
    sourceModel: actionModel
    filters: [
      ListFilter {
        roleName: "actionEnum"
        list: root.actionList
        inverted: false
      },
      ValueFilter {
        roleName: "visible"
        value: true
      }
    ]
  }

  Action {
    id: pin
    onTriggered: {
      if (hasSelection) {
        console.log("PIN action, any object pinned?", anyPinned)
        UserListModel.pinList(selection, !anyPinned)
        cleanSelection()
      } else {
        UserListModel.pinOne(currentObject.id, currentObject.objectType)
      }
    }
  }

  Action {
    id: editNickname
  }

  Action {
    id: givePower
    onTriggered: {
      if (givePowerPopup) {
        givePowerRequested = true
        givePowerPopup.open()
      } else {
        console.warn("No popup assigned")
      }
    }
  }

  Action {
    id: blockBlacklist
  }

  Action {
    id: deleteAction
  }

  Action {
    id: select
    onTriggered: selectCurrent()
  }

  Action {
    id: inviteUserAction
    onTriggered: {
      homeManager.openPage(Pages.InviteUserPage)
    }
  }

  Action {
    id: addContactAction
    onTriggered: {
      homeManager.openPage(Pages.InviteUserPage)
    }
  }

  Action {
    id: showInfo
    onTriggered: {
      homeManager.enterShowInfoPage(currentObject.id, currentObject.objectType)
    }
  }

  Component.onCompleted: {
    actionModel.append({ name: Strings.pinTxt,
                         iconSource: "qrc:/pin.svg",
                         actionSource: pin,
                         expanded: true,
                         actionEnum: UserActionModel.Actions.Pin,
                         visible: true
                       })

    actionModel.append({ name: Strings.editNickname,
                         iconSource: "qrc:/edit-icon.svg",
                         actionSource: editNickname,
                         expanded: true,
                         actionEnum: UserActionModel.Actions.EditNickname,
                         visible: true
                       })

    actionModel.append({ name: Strings.givePower,
                         iconSource: "qrc:/power-icon.svg",
                         actionSource: givePower,
                         expanded: true,
                         actionEnum: UserActionModel.Actions.GivePower,
                         visible: true
                       })

    actionModel.append({ name: app.isPhone ? Strings.blockBlacklist : Strings.block,
                         iconSource: "qrc:/remove-circle.svg",
                         actionSource: blockBlacklist,
                         expanded: true,
                         actionEnum: UserActionModel.Actions.Block,
                         visible: true
                       })

    actionModel.append({ name: Strings.deleteStr,
                         iconSource: "qrc:/delete.svg",
                         actionSource: deleteAction,
                         expanded: true,
                         actionEnum: UserActionModel.Actions.Delete,
                         visible: true
                       })

    if (app.isMobile) {
      actionModel.append({ name: Strings.select,
                           iconSource: "qrc:/check-circle.svg",
                           actionSource: select,
                           expanded: true,
                           actionEnum: UserActionModel.Actions.Select,
                           visible: true
                         })
    }

    actionModel.append({ name: Strings.invite,
                         iconSource: "qrc:/invite-user.svg",
                         actionSource: inviteUserAction,
                         expanded: true,
                         actionEnum: UserActionModel.Actions.InviteUser,
                         visible: true
                       })

    actionModel.append({ name: Strings.addContact,
                         iconSource: "qrc:/invite-user.svg",
                         actionSource: addContactAction,
                         expanded: true,
                         actionEnum: UserActionModel.Actions.AddContact,
                         visible: true
                       })

    actionModel.append({ name: Strings.showInfo,
                         iconSource: "qrc:/info-circle.svg",
                         actionSource: showInfo,
                         expanded: true,
                         actionEnum: UserActionModel.Actions.ShowInfo,
                         visible: true
                       })

    __initialized = true
    modelLoaded()
  }
}
