import QtQuick 2.15
import QtQml.Models 2.12
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2

Item {
  property alias sourceModel: filterModel
  property alias model: proxy
  property alias ism: ism

  function select(index) {
    var sourceIdx = proxy.mapToSource(proxy.index(index, 0))
    filterModel.setProperty(sourceIdx.row, "selected", true)
    proxy.invalidate()
  }

  ItemSelectionModel {
    id: ism
    model: filterModel
  }

  ListModel {
    id: filterModel
  }

  SortFilterProxyModel {
    id: proxy
    sourceModel: filterModel
    filters: [
      ValueFilter {
        enabled: true
        roleName: "selected"
        value: false
      }
    ]
  }

  Action {
    id: ownedByMe
  }
  Action {
    id: modifiedByMe
  }
  Action {
    id: locked
  }

  Component.onCompleted: {
    filterModel.append({ name: Strings.ownedByMe,
                         selected: false,
                         action: ownedByMe,
                       })
    filterModel.append({ name: Strings.modifiedByMe,
                         selected: false,
                         action: modifiedByMe,
                       })
    filterModel.append({ name: Strings.locked,
                         selected: false,
                         action: locked,
                       })
  }
}
