import QtQuick 2.15
import SortFilterProxyModel 0.2
import com.test 1.0
import com.testintl 1.0

Item {
  readonly property alias proxy: sorterModel
  property alias sourceModel: sorterModel.sourceModel
  property int sorter: DevicesSortMenuModel.Sorters.LastSeen
  property string searchPattern: ""
  property bool pinnedFilterEnabled: true
  property alias customListSorter: listSorter
  onSorterChanged: {
    console.log("CURRENT SORTER", sorter)
  }

  function syncSort(left, right) {
    return Utility.syncStatusLessThan(left, right)
  }

  SortFilterProxyModel {
    id: sorterModel
    filters: [
      ValueFilter {
        roleName: "pinned"
        value: false
        enabled: pinnedFilterEnabled
      },
      RegExpFilter {
        roleName: "deviceModel"
        pattern: searchPattern
        enabled: searchPattern.length > 0
        caseSensitivity: Qt.CaseInsensitive
      },
      ListFilter {
        id: listSorter
        enabled: false
      }

    ]
    sorters: [
      RoleSorter {
        roleName: "lastSeen"
        enabled: sorter === DevicesSortMenuModel.Sorters.LastSeen
      },
      RoleSorter {
        roleName: "active"
        enabled: sorter === DevicesSortMenuModel.Sorters.ActiveFirst
      },
      RoleSorter {
        roleName: "created"
        enabled: sorter === DevicesSortMenuModel.Sorters.AddedMostRecently
      },
      RoleSorter {
        roleName: "powerDevice"
        enabled: sorter === DevicesSortMenuModel.Sorters.PowerDevices
      },
      RoleSorter {
        roleName: "masterDevice"
        enabled: sorter === DevicesSortMenuModel.Sorters.MasterDevices
      }
    ]
  }
}
