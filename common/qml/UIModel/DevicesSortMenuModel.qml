import QtQuick 2.0
import com.test 1.0
import SortFilterProxyModel 0.2

Item {
  property alias model: sortModel
  property int defaultSorter: DevicesSortMenuModel.Sorters.LastSeen
  enum Sorters {
    LastSeen, ActiveFirst, AddedMostRecently, PowerDevices, MasterDevices
  }

  ListModel {
    id: sortModel
  }

  Component.onCompleted: {
    sortModel.append({ name: Strings.lastSeenStr, type: DevicesSortMenuModel.Sorters.LastSeen});
    sortModel.append({ name: Strings.activeFirst, type: DevicesSortMenuModel.Sorters.ActiveFirst});
    sortModel.append({ name: Strings.addedMostRecently, type: DevicesSortMenuModel.Sorters.AddedMostRecently});
    sortModel.append({ name: Strings.powerDevicesStr, type: DevicesSortMenuModel.Sorters.PowerDevices});
    sortModel.append({ name: Strings.masterDevices, type: DevicesSortMenuModel.Sorters.MasterDevices});
  }
}
