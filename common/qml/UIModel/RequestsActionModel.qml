import QtQuick 2.15
import QtQml.Models 2.12
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2

Item {
  property alias sourceModel: actionModel
  property alias model: actionModel

  ListModel {
    id: actionModel
  }

  Action {
    id: approve
    onTriggered: PendingDeviceListModel.approveAll()
  }
  Action {
    id: reject
    onTriggered: PendingDeviceListModel.rejectAll()
  }
  Action {
    id: block
    onTriggered: PendingDeviceListModel.blockAll()
  }

  Component.onCompleted: {
    actionModel.append({ name: Strings.approveAll,
                         applied: true,
                         iconSource: "qrc:/check-circle.svg",
                         actionSource: approve,
                         details: ""
                       })
    actionModel.append({ name: Strings.rejectAll,
                         applied: true,
                         iconSource: "qrc:/delete-dark.svg",
                         actionSource: reject,
                         details: ""
                       })
    actionModel.append({ name: Strings.blockAll,
                         applied: false,
                         iconSource: "qrc:/remove-circle.svg",
                         actionSource: block,
                         details: ""
                       })
  }
}
