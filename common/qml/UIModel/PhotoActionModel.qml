import QtQuick 2.15
import QtQml.Models 2.12
import com.test 1.0
import com.testintl 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2

Item {
  id: menu
  property alias sourceModel: actionModel
  property alias model: actionModel

  ListModel {
    id: actionModel
  }

  Action {
    id: shareAction
  }

  Action {
    id: openWithAction
  }

  Component.onCompleted: {
    // Populate model
    actionModel.append({ name: Strings.openWith,
                         iconSource: "qrc:/change-owner.svg",
                         actionSource: openWithAction,
                       })
    actionModel.append({ iconSource: "qrc:/share.svg",
                         name: Strings.share,
                         actionSource: shareAction,
                       })
  }
}
