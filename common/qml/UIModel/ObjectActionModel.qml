import QtQuick 2.15
import QtQml.Models 2.12
import com.test 1.0
import com.testintl 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2

Item {
  id: menu
  property alias sourceModel: actionModel
  property alias model: proxy
  property var actionList: []

  // Set this property to SyncableObject.ObjectType when used with selection[] property
  property int objectType: SyncableObject.Generic

  signal modelLoaded()
  signal openExportPopup(int xPos, int yPos)
  signal cleanSelection()

  property var currentObject: undefined
  property var selection: []

  readonly property bool hasSelection: selection.length > 0

  property bool anyPinned: false
  property bool allPinned: false
  property bool anyLocked: false
  property bool allLocked: false
  property bool anyAddedInArca: false
  property bool anyIsArcaType: false
  property bool anyIsFileType: false
  property bool anyIsQuodType: false
  property bool initialized: false
  // Predefined actions lists

  readonly property var singleActions: [Enums.AddToArcaAction, Enums.MoveToArcaAction, Enums.RemoveFromArcaAction,
      Enums.LockAction, Enums.UnLockAction, Enums.RenameAction, Enums.DuplicateAction, Enums.DeleteAction, Enums.OpenWithAction,
      Enums.PinAction, Enums.UnPinAction, Enums.SetPermissionsAction, Enums.SetSyncPropertyAction, Enums.KeepRemoteAction, Enums.SendAction,
      Enums.ShareAction, Enums.UnshareAction, Enums.ExportAction, Enums.SyncNowAction, Enums.ViewAndEditAction,
      Enums.PropertiesAction, Enums.ShowInfoAction]
    readonly property var selectMenuAction: [Enums.AddToArcaAction, Enums.MoveToArcaAction, Enums.RemoveFromArcaAction,
      Enums.LockAction, Enums.UnLockAction, Enums.DuplicateAction, Enums.DeleteAction,
      Enums.PinAction, Enums.UnPinAction, Enums.SetPermissionsAction, Enums.KeepRemoteAction, Enums.SendAction,
      Enums.ShareAction, Enums.UnshareAction, Enums.ExportAction, Enums.SyncNowAction]

  property bool isHomeScreen: homeManager.flow === HomeManager.None
  property var currentModel: {
    if (homeManager.flow === HomeManager.ManageArca ||
        homeManager.flow === HomeManager.CreateArca)
      return ArcaListModel
    if (homeManager.flow === HomeManager.ManageQuod ||
        homeManager.flow === HomeManager.CreateQuod)
      return QuodListModel
    if (homeManager.flow === HomeManager.ManageFile ||
        homeManager.flow === HomeManager.CreateFile)
      return FileListModel
    if (homeManager.flow === HomeManager.None)
      return SyncableObjectListModel
  }

  // This property holds object type. If menu has selection then it is equal to objectType,
  // otherwise it is equal objectType property from currentObject (SyncableObject.objectType)
  readonly property int __syncableType: hasSelection ? objectType : __objectTypeFromObject
  readonly property int __objectTypeFromObject: currentObject != undefined ? currentObject.objectType : -1
  readonly property int __typeForFilter: {
    switch(__syncableType) {
    case SyncableObject.Arca:
      return Enums.Arca;
    case SyncableObject.Quod:
      return Enums.Quod;
    case SyncableObject.File:
      return Enums.File
    case SyncableObject.Generic:
      return anyIsFileType ? Enums.File : anyIsQuodType ? Enums.Quod : Enums.Arca
    default:
      return -1;
    }
  }

  on__TypeForFilterChanged: {
    console.log("Invalidate filter", __syncableType, __typeForFilter)
    proxy.invalidate()
  }

  onSelectionChanged: {
    console.log("SELECTION CHANGED", selection.length, hasSelection, currentModel)
    updateModelValues()
    if (isHomeScreen) {
      anyIsFileType = SyncableObjectListModel.anyIsFileType(selection)
      anyIsQuodType = SyncableObjectListModel.anyIsQuodType(selection)
    }
  }

  onAnyIsArcaTypeChanged: {
//    if (anyIsArcaType) {
//      objectType = SyncableObject.Arca
//    } else {
//      objectType = SyncableObject.Quod
//    }
  }

  onCurrentObjectChanged: {
    if (!hasSelection && currentObject !== undefined) {
      updateModelValues();
    }
  }

  onObjectTypeChanged: console.log("OBJECT TYPE CHANGED", objectType)

  onModelLoaded: updateModelValues()

  Connections {
    target: homeManager.currentEditor
    function onObjectChanged() {
      if (!hasSelection && currentObject != undefined) {
        updateModelValues();
      }
    }
    function onCleanSelection() {
      menu.cleanSelection()
    }
  }

  function updateModelValues() {
      if (!initialized) return
      if(UserManager.currentUser){
        actionModel.setProperty(Enums.SetPermissionsAction, "visible", UserManager.currentUser.hasPower === UserManager.currentUser.accountOwner)
        actionModel.setProperty(Enums.DeleteAction, "visible", !UserManager.currentUser.guest)
      }

      if (selection.length > 0) {

          // Pinned update
          anyPinned = currentModel.anyPinned(selection);
          allPinned = currentModel.allPinned(selection);
          actionModel.setProperty(Enums.PinAction, "visible", !allPinned)
          actionModel.setProperty(Enums.UnPinAction, "visible", anyPinned)

          // Shared update
          var anyShared = true//
          var allShared = false//
          actionModel.setProperty(Enums.ShareAction, "visible", !allShared)
          actionModel.setProperty(Enums.UnShareAction, "visible", anyShared)

          // Locked update
          anyLocked = currentModel.anyLocked(selection);
          allLocked = currentModel.allLocked(selection);
          actionModel.setProperty(Enums.LockAction, "visible", !allLocked)
          actionModel.setProperty(Enums.UnLockAction, "visible", anyLocked)

          // Arca update
          anyAddedInArca = currentModel.anyAddedInArca(selection)
          actionModel.setProperty(Enums.MoveToArcaAction, "visible", anyAddedInArca)
          actionModel.setProperty(Enums.RemoveFromArcaAction, "visible", anyAddedInArca)

      } else {
          if (currentObject != undefined) {

              actionModel.setProperty(Enums.SetSyncPropertyAction, "visible", UserManager.currentUser.userId === currentObject.ownerId)

              // Pinned update
              actionModel.setProperty(Enums.PinAction, "visible", !currentObject.pinned)
              actionModel.setProperty(Enums.UnPinAction, "visible", currentObject.pinned)

              // Shared update
              var shared = true//currentObject.shared
              actionModel.setProperty(Enums.ShareAction, "visible", !shared)
              actionModel.setProperty(Enums.UnShareAction, "visible", shared)

              if (currentObject.objectType === SyncableObject.File || currentObject.objectType === SyncableObject.Quod) {
                  // Locked update
                  actionModel.setProperty(Enums.LockAction, "visible", !currentObject.locked)
                  actionModel.setProperty(Enums.UnLockAction, "visible", currentObject.locked)
                  // Arca update
                  var addedInArca = currentModel.addedInArca(currentObject.id, currentObject.objectType)
                  actionModel.setProperty(Enums.MoveToArcaAction, "visible", addedInArca)
                  actionModel.setProperty(Enums.RemoveFromArcaAction, "visible", addedInArca)
              }
          }
      }
  }

  ListModel {
    id: actionModel
  }

  SortFilterProxyModel {
    id: proxy
    sourceModel: actionModel
    filters: [
      ListFilter {
        roleName: "actionEnum"
        list: menu.actionList
        inverted: false
      },
      ExpressionFilter {
        enabled: true
        expression: {
          return visibleFor & menu.__typeForFilter
        }
      },
      ValueFilter {
         roleName: "visible"
         value: true
      },
      ExpressionFilter {
        enabled: !app.isPhone
        expression: {
          return actionEnum !== 0
        }
      }

    ]
    dynamicSortFilter: true
    sorters: [
      ExpressionSorter {
        enabled: true
        expression: {
          return menu.actionList.indexOf(modelLeft.actionEnum) < menu.actionList.indexOf(modelRight.actionEnum)
        }
      }
    ]
  }

  // Actions
  Action {
    id: editAction
    onTriggered: {
      console.log("Triggered edit action")
      quodEditor.cleanEditSection()
//      quodEditor.editedSectionName = quodEditor.object.quodType.name
      homeManager.openPage(Pages.EditQuodDetailsPage)
    }
  }

  Action {
    id: renameAction
  }
  Action {
    id: openWithAction
  }
  Action {
    id: share
  }
  Action {
    id: unshare
  }
  Action {
    id: keepRemote
  }
  Action {
     id: exportAction
     onTriggered: openExportPopup(source.x, source.y)
   }
  Action {
    id: properties
  }
  Action {
    id: duplicate
    onTriggered: {
      if (hasSelection) {
        homeManager.currentEditor.duplicateList(selection)
        cleanSelection()
      } else {
        homeManager.currentEditor.duplicateOne(currentObject.id)
      }
    }
  }
  Action {
    id: deleteAction
    onTriggered: {
      if (hasSelection) {
        homeManager.currentEditor.preprareToDelete(selection)
        homeManager.enterConfirmObjectDeletionPopupPage()
      } else {
        homeManager.currentEditor.preprareToDelete(currentObject.id, currentObject.objectType)
        homeManager.enterConfirmObjectDeletionPopupPage()
      }
    }
  }
  Action {
    id: syncNow
  }
  Action {
    id: viewAndEdit
    onTriggered: {
      //if (hasSelection) {
      //  homeManager.enterEditListPage(selection, Pages.SyncPage, __syncableType)
      //} else {
      //  homeManager.enterEditPage(currentObject.id, Pages.SyncPage,  __syncableType)
      //}
    }
  }
  Action {
    id: send
  }
  Action {
    id: setSyncProperty
    onTriggered:{
      //if (hasSelection) {
      //  console.log("EDIT TAGS", __syncableType, objectType)
      //  homeManager.enterEditListPage(selection, Pages.AddTagsPage,  __syncableType)
      //} else {
      //  homeManager.enterEditPage(currentObject.id, Pages.AddTagsPage,  __syncableType)
      //}
    }
  }
  Action {
    id: pinAction
    onTriggered: {
      if (hasSelection) {
        console.log("PIN action, any object pinned?", anyPinned)
        currentModel.pinList(selection, !anyPinned)
        cleanSelection()
      } else {
        currentModel.pinOne(currentObject.id, currentObject.objectType)
      }
    }
  }
  Action {
    id: unpinAction
    onTriggered: {
      if (hasSelection) {
        console.log("PIN action, any object pinned?", anyPinned)
        currentModel.pinList(selection, !anyPinned)
        cleanSelection()
      } else {
        currentModel.pinOne(currentObject.id, currentObject.objectType)
      }
    }
  }
  Action {
    id: setPermissions
    onTriggered: {
     if (hasSelection) {
       homeManager.enterEditListPage(selection, Pages.EditPermissionsPage,  __syncableType)
     } else {
       homeManager.enterEditPage(currentObject.id, Pages.EditPermissionsPage,  __syncableType)
     }
    }
  }
  Action {
    id: showInfo
    onTriggered: homeManager.enterEditPage(currentObject.id, Pages.ShowInfoPage,  __syncableType)
  }
  Action {
    id: addToArcaAction
    onTriggered: linkToArca(Editor.AddToArca)
  }
  Action {
    id: moveToArcaAction
    onTriggered: linkToArca(Editor.MoveToArca)
  }
  Action {
    id: removeFromArcaAction
    onTriggered: linkToArca(Editor.RemoveFromArca)
  }
  Action {
    id: lockAction
    onTriggered: {
      if (hasSelection) {
        currentModel.lockList(selection, !anyLocked)
        cleanSelection()
      } else {
        currentModel.lockOne(currentObject.id, currentObject.objectType)
      }
    }
  }
  Action {
    id: unlockAction
    onTriggered: {
      if (hasSelection) {
        currentModel.lockList(selection, !anyLocked)
        cleanSelection()
      } else {
        currentModel.lockOne(currentObject.id, currentObject.objectType)
      }
    }
  }

  function linkToArca(action) {
    if (hasSelection) {
      homeManager.enterLinkListToArcaPage(selection, __syncableType, action)
    } else {
      homeManager.enterLinkToArcaPage(currentObject.id, __syncableType, action)
    }
  }

  Component.onCompleted: {
      // Populate model
      actionModel.append({ name: Strings.edit,
                             expanded: true,
                             iconSource: "qrc:/edit.svg",
                             actionSource: editAction,
                             visibleFor: Enums.Quod,
                             visible: true,
                             actionEnum: Enums.EditAction
                         })
      actionModel.append({ name: Strings.rename,
                             expanded: true,
                             iconSource: "qrc:/rename.svg",
                             actionSource: renameAction,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.RenameAction
                         })
      actionModel.append({ iconSource: "qrc:/share.svg",
                             name: Strings.share,
                             expanded: true,
                             actionSource: share,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.ShareAction
                         })
      actionModel.append({ iconSource: "qrc:/unshare.svg",
                             name: Strings.unshare,
                             expanded: true,
                             actionSource: unshare,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.UnshareAction
                         })
      actionModel.append({ iconSource: "qrc:/export-default.svg",
                             name: Strings.exportStr,
                             expanded: true,
                             actionSource: exportAction,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.ExportAction
                         })
      actionModel.append({ iconSource: "qrc:/duplicate.svg",
                             name: Strings.duplicate,
                             expanded: true,
                             actionSource: duplicate,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.DuplicateAction
                         })
      actionModel.append({ iconSource: "qrc:/delete.svg",
                             name: Strings.deleteStr,
                             expanded: true,
                             actionSource: deleteAction,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.DeleteAction
                         })
      actionModel.append({ iconSource: "qrc:/pin.svg",
                             name: Strings.pinObject,
                             expanded: true,
                             actionSource: pinAction,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.PinAction
                         })
      actionModel.append({ iconSource: "qrc:/unpin.svg",
                             name: Strings.unpin,
                             expanded: true,
                             actionSource: unpinAction,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.UnPinAction
                         })
      actionModel.append({ iconSource: "qrc:/arca-add-to.svg",
                             name: Strings.addToArca,
                             expanded: true,
                             actionSource: addToArcaAction,
                             visibleFor: Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.AddToArcaAction
                         })
      actionModel.append({ iconSource: "qrc:/arca-move-to.svg",
                             name: Strings.moveToArca,
                             expanded: true,
                             actionSource: moveToArcaAction,
                             visibleFor: Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.MoveToArcaAction
                         })
      actionModel.append({ iconSource: "qrc:/arca-remove.svg",
                             name: Strings.removeFromArca,
                             expanded: true,
                             actionSource: removeFromArcaAction,
                             visibleFor: Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.RemoveFromArcaAction
                         })
      actionModel.append({ iconSource: "qrc:/lock.svg",
                             name: Strings.lock,
                             expanded: true,
                             actionSource: lockAction,
                             visibleFor: Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.LockAction
                         })
      actionModel.append({ iconSource: "qrc:/unlock.svg",
                             name: Strings.unlock,
                             expanded: true,
                             actionSource: unlockAction,
                             visibleFor: Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.UnLockAction
                         })
      actionModel.append({ iconSource: "qrc:/sync.svg",
                             name: Strings.syncNow,
                             expanded: true,
                             actionSource: syncNow,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.SyncNowAction
                         })
      actionModel.append({ iconSource: "qrc:/sent-email.svg",
                             name: Strings.send,
                             expanded: true,
                             actionSource: send,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.SendAction
                         })
      actionModel.append({ iconSource: "qrc:/set-sync-property.svg",
                             name: Strings.setSyncProperty,
                             expanded: true,
                             actionSource: setSyncProperty,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.SetSyncPropertyAction
                         })
      actionModel.append({ iconSource: "qrc:/view-edit.svg",
                             name: Strings.viewAndEdit,
                             expanded: true,
                             actionSource: viewAndEdit,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.ViewAndEditAction
                         })
      actionModel.append({ name: Strings.openWith,
                             expanded: true,
                             iconSource: "qrc:/open-with.svg",
                             actionSource: openWithAction,
                             visibleFor: Enums.File,
                             visible: true,
                             actionEnum: Enums.OpenWithAction
                         })
      actionModel.append({ iconSource: "qrc:/cog-default.svg",
                             name: Strings.setPermissions,
                             expanded: true,
                             actionSource: setPermissions,
                             visibleFor: Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.SetPermissionsAction
                         })
      actionModel.append({ iconSource: "qrc:/all.svg",
                             name: Strings.properties,
                             expanded: true,
                             actionSource: properties,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.PropertiesAction
                         })
      actionModel.append({ iconSource: "qrc:/keep-remote.svg",
                             name: Strings.keepRemote,
                             expanded: true,
                             actionSource: keepRemote,
                             visibleFor: Enums.File,
                             visible: true,
                             actionEnum: Enums.KeepRemoteAction
                         })

      actionModel.append({ name: Strings.showInfo,
                             iconSource: "qrc:/info-circle.svg",
                             expanded: true,
                             actionSource: showInfo,
                             visibleFor: Enums.Arca | Enums.Quod | Enums.File,
                             visible: true,
                             actionEnum: Enums.ShowInfoAction
                         })

      initialized = true

      modelLoaded()
      proxy.invalidate()
  }
}
