import QtQuick 2.15
import QtQml.Models 2.12
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2

Item {
  property alias sourceModel: filterModel
  property alias model: filterModel
  property var searchModel: null
  property var _phoneFilters: [SFilter.OwnedByFilter, SFilter.SentByFilter,SFilter.SharedByFilter,
    SFilter.ModifiedFilter,SFilter.OpenedFilter,SFilter.TagFilter,SFilter.TypeFilter,
    SFilter.SyncStatusFilter,SFilter.LockStatusFilter]
  property var _desktopFilters: [SFilter.OwnedByFilter, SFilter.SentByFilter,SFilter.SharedByFilter,
    SFilter.ModifiedFilter,SFilter.OpenedFilter,SFilter.TagFilter]
  property var _filters: app.isPhone ? _phoneFilters : _desktopFilters

  Connections {
    target: searchModel.filters
    function onFiltersUpdated() {
      var details = searchModel.filters.filtersDetails()

      for (var prop in details) {
        var det = details[prop] === undefined ? "" : details[prop]
        if (_filters.indexOf(prop) < 0) continue
        console.log("Object item:", prop, "=", details[prop], "len", det, det.length)
        filterModel.setProperty(_filters.indexOf(prop), "det", det)
        filterModel.setProperty(_filters.indexOf(prop), "applied", det.length > 0)
      }
      if (!app.isPhone) {
        console.log("More filters det", searchModel.filters.moreFiltersDetails());
        var moreDet = searchModel.filters.moreFiltersDetails()
        filterModel.setProperty(_desktopFilters.length, "det", moreDet)
        filterModel.setProperty(_desktopFilters.length, "applied", moreDet.length > 0)
      }
    }
  }

  function removeFilter(fenum){
    searchModel.removeFilter(fenum)
  }

  ListModel {
    id: filterModel
    dynamicRoles: true
  }

  Action {
    id: ownedBy
  }
  Action {
    id: sentBy
  }
  Action {
    id: sharedBy
  }
  Action {
    id: modified
    onTriggered: {
      if (app.isPhone) {
        homeManager.enterFilterPage(Pages.DateFilterPage,{"type":SFilter.ModifiedFilter, "searchModel":searchModel})
      } else {
        showDate(SFilter.ModifiedFilter, source.x, source.width)
      }
    }
  }
  Action {
    id: opened
    onTriggered: {
      if (app.isPhone) {
        homeManager.enterFilterPage(Pages.DateFilterPage,{"type":SFilter.OpenedFilter, "searchModel":searchModel})
      } else {
        showDate(SFilter.OpenedFilter, source.x, source.width)
      }
    }
  }
  Action {
    id: tags
    onTriggered: {
      if (app.isPhone) {
        homeManager.enterFilterPage(Pages.TagsFiltersPage,{"searchModel":searchModel})
      } else {
        showTags(source.x, source.width)
      }
    }
  }

  Action {
    id: type
    onTriggered: {
      if (app.isPhone) {
        homeManager.enterFilterPage(Pages.FiltersTypePage,{"searchModel":searchModel})
      }
    }
  }
  Action {
    id: syncStatus
    onTriggered: {
      if (app.isPhone) {
        homeManager.enterFilterPage(Pages.SyncStatusPage,{"searchModel":searchModel})
      }
    }
  }
  Action {
    id: lockStatus
    onTriggered: {
      if (app.isPhone) {
        homeManager.enterFilterPage(Pages.LockStatusPage,{"searchModel":searchModel})
      }
    }
  }
  Action {
    id: more
    onTriggered: showMore()
  }

  Component.onCompleted: {
    filterModel.append({ fEnum: SFilter.OwnedByFilter,
                         name: Strings.ownedBy,
                         applied: false,
                         iconSource: "qrc:/single-user.svg",
                         actionSource: ownedBy,
                         det: ""
                       })
    filterModel.append({ fEnum: SFilter.SentByFilter,
                         name: Strings.sentBy,
                         applied: false,
                         iconSource: "qrc:/sent-email.svg",
                         actionSource: sentBy,
                         det: ""
                       })
    filterModel.append({ fEnum: SFilter.SharedByFilter,
                         name: Strings.sharedBy,
                         applied: false,
                         iconSource: "qrc:/share-2.svg",
                         actionSource: sharedBy,
                         det: ""
                       })
    filterModel.append({ fEnum: SFilter.ModifiedFilter,
                         name: Strings.modified,
                         applied: false,
                         iconSource: "qrc:/calendar.svg",
                         actionSource: modified,
                         det: ""
                       })
    filterModel.append({ fEnum: SFilter.OpenedFilter,
                         name: Strings.opened,
                         applied: false,
                         iconSource: "qrc:/date-opened.svg",
                         actionSource: opened,
                         det: ""
                       })
    filterModel.append({ fEnum: SFilter.TagFilter,
                         name: Strings.tags,
                         applied: false,
                         iconSource: "qrc:/tags.svg",
                         actionSource: tags,
                         det: ""
                       })

    if(app.isPhone){
      filterModel.append({ fEnum: SFilter.TypeFilter,
                           name: Strings.type,
                           applied: false,
                           iconSource: "qrc:/object-type.svg",
                           actionSource: type,
                           det: ""
                         })
      filterModel.append({ fEnum: SFilter.SyncStatusFilter,
                           name: Strings.syncStatus,
                           applied: false,
                           iconSource: "qrc:/sync-status.svg",
                           actionSource: syncStatus,
                           det: ""
                         })
      filterModel.append({ fEnum: SFilter.LockStatusFilter,
                           name: Strings.lockStatus,
                           applied: false,
                           iconSource: "qrc:/lock.svg",
                           actionSource: lockStatus,
                           det: ""
                         })
    } else {
      filterModel.append({ fEnum: SFilter.MoreFilter,
                           name: Strings.more,
                           applied: false,
                           iconSource: "qrc:/filters.svg",
                           actionSource: more,
                           det: ""
                         })
    }
  }
}
