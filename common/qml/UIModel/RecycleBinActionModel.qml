import QtQuick 2.15
import QtQml.Models 2.12
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2

Item {
  id: root
  property alias sourceModel: actionModel
  property alias model: proxy
  property var actionList: []

  property var currentObject: undefined
  property var selection: []
  readonly property bool hasSelection: selection.length > 0

  property bool inSelectMode: false

  signal modelLoaded()
  signal cleanSelection()
  signal selectCurrent()

  property bool __initialized: false

  // Predefined actions lists
  readonly property var singleRecycledItemActions: [
    RecycleBinActionModel.Actions.Restore,
    RecycleBinActionModel.Actions.Remove
  ]

  readonly property var recycleBinPageActions: [
    RecycleBinActionModel.Actions.Select,
    RecycleBinActionModel.Actions.Empty
  ]

  onSelectionChanged: updateModelValues()
  onModelLoaded: updateModelValues()
  onCurrentObjectChanged: {
    if (!hasSelection && currentObject !== undefined) {
      updateModelValues();
    }
  }
  onInSelectModeChanged: {
    if (app.isMobile) {
      actionModel.setProperty(RecycleBinActionModel.Actions.Select, "visible", !inSelectMode)
    }
  }

  // Warning, order matters! Should be the same as element order in model
  enum Actions {
    Restore, Remove, Empty, Select
  }

  function enumToIndex(e) {
    for (var i = 0; i < actionModel.count; ++i) {
      var item = actionModel.get(i)
      if (item.actionEnum === e) return i;
    }
  }

  function updateModelValues() {
    if (!__initialized) return
    if (selection.length > 0) {
      if (app.isMobile) {
        actionModel.setProperty(enumToIndex(RecycleBinActionModel.Actions.Select), "visible", false)
      }
    } else {
      console.log("UPDATE VALUES", currentObject)
      if (currentObject != undefined) {
        if (app.isMobile) {
          actionModel.setProperty(enumToIndex(RecycleBinActionModel.Actions.Select), "visible", true)
        }
      }
    }
  }

  ListModel {
    id: actionModel
  }

  SortFilterProxyModel {
    id: proxy
    sourceModel: actionModel
    filters: [
      ListFilter {
        roleName: "actionEnum"
        list: root.actionList
        inverted: false
      },
      ValueFilter {
        roleName: "visible"
        value: true
      }
    ]
  }

  Action {
    id: restore
    onTriggered: {
      RecycleBinModel.restoreItems(selection)
    }
  }

  Action {
    id: remove
    onTriggered: {
      RecycleBinModel.deleteItems(selection);
    }
  }

  Action {
    id: empty
    onTriggered: {
      RecycleBinModel.emptyRecycleBin()
    }
  }

  Action {
    id: select
    onTriggered: selectCurrent()
  }

  Component.onCompleted: {
    actionModel.append({ name: Strings.restore,
                         iconSource: "qrc:/unshare.svg",
                         actionSource: restore,
                         expanded: true,
                         actionEnum: RecycleBinActionModel.Actions.Restore,
                         visible: true
                       })

    actionModel.append({ name: Strings.remove,
                         iconSource: "qrc:/close-circle.svg",
                         actionSource: remove,
                         expanded: true,
                         actionEnum: RecycleBinActionModel.Actions.Remove,
                         visible: true
                       })

    actionModel.append({ name: Strings.emptyRecycleBinAction,
                         iconSource: "qrc:/delete.svg",
                         actionSource: empty,
                         expanded: true,
                         actionEnum: RecycleBinActionModel.Actions.Empty,
                         visible: true
                       })

    if (app.isMobile) {
      actionModel.append({ name: Strings.select,
                           iconSource: "qrc:/check-circle.svg",
                           actionSource: select,
                           expanded: true,
                           actionEnum: RecycleBinActionModel.Actions.Select,
                           visible: true
                         })
    }

    __initialized = true
    modelLoaded()
  }
}
