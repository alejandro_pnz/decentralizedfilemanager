import QtQuick 2.15
import QtQml.Models 2.12
import com.test 1.0
import QtQuick.Controls 2.15

Item {
  id: root
  property alias sourceModel: actionModel
  property alias model: actionModel
  signal modelLoaded()

  ListModel {
    id: actionModel
  }

  Action {
    id: approve
  }
  Action {
    id: reject
  }
  Action {
    id: block
  }

  Component.onCompleted: {
    actionModel.append({ iconSource: "qrc:/check-circle.svg",
                         name: Strings.approve,
                         expanded: false,
                         actionSource: approve,
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/delete-dark.svg",
                         name: Strings.reject,
                         expanded: false,
                         actionSource: reject,
                         visible: true
                       })
    actionModel.append({ name: Strings.block,
                         iconSource: "qrc:/remove-circle.svg",
                         actionSource: block,
                         expanded: false,
                         visible: true
                       })

    modelLoaded()
  }
}
