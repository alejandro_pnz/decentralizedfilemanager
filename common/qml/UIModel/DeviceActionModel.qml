import QtQuick 2.15
import QtQml.Models 2.12
import com.test 1.0
import com.testintl 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2

Item {
  id: root
  property alias sourceModel: actionModel
  property alias model: proxy
  property var actionList: []

  property bool isAO: AuthenticationManager.currentAccount.accountOwner
  property var currentObject: undefined
  property var selection: []
  readonly property var editor: homeManager.deviceEditor
  readonly property bool hasSelection: selection.length > 0

  property bool inSelectMode: false

  signal modelLoaded()
  signal cleanSelection()
  signal selectCurrent()

  property bool anyPinned: false
  property bool anyBlocked: false
  property bool anyActive: false
  property bool anyPowerDevice: false

  property bool __initialized: false

  // Predefined actions lists
  readonly property var singleDeviceActions: [
    DeviceActionModel.Actions.Pin,
    DeviceActionModel.Actions.Select,
    DeviceActionModel.Actions.Activate,
    DeviceActionModel.Actions.Block,
    DeviceActionModel.Actions.Delete,
    DeviceActionModel.Actions.Logout,
    DeviceActionModel.Actions.Sync
  ]
  readonly property var oneDeviceActionsOnlyShowInfo: [
    DeviceActionModel.Actions.ShowInfo
  ]
  readonly property var oneDeviceActions: [
    DeviceActionModel.Actions.Delete,
    DeviceActionModel.Actions.Logout,
    DeviceActionModel.Actions.Sync,
    DeviceActionModel.Actions.ShowInfo
  ]
  readonly property var devicePageActions: [
    //DeviceActionModel.Actions.Select,
    //DeviceActionModel.Actions.LinkCloud,
    DeviceActionModel.Actions.AddDevice,
    DeviceActionModel.Actions.LinkAccount,
    DeviceActionModel.Actions.LogoutAll,
    DeviceActionModel.Actions.SyncAll,
    DeviceActionModel.Actions.PauseSync
  ]

  onSelectionChanged: updateModelValues()
  onModelLoaded: updateModelValues()
  onCurrentObjectChanged: {
    if (!hasSelection && currentObject !== undefined) {
      updateModelValues();
    }
  }
  onInSelectModeChanged: {
    if (app.isMobile) {
      actionModel.setProperty(DeviceActionModel.Actions.Select, "visible", !inSelectMode)
    }
  }

  // Warning, order matters! Should be the same as element order in model
  enum Actions {
    AddDevice, Pin, Select, Activate, Block, Delete, Logout, Sync, ShowInfo, LinkCloud,
    LinkAccount, LogoutAll, SyncAll, PauseSync
  }

  function enumToIndex(e) {
    for (var i = 0; i < actionModel.count; ++i) {
      var item = actionModel.get(i)
      if (item.actionEnum === e) return i;
    }
  }

  function updateModelValues() {
    if (!__initialized) return
    if (selection.length > 0) {
      anyPinned = DeviceListModel.anyPinned(selection)
      __updatePin(!anyPinned)

      anyActive = DeviceListModel.anyActive(selection);
      __updateActivate(!anyActive)

      anyBlocked = DeviceListModel.anyBlocked(selection);
      __updateBlock(!anyBlocked)

      if (app.isMobile) {
        actionModel.setProperty(enumToIndex(DeviceActionModel.Actions.Select), "visible", false)
      }
    } else {
      console.log("UPDATE VALUES", currentObject)
      if (currentObject != undefined) {
        __updatePin(!currentObject.pinned)
        __updateActivate(!currentObject.active)
        __updateBlock(!currentObject.blocked)

        if (app.isMobile) {
          actionModel.setProperty(enumToIndex(DeviceActionModel.Actions.Select), "visible", true)
        }
      }
    }    
  }

  function __updatePin(show) {
    actionModel.setProperty(enumToIndex(DeviceActionModel.Actions.Pin), "name",
                            show ? Strings.pinObject : Strings.unpin)
  }

  function __updateActivate(show) {
    actionModel.setProperty(enumToIndex(DeviceActionModel.Actions.Activate), "name",
                            show ? Strings.activate : Strings.deactivate)
    actionModel.setProperty(enumToIndex(DeviceActionModel.Actions.Activate), "iconSource",
                            show ? "qrc:/check-circle.svg" : "qrc:/delete-dark.svg")
  }

  function __updateBlock(show) {
    actionModel.setProperty(enumToIndex(DeviceActionModel.Actions.Block), "name",
                            show ? Strings.block : Strings.unblock)
    actionModel.setProperty(enumToIndex(DeviceActionModel.Actions.Block), "iconSource",
                            show ? "qrc:/remove-circle.svg" : "qrc:/unblock.svg")
  }

  ListModel {
    id: actionModel
  }

  SortFilterProxyModel {
    id: proxy
    sourceModel: actionModel
    filters: [
      ListFilter {
        roleName: "actionEnum"
        list: root.actionList
        inverted: false
      },
      ValueFilter {
        roleName: "visible"
        value: true
      }
    ]
  }

  Action {
    id: pinAction
    onTriggered: {
      if (hasSelection) {
        console.log("PIN action, any object pinned?", anyPinned)
        DeviceListModel.pinList(selection, !anyPinned)
        cleanSelection()
      } else {
        DeviceListModel.pinOne(currentObject.id, currentObject.objectType)
      }
    }
  }

  Action {
    id: activate
    onTriggered: {
      if (hasSelection) {
        DeviceListModel.activateList(selection, !anyActive)
        cleanSelection()
      } else {
        DeviceListModel.activateOne(currentObject.id)
      }
    }
  }

  Action {
    id: block
    onTriggered: {
      if (hasSelection) {
        DeviceListModel.blockList(selection, !anyBlocked)
        cleanSelection()
      } else {
        DeviceListModel.blockOne(currentObject.id)
      }
    }
  }

  Action {
    id: deleteDevice
    onTriggered: {
      if (hasSelection) {
        DeviceListModel.removeList(selection)
        cleanSelection()
      } else {
        DeviceListModel.removeOne(currentObject.id)
      }
    }
  }

  Action {
    id: logoutThisDevice
    onTriggered: {
      if (hasSelection) {
        DeviceListModel.logoutList(selection)
        cleanSelection()
      } else {
        DeviceListModel.logoutOne(currentObject.id)
      }
    }
  }

  Action {
    id: syncNow
    onTriggered: {
      // TODO, backend not ready
    }
  }

  Action {
    id: pauseSync
    onTriggered: {
      // TODO, backend not ready
    }
  }
  Action {
    id: addDevice
    onTriggered: {
      homeManager.openAddNewDevicePage(DeviceListModel.canAddNewDevice())
    }
  }

  Action {
    id: showInfo
    onTriggered: {
      homeManager.enterShowInfoPage(currentObject.id, currentObject.objectType)
    }
  }

  Action {
    id: select
    onTriggered: selectCurrent()
  }

  Action {
    id: linkCloudStorage
    onTriggered: {
      homeManager.openPage(Pages.LinkCloudPage)
    }
  }

  Action {
    id: linkAccountStorage
    onTriggered: {
      homeManager.openPage(Pages.LinkCloudPage)
    }
  }

  Action {
    id: logoutAll
    onTriggered: {
      deviceEditor.logoutAll()
    }
  }

  Action {
    id: syncAll
    onTriggered: {
      // TODO, backend not ready
    }
  }

  Component.onCompleted: {
      actionModel.append({ name: Strings.addDevice,
                           iconSource: "qrc:/devices-add.svg",
                           actionSource: addDevice,
                           expanded: true,
                           actionEnum: DeviceActionModel.Actions.AddDevice,
                           visible: true
                         })
    actionModel.append({ iconSource: "qrc:/pin.svg",
                         name: Strings.unpin,
                         expanded: true,
                         actionSource: pinAction,
                         visible: true,
                         actionEnum: DeviceActionModel.Actions.Pin
                       })

    if (app.isPhone) {
      actionModel.append({ name: Strings.select,
                           iconSource: "qrc:/check-circle.svg",
                           actionSource: select,
                           expanded: true,
                           actionEnum: DeviceActionModel.Actions.Select,
                           visible: true
                         })
    }

    actionModel.append({ iconSource: "qrc:/check-circle.svg",
                         name: Strings.activate,
                         expanded: true,
                         actionSource: activate,
                         actionEnum: DeviceActionModel.Actions.Activate,
                         visible: true
                       })

    actionModel.append({ name: Strings.block,
                         iconSource: "qrc:/remove-circle.svg",
                         actionSource: block,
                         expanded: true,
                         actionEnum: DeviceActionModel.Actions.Block,
                         visible: true
                       })

    actionModel.append({ name: Strings.deleteStr,
                         iconSource: "qrc:/delete.svg",
                         actionSource: deleteDevice,
                         expanded: true,
                         actionEnum: DeviceActionModel.Actions.Delete,
                         visible: true
                       })

    actionModel.append({ name: Strings.logoutThisDevice,
                         iconSource: "qrc:/sign-out-default.svg",
                         actionSource: logoutThisDevice,
                         expanded: true,
                         actionEnum: DeviceActionModel.Actions.Logout,
                         visible: true
                       })

    actionModel.append({ name: Strings.syncNow,
                         iconSource: "qrc:/sync.svg",
                         actionSource: syncNow,
                         expanded: true,
                         actionEnum: DeviceActionModel.Actions.Sync,
                         visible: true
                       })

    actionModel.append({ name: Strings.showInfo,
                         iconSource: "qrc:/info-circle.svg",
                         actionSource: showInfo,
                         expanded: true,
                         actionEnum: DeviceActionModel.Actions.ShowInfo,
                         visible: true
                       })

    if(!isAO){
      actionModel.append({ name: Strings.linkCloudStorage,
                           iconSource: "qrc:/personal-cloud.svg",
                           actionSource: linkCloudStorage,
                           expanded: true,
                           actionEnum: DeviceActionModel.Actions.LinkCloud,
                           visible: true
                         })
    } else {
      actionModel.append({ name: Strings.linkPersonal,
                           iconSource: "qrc:/personal-cloud.svg",
                           actionSource: linkCloudStorage,
                           expanded: true,
                           actionEnum: DeviceActionModel.Actions.LinkCloud,
                           visible: true
                         })

      actionModel.append({ name: Strings.linkCloud,
                           iconSource: "qrc:/cloud-account.svg",
                           actionSource: linkAccountStorage,
                           expanded: true,
                           actionEnum: DeviceActionModel.Actions.LinkAccount,
                           visible: true
                         })
    }

    actionModel.append({ name: Strings.logoutAll,
                         iconSource: "qrc:/sign-out-default.svg",
                         actionSource: logoutAll,
                         expanded: true,
                         actionEnum: DeviceActionModel.Actions.LogoutAll,
                         visible: true
                       })

    actionModel.append({ name: Strings.syncAll,
                         iconSource: "qrc:/sync.svg",
                         actionSource: syncAll,
                         expanded: true,
                         actionEnum: DeviceActionModel.Actions.SyncAll,
                         visible: true
                       })

    actionModel.append({ name: Strings.pauseSync,
                         iconSource: "qrc:/pause-no-fill.svg",
                         actionSource: pauseSync,
                         expanded: true,
                         actionEnum: DeviceActionModel.Actions.PauseSync,
                         visible: true
                       })

    __initialized = true
    modelLoaded()
  }
}
