import QtQuick 2.15
import QtQml.Models 2.12
import com.test 1.0
import com.testintl 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2

Item {
  property alias sourceModel: actionModel
  property alias model: actionModel

  signal searchMode()

  ListModel {
    id: actionModel
  }

  Action {
    id: search
    onTriggered: searchMode()
  }

  Component.onCompleted: {
    actionModel.append({ name: Strings.searchTxt,
                         applied: true,
                         iconSource: "qrc:/search.svg",
                         actionSource: search,
                         details: ""
                       })
  }
}
