pragma Singleton

import QtQuick 2.0
import SortFilterProxyModel 0.2
import com.test 1.0
import com.testintl 1.0

SortFilterProxyModel {
  sourceModel: SyncableObjectListModel
  filters: [
    ValueFilter {
      roleName: "objectType"
      value: SyncableObject.File
    }
  ]

  function anyIsArcaType(indexes) {
    return sourceModel.anyIsArcaType(mapListToSource(indexes));
  }

  function pinList(indexes, pin) {
    sourceModel.pinList(mapListToSource(indexes), pin);
  }

  function pinOne(id, type) {
    sourceModel.pinOne(id, type);
  }

  function anyPinned(indexes) {
    return sourceModel.anyPinned(mapListToSource(indexes));
  }

  function lockList(indexes, lock) {
    sourceModel.pinList(mapListToSource(indexes), lock);
  }

  function lockOne(id, type) {
    sourceModel.lockOne(id, type);
  }

  function anyLocked(indexes) {
    return sourceModel.anyLocked(mapListToSource(indexes));
  }
}
