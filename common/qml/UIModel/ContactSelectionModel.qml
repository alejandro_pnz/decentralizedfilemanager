import QtQuick 2.15
import QtQml.Models 2.12
import com.test 1.0
import QtQuick.Controls 2.15

Item {
  id: root
  property alias sourceModel: actionModel
  property alias model: actionModel
  signal modelLoaded()

  ListModel {
    id: actionModel
  }

  Action {
    id: pin
  }

  Action {
    id: sendUserInfo
  }

  Action {
    id: deleteAction
  }

  Component.onCompleted: {
    actionModel.append({ name: Strings.pinTxt,
                         applied: true,
                         iconSource: "qrc:/pin.svg",
                         actionSource: pin,
                         details: ""
                       })

    actionModel.append({ name: app.isPhone ? Strings.sendUserInfo : Strings.send,
                         applied: false,
                         iconSource: "qrc:/sent-email.svg",
                         actionSource: sendUserInfo,
                         details: ""
                       })

    actionModel.append({ name: Strings.deleteStr,
                         applied: false,
                         iconSource: "qrc:/delete.svg",
                         actionSource: deleteAction,
                         details: ""
                       })

    modelLoaded()
  }
}
