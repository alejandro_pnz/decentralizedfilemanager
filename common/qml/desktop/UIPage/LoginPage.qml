import QtQuick 2.0
import UIFragments 1.0
BasePage {
  id: root
  hasLeftButton: false

  objectName: "loginPage"

  property bool busy: false

  Login {
    id: login
    anchors.verticalCenter: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter
    width: root.getWidth(defaultComponentColumnSpan)

    busy: root.busy
  }
}
