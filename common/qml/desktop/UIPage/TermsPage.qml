import QtQuick 2.12
import UIFragments 1.0

BasePage {
  id: root

  objectName: "termsPage"

  Terms {
    id: terms
    anchors.verticalCenter: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter
    width: root.getWidth(defaultComponentColumnSpan)
  }
}
