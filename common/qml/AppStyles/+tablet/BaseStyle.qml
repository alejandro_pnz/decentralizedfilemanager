import QtQuick 2.15
import UIKit 1.0 as UIKit
// This file should contain device-specific styles, sizes etc.
AbstractBaseStyle {
  pageHorizontalMargin: ThemeController.style.margin.m16
  pageHorizontalGutter: s(32)
  pageColumns: 8
  toolbarHeight: s(44)
  backButtonDisplayMode: UIKit.StyledButton.ButtonStyle.Text
  pageHeaderHeight: s(68)
  homePageVerticalMargin: ThemeController.style.margin.m16
}
