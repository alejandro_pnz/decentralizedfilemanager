import QtQuick 2.15
import com.test 1.0
// All button-related styling should be placed here
QtObject {
  property color bgColor: "transparent"
  property color bgColorHover: "transparent"
  property color bgColorPressed: "transparent"
  property color bgColorFocus: "transparent"
  property color bgColorDisabled: "transparent"

  property color bdColor: "transparent"
  property color bdColorHover: "transparent"
  property color bdColorPressed: "transparent"
  property color bdColorFocus: "transparent"
  property color bdColorDisabled: "transparent"

  property color textColor: "transparent"
  property color textColorHover: "transparent"
  property color textColorPressed: "transparent"
  property color textColorFocus: "transparent"
  property color textColorDisabled: "transparent"

  property color iconColor: "transparent"
  property color iconColorHover: "transparent"
  property color iconColorPressed: "transparent"
  property color iconColorFocus: "transparent"
  property color iconColorDisabled: "transparent"
  property color iconColorChecked: "transparent"

  property color dropShadowColor: "transparent"

  property int leftPadding: s(24)
  property int rightPadding: s(24)
  property int topPadding: s(12)
  property int bottomPadding: s(12)

  function s(value) {
    return value * Utility.scaleFactor
  }
}
