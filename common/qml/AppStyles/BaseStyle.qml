import QtQuick 2.15
import UIKit 1.0 as UIKit

// This file should contain device-specific styles, sizes etc.
AbstractBaseStyle {
  pageHorizontalMargin: s(120)
  pageHorizontalGutter: ThemeController.style.margin.m24
  pageColumns: 12
  toolbarHeight: ThemeController.style.defaultInputHeight
  backButtonDisplayMode: UIKit.StyledButton.ButtonStyle.SecondaryCircular
  pageTopMargin: s(60)
  pageHeaderHeight: s(64)
  homePageVerticalMargin: ThemeController.style.margin.m24
  pageFooterHeight: s(64)
}
