import QtQuick 2.15
import com.test 1.0

// All tab button-related styling should be placed here
QtObject {
  property color bgColor: transparent
  property color bgColorSelected: transparent
  property color bgColorFocus: transparent
  property color bgColorHover: transparent

  property color textColor: manateeColor
  property color textColorSelected: slatePurpleColor
  property color textColorFocus: slatePurpleColor
  property color textColorHover: blackColor

  property color iconColor: transparent
  property color iconColorSelected: slatePurpleColor
  property color iconColorFocus: slatePurpleColor
  property color iconColorHover: blackColor

  property color lineColor: transparent
  property color lineColorSelected: slatePurpleColor
  property color lineColorFocus: slatePurpleColor
  property color lineColorHover: altoColor
}

