import QtQuick 2.15


QtObject {
    property color textColor: charadeColor
    property color textColorChecked: blackColor
    
    property color bgColor: whiteColor
    property color bgColorPressed: whiteSmokeColor
    
    property color iconColor: charadeColor
    property color iconColorChecked: blackColor
    property color iconColorPressed: blackColor
}
