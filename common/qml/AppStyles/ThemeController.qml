pragma Singleton

import QtQuick 2.15
import QtQml 2.12
import QuodArca.ThemeSettings 1.0

Item {
  id: root
  property BaseStyle style: QuodArcaTheme
  property ListModel themesModel: ListModel {
    ListElement {
      name: "QuodArcaTheme"
    }
    ListElement {
      name: "QuodArcaDarkTheme"
    }
  }

  property var themeObjectsArray: [
    QuodArcaTheme,
    QuodArcaDarkTheme
  ]

  Component.onCompleted: {
    privateData.activateThemeWithUId(ThemeSettings.activeThemeId)
  }

  Connections {
    target: ThemeSettings
    function onActiveThemeIdChanged() {
      privateData.activateThemeWithUId(ThemeSettings.activeThemeId)
    }
  }

  QtObject {
    id: privateData
    function activateThemeWithUId(uId)
    {
      for (var i = 0; i < themeObjectsArray.length; ++i)
      {
        var currentTheme = themeObjectsArray[i]
        if (currentTheme.uId === uId)
        {
          style = currentTheme
          return
        }
      }
      console.warn("Theme with id[%1] not found.".arg(uId))
    }
  }
}
