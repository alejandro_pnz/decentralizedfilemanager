pragma Singleton
import QtQuick 2.15

// TODO:

BaseStyle {
  uId: "e7704210-f2b6-4179-84fd-6f68c3560625"

  // colors palette
  property color nobleBlackColor: "#212124"
  property color trolleyGreyColor: "#818181"

  // Font & text
  property QtObject font: QtObject {
    id: font
    property string primaryFontFamily: Qt.platform.os === "windows"
                                       || Qt.platform.os === "android"
                                       ? "Roboto" : "SF Pro Text"
    property string secondaryFontFamily: Qt.platform.os === "windows"
                                         || Qt.platform.os === "android"
                                         ? "Roboto" : "SF Pro Display"
    property string codeFont: "Courier Prime Code"
    property int h1Size: Qt.platform.os === "android" || Qt.platform.os === "ios" ? (s(32)) : (s(40))
    property int h2Size: Qt.platform.os === "android" || Qt.platform.os === "ios" ? (s(24)) : (s(32))
    property int h3Size: s(18)
    property int bodySize: s(14)
    property int smallSize: s(12)
    property int xsmallSize: s(11)

    property int h1LineHeight: Qt.platform.os === "android" || Qt.platform.os === "ios" ? (s(40)) : (s(56))
    property int h2LineHeight: s(48)
    property int h3LineHeight: s(32)
    property int bodyLineHeight: s(24)
    property int smallLineHeight: s(16)
    property int xsmallLineHeight: s(12)

    property color defaultColor: whiteColor
  }

  property QtObject window: QtObject {
    property color bgColor: nobleBlackColor
  }

  property QtObject textInput: QtObject {
    property color bgColor: trolleyGreyColor
    property color labelTextColor: shuttleColor
    property color asteriskColor: slatePurpleColor
    property color textColor: whiteColor
    property color placeholderTextColor: manateeColor
    property color errorBorderColor: carnationColor
    property color focusedBorderColor: slatePurpleColor
    property color defaultBorderColor: ironColor
    property color hoveredBorderColor: manateeColor
    property color activeBorderColor: slatePurpleColor
    property int fontSize: font.bodySize
    property color tipTextColor: charadeColor
    property color disabledTextColor: manateeColor
  }

}
