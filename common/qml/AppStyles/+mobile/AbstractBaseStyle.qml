import QtQuick 2.15
import com.test 1.0

QtObject {
  property string uId: "0"

  // colors palette
  property color whiteColor: "#FFFFFF"
  property color ironColor: "#D6D9E1"
  property color manateeColor: "#858A99"
  property color shuttleColor: "#5A5F6D"
  property color charadeColor: "#2F3441"
  property color blackColor: "#030303"
  property color whiteSmokeColor: "#F6F7F9"
  property color whisperColor: "#F1F3F8"
  property color seashellColor: "#ECEEF3"
  property color mercuryColor: "#E7E9EE"
  property color altoColor: "#D6D9E1"
  property color mirageColor: "#1D2027"
  property color governorBayColor: "#3C3BB0"
  property color blueVioletColor: "#4644CA"
  property color slatePurpleColor: "#5C5AE8"
  property color portageColor: "#ADACF4"
  property color punchColor: "#DA403E"
  property color cinnabarColor: "#ED4745"
  property color carnationColor: "#FB5351"
  property color sundownColor: "#FFB3B3"
  property color mistyRoseColor: "#FFE5E6"
  property color amberColor: "#FFB904"
  property color dandelionColor: "#FFD66D"
  property color colonialWhiteColor: "#FDF6D3"
  property color deepskyBlueColor: "#1AA7F9"
  property color malibuColor: "#4CBDFF"
  property color mabelColor: "#D1EEFF"
  property color jadeColor: "#13B47C"
  property color shamrockColor: "#22D396"
  property color mintColor: "#C8F4E4"
  property color orchidColor: "#BD0ADB"
  property color flamingoColor: "#E042FB"
  property color lilacColor: "#F8CCFF"
  property color transparent: "transparent"
  property color slatePurpleColorOpacity10: "#1A5C5AE8"
  property color slatePurpleColorOpacity20: "#335C5AE8"
  property color cinnabarColorOpacity20: "#33ED4745"
  property color whiteColorOpacity88: "#E0FFFFFF"
  property color primaryColorOpacity90: "#E6232526"

  // Separator color
  property color separatorColor: seashellColor


  // Margins
  property QtObject margin: QtObject {
    property int m4: s(4)
    property int m8: s(8)
    property int m10: s(10)
    property int m12: s(12)
    property int m16: s(16)
    property int m20: s(20)
    property int m24: s(24)
    property int m28: s(28)
    property int m32: s(32)
    property int m36: s(36)
    property int m40: s(40)
    property int m44: s(44)
    property int m48: s(48)
  }


  // Font & text
  property QtObject font: QtObject {
    id: font
    property string primaryFontFamily: Qt.platform.os === "windows"
                                       || Qt.platform.os === "android"
                                       ? "Roboto" : "SF Pro Text"
    property string secondaryFontFamily: Qt.platform.os === "windows"
                                         || Qt.platform.os === "android"
                                         ? "Roboto" : "SF Pro Display"
    property string codeFont: "Courier Prime Code"
    property int h1Size: Qt.platform.os === "android" || Qt.platform.os === "ios" ? (s(32)) : (s(40))
    property int h2Size: Qt.platform.os === "android" || Qt.platform.os === "ios" ? (s(24)) : (s(32))
    property int h3Size: s(18)
    property int bodySize: s(14)
    property int smallSize: s(12)
    property int xsmallSize: s(11)

    property int h1LineHeight: Qt.platform.os === "android" || Qt.platform.os === "ios" ? (s(40)) : (s(56))
    property int h2LineHeight: Qt.platform.os === "android" || Qt.platform.os === "ios" ? (s(32)) : (s(48))
    property int h3LineHeight: s(32)
    property int bodyLineHeight: s(24)
    property int smallLineHeight: s(16)
    property int xsmallLineHeight: s(12)

    property color defaultColor: blackColor
  }

  property ButtonStyle largeIconButton: ButtonStyle {
    bgColor: whiteColor
    bgColorHover: bgColor
    bgColorPressed: whiteSmokeColor
    bgColorFocus: bgColor
    bgColorDisabled: bgColor

    bdColor: mercuryColor
    bdColorHover: altoColor
    bdColorPressed: altoColor
    bdColorFocus: transparent
    bdColorDisabled: mercuryColor

    textColor: charadeColor
    textColorHover: blackColor
    textColorPressed: blackColor
    textColorFocus: blackColor
    textColorDisabled: ironColor

    iconColor: textColor
    iconColorHover: textColorHover
    iconColorPressed: textColorPressed
    iconColorFocus: textColorFocus
    iconColorDisabled: textColorDisabled
    iconColorChecked: slatePurpleColor

    dropShadowColor: Qt.rgba(92, 90, 232, 0.2)
  }

  property ButtonStyle closeCameraButton: ButtonStyle {
    bgColor: transparent
    bgColorHover: bgColor
    bgColorPressed: bgColor
    bgColorFocus: bgColor
    bgColorDisabled: bgColor

    bdColor: transparent
    bdColorHover: bdColor
    bdColorPressed: bdColor
    bdColorFocus: bdColor
    bdColorDisabled: bdColor

    textColor: whiteColor
    textColorHover: whiteColor
    textColorPressed: whiteColor
    textColorFocus: whiteColor
    textColorDisabled: whiteColor

    iconColor: textColor
    iconColorHover: textColorHover
    iconColorPressed: textColorPressed
    iconColorFocus: textColorFocus
    iconColorDisabled: textColorDisabled

    dropShadowColor: Qt.rgba(92, 90, 232, 0.2)
  }

  // Butons styles definition
  property QtObject button: QtObject {
    property var primary: ButtonStyle {
      bgColor: slatePurpleColor
      bgColorHover: blueVioletColor
      bgColorPressed: governorBayColor
      bgColorFocus: slatePurpleColor
      bgColorDisabled: portageColor

      bdColor: transparent
      bdColorHover: bdColor
      bdColorPressed: bdColor
      bdColorFocus: slatePurpleColorOpacity20
      bdColorDisabled: bdColor

      textColor: whiteColor
      textColorHover: whiteColor
      textColorPressed: whiteColor
      textColorFocus: whiteColor
      textColorDisabled: whiteColor

      iconColor: whiteColor
      iconColorHover: whiteColor
      iconColorPressed: whiteColor
      iconColorFocus: whiteColor
      iconColorDisabled: whiteColor

      dropShadowColor: Qt.rgba(92, 90, 232, 0.2)
    }
    property var secondary: ButtonStyle {
      bgColor: whiteColor
      bgColorHover: bgColor
      bgColorPressed: whiteSmokeColor
      bgColorFocus: bgColor
      bgColorDisabled: bgColor

      bdColor: mercuryColor
      bdColorHover: altoColor
      bdColorPressed: altoColor
      bdColorFocus: slatePurpleColorOpacity20
      bdColorDisabled: mercuryColor

      textColor: charadeColor
      textColorHover: blackColor
      textColorPressed: blackColor
      textColorFocus: blackColor
      textColorDisabled: ironColor

      iconColor: textColor
      iconColorHover: textColorHover
      iconColorPressed: textColorPressed
      iconColorFocus: textColorFocus
      iconColorDisabled: textColorDisabled

      dropShadowColor: Qt.rgba(92, 90, 232, 0.2)
    }
    property var destructive: ButtonStyle {
      bgColor: carnationColor
      bgColorHover: cinnabarColor
      bgColorPressed: punchColor
      bgColorFocus: carnationColor
      bgColorDisabled: sundownColor

      bdColor: transparent
      bdColorHover: bdColor
      bdColorPressed: bdColor
      bdColorFocus: cinnabarColorOpacity20
      bdColorDisabled: bdColor

      textColor: whiteColor
      textColorHover: whiteColor
      textColorPressed: whiteColor
      textColorFocus: whiteColor
      textColorDisabled: whiteColor

      iconColor: textColor
      iconColorHover: textColorHover
      iconColorPressed: textColorPressed
      iconColorFocus: textColorFocus
      iconColorDisabled: textColorDisabled

      dropShadowColor: Qt.rgba(237, 71, 69, 0.2)
    }
    property var textNoIcon: ButtonStyle {
      bgColor: whiteColor
      bgColorHover: whiteColor
      bgColorPressed: whiteColor
      bgColorFocus: whiteColor
      bgColorDisabled: whiteColor

      bdColor: transparent
      bdColorHover: bdColor
      bdColorPressed: bdColor
      bdColorFocus: bdColor
      bdColorDisabled: bdColor

      textColor: slatePurpleColor
      textColorHover: blueVioletColor
      textColorPressed: governorBayColor
      textColorFocus: slatePurpleColor
      textColorDisabled: portageColor

      iconColor: textColor
      iconColorHover: textColorHover
      iconColorPressed: textColorPressed
      iconColorFocus: textColorFocus
      iconColorDisabled: textColorDisabled

      dropShadowColor: Qt.rgba(92, 90, 232, 0.2)

      leftPadding: 0
      rightPadding: 0
      topPadding: 0
      bottomPadding: 0
    }
    property var text: ButtonStyle {
      bgColor: whiteColor
      bgColorHover: whiteColor
      bgColorPressed: whiteColor
      bgColorFocus: whiteColor
      bgColorDisabled: whiteColor

      bdColor: transparent
      bdColorHover: bdColor
      bdColorPressed: bdColor
      bdColorFocus: transparent
      bdColorDisabled: bdColor

      textColor: blackColor
      textColorHover: blueVioletColor
      textColorPressed: governorBayColor
      textColorFocus: slatePurpleColor
      textColorDisabled: portageColor

      iconColor: slatePurpleColor
      iconColorHover: textColorHover
      iconColorPressed: textColorPressed
      iconColorFocus: textColorFocus
      iconColorDisabled: textColorDisabled

      dropShadowColor: Qt.rgba(92, 90, 232, 0.2)

      leftPadding: 0
      rightPadding: 0
      topPadding: 0
      bottomPadding: 0
    }
  }

  property ButtonStyle photoButton: ButtonStyle {
    bgColor: transparent
    bgColorHover: transparent
    bgColorPressed: transparent
    bgColorFocus: transparent
    bgColorDisabled: transparent

    bdColor: transparent
    bdColorHover: bdColor
    bdColorPressed: bdColor
    bdColorFocus: slatePurpleColorOpacity20
    bdColorDisabled: bdColor

    textColor: blackColor
    textColorHover: blueVioletColor
    textColorPressed: governorBayColor
    textColorFocus: slatePurpleColor
    textColorDisabled: portageColor

    iconColor: slatePurpleColor
    iconColorHover: textColorHover
    iconColorPressed: textColorPressed
    iconColorFocus: textColorFocus
    iconColorDisabled: textColorDisabled

    dropShadowColor: Qt.rgba(92, 90, 232, 0.2)

    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0
  }
  // input styles
  property int defaultInputHeight: s(48)

  property QtObject textInput: QtObject {
    property color bgColor: whiteColor
    property color labelTextColor: shuttleColor
    property color asteriskColor: slatePurpleColor
    property color textColor: blackColor
    property color placeholderTextColor: manateeColor
    property color errorBorderColor: carnationColor
    property color focusedBorderColor: slatePurpleColor
    property color defaultBorderColor: ironColor
    property color hoveredBorderColor: manateeColor
    property color activeBorderColor: slatePurpleColor
    property int fontSize: font.bodySize
    property color tipTextColor: charadeColor
    property color disabledTextColor: manateeColor
  }

  // infobox styles
  property QtObject infobox: QtObject {
    property color bgColor: mistyRoseColor
    property color textColor: carnationColor
  }

  property int pageHorizontalMargin: 0
  property int pageHorizontalGutter: 0
  property int pageColumns: 1
  property int toolbarHeight: 0
  property int backButtonDisplayMode: 0
  property int pageTopMargin: 0
  property int pageHeaderHeight: 0
  property int pageFooterHeight: 0
  property int homePageVerticalMargin: 0

  property QtObject checkBox: QtObject {
    property int height: s(24)
    property int width: s(24)
    property color uncheckedBdColor: mercuryColor
    property color errorBdColor: carnationColor
    property color checkedBgColor: slatePurpleColor
    property color defaultBgColor: whiteColor
    property int textSize: font.bodySize
    property color textColor: blackColor
    property color highlightedTextColor: slatePurpleColor
  }

  property QtObject radioButton: QtObject {
    property color defaultBgColor: whiteColor
    property color defaultBdColor: mercuryColor
    property color checkedBgColor: charadeColor
    property color disabledCheckedBgColor: manateeColor
    property color disabledBdColor: whisperColor
    property color middleColor: whiteColor
    property int middleHeight: s(12)
    property int height: s(32)
  }

  property QtObject strengthIndicator: QtObject {
    property color bgColor: whiteSmokeColor
    property color textColor: blackColor
    property int textSize: font.bodySize
    property color strengthDefaultColor: mercuryColor
    property color strengthHighColor: slatePurpleColor
    property color strengthMediumColor: amberColor
    property color strengthLowColor: carnationColor
  }

  property QtObject deviceDelegate: QtObject {
    property color iconBgColor: whisperColor
  }

  // search input styles
  property QtObject searchInput: QtObject {
    property color bgColor: whiteSmokeColor
    property color textColor: blackColor
    property color placeholderTextColor: manateeColor
    property color searchIconColor: manateeColor
    property color cursorColor: slatePurpleColor
    property int fontSize: font.bodySize
  }

  property QtObject objectAvatar: QtObject {
    property color bgColor: whiteSmokeColor
    property color outOfSyncIconColor: carnationColor
    property color syncingIconColor: slatePurpleColor
    property color shareModeIconColor: shuttleColor
    property color shareModeIconBgColor: whiteColor
  }

  property QtObject objectDelegate: QtObject {
    property color bgColor: whiteColor
    property color bgColorPressed: slatePurpleColorOpacity10

    property color bdColor: whisperColor
    property color bdColorSelected: slatePurpleColor

    property color dropShadowColor05: Qt.rgba(0,0,0,0.05)
    property color dropShadowColor04: Qt.rgba(0,0,0,0.04)

    property color titleTextColor: blackColor
    property color textColor: shuttleColor

    property color titleIconColor: manateeColor

    property ButtonStyle actionButton: ButtonStyle {
      bgColor: transparent
      bgColorHover: bgColor
      bgColorPressed: bgColor
      bgColorFocus: bgColor
      bgColorDisabled: bgColor

      bdColor: transparent
      bdColorHover: bdColor
      bdColorPressed: bdColor
      bdColorFocus: bdColor
      bdColorDisabled: bdColor

      textColor: transparent
      textColorHover: transparent
      textColorPressed: transparent
      textColorFocus: transparent
      textColorDisabled: transparent

      iconColor: charadeColor
      iconColorHover: iconColor
      iconColorPressed: iconColor
      iconColorFocus: iconColor
      iconColorDisabled: iconColor

      dropShadowColor: transparent
    }
  }

  property QtObject tabButton: QtObject {
    property var simple: TabStyle {}
    property var closable: TabStyle {
      textColor: shuttleColor
      textColorSelected: charadeColor
      textColorFocus: charadeColor
      textColorHover: charadeColor

      bgColor: whiteSmokeColor
      bgColorSelected: whiteColor
      bgColorFocus: whiteColor
      bgColorHover: mercuryColor
    }
  }

    property QtObject cardDelegate: QtObject {
    property color bgColor: whiteColor
    property color bgColorPressed: whiteColor

    property color bdColor: whisperColor
    property color bdColorSelected: slatePurpleColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.05)

    property color titleTextColor: blackColor
    property color textColor: shuttleColor

    property color titleIconColor: manateeColor

    property ButtonStyle actionButtonStyle: ButtonStyle {
      textColor: slatePurpleColor
      textColorHover: blueVioletColor
      textColorPressed: governorBayColor
      textColorFocus: slatePurpleColor
      textColorDisabled: portageColor

      dropShadowColor: Qt.rgba(92, 90, 232, 0.2)

      leftPadding: 0
      rightPadding: 0
      topPadding: 0
      bottomPadding: 0
    }
  }
  property QtObject avatar: QtObject {
    property color borderColor: whiteColor
    property color textColor: whiteColor
  }

  property QtObject actionListDelegate: QtObject {
    property color bgColor: whiteColor
    property color bgColorPressed: slatePurpleColorOpacity10
    property color bgColorHover: whiteSmokeColor

    property color textColor: charadeColor
    property color textColorHover: blackColor

    property color iconColor: charadeColor
    property color iconColorHover: blackColor
    property color separatorColor: seashellColor

    property color checkBgColor: slatePurpleColor
  }

  property QtObject arcaMobileDelegate: QtObject {
    property color bgColor: whiteColor
    property color bgColorPressed: slatePurpleColorOpacity10

    property color bdColor: whisperColor
    property color bdColorSelected: slatePurpleColor

    property color dropShadowSelectedColor: Qt.rgba(0,0,0,0.05)
    property color dropShadowColor: Qt.rgba(0,0,0,0.04)

    property color titleTextColor: blackColor
    property color textColor: shuttleColor

    property ButtonStyle actionButton: objectDelegate.actionButton
  }

  property QtObject avatarDelegate: QtObject {
    property color textColor: shuttleColor
  }

  property QtObject contactCard: QtObject {

    property color bgColor: whiteColor

    property color bdColor: whisperColor
    property color bdColorSelected: slatePurpleColor

    property color dropShadowColor: Qt.rgba(0,0,0,0.05)

    property color titleTextColor: blackColor
    property color textColor: shuttleColor

    property color titleIconColor: manateeColor
    property color labelColor: mercuryColor

  }

  property MenuItemStyle menuItem: MenuItemStyle { }

  property QtObject sharePage: QtObject {
    property color sectionBgColor: whiteSmokeColor
    property ButtonStyle infoIconStyle: ButtonStyle {
      iconColor: slatePurpleColor
      iconColorHover: slatePurpleColor
      iconColorPressed: slatePurpleColor
      iconColorFocus: slatePurpleColor
      iconColorDisabled: slatePurpleColor
    }
  }

  property ButtonStyle textLabelDelegate: ButtonStyle {
    bgColor: whiteColor
    bgColorPressed: slatePurpleColor

    bdColor: ironColor
    bdColorPressed: transparent
    bdColorHover: manateeColor

    textColor: charadeColor
    textColorPressed: whiteColor
  }

  property ButtonStyle closeableTextLabelDelegate: ButtonStyle {
    bgColor: whiteColor
    bdColor: ironColor
    textColor: blackColor
  }

  property ButtonStyle checkableTextLabelDelegate: ButtonStyle {
    bgColor: whiteColor
    bgColorPressed: slatePurpleColor

    bdColor: ironColor
    bdColorPressed: transparent
    bdColorHover: slatePurpleColor

    textColor: charadeColor
    textColorPressed: whiteColor
  }

  property QtObject appliedSearchFiltersPanel: QtObject {
    property color bgColor: whiteSmokeColor
    property color bdColor: mercuryColor
    property Gradient gradient: Gradient {
      GradientStop {position: 0.07; color: "#05f6f7f9"}
      GradientStop {position: 0.92; color: whiteSmokeColor}
    }
  }

  property ButtonStyle desktopSearchFilterDelegate: ButtonStyle {
    bgColor: whiteColor
    bgColorPressed: whiteColor

    bdColor: ironColor
    bdColorPressed: manateeColor

    textColor: charadeColor
    textColorPressed: blackColor

    dropShadowColor: Qt.rgba(0,0,0,0.04)
  }

  property QtObject imageListDelegate : QtObject {
    property color bgColor: whiteColor
    property color bdColor: whisperColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.04)
  }

  property QtObject buttonIndicator: QtObject {
    property color bgColor: carnationColor
    property color bdColor: whiteColor
  }

  property QtObject devicesPendingDelegate: QtObject {

    property Gradient gradient: Gradient {
      GradientStop { position: -0.27; color: slatePurpleColor}
      GradientStop { position: 1.74; color: governorBayColor}
    }
    property color fontColor: whiteColor
    property color deviceCircleColor: blueVioletColor
    property color deviceCircleBdColor: slatePurpleColor
    property color bgColorHovered: blueVioletColor

    property ButtonStyle actionButton: objectDelegate.actionButton
  }

  property QtObject htmlLinkEditor: QtObject {
    property color bgColor: charadeColor
    property color textColor: whiteColor
    property color placeholderColor: altoColor
  }

  property QtObject notificationDelegate : QtObject {
    property color bgColor: charadeColor
    property color iconBgColor: blackColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.12)
    property color fontColor: whiteColor
  }

  property QtObject notificationDelegateGreen : QtObject {
    property color bgColor: shamrockColor
    property color iconBgColor: jadeColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.12)
    property color fontColor: whiteColor
  }

  property QtObject blockedUserPanel : QtObject {
    property color bgColor: whiteColor
    property color bdColor: whisperColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.04)
    property color contentTextColor: shuttleColor
  }

  property QtObject mediaPlayerDelegate : QtObject {
    property color bgColor: whiteColor
    property color bdColor: whisperColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.04)
    property color fontColor: shuttleColor
  }

  property QtObject subscriptionPanel : QtObject {
    property color bgColor: slatePurpleColor
    property color fontColor: whiteColor
  }

  property QtObject deviceInformationDelegate : QtObject {
    property color bgColor: whiteColor
    property color bdColor: whisperColor
    property color bdCheckedColor: slatePurpleColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.04)
    property color contentTextColor: shuttleColor
  }

  property QtObject calendar : QtObject {
    property color dayOfWeekColor: shuttleColor
    property color textColorCurrentDate: slatePurpleColor
    property color textColor: blackColor
    property color textColorChecked: whiteColor
    property color textColorDisabled: manateeColor
    property color bgColor: transparent
    property color bgColorChecked: slatePurpleColor
    property color bdColor: transparent
    property color bdColorHover: mercuryColor

    property ButtonStyle navigationButton: ButtonStyle {
      bgColor: whiteSmokeColor
      bgColorHover: bgColor
      bgColorPressed: bgColor
      bgColorFocus: bgColor
      bgColorDisabled: bgColor

      textColor: charadeColor
      textColorHover: blackColor
      textColorPressed: blackColor
      textColorFocus: blackColor
      textColorDisabled: ironColor

      iconColor: textColor
      iconColorHover: textColorHover
      iconColorPressed: textColorPressed
      iconColorFocus: textColorFocus
      iconColorDisabled: textColorDisabled

      dropShadowColor: Qt.rgba(92, 90, 232, 0.0)
    }
  }

  property QtObject styledSlider : QtObject {
    property color bgColor: mercuryColor
    property color bgSelectedColor: slatePurpleColor
    property color handleBgColor: whiteColor
    property color handleBdColor: ironColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.08)
  }

  property QtObject passwordLengthSlider : QtObject {
    property color bgColor: slatePurpleColor
    property color contentFontColor: shuttleColor
  }

  property ButtonStyle clearDateInputButtonStyle: ButtonStyle {
    textColor: blackColor
    textColorHover: blackColor
    textColorFocus: blackColor
    textColorPressed: blackColor
    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0
  }

  property QtObject noteMenu: QtObject {
    property color bgColor: whiteColor
    property color bdColor: mercuryColor
  }

  property QtObject notificationCard : QtObject {
    property color bgColor: transparent
    property color unreadBgColor: slatePurpleColorOpacity10
    property color hoveredBgColor: whiteSmokeColor
    property color dateFontColor: shuttleColor
  }

  property QtObject deviceAvatar: QtObject {
    property color tabletBgColor: slatePurpleColor
    property color desktopBgColor: shamrockColor
    property color mobileBgColor: malibuColor
    property color virtualBgColor: whiteSmokeColor
    property color outOfSyncBgColor: mistyRoseColor
    property color syncingBgColor: slatePurpleColorOpacity10
    property color syncingIconColor: slatePurpleColor
    property color outOfSyncIconColor: carnationColor
  }

  property QtObject chartDelegate : QtObject {
    property color bgColor: whiteColor
    property color bgColorPressed: slatePurpleColorOpacity10
    property color bdColor: whisperColor
    property color dropShadowColor: Qt.rgba(0, 0, 0, 0.04)
    property color dropShadowHoveredColor: Qt.rgba(0, 0, 0, 0.05)
  }

  property QtObject weakPasswordsDelegate: QtObject {

    property Gradient gradient: Gradient {
      GradientStop { position: -0.27; color: slatePurpleColor}
      GradientStop { position: 1.74; color: governorBayColor}
    }
    property color fontColor: whiteColor
    property color avatarColor: blueVioletColor
    property color avatarBdColor: governorBayColor
    property color bgColorHovered: blueVioletColor
    property color bgColorPressed: governorBayColor

    property ButtonStyle actionButton: objectDelegate.actionButton
  }

  property ButtonStyle overwriteCancelButtonStyle: ButtonStyle {
    textColor: charadeColor
    textColorHover: charadeColor
    textColorFocus: charadeColor
    textColorPressed: charadeColor
    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0
  }

  property QtObject toggleMenuOption: QtObject {
    property color bgColor: whiteColor
    property color bgColorPressed: slatePurpleColorOpacity10
    property color separatorColor: seashellColor
  }

  property QtObject codeInput: QtObject {
    property color bgColor: whiteColor
    property color bdColor: ironColor
    property color bdColorActive: slatePurpleColor
    property color cursorColor: slatePurpleColor
  }


  property ButtonStyle fieldButton: ButtonStyle {
    iconColor: manateeColor
    iconColorHover: blackColor
    iconColorPressed: blackColor
    iconColorFocus: shuttleColor
    iconColorDisabled: altoColor
  }

  property ButtonStyle fieldButtonAlt: ButtonStyle {
    iconColor: slatePurpleColor
    iconColorHover: slatePurpleColor // TODO adjust those colors
    iconColorPressed: slatePurpleColor
    iconColorFocus: slatePurpleColor
    iconColorDisabled: slatePurpleColor
  }

  property QtObject sortListDelegate: QtObject {
    property color bgColor: whiteColor
    property color bgColorPressed: slatePurpleColorOpacity10
    property color bgColorHover: whiteSmokeColor

    property color textColor: charadeColor
    property color textColorHover: blackColor
    property color separatorColor: seashellColor
  }

  property QtObject oneUserDelegate: QtObject {
    property color titleTextColor: blackColor
    property color textColor: shuttleColor
    property color labelColor: mercuryColor
  }

  property QtObject userRequestDelegate : QtObject {
    property color bgColor: whiteColor
    property color bdColor: whisperColor
    property color bdCheckedColor: slatePurpleColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.04)
    property color contentTextColor: shuttleColor
  }  

  property QtObject mediaSlider : QtObject {
    property color bgColor: whisperColor
    property color bgSelectedColor: slatePurpleColor
    property color handleBgColor: whiteColor
    property color handleBdColor: ironColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.04)
  }

  property QtObject audioPlayer: QtObject {
    property color bgColor: whiteColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.04)
    property color textColor: shuttleColor
    property color bdColor: whisperColor
  }

  property QtObject volumeControl: QtObject {
    property color bgColor: whiteColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.08)
    property color bdColor: whisperColor
  }

  property ButtonStyle clearTextButtonStyle: ButtonStyle {
    textColor: charadeColor
    textColorHover: charadeColor
    textColorFocus: charadeColor
    textColorPressed: charadeColor
    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0
  }

  property QtObject lightTooltip: QtObject {
    property color bgColor: whiteColor
    property color textColor: blackColor
    property color confirmButtonColor: charadeColor
  }

  property QtObject darkTooltip: QtObject {
    property color bgColor: charadeColor
    property color textColor: whiteColor
    property color confirmButtonColor: whiteColor
  }

  property QtObject fileSearchInput: QtObject {
    property color bgColor: whiteColor
    property color bdColor: whisperColor
    property color textColor: blackColor
    property color iconColor: blackColor
    property color placeholderTextColor: blackColor
    property color cursorColor: blackColor
    property int fontSize: font.bodySize
    property int height: s(32)
    property color dropShadowColor: Qt.rgba(0,0,0,0.04)
  }

  property QtObject pageSwitch : QtObject {
    property color bgColor: whiteColor
    property color bdColor: whisperColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.04)
  }

  property QtObject textFrame : QtObject {
    property color bgColor: whiteColor
    property color bdColor: whisperColor
    property color dropShadowColor: Qt.rgba(0,0,0,0.04)
  }

  property QtObject circularProgressBar : QtObject {
    property color primaryColor: slatePurpleColor
    property color secondaryColor: seashellColor
  }

  // Video player dark
  property QtObject videoPlayerDark: QtObject {
    property color background: "#030303"
    property color toolBarColor: "#1c1c1c"
    property color sliderBgColor: "#3d3d3d"
    property color buttonsColor: "#d2d2d2"
    property color textColor: "#8e8e8e"
    property color toolbarBorderColor: "transparent"

    property ButtonStyle buttonStyle: ButtonStyle {
      iconColor: videoPlayerDark.buttonsColor
      iconColorHover: iconColor
      iconColorPressed: iconColor
      iconColorFocus: iconColor
      iconColorDisabled: iconColor
      iconColorChecked: iconColor

      bgColor: videoPlayerDark.toolBarColor
      bgColorHover: bgColor
      bgColorPressed: bgColor
      bgColorFocus: bgColor
      bgColorDisabled: bgColor
    }

    property QtObject slider : QtObject {
      property color bgColor: videoPlayerDark.sliderBgColor
      property color bgSelectedColor: videoPlayerDark.buttonsColor
      property color handleBgColor: whiteColor
      property color handleBdColor: whiteColor
      property color dropShadowColor: Qt.rgba(0,0,0,0.04)
    }

    property QtObject volumeControl: QtObject {
      property color bgColor: videoPlayerDark.toolBarColor
      property color dropShadowColor: Qt.rgba(0,0,0,0.08)
      property color bdColor: transparent
    }
  }

  // Audio player
  property QtObject audioPlayerLight: QtObject {
    property color background: whiteColor
    property color toolBarColor: whiteColor
    property color sliderBgColor: whisperColor
    property color buttonsColor: charadeColor
    property color textColor: shuttleColor
    property color toolbarBorderColor: whisperColor

    property ButtonStyle buttonStyle: ButtonStyle {
      iconColor: audioPlayerLight.buttonsColor
      iconColorHover: iconColor
      iconColorPressed: iconColor
      iconColorFocus: iconColor
      iconColorDisabled: iconColor
      iconColorChecked: iconColor

      bgColor: audioPlayerLight.toolBarColor
      bgColorHover: bgColor
      bgColorPressed: bgColor
      bgColorFocus: bgColor
      bgColorDisabled: bgColor
    }

    property QtObject slider : QtObject {
      property color bgColor: audioPlayerLight.sliderBgColor
      property color bgSelectedColor: audioPlayerLight.buttonsColor
      property color handleBgColor: whiteColor
      property color handleBdColor: whiteColor
      property color dropShadowColor: Qt.rgba(0,0,0,0.04)
    }

    property QtObject volumeControl: QtObject {
      property color bgColor: audioPlayerLight.toolBarColor
      property color dropShadowColor: Qt.rgba(0,0,0,0.08)
      property color bdColor: transparent
    }
  }

  //TODO other compoments properties will be added during development of compoments

  function s(value) {
    return value * Utility.scaleFactor
  }
}
