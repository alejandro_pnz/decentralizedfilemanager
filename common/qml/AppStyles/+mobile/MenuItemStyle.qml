import QtQuick 2.15


QtObject {
    property color textColor: manateeColor
    property color textColorChecked: blackColor
    
    property color bgColor: whiteColor
    property color bgColorPressed: whiteSmokeColor
    
    property color iconColor: manateeColor
    property color iconColorChecked: blackColor
    property color iconColorPressed: blackColor
}
