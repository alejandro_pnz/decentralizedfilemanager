import QtQuick 2.15
import UIKit 1.0 as UIKit
import QtQuick.Window 2.12
import "../"
// This file should contain device-specific styles, sizes etc.
AbstractBaseStyle {
  pageHorizontalMargin: ThemeController.style.margin.m16
  pageHorizontalGutter: s(8)
  pageColumns: Screen.orientation === Qt.PortraitOrientation ? 2 : 4
  toolbarHeight: s(44)
  backButtonDisplayMode: UIKit.StyledButton.ButtonStyle.Text
  pageHeaderHeight: s(68)
  homePageVerticalMargin: ThemeController.style.margin.m16
  pageFooterHeight: s(64)
}
