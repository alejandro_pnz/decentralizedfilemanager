import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2
import QtQml.Models 2.12

import com.testintl 1.0

ColumnLayout {
  id: content
  spacing: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m8
  signal ownerChanged(string ownerName)
  property int selectedIndex: 0
  property alias checkedButton: buttonsGroup.checkedButton
  property int componentsHeight: search.height + column.height + spacing

  property var objectOwnerId: homeManager.currentEditor.object.ownerId

  UIKit.SearchInput {
    id: search
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : 0
    Layout.fillWidth: true
    placeholderText: Strings.searchUsers
    Layout.preferredHeight: ThemeController.style.defaultInputHeight
    micIconVisible: false
  }

  UIKit.BaseText {
    text: Strings.owner
    font.weight: Font.Bold
    visible: app.isPhone
  }

  Flickable {
    Layout.fillHeight: true
    Layout.fillWidth: true
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: column.height
    clip: true

    ColumnLayout {
      id: column
      width: parent.width
      spacing: app.isPhone ? ThemeController.style.margin.m16 : 0

      ListView {
        id: selectedItem
        Layout.fillWidth: true
        Layout.preferredHeight: contentHeight
        clip: true
        interactive: false

        model: selectedProxy
        delegate: UIKit.ContactCard {
          id: contactCard
          title: model.email
          role: model.guest ? Strings.guest : ""
          checkBoxMode: false
          checked: model.userId > 0 && model.userId === objectOwnerId
          btnId: model.userId
          ButtonGroup.group: buttonsGroup
          leftPadding: app.isPhone ? ThemeController.style.margin.m16 : 0
          rightPadding: app.isPhone ? ThemeController.style.margin.m16 : 0
        }
      }

      Rectangle {
        Layout.fillWidth: true
        Layout.preferredHeight: s(1)
        color: ThemeController.style.mercuryColor
      }

      ListView {
        id: listView
        Layout.fillWidth: true
        Layout.preferredHeight: contentHeight
        spacing: app.isPhone ? ThemeController.style.margin.m12 : 0
        clip: true
        interactive: false

        model: defaultProxy
        delegate: UIKit.ContactCard {
          title: model.email
          role: model.guest ? Strings.guest : ""
          checkBoxMode: false
          checked: model.userId > 0 && model.userId === objectOwnerId
          btnId: model.userId
          ButtonGroup.group: buttonsGroup
          leftPadding: app.isPhone ? ThemeController.style.margin.m16 : 0
          rightPadding: app.isPhone ? ThemeController.style.margin.m16 : 0
        }
      }
    }
  }

  ButtonGroup {
    id: buttonsGroup
    exclusive: true
    onClicked: {
      contactsModel.setProperty(selectedIndex, "isChecked", false)
      selectedIndex = checkedButton.btnId
      contactsModel.setProperty(checkedButton.btnId, "isChecked", true)

      if(!app.isPhone){
        ownerChanged(checkedButton.title)
      }
    }
  }

  SortFilterProxyModel {
    id: selectedProxy
    sourceModel: UserListModel
    filters: [
      ValueFilter {
        roleName: "userId"
        value: objectOwnerId
      },
      ExpressionFilter {
        expression: {
          return objectOwnerId > 0
        }
      }
    ]
  }

  SortFilterProxyModel {
    id: defaultProxy
    sourceModel: UserListModel
    filters: [
      ValueFilter {
        roleName: "userId"
        value: objectOwnerId
        inverted: true
      }
    ]
  }
}
