import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

ColumnLayout {
  spacing: ThemeController.style.margin.m12
  width: parent.width
  signal closeClicked()
  signal accept()

  Item {
    Layout.fillWidth: true
    Layout.preferredHeight: s(32)

    UIKit.BaseText {
      text: Strings.givePowerToDevice
      font.weight: Font.Bold
      size: UIKit.BaseText.TextSize.H3
      anchors.centerIn: parent
    }

    UIKit.LargeIconButton {
      icon.source: "qrc:/close.svg"
      height: s(32)
      width: s(32)
      anchors.right: parent.right
      visible: !app.isPhone
      onClicked: closeClicked()
    }
  }

  UIKit.BaseText {
    Layout.topMargin: app.isPhone ? 0 : ThemeController.style.margin.m4
    text: Strings.deviceWithPower
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.SwitchItem {
    Layout.topMargin: ThemeController.style.margin.m12
    Layout.fillWidth: true
    text: Strings.dontShowThisAgain
  }

  UIKit.StyledButton {
    id: button
    text: Strings.yesGivePower
    Layout.topMargin: ThemeController.style.margin.m20
    Layout.fillWidth: true
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Primary
    onClicked: accept()
  }

  UIKit.StyledButton {
    id: cancelButton
    text: Strings.doNotGivePower
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Secondary
    Layout.fillWidth: true
    onClicked: closeClicked()
  }

  Item {
    Layout.preferredHeight: s(4)
    Layout.fillWidth: true
  }
}
