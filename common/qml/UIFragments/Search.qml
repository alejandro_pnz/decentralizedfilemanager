import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import com.testintl 1.0
import QtQml.Models 2.15

ColumnLayout {
  id: content
  anchors.fill: parent
  spacing: ThemeController.style.margin.m16
  property bool isMobile: app.isPhone
  property alias sModel: searchModel
  property bool searching: search.preeditText.length > 0 || search.text.length > 0

  signal showDateFilter(int xPos, int yPos)
  signal showTagsFilters(int xPos, int yPos)

  AppliedSearchFilters {
    Layout.fillWidth: true
    Layout.leftMargin: -ThemeController.style.margin.m16
    Layout.rightMargin: -ThemeController.style.margin.m16
    visible: isMobile && filtersApplied > 0
    sModel: searchModel
  }

  UIKit.SearchInput {
    id: search
    Layout.topMargin: ThemeController.style.margin.m24
    Layout.fillWidth: true
    placeholderText: Strings.searchQuodArcaTxt
    Layout.preferredHeight: ThemeController.style.defaultInputHeight
    visible: !isMobile
    micIconVisible: false
    onTextChanged: searchModel.search(text)
    onPreeditTextChanged: searchModel.search(text)
  }

  DesktopFiltersPanel {
    id: filtersPanel
    Layout.fillWidth: true
    visible: !isMobile
    sModel: searchModel

    onShowMore: {
      homeManager.enterMoreFiltersPage({"searchModel":searchModel})
    }
  }

  SearchModel {
    id: searchModel
    filters.librarySearch: true
  }

  Flickable {
    id: flickable
    clip: true
    Layout.fillHeight: true
    Layout.fillWidth: true
    contentHeight: column.height

    ColumnLayout {
      id: column
      anchors.left: parent.left
      anchors.right: parent.right
      spacing: ThemeController.style.margin.m16

      UIKit.BaseText {
        Layout.topMargin: ThemeController.style.homePageVerticalMargin
        text: Strings.recentlyOpened
        font.weight: Font.DemiBold
        font.capitalization: Font.AllUppercase
        color: ThemeController.style.charadeColor
        size: UIKit.BaseText.TextSize.Small
      }      

      ListView {
        id: recentlyOpenedList
        Layout.fillWidth: true
        Layout.preferredHeight: contentHeight
        spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
        clip: true
        interactive: false

        model: searchModel
        delegate: UIKit.ObjectDelegate {
          // Model data
          title: model.name
          text: model.content
          avatar.shareMode: model.syncProperty
          titleIcon: locked === undefined ? false : locked
          avatar.iconSrc: "image://item/type=" + model.objectType + "&id=" + model.id

          width: ListView.view.width
          checkable: false
          actionButton: true
        }
      }

      UIKit.BaseText {
        Layout.topMargin: isMobile ? ThemeController.style.margin.m8 :
                                     ThemeController.style.margin.m24
        text: Strings.recentSearches
        font.weight: Font.DemiBold
        font.capitalization: Font.AllUppercase
        color: ThemeController.style.charadeColor
        size: UIKit.BaseText.TextSize.Small
      }

      ListView {
        id: recentSearchesView
        Layout.fillWidth: true
        Layout.preferredHeight: recentSearchesView.contentHeight
        spacing: ThemeController.style.margin.m12
        interactive: false

        model: searchModel
        delegate: UIKit.ObjectDelegate {
          title: name
          width: ListView.view.width
          avatar.iconSrc: "qrc:/search-gray.svg"
          actionButton: true
          actionButtonIconSource: "qrc:/close.svg"
          avatar.iconSize: s(24)
        }
      }

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
      }
    }
  }

  UIKit.GenericListView {
    id: listView
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.topMargin: ThemeController.style.margin.m4
    clip: true
    spacing: ThemeController.style.margin.m12
    selectionMode: ism.hasSelection

    ItemSelectionModel {
      id: ism
      model: searchModel
    }

    model: searchModel
    delegate: UIKit.ObjectDelegate {
      property var locked: model.locked
      // Model data
      title: model.name
      text: model.content
      avatar.shareMode: model.syncProperty
      titleIcon: locked === undefined ? false : locked
      avatar.iconSrc: "image://item/type=" + model.objectType + "&id=" + model.id

      // Other properties
      width: listView.width
      actionButton: true
      checkboxOnTheLeft: !app.isMobile
      checkable: !app.isMobile
      delegateModel: listView.model
      selectionModel: ism
      selectionMode: listView.selectionMode

      onDelegateClicked: {
        // Open object
        switch(model.objectType) {
        case SyncableObject.Arca:
          homeManager.openOneArcaPage(model.id)
          break;
        case SyncableObject.Quod:
          homeManager.openOneQuodPage(model.id)
          break;
        case SyncableObject.File:
          homeManager.openOneFilePage(model.id)
          break;
        default:
          console.warn("Unsupported object type", model.objectType)
          break;
        }
      }

      onActionButtonClicked: {
        listView.currentIndex = index
        actionMenu.model.currentObject = object
        actionMenu.handleActionButton(listView)
      }

      Binding on actionButton {
        when: !app.isPhone
        value: !ism.hasSelection
      }

      onTextLinkActivated: {
        homeManager.openOneArcaPage(link)
      }
    }
  }

  states: [
    State {
      name: "default"
      when: !searching
      PropertyChanges {
        target: listView
        visible: false
      }
      PropertyChanges {
        target: flickable
        visible: true
      }
    },
    State {
      name: "search"
      when: searching
      PropertyChanges {
        target: listView
        visible: true
      }
      PropertyChanges {
        target: flickable
        visible: false
      }
    }
  ]
}
