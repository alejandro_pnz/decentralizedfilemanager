import QtQuick 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  spacing: 0
  width: parent.width

  UIKit.ActionListDelegate {
    leftPadding: app.isPhone ? 0 : ThemeController.style.margin.m16
    rightPadding: app.isPhone ? 0 : ThemeController.style.margin.m16
    Layout.fillWidth: true
    text: Strings.csv
    icon.source: "qrc:/csv.svg"
    display: AbstractButton.TextBesideIcon
    separator: app.isPhone
  }

  UIKit.ActionListDelegate {
    leftPadding: app.isPhone ? 0 : ThemeController.style.margin.m16
    rightPadding: app.isPhone ? 0 : ThemeController.style.margin.m16
    Layout.fillWidth: true
    text: Strings.archiveFormat
    icon.source: "qrc:/archive.svg"
    display: AbstractButton.TextBesideIcon
    separator: app.isPhone
  }
}
