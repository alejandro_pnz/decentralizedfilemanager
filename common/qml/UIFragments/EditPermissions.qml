import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0

ColumnLayout {
  id: content
  spacing: 0

  function save() {
    for (var i = 0; i < group.buttons.length; ++i) {
      homeManager.currentEditor.setPermission(group.buttons[i].enumVal, !group.buttons[i].checked)
    }
  }

  ListModel {
    id: permisssionModel
  }

  Component.onCompleted: {
    permisssionModel.append({name: Strings.yesDeletePermission, description: Strings.yesDeleteDescription, value: SyncableObject.NoDelete});
    permisssionModel.append({name: Strings.yesEditPermission, description: Strings.yesEditDescription, value: SyncableObject.NoEdit});
    permisssionModel.append({name: Strings.yesExportPermission, description: Strings.yesExportDescription, value: SyncableObject.NoExport});
    permisssionModel.append({name: Strings.yesSendPermission, description: Strings.yesSendDescription, value: SyncableObject.NoSend});
    permisssionModel.append({name: Strings.yesSharePermission, description: Strings.yesShareDescription, value: SyncableObject.NoShare});
  }

  ButtonGroup {
    id: group
    exclusive: false
  }

  Repeater {
    model: permisssionModel
    UIKit.ToggleMenuOption {
      property int enumVal: value
      text: name
      descriptionText: description
      checked: !(homeManager.currentEditor.permissions & value)
      showSeparator: index < (permisssionModel.count - 1)
      ButtonGroup.group: group
      onCheckedChanged: {
        // Break the binding. Otherwise saving will fail
        checked = checked
      }
    }
  }

  Item {
    Layout.fillHeight: app.isMobile && !app.isTablet
    Layout.preferredHeight: app.isMobile && !app.isTablet ? -1 : s(32)
    Layout.fillWidth: true
  }
}
