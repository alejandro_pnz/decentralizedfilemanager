import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

Flickable {
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true

  ColumnLayout {
    id: content
    spacing: ThemeController.style.margin.m16
    width: parent.width

    UIKit.BaseText {
      text: Strings.unlockAuthorization
      font.weight: Font.Bold
      size: app.isPhone && !app.isTablet ? UIKit.BaseText.TextSize.H2
                                          : UIKit.BaseText.TextSize.H3
      Layout.alignment: Qt.AlignHCenter
    }

    UIKit.BaseText {
      Layout.topMargin: app.isPhone && !app.isTablet ? 0 : -ThemeController.style.margin.m4
      text: Strings.needYouToAuthorize
      Layout.alignment: Qt.AlignHCenter
      horizontalAlignment: Qt.AlignHCenter
      Layout.fillWidth: true
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }

    UIKit.BaseText {
      Layout.topMargin: ThemeController.style.margin.m8
      text: Strings.wishToAuthorize
      font.weight: Font.Bold
    }

    UIKit.CardDelegate {
      id: selectedObjects
      Layout.fillWidth: true
      title: Strings.selectedObjects
      text: Strings.selectedObjectsUnlocked
      actionButton: false
      textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      textComponent.elide: Text.ElideNone
      topPadding: s(18)
      bottomPadding: s(18)
      checkable: true
      ButtonGroup.group: cardsGroup
      checked: true
      avatar.visible: false
    }

    UIKit.CardDelegate {
      id: anyLockedObject
      Layout.topMargin: -ThemeController.style.margin.m4
      Layout.fillWidth: true
      title: Strings.anyOtherLockedObject
      text: Strings.lockedObjectCanBeUnlocked
      actionButton: false
      textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      textComponent.elide: Text.ElideNone
      topPadding: s(18)
      bottomPadding: s(18)
      checkable: true
      ButtonGroup.group: cardsGroup
      checked: true
      avatar.visible: false
    }

    ButtonGroup {
      id: cardsGroup
      exclusive: true
    }

    UIKit.BaseText {
      Layout.topMargin: ThemeController.style.margin.m8
      text: Strings.unlockWithoutAuthorization
      font.weight: Font.Bold
      Layout.fillWidth: true
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      visible: anyLockedObject.checked
    }

    Column {
      id: noAuthorizationTime
      spacing: ThemeController.style.margin.m24
      Layout.topMargin: ThemeController.style.margin.m8
      visible: anyLockedObject.checked

      UIKit.StyledRadioButton {
        checked: true
        text: Strings.hour
      }

      UIKit.StyledRadioButton {
        text: Strings.hours.arg("2")
      }

      UIKit.StyledRadioButton {
        text: Strings.hours.arg("5")
      }

      UIKit.StyledRadioButton {
        text: Strings.allDay
      }
    }

    ButtonGroup {
      id: buttonsGroup
      buttons: noAuthorizationTime.children
      exclusive: true
    }

    UIKit.StyledButton {
      id: button
      text: Strings.continueTxt
      Layout.topMargin: anyLockedObject.checked ? s(28) : ThemeController.style.margin.m16
      Layout.fillWidth: true
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      visible: app.isPhone && !app.isTablet
    }

    Item {
      Layout.preferredHeight: app.isPhone && !app.isTablet ? s(36) : s(12)
      Layout.fillWidth: true
    }
  }
}
