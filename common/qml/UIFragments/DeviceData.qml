import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m24

  UIKit.BaseText {
    text: Strings.dataStr
    visible: !app.isPhone
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
  }

  Flickable {
    id: flick
    Layout.fillHeight: true
    Layout.fillWidth: true
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: content.height
    clip: true

    ColumnLayout {
      id: content
      spacing: ThemeController.style.margin.m12
      width: parent.width

      UIKit.BaseText {
        text: Strings.unlockAuthorizationMethod
        font.weight: Font.Bold
      }

      ButtonGroup {
        id: authorizationGroup
        buttons: authoriztionMethod.children
        exclusive: true
      }

      Column {
        id: authoriztionMethod
        spacing: ThemeController.style.margin.m24
        Layout.topMargin: ThemeController.style.margin.m12

        UIKit.StyledRadioButton {
          checked: true
          text: Strings.authenticator
        }

        UIKit.StyledRadioButton {
          text: Strings.email
        }

        UIKit.StyledRadioButton {
          text: Strings.sms
        }
      }

      Rectangle {
        Layout.topMargin: ThemeController.style.margin.m16
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
      }

      UIKit.PickerDelegate {
        Layout.topMargin: app.isPhone ? ThemeController.style.margin.m4 : ThemeController.style.margin.m12
        labelText: Strings.maximumSyncable
        inputRequired: false
        placeholderTxt: "1 GB"
        leftEyeIcon: false
      }

      UIKit.PickerDelegate {
        Layout.topMargin: ThemeController.style.margin.m4
        labelText: Strings.deleteItemsAfter
        inputRequired: false
        placeholderTxt: "30 days"
        leftEyeIcon: false
      }

      Rectangle {
        Layout.topMargin: ThemeController.style.margin.m4
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
      }

      UIKit.BaseText {
        Layout.topMargin: ThemeController.style.margin.m4
        text: Strings.purgeClipboard
        font.weight: Font.Bold
      }

      ButtonGroup {
        id: purgeClipboardGroup
        buttons: purgeClipboard.children
        exclusive: true
      }

      Column {
        id: purgeClipboard
        spacing: ThemeController.style.margin.m24
        Layout.topMargin: ThemeController.style.margin.m12

        UIKit.StyledRadioButton {
          checked: true
          text: Strings.minutes.arg("2")
        }

        UIKit.StyledRadioButton {
          text: Strings.minutes.arg("5")
        }

        UIKit.StyledRadioButton {
          text: Strings.minutes.arg("10")
        }
      }

      Rectangle {
        Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
      }

      UIKit.SwitchItem {
        text: Strings.allFilesRemote
        contentText: Strings.enableThisToSaveSpace
        showSeparator: true
      }

      UIKit.SwitchItem {
        text: Strings.enablePasswordAutofill
        showSeparator: true
        Layout.topMargin: ThemeController.style.margin.m4
      }

      UIKit.SwitchItem {
        text: Strings.allowScreenshots
        showSeparator: true
        Layout.topMargin: ThemeController.style.margin.m4
      }

      UIKit.SwitchItem {
        text: Strings.receiveInAppNotifications
        contentText: Strings.inAppMessagesToKeepUpdated
        showSeparator: true
        Layout.topMargin: ThemeController.style.margin.m4
      }

      UIKit.SwitchItem {
        text: Strings.receivePushNotofications
        contentText: Strings.pushNotificationsToKeepUpdated
        Layout.topMargin: ThemeController.style.margin.m4
      }

      Item {
        Layout.fillWidth: true
        Layout.preferredHeight: ThemeController.style.margin.m4
      }
    }
  }
}
