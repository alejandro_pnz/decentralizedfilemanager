import QtQuick 2.15
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Layouts 1.12
import DocumentEditor 1.0

Item {
  id: root
  anchors.fill: parent
  property bool editMode: true
  readonly property bool isMobile: app.isMobile
  property string noteContent
  property int horizontalMargins: 0
  property bool noteMode: true

  MarkdownViewer {
    id: viewer
    anchors.fill: parent
  }

  function save() {
    if (root.noteMode) {
      viewer.saveNote();
    }
    else {
      viewer.saveMarkdown();
    }
  }

  Connections {
    target: homeManager
    function onSetupNotePage(note, editMode, noteMode) {
      console.log("Setup note page", noteMode, note);
      root.noteMode = noteMode;
    }
  }

/*
  DocumentEditor {
    id: document
    document: noteArea.textDocument
    cursorPosition: noteArea.cursorPosition
    selectionStart: noteArea.selectionStart
    selectionEnd: noteArea.selectionEnd
    defaultFontSize: ThemeController.style.font.bodySize
    defaultFontFamily: ThemeController.style.font.primaryFontFamily
  }

  ScrollView {
    id: scrollView
    anchors{
      fill: parent
      topMargin: ThemeController.style.margin.m16
    }
    TextArea {
      id: noteArea
      color: ThemeController.style.font.defaultColor
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      text: noteContent
      textFormat: Qt.RichText
      selectByMouse: true
      selectedTextColor: ThemeController.style.whiteColor
      persistentSelection: true

      onSelectedTextChanged: {
        if (selectionStart === selectionEnd || isMobile) return

        //Calculate the selection bounding rectangle
        var startRect = positionToRectangle(selectionStart)
        var startPoint = Qt.point(startRect.x, startRect.y)
        var endRect = positionToRectangle(selectionEnd)
        var endPoint = Qt.point(endRect.x + endRect.width, endRect.y + endRect.height)
        var menuMargin = ThemeController.style.margin.m8

        var topPosition = root.mapFromItem(noteArea, startPoint.x, startPoint.y - menuMargin - notesMenu.height)
        var bottomPosition = root.mapFromItem(noteArea, endPoint.x, endPoint.y + menuMargin + notesMenu.height)

        if (bottomPosition.y <= root.height) {
          // Place the menu at the bottom of the selection
          notesMenu.y = bottomPosition.y - notesMenu.height
        } else if (topPosition.y > 0) {
          // Place menu at the top
          notesMenu.y = topPosition.y
        } else {
          // All page selection, place in 3/4 of the selection height
          let selectionHeight = Math.min(root.height, endPoint.y - startPoint.y)
          notesMenu.y = 0.75 * selectionHeight
        }
      }

      font {
        pixelSize: ThemeController.style.font.bodySize
        family: ThemeController.style.font.primaryFontFamily
      }
      UIKit.HTMLLinkPopup {
        id: linkTooltip
        delay: 200
        visible: noteArea.hoveredLink.length > 0
        text: noteArea.hoveredLink
      }

      onFocusChanged: {
        if(!editMode)
          editMode = true
      }

    }
  }

  // TODO position the tooltip correctly. The idea is to implement custom QQuickItem
  // that will keep track of hover events but ignores them (to pass it to textArea)
  UIKit.HTMLLinkEditor {
    id: linkEditor
    visible: false
    onAccepted: {
      document.addAnchor(text)
      visible = false
    }
    onFocusChanged: {
      if (!focus) {
        visible = false
        document.preformatAnchor(true)
      }
    }
  }

  UIKit.ToolTip {
    id: tooltip
    visible: !isMobile && editMode
    tipContent: Strings.selectAnyText
    confirmTxt: Strings.gotIt
  }

  UIKit.NotesMenu {
    id: notesMenu
    y: parent.height * 0.5
    x: -horizontalMargins + s(24)
    implicitWidth: Math.min(root.width + 2 * horizontalMargins - s(48), fullMenuWidth)
    visible: !isMobile && editMode && (noteArea.selectionStart != noteArea.selectionEnd)
    documentEditor: document

    onAddLinkRequest:{
      document.preformatAnchor()
      linkEditor.visible = true
      // TODO position when cursor is not visible or there is not enough space to show the link editor
      var cursorPosition = noteArea.positionToRectangle(noteArea.cursorPosition)
      noteArea.selec
      var point = root.mapFromItem(noteArea, cursorPosition.x, cursorPosition.y + cursorPosition.height)
      linkEditor.x = point.x - linkEditor.width / 2
      linkEditor.y = point.y + s(16)
      linkEditor.focus = true
      linkEditor.text = ""
    }
  }
*/

}
