import QtQuick 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  spacing: ThemeController.style.margin.m16

  Connections {
    target: pageManager
    function onSetupOnboardingPage(config) {
      skipButton.clicked.connect(config.buttonHandler)
    }
  }

  SwipeView {
    id: view
    Layout.fillWidth: true
    Layout.fillHeight: true
    clip: true

    Repeater {
      model: onboardingModel
      Loader {
        active: SwipeView.isCurrentItem
        sourceComponent: ColumnLayout {
          id: swipePage
          spacing: ThemeController.style.margin.m16
          UIKit.SvgImage {
            visible: logo
            source: "qrc:/login-logo.svg"
            width: app.isMobile ? s(55) : s(64)
            height: app.isMobile ? s(64) : s(75)
            Layout.alignment: Qt.AlignHCenter
          }

          UIKit.BaseText {
            Layout.topMargin: ThemeController.style.margin.m8 // 16 + 8 = 24 margin
            Layout.fillWidth: true
            text: title
            font.weight: Font.Bold
            size: UIKit.BaseText.TextSize.H2
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            horizontalAlignment: Qt.AlignHCenter
            Layout.minimumHeight: lineHeight * 2
            Layout.maximumHeight: lineHeight * 3
          }

          UIKit.BaseText {
            Layout.fillWidth: true
            text: contentText
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            horizontalAlignment: Qt.AlignHCenter
            Layout.maximumHeight: lineHeight * 2
          }

          Rectangle {
            Layout.alignment: Layout.Center
            Layout.topMargin: ThemeController.style.margin.m8 // 16 + 8 = 24 margin
            Layout.fillWidth: true
            Layout.fillHeight: true

            Image {
              id: img
              height: app.isMobile ? index === 0 ? 0.6 * parent.height : 0.8 * parent.height : index === 0 ?  0.7 * parent.height : parent.height
              width: app.isMobile ? index === 0 ? 0.6 * parent.width : 0.8 * parent.width : index === 0 ? 0.7 * parent.width : parent.width
              anchors.centerIn: parent
              fillMode: Image.PreserveAspectFit
              source: icon
              mipmap: true
            }
          }
        }
      }
    }
  }

  UIKit.SwipeIndicator{
    Layout.fillWidth: true
    Layout.leftMargin: s(14)
    Layout.rightMargin: s(14)
    Layout.topMargin: app.isMobile ? ThemeController.style.margin.m12 : s(19)
    swipeView: view
    modelSize: 5
  }

  UIKit.StyledButton {
    id: skipButton
    Layout.topMargin: ThemeController.style.margin.m12
    Layout.fillWidth: true
    text: Strings.continueTxt
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
  }

  ListModel {
    id: onboardingModel
  }

  Component.onCompleted: {
    onboardingModel.append({title: Strings.welcomeToQA,
                             contentText: Strings.onboardingContent,
                             icon: "qrc:/illustrations/slide_1.png",
                             logo: true})
    onboardingModel.append({title: Strings.easyPowerFulSecure, contentText: Strings.easyPowerFulSecureContent,
                             icon: "qrc:/illustrations/slide_2.png",
                             logo: false})
    onboardingModel.append({title: Strings.crossplatformCollaboration, contentText: Strings.crossplatformCollaborationContent,
                             icon: "qrc:/illustrations/slide_3.png",
                             logo: false})
    onboardingModel.append({title: Strings.contextualData, contentText: Strings.contextualDataContent,
                             icon: "qrc:/illustrations/slide_4.png",
                             logo: false})
    onboardingModel.append({title: Strings.p2pEncryptedSync, contentText: Strings.p2pEncryptedSyncContent,
                             icon: "qrc:/illustrations/slide_5.png",
                             logo: false})
  }
}
