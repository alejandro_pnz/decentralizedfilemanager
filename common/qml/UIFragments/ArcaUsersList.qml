import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQml.Models 2.12
import SortFilterProxyModel 0.2

ColumnLayout {
  id: content
  width: parent.width
  height: parent.height
  spacing: isMobile ? ThemeController.style.margin.m16 : ThemeController.style.margin.m12
  readonly property bool isMobile: app.isPhone
  property bool whitelistMode: true
  property int horizontalMargins: 0

  TabBar {
    id: bar
    Layout.topMargin: ThemeController.style.margin.m12
    spacing: ThemeController.style.margin.m12
    visible: !isMobile
    UIKit.StyledTabButton {
      text: Strings.whitelist
      onClicked: {
        whitelistMode = true
      }
    }
    UIKit.StyledTabButton {
      text: Strings.blacklist
      onClicked: {
        whitelistMode = false
      }
    }
  }

  Rectangle {
    id: spacer
    visible: !isMobile
    Layout.fillWidth: true
    Layout.leftMargin: -horizontalMargins
    Layout.rightMargin: -horizontalMargins
    color: ThemeController.style.mercuryColor
    height: s(1)
  }

  UIKit.SearchInput {
    id: search
    Layout.topMargin: isMobile ? ThemeController.style.margin.m16 : ThemeController.style.margin.m12
    Layout.fillWidth: true
    placeholderText: Strings.searchUsers
    Layout.preferredHeight: ThemeController.style.defaultInputHeight
  }

  UIKit.BaseText {
    Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m12
    text: whiteListProxy.selectionCount > 0 ? Strings.usersSelected.arg(whiteListProxy.selectionCount) :
                                   Strings.selectUsers
    font.weight: Font.Bold
  }

  ListView {
    id: selectedList
    Layout.fillWidth: true
    Layout.preferredHeight: s(88)
    spacing: ThemeController.style.margin.m16
    orientation: ListView.Horizontal
    visible: count > 0 && isMobile
    model: selectedProxy
    delegate: UIKit.AvatarDelegate {
      email: model.email
      avatarObject.onTopIconClicked: {
        whiteListProxy.setData(whiteListProxy.index(selectedProxy.mapToSource(index),0),
                               false,
                               whiteListProxy.checkStateRoleValue())
      }
    }
  }

  Rectangle {
    Layout.preferredHeight: s(1)
    Layout.fillWidth: true
    color: ThemeController.style.seashellColor
    visible: selectedList.count > 0 && isMobile
  }

  ListView {
    id: listView
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m4
    spacing: ThemeController.style.margin.m12
    clip: true

    model: proxy
    delegate: UIKit.ContactCard {
      title: model.email
      checked: isChecked
      width: listView.width
      onClicked: {
        whiteListProxy.setData(whiteListProxy.index(proxy.mapToSource(index),0),
                               checked,
                               whiteListProxy.checkStateRoleValue())
      }
    }
  }

  ItemSelectionModel {
    id: ism
  }

  WhiteListProxy {
    id: whiteListProxy
    sourceModel: UserListModel
    whiteListMode: content.whitelistMode
  }

  SortFilterProxyModel {
    id: selectedProxy
    sourceModel: whiteListProxy
    filters: [
      ValueFilter {
        enabled: !whiteListProxy.whiteListMode
        roleName: "blacklisted"
        value: true
      },
      ValueFilter {
        enabled: whiteListProxy.whiteListMode
        roleName: "whitelisted"
        value: true
      }
    ]
  }

  SortFilterProxyModel {
    id: proxy
    sourceModel: whiteListProxy
    filters: [
      RegExpFilter {
        roleName: "email"
        pattern: search.searchText
        caseSensitivity: Qt.CaseInsensitive
      },
      ValueFilter {
        enabled: whiteListProxy.whiteListMode
        roleName: "blacklisted"
        value: false
      },
      ValueFilter {
        enabled: !whiteListProxy.whiteListMode
        roleName: "whitelisted"
        value: false
      }
    ]
  }
}
