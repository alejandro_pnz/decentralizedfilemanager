import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import QtQuick.Layouts 1.12

Flickable {
  implicitHeight: parent.height
  implicitWidth: s(208)
  clip: true
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: column.height + s(16)

  property int selectedIndex: buttonsGroup.checkedButton.idx
  Component.onCompleted: console.log("COMPLETED", selectedIndex)

  onSelectedIndexChanged: {
    console.log("Selected index checked", selectedIndex)
    buttonsGroup.buttons[buttonsGroup.buttons.length - 1 - selectedIndex].checked = true
  }

  ColumnLayout {
    id: column
    anchors {
      left: parent.left
      right: parent.right
      rightMargin: ThemeController.style.margin.m16
    }

    height: s(711)
    spacing: ThemeController.style.margin.m4

    UIKit.SettingsSidebarTitle {
      title: Strings.globalSettings
      Layout.leftMargin: ThemeController.style.margin.m16
      tooltipDirection: UIKit.SettingsSidebarTitle.TooltipDirection.Up
    }

    ButtonGroup {
      id: buttonsGroup
      exclusive: true
      onClicked: {
        console.log("CLICKED", checkedButton.idx)
        selectedIndex = checkedButton.idx
      }
    }

    UIKit.SidebarSettingsItem {
      Layout.topMargin: ThemeController.style.margin.m8
      text: Strings.account
      ButtonGroup.group: buttonsGroup
      idx: Enums.GlobalFragment
      checked: true
    }

    UIKit.SidebarSettingsItem {
      text: Strings.loginAndInactivity
      ButtonGroup.group: buttonsGroup
      idx: Enums.GlobalLogin
    }

    UIKit.SidebarSettingsItem {
      text: Strings.devices
      ButtonGroup.group: buttonsGroup
      idx: Enums.GlobalDevices
    }

    UIKit.SidebarSettingsItem {
      text: Strings.dataStr
      ButtonGroup.group: buttonsGroup
      idx: Enums.GlobalData
    }

    Rectangle {
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.fillWidth: true
      color: ThemeController.style.mercuryColor
      Layout.preferredHeight: s(1)
    }

    UIKit.SettingsSidebarTitle {
      Layout.topMargin: ThemeController.style.margin.m8
      title: Strings.userSettings
      Layout.leftMargin: ThemeController.style.margin.m16
    }

    UIKit.SidebarSettingsItem {
      Layout.topMargin: ThemeController.style.margin.m8
      text: Strings.user
      ButtonGroup.group: buttonsGroup
      idx: Enums.UserFragment
    }

    UIKit.SidebarSettingsItem {
      text: Strings.loginAndInactivity
      ButtonGroup.group: buttonsGroup
      idx: Enums.UserLogin
    }

    UIKit.SidebarSettingsItem {
      text: Strings.dataStr
      ButtonGroup.group: buttonsGroup
      idx: Enums.UserData
    }

    Rectangle {
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.fillWidth: true
      color: ThemeController.style.mercuryColor
      Layout.preferredHeight: s(1)
    }

    UIKit.SettingsSidebarTitle {
      Layout.topMargin: ThemeController.style.margin.m8
      title: Strings.deviceSettings
      Layout.leftMargin: ThemeController.style.margin.m16
    }

    UIKit.SidebarSettingsItem {
      Layout.topMargin: ThemeController.style.margin.m8
      text: Strings.device
      ButtonGroup.group: buttonsGroup
      idx: Enums.DeviceFragment
    }

    UIKit.SidebarSettingsItem {
      text: Strings.loginAndInactivity
      ButtonGroup.group: buttonsGroup
      idx: Enums.DeviceLogin
    }

    UIKit.SidebarSettingsItem {
      text: Strings.dataStr
      ButtonGroup.group: buttonsGroup
      idx: Enums.DeviceData
    }

    UIKit.SidebarSettingsItem {
      text: Strings.miscellaneous
      ButtonGroup.group: buttonsGroup
      idx: Enums.DeviceMiscellaneous
    }

    Rectangle {
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.fillWidth: true
      color: ThemeController.style.mercuryColor
      Layout.preferredHeight: s(1)
    }

    UIKit.StyledButton {
      id: button
      Layout.topMargin: ThemeController.style.margin.m8
      text: Strings.resetSettings
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignHCenter
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      type: UIKit.StyledButton.ButtonStyle.Secondary
      onClicked: {
        resetSettings.open()
      }

      UIKit.ResetSettingsPopup {
        id: resetSettings
        x: button.width + 8
        y: -resetSettings.height + button.height
      }
    }
  }
}
