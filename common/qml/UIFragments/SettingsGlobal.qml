import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: layout
  spacing: 0

  UIKit.InfoDelegate {
    contentText: Strings.theseSettingsWillApply
  }

  UIKit.SettingsDelegate {
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    title: Strings.account
    contentText: Strings.nameAndEmail
    onClicked: homeManager.openPage(Pages.GlobalAccountSettingsPage)
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.loginAndInactivity
    contentText: Strings.settingsToControl
    onClicked: homeManager.enterLoginInactivitySettingsPage({"pageMode": Enums.GlobalSettings})
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.devices
    contentText: Strings.setTheNumber
    onClicked: homeManager.openPage(Pages.GlobalDevicesSettingsPage)
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.dataStr
    contentText: Strings.setTheAccessibility
    onClicked: homeManager.enterDataSettingsPage({"pageMode": Enums.GlobalSettings})
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }

  UIKit.StyledButton {
    id: button
    text: Strings.resetSettings
    Layout.bottomMargin: ThemeController.style.margin.m16
    Layout.alignment: Qt.AlignHCenter
    Layout.preferredWidth: app.isTablet ? s(352) : parent.width
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Secondary
    onClicked: homeManager.enterResetSettingsPage({"pageMode": Enums.GlobalSettings})
  }
}
