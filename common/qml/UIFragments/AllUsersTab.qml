import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import UIFragments 1.0 as UIFragments
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2
import QtQml.Models 2.12
import UIModel 1.0

import com.testintl 1.0

ColumnLayout {
  spacing: ThemeController.style.homePageVerticalMargin
  width: parent.width
  height: parent.height
  property bool selectionMode: ism.hasSelection
  property ItemSelectionModel ism: selectionModel
  property string objectTypeName: Strings.users
  property int objectType: Enums.User

  property bool isAO: AuthenticationManager.currentAccount.accountOwner
  property bool isGuest: AuthenticationManager.currentAccount.guest

  ItemSelectionModel {
    id: selectionModel
    model: userModel
  }

  SortFilterProxyModel {
    id: userModel
    sourceModel: UserListModel
    filters: ValueFilter {
      roleName: "pending"
      value: false
    }
  }

  SortFilterProxyModel {
    id: pendingUsersModel
    sourceModel: UserListModel
    filters: ValueFilter {
      roleName: "pending"
      value: true
    }
  }

  SortFilterProxyModel {
    id: pinnedModel
    sourceModel: userModel
    filters: ValueFilter {
      roleName: "pinned"
      value: true
    }
  }

  SortFilterProxyModel {
    id: objectsProxyModel
    sourceModel: userModel
    filters: ValueFilter {
      roleName: "pinned"
      value: false
    }
  }

  UIKit.SearchInput {
    id: search
    Layout.fillWidth: true
    placeholderText: Strings.searchUsers
    micIconVisible: false
    Layout.preferredHeight: ThemeController.style.defaultInputHeight
  }

  UIKit.DevicesPendingDelegate {
    id: pendingInvitationsDelegate
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m8 : 0
    Layout.fillWidth: true
    mode: UIKit.DevicesPendingDelegate.Mode.Users
    objectsModel: pendingUsersModel
    requestsCount: pendingUsersModel.count
    visible: pendingUsersModel.count > 0
    onButtonClicked: homeManager.openPage(Pages.PendingInvitationsPage)
  }

  UIKit.DevicesPendingDelegate {
    id: pendingRequestsDelegate
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m8 : 0
    Layout.fillWidth: true
    mode: UIKit.DevicesPendingDelegate.Mode.Contacts
    objectsModel: pendingUsersModel
    requestsCount: pendingUsersModel.count
    visible: pendingUsersModel.count > 0
    onButtonClicked: homeManager.openPage(Pages.PendingRequestsPage)
  }

  Flickable {
    clip: true
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m8 : 0
    contentHeight: column.height

    Column {
      id: column
      anchors.left: parent.left
      anchors.right: parent.right
      spacing: ThemeController.style.margin.m16
      bottomPadding: ThemeController.style.margin.m16

      UIKit.GenericListView {
        id: pinnedObjectList
        anchors.left: parent.left
        anchors.right: parent.right
        height: contentHeight
        clip: true
        spacing: app.isPhone ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
        visible: pinnedObjectList.count > 0

        header: PinHeader { }

        footer: ListFooter { }

        model: pinnedModel
        delegate: UIKit.UserDelegate {
          title: model.email
          stateVisible: true
          online: model.online
          lastSeenDate: model.lastSeen
          objectsModel: pinnedObjectList.model

          width: pinnedObjectList.width
          actionButton: !ism.hasSelection
          checkboxOnTheLeft: !isMobile
          checkable: !isMobile || pinnedObjectList.selectionMode
          selectionModel: ism
          onPressAndHold: {
            if (isMobile)
              pinnedObjectList.selectionMode = !pinnedObjectList.selectionMode
          }

          onDelegateClicked: {
            homeManager.openOneUserPage(model.id)
          }

          onActionButtonClicked: {
            pinnedObjectList.currentIndex = index
            actionMenu.model.currentObject = object
            actionMenu.handleActionButton(pinnedObjectList)
          }
        }
      }

      UIKit.GenericListView {
        id: listView
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: ThemeController.style.margin.m8
        height: contentHeight
        clip: true
        spacing: app.isPhone ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
        header: UIFragments.UsersListSortButton {}
        visible: listView.count > 0

        model: objectsProxyModel
        delegate: UIKit.UserDelegate {
          title: model.email
          titleIcon: model.hasPower
          //role: ""
          stateVisible: true
          online: model.online
          lastSeenDate: model.lastSeen
          avatar.editIcon: false

          actionButton: !ism.hasSelection
          width: listView.width
          objectsModel: listView.model

          checkboxOnTheLeft: !isMobile
          checkable: !isMobile || listView.selectionMode
          selectionMode: listView.selectionMode
          selectionModel: ism

          onDelegateClicked: {
            homeManager.openOneUserPage(model.id)
          }

          onActionButtonClicked: {
            listView.currentIndex = index
            actionMenu.model.currentObject = model.object
            actionMenu.handleActionButton(listView)
          }
        }
      }
    }
  }

  OneUserActionMenu {
    id: actionMenu
  }
}
