import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m24

  UIKit.BaseText {
    text: Strings.miscellaneous
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
  }

  Flickable {
    id: flick
    Layout.fillHeight: true
    Layout.fillWidth: true
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: content.height
    clip: true

    ColumnLayout {
      id: content
      spacing: ThemeController.style.margin.m24
      width: parent.width

      UIKit.BaseText {
        text: Strings.useNetworkProxy
        font.weight: Font.Bold
      }

      ButtonGroup {
        id: networkProxyGroup
        buttons: networkProxy.children
        exclusive: true
      }

      Column {
        id: networkProxy
        spacing: ThemeController.style.margin.m24
        Layout.topMargin: app.isPhone ? ThemeController.style.margin.m8 : 0

        UIKit.StyledRadioButton {
          checked: true
          text: Strings.no
        }

        UIKit.StyledRadioButton {
          text: Strings.useSystem
        }
      }

      Rectangle {
        Layout.topMargin: ThemeController.style.margin.m12
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
      }

      UIKit.SwitchItem {
        text: Strings.autoCheckUpdates
        showSeparator: true
        separatorMargin: ThemeController.style.margin.m16
      }

      UIKit.SwitchItem {
        text: Strings.launchAtStartup
        showSeparator: true
        separatorMargin: ThemeController.style.margin.m16
      }

      UIKit.SwitchItem {
        text: Strings.minimizeToTray
        showSeparator: true
        separatorMargin: ThemeController.style.margin.m16
      }

      UIKit.SwitchItem {
        text: Strings.closeToTray
        showSeparator: true
        separatorMargin: ThemeController.style.margin.m16
      }

      UIKit.SwitchItem {
        text: Strings.receiveInAppNotifications
        contentText: Strings.inAppMessagesToKeepUpdated
        showSeparator: true
        separatorMargin: ThemeController.style.margin.m16
      }

      UIKit.SwitchItem {
        text: Strings.receivePushNotofications
        contentText: Strings.pushNotificationsToKeepUpdated
        showSeparator: true
        separatorMargin: ThemeController.style.margin.m16
      }

      UIKit.SwitchItem {
        text: Strings.allowScanning
        contentText: Strings.buildContextSyllabus
        showSeparator: true
        separatorMargin: ThemeController.style.margin.m16
      }

      UIKit.SwitchItem {
        text: Strings.deleteAfterImport
        contentText: Strings.allowDeleting
        Layout.bottomMargin: s(41)
      }
    }
  }
}
