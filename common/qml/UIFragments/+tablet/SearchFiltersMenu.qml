import QtQuick 2.12
import QtQuick.Controls 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import QtQuick.Layouts 1.12
import UIFragments 1.0
import QtQml.Models 2.12
import UIModel 1.0 as UIModel

import Qt5Compat.GraphicalEffects

Popup {
  id: popup
  implicitHeight: header.height + content.height + bottomPadding + topPadding + 24
  implicitWidth: 262

  modal: true
  Overlay.modal: Item{}
  leftPadding: 0
  rightPadding: 0
  topPadding: 8
  bottomPadding: 8

  background: Item {
    Rectangle {
      id: bgComponent
      anchors.fill: parent
      radius: 8
    }
    DropShadow {
      id: dropShadow
      anchors.fill: bgComponent
      horizontalOffset: 0
      verticalOffset: 0
      radius: 32
      samples: 63
      color: Qt.rgba(0, 0, 0, 0.08)
      source: bgComponent
    }
  }

  Item {
    id: header
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
    }
    height: 56

    RowLayout {
      anchors {
        fill: parent
        leftMargin: 16
        rightMargin: 16
      }

      UIKit.BaseText {
        id: text
        text: Strings.filters
        Layout.alignment: Qt.AlignHCenter
        font.weight: Font.Bold
        elide: Text.ElideRight
        color: ThemeController.style.arcaMobileDelegate.titleTextColor
      }

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
      }

      UIKit.StyledButton {
        type: UIKit.StyledButton.ButtonStyle.Text
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        text: Strings.clearAll
        visible: true
      }
    }

    Rectangle {
      anchors {
        bottom: parent.bottom
        left: parent.left
        right: parent.right
      }
      color: ThemeController.style.mercuryColor
      height: 1
      visible: headerSeparatorVisible
    }
  }

  Flickable {
    id: flickable
    anchors {
      top: header.bottom
      topMargin: 24
      bottom: parent.bottom
    }

    width: parent.width
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: content.height
    clip: true
    ColumnLayout {
      id: content
      spacing: 16
      width: parent.width

      QuickFilters {
        Layout.fillWidth: true
        Layout.leftMargin: 16
        Layout.rightMargin: 16
      }

      UIKit.BaseText {
        Layout.topMargin: 8
        Layout.leftMargin: 16
        font.weight: Font.DemiBold
        elide: Text.ElideRight
        color: ThemeController.style.charadeColor
        font.capitalization: Font.AllUppercase
        text: Strings.moreFilters
      }

      ListView {
        id: listView
        Layout.preferredHeight: contentHeight
        Layout.fillWidth: true
        model: filtersModel.model
        interactive: false
        delegate: UIKit.ActionListDelegate {
          leftPadding: 16
          rightPadding: 16
          width: parent.width
          text: name
          icon.source: iconSource
          display: AbstractButton.TextBesideIcon
          separator: false
          actionIcon: true
        }
      }
    }
  }

  UIModel.FiltersModel {
    id: filtersModel
  }
}
