import QtQuick 2.12
import QtQuick.Controls 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0

import Qt5Compat.GraphicalEffects

Popup {
  id: popup
  implicitHeight: listView.contentHeight + bottomPadding + topPadding
  implicitWidth: s(262)

  // If popup should be scrollable use this property to set maximum height
  // Also explicitly set popup.height to the same value
  property int maxHeight: 0

  modal: true
  Overlay.modal: Item{}
  leftPadding: 0
  rightPadding: 0
  topPadding: s(8)
  bottomPadding: s(8)

  property alias model: listView.model
  // Object item
  property var currentItem

  background: Item {
    Rectangle {
      id: bgComponent
      anchors.fill: parent
      radius: s(8)
    }
    DropShadow {
      id: dropShadow
      anchors.fill: bgComponent
      horizontalOffset: 0
      verticalOffset: 0
      radius: s(32)
      //samples: s(63)
      color: Qt.rgba(0, 0, 0, 0.08)
      source: bgComponent
    }
  }


  ListView {
    id: listView
    height: maxHeight > 0 ? popup.height - bottomPadding - topPadding : contentHeight
    width: parent.width
    clip: true
    delegate: UIKit.ActionListDelegate {
      width: parent.width
      text: name
      icon.source: iconSource
      display: AbstractButton.TextBesideIcon
      onClicked: popup.close()
      action: actionSource
    }
  }
}
