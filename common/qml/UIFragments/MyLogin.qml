import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: layout
  spacing: 0
  signal enterDeleteLogin()

  UIKit.SettingsDelegate {
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    title: Strings.profileDetails
    contentText: Strings.nameNicknameAndEmail
    onClicked: homeManager.openPage(Pages.ProfileDetailsPage)
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.loginPassphrase
    contentText: Strings.changeLoginPassphrase
    onClicked: homeManager.openPage(Pages.LoginPassphrasePage)
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.historyStatus
    contentText: Strings.loginCreation
    onClicked: homeManager.openPage(Pages.HistoryAndStatusPage)
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.gdprInformation
    contentText: Strings.eulaPrivacyPolicy
    onClicked: homeManager.openPage(Pages.GdprInformationPage)
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.loginInformation
    contentText: Strings.overviewOfLogin
    onClicked: homeManager.openPage(Pages.LoginInformationPage)
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }

  UIKit.StyledButton {
    id: button
    text: Strings.deleteLogin
    Layout.bottomMargin: ThemeController.style.margin.m16
    Layout.alignment: Qt.AlignHCenter
    Layout.preferredWidth: app.isTablet ? s(352) : parent.width
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Secondary
    onClicked: enterDeleteLogin()
  }
}
