import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.15

ColumnLayout {
  id: panel
  spacing: ThemeController.style.margin.m12
  height: parent.height
  width: parent.width

  property var searchModel
  property var ism
  property int selectedCount: ism ? ism.selectedIndexes.length : 0

  function clearAll() {
    if (ism) ism.clearSelection()
  }

  // Applies filtering
  function applyFilters() {
    searchModel.updateFilters();
  }

  onSearchModelChanged: {
    if (searchModel) {
      var m = searchModel.filters.filter(SFilter.LockStatusFilter);
      repeater.model = m.model
      panel.ism = m.selectionModel
    }
  }

  Repeater {
    id: repeater
    UIKit.ActionListDelegate {
      Layout.fillWidth: true
      text: model.name
      icon.source: model.iconSource
      display: AbstractButton.TextBesideIcon
      separator: true
      checkable: true
      leftPadding: 0
      rightPadding: 0
      checkMarkIcon: true
      onClicked: {
        searchModel.filters.filter(SFilter.LockStatusFilter).toggle(index, ItemSelectionModel.Toggle)
      }
    }
  }

  ButtonGroup {
    id: buttonsGroup
    exclusive: true
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
