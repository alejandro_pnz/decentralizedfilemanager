import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import QtQuick.Layouts 1.12

ColumnLayout {
  id: column

  signal deleteLogin()
  property int selectedIndex: buttonsGroup.checkedButton.idx

  implicitWidth: s(208)
  spacing: ThemeController.style.margin.m12

  UIKit.BaseText {
    text: Strings.myLogin
    Layout.leftMargin: ThemeController.style.margin.m16
    Layout.topMargin: ThemeController.style.margin.m16
    size: UIKit.BaseText.TextSize.Small
    font.weight: Font.DemiBold
    font.capitalization: Font.AllUppercase
  }

  ButtonGroup {
    id: buttonsGroup
    exclusive: true
  }

  Column {
    spacing: ThemeController.style.margin.m4
    Layout.topMargin: ThemeController.style.margin.m4
    Repeater {
      id: repeater
      model: buttonsModel
      delegate: UIKit.SidebarSettingsItem {
        text: name
        ButtonGroup.group: buttonsGroup
        checked: index === 0
        idx: index
      }
    }
  }

  Rectangle {
    id: spacer
    Layout.fillWidth: true
    color: ThemeController.style.mercuryColor
    Layout.preferredHeight: s(1)
  }

  UIKit.StyledButton {
    id: button
    text: Strings.deleteLogin
    Layout.fillWidth: true
    Layout.alignment: Qt.AlignHCenter
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Secondary
    onClicked: deleteLogin()
  }

  ListModel {
    id: buttonsModel
  }

  Component.onCompleted: {
    buttonsModel.append({ index: 0, name: Strings.profileDetails});
    buttonsModel.append({ index: 1, name: Strings.loginPassphrase});
    buttonsModel.append({ index: 2, name: Strings.historyStatus});
    buttonsModel.append({ index: 3, name: Strings.gdprInformation});
    buttonsModel.append({ index: 4, name: Strings.loginInformation});
  }
}
