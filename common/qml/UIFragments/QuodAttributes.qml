import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2

Item {
  anchors.fill: parent
  property alias buttonGroup: btnGroup
  property bool recentlyUsedMode: false
  onVisibleChanged: {
    if (!visible) recentlyUsedMode = false
  }

  MouseArea {
    anchors.fill: content
    onPressed: {
      forceActiveFocus()
      recentlyUsedMode = false
    }
  }

  ButtonGroup {
    id: btnGroup
  }
  ColumnLayout {
    id: content
    anchors.fill: parent
    spacing: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m12
    UIKit.SearchInput {
      id: search
      Layout.topMargin: ThemeController.style.margin.m16
      Layout.fillWidth: true
      placeholderText: Strings.searchAttributes
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
      micIconVisible: false
      onActiveFocusChanged: {
        if (activeFocus) {
          if (text.length <= 0) recentlyUsedMode = true
        }
      }
      onTextChanged: {
        if (text.length <= 0) {
          recentlyUsedMode = activeFocus
        } else {
          recentlyUsedMode = false
        }
      }
    }

    UIKit.AddComponent {
      id: addComponent
      Layout.topMargin: app.isPhone ? 0 : ThemeController.style.margin.m4
      buttonText: search.searchText.length > 0
                  ? Strings.create + " \"" + search.searchText + "\""
                  : Strings.createCustomAttribute
      onAddButtonClicked: homeManager.createCustomAttribute(search.searchText)
      visible: !itemsModel.attributeExists(search.searchText)
    }

    // All attributes list
    ListView {
      id: listView
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.leftMargin: app.isPhone ? 0 : -ThemeController.style.margin.m16
      Layout.rightMargin: app.isPhone ? 0 : -ThemeController.style.margin.m16
      visible: !recentlyUsedView.visible

      spacing: 0
      clip: true

      model: proxyModel
      delegate: UIKit.ActionListDelegate {
        width: parent.width
        text: search
        separator: app.isPhone
        leftPadding: app.isPhone ? 0 : ThemeController.style.margin.m16
        rightPadding: app.isPhone ? 0 : ThemeController.style.margin.m16
        actionIcon: app.isPhone
        pointingHandCursor: true
        ButtonGroup.group: btnGroup
        onClicked: {
          QuodTypeModel.currentType.addAttribute(object)
        }
      }
    }

    UIKit.BaseText {
      text: Strings.recentlyUsed
      font.capitalization: Font.AllUppercase
      font.weight: Font.DemiBold
      visible: recentlyUsedView.visible
      color: ThemeController.style.charadeColor
      size: UIKit.BaseText.TextSize.Small
    }


    // Recently used list
    ListView {
      id: recentlyUsedView
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.topMargin: -ThemeController.style.margin.m4
      Layout.leftMargin: app.isPhone ? 0 : -ThemeController.style.margin.m16
      Layout.rightMargin: app.isPhone ? 0 : -ThemeController.style.margin.m16

      spacing: 0
      clip: true

      model: recentlyUsedProxy
      visible: recentlyUsedMode
      delegate: UIKit.ActionListDelegate {
        width: parent.width
        text: object.name
        separator: app.isPhone
        leftPadding: app.isPhone ? 0 : ThemeController.style.margin.m16
        rightPadding: app.isPhone ? 0 : ThemeController.style.margin.m16
        actionIcon: app.isPhone
        pointingHandCursor: true
        ButtonGroup.group: btnGroup
        onClicked: {
          QuodTypeModel.currentType.addAttribute(object)
          recentlyUsedMode = false
        }
      }
    }

    SortFilterProxyModel {
      id: recentlyUsedProxy
      sourceModel: itemsModel
      filters: [
        ValueFilter {
          roleName: "recentlyUsed"
          value: true
        }
      ]
    }

    SortFilterProxyModel {
      id: proxyModel
      sourceModel: searchProxyModel
      filters: [
        RegExpFilter {
          roleName: "name"
          pattern: search.searchText
          caseSensitivity: Qt.CaseInsensitive
        }
      ]
    }

    SearchProxyModel {
      id: searchProxyModel
      sourceModel: itemsModel
      pattern: search.searchText
    }

    ListModel {
      id: itemsModel
    }
  }
}
