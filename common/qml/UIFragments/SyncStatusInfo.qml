import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

ColumnLayout {
  spacing: 12
  width: parent.width
  signal okClicked()

  UIKit.BaseText {
    text: Strings.whatIsUserSyncStatus
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H3
    Layout.fillWidth: true
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.BaseText {
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m4 : 0
    text: Strings.syncStatusDefines
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.CardDelegate {
    Layout.topMargin: ThemeController.style.margin.m4
    Layout.fillWidth: true
    title: Strings.publicSync
    text: Strings.shareWithEveryone
    icon.source: "qrc:/public-dark.svg"
    checkable: false
    background: Item {}
    textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    textComponent.elide: Text.ElideNone
    leftPadding: 0
    rightPadding: 0
    avatar.pixelSize: s(48)
    avatar.iconSize: s(24)
  }

  Rectangle {
    Layout.preferredHeight: s(1)
    Layout.fillWidth: true
    color: ThemeController.style.seashellColor
  }

  UIKit.CardDelegate {
    Layout.topMargin: ThemeController.style.margin.m4
    Layout.fillWidth: true
    title: Strings.privateSync
    text: Strings.visibleOnYourDevices
    icon.source: "qrc:/private.svg"
    checkable: false
    background: Item {}
    textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    textComponent.elide: Text.ElideNone
    leftPadding: 0
    rightPadding: 0
    avatar.pixelSize: s(48)
    avatar.iconSize: s(24)
  }

  Rectangle {
    Layout.preferredHeight: s(1)
    Layout.fillWidth: true
    color: ThemeController.style.seashellColor
  }

  UIKit.CardDelegate {
    Layout.topMargin: ThemeController.style.margin.m4
    Layout.fillWidth: true
    title: Strings.noSync
    text: Strings.visibleOnThisDevice
    icon.source: "qrc:/no-sync-dark.svg"
    checkable: false
    background: Item {}
    textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    textComponent.elide: Text.ElideNone
    leftPadding: 0
    rightPadding: 0
    avatar.pixelSize: s(48)
    avatar.iconSize: s(24)
  }

  UIKit.InfoDelegate {
    Layout.fillWidth: true
    contentText: Strings.canChangeSyncStatus
  }

  UIKit.StyledButton {
    id: button
    text: Strings.okGotIt
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Primary
    onClicked: okClicked()
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
