import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2

import com.testintl 1.0

Flickable {
  id: flick
  height: parent.height
  width: parent.width
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: layout.height
  clip: true
  property bool showFooter: false
  property bool isMobile: app.isPhone

  ColumnLayout {
    id: layout
    spacing: ThemeController.style.margin.m16
    width: parent.width

    UIKit.BaseText {
      text: Strings.historyStatus
      visible: !isMobile
      font.weight: Font.Bold
      size: UIKit.BaseText.TextSize.H2
    }

    UIKit.BaseText {
      Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m8
      font.weight: Font.Bold
      text: Strings.yourLogin
    }

    UIKit.InfoItem {
      labelText: Strings.createdOn
      content: Utility.formatDateTime(UserManager.currentUser.created)
    }

    UIKit.BaseText {
      Layout.topMargin: isMobile ? s(9) : ThemeController.style.margin.m8
      font.weight: Font.Bold
      text: Strings.yourDevices
    }

    ListView {
      id: listView
      Layout.fillWidth: true
      Layout.preferredHeight: listView.contentHeight
      spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
      model: devicesModel
      interactive: false
      delegate: UIKit.DeviceDelegate {
        width: listView.width
        title: model.deviceModel
        titleIcon: false
        actionButton: false
        avatar.deviceType: model.deviceType
        avatar.bottomIcon: true
        avatar.avatarMode: UIKit.DeviceAvatar.Mode.Inactive
        online: model.online
        contentText: Strings.addedOn + " " + Utility.formatDateTime(model.created)
        syncing: model.syncing
        outOfSync: model.outOfSync
        lastSeenDate: model.lastSeen
        checkable: false
        stateVisible: false
        showStatusText: false
        enabled: false
      }
    }

    UIKit.BaseText {
      Layout.topMargin: isMobile ? s(9) : ThemeController.style.margin.m8
      font.weight: Font.Bold
      text: Strings.activeSessionsStr
    }

    ListView {
      id: sessionsListView
      Layout.fillWidth: true
      Layout.preferredHeight: sessionsListView.contentHeight
      spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
      interactive: false
      model: sessionsModel
      delegate: UIKit.SessionDelegate {
        title: name
        actionButton: true
        ipAddress: ip
        date: dateTxt
      }
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }

    SortFilterProxyModel {
      id: devicesModel
      sourceModel: DeviceListModel
      filters: [
        ValueFilter {
          roleName: "pending"
          value: false
        },
        ValueFilter {
          roleName: "email"
          value: AuthenticationManager.currentAccount.email
        }
      ]
    }

    ListModel {
      id: sessionsModel
    }

    Component.onCompleted: {
      sessionsModel.append({name: "London, UK", ip: "192.168.23.3", dateTxt: "13/04/2021"});
      sessionsModel.append({name: "Derby, UK", ip: "192.168.25.3", dateTxt: "13/04/2021"});
    }
  }
}
