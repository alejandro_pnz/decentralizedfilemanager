import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIPage 1.0

Item {
  property alias homeStackView: homeStackView

  Connections {
    target: homeManager

    function onPushPage(page, pagePath) {
      if (pagePath.length > 0) {
        switch (page) {
        case Pages.LibraryPage:
          if (libraryScreenLoader.status === Loader.Ready) {
            homeStackView.pushPage(libraryScreenLoader.item)
            libraryScreenLoader.item.visible = true
          } else {
            libraryScreenLoader.requested = true
          }
          break
        default:
          homeStackView.pushPage(Qt.createComponent(pagePath))
          break
        }
        console.log("Items on stack", homeStackView.depth)
      } else {
        homeStackView.push(emptyScreen)
      }
    }

    function onPopPage() {
      homeStackView.pop()
      homeStackView.current = Qt.binding(function(){ return homeStackView.currentItem})
      console.log("Items on stack after pop", homeStackView.depth)
    }
  }

  StackView {
    id: homeStackView
    anchors.fill: parent

    property Item current: currentItem

    initialItem: homeScreen

    popEnter: Transition {}
    pushEnter: Transition {}
    popExit: Transition {}
    pushExit: Transition {}
    replaceEnter: Transition {}
    replaceExit: Transition {}

    function pushPage(page) {
      push(page)
      console.log("IS POPUP", currentItem.isPopup)
      if (currentItem.isPopup) showPrevPage()
    }

    // Method used with popups pages.
    function showPrevPage() {
      var prevPageIndex = homeStackView.depth - 2
      console.log("Show prev page", prevPageIndex)
      if (prevPageIndex >= 0) {
        var page = homeStackView.get(prevPageIndex)
        page.visible = true
        page.opacity = 1
        page.scale = 1
        current = page
      }
    }
  }


  // Pages components
  Component {
    id: emptyScreen
    AbstractHomePage {
      UIKit.BaseText {
        anchors.centerIn: parent
        text: "NOT IMPLEMENTED"
      }
    }
  }

  Component {
    id: homeScreen
    HomeContentPage { }
  }

  Loader {
    id: libraryScreenLoader
    sourceComponent: libraryScreen
    asynchronous: true
    visible: false
    property bool requested: false

    onLoaded: {
      if(libraryScreenLoader.requested) {
        homeStackView.pushPage(libraryScreenLoader.item)
        libraryScreenLoader.item.visible = true
      } else {
        libraryScreenLoader.item.visible = false
      }
    }
  }

  Component {
    id: libraryScreen
    LibraryPage {
      onShowSearchScreen: {
       homeManager.openPage(Pages.SearchPage)
      }
      Component.onCompleted: console.log("Library completed")
    }
  }
}
