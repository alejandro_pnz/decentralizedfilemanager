import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

Flickable {
  id: flick
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true
  property var searchModel: null
  property bool accepted: false
  property bool __initRequested: false
  property int selectedCount: quodType.selectedCount + quickFilters.selectedCount + fileType.selectedCount

  onSearchModelChanged: {
    if (__initRequested) init()
  }

  // Signal to store the current selection state before user do some changes
  function init() {
    if (searchModel) {
      __initRequested = false
      console.log("MORE FILTERS.qml", searchModel)
      searchModel.filters.filter(SFilter.QuodTypeFilter).init()
      searchModel.filters.filter(SFilter.FileTypeFilter).init()
      if (!app.isPhone)
        searchModel.filters.filter(SFilter.QuickFilters).init()
    } else {
      __initRequested = true
    }
  }

  // Reverts all changes made by user
  function revertChanges() {
    searchModel.filters.filter(SFilter.QuodTypeFilter).revertChanges()
    searchModel.filters.filter(SFilter.FileTypeFilter).revertChanges()
    if (!app.isPhone)
      searchModel.filters.filter(SFilter.QuickFilters).revertChanges()
    cleanup()
  }

  // Clears the selection
  function clearAll() {
    quickFilters.clearAll()
    quodType.clearAll()
    fileType.clearAll()
  }

  // Applies filtering
  function applyFilters() {
    accepted = true
    searchModel.updateFilters();
    cleanup()
  }

  function cleanup() {
  }

  ColumnLayout {
    id: content
    spacing: ThemeController.style.margin.m24
    width: parent.width

    Connections {
     target: homeManager
     function onSetupMoreFiltersPage(config) {
       console.log("Setup more filters", config.searchModel)
       searchModel = config.searchModel
     }
    }

    QuickFilters {
      id: quickFilters
      Layout.fillWidth: true
      visible: !app.isPhone
      searchModel: flick.searchModel
    }

    Rectangle {
      color: ThemeController.style.mercuryColor
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      visible: !app.isPhone
    }

    QuodTypePanel {
      id: quodType
      Layout.fillWidth: true
      searchModel: flick.searchModel
    }

    FileTypePanel {
      id: fileType
      Layout.fillWidth: true
      searchModel: flick.searchModel
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }
  }
}
