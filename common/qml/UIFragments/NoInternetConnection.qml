import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: columnLayout

  spacing: ThemeController.style.margin.m8

  signal tryAgainClicked()

  Rectangle {
    Layout.preferredHeight: app.isPhone ? s(222) : s(230)
    Layout.fillWidth: true
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: "qrc:/illustrations/NoInternetConnection.png"
      mipmap: true
    }
  }

  UIKit.BaseText {
    text: Strings.noInternetConnection
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.BaseText {
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Text.AlignHCenter
    text: Strings.makeSureWiFiIsOn
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.StyledButton {
    Layout.alignment: Qt.AlignHCenter
    width: s(115)
    text: Strings.tryAgain
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: tryAgainClicked()
  }

}
