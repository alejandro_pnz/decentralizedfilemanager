import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import "../UIKit/Charts/PieChart"

ColumnLayout {
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m24
  property bool showFooter: false

  property string ownerName: "Lauren Paulson"
  property string ownerEmail: "owner@companyemail.com"
  property string name: "Lauren Paulson"
  property string nickName: "-"
  property string email: "laurenpaulson@example.com"
  property string joinedOn: "14 April 2021, 17:36"
  property int userDevices: 3
  property int powerDevices: 3
  property int quodNumber: 130
  property int arcaNumber: 12
  property int filesNumber: 30
  property string freeSpace: "1.57 GB"
  property string usedSpace: "654 MB"
  property string ipLocation: "192.168.23.3 \u2022 London, UK"
  property string secondIpLocation: "192.168.23.3 \u2022 London, UK"

  UIKit.BaseText {
    text: Strings.loginInformation
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    visible: !app.isMobile
  }

  Flickable {
    Layout.fillHeight: true
    Layout.fillWidth: true
    Layout.topMargin: app.isMobile ? ThemeController.style.margin.m16 : 0
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: content.height
    clip: true

    Flow {
      id: content
      width: parent.width
      height: app.isMobile ? undefined : s(796)
      flow: Flow.TopToBottom
      spacing: ThemeController.style.margin.m24

      Column {
        width: s(256)
        spacing: ThemeController.style.margin.m12
        UIKit.BaseText {
          text: Strings.icon
          color: ThemeController.style.shuttleColor
        }

        UIKit.Avatar {
          id: objectAvatar
          size: UIKit.Avatar.Size.Big
          text: name
        }
      }

      UIKit.InfoItem {
        width: s(256)
        labelText: Strings.accountOwnerName
        content: ownerName
      }

      UIKit.InfoItem {
        width: s(256)
        labelText: Strings.accountOwnerEmail
        content: ownerEmail
      }

      UIKit.InfoItem {
        width: s(256)
        labelText: Strings.name
        content: name
      }

      UIKit.InfoItem {
        width: s(256)
        labelText: Strings.nickname
        content: nickName
      }

      UIKit.InfoItem {
        width: s(256)
        labelText: Strings.email
        content: email
      }

      UIKit.InfoItem {
        width: s(256)
        labelText: Strings.joinedAccountOn
        content: joinedOn
      }

      Column {
        spacing: ThemeController.style.margin.m12
        width: s(256)
        UIKit.BaseText {
          text: Strings.mainIPLocations
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          text: ipLocation
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: secondIpLocation
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: Strings.otherLocations.arg(8)
          font.weight: Font.DemiBold
        }
      }

      Column {
        spacing: ThemeController.style.margin.m12
        width: s(256)
        UIKit.BaseText {
          text: Strings.loginDevices
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          text: Strings.userDevices.arg(userDevices)
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: Strings.powerDevicesOutOf.arg(powerDevices).arg("3")
          font.weight: Font.DemiBold
        }
      }

      AccountInformationChart {
        dataModel: modelData
        mobileChartTopMargin: 0
      }

      Column {
        spacing: ThemeController.style.margin.m12
        width: s(256)
        UIKit.BaseText {
          text: Strings.loginObjects
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          text: quodNumber + " " + Strings.quod
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: filesNumber + " " + Strings.files
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: arcaNumber + " " + Strings.arca
          font.weight: Font.DemiBold
        }
      }

      Column {
        spacing: ThemeController.style.margin.m12
        width: s(256)
        bottomPadding: app.isMobile ? ThemeController.style.margin.m16 : 0
        UIKit.BaseText {
          text: Strings.localStorage
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          text: Strings.usedSpace.arg(usedSpace)
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: Strings.freeSpace.arg(freeSpace)
          font.weight: Font.DemiBold
        }
      }
    }
  }

  ListModel {
    id: modelData
  }

  Component.onCompleted: {
    modelData.append({label: "Windows", colorValue: "#2F3441", value: 80})
    modelData.append({label: "Android", colorValue: "#22D396", value: 20})
  }
}
