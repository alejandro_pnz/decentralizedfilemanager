import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

ColumnLayout {
  spacing: ThemeController.style.margin.m12
  width: parent.width
  height: parent.height

  Rectangle {
    Layout.preferredHeight: s(222)
    Layout.fillWidth: true
    radius: s(10)

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: "qrc:/illustrations/u7.png"
      mipmap: true
    }
  }

  UIKit.BaseText {
    text: Strings.lockQuodOrFileTipTitle
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H3
    Layout.topMargin: ThemeController.style.margin.m12
    Layout.fillWidth: true
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.BaseText {
    text: Strings.lockQuodOrFileTipContent
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.fillHeight: true
    //TODO: text needs to be adjusted to fit available space
    elide: Text.ElideRight
  }

  UIKit.Infobox {
    Layout.topMargin: ThemeController.style.margin.m12
    Layout.fillWidth: true
    text: Strings.lockQuodOrFileTipInfo
  }

  RowLayout {
    Layout.topMargin: ThemeController.style.margin.m12
    Layout.fillWidth: true
    UIKit.BaseText {
      text: Strings.dontShowItAgain
      font.weight: Font.DemiBold
      Layout.fillWidth: true
    }
    UIKit.SwitchButton {
    }
  }

  UIKit.StyledButton {
    id: button
    text: Strings.okGotIt
    Layout.topMargin: ThemeController.style.margin.m20
    Layout.fillWidth: true
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Primary
    onClicked: homeManager.back()
  }
}
