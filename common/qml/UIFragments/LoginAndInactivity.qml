import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

import com.testintl 1.0

ColumnLayout {
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m24

  property int pageMode: Enums.GlobalSettings
  property bool globalMode: pageMode === Enums.GlobalSettings
  property bool isAO: AuthenticationManager.currentAccount.accountOwner

  UIKit.BaseText {
    text: Strings.loginAndInactivity
    visible: !app.isMobile
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
  }

  QuodArcaSettings {
    id: loginSectionSettings
    preferencesType: {
      if (pageMode === Enums.GlobalSettings) {
        return SettingsManager.Account;
      }
      else if (pageMode === Enums.UserSettings) {
        return SettingsManager.User
      }
      else if (pageMode === Enums.DeviceSettings) {
        return SettingsManager.Device
      }
    }
    category: "login"

    property string maximum_login_attempts
    property string use_authenticator_app
    property string force_passphrase_reset
    property string lock_inactive
    property string logout_inactive
    property string lock_app_on_exit
    property string unlock_app_using
  }

  QuodArcaSettings {
    id: deviceSectionSettings
    preferencesType: {
      if (pageMode === Enums.GlobalSettings) {
        return SettingsManager.Account;
      }
      else if (pageMode === Enums.UserSettings) {
        return SettingsManager.User
      }
      else if (pageMode === Enums.DeviceSettings) {
        return SettingsManager.Device
      }
    }
    category: "device"

  }

  QuodArcaSettings {
    id: dataSectionSettings
    preferencesType: {
      if (pageMode === Enums.GlobalSettings) {
        return SettingsManager.Account;
      }
      else if (pageMode === Enums.UserSettings) {
        return SettingsManager.User
      }
      else if (pageMode === Enums.DeviceSettings) {
        return SettingsManager.Device
      }
    }
    category: "data"

  }

  Flickable {
    id: flick
    Layout.fillHeight: true
    Layout.fillWidth: true
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: content.height
    clip: true

    ColumnLayout {
      id: content
      spacing: ThemeController.style.margin.m12
      width: parent.width

      UIKit.BaseText {
        text: Strings.maximumWrongAttempts
        font.weight: Font.Bold
        visible: globalMode
      }

      ButtonGroup {
        id: loginAttemptsGroup
        buttons: loginAttempts.children
        exclusive: true
      }

      Column {
        id: loginAttempts
        spacing: ThemeController.style.margin.m24
        Layout.topMargin: ThemeController.style.margin.m12
        visible: globalMode

        UIKit.StyledRadioButton {
          checked: loginSectionSettings.maximum_login_attempts === text
          text: "3"
          enabled: !globalMode || isAO
        }

        UIKit.StyledRadioButton {
          checked: loginSectionSettings.maximum_login_attempts === text
          text: "5"
          enabled: !globalMode || isAO
        }

        UIKit.StyledRadioButton {
          checked: loginSectionSettings.maximum_login_attempts === text
          text: "10"
          enabled: !globalMode || isAO
        }

        UIKit.StyledRadioButton {
          checked: loginSectionSettings.maximum_login_attempts === text
          text: "15"
          enabled: !globalMode || isAO
        }
      }

      Rectangle {
        Layout.topMargin: ThemeController.style.homePageVerticalMargin
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
        visible: globalMode
      }

      UIKit.BaseText {
        Layout.topMargin: ThemeController.style.margin.m12
        text: Strings.loginWithAuthenticator
        font.weight: Font.Bold
        visible: globalMode
      }

      ButtonGroup {
        id: loginAuthenticatorGroup
        buttons: loginAuthenticator.children
        exclusive: true
      }

      Column {
        id: loginAuthenticator
        spacing: ThemeController.style.margin.m24
        Layout.topMargin: ThemeController.style.margin.m12
        visible: globalMode

        UIKit.StyledRadioButton {
          checked: true
          text: Strings.yes
          enabled: !globalMode || isAO
        }

        UIKit.StyledRadioButton {
          checked: loginSectionSettings.use_authenticator_app === "user_choice"
          text: Strings.allowUsersToChoose
          enabled: !globalMode || isAO
        }
      }

      Rectangle {
        Layout.topMargin: ThemeController.style.homePageVerticalMargin
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
        visible: globalMode
      }

      UIKit.BaseText {
        text: Strings.passphraseResetPeriod
        font.weight: Font.Bold
        visible: globalMode
      }

      ButtonGroup {
        id: resetPassphraseGroup
        buttons: resetPassphrase.children
        exclusive: true
      }

      Column {
        id: resetPassphrase
        Layout.topMargin: ThemeController.style.margin.m12
        spacing: ThemeController.style.margin.m24
        visible: globalMode

        UIKit.StyledRadioButton {
          checked: loginSectionSettings.force_passphrase_reset === "1"
          text: Strings.month.arg("1")
          enabled: !globalMode || isAO
        }

        UIKit.StyledRadioButton {
          checked: loginSectionSettings.force_passphrase_reset === "2"
          text: Strings.months.arg("2")
          enabled: !globalMode || isAO
        }
        UIKit.StyledRadioButton {
          checked: loginSectionSettings.force_passphrase_reset === "3"
          text: Strings.months.arg("3")
          enabled: !globalMode || isAO
        }

        UIKit.StyledRadioButton {
          checked: loginSectionSettings.force_passphrase_reset === "0"
          text: Strings.never
          enabled: !globalMode || isAO
        }
      }

      Rectangle {
        Layout.topMargin: ThemeController.style.homePageVerticalMargin
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
        visible: globalMode
      }

      UIKit.SwitchItem {
        text: Strings.loginWithAuthenticator
        showSeparator: true
        visible: !globalMode
        separatorMargin: app.isMobile ? ThemeController.style.margin.m8 : ThemeController.style.margin.m16
        switchButton.checked: loginSectionSettings.loginSectionSettings === "on"
      }

      UIKit.PickerDelegate {
        Layout.topMargin: app.isMobile ? ThemeController.style.margin.m4 :
                                         ThemeController.style.margin.m12
        labelText: Strings.lockAfter
        pickerTitle: Strings.lockAfter
        inputRequired: false
        placeholderTxt: Strings.userChoice
        leftEyeIcon: false
        enabled: !globalMode || isAO
        objectsModel: [
          { text: Strings.minutes.arg("2"), value: "2" },
          { text: Strings.minutes.arg("5"), value: "5" },
          { text: Strings.minutes.arg("10"), value: "10" },
          { text: Strings.minutes.arg("15"), value: "15" },
          { text: Strings.minutes.arg("30"), value: "30" },
          { text: Strings.userChoice, value: "0"}
        ]
        comboBox.textRole: "text"
        comboBox.valueRole: "value"
        comboBox.currentIndex: comboBox.indexOfValue(loginSectionSettings.lock_inactive)
      }

      UIKit.PickerDelegate {
        Layout.topMargin: ThemeController.style.margin.m4
        visible: globalMode
        labelText: Strings.logoutAfter
        pickerTitle: Strings.logoutAfter
        inputRequired: false
        placeholderTxt: Strings.userChoice
        leftEyeIcon: false
        enabled: !globalMode || isAO
        objectsModel: [
          { text: Strings.day.arg("1"), value: "1" },
          { text: Strings.days.arg("2"), value: "2" },
          { text: Strings.week.arg("1"), value: "7" },
          { text: Strings.weeks.arg("2"), value: "14" },
          { text: Strings.weeks.arg("4"), value: "28" }
        ]
        comboBox.textRole: "text"
        comboBox.valueRole: "value"
        comboBox.currentIndex: comboBox.indexOfValue(loginSectionSettings.logout_inactive)
      }

      Rectangle {
        Layout.topMargin: app.isMobile ? ThemeController.style.margin.m4 : ThemeController.style.margin.m12
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: 1
        visible: !globalMode
      }

      UIKit.SwitchItem {
        Layout.topMargin: app.isMobile ? ThemeController.style.margin.m4 : ThemeController.style.margin.m12
        text: Strings.lockAppOnExit
        showSeparator: true
        visible: !globalMode
        separatorMargin: app.isMobile ? ThemeController.style.margin.m8 : ThemeController.style.margin.m16
        switchButton.checked: loginSectionSettings.lock_app_on_exit === "on"
      }

      ButtonGroup {
        id: unlockAppGroup
        buttons: unlockAppColumn.children
        exclusive: true
      }

      UIKit.BaseText {
        Layout.topMargin: app.isMobile ? ThemeController.style.margin.m4 : ThemeController.style.margin.m12
        text: Strings.unlockAppUsing
        font.weight: Font.Bold
        visible: !globalMode
      }

      Column {
        id: unlockAppColumn
        visible: !globalMode
        Layout.topMargin: ThemeController.style.margin.m12
        spacing: ThemeController.style.margin.m24

        UIKit.StyledRadioButton {
          checked: loginSectionSettings.unlock_app_using === "pin"
          text: Strings.pin
        }

        UIKit.StyledRadioButton {
          checked: loginSectionSettings.unlock_app_using === "pattern"
          text: Strings.pattern
        }
        UIKit.StyledRadioButton {
          checked: loginSectionSettings.unlock_app_using === "biometric"
          text: Strings.biometric
        }
      }

      Item {
        Layout.fillWidth: true
        Layout.preferredHeight: s(25)
      }
    }
  }
}
