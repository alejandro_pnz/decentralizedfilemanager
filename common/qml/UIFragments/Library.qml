import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import QtQml.Models 2.12

Item {
  id: root
  anchors.fill: parent
  property int horizontalMargins: 0
  property int horizontalSpacing: 0
  property int verticalSpacing: 0
  property int arcaHeight: s(183)
  property int minimumArcaWidth: s(168)
  property int maximumArcaWidth: s(232)

  property bool selectionMode: stackLayout.children[stackLayout.currentIndex].selectionMode
  property ItemSelectionModel ism: stackLayout.children[stackLayout.currentIndex].ism
  property string objectTypeName: stackLayout.children[stackLayout.currentIndex].objectTypeName
  property int objectType: stackLayout.children[stackLayout.currentIndex].objectType
  property alias stackLayout: stackLayout

  TabBar {
    id: bar
    anchors.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m12
    anchors.top: parent.top
    anchors.left: parent.left
    spacing: ThemeController.style.margin.m12
    visible: app.isPhone ? !selectionMode : true
    height: app.isPhone && selectionMode ? 0 : implicitHeight
    UIKit.StyledTabButton {
      id: allButton
      text: Strings.all
      icon.source: "qrc:/all.svg"
    }
    UIKit.StyledTabButton {
      id: arcaButton
      text: Strings.arca
      icon.source: "qrc:/arca.svg"
    }
    UIKit.StyledTabButton {
      id: quodButton
      text: Strings.quod
      icon.source: "qrc:/quods.svg"
    }
    UIKit.StyledTabButton {
      id: filesButton
      text: Strings.files
      icon.source: "qrc:/files.svg"
    }
  }

  Rectangle {
    id: spacer
    visible: app.isPhone ? selectionMode : true
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.leftMargin: -horizontalMargins
    anchors.rightMargin: -horizontalMargins
    anchors.top: bar.bottom
    anchors.topMargin: app.isPhone ? -ThemeController.style.margin.m16 :
                                   ThemeController.style.margin.m12
    color: ThemeController.style.mercuryColor
    height: s(1)
  }

  NoItemsInfo {
    id: noItemsColumn
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.top: app.isPhone ? bar.bottom : spacer.bottom
    title: stackLayout.children[stackLayout.currentIndex].noItemTitle
    text: stackLayout.children[stackLayout.currentIndex].noItemDescription
    iconSource: stackLayout.children[stackLayout.currentIndex].noItemIcon
    topMargin: app.isPhone ? s(88) : s(140)
  }

  StackLayout {
    id: stackLayout
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.top: app.isPhone ? bar.bottom : spacer.bottom
    anchors.topMargin: ThemeController.style.margin.m24
    currentIndex: bar.currentIndex
    AllTab {
      id: allTab
    }
    ArcaTab {
      id: arcaTab
      horizontalSpacing: root.horizontalSpacing
      verticalSpacing: root.verticalSpacing
      delegateHeight: root.arcaHeight
      delegateMinimumWidth: root.minimumArcaWidth
      delegateMaximumWidth: root.maximumArcaWidth
    }
    QuodTab {
      id: quodTab
    }
    FilesTab {
      id: filesTab
    }

    onCurrentIndexChanged: {
      homeManager.setCurrentLibraryTab(currentIndex)
    }
  }

  Connections {
    target: homeManager
    function onSetupLibraryPage(tab, sorter) {
      bar.currentIndex = tab
      // Set custom sorting
      if (tab === 0) {
        allTab.changeSorter(sorter);
      }
      else if (tab === 1) {
        arcaTab.changeSorter(sorter)
      }
      else if (tab === 2) {
        quodTab.changeSorter(sorter)
      }
      else if (tab === 3) {
        filesTab.changeSorter(sorter)
      }
    }
    function onClearLibrarySelection() {
      root.ism.clearSelection()
    }
  }

  Component.onCompleted: console.log(state, stackLayout.children[stackLayout.currentIndex].noItem)
  states: [
    State {
      name: "empty"
      when: stackLayout.children[stackLayout.currentIndex].noItem
      PropertyChanges {
        target: noItemsColumn
        visible: true
      }
      PropertyChanges {
        target: stackLayout
        visible: false
      }
    },
    State {
      name: "normal"
      when: !stackLayout.children[stackLayout.currentIndex].noItem
      PropertyChanges {
        target: noItemsColumn
        visible: false
      }
      PropertyChanges {
        target: stackLayout
        visible: true
      }
    }
  ]
}
