import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import QtQml.Models 2.12

Item {
  id: tab

  property string noItemTitle: ""
  property string noItemDescription: ""
  property string noItemIcon: ""
  property bool noItem: true
  readonly property bool isMobile: app.isMobile
  property string objectTypeName: ""
  property int objectType: -1

  signal changeSorter(var sorter)

  property bool selectionMode: ism.hasSelection
  property alias ism: ism
  ItemSelectionModel {
    id: ism
    property bool pinnedSelected: false
    property bool unpinnedSelected: false
  }

  Component.onCompleted: {
    if (objectTypeName.length < 1)
      console.warn("No object type name set in Library Tab!")
    if (objectType < 0)
      console.warn("No object type set in Library Tab!")
  }
}
