import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  spacing: ThemeController.style.margin.m16
  property bool error: false
  Rectangle {
    Layout.preferredHeight: app.isPhone ? s(205) : s(230)
    Layout.preferredWidth: parent.width
    radius: s(10)
    border.width: s(3)
    border.color: ThemeController.style.slatePurpleColor
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.BaseText {
    Layout.topMargin: ThemeController.style.margin.m24
    Layout.fillWidth: true
    text: Strings.goodNews
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.BaseText {
    text: Strings.resetLoginPassphrase
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.InputField {
    id: passphraseInput
    Layout.topMargin: ThemeController.style.margin.m8
    labelText: Strings.newPassphrase
    placeholderTxt: Strings.examplePassphrase
    inputRequired: true
    isPassPhrase: true
    eyeIconVisible: true
    onBacktabPressed: repeatPassphraseInput.textField.forceActiveFocus()
    onTabPressed: repeatPassphraseInput.textField.forceActiveFocus()
    onEnterPressed: repeatPassphraseInput.textField.forceActiveFocus()
  }

  UIKit.InputField {
    id: repeatPassphraseInput
    labelText: Strings.repeatNewPassphrase
    placeholderTxt: Strings.examplePassphrase
    inputRequired: true
    isPassPhrase: true
    eyeIconVisible: true
    onBacktabPressed: passphraseInput.textField.forceActiveFocus()
    onTabPressed: passphraseInput.textField.forceActiveFocus()
    onEnterPressed: resetButton.clicked()
  }

  UIKit.StyledButton {
    id: resetButton
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    text: Strings.resetPassphrase
    textItem.visible: !pulseLoader.visible
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      if (validatePassphraseInput(passphraseInput, repeatPassphraseInput)) {
        RegistrationManager.resetPassphrase(passphraseInput.textField.text)
        passphraseInput.textField.text = ""
        repeatPassphraseInput.textField.text = ""
      }
    }

    UIKit.PulseLoader {
      id: pulseLoader
      anchors.centerIn: parent
      visible: RegistrationManager.unblocking
    }
  }
}
