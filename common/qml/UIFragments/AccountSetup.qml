import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
    id: layout
    spacing: ThemeController.style.margin.m16

    signal buttonClicked(string name)

    Connections {
      target: pageManager
      function onSetupAccountSetupPage(config) {
          layout.buttonClicked.connect(config.buttonHandler)
        if (config.email !== undefined) {
          loginEmailDelegate.email = config.email
        }
      }
    }
    Rectangle {
        Layout.preferredHeight: app.isMobile ? s(222) : s(230)
        Layout.fillWidth: true
        radius: s(10)
        Layout.alignment: Qt.AlignHCenter

        Image {
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source: "qrc:/illustrations/r11-personal-subsciption.png"
            mipmap: true
        }
    }

    UIKit.BaseText {
        Layout.topMargin: ThemeController.style.margin.m24
        Layout.fillWidth: true
        text: Strings.accountSetup
        font.weight: Font.Bold
        size: UIKit.BaseText.TextSize.H2
        Layout.alignment: Qt.AlignHCenter
        horizontalAlignment: Qt.AlignHCenter
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }

    UIKit.LoginEmailDelegate {
        id: loginEmailDelegate
        Layout.topMargin: ThemeController.style.margin.m8
        Layout.fillWidth: true
    }

    UIKit.InputField {
        id: accountNameInput
        labelText: Strings.accountName
        placeholderTxt: Strings.typeAccountName
        textField.text: loginEmailDelegate.email.substring(0, loginEmailDelegate.email.indexOf("@"))
        inputRequired: true
        onTabPressed: nextButton.forceActiveFocus()
        onEnterPressed: nextButton.forceActiveFocus()
        onBacktabPressed: nextButton.forceActiveFocus();
    }

    UIKit.StyledButton {
        id: nextButton
        Layout.topMargin: ThemeController.style.margin.m8
        Layout.fillWidth: true
        text: Strings.next
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        onClicked: {
          if (validateInput(accountNameInput))
              buttonClicked(accountNameInput.textField.text)
        }
    }
}
