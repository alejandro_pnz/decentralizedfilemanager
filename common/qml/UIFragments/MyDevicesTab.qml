import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import UIFragments 1.0 as UIFragments
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2
import QtQml.Models 2.12
import UIModel 1.0

ColumnLayout {
    spacing: ThemeController.style.homePageVerticalMargin
    width: parent.width
    height: parent.height
    property bool selectionMode: ism.hasSelection
    property ItemSelectionModel ism: selectionModel
    property string objectTypeName: Strings.devices
    property int objectType: Enums.Device

    ListModel {
        id: mockedDeviceModel
        ListElement {email: "a@b.c"; online: false; pending: true; active: true; blocked: false; masterDevice: true; powerDevice: true; syncing: false; deviceType: 1; deviceModel: "mi"; deviceOS: "Vedroid"; ipAddress: "127.0.0.1"; countryCode: "RU"; city: "PNZ"}
        ListElement {email: "a@b.c"; online: true; pending: false; active: true; blocked: false; masterDevice: true; powerDevice: true; syncing: false; deviceType: 1; deviceModel: "mi1"; deviceOS: "Vedroid"; ipAddress: "127.0.0.1"; countryCode: "RU"; city: "PNZ"}
//        ListElement {email: "a@b.c"; online: true; pending: false; active: false; blocked: false; masterDevice: true; powerDevice: true; syncing: false; deviceType: 1; deviceModel: "mi2"; deviceOS: "Vedroid"; ipAddress: "127.0.0.1"; countryCode: "RU"; city: "PNZ"}
//        ListElement {email: "a@b.c"; online: true; pending: false; active: true; blocked: true; masterDevice: true; powerDevice: true; syncing: false; deviceType: 1; deviceModel: "mi3"; deviceOS: "Vedroid"; ipAddress: "127.0.0.1"; countryCode: "RU"; city: "PNZ"}
//        ListElement {email: "a@b.c"; online: true; pending: false; active: true; blocked: false; masterDevice: true; powerDevice: false; syncing: true; deviceType: 1; deviceModel: "mi4"; deviceOS: "Vedroid"; ipAddress: "127.0.0.1"; countryCode: "RU"; city: "PNZ"}
//        ListElement {email: "a@b.c"; online: true; pending: false; active: true; blocked: false; masterDevice: false; powerDevice: true; syncing: false; deviceType: 1; deviceModel: "mi5"; deviceOS: "Vedroid"; ipAddress: "127.0.0.1"; countryCode: "RU"; city: "PNZ"}
//        ListElement {email: "a@b.c"; online: true; pending: false; active: true; blocked: false; masterDevice: true; powerDevice: true; syncing: false; deviceType: 1; deviceModel: "mi6"; deviceOS: "Vedroid"; ipAddress: "127.0.0.1"; countryCode: "RU"; city: "PNZ"}
    }

    Connections {
        target: homeManager
        function onCannotAddMoreDevices() {
            devicesCountError.visible = true;
        }
    }

    ItemSelectionModel {
        id: selectionModel
        model: devicesModel
    }

    SortFilterProxyModel {
        id: devicesModel
//        sourceModel: DeviceListModel
        sourceModel: mockedDeviceModel
    }

    SortFilterProxyModel {
      id: objectsProxyModel
      sourceModel: devicesModel
    }
    RowLayout {
        id: devicesCountError
        Layout.fillWidth: true
        visible: false
        spacing: ThemeController.style.margin.m48
        UIKit.Errorbox {
            errorMsg: RegistrationManager.accountOwner ? Strings.cannotAddMoreDevicesForAccountOwner : Strings.cannotAddMoreDevices
            Layout.fillWidth: true
        }
        UIKit.StyledButton {
            visible: RegistrationManager.accountOwner
            text: Strings.upgradePlan
            type: UIKit.StyledButton.ButtonStyle.Text
            displayMode: UIKit.StyledButton.DisplayMode.TextOnly
            onClicked: {
                RegistrationManager.openLink("https://website-staging.test.com/webview/my-subscriptions.html")
            }
        }
    }

    AddedDevicesDetails {
      Layout.fillWidth: true
      Layout.alignment: parent
    }

    Rectangle {
      height: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
    }

    UIKit.ResponsiveGridLayout {
        id: listView
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: parent

        Layout.rightMargin: isMobile ? 0 : -ThemeController.style.margin.m32

        horizontalSpacing: isMobile ? ThemeController.style.margin.m8 : ThemeController.style.margin.m32
        verticalSpacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m24

        delegateHeight: isMobile ? s(183) : s(184)
        delegateMinimumWidth: s(168)
        delegateMaximumWidth: s(232)

        model: objectsProxyModel
        visible: listView.count > 0
        delegate: Item {
            id: delegate
            width: listView.cellWidth
            height: listView.cellHeight

            property int deviceGridItemIndex: index

            UIKit.DeviceDelegateSquare {
                // Model data

                // TODO: increase width of elements on Android (add "contentWidth property")
                anchors {
                    top: parent.top
                    left: getLeftAnchor()
                    right: getRightAnchor()
                }

                deviceName: model.deviceModel
                avatar.deviceType: model.deviceType
                avatar.blocked: model.blocked
                avatar.inactive: !model.active
                online: model.online
                deviceOSAndVersion: model.deviceOS + " Version"//model.deviceOSVesion
                syncing: model.syncing
                deleted: false//model.deleted
                outOfSync: model.outOfSync
                pending: model.pending//index === 3 for test

                // Other properties
                width: listView.maximumCellContentWidth
                height: listView.cellHeight - listView.verticalSpacing

                actionButton: !ism.hasSelection
                objectsModel: listView.model

                checkable: !isMobile || listView.selectionMode
                selectionMode: listView.selectionMode
                selectionModel: ism

                onDelegateClicked: {
                    homeManager.openOneDevicePage(model.object.id);
                }

                onActionButtonClicked: {
                    listView.currentIndex = index
                    actionMenu.model.currentObject = model.object
                    actionMenu.pending = model.pending//index === 3 for test
                    actionMenu.handleActionButton(listView)
                }

                function getLeftAnchor() {
                    if (isMobile) {
                        var calc = parent.deviceGridItemIndex % listView.cellCountInRow
                        if (calc === 0) {
                            return parent.left
                        } else {
                            return undefined
                        }
                    } else {
                        return parent.left
                    }
                }

                function getRightAnchor() {
                    if (isMobile) {
                        var calc = (parent.deviceGridItemIndex + 1) % listView.cellCountInRow
                        if (calc === 0) {
                            return parent.right
                        } else {
                            return undefined
                        }
                    } else {
                        return undefined
                    }
                }
            }
        }
    }

    function selectCurrent() {
        if (actionMenu.popup.opened && actionMenu.popup.currentItem) {
            actionMenu.popup.currentItem.check()
        } else {
            listView.itemAtIndex(0).check()
        }
    }

    OneDeviceActionMenu {
        id: actionMenu
        Connections {
            target: actionMenu.model
            function onSelectCurrent() {
                fragment.selectCurrent()
            }
        }
    }
}
