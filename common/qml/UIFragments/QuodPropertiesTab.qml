import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import com.testintl 1.0

Flickable {
  id: flickable
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true
  readonly property bool isMobile: app.isPhone
  //properties for tests
  property string uid: "TODO"
  property string lastModifiedDate: Utility.formatDateTime(currentEditor.object.modified)
  property bool editMode: false

  readonly property var currentEditor: homeManager.currentEditor

  EditGroup {
    id: editGroup
  }

  ColumnLayout {
    id: content
    spacing: ThemeController.style.margin.m16
    width: parent.width

    readonly property bool isMobile: app.isPhone

    NameAndOwnerPanel {
      id: ownerPanel
      Layout.fillWidth: true
      Layout.preferredHeight: height
      nameInput.labelText: Strings.quodName
      nameInput.placeholderTxt: Strings.typeQuodName
      hasBottomSpacer: true
    }

    TagPanel {
      model: currentEditor.taglistModel
      Layout.preferredHeight: height
      Layout.topMargin: 0
      Layout.fillWidth: true
    }

    SyncPanel {
      id: syncDelegate
      Layout.fillWidth: true
      Layout.preferredHeight: height
      syncStatus: quodEditor.object.syncProperty
      // hasWhitelist: true TODO
    }

    Rectangle {
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
      visible: isMobile
    }

    UIKit.ObjectInformationPanel {
      uid: flickable.uid
      lastModifiedDate: flickable.lastModifiedDate
      ownerName: ownerPanel.ownerName
      visible: isMobile
      Layout.alignment: Qt.AlignHCenter
    }

    Item {
      Layout.fillWidth: true
      Layout.preferredHeight: isMobile ? s(1) : s(24)
    }
  }
}
