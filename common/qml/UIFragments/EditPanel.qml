import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import com.test 1.0
import AppStyles 1.0

EditablePanel {
  id: panel

  enable: !app.isPhone
  height: visible ? column.height : 0
  property string defaultModeText: Strings.edit
  property bool hasBottomSpacer: false
  default property alias children: content.children
  property alias contentLayout: content
  property alias menu: menu
  property alias titleComponent: t
  property alias menuChildren: additionalHeaderContent.children
  property alias editButton: editButton
  property var validatorMethod: function validate() {
    return true
  }

  property bool onlyMenu: false

  ColumnLayout {
    id: column
    anchors {
      left: parent.left
      right: parent.right
    }

    spacing: ThemeController.style.margin.m16

    Rectangle {
      id: line
      visible: hasSpacer
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
    }

    RowLayout {
      id: menu
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.bottomMargin: onlyMenu ? ThemeController.style.margin.m8 : 0
      Layout.fillWidth: true
      Layout.preferredHeight: ThemeController.style.margin.m24
      BaseText {
        id: t
        font.weight: Font.Bold
        text: title
        Layout.alignment: Qt.AlignLeft
        visible: text.length > 0
      }

      Item {
        id: sp
        Layout.preferredHeight: s(1)
        Layout.fillWidth: true
      }

      StyledButton {
        id: editButton
        text: editMode ? Strings.cancel : (hasAdd ? Strings.add : Strings.edit)
        type: StyledButton.ButtonStyle.Text
        displayMode: StyledButton.DisplayMode.TextOnly
        Layout.preferredHeight: ThemeController.style.margin.m24
        Layout.alignment: Qt.AlignRight
        visible: hasEdit || hasAdd
        onClicked: {
          if (editMode) {
            panel.finishEditing()
            panel.cancelButtonClicked()
          } else {
            panel.edit()
            if (hasAdd) {
              panel.addButtonClicked()
            } else {
              panel.editButtonClicked()
            }
          }
        }
        enabled: canEdit
      }

      RowLayout {
        id: additionalHeaderContent
        Layout.preferredHeight: ThemeController.style.margin.m24
        visible: children.length > 0
      }
    }

    ColumnLayout {
      spacing: 0
      id: content
      Layout.fillWidth: true
      visible: !onlyMenu
    }

    StyledButton {
      id: button
      Layout.topMargin: ThemeController.style.margin.m8
      text: Strings.done
      displayMode: StyledButton.DisplayMode.TextOnly
      onClicked: {
        if (validatorMethod()) {
          panel.finishEditing()
          saveButtonClicked()
        }
      }
      visible: hasSave && !app.isPhone && editMode
    }

    Rectangle {
      id: bottomLine
      visible: hasBottomSpacer
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      Layout.topMargin: app.isPhone ? 0 : ThemeController.style.margin.m8
      color: ThemeController.style.seashellColor
    }
  }
}
