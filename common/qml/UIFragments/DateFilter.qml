import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: content
  spacing: ThemeController.style.margin.m12
  width: parent.width
  height: parent.height

  property var searchModel
  property string firstDateString: ""
  property string secondDateString: ""
  property var firstDate
  property var secondDate
  property int dateFilterType: -1

  // Signal to store the current selection state before user do some changes
  function init() {
  }

  // Reverts all changes made by user
  function revertChanges() {
    cleanup()
  }

  // Clears the selection
  function clearAll() {
    cleanup()
  }

  // Applies filtering
  function applyFilters() {
//    searchModel.setDateFilter(dateFilterType, startDate, endDate);
    searchModel.filters.filter(dateFilterType).startDate = firstDate;
    searchModel.filters.filter(dateFilterType).startDate = secondDate;
    searchModel.updateFilters();
    cleanup()
  }

  function cleanup() {
    firstDateString = ""
    secondDateString = ""
    firstDate = null
    secondDate = null
  }

  RowLayout {
    spacing: s(10)

    UIKit.InputField {
      id: startDate
      inputRequired: false
      eyeIconVisible: false
      clearIconVisible: false
      isPassPhrase: false
      Layout.fillWidth: true
      implicitWidth: 0
      textField.text: firstDateString
      textField.readOnly: true
      textField.color: ThemeController.style.textInput.textColor
      onTabPressed: endDate.textField.forceActiveFocus()
      onEnterPressed: endDate.textField.forceActiveFocus()
      onBacktabPressed: endDate.textField.forceActiveFocus()
    }

    Rectangle {
      Layout.preferredHeight: s(2)
      Layout.preferredWidth: s(12)
      color: ThemeController.style.mercuryColor
      radius: s(100)
    }

    UIKit.InputField {
      id: endDate
      inputRequired: false
      eyeIconVisible: false
      clearIconVisible: false
      isPassPhrase: false
      Layout.fillWidth: true
      implicitWidth: 0
      textField.text: secondDateString
      textField.readOnly: true
      textField.color: ThemeController.style.textInput.textColor
      onTabPressed: startDate.textField.forceActiveFocus()
      onEnterPressed: startDate.textField.forceActiveFocus()
      onBacktabPressed: startDate.textField.forceActiveFocus()
    }
  }

  UIKit.Calendar {
    id: calendar

    onDateChoosen: {
      // TODO this logic is kinda strange...
      if(firstDateString === "" && secondDateString === ""){
        firstDate = d
        secondDate = d
        firstDateString = d.toLocaleString(locale, "dd/MM/yyyy")
        secondDateString = d.toLocaleString(locale, "dd/MM/yyyy")
      }
      else if(secondDate === firstDate) {
        secondDate = d
        secondDateString = d.toLocaleString(locale, "dd/MM/yyyy")
      }
    }
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
