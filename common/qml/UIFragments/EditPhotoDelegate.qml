import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import QtQml 2.15

import com.testintl 1.0

Item {
  id: root
  property bool editMode: false
  property string photoName: ""
  property string photoSource: ""
  property int previewWidth: s(app.isPhone ? 167 : 181)
  property string typedName: app.isPhone ? mobileInput.textField.text : desktopInput.textField.text
  signal textChanged(string text)
  signal deleteRequest()
  signal retakeRequest()
  signal actionButtonClicked()
  implicitHeight: column.height
  implicitWidth: previewWidth
  property bool hasErrors: mobileInput.visible ? mobileInput.hasErrors : desktopInput.hasErrors

  signal tabPressed()
  signal backtabPressed()
  signal enterPressed()
  signal inputActivated(var globalPos, var height)

  function activateInput() {
    if (mobileInput.visible) mobileInput.textField.forceActiveFocus()
    else desktopInput.textField.forceActiveFocus()
  }

  function validate() {
    console.log("VALIDATE", desktopInput.textField.text)
    if (mobileInput.visible) {
      return validateMandatoryField(mobileInput)
    } else {
      var va = validateMandatoryField(desktopInput)
      console.log("AFTER VALIDATION", va)
      return
    }
  }

  onVisibleChanged: {
    if (visible) {
      mobileInput.textField.text = photoName
      desktopInput.textField.text = photoName
    }
  }

  Column {
    id: column
    anchors.left: parent.left
    anchors.right: parent.right
    spacing: ThemeController.style.margin.m16    

    UIKit.InputField {
      id: mobileInput
      visible: editMode && app.isPhone
      width: parent.width

      labelText: Strings.photoName
      placeholderTxt: Strings.photoNamePlaceholder
      textField.text: photoName
      textField.onTextChanged: {
        if (visible) root.textChanged(textField.text)
      }
      inputRequired: false
      leftEyeIcon: false
      clearIconVisible: true
      onEnterPressed: {
        textField.focus = false
        root.tabPressed()
      }
      onBacktabPressed: root.backtabPressed()
      onTabPressed: root.tabPressed()
      textField.onActiveFocusChanged: {
        if (textField.activeFocus) inputActivated(mobileInput.mapToGlobal(0, mobileInput.height), mobileInput.height)
      }
    }

    UIKit.ImageListDelegate {
      width: parent.width
      height: width * 0.75
      showMenu: !editMode
      icon.source: photoSource
      text: photoName
      onActionButtonClicked: root.actionButtonClicked()
      MouseArea {
        id: ma
        anchors.fill: parent
        hoverEnabled: true
        visible: editMode
      }
      UIKit.StyledButton {
        id: addButton
        icon.source: "qrc:/delete.svg"
        displayMode: UIKit.StyledButton.DisplayMode.IconOnly
        type: UIKit.StyledButton.ButtonStyle.Secondary
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.topMargin: ThemeController.style.margin.m16
        anchors.rightMargin: ThemeController.style.margin.m16
        opacity: {
          if (app.isTablet) return editMode
          else editMode && (ma.containsMouse || addButton.hovered) ? 1.0 : 0
        }
        visible: !app.isPhone
        enabled: opacity == 1.0
        onClicked: deleteRequest()
      }
    }

    UIKit.InputField {
      id: desktopInput
      visible: editMode && !app.isPhone
      width: parent.width
      textField.onTextChanged: {
        if (visible) root.textChanged(textField.text)
      }
      labelText: Strings.photoName
      placeholderTxt: Strings.photoNamePlaceholder
      textField.text: photoName
      inputRequired: false
      leftEyeIcon: false
      clearIconVisible: true
      onBacktabPressed: root.backtabPressed()
      onTabPressed: root.tabPressed()
      onEnterPressed: root.tabPressed()
      textField.onActiveFocusChanged: {
        if (textField.activeFocus) inputActivated(desktopInput.mapToGlobal(0, desktopInput.height), desktopInput.height)
      }
    }

    Rectangle {
      height: ThemeController.style.defaultInputHeight
      width: parent.width
      radius: ThemeController.style.margin.m4
      border.width: s(1)
      border.color: ThemeController.style.seashellColor
      color: ThemeController.style.whiteSmokeColor
      visible: editMode && app.isMobile

      RowLayout {
        anchors.fill: parent
        anchors.margins: s(12)

        UIKit.StyledButton {
          type: UIKit.StyledButton.ButtonStyle.Text
          displayMode: UIKit.StyledButton.DisplayMode.TextRightSideIcon
          text: Strings.retake
          buttonStyle: ThemeController.style.photoButton
          icon.source: "qrc:/camera.svg"
          onClicked: retakeRequest()
          Layout.fillWidth: true
          Layout.fillHeight: true
        }
        Rectangle {
          implicitWidth: s(1)
          Layout.fillHeight: true
          color: ThemeController.style.seashellColor
        }

        UIKit.StyledButton {
          type: UIKit.StyledButton.ButtonStyle.Text
          displayMode: UIKit.StyledButton.DisplayMode.TextRightSideIcon
          text: Strings.remove
          buttonStyle: ThemeController.style.photoButton
          icon.source: "qrc:/delete.svg"
          onClicked: deleteRequest()
          Layout.fillWidth: true
          Layout.fillHeight: true
        }
      }
    }
  }
}
