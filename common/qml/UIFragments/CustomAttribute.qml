import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

Flickable {
  id: flick
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true
  property alias attributeName: nameInput.textField

  function clear() {
    nameInput.textField.clear()
    formatInput.comboBox.currentIndex = -1
    textInput.textField.clear();
  }

  function save() {
    var nameOK = validateMandatoryField(nameInput)
    var textOK = false
    if (formatInput.comboBox.currentIndex === 0)
      textOK = validateMandatoryField(textInput)
    else if (formatInput.comboBox.currentIndex === 1)
      textOK = validateMandatoryField(dateInput)

    // TODO validate combobox
    if (nameOK && textOK) {
      if (formatInput.comboBox.currentIndex === 0)
        QuodTypeModel.currentType.addAttribute(nameInput.textField.text, textInput.textField.text, true)
      else if (formatInput.comboBox.currentIndex === 1)
        QuodTypeModel.currentType.addAttribute(nameInput.textField.text, dateInput.textField.text, false)
      return true
    }
    return false
  }

  ColumnLayout {
    id: content
    width: parent.width
    spacing: ThemeController.style.margin.m16

    UIKit.InputField {
      id: nameInput
      Layout.topMargin: ThemeController.style.margin.m16
      labelText: Strings.attributeName
      placeholderTxt: Strings.typeAttributeName
      inputRequired: true
      clearIconVisible: true
    }

    UIKit.AttributesPickerDelegate {
      id: formatInput
      labelText: Strings.attributeFormat
      placeholderTxt: Strings.selectType
      inputRequired: true
      pickerTitle: Strings.attributeFormat
    }

    UIKit.InputField {
      id: textInput
      labelText: Strings.attributeText
      placeholderTxt: Strings.attributeText
      inputRequired: true
      clearIconVisible: false
      eyeIconVisible: true
      visible: formatInput.comboBox.currentIndex === 0
    }

    UIKit.DateInput {
      id: dateInput
      labelText: Strings.date
      inputRequired: true
      visible: formatInput.comboBox.currentIndex === 1
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }
  }
}
