import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

EditPanel {
  title: Strings.tags
  hasSave: false
  hasSpacer: false
  hasAdd: model.count < 1
  onlyMenu: model.count < 1

  onEditButtonClicked: {
    finishEditing()
    homeManager.openPage(Pages.AddTagsPage)
  }

  onAddButtonClicked: {
    finishEditing()
    homeManager.openPage(Pages.AddTagsPage)
  }

  property alias model: listView.model

  ListView {
    id: listView
    visible: model.count > 0
    Layout.fillWidth: true
    Layout.preferredHeight: visible ? s(32) : 0
    spacing: ThemeController.style.margin.m12
    orientation: ListView.Horizontal
    delegate: UIKit.Tag {
      text: name
      tagColor: color
      // TODO pass the tag object / object id as parameter to show proper filter on tag page
      onClicked: homeManager.openPage(Pages.TagPage)
    }
  }
}
