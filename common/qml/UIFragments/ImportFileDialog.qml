import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import Qt.labs.platform 1.1

FileDialog {
    id: fileDialog
    acceptLabel: Strings.import
    folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
    fileMode: FileDialog.OpenFiles
    onAccepted: {
        console.log("IMPORT FILES", files);
        ImportManager.importFiles(files);
    }
}
