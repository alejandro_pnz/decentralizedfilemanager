import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import UIModel 1.0 as UIModel
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import QtQml.Models 2.12

import "../UIKit/Charts/PieChart"
import "../UIKit/Charts/StackedBarPropertyChart"
import "../UIKit/Charts/StackedBarStatusChart"
import "../UIKit/Charts/CustomComponents"

ColumnLayout {
  id: dataFragment

  spacing: ThemeController.style.margin.m16
  height: parent.height
  width: parent.width

  property int numberOfChartsInCurrentPage: 0
  property var pageChartsObjects

  property int horizontalMargins: 0
  property int horizontalSpacing: 0
  property int verticalSpacing: 0

  property bool extendedDialogVisible: false

  UIModel.ChartsDummyModels {
    id: dummyModels
    onTabsModelLoaded: mainLoader.sourceComponent = arcaTabComponent
  }

  TabBar {
    id: bar
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m12
    Layout.alignment: isPhone ? Qt.AlignHCenter : Qt.AlignLeft
    spacing: ThemeController.style.margin.m12
    height: ThemeController.style.margin.m40

    UIKit.StyledTabButton {
      text: Strings.arca
    }
    UIKit.StyledTabButton {
      text: Strings.quod
    }
    UIKit.StyledTabButton {
      text: Strings.files
    }

    onCurrentIndexChanged: {
      //Reset the Loader
      mainLoader.active = false
      mainLoader.active = true
      mainLoader.sourceComponent = undefined
      mainLoader.sourceComponent = arcaTabComponent
    }
  }

  Rectangle {
    id: spacer
    Layout.fillWidth: true
    Layout.leftMargin: app.isPhone ? -ThemeController.style.margin.m16 : -horizontalMargins
    Layout.rightMargin: app.isPhone ? -ThemeController.style.margin.m16 : -horizontalMargins
    color: ThemeController.style.mercuryColor
    height: s(1)
  }

  NoItemsInfo {
    id: noItemsColumn
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.topMargin: s(app.isPhone ? 88 : 140)
    visible: false
  }

  Loader {
    id: mainLoader
    Layout.fillHeight: true
    Layout.fillWidth: true
  }

  Component {
    id: arcaTabComponent

    Rectangle {
      id: parentComponent

      property var componentModel: dummyModels.windowTabsModel.get(bar.currentIndex)

      anchors.fill: parent
      property string newContentTitle: ""
      property string newComponentTitle: ""
      Component.onCompleted: {
        if(componentModel !== undefined) {
          newContentTitle = componentModel.contentTitle
          newComponentTitle = componentModel.componentTitle
        }
        var chartsModel = componentModel.charts
        numberOfChartsInCurrentPage = chartsModel !== undefined ? (chartsModel.count > 0 ? chartsModel.count : 0) : 0
        pageChartsObjects = []

        for(var i=0; i< chartsModel.count; i++) {
          var currentChart = chartsModel.get(i)

          if(currentChart.type === "status") {
            pageChartsObjects[i] = statusBarComponent.createObject(mainGrid,
                                                                   {contentTitle: newContentTitle,
                                                                     componentTitle: currentChart.componentTitle ?
                                                                                       currentChart.componentTitle : newComponentTitle,
                                                                     chartModel: currentChart.modelName,
                                                                     visible: false})
          } else if(currentChart.type === "property") {
            pageChartsObjects[i] = propertyBarComponent.createObject(mainGrid,
                                                                     {contentTitle: newContentTitle,
                                                                       chartModel: currentChart.modelName,
                                                                       visible: false})
          } else if(currentChart.type === "pie") {
            pageChartsObjects[i] = pieChartComponent.createObject(mainGrid,
                                                                  {contentTitle: newContentTitle,
                                                                    chartModel: currentChart.modelName,
                                                                    visible: false})
          }
        }
      }

      UIKit.ResponsiveGridLayout {
        id: mainGrid
        anchors {
          left: parent.left
          right: parent.right
          rightMargin: -horizontalSpacing
          top: parent.top
          topMargin: app.isPhone ? 0 : ThemeController.style.margin.m8
        }
        height: contentHeight
        horizontalSpacing: app.isMobile ? app.isTablet ? ThemeController.style.margin.m32 : ThemeController.style.margin.m8 : ThemeController.style.margin.m32
        verticalSpacing: app.isMobile ? app.isTablet ? ThemeController.style.margin.m24 : ThemeController.style.margin.m12 : ThemeController.style.margin.m24
        delegateHeight: app.isPhone ? s(180) : s(192)
        delegateMinimumWidth: s(168)
        delegateMaximumWidth: app.isPhone ? s(224) : s(192)
        model: componentModel.charts

        delegate: Item {
          width: mainGrid.cellWidth
          height: mainGrid.cellHeight

          Loader {
            id: loader
            width: mainGrid.maximumCellContentWidth
            height: mainGrid.cellHeight - mainGrid.verticalSpacing
            property string chartTitle: parentComponent.newContentTitle
            property string chartComponentTitle: componentTitle ? componentTitle : parentComponent.newComponentTitle
            property var chartDataModel: modelName
            sourceComponent: {
              if(type === "status") {
                return statusBarComponent
              }
              else if(type === "property") {
                return propertyBarComponent
              } else if(type === "pie") {
                return pieChartComponent
              }
            }
          }
        }
      }

      UIKit.WeakPasswordsDelegate {
        anchors.top: mainGrid.bottom
        anchors.topMargin: ThemeController.style.margin.m32 - mainGrid.verticalSpacing
      }
    }
  }

  Component {
    id: statusBarComponent

    StatusBarChart {
      numberOfChartsInCurrentPage: dataFragment.numberOfChartsInCurrentPage
      pageChartsObjects: dataFragment.pageChartsObjects
      isTablet: app.isTablet
      contentTitle: chartTitle
      componentTitle: chartComponentTitle
      chartModel: chartDataModel
    }
  }

  Component {
    id: propertyBarComponent

    PropertyBarChart {
      numberOfChartsInCurrentPage: dataFragment.numberOfChartsInCurrentPage
      pageChartsObjects: dataFragment.pageChartsObjects
      isTablet: app.isTablet
      contentTitle: chartTitle
      chartModel: chartDataModel
    }
  }

  Component {
    id: pieChartComponent

    AllTypesPieChart {
      numberOfChartsInCurrentPage: dataFragment.numberOfChartsInCurrentPage
      pageChartsObjects: dataFragment.pageChartsObjects
      contentTitle: chartTitle
      chartModel: chartDataModel
    }
  }
}
