import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

import com.testintl 1.0

Flickable {
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true

  property var object: null

  Connections {
    target: homeManager
    function onSetupInfoPage(id, type) {
      // TODO: use SDK api
      console.log(id, type);
      object = DeviceListModel.qmlObject(id)
    }
  }

  onObjectChanged: {
    infoModel.clear()

    if (object) {
      infoModel.append({ label: Strings.accountOwnerName, contentTxt: "-"});
      infoModel.append({ label: Strings.accountOwnerEmail, contentTxt: ""});
      infoModel.append({ label: Strings.deviceEmail, contentTxt: object.email});
      infoModel.append({ label: Strings.storedObjects, contentTxt: "130 quod: \n 25% public"});
      infoModel.append({ label: Strings.totalSize, contentTxt: "800 MB"});
      infoModel.append({ label: Strings.localStorageUsed, contentTxt: "800 MB"});
      infoModel.append({ label: Strings.operatingSystems, contentTxt: object.deviceOS});
      infoModel.append({ label: Strings.mainIPLocations, contentTxt: object.ipAddress});
      infoModel.append({ label: Strings.numberOfUsers, contentTxt: "1 user"});
    }
  }

  ScrollBar.vertical: ScrollBar {
    width: s(8)
    visible: !isMobile
  }

  ColumnLayout {
    id: content
    width: parent.width
    spacing: ThemeController.style.margin.m24

    UIKit.BaseText {
      Layout.topMargin: ThemeController.style.margin.m16
      text: Strings.icon
      color: ThemeController.style.shuttleColor
    }

    UIKit.DeviceAvatar {
      id: iconContainer
      Layout.topMargin: -ThemeController.style.margin.m12
      size: UIKit.ObjectAvatar.Size.Small
      deviceType: object ? object.deviceType : DeviceObject.Unknown
    }

    Repeater {
      id: repeater
      model: infoModel
      delegate: UIKit.InfoItem {
        labelText: label
        content: contentTxt
      }
    }

    Item {
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
    }
  }

  ListModel {
    id: infoModel
  }
}
