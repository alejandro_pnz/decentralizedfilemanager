import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2
import com.testintl 1.0

Flickable {
  id: flick
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true

  property bool anyUnsavedChanges: quodEditor.deletedPasswords.length > 0

  Component.onCompleted: {
    console.log("Password History", quodEditor.deletedPasswords.length)
  }

  function accept() {
    quodEditor.removePasswords()
    homeManager.backUntil(Pages.PasswordHistoryPage)
  }

  function reject() {
    homeManager.backUntil(Pages.PasswordHistoryPage)
  }

  function doNothing() {
    homeManager.back() // To close the popup
  }

  function save() {
    if (anyUnsavedChanges) {
      homeManager.enterGenericPopupPage({"accept": accept,
                                          "reject": reject,
                                          "positiveText": Strings.yes,
                                          "negativeText": Strings.no,
                                          "title": Strings.confirmPasswordHistory,
                                          "content": Strings.confirmPasswordHistoryContent})
    } else {
      reject()
    }
  }

  function close() {
    if (anyUnsavedChanges) {
      homeManager.enterGenericPopupPage({"accept": reject,
                                          "reject": doNothing,
                                          "positiveText": Strings.yes,
                                          "negativeText": Strings.no,
                                          "title": Strings.confirmDiscardChanges,
                                          "content": Strings.confirmDiscardChangesContent})
    } else {
      reject()
    }
  }

  BaseListModel {
    id: historyModel
    keyField: "id"
    fields: ["id", "value", "modified"]
    source: quodEditor.passwordList
    // source: TODO: AttributeHistoryList
  }

  SortFilterProxyModel {
    id: proxy
    sourceModel: historyModel
    filters: [
      ListFilter {
        roleName: "id"
        list: quodEditor.deletedPasswords
        inverted: true
      }
    ]
  }

  ColumnLayout {
    id: content
    width: parent.width
    spacing: 0

    Item {
      Layout.fillWidth: true
      Layout.minimumHeight: grid.contentHeight - grid.rowSpacing
      Layout.preferredHeight: grid.contentHeight - grid.rowSpacing
      Layout.maximumHeight: grid.contentHeight - grid.rowSpacing
      Layout.topMargin: ThemeController.style.margin.m24
      GridView {
        boundsBehavior: Flickable.StopAtBounds
        id: grid
        height: grid.contentHeight
        width: parent.width + columnSpacing
        model: proxy

        property int columnSpacing: ThemeController.style.margin.m16
        property int rowSpacing: ThemeController.style.margin.m32
        property int delegateWidth: cellWidth - columnSpacing
        property int delegateHeight: cellHeight - rowSpacing
        property bool singleColumn: cellWidth == grid.width

        signal focusNextItem(var idx)

        cellWidth: {
          if (app.isPhone) return grid.width // one column on phones
          if ((grid.width - columnSpacing) * 0.5 < s(268)) return grid.width
          return grid.width / 2
        }
        cellHeight: s(80) + rowSpacing

        delegate: Item {
          id: gridDelegate
          height: grid.cellHeight
          width: grid.cellWidth
          property int i: index

          Column {
            spacing: ThemeController.style.margin.m16
            width: grid.delegateWidth
            UIKit.RemovableInput {
              id: parentInput
              width: grid.delegateWidth
              height: grid.delegateHeight
              labelText: Utility.formatDateTime(model.modified)
              value: model.value
              property alias input: passwordInputField

              onDeleteButtonClicked: {
                // TODO handle deletion
                console.log("delete password", model.id)
                quodEditor.appendPasswordToDelete(model.id)
              }

              UIKit.InputField {
                id: passwordInputField
                labelText: parent.labelText
                placeholderTxt: parent.placeholderText
                textField.text: isPassPhrase ? passwordMask : parent.value
                inputRequired: false
                Layout.fillWidth: true
                eyeIconVisible: false
                clearIconVisible: false
                isPassPhrase: true
                textField.readOnly: true
                textField.maximumLength: parent.maxLength ? parent.maxLength : 32767
                leftEyeIcon: true
                rightIconSource: "qrc:/duplicate-icon.svg"
                rightIconBtn.style: ThemeController.style.fieldButtonAlt
                overrideRightButton: true
                onRightButtonClicked: {
                  if (overrideRightButton) {
                    // Always copy the original value
                    app.clipboard.copy(parentInput.value)
                  }
                }
              }
            }
            Rectangle {
              width: parent.width - parentInput.removeButtonWidth
              height: s(1)
              color: ThemeController.style.separatorColor
            }
          }
        }
      }
    }
  }
}
