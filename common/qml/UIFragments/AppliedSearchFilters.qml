import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import QtQml.Models 2.12
import UIModel 1.0 as UIModel

Rectangle {
  property int filtersApplied: listView.count
  property var sModel
  color: ThemeController.style.appliedSearchFiltersPanel.bgColor
  implicitHeight: s(72)
  Rectangle {
    id: topSeparator
    color: ThemeController.style.appliedSearchFiltersPanel.bgColor
    height: s(1)
    anchors {
      left: parent.left
      right: parent.right
      top: parent.top
    }
  }
  Rectangle {
    id: bottomSeparator
    color: ThemeController.style.appliedSearchFiltersPanel.bgColor
    height: s(1)
    anchors {
      left: parent.left
      right: parent.right
      top: parent.bottom
    }
  }

  ListView {
    id: listView
    height: s(40)
    anchors {
      left: parent.left
      right: parent.right
      verticalCenter: parent.verticalCenter
    }
    leftMargin: ThemeController.style.margin.m16
    spacing: ThemeController.style.margin.m12
    clip: true
    orientation: ListView.Horizontal

    model: sModel ? sModel.filters.appliedFiltersModel : null
    delegate: UIKit.TextLabelDelegate {
      closeBtn.visible: true
      text: name
      onCloseButtonClicked: {
        console.log("REMOVE FILTER", model.type, model.filterId)
        sModel.removeFilter(model.type, model.filterId)
      }
    }
  }

  LinearGradient {
    anchors {
      right: parent.right
      top: parent.top
      bottom: parent.bottom
      topMargin: topSeparator.height
    }
    width: s(65)

    start: Qt.point(0,0)
    end: Qt.point(width, 0)
    gradient: ThemeController.style.appliedSearchFiltersPanel.gradient
  }

  UIModel.QuickFilterModel {
    id: qfModel
  }
}
