import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m16

  signal buttonClicked()
  Connections {
    target: pageManager
    function onSetupTermsPage(config) {
      layout.buttonClicked.connect(config.buttonHandler)
    }
  }

  Rectangle {
    Layout.preferredHeight: app.isPhone ? s(222) : s(230)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: "qrc:/illustrations/r19_24_25.tif"
      mipmap: true
    }
  }

  UIKit.BaseText {
    Layout.topMargin: app.isPhone ? s(17) : s(8)
    Layout.alignment: Qt.AlignHCenter
    text: Strings.valuePrivacy
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    horizontalAlignment: Qt.AlignHCenter
    lineHeight: app.isPhone ? s(32) : s(48)
  }

  UIKit.BaseText {
    Layout.fillWidth: true
    text: Strings.readTerms
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    horizontalAlignment: Qt.AlignHCenter
    lineHeight: s(24)
  }

  UIKit.StyledCheckBox {
    id: acceptEula
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    text: Strings.acceptEula
//TODO replace
    link: "https://website-staging.test.com/en/eula"
    checkRequired: true
    boldedTextColor: ThemeController.style.blackColor
  }

  UIKit.StyledCheckBox {
    id: acceptPrivacyPolicy
    Layout.fillWidth: true
    text: Strings.acceptPrivacyPolicy
//TODO replace
    link: "https://website-staging.test.com/en/privacy-policy"
    checkRequired: true
    boldedTextColor: ThemeController.style.blackColor
  }

  UIKit.StyledCheckBox {
    id: acceptAge
    Layout.fillWidth: true
    text: Strings.over16years
    checkRequired: true
    boldedTextColor: ThemeController.style.blackColor
  }

  UIKit.Errorbox {
    id: infobox
    errorMsg: Strings.checkboxError
    visible: acceptEula.errorState === true ||
             acceptPrivacyPolicy.errorState === true ||
             acceptAge.errorState === true
  }

  UIKit.StyledButton {
    id: createAccount
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    text: Strings.next
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      if(!acceptEula.checked){
        acceptEula.errorState = true
      }
      if(!acceptPrivacyPolicy.checked){
        acceptPrivacyPolicy.errorState = true
      }
      if(!acceptAge.checked){
        acceptAge.errorState = true
      }

      if (acceptEula.checked && acceptPrivacyPolicy.checked && acceptAge.checked) {
        buttonClicked()
//        RegistrationManager.createAccountPage();
      }
    }
  }

  UIKit.BaseText {
    Layout.topMargin: -ThemeController.style.margin.m8
    Layout.bottomMargin: ThemeController.style.margin.m24
    Layout.alignment: Qt.AlignHCenter
    textFormat: Text.RichText
    lineHeight: s(24)
    visible: false
    text: Strings.alreadyHaveAccount.arg("<a href 'link' style='text-decoration: none'><font color=\"%3\"><b>")
    .arg("</b></font></a>").arg(ThemeController.style.slatePurpleColor)
    onLinkActivated: {
      pageManager.backToLogin()
    }
  }
}
