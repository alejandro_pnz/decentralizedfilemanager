import QtQuick 2.15
import UIKit 1.0 as UIKit
import com.test 1.0

Item {
  anchors.fill: parent
  signal rightButtonClicked()
  signal leftButtonClicked()

  property alias leftButton: leftBtn
  property alias rightButton: rightBtn

  UIKit.StyledButton {
    id: leftBtn
    text: Strings.saveArca
    type: UIKit.StyledButton.ButtonStyle.Secondary
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    anchors.left: parent.left
    anchors.verticalCenter: parent.verticalCenter
    onClicked: leftButtonClicked()
  }

  UIKit.StyledButton {
    id: rightBtn
    text: Strings.continueTxt
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    anchors.right: parent.right
    anchors.verticalCenter: parent.verticalCenter
    onClicked: rightButtonClicked()
  }
}
