import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: content
  anchors.fill: parent
  spacing: ThemeController.style.margin.m16

  UIKit.InputField {
    id: titleInput
    Layout.topMargin: ThemeController.style.margin.m16
    labelText: Strings.title
    placeholderTxt: Strings.title
    inputRequired: false
    clearIconVisible: true
    onTabPressed: linkInput.textField.forceActiveFocus()
    onEnterPressed: linkInput.textField.forceActiveFocus()
    onBacktabPressed: linkInput.textField.forceActiveFocus()
  }

  UIKit.InputField {
    id: linkInput
    labelText: Strings.link
    placeholderTxt: Strings.typeOrPasteAddress + "..."
    inputRequired: false
    clearIconVisible: true
    onTabPressed: titleInput.textField.forceActiveFocus()
    onEnterPressed: titleInput.textField.forceActiveFocus()
    onBacktabPressed: titleInput.textField.forceActiveFocus()
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
