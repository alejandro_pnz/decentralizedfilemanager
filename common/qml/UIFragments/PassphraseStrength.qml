import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0

import QtPromise 1.1

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m16
  signal showHint(point globalPos)
  signal buttonClicked()
  property alias tipButtonWidth: inputField.tipsButtonWidth
  property var handler
  Connections {
    target: pageManager
    function onSetupPowerPasswordPage(config) {
      handler = config.buttonHandler
//      layout.buttonClicked.connect(config.buttonHandler)
    }
  }

  onButtonClicked: {
    if (handler)
      handler(inputField.textField.text)
  }

  UIKit.BaseText {
    Layout.fillWidth: true
    text: Strings.emailValidated
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.BaseText {
    text: Strings.createPowerPassphrase
    Layout.alignment: Qt.AlignHCenter
    Layout.fillWidth: true
    horizontalAlignment: Qt.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.Errorbox {
    id: infobox
    errorMsg: Strings.powerPassphraseRequired
    Layout.fillWidth: true
  }

  UIKit.TipsInput {
    id: inputField
    Layout.topMargin: ThemeController.style.margin.m8
    inputRequired: true
    Layout.fillWidth: true
    labelText: Strings.powerPassphrase
    isPassPhrase: true
    placeholderTxt: Strings.typePassphrase
    strengthIndicatorVisible: true
    eyeIconVisible: true

    onHandleShowHint: {
      showHint(p)
    }

    onTextChanged: {
      var promise = Future.promise(PasswordStrengthEvaluator.strength(text))

      promise.then(function(strength) {
        if (strength === PasswordStrengthEvaluator.VeryStrongPassword ||
            strength === PasswordStrengthEvaluator.StrongPassword) {
          inputField.passwordStrengthIndicator.state = 'high'
        }
        else if (strength === PasswordStrengthEvaluator.ReasonablePassword) {
          inputField.passwordStrengthIndicator.state = 'medium'
        }
        else if (strength === PasswordStrengthEvaluator.WeakPassword) {
          inputField.passwordStrengthIndicator.state = 'low'
        }
        else {
          inputField.passwordStrengthIndicator.state = 'verylow'
        }
      });
    }
  }

  UIKit.StyledButton {
    id: button
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    text: Strings.next
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      if (validateMandatoryField(inputField)) {
        buttonClicked()
      }
    }
  }
}
