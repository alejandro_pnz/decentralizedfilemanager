import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQml.Models 2.12
import SortFilterProxyModel 0.2

ColumnLayout {
  spacing: ThemeController.style.homePageVerticalMargin
  width: parent.width
  height: parent.height

  property int selectedCount: ism.selectedIndexes.length

  function save() {
    homeManager.currentEditor.saveLinkedArca(ism.selectedIndexes)
  }

  function clearSelection() {
    ism.clearSelection()
  }

  UIKit.SearchInput {
    id: search
    Layout.topMargin: ThemeController.style.homePageVerticalMargin
    Layout.fillWidth: true
    placeholderText: Strings.searchArca
    Layout.preferredHeight: ThemeController.style.defaultInputHeight
    micIconVisible: false
  }

  ListView {
    id: listView
    Layout.fillWidth: true
    Layout.fillHeight: true
    spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
    clip: true

    model: proxy
    delegate: UIKit.ObjectDelegate {
      title: model.name
      text: model.content
      titleIcon: false
      avatar.iconSrc: "image://item/type=" + SyncableObject.Arca + "&id=" + model.id

      width: parent.width

      checkboxOnTheLeft: !app.isMobile
      checkable: true
      selectionModel: ism
      delegateModel: proxy
      selectionMode: true
    }
  }

  SortFilterProxyModel {
    id: proxy
    sourceModel: ArcaListModel
    filters: [
      ListFilter {
        roleName: "id"
        list: homeManager.currentEditor.linkedArcaIds
        inverted: homeManager.currentEditor.invertLinkedList
      },
      RegExpFilter {
        roleName: "name"
        pattern: search.searchText
        caseSensitivity: Qt.CaseInsensitive
      }
    ]
  }

  ItemSelectionModel {
    id: ism
    model: ArcaListModel
  }
}
