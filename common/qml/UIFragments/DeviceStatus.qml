import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m16

  property int deviceType: Enums.Laptop
  signal buttonClicked()

  property bool busy: false
  property string imageSrc: ""

  enabled: !layout.busy

  Connections {
    target: pageManager
    function onSetupDeviceStatusPage(config) {
      setupPage(config)
      layout.deviceType = config.deviceType
      deviceDelegate.txt = config.deviceName
      titleComponent.text = config.title
      if (config.content !== undefined) {
        contentComponent.text = config.content
      }
      if (config.buttonText !== undefined) {
        button.text = config.buttonText
      }
      if (config.imageSrc !== undefined) {
        imageSrc = config.imageSrc
      }
      layout.buttonClicked.connect(config.buttonHandler)
    }
  }

  Rectangle {
    height: app.isMobile ? s(222) : s(230)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: imageSrc
      mipmap: true
    }
  }

  UIKit.BaseText {
    id: titleComponent
    Layout.topMargin: ThemeController.style.margin.m24
    alternativeFont: true
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.alignment: Qt.AlignHCenter
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
  }

  UIKit.BaseText {
    id: contentComponent
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.alignment: Qt.AlignHCenter
    visible: text !== ""
  }

  UIKit.IconDelegate {
    id: deviceDelegate
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    boldText: true
    iconBgCircle: true
    iconBgColor: ThemeController.style.deviceDelegate.iconBgColor
    iconSrc: {
      switch(layout.deviceType) {
      case Enums.Laptop: return "qrc:/laptop.svg"
      case Enums.Phone: return "qrc:/phone.svg"
      case Enums.Tablet: return "qrc:/tablet.svg"
      }
    }
  }

  UIKit.StyledButton {
    id: button
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    text: Strings.openMailApp
    textItem.visible: !pulseLoader.visible
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: buttonClicked()

    UIKit.PulseLoader {
      id: pulseLoader
      anchors.centerIn: parent
      visible: layout.busy
    }
  }

  Item {
    Layout.minimumHeight: ThemeController.style.margin.m16
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
