import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import UIFragments 1.0 as UIFragments
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2
import UIModel 1.0

import com.testintl 1.0
import QtQml.Models 2.12

ColumnLayout {
  spacing: ThemeController.style.homePageVerticalMargin
  width: parent.width
  height: parent.height

  enum ShownObjects { QuodAndFile, File }

  property int shownObjects: SelectObjects.ShownObjects.QuodAndFile

  property int selectedCount: arcaEditor.linkedObjectsModel.selectedIndexes.length/*arcaEditor.object.quodCount + arcaEditor.object.filesCount*/
  property bool linkedObjectsVisible: isPhone && multipleSelection

  property var displayedTypes: switch(shownObjects) {
                               case SelectObjects.ShownObjects.QuodAndFile: return [SyncableObject.Quod, SyncableObject.File]
                               case SelectObjects.ShownObjects.File: return [SyncableObject.File]
                               }
  property bool showCertFiles: false
  property bool multipleSelection: true

  Connections {
    target: homeManager
    function onSetupSelectObjectsPage(config) {
      if(config.multipleSelection !== undefined){
        multipleSelection = config.multipleSelection
      }
      if(config.shownObjects !== undefined){
        shownObjects = config.shownObjects
      }
      if(config.showCertFiles !== undefined){
        showCertFiles = config.showCertFiles
      }
    }
  }

  UIKit.SearchInput {
    id: search
    Layout.topMargin: ThemeController.style.homePageVerticalMargin
    Layout.fillWidth: true
    placeholderText: Strings.searchQuodOrFiles
    Layout.preferredHeight: ThemeController.style.defaultInputHeight
    micIconVisible: false
  }

  Flickable {
    Layout.fillHeight: true
    Layout.fillWidth: true
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: content.height
    clip: true

    ColumnLayout {
      id: content
      width: parent.width
      spacing: ThemeController.style.homePageVerticalMargin

      UIKit.BaseText {
        text: Strings.linkedData
        font.weight: Font.DemiBold
        visible: linkedObjectsVisible
        font.capitalization: Font.Capitalize
      }

      UIKit.GenericListView {
        id: linkedList
        Layout.fillWidth: true
        Layout.preferredHeight: contentHeight
        spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
        clip: true
        interactive: false
        visible: linkedObjectsVisible

        model: linkedProxy
        delegate: UIKit.ObjectDelegate {
          title: model.name
          text: model.content
          width: parent.width
          titleIcon: true
          avatar.iconSrc: "image://item/type=" + model.objectType + "&id=" + model.id

          checkboxOnTheLeft: !isMobile && checkable
          checked: true
          checkable: true
          selected: true
          onClicked: arcaEditor.linkedObjectsModel.select(linkedProxy.mapToSource(linkedProxy.index(index,0)), ItemSelectionModel.Deselect)

          onTextLinkActivated: {
            homeManager.openOneArcaPage(link)
          }
        }
      }

      Rectangle {
        id: spacer
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
        visible: linkedObjectsVisible
      }

      UIFragments.ObjectListSortButton {
        id: sortButton
        headerBottomMargin: 0
        Layout.topMargin: isMobile ? ThemeController.style.margin.m8 : 0
      }

      UIKit.GenericListView {
        id: listView
        Layout.fillWidth: true
        Layout.preferredHeight: contentHeight
        Layout.topMargin: isMobile ? ThemeController.style.margin.m4 : 0
        spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
        clip: true
        interactive: false

        model: multipleSelection ? sortModel.proxy : singleSelectModel.proxy
        delegate: UIKit.ObjectDelegate {
          title: model.name
          text: model.content
          titleIcon: model.locked
          avatar.shareMode: model.syncProperty
          avatar.iconSrc: "image://item/type=" + model.objectType + "&id=" + model.id

          width: parent.width
          checkboxOnTheLeft: !isMobile && checkable
          checkable: multipleSelection
          delegateModel: multipleSelection ? sortModel.proxy : singleSelectModel.proxy
          selectionMode: multipleSelection
          selectionModel: arcaEditor.linkedObjectsModel
          selected: selectionModel.isSelected(delegateModel.mapToSource(delegateModel.index(index, 0)))
          onTextLinkActivated: {
            homeManager.openOneArcaPage(link)
          }

          onDelegateClicked: {
            if(!multipleSelection){
              homeManager.fileChosen(model.name + "." + model.fileType, model.id)
              homeManager.back()
            }
          }
        }
      }
    }
  }

  // This model is only used on mobiles and should contain current selection
  SortFilterProxyModel {
    id: linkedProxy
    sourceModel: QuodFileListModel
    filters: [
      ListFilter {
        roleName: "id"
        enabled: app.isPhone
        list: arcaEditor.selectedObjectsIds
      }
    ]
  }

  SortFilterProxyModel {
    id: filterModel
    sourceModel: SyncableObjectListModel
    filters: [
      ListFilter {
        roleName: "objectType"
        list: displayedTypes
      },
      ListFilter {
        roleName: "fileType"
        list: ["pem", "pkcs7", "der", "pfx"]
        enabled: showCertFiles
      }
    ]
  }

  SortModel {
    id: sortModel
    searchPattern: search.searchText
    sourceModel: QuodFileListModel
    sorter: sortButton.sorter
    pinnedFilterEnabled: false
    customListSorter.roleName: "id"
    customListSorter.enabled: app.isPhone
    customListSorter.list: arcaEditor.selectedObjectsIds
    customListSorter.inverted: true
  }

  SortModel {
    id: singleSelectModel
    searchPattern: search.searchText
    sourceModel: filterModel
    sorter: sortButton.sorter
    pinnedFilterEnabled: false
    customListSorter.roleName: "id"
    customListSorter.enabled: app.isPhone
    customListSorter.list: arcaEditor.selectedObjectsIds
    customListSorter.inverted: true
  }
}
