import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

Flickable {
  id: flick
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true
  property bool isPhone: app.isPhone
  property var object
  signal closeRequest()
  function save() {
    console.log("Save tag", object.name, object.color, nameInput.textField.text)
    if (validateMandatoryField(nameInput, Strings.emptyTagName)) {
      object.name = nameInput.textField.text
      object.color = buttonsGroup.checkedButton.itemColor
      TagListModel.updateTag(object)
      return true
    }
    return false
  }

  function deleteTag() {
    // copy the pointer to avoid warnings after removing
    var obj = object
    homeManager.enterConfirmTagDeletionPopupPage(obj.id)
  }

  Connections {
    target: TagListModel
    function onTagDeleted(tagId) {
      console.log("Tag deleted", tagId)
      if (tagId == object.id) {
        object = undefined
        console.log("Tag deleted, close request")
        closeRequest()
      }
    }
  }

  ColumnLayout {
    id: content
    spacing: ThemeController.style.margin.m16
    width: parent.width

    UIKit.InputField {
      id: nameInput
      Layout.topMargin: ThemeController.style.margin.m16
      labelText: Strings.tagName
      placeholderTxt: Strings.tagName
      clearIconVisible: true
      inputRequired: false
      textField.text: object !== undefined ? object.name : ""
    }

    UIKit.BaseText {
      Layout.topMargin: ThemeController.style.margin.m8
      text: Strings.colour
      font.weight: Font.Bold
    }

    ListView {
      id: listView
      Layout.fillWidth: true
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
      spacing: ThemeController.style.margin.m12
      orientation: ListView.Horizontal
      interactive: true
      clip: true

      model: colorModel
      delegate: UIKit.ColorItem {
        itemColor: colorValue
        ButtonGroup.group: buttonsGroup
        checked: object !== undefined ? Qt.colorEqual(object.color,colorValue) : index === 0
      }
    }

    ButtonGroup {
      id: buttonsGroup
      exclusive: true
    }

    Rectangle {
      Layout.topMargin: isPhone ? 0 : s(8)
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
    }

    UIKit.StyledButton {
      Layout.topMargin: isPhone ? 0 : s(8)
      type: UIKit.StyledButton.ButtonStyle.Text
      displayMode: UIKit.StyledButton.DisplayMode.TextRightSideIcon
      text: Strings.deleteTag
      icon.source: "qrc:/delete-purple.svg"
      visible: object !== undefined
      onClicked: deleteTag()
    }

    Rectangle {
      Layout.topMargin: isPhone ? 0 : s(8)
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }

    ListModel {
      id: colorModel
    }

    Component.onCompleted: {
      colorModel.append({ colorValue: "#5C5AE8"});
      colorModel.append({ colorValue: "#E042FB"});
      colorModel.append({ colorValue: "#4CBDFF"});
      colorModel.append({ colorValue: "#22D396"});
      colorModel.append({ colorValue: "#FFB904"});
      colorModel.append({ colorValue: "#2F3441"});
    }
  }
}
