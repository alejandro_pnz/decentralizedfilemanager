import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2

import com.testintl 1.0

Flickable {
  id: flick
  clip: true
  height: parent.height
  width: parent.width
  contentHeight: content.height

  property bool hasSubtypes: quodSubTypeModel.count > 0

  signal showSubTypeSelection()

  Column {
    id: content
    anchors.top: parent.top
    anchors.topMargin: ThemeController.style.margin.m12
    anchors.left: parent.left
    anchors.right: parent.right
    spacing: ThemeController.style.margin.m24
    bottomPadding: ThemeController.style.margin.m24

    QuodTypeListModel {
      id: quodTypeModel
      source: DatabaseManager.quodTypes()
    }

    UIKit.ResponsiveGridLayout {
      id: listView
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.rightMargin: -horizontalSpacing
      horizontalSpacing: ThemeController.style.margin.m32
      verticalSpacing: ThemeController.style.margin.m12
      delegateHeight: s(64)
      delegateMinimumWidth: s(268)
      delegateMaximumWidth: parent.width

      currentIndex: -1
      model: quodTypeModel
      delegate: Item {
        id: delegate
        width: listView.cellWidth
        height: listView.cellHeight
        property string titleTxt: name
        property string iconSource: iconPath
        property var subtypes: subTypes
        property alias card: card

        UIKit.CardDelegate {
          id: card
          property int currentIndex: index
          icon.source: iconSource
          title: titleTxt
          checkable: false
          checked: listView.currentIndex == card.currentIndex
          height: listView.cellHeight - listView.verticalSpacing
          width: listView.maximumCellContentWidth
          avatar.pixelSize: s(48)
          avatar.iconSize: s(24)
          topPadding: ThemeController.style.margin.m8
          bottomPadding: ThemeController.style.margin.m8

          onClicked: {
            quodEditor.object = object
            quodEditor.subTypes = delegate.subtypes
            quodSubTypeModel.source = delegate.subtypes
            quodSubTypeModel.source = subtypes
            listView.currentIndex = -1
            if (delegate.subtypes.length > 0) {
              if (app.isPhone) {
                homeManager.openPage(Pages.SelectQuodSubTypePage)
              } else {
                listView.currentIndex = card.currentIndex
              }
            } else {
              homeManager.openPage(Pages.EditQuodDetailsPage)
            }
          }

         }
      }
      cellWidth: {
        if((width - horizontalSpacing) * 0.5 < delegateMinimumWidth){
          width
        }
        else {
          width * 0.5
        }
      }
    }

    Rectangle {
      height: s(1)
      width: parent.width
      color: ThemeController.style.seashellColor
      visible: subTypeView.visible
    }

    UIKit.BaseText {
      text: Strings.selectQuodSubType
      font.weight: Font.Bold
      visible: subTypeView.visible
    }

    QuodTypeListModel {
      id: quodSubTypeModel
    }

    UIKit.ResponsiveGridLayout {
      id: subTypeView
      visible: quodSubTypeModel.count > 0 && !app.isPhone
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.rightMargin: -horizontalSpacing
      horizontalSpacing: ThemeController.style.margin.m32
      verticalSpacing: ThemeController.style.margin.m12
      delegateHeight: s(64)
      delegateMinimumWidth: app.isPhone ? parent.width : s(268)
      delegateMaximumWidth: parent.width
      model: quodSubTypeModel
      currentIndex: -1
      delegate: Item {
        id: isDelegate
        width: subTypeView.cellWidth
        height: subTypeView.cellHeight
        property string titleTxt: name
        property string iconSource: iconPath
        property alias cardDelegate: cardDelegate
        UIKit.CardDelegate {
          id: cardDelegate
          icon.source: iconSource
          title: titleTxt
          checkable: false
          height: subTypeView.cellHeight - subTypeView.verticalSpacing
          width: subTypeView.maximumCellContentWidth
          avatar.pixelSize: s(48)
          avatar.iconSize: s(24)
          ButtonGroup.group: subtypeButtonsGroup
          topPadding: s(8)
          bottomPadding: s(8)

          onClicked: {
            quodEditor.object = object
            listView.currentIndex = -1
            homeManager.openPage(Pages.EditQuodDetailsPage)
            quodSubTypeModel.source = {}
          }
        }
      }

      cellWidth: {
        if((width - horizontalSpacing) * 0.5 < delegateMinimumWidth){
          width
        }
        else {
          width * 0.5
        }
      }
    }

    ButtonGroup {
      id: subtypeButtonsGroup
      exclusive: true
    }
  }

  Component.onCompleted: {
    quodEditor.create()
  }
}
