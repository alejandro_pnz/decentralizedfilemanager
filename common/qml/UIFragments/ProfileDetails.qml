import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: content
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m16
  property bool showFooter: true

  UIKit.BaseText {
    text: Strings.profileDetails
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    visible: !app.isPhone
  }

  UIKit.InputField {
    id: nameInput
    Layout.topMargin: app.isPhone ? 0 : ThemeController.style.margin.m8
    labelText: Strings.name
    placeholderTxt: Strings.typeYourFullName
    inputRequired: false
    clearIconVisible: true
    onBacktabPressed: emailInput.textField.forceActiveFocus()
    onTabPressed: nicknameInput.textField.forceActiveFocus()
    onEnterPressed: nicknameInput.textField.forceActiveFocus()
  }

  UIKit.InputField {
    id: nicknameInput
    labelText: Strings.nickname
    placeholderTxt: Strings.typeYourNickname
    inputRequired: false
    clearIconVisible: true
    onBacktabPressed: nameInput.textField.forceActiveFocus()
    onTabPressed: emailInput.TextField.forceActiveFocus()
    onEnterPressed: emailInput.TextField.forceActiveFocus()
  }

  UIKit.InputField {
    id: emailInput
    labelText: Strings.email
    placeholderTxt: Strings.typeEmail
    inputRequired: false
    clearIconVisible: true
    fieldType: UIKit.InputField.FieldType.Email
    onBacktabPressed: nicknameInput.textField.forceActiveFocus()
    onTabPressed: nameInput.textField.forceActiveFocus()
    onEnterPressed: nameInput.textField.forceActiveFocus()
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
