import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import QtQml.Models 2.12
import UIModel 1.0 as UIModel

CheckableTextLabelPanel {
  id: panel
  title: Strings.quickFilters
  property bool applyImmediately: false
  signal actionTaken()
  onDelegateClicked: {
    searchModel.filters.filter(SFilter.QuickFilters).toggle(index, ItemSelectionModel.Toggle)
    if (applyImmediately) {
      searchModel.updateFilters();
    }
    actionTaken()
  }

  onSearchModelChanged: {
    if (panel.searchModel) {
      var m = searchModel.filters.filter(SFilter.QuickFilters);
      panel.model = m.model
      panel.ism = m.selectionModel
    }
  }
}
