import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQml.Models 2.12
import SortFilterProxyModel 0.2

ColumnLayout {
  id: content
  width: parent.width
  height: parent.height
  spacing: ThemeController.style.margin.m16
  readonly property bool isMobile: app.isPhone

  UIKit.SearchInput {
    id: search
    Layout.topMargin: isMobile ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
    Layout.fillWidth: true
    placeholderText: Strings.searchUsers
    Layout.preferredHeight: s(48)
    micIconVisible: false
  }

  UIKit.BaseText {
    Layout.topMargin: ThemeController.style.margin.m12
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    text: whiteListProxy.selectionCount > 0 ? Strings.usersSelected.arg(whiteListProxy.selectionCount) :
                                              Strings.shareWithWhom
    font.weight: Font.DemiBold
  }

  ListView {
    id: selectedList
    Layout.fillWidth: true
    Layout.preferredHeight: s(88)
    spacing: ThemeController.style.margin.m16
    orientation: ListView.Horizontal
    visible: count > 0 && isMobile
    model: selectedProxy
    delegate: UIKit.AvatarDelegate {
      email: object.name
      avatarObject.onTopIconClicked: {
        whiteListProxy.setData(whiteListProxy.index(selectedProxy.mapToSource(index),0),
                               false,
                               whiteListProxy.checkStateRoleValue())
      }
    }
  }

  UIKit.AccountDelegate {
    accountName: "bellejk@example.com"
    Layout.fillWidth: true
    Layout.leftMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    Layout.rightMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m8
  }

  ListView {
    id: listView
    Layout.fillWidth: true
    Layout.fillHeight: true
    spacing: ThemeController.style.margin.m12
    clip: true

    model: UserListModel
    delegate: UIKit.ContactCard {
      title: object.name
      checked: isChecked
      onClicked: {
        whiteListProxy.setData(whiteListProxy.index(proxy.mapToSource(index),0),
                               checked,
                               whiteListProxy.checkStateRoleValue())
      }
    }
  }

  ItemSelectionModel {
    id: ism
  }

  WhiteListProxy {
    id: whiteListProxy
    sourceModel: UserListModel
    whiteListMode: content.whitelistMode
  }

  SortFilterProxyModel {
    id: selectedProxy
    sourceModel: whiteListProxy
    filters: [
      ValueFilter {
        enabled: !whiteListProxy.whiteListMode
        roleName: "blacklisted"
        value: true
      },
      ValueFilter {
        enabled: whiteListProxy.whiteListMode
        roleName: "whitelisted"
        value: true
      }
    ]
  }

  SortFilterProxyModel {
    id: proxy
    sourceModel: whiteListProxy
    filters: [
      RegExpFilter {
        roleName: "name"
        pattern: search.searchText
        caseSensitivity: Qt.CaseInsensitive
      },
      ValueFilter {
        enabled: whiteListProxy.whiteListMode
        roleName: "blacklisted"
        value: false
      },
      ValueFilter {
        enabled: !whiteListProxy.whiteListMode
        roleName: "whitelisted"
        value: false
      }
    ]
  }
}
