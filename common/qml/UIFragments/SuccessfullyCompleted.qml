import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
    spacing: ThemeController.style.margin.m16
    property string imageSrc: "qrc:/illustrations/r13.png"
    Connections {
      target: homeManager
      function onSetupSuccessfullyCompletedPopupPage(config) {
          if (config.title !== undefined) {
            title.text = config.title
          }
          if (config.contentText !== undefined) {
            contentText.text = config.contentText
          }
          if (config.imageSrc !== undefined) {
            imageSrc = config.imageSrc
          }
      }
    }

    Rectangle {
        Layout.preferredHeight: s(app.isMobile ? 222 : 230)
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter
        radius: s(10)
        Image {
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source: imageSrc
            mipmap: true
        }
    }

    UIKit.BaseText {
        id: title
        Layout.topMargin: ThemeController.style.margin.m8
        Layout.alignment: Qt.AlignHCenter
        text: Strings.congratulations
        font.weight: Font.Bold
        size: UIKit.BaseText.TextSize.H3
        horizontalAlignment: Text.AlignHCenter
    }

    UIKit.BaseText {
        id: contentText
        Layout.alignment: Qt.AlignHCenter
        text: Strings.allDoneReadytoGo
        horizontalAlignment: Text.AlignHCenter
    }
}

