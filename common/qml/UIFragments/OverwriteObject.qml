import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: content
  spacing: ThemeController.style.margin.m16

  property int conflictsNumber: 1

  UIKit.BaseText {
    Layout.topMargin: app.isPhone && !app.isTablet ? s(70) : ThemeController.style.margin.m24
    text: Strings.overwriteObject
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    font.weight: Font.Bold
    size: app.isPhone ? UIKit.BaseText.TextSize.H2 : UIKit.BaseText.TextSize.H3
  }

  UIKit.BaseText {
    text: Strings.objectWithTheSameName
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  Item {
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    Layout.preferredHeight: switchButton.height
    visible: conflictsNumber > 1

    RowLayout {
      width: parent.width
      height: parent.height
      UIKit.BaseText {
        text: Strings.applyToNextConflicts.arg(conflictsNumber - 1)
        font.weight: Font.DemiBold
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignVCenter
      }
      UIKit.SwitchButton {
        id: switchButton
      }
    }
  }

  UIKit.StyledButton {
    Layout.topMargin: ThemeController.style.margin.m16
    text: Strings.keepNewerObject
    Layout.fillWidth: true
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Primary
  }

  UIKit.StyledButton {
    text: Strings.keepBothObjects
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Secondary
    Layout.fillWidth: true
  }

  UIKit.StyledButton {
    text: conflictsNumber > 1 ? Strings.skipThisObject : Strings.cancel
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Text
    Layout.alignment: Qt.AlignHCenter
    buttonStyle: ThemeController.style.overwriteCancelButtonStyle
  }

  Item {
    Layout.fillHeight: app.isMobile && !app.isTablet  ? true  : false
    Layout.preferredHeight: app.isMobile && !app.isTablet ? undefined : s(8)
    Layout.fillWidth: true
  }
}
