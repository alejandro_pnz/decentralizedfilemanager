import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m16

  property string title: ""
  property string contentText: ""
  property alias input: inputField
  property alias button: button

  property string validatorText
  property string imageSrc: ""

  property bool busy: false

  enabled: !layout.busy

  signal buttonClicked(string text)

  Connections {
    target: pageManager
    function onSetupGenericInputPage(config) {
      setupPage(config)
      layout.title = config.title
      layout.contentText = config.contentText
      layout.button.text = config.buttonText
      layout.input.labelText = config.labelText
      layout.input.placeholderTxt = config.placeholderText
      layout.input.isPassPhrase = config.isPassPhrase
      layout.input.eyeIconVisible = config.eyeIconVisible
      layout.input.clearIconVisible = config.clearIconVisible
      layout.buttonClicked.connect(config.buttonHandler)
      if (config.validatorText !== undefined) {
        console.log("Validator text", config.validatorText)
        layout.validatorText = config.validatorText
      }
      if (config.emailValidator !== undefined) {
        if (config.emailValidator) {
          console.log("Validator set")
          layout.input.validator = emailRegExpValidator
        }
      }
      if (config.imageSrc !== undefined) {
        imageSrc = config.imageSrc
      }
    }
  }

  Connections {
    target: RegistrationManager
    function onEmailDoesNotExist(config) {
      if (config.emailError !== undefined) {
        inputField.errorMessage = [config.emailError]
        inputField.state = 'error'
      }
    }
  }

  RegExpValidator {
    id: emailRegExpValidator
    regExp: Strings.emailRegExp
  }
  Rectangle {
    Layout.minimumHeight: app.isPhone ? s(222) : s(230)
    Layout.maximumHeight: app.isPhone ? s(222) : s(230)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: imageSrc
      mipmap: true
    }
  }

  UIKit.BaseText {
    Layout.topMargin: ThemeController.style.margin.m24
    Layout.fillWidth: true
    text: title
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.BaseText {
    text: contentText
    Layout.alignment: Qt.AlignHCenter
    Layout.fillWidth: true
    horizontalAlignment: Qt.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.InputField {
    id: inputField
    Layout.topMargin: ThemeController.style.margin.m8
    inputRequired: true
    Layout.fillWidth: true
    onEnterPressed: button.clicked()
  }

  UIKit.StyledButton {
    id: button
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    textItem.visible: !pulseLoader.visible
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      if (validateInput(input, layout.validatorText)) {
        buttonClicked(inputField.textField.text)
      }
    }

    UIKit.PulseLoader {
      id: pulseLoader
      anchors.centerIn: parent
      visible: layout.busy
    }
  }
}
