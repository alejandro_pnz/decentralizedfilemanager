import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import com.test 1.0

Popup {
  id: popup
  implicitHeight: listView.contentHeight + bottomPadding + topPadding
  implicitWidth: s(262)

  modal: false
  leftPadding: 1
  rightPadding: 1
  topPadding: ThemeController.style.margin.m8
  bottomPadding: ThemeController.style.margin.m8
  property int listViewVerticalPadding: ThemeController.style.margin.m8

  property alias model: listView.model
  // Object item
  property var currentItem
  property var customItemDataProvider: null

  Connections{
      target: applicationMainWindow
      function onWidthChanged(){close()}
      function onHeightChanged(){close()}
  }

  background: Item {
    Rectangle {
      id: bgComponent
      anchors.fill: parent
      radius: ThemeController.style.margin.m8
      color: ThemeController.style.whiteColor
      border {
        color: ThemeController.style.whisperColor
        width: s(1)
      }
    }
    DropShadow {
        id: dropShadow
        anchors.fill: bgComponent
        transparentBorder: true
        radius: s(32)
        spread: 0.2
        color: Qt.rgba(0, 0, 0, 0.08)
        source: bgComponent
    }
  }

  onVisibleChanged: {
      if(visible){
          listView.height = Math.min(applicationMainWindow.height - (homePage.homeHeader.height + 2 * popup.listViewVerticalPadding) - bottomPadding - topPadding, listView.contentHeight)
          height = listView.height + bottomPadding + topPadding
      }
  }
  ListView {
    id: listView
    height: contentHeight
    clip: true
    onContentHeightChanged: {
        height = Math.min(applicationMainWindow.height - (homePage.homeHeader.height + 2 * popup.listViewVerticalPadding) - popup.bottomPadding - popup.topPadding, contentHeight)
        popup.height = height + popup.bottomPadding + popup.topPadding
    }

    ScrollBar.vertical: ScrollBar {
      parent: listView
      anchors {
        top: listView.top
        bottom: listView.bottom
        right: listView.right
      }

      width: s(8)
    }
    width: parent.width
    delegate: UIKit.ActionListDelegate {
      width: listView.width
      text: name
      icon.source: iconSource
      display: AbstractButton.TextBesideIcon
      onClicked: popup.close()
      action: actionSource
      pointingHandCursor: true
    }
  }
}
