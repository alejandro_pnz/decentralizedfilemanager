import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import QtQuick.Controls 2.15
import UIFragments 1.0 as UIFragments
import QtQml.Models 2.12
import SortFilterProxyModel 0.2
import UIModel 1.0

LibraryTab {
  id: arcaTab

  noItemTitle: Strings.noArcaInLibrary
  noItemDescription: Strings.noArcaInLibraryDescription
  noItemIcon: "qrc:/arca/neutral.svg"
  noItem: ArcaListModel.count < 1

  ism.model: ArcaListModel
  objectTypeName: Strings.arca
  objectType: SyncableObject.Arca

  property int horizontalSpacing
  property int verticalSpacing
  property int delegateHeight
  property int delegateMinimumWidth
  property int delegateMaximumWidth

  SortModel {
    id: sortedProxyModel
    pinnedFilterEnabled: false
    sourceModel: ArcaListModel
    sorter: sortBtn.sorter
    searchPattern: search.text
  }

  ColumnLayout {
    spacing: ThemeController.style.homePageVerticalMargin
    width: parent.width
    height: parent.height

    UIKit.SearchInput {
      id: search
      Layout.fillWidth: true
      placeholderText: Strings.searchArca
      micIconVisible: false
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
    }

    Column {
      spacing: 0
      Layout.fillWidth: true
      Layout.fillHeight: true

      UIFragments.ObjectListSortButton {
        id: sortBtn
        headerBottomMargin: 0

        Connections {
          target: arcaTab
          function onChangeSorter(sorter) {
            sortBtn.init(sorter)
          }
        }
        objectType: Enums.Arca
      }

      UIKit.ResponsiveGridLayout {
        id: listView
        width: parent.width
        height: parent.height - sortBtn.height

        horizontalSpacing: arcaTab.horizontalSpacing
        verticalSpacing: arcaTab.verticalSpacing
        delegateHeight: arcaTab.delegateHeight
        delegateMinimumWidth: arcaTab.delegateMinimumWidth
        delegateMaximumWidth: arcaTab.delegateMaximumWidth

        selectionMode: arcaTab.ism.hasSelection

        currentIndex: -1
        model: sortedProxyModel.proxy
        delegate: Item {
          id: delegate
          width: listView.cellWidth
          height: listView.cellHeight
          property var objectId: model.id
          property string title: model.name
          property string iconColor: model.color
          property string contentText: model.content
          property var syncPropertyValue: model.syncProperty
          property var obj: model.object
          UIKit.ArcaMobileDelegate {
            // Model data
            txt: delegate.title
            contentTxt: delegate.contentText
            avatarObject.shareMode: delegate.syncPropertyValue
            avatarObject.iconSrc: "image://item/type=" + SyncableObject.Arca + "&id=" + objectId

            // Other properties
            width: listView.maximumCellContentWidth
            height: listView.cellHeight - listView.verticalSpacing
            checkable: !app.isMobile
            model: listView.model
            selectionModel: arcaTab.ism
            selectionMode: listView.selectionMode

            actionButton.visible: !arcaTab.ism.hasSelection

            onDelegateClicked: {
              // Open object
              console.log("Delegate clicked")
              homeManager.openOneArcaPage(delegate.objectId);
            }

            onActionButtonClicked: {
              listView.currentIndex = index
              actionMenu.model.currentObject = delegate.obj
              actionMenu.handleActionButton(listView)
            }
          }
        }
      }

      Item {
        Layout.fillHeight: true
      }

    }
  }

  OneObjectActionMenu {
    id: actionMenu
    arcaMode: true
  }
}
