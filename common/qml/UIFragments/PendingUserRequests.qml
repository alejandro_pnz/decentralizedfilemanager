import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import QtQml.Models 2.12
import UIModel 1.0
import SortFilterProxyModel 0.2

ColumnLayout {
  id: content
  width: parent.width
  height: parent.height

  property bool selectionMode: ism.hasSelection
  property ItemSelectionModel ism: selectionModel
  property string objectTypeName: Strings.users
  property int objectType: Enums.Request

  SortFilterProxyModel {
    id: pendingUsersModel
    sourceModel: UserListModel
    filters: [
      ValueFilter {
        roleName: "pending"
        value: true
      },
      ValueFilter {
        roleName: "invited"
        value: true // TODO: should be false
      }
    ]
  }

  ItemSelectionModel {
    id: selectionModel
    model: pendingUsersModel
  }

  ListView {
    id: listView
    Layout.fillWidth: true
    Layout.topMargin: isMobile ? s(21) : ThemeController.style.margin.m24
    Layout.fillHeight: true
    spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
    interactive: true
    clip: true

    model: pendingUsersModel

    delegate: Component {
      Loader {
        id: loader
        sourceComponent: isMobile ? mobileDelegate : desktopDelegate
        width: listView.width
        property var userId: model.id
        property string emailText: model.email // TODO email
        property string contentTxt: "" // TODO
        property int idx: index
      }
    }
  }

  Component {
    id: mobileDelegate
    UIKit.DeviceInfoMobileDelegate {
      email: emailText
      contentText: contentTxt
      avatar.deviceType: deviceType
      onBlockClicked: UserListModel.blockOne(userId)
      onApproveClicked: UserListModel.approveOne(userId)
      onRejectClicked: UserListModel.rejectOne(userId)
    }
  }

  Component {
    id: desktopDelegate
    UIKit.DeviceInfoDesktopDelegate {
      email: emailText
      contentText: contentTxt
      selectionModel: ism
      //model: listView.model
      index: idx
      onBlockClicked: UserListModel.blockOne(userId)
      onApproveClicked: UserListModel.approveOne(userId)
      onRejectClicked: UserListModel.rejectOne(userId)
    }
  }
}
