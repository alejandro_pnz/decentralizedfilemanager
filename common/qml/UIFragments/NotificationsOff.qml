import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m12
  width: parent.width
  height: parent.height
  signal buttonClicked()

  Rectangle {
    Layout.preferredHeight: s(222)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: "qrc:/illustrations/u10.png"
      mipmap: true
    }
  }

  UIKit.BaseText {
    id: titleComponent
    Layout.topMargin: ThemeController.style.margin.m12
    alternativeFont: true
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H3
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.alignment: Qt.AlignHCenter
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    text: Strings.turnOffInApp
  }

  UIKit.BaseText {
    id: contentComponent
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.alignment: Qt.AlignHCenter
    text: Strings.weUseInAppMessages
  }

  UIKit.BaseText {
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.alignment: Qt.AlignHCenter
    text: Strings.byOpting
  }

  UIKit.StyledButton {
    Layout.topMargin: ThemeController.style.margin.m20
    Layout.fillWidth: true
    text: Strings.linkArca
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
  }

  UIKit.BaseText {
    text: Strings.or
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.Small
    Layout.alignment: Qt.AlignHCenter
    color: ThemeController.style.shuttleColor
    font.capitalization: Font.AllUppercase
  }

  UIKit.StyledButton {
    Layout.fillWidth: true
    text: Strings.keepMeInformed
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Secondary
    onClicked: {
    }
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
