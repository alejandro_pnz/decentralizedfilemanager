import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

Flickable {
    Layout.fillHeight: true
    Layout.fillWidth: true
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: contentColumn.height
    clip: true

    function save() {
        if (desktop.checked) {
            //TODO
        } else if (tablet.checked) {
            //TODO
        } else {
            //TODO
        }
    }

    ColumnLayout {
        id: contentColumn
        spacing: ThemeController.style.margin.m12
        width: parent.width

        UIKit.BaseText {
            text: Strings.addNewDeviceInfo
            Layout.fillWidth: true
            Layout.bottomMargin: ThemeController.style.margin.m12
            horizontalAlignment: Text.AlignHCenter
        }
        property int delegateHeight : s(64)
        property int delegateLeftPadding: ThemeController.style.margin.m12
        property int delegateVerticalPadding: ThemeController.style.margin.m8
        UIKit.CardDelegate {
            id: desktop
            title: Strings.deviceDesktop
            icon.source: hovered || checked ? "qrc:/device-desktop-filled.svg" : "qrc:/device-desktop.svg"
            Layout.fillWidth: true
            Layout.preferredHeight: contentColumn.delegateHeight
            checkable: true
            checked: true
            ButtonGroup.group: buttonsGroup
            leftPadding: contentColumn.delegateLeftPadding
            topPadding: contentColumn.delegateVerticalPadding
            bottomPadding: contentColumn.delegateVerticalPadding
        }

        UIKit.CardDelegate {
            id: tablet
            title: Strings.deviceTablet
            icon.source: hovered || checked ? "qrc:/device-tablet-filled.svg" : "qrc:/device-tablet.svg"
            Layout.fillWidth: true
            Layout.preferredHeight: contentColumn.delegateHeight
            checkable: true
            ButtonGroup.group: buttonsGroup
            leftPadding: contentColumn.delegateLeftPadding
            topPadding: contentColumn.delegateVerticalPadding
            bottomPadding: contentColumn.delegateVerticalPadding
        }

        UIKit.CardDelegate {
            id: mobile
            title: Strings.deviceMobile
            icon.source: hovered || checked ?  "qrc:/device-mobile-filled.svg" : "qrc:/device-mobile.svg"
            Layout.fillWidth: true
            Layout.preferredHeight: contentColumn.delegateHeight
            checkable: true
            ButtonGroup.group: buttonsGroup
            leftPadding: contentColumn.delegateLeftPadding
            topPadding: contentColumn.delegateVerticalPadding
            bottomPadding: contentColumn.delegateVerticalPadding
        }

        ButtonGroup {
            id: buttonsGroup
            exclusive: true
        }

        Rectangle {
          Layout.topMargin: ThemeController.style.margin.m12
          height: s(1)
          Layout.fillWidth: true
          color: ThemeController.style.seashellColor
        }

        UIKit.InputField {
          id: uuIdInput
          Layout.fillWidth: true
          Layout.topMargin: ThemeController.style.margin.m4
          labelText: Strings.deviceUUIDLabel
          placeholderTxt: Strings.typeUUID
          onEnterPressed: button.clicked()
        }

        UIKit.StyledButton {
            id: button
            Layout.fillWidth: true
            Layout.topMargin: ThemeController.style.margin.m20
            text: Strings.confirm
            displayMode: UIKit.StyledButton.DisplayMode.TextOnly
            onClicked: {
                save();
                if(!RegistrationManager.accountOwner){
                    homeManager.enterCheckEmailPopupPage()
                }
                else {
                    RegistrationManager.enterAddDeviceAdvancedPage()
                    homeManager.back();
                }
            }
        }

        UIKit.StyledButton {
            Layout.fillWidth: true
            text: Strings.cancel
            type: UIKit.StyledButton.ButtonStyle.Secondary
            displayMode: UIKit.StyledButton.DisplayMode.TextOnly
            onClicked: popup.close()

        }

        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }
}
