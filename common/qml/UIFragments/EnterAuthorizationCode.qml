import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: content
  spacing: isMobile ? ThemeController.style.margin.m16 : ThemeController.style.margin.m12
  width: parent.width
  property bool linkedAuthenticatorApp: false
  property bool isMobile: app.isPhone

  UIKit.BaseText {
    text: Strings.enterUnlockAuthorizationCode
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    font.weight: Font.Bold
    size: isMobile ? UIKit.BaseText.TextSize.H2 : UIKit.BaseText.TextSize.H3
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.BaseText {
    text: linkedAuthenticatorApp ? Strings.codeFromAuthenticatorApp : Strings.pleaseEnterCode.arg("4040400509")
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    Layout.fillWidth: true
    Layout.leftMargin: isMobile ? 0 : ThemeController.style.margin.m24
    Layout.rightMargin: isMobile ? 0 : ThemeController.style.margin.m24
    font.weight: isMobile ? Font.Normal : Font.Bold
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  Row {
    spacing: ThemeController.style.margin.m8
    Layout.alignment: Qt.AlignHCenter
    Layout.topMargin: isMobile ? ThemeController.style.margin.m8 : linkedAuthenticatorApp ? ThemeController.style.margin.m12 : -ThemeController.style.margin.m12

    Repeater {
      id: repeater
      model: 6
      delegate: UIKit.CodeInput {
        onTextChanged: {
          if(text !== "" && index < repeater.count - 1){
            repeater.itemAt(index + 1).forceActiveFocus()
          }
        }
      }
      Component.onCompleted: {
        repeater.itemAt(0).forceActiveFocus()
      }
    }
  }

  Row {
    Layout.topMargin: isMobile ? ThemeController.style.margin.m8 : ThemeController.style.margin.m12
    Layout.alignment: Qt.AlignHCenter
    spacing: ThemeController.style.margin.m8
    visible: !linkedAuthenticatorApp

    UIKit.BaseText {
      text: Strings.didntReceiveText
    }

    UIKit.BaseText {
      text: Strings.sendAgain
      color: ThemeController.style.slatePurpleColor
      font.weight: Font.Bold
    }
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
