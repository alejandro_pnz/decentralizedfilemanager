import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQml.Models 2.12
import UIModel 1.0

Item {
  property alias fileViewer: fragment.fileViewer
  function handleIndexChanged(nowCurrent) {}

  Item {
    id: header
    anchors{
      top: parent.top
      right: parent.right
      left: parent.left
    }
    height: s(64)

    UIKit.BaseText {
      text: fragment.fileName
      size: UIKit.BaseText.TextSize.H3
      font.weight: Font.DemiBold
      elide: Text.ElideRight
      anchors {
        left: parent.left
        leftMargin: ThemeController.style.margin.m20
        right: toolbar.left
        rightMargin: ThemeController.style.margin.m16
        verticalCenter: parent.verticalCenter
      }
    }

    PdfToolbar {
      id: toolbar
      height: parent.height
      anchors.horizontalCenter: parent.horizontalCenter
      fragment: fragment
      state: "img"
    }

    RowLayout {
      spacing: ThemeController.style.margin.m8
      anchors {
        right: parent.right
        rightMargin: ThemeController.style.margin.m20
        verticalCenter: parent.verticalCenter
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/close.svg"
        onClicked: {
          fileEditor.fileViewerManager.closeFile(fragment.fileViewer.id)
        }
      }
    }

    Rectangle {
      height: s(1)
      width: parent.width
      color: ThemeController.style.mercuryColor
      anchors.bottom: parent.bottom
    }
  }

  Rectangle {
    anchors.top:  header.bottom
    anchors.bottom: parent.bottom
    width: parent.width
    radius: s(8)

    ImageViewer {
      id: fragment
      anchors.fill: parent
    }
  }

}
