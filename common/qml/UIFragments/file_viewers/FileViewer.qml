import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

Item {
  id: layout
  anchors.fill: parent
  //  boundsBehavior: Flickable.StopAtBounds
  //  contentHeight: content.height
  clip: true
  property var fileViewer //fileEditor.fileViewer
  property bool txtFileMode: false
  property string fileName: fileViewer.fileName
  property real scaleFactor: fileViewer.scaleFactor
  property int currentPage: 0
  property int horizontalPageMargins: ThemeController.style.margin.m16
  property int verticalPageMargins: ThemeController.style.margin.m16

  property bool _scrollingAutomaticaly: false
  property bool _fileViewerInitialized: false
  property bool _fitToWidthCalculated: false

  onFileViewerChanged: {
    if (fileViewer) {
      _fileViewerInitialized = false
      console.log("Restore scroll", fileViewer.contentX, fileViewer.contentY,
                  "old", listView.contentX, listView.contentY)
      listView.contentX = fileViewer.contentX
      listView.contentY = fileViewer.contentY
      _calculateFitToWidthScale()
      _fileViewerInitialized = true
    }
  }

  on_FitToWidthCalculatedChanged: {
    if (_fitToWidthCalculated) {
      if (app.isMobile) {
        console.log("Fit to width scale", fileViewer.fitToWidthScale)
        fitToWidth()
      }
    }
  }

  onWidthChanged: {
    // Recalculate the 'fit to width' scale
    if (fileViewer)
      _calculateFitToWidthScale()
  }

  function fitPage() {
    var horizontalScale = (listView.width - 2 * horizontalPageMargins) / fileViewer.maxWidth
    var pageHeight = fileViewer.pageHeight(currentPage)
    var verticalScale = (listView.height) / pageHeight
    fileViewer.scaleFactor = Math.min(horizontalScale, verticalScale)
  }

  function fitToWidth() {
    fileViewer.scaleFactor = fileViewer.fitToWidthScale
  }

  function nextPage() {
    fileViewer.currentPage++
    scrollToCurrentPage()
  }

  function previousPage() {
    fileViewer.currentPage--
    scrollToCurrentPage()
  }

  function lastPage() {
    fileViewer.currentPage = fileViewer.pageCount - 1
    scrollToCurrentPage()
  }

  function firstPage() {
    fileViewer.currentPage = 0
    scrollToCurrentPage()
  }

  function scrollToCurrentPage() {
    _scrollingAutomaticaly = true
    listView.positionViewAtIndex(fileViewer.currentPage, ListView.Beginning)
    _scrollingAutomaticaly = false
  }

  function zoomIn() {
    fileViewer.zoomIn()
  }

  function zoomOut() {
    fileViewer.zoomOut()
  }

  function _checkCurrentPage(cntY) {
    var indexContentY = listView.indexAt(width/2, cntY)
    if (indexContentY == -1) {
      // This can be either spcaer or beggining
      indexContentY = listView.indexAt(width/2, cntY - listView.spacing)
      if (indexContentY == -1) {
        // Still invalid, so it's beggining
        fileViewer.currentPage = 0
        return
      }
    }
    _checkIndex(indexContentY)

    console.log(indexContentY)
  }

  function _calculateFitToWidthScale() {
    console.log("Calculate max width", listView.width, fileViewer.maxWidth, listView.width - 2 * horizontalPageMargins)
    fileViewer.fitToWidthScale = (listView.width - 2 * horizontalPageMargins) / fileViewer.maxWidth
    if (fileViewer.fitToWidthScale > 0.0) {
      _fitToWidthCalculated = true
    } else {
      _fitToWidthCalculated = false
    }
  }

  function _checkIndex(index) {
    var item = listView.itemAtIndex(index)
    if (item.y + item.height * 0.5 < listView.contentY) {
      fileViewer.currentPage = Math.min(index + 1, fileViewer.pageCount - 1)
    } else {
      fileViewer.currentPage = index
    }
  }

  Rectangle {
    anchors.fill: parent
    color: ThemeController.style.mercuryColor
  }

  ListView {
    id: listView
    anchors.fill: parent
    anchors.topMargin: verticalPageMargins
    anchors.bottomMargin: verticalPageMargins
    spacing: ThemeController.style.margin.m16
    model: fileViewer.pageCount
    contentWidth: fileViewer.maxWidth * fileViewer.scaleFactor
    currentIndex: fileViewer.currentPage
    flickableDirection: Flickable.AutoFlickIfNeeded

    onContentYChanged: {
      if (_fileViewerInitialized) {
        fileViewer.contentY = contentY
        console.log("Store scrolling", contentY, fileViewer.contentY)
      }
      if (!_scrollingAutomaticaly) {
        _checkCurrentPage(listView.contentY)
      }
    }
    onContentXChanged: {
      if (_fileViewerInitialized)
        fileViewer.contentX = contentX
    }

    ScrollBar.vertical: ScrollBar { }
    ScrollBar.horizontal: ScrollBar {
      id: hbar
      policy: ScrollBar.AlwaysOn
      visible: listView.contentWidth > listView.width
      anchors.bottom: listView.bottom
      anchors.bottomMargin: -listView.anchors.bottomMargin
    }

    PinchArea {
      id: pinchArea
      anchors.fill: listView
      pinch.maximumScale: 5.0
      pinch.minimumScale: 1.0
      pinch.dragAxis: Pinch.XAndYAxis
      onPinchUpdated: {
        fileViewer.scaleFactor = fileViewer.scaleFactor * pinch.scale
      }

      MouseArea {
        anchors.fill: parent
        onDoubleClicked: fitToWidth()
      }
    }

    delegate:  Rectangle {
      x: {
        if (listView.contentWidth > listView.width) {
          if (width > listView.width) return 0;
          return listView.contentWidth - width / 2
        } else {
          return (listView.width - width) / 2
        }
      }
      width: fileViewer.pageWidth(index) * fileViewer.scaleFactor
      height: fileViewer.pageHeight(index) * fileViewer.scaleFactor
      //        anchors.centerIn: parent
      border.width: 1
      property bool idx: index

      Image {
        id: pageImage
        anchors.fill: parent
        asynchronous: true
        cache: true
        source: "image://viewer/" + fileViewer.id + "/" + index

        UIKit.PulseLoader {
          id: busyIndicator
          anchors.centerIn: parent
          color: ThemeController.style.button.primary.bgColor
          width: 100
          height: 50
          visible: running
          running: pageImage.status === Image.Loading
        }
      }
    }
  }
}
