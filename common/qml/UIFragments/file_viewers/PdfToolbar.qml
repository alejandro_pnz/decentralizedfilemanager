import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQml.Models 2.12
import UIModel 1.0

RowLayout {
  property var fragment
  spacing: ThemeController.style.margin.m20
  state: 'pdf'
  Rectangle {
    id: leftSeparator
    Layout.fillHeight: true
    width: s(1)
    color: ThemeController.style.mercuryColor
  }

  ButtonGroup {
    id: cursorButtonGroup
    exclusive: true
  }

  ButtonGroup {
    id: fitToButtonGroup
    exclusive: true
  }

  UIKit.LargeIconButton {
    id: cursorButton
    icon.source: "qrc:/cursor.svg"
    overlay.visible: true
    checkable: true
    checked: !fragment.fileViewer.canDrag
    ButtonGroup.group: cursorButtonGroup
  }

  UIKit.LargeIconButton {
    id: handButton
    icon.source: "qrc:/hand.svg"
    checkable: true
    checked: fragment.fileViewer.canDrag ? true : false
    ButtonGroup.group: cursorButtonGroup
    onCheckedChanged: {
      if (checked) {
        fragment.fileViewer.canDrag = true
      } else {
        fragment.fileViewer.canDrag = false
      }
    }
  }

  Rectangle {
    Layout.fillHeight: true
    width: s(1)
    color: ThemeController.style.mercuryColor
  }

  UIKit.LargeIconButton {
    id: zoomOut
    icon.source: "qrc:/zoom-out.svg"
    overlay.visible: false
    onClicked: fragment.zoomOut()
  }

  UIKit.TextFrame {
    text: qsTr("%1%").arg(Math.round(fragment.scaleFactor * 100))
  }

  UIKit.LargeIconButton {    
    icon.source: "qrc:/zoom-in.svg"
    overlay.visible: false
    onClicked: fragment.zoomIn()
  }

  Rectangle {
    Layout.fillHeight: true
    width: s(1)
    color: ThemeController.style.mercuryColor
  }

  UIKit.LargeIconButton {
    id: fitPage
    icon.source: "qrc:/fit-one-page.svg"
    onClicked: fragment.fitPage()
    checkable: true
    ButtonGroup.group: fitToButtonGroup
  }

  UIKit.LargeIconButton {
    id: fitToWidth
    icon.source: "qrc:/fit-to-width.svg"
    onClicked: fragment.fitToWidth()
    checkable: true
    checked: true
    onCheckedChanged: console.log("Checked", checked)
    ButtonGroup.group: fitToButtonGroup
  }

  Rectangle {
    Layout.fillHeight: true
    width: s(1)
    color: ThemeController.style.mercuryColor
  }

  states: [
    State {
      name: "pdf"
      PropertyChanges {
        target: cursorButton
        visible: true
      }
      PropertyChanges {
        target: handButton
        visible: true
      }
      PropertyChanges {
        target: leftSeparator
        visible: true
      }
    },
    State {
      name: "img"
      PropertyChanges {
        target: cursorButton
        visible: false
      }
      PropertyChanges {
        target: handButton
        visible: false
      }
      PropertyChanges {
        target: leftSeparator
        visible: false
      }
    }
  ]
}
