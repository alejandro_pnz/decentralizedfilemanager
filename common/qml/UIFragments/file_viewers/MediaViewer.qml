import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects


import QmlVlc 0.1
import QtMultimedia 5.12

Item {
  id: mediaPlayerDelegate
  implicitWidth: parent.width
  implicitHeight: s(500)
  property string imgSrc: ""
  property string txt: ""
  property var fileViewer
  property alias vlcPlayer: vlcPlayer
//  property var vlcPlayer
  onFileViewerChanged: {
    if (fileViewer) {
      console.log("FILE VIEWER VALID", fileViewer.id)
//      vlcPlayer = fileViewer.player
    }
  }

  function handleIndexChanged(notCurrent) {
    console.log("Current index changed", notCurrent, vlcPlayer.playing)
    if (notCurrent) {
      console.log("Current index changed Pause playing!")
      if (vlcPlayer.playing)
        vlcPlayer.pause()
    }
  }

  property var objectId: fileViewer ? fileViewer.id : fileEditor.object.id

  property alias playButton: toolBar.pauseButton
  property alias textItem: toolBar.startTxt

  property alias bgRect: contentRect

  signal playButtonClicked()
  signal expandButtonClicked()

  Connections {
    target: mediaPlayerDelegate
    function onPlayButtonClicked() {
      if (vlcPlayer.state === VlcPlayer.Paused || vlcPlayer.state === VlcPlayer.Playing)
        vlcPlayer.togglePause()
      else
        vlcPlayer.play(fileViewer.device)
    }
  }

  Rectangle {
    id: contentRect
    anchors.fill: parent
    radius: s(4)
    color: ThemeController.style.mediaPlayerDelegate.bgColor
    border.color: ThemeController.style.mediaPlayerDelegate.bdColor
    border.width: s(1)

    VlcPlayer {
      id: vlcPlayer;
      onVolumeChanged: console.log("Video volume changed", vlcPlayer.volume)
      onPositionChanged: console.log("Position changed", vlcPlayer.position)
    }

    UIKit.CVideoOutput {
      anchors.bottom: toolBar.top
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.top: parent.top
      source: mediaPlayerDelegate.vlcPlayer
      vlcPlayer: mediaPlayerDelegate.vlcPlayer
      objectId: mediaPlayerDelegate.objectId
      hasVideo: fileViewer ?
                  fileViewer.hasVideo : true
    }

    UIKit.MediaPlayerToolbar {
      id: toolBar
      height: s(56)
      anchors {
        bottom: parent.bottom
        left: parent.left
        right: parent.right
        leftMargin: s(1)
        rightMargin: s(1)
        bottomMargin: s(1)
      }
      mode: fileViewer ?
              fileViewer.hasVideo ? UIKit.MediaPlayerToolbar.DisplayMode.Video : UIKit.MediaPlayerToolbar.DisplayMode.Audio
            : UIKit.MediaPlayerToolbar.DisplayMode.Video
      playing: vlcPlayer.playing
      onPlay: playButtonClicked()
      onExpand: expandButtonClicked()
      slider.value: vlcPlayer.state === VlcPlayer.Ended ? 1.0 : vlcPlayer.position
      slider.onMoved: {
        console.log("Slider moved", slider.value)
        vlcPlayer.position = slider.value
      }
      volumeSlider.slider.value: {
        console.log("VOLUME", vlcPlayer.volume)
        return vlcPlayer.volume
      }
      volumeSlider.slider.onMoved: {
        console.log("Set new volume", volumeSlider.slider.value)
        vlcPlayer.volume = volumeSlider.slider.value
      }
      startTxt.text: vlcPlayer.state === VlcPlayer.Ended
                     ? Utility.formatPlayerTime(vlcPlayer.length)
                     : Utility.formatPlayerTime(vlcPlayer.time)
      endTxt.text: Utility.formatPlayerTime(vlcPlayer.length)
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: contentRect
    source: contentRect
    color: ThemeController.style.mediaPlayerDelegate.dropShadowColor
    radius: s(10)
    horizontalOffset: 0
    verticalOffset: s(2)
    //samples: radius * 2 + 1
  }
}
