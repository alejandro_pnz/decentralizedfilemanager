import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQml.Models 2.12
import UIModel 1.0

//import QtWebView 1.1

Item {
  property var fileViewer
  function handleIndexChanged(nowCurrent) {}

  Item {
    id: header
    anchors{
      top: parent.top
      right: parent.right
      left: parent.left
    }
    height: s(64)

    UIKit.BaseText {
      text: fileViewer.fileName
      size: UIKit.BaseText.TextSize.H3
      font.weight: Font.DemiBold
      elide: Text.ElideRight
      anchors {
        left: parent.left
        leftMargin: ThemeController.style.margin.m20
        right: toolbar.left
        rightMargin: ThemeController.style.margin.m16
        verticalCenter: parent.verticalCenter
      }
    }

    RowLayout {
      spacing: ThemeController.style.margin.m8
      anchors {
        right: parent.right
        rightMargin: ThemeController.style.margin.m20
        verticalCenter: parent.verticalCenter
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/close.svg"
        onClicked: {
          fileEditor.fileViewerManager.closeFile(fileViewer.id)
        }

        visible: !app.isTablet
      }
    }

    Rectangle {
      height: s(1)
      width: parent.width
      color: ThemeController.style.mercuryColor
      anchors.bottom: parent.bottom
    }

    visible: app.isMobile
  }

  Rectangle {
    anchors.top: header.visible ? header.bottom : parent.top
    anchors.bottom: parent.bottom
    width: parent.width
    radius: s(8)

    GenericWebView {
      id: webview
      anchors.fill: parent
      url: "http://localhost:44387/pdfviewer/WebViewer/samples/viewing/viewing/index.html?fileid=" + fileViewer.id + "." + fileViewer.fileType
    }
  }
}
