import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

Item {
  anchors.fill: parent
  //  boundsBehavior: Flickable.StopAtBounds
  //  contentHeight: content.height
  clip: true

  property var fileViewer
  property string fileName: fileViewer.fileName
  property real scaleFactor: fileViewer.scaleFactor
  property int horizontalPageMargins: ThemeController.style.margin.m16
  property int verticalPageMargins: ThemeController.style.margin.m16

  function fitToWidth() {
    fileViewer.scaleFactor = fileViewer.fitToWidthScale
  }

  function fitPage() {
    var horizontalScale = (listView.width - 2 * horizontalPageMargins) / fileViewer.sourceSize.width
    var imageHeight = fileViewer.sourceSize.height
    var verticalScale = (listView.height) / imageHeight
    fileViewer.scaleFactor = Math.min(horizontalScale, verticalScale)
  }
  function _calculateFitToWidthScale() {
    console.log("Calculate max width", listView.width, listView.width - 2 * horizontalPageMargins)
    fileViewer.fitToWidthScale = (listView.width - 2 * horizontalPageMargins) / fileViewer.sourceSize.width
    if (fileViewer.fitToWidthScale > 0.0) {
      _fitToWidthCalculated = true
    } else {
      _fitToWidthCalculated = false
    }
  }

  function zoomIn() {
    fileViewer.zoomIn()
  }

  function zoomOut() {
    fileViewer.zoomOut()
  }

  property bool _scrollingAutomaticaly: false
  property bool _fileViewerInitialized: false
  property bool _fitToWidthCalculated: false

  on_FitToWidthCalculatedChanged: {
    if (_fitToWidthCalculated) {
      if (fileViewer.sourceSize.width < listView.width) {
        // Image is smaller than vieport, dont scale
      } else {
        console.log("Fit to width")
        fitToWidth()
      }
    }
  }

  onWidthChanged: {
    // Recalculate the 'fit to width' scale
    if (fileViewer)
      _calculateFitToWidthScale()
  }

  onFileViewerChanged: {
    if (fileViewer) {
      _fileViewerInitialized = false
      console.log("Restore scroll", fileViewer.contentX, fileViewer.contentY,
                  "old", listView.contentX, listView.contentY)
      _calculateFitToWidthScale()
      _fileViewerInitialized = true
    }
  }

  ListView {
    id: listView
    anchors.fill: parent
    anchors.topMargin: verticalPageMargins
    anchors.bottomMargin: verticalPageMargins
    spacing: ThemeController.style.margin.m16
    model: 1
    contentWidth: fileViewer.sourceSize.width * fileViewer.scaleFactor
    flickableDirection: Flickable.AutoFlickIfNeeded

    ScrollBar.vertical: ScrollBar { }
    ScrollBar.horizontal: ScrollBar {
      id: hbar
      policy: ScrollBar.AlwaysOn
      visible: listView.contentWidth > listView.width
      anchors.bottom: listView.bottom
      anchors.bottomMargin: -listView.anchors.bottomMargin
    }

    PinchArea {
      id: pinchArea
      anchors.fill: listView
      pinch.maximumScale: 5.0
      pinch.minimumScale: 1.0
      pinch.dragAxis: Pinch.XAndYAxis
      property real initialScale

      onPinchStarted: {
        initialScale = fileViewer.scaleFactor
      }

      onPinchUpdated: {
        fileViewer.scaleFactor = initialScale * (pinch.scale)
      }

      MouseArea {
        anchors.fill: parent
        onDoubleClicked: {

          fitToWidth()
        }
      }
    }

    delegate:  Rectangle {
      x: {
        if (listView.contentWidth > listView.width) {
          if (width > listView.width) return 0;
          return listView.contentWidth - width / 2
        } else {
          return (listView.width - width) / 2
        }
      }
      width: fileViewer.sourceSize.width * fileViewer.scaleFactor
      height: fileViewer.sourceSize.height * fileViewer.scaleFactor
      onWidthChanged: console.log("Image width changed", width, fileViewer.sourceSize.width, fileViewer.scaleFactor)
      onHeightChanged: console.log("Image height changed", width, fileViewer.sourceSize.height, fileViewer.scaleFactor)

      Image {
        id: pageImage
        anchors.fill: parent
        asynchronous: true
        cache: true
        source: "image://imageViewer/" + fileViewer.id
        fillMode: Image.PreserveAspectFit

        UIKit.PulseLoader {
          id: busyIndicator
          anchors.centerIn: parent
          color: ThemeController.style.button.primary.bgColor
          width: 100
          height: 50
          visible: running
          running: pageImage.status === Image.Loading
        }
      }
    }
  }
}
