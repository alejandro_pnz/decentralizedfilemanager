import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQml.Models 2.12
import UIModel 1.0

//import QtWebView 1.1

Item {
  property alias fileViewer: fragment.fileViewer
  function handleIndexChanged(nowCurrent) {}

  Item {
    id: header
    anchors{
      top: parent.top
      right: parent.right
      left: parent.left
    }
    height: s(64)

    UIKit.BaseText {
      text: fileViewer ? fileViewer.fileName : ""
      size: UIKit.BaseText.TextSize.H3
      font.weight: Font.DemiBold
      elide: Text.ElideRight
      anchors {
        left: parent.left
        leftMargin: ThemeController.style.margin.m20
        right: buttonLayout.left
        rightMargin: ThemeController.style.margin.m16
        verticalCenter: parent.verticalCenter
      }
    }

    RowLayout {
      id: buttonLayout
      spacing: ThemeController.style.margin.m8
      anchors {
        right: parent.right
        rightMargin: ThemeController.style.margin.m20
        verticalCenter: parent.verticalCenter
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/close.svg"
        onClicked: {
          fileEditor.fileViewerManager.closeFile(fragment.fileViewer.id)
        }
      }
    }

    Rectangle {
      height: s(1)
      width: parent.width
      color: ThemeController.style.mercuryColor
      anchors.bottom: parent.bottom
    }
  }

  Item {
    id: searchPanel
    visible: false
    anchors{
      top: header.bottom
      right: parent.right
      left: parent.left
    }
    height: s(64)

    UIKit.FileSearchInput {
      id: search
      anchors.left: parent.left
      anchors.leftMargin: ThemeController.style.margin.m16
      anchors.verticalCenter: parent.verticalCenter
    }

    UIKit.StyledButton {
      anchors.left: search.right
      anchors.leftMargin: ThemeController.style.margin.m16
      anchors.verticalCenter: parent.verticalCenter
      text: Strings.done
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      type: UIKit.StyledButton.ButtonStyle.Text
      buttonStyle: ThemeController.style.clearTextButtonStyle
      onClicked: searchPanel.visible = false
    }

    Rectangle {
      height: s(1)
      width: parent.width
      color: ThemeController.style.mercuryColor
      anchors.bottom: parent.bottom
    }
  }

  Rectangle {
    anchors.top: searchPanel.visible ? searchPanel.bottom : header.bottom
    anchors.bottom: footer.visible ? footer.top : parent.bottom
    width: parent.width
    radius: s(8)

    RowLayout {
      anchors.fill: parent

      GenericWebView {
        id: webview
        url: {
          var url = "http://localhost:44387/mdeditor/tui.editor/example/index.html";
          if (fragment.fileViewer) {
            url += "?fileid=" + fragment.fileViewer.id + "." + fragment.fileViewer.fileType;
          }

          return url;
        }
        Layout.alignment: Qt.AlignHCenter
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.preferredWidth: Layout.maximumWidth
        Layout.maximumWidth: parent.width //Math.min(parent.width, s(792))
      }

      MarkdownPreview {
        id: fragment
        visible: false
        Layout.alignment: Qt.AlignHCenter
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.preferredWidth: Layout.maximumWidth
        Layout.maximumWidth: Math.min(parent.width, s(792))
      }
    }
  }

  Rectangle {
    id: footer
    height: s(64)
    width: parent.width
    radius: s(8)
    anchors.bottom: parent.bottom

    visible: false

    Rectangle {
      height: s(1)
      width: parent.width
      color: ThemeController.style.mercuryColor
      anchors.bottom: parent.top
    }

    UIKit.StyledButton {
      anchors {
        right: parent.right
        rightMargin: ThemeController.style.margin.m24
        verticalCenter: parent.verticalCenter
      }
      text: Strings.editInQuodArca
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      onClicked: {
        homeManager.enterEditMarkdownFile(fileViewer.id)
        app.activateMainWindow()
      }
    }
  }

  PdfViewerActionModel {
    id: actionModel
    onSearchMode: searchPanel.visible = true
  }

  function saveNote() {
    webview.runJavaScript("editor.getMarkdown()", function(result) {
      homeManager.saveNote(result);
    });
  }

  function saveMarkdown() {
    webview.runJavaScript("editor.getMarkdown()", function(result) {
      // Save file content
      fileEditor.saveMarkdownContent(result);
      //homeManager.back();

      // TODO:
      if (!homeManager.isPageOnStack(Pages.CreateFilePage)) {
          homeManager.openPage(Pages.CreateFilePage);
      }
    });
  }
}
