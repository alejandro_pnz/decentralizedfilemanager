import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

import com.testintl 1.0

EditPanel {
  id: syncDelegate

  title: Strings.syncStatus
  hasSave: false
  hasSpacer: true
  hasEdit: false
  enabled: canEdit

  // TODO connect with model
  property int syncStatus: SyncableObject.Private
  property bool hasWhitelist: false

  // Base delegate
  UIKit.CardDelegate {
    id: syncCard
    Layout.fillWidth: true
    title: {
      switch (syncStatus) {
      case SyncableObject.Private: return Strings.privateStr
      case SyncableObject.Public: return Strings.publicStr
      case SyncableObject.NoSync: return Strings.noSync
      }
    }
    text: {
      switch (syncStatus) {
      case SyncableObject.Private: return Strings.syncOnlyYourDevices
      case SyncableObject.Public: return Strings.syncWithOtherUsers
      case SyncableObject.NoSync: return Strings.fileOnlyOnThisDevice
      }
    }
    icon.source: {
      switch (syncStatus) {
      case SyncableObject.Private: return "qrc:/private.svg"
      case SyncableObject.Public: return "qrc:/public-dark.svg"
      case SyncableObject.NoSync: return "qrc:/no-sync-dark.svg"
      }
    }
    actionButton: true
    onActionButtonClicked: homeManager.openPage(Pages.SyncPage)
  }

  // Whitelist Delegate
  UIKit.CardDelegate {
    Layout.topMargin: app.isPhone ? -ThemeController.style.margin.m4 : 0
    Layout.fillWidth: true
    visible: hasWhitelist && syncStatus === SyncableObject.Public
    title: Strings.whitelist
    text: Strings.syncWithFewPeople
    icon.source: "qrc:/whitelist.svg"
    actionButton: true
    onActionButtonClicked: homeManager.openPage(Pages.ArcaUsersListPage)

    RowLayout {
      spacing: ThemeController.style.margin.m4
      Avatars {
        id: avatars
        model: UserListModel
        maxCount: 3
        alternativeColor: false
        showLastAvatar: false
      }
    }
  }

  UIKit.AddComponent {
    id: addButton
    visible: !hasWhitelist && syncStatus === SyncableObject.Public
    Layout.topMargin: app.isPhone ? 0 : ThemeController.style.margin.m8
    buttonText: Strings.addWhitelistBlacklist
    onAddButtonClicked: homeManager.openPage(Pages.ArcaUsersListPage)
  }
}
