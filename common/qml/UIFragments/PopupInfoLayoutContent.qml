import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

ColumnLayout {
  id: content
  spacing: ThemeController.style.margin.m16
  property string title: Strings.allowScan
  property string content: Strings.allowScanQuestion
  property string infoboxText: Strings.allowScanInfoBox

  signal closeClicked()

  UIKit.PopupHeader {
    id: popupHeader
    Layout.fillWidth: true
    title: content.title
    onCloseClicked: content.closeClicked()
  }

  UIKit.BaseText {
    text: content.content
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    horizontalAlignment: Text.AlignHCenter
  }

  // Info box
  UIKit.Infobox {
    Layout.topMargin: ThemeController.style.margin.m12
    Layout.fillWidth: true
    text: infoboxText
    visible: text.length > 0
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
