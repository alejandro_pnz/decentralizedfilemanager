import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2
import com.testintl 1.0

Item {
  id: root

  readonly property bool isMobile: app.isPhone

  Item {
    id: noItemsInfo
    anchors.fill: parent
    anchors.topMargin: isMobile ? 0 : -ThemeController.style.margin.m24
    visible: quodAndFilesModel.count === 0

    ColumnLayout {
      width: parent.width
      spacing: ThemeController.style.margin.m12
      anchors.centerIn: isMobile ? undefined : parent
      anchors.top: isMobile ? parent.top : undefined

      SvgImage {
        source: "qrc:/alert-circle.svg"
        width: s(100)
        height: s(100)
        Layout.alignment: Qt.AlignHCenter
      }

      BaseText {
        Layout.topMargin: isMobile ? s(3) : ThemeController.style.margin.m20
        text: Strings.noObjectsLinked
        font.weight: Font.Bold
        size: BaseText.TextSize.H3
        Layout.alignment: Qt.AlignHCenter
        horizontalAlignment: Qt.AlignHCenter
      }

      BaseText {
        text: Strings.noLinkedToThisArca.arg("<a href='#linkobjectpage' style='text-decoration:none'><font color='#5C5AE8'><b>%2</font></b></a>").arg(Strings.linkQuodOrFiles)
        textFormat: Text.RichText
        Layout.alignment: Qt.AlignHCenter
        Layout.preferredWidth: s(295)
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        horizontalAlignment: Qt.AlignHCenter
        onLinkActivated: homeManager.enterPage(Pages.SelectObjectsPage)
      }
    }
  }

  ColumnLayout {
    id: linkedColumn
    spacing: ThemeController.style.margin.m16
    visible: !noItemsInfo.visible
    anchors.fill: parent

    RowLayout {
      visible: isMobile
      BaseText {
        font.weight: Font.Bold
        text: Strings.quodAndFiles
        Layout.alignment: Qt.AlignLeft
      }

      Item {
        Layout.preferredHeight: s(1)
        Layout.fillWidth: true
      }

      StyledButton {
        text: Strings.edit
        type: StyledButton.ButtonStyle.Text
        displayMode: StyledButton.DisplayMode.TextOnly
        onClicked: homeManager.enterPage(Pages.SelectObjectsPage)
        Layout.alignment: Qt.AlignRight
      }
    }

    SortFilterProxyModel {
      id: quodAndFilesModel
      sourceModel: QuodFileListModel
      filters: ListFilter {
        roleName: "id"
        list: arcaEditor.linkedObjectsIdList
      }
    }

    ListView {
      id: listView
      Layout.fillWidth: true
      Layout.fillHeight: true
      clip: true
      spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16

      model: quodAndFilesModel
      delegate: ObjectDelegate {
        title: model.name
        text: model.content
        avatar.shareMode: model.syncProperty
        width: listView.width
        actionButton: false
        titleIcon: model.locked
        avatar.iconSrc: "image://item/type=" + model.objectType + "&id=" + model.id

        onTextLinkActivated: {
          homeManager.openOneArcaPage(link)
        }
      }
    }
  }
}
