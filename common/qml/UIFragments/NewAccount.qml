import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

import com.testintl 1.0

import QtPromise 1.1

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m16

  property string title
  property string contentText: ""
  property string buttonText
  signal showHint(point globalPos)
  signal buttonClicked(string email, string passphrase)
  property bool overrideButtonClick: false

  property alias tipButtonWidth: passphraseInput.tipsButtonWidth

  readonly property bool error: emailInput.state === 'error' || passphraseInput.state === 'error' || repeatPassphraseInput.state === 'error'

  property bool busy: false

  enabled: !layout.busy

  Connections {
    target: pageManager
    function onSetupCreateAccountPage(config) {
      if (config.type !== undefined)
        layout.type = config.type
      layout.title = config.title
      if (config.contentText !== undefined)
        layout.contentText = config.contentText
      layout.buttonText = config.buttonText
      if (config.buttonHandler !== undefined) {
        layout.buttonClicked.connect(config.buttonHandler)
        overrideButtonClick = true
      }
      if(config.noEmail !== undefined) {
        emailInput.visible = !config.noEmail
      }
    }
  }

  Connections {
    target: RegistrationManager
    function onEmailAlreadyUsed(config) {
      if (config.emailError !== undefined) {
        emailInput.errorMessage = [config.emailError]
        emailInput.state = 'error'
      }
    }
  }

  Connections {
    target: layout
    function onBusyChanged() {
      if (!layout.busy) {
        if (layout.error) {
          passphraseInput.textField.text = ""
          repeatPassphraseInput.textField.text = ""
        }
        else {
          emailInput.textField.text = ""
          passphraseInput.textField.text = ""
          repeatPassphraseInput.textField.text = ""
        }
      }
    }
  }

  UIKit.BaseText {
    text: title
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Text.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.BaseText {
    text: contentText
    visible: contentText.length > 0
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Text.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.InputField {
    id: emailInput
    Layout.topMargin: ThemeController.style.margin.m8
    labelText: Strings.email
    placeholderTxt: Strings.typeEmail
    clearIconVisible: true
    inputRequired: true
    Layout.fillWidth: true
    fieldType: UIKit.InputField.FieldType.Email
    onTabPressed: passphraseInput.textField.forceActiveFocus()
    onEnterPressed: passphraseInput.textField.forceActiveFocus()
    onBacktabPressed: repeatPassphraseInput.textField.forceActiveFocus()
  }

  UIKit.TipsInput {
    id: passphraseInput
    labelText: Strings.passphrase
    placeholderTxt: Strings.examplePassphrase
    strengthIndicatorVisible: true
    inputRequired: true
    isPassPhrase: true
    Layout.fillWidth: true
    onTabPressed: repeatPassphraseInput.textField.forceActiveFocus()
    onEnterPressed: repeatPassphraseInput.textField.forceActiveFocus()
    onBacktabPressed: emailInput.textField.forceActiveFocus()

    onHandleShowHint: {
      showHint(p)
    }

    onTextChanged: {
      var promise = Future.promise(PasswordStrengthEvaluator.strength(text))

      promise.then(function(strength) {
        if (strength === PasswordStrengthEvaluator.VeryStrongPassword ||
            strength === PasswordStrengthEvaluator.StrongPassword) {
          passphraseInput.passwordStrengthIndicator.state = 'high'
        }
        else if (strength === PasswordStrengthEvaluator.ReasonablePassword) {
          passphraseInput.passwordStrengthIndicator.state = 'medium'
        }
        else if (strength === PasswordStrengthEvaluator.WeakPassword) {
          passphraseInput.passwordStrengthIndicator.state = 'low'
        }
        else {
          passphraseInput.passwordStrengthIndicator.state = 'verylow'
        }
      });
    }
  }

  UIKit.InputField {
    id: repeatPassphraseInput
    labelText: Strings.passphrase
    placeholderTxt: Strings.examplePassphrase
    inputRequired: true
    isPassPhrase: true
    eyeIconVisible: true
    Layout.fillWidth: true
    onTabPressed: emailInput.textField.forceActiveFocus()
    onBacktabPressed: repeatPassphraseInput.textField.forceActiveFocus()
    onEnterPressed: actionButton.clicked()
  }

  UIKit.StyledButton {
    id: actionButton
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    text: buttonText
    textItem.visible: !pulseLoader.visible
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      var emailOK = validateEmailInput(emailInput)
      var passOK = validatePassphraseInput(passphraseInput, repeatPassphraseInput)

      if (emailOK && passOK) {
        // TODO do some SDK processing
        buttonClicked(emailInput.textField.text, passphraseInput.textField.text)
        if (!overrideButtonClick)
          pageManager.enterGenericInfoPage({title: Strings.checkYourEmail,
                                                                  content: Strings.weJustSentEmail
                                                                  .arg("<font color=\"%4\"><b>")
                                                                  .arg(emailInput.textField.text)
                                                                  .arg("</b></font>")
                                                                  .arg(ThemeController.style.slatePurpleColor),
                                                                  primaryButtonText: Strings.openMailApp,
                                                                  secondaryButtonText: Strings.sendAgain,
                                                                  secondaryButtonLabel: Strings.didntReceiveEmail,
                                                                  type: Pages.Double,
                                                                  primaryButtonHandler: function(){RegistrationManager.emailValidated()},
                                                                  secondaryButtonHandler: function(){}})
      }
    }

    UIKit.PulseLoader {
      id: pulseLoader
      anchors.centerIn: parent
      visible: layout.busy
    }
  }
}
