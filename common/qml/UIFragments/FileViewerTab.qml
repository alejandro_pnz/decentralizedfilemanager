import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects


import QmlVlc 0.1
import QtMultimedia 5.12

Column {
  id: column
  width: parent.width
  height: parent.height
  spacing: 0

  property bool showImage: true
  //for tests
  property string imgSource

  Item {
    id: imageDelegate
    width: parent.width
    height: parent.height - ThemeController.style.margin.m16
    visible: showImage

    Rectangle {
      id: content
      width: parent.width
      height: parent.height
      radius: s(4)

      Image {
        id: image
        anchors {
          top: parent.top
          left: parent.left
          bottom: parent.bottom
        }

        // TODO remove temporary placeholder for mp3 when waves generator will be ready
        source: fileEditor.object.fileType == "mp3"
                ? "qrc:/audio_preview_waves.png"
                : "image://photo/" + fileEditor.object.id
        fillMode: Image.PreserveAspectFit

        layer.enabled: true
        layer.effect: OpacityMask {
          maskSource: Item {
            width: image.width
            height: image.height
            Rectangle {
              anchors.centerIn: parent
              width: image.width
              height: image.height
              radius: s(4)
            }
          }
        }

        StyledButton {
          id: addButton
          icon.source: "qrc:/expand.svg"
          displayMode: StyledButton.DisplayMode.IconOnly
          anchors.top: parent.top
          anchors.topMargin: ThemeController.style.margin.m20
          anchors.right: parent.right
          anchors.rightMargin: ThemeController.style.margin.m20
          width: s(48)
          height: s(48)
          onClicked: {
            console.log("Clicked")
            if (app.isMobile) {
              switch (fileEditor.fileViewer.type) {
              case FileViewerClass.VideoType:
                if (fileEditor.fileViewer.hasVideo) {
                  homeManager.openPage(Pages.VideoViewerPage)
                }
                else {
                  homeManager.openPage(Pages.AudioViewerPage)
                }
                break;
              case FileViewerClass.DocumentType:
                homeManager.openPage(Pages.PdfViewerPage)
                break;
              case FileViewerClass.ImageType:
                homeManager.openPage(Pages.ImageViewerPage)
                break;
              case FileViewerClass.MarkdownType:
                homeManager.openPage(Pages.MarkdownViewerPage)
              }
            }
            else {
              fileEditor.openFile()
              homeManager.enterFileViewerWindow()
            }
          }
        }
      }

      DropShadow {
        id: dropShadow
        anchors.fill: content
        horizontalOffset: s(4)
        verticalOffset: s(4)
        radius: s(20)
        //samples: 2 * radius + 1
        color: Qt.rgba(0, 0, 0, 0.04)
        source: content
      }
    }
  }
}

