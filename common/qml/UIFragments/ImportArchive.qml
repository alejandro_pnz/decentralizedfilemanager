import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m16

  UIKit.InfoDelegate {
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 :
                                     ThemeController.style.margin.m24
    contentText: Strings.archiveProtected
  }

  UIKit.InputField {
    labelText: Strings.archivePassphrase
    placeholderTxt: Strings.typePassphrase
    inputRequired: false
    isPassPhrase: true
    clearIconVisible: false
    eyeIconVisible: true
    Layout.fillWidth: true
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
