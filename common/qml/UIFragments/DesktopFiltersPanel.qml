import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import QtQml.Models 2.12
import UIModel 1.0 as UIModel

Column {
  id: panel
  property var sModel

  function showTags(xPos, buttonWidth) {
    tagsFilters.x = xPos - tagsFilters.width * 0.5 + buttonWidth * 0.5
    tagsFilters.y = panel.height + ThemeController.style.margin.m8
    tagsFilters.open()
  }

  function showDate(filter, xPos, buttonWidth) {
    dateFilter.filterType = filter
    dateFilter.x = xPos - dateFilter.width * 0.5 + buttonWidth * 0.5
    dateFilter.y = panel.height + ThemeController.style.margin.m8
    dateFilter.open()
  }

  signal showMore()

  Flow {
    anchors.left: parent.left
    anchors.right: parent.right
    spacing: ThemeController.style.margin.m8
    Repeater {
      id: repeater
      model: filtersModel.model
      UIKit.DesktopSearchFilterDelegate {
        text: name
        icon.source: iconSource
        closeBtn.visible: applied
        action: actionSource
        details: det
        filterEnum: fEnum
        onCloseButtonClicked: {
          filtersModel.removeFilter(fenum)
        }
      }
    }
  }

  UIModel.FiltersModel {
    id: filtersModel
    searchModel: sModel
  }

  UIKit.DateFilterPopup {
    id: dateFilter
    sModel: panel.sModel
  }

  UIKit.TagsFiltersPopup {
    id: tagsFilters
    sModel: panel.sModel
  }
}

