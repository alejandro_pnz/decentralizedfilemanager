import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0

ColumnLayout {
  id: content
  anchors.fill: parent
  spacing: ThemeController.style.margin.m16
  property string ownerName: ""

  function validate() {
    var ok = validateMandatoryField(nameInput)
    return ok
  }

  function save() {
    homeManager.currentEditor.name = nameInput.textField.text
    homeManager.currentEditor.save()
  }

  Connections {
    target: homeManager
    function onSetupEditGeneralDetailsPage(config) {
      content.ownerName = config.ownerName
    }
  }

  UIKit.InputField {
    id: nameInput
    Layout.topMargin: ThemeController.style.margin.m16
    labelText: Strings.name
    textField.text: homeManager.currentEditor.object.name
    inputRequired: true
    clearIconVisible: true
  }

  UIKit.InputField {
    id: ownerInput
    labelText: Strings.owner
    textField.text: ownerName
    inputRequired: false
    clearIconVisible: false
    rightIconSource: "qrc:/chevron-down.svg"
    enabled: false
    MouseArea {
      anchors.fill: parent
      onClicked: {
        console.log("Clicked")
        homeManager.enterChangeOwnerPage(SyncableObject.Arca)
      }
    }
  }


  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
