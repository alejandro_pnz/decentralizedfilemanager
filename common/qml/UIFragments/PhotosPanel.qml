import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import UIFragments 1.0

import com.testintl 1.0
import SortFilterProxyModel 0.2

EditPanel {
  id: photoPanel

  title: Strings.photos
  hasSave: !app.isPhone
  hasSpacer: false
  hasBottomSpacer: true
  hasEdit: true
  hasAdd: fileProxy.count <= 0
  enabled: canEdit

  property int errorsCount: 0
  property bool hasPhotos: fileProxy.count > 0

  onEditButtonClicked: handleEditButton()
  onSaveButtonClicked: save()
  onAddButtonClicked: handleAddButton()

  signal focusInput(var idx)
  signal focusNextSection()
  signal focusPreviousSection()
  signal inputActivated(var globalPos, var height)

  function focusFirstInput() {
    focusInput(0)
  }

  function focusLastInput() {
    focusInput(editGrid.count - 1)
  }


  Item {
    SortFilterProxyModel {
      id: fileProxy
      //sourceModel: FileListModel
      filters: [
        ListFilter {
          roleName: "id"
          list: quodEditor.photoEditor.photosIds
          enabled: true
        }
      ]
    }
    ImportFileDialog {
      id: fileDialog
      onAccepted: {
          console.log("IMPORT FILES", files)
          quodEditor.photoEditor.importFiles(files)
      }
      onRejected: {
        if (fileProxy.count <= 0)
          photoPanel.finishEditing()
      }
    }

    PhotoActionMenu {
      id: actionMenu
    }
  }

  UIKit.InfoDelegate {
    contentText: Strings.addPhotoInfo
    visible: fileProxy.count <= 0
    Layout.preferredHeight: visible ? implicitHeight : 0
    Layout.minimumHeight: Layout.preferredHeight
    Layout.fillWidth: true
  }

  UIKit.AddComponent {
    visible: editMode && fileProxy.count > 0 && app.isPhone
    Layout.topMargin: app.isPhone ? 0 : ThemeController.style.margin.m8
    buttonText: Strings.addAnotherPicture
    onAddButtonClicked: handleAddButton()
  }

  UIKit.ResponsiveGridLayout {
    id: editGrid
    visible: editMode && fileProxy.count > 0
    Layout.fillWidth: true
    Layout.rightMargin: -horizontalSpacing
    Layout.preferredHeight: visible ? editGrid.contentHeight : 0
    Layout.minimumHeight: visible ? editGrid.contentHeight : 0
    delegateMaximumWidth: app.isPhone ? parent.width : s(394)
    delegateMinimumWidth: s(250)
    horizontalSpacing: app.isPhone ? ThemeController.style.margin.m8 : ThemeController.style.margin.m32
    verticalSpacing: ThemeController.style.margin.m16
    // Unfortunately the height of the delegate has to be explicitly defined
    delegateHeight: (cellWidth - horizontalSpacing) * 0.75 // Image size
                    + s(16) + s(80) // Spacing + input size
                    + (app.isPhone ? ThemeController.style.defaultInputHeight + s(16) : 0)

    model: fileProxy
    delegate: Item {
      id: editGridDelegate
      width: editGrid.cellWidth
      height: editGrid.cellHeight
      property var i: model.index
      property var fileId: model.id
      property alias fileName: panel.typedName
      function validate() {
        return panel.validate()
      }

      Connections {
        target: photoPanel
        function onFocusInput(idx) {
          if (editGridDelegate.i === idx) {
            panel.activateInput()
          }
        }
      }

      EditPhotoDelegate {
        id: panel
        editMode: photoPanel.editMode
        width: editGrid.maximumCellContentWidth
        height: editGrid.delegateHeight
        photoName: model.name
        photoSource: "image://photo/" + model.id

        onDeleteRequest: {
          quodEditor.photoEditor.remove(model.id)
        }

        onRetakeRequest: {
          quodEditor.photoEditor.retake(model.id)
          homeManager.openPage(Pages.CameraCapturePage)
        }

        onTabPressed: {
          if (editGridDelegate.i + 1 == editGrid.count) {
            focusNextSection()
          } else {
            focusInput(editGridDelegate.i + 1)
          }
        }
        onBacktabPressed: {
          if (editGridDelegate.i === 0) {
            focusPreviousSection()
          } else {
            focusInput(editGridDelegate.i - 1)
          }
        }
        onInputActivated: photoPanel.inputActivated(globalPos, height)
      }
    }
  }

  UIKit.ResponsiveGridLayout {
    id: flow
    visible: !editMode && fileProxy.count > 0
    Layout.fillWidth: true
    Layout.rightMargin: -horizontalSpacing
    Layout.preferredHeight: visible ? flow.contentHeight : 0
    Layout.minimumHeight: visible ? flow.contentHeight : 0
    delegateMaximumWidth: s(app.isPhone ? 167 : 181)
    delegateMinimumWidth: s(app.isPhone ? 167 : 181)
    horizontalSpacing: app.isPhone ? ThemeController.style.margin.m8 : ThemeController.style.margin.m32
    verticalSpacing: ThemeController.style.margin.m16
    delegateHeight: (cellWidth - horizontalSpacing) * s(170.0 / (app.isPhone ? 167.0 : 181.0))

    onWidthChanged: {
      console.log("PW", cellWidth, cellHeight, delegateHeight, cellCount, count, s(170.0 / (app.isPhone ? 167.0 : 181.0)), cellWidth, horizontalSpacing, (cellWidth - horizontalSpacing) * s(170 / app.isPhone ? 167 : 181))
    }

    model: fileProxy
    delegate: Item {
      width: flow.cellWidth
      height: flow.cellHeight

      UIKit.ImageListDelegate {
        id: panels
        implicitWidth: 0
        implicitHeight: 0
        width: flow.maximumCellContentWidth
        height: flow.delegateHeight
        text: model.name
        icon.source: "image://photo/" + model.id
        onActionButtonClicked: {
          console.log("Action menu clicked", model.object)
          actionMenu.currentItem = model.object
          actionMenu.popup.open()
        }
      }
    }
  }

  UIKit.AddComponent {
    visible: editMode && fileProxy.count > 0 && !app.isPhone
    Layout.topMargin: app.isPhone ? 0 : ThemeController.style.margin.m8
    buttonText: Strings.addAnotherPicture
    onAddButtonClicked: handleAddButton()
  }

  function handleEditButton() {
    if (app.isPhone) {
      finishEditing()
      homeManager.openPage(Pages.EditPhotosPage)
    }
  }

  function handleAddButton() {
    if (app.isMobile) {
      if (app.isPhone) finishEditing()
      //Show popup to choose the source - camera or files
      homeManager.openPage(Pages.ImportSourcePopupPage)
    } else {
      // Import from files
      fileDialog.open()
    }
  }

  function save() {
    for (var i = 0; i < editGrid.count; ++i) {
      var item = editGrid.itemAtIndex(i)
      quodEditor.photoEditor.setName(item.fileId, item.fileName)
    }
    quodEditor.photoEditor.save()
  }
}
