import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  spacing: ThemeController.style.margin.m16

  Rectangle {
    Layout.preferredHeight: s(app.isMobile ? 188 : 230)
    Layout.preferredWidth: parent.width
    radius: s(10)
    border.width: s(3)
    border.color: ThemeController.style.slatePurpleColor
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.BaseText {
    Layout.topMargin: ThemeController.style.margin.m24
    Layout.fillWidth: true
    text: Strings.whoIsOwner
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.InputField {
    id: emailInput
    Layout.topMargin: ThemeController.style.margin.m8
    labelText: Strings.email
    placeholderTxt: Strings.typeEmail
    inputRequired: true
    fieldType: UIKit.InputField.FieldType.Email
    onEnterPressed: button.clicked()
  }

  UIKit.StyledButton {
    id: button
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    text: Strings.continueTxt
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      if (validateEmailInput(emailInput)) {
        RegistrationManager.newDeviceRegisteredUser()
        emailInput.textField.text = ""
      }
    }
  }
}
