import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import QtQml.Models 2.12
import UIModel 1.0 as UIModel

CheckableTextLabelPanel {
  id: panel
  title: Strings.quodType
  onDelegateClicked: {
    searchModel.filters.filter(SFilter.QuodTypeFilter).toggle(index, ItemSelectionModel.Toggle)
  }

  onSearchModelChanged: {
    if (panel.searchModel) {
      var m = searchModel.filters.filter(SFilter.QuodTypeFilter);
      panel.model = m.model
      panel.ism = m.selectionModel
    }
  }
}
