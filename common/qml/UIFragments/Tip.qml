import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects

Item {
    id: tipFragment
    implicitHeight: app.isMobile ? s(712) : s(664)
    implicitWidth: app.isMobile ? parent.width : s(376)

    property string headerText: Strings.primerPassphrases

    // Point that triangle should point to in global coordinate system
    property point pointToGlobal

    property int pointing

    signal handleHideHint()

    property ListModel itemsModel: tipsModel

    function show() {
        visible = true
        // Transpose the global position to local position and bind the y value to the triangle
        var p = tipFragment.mapFromGlobal(pointToGlobal.x, pointToGlobal.y)
        triangle.y = Qt.binding(function(){return p.y - triangle.height * .5})
    }

    UIKit.CanvasTriangle {
        id: triangle
        width: s(12)
        height: s(15)
        x: pointing === UIKit.CanvasTriangle.Pointing.Left ? -width + s(2) : tipFragment.width - s(2)
        pointing: tipFragment.pointing
    }
    DropShadow {
        id: dropShadow
        anchors.fill: triangle
        transparentBorder: true
        radius: s(8)
        spread: 0.1
        color: Qt.rgba(0, 0, 0, 0.2)
        source: triangle
        visible: triangle.visible
    }

    Rectangle {
        anchors.fill: parent
        radius: 2
        ColumnLayout {
            id: layout
            anchors.fill: parent
            anchors.leftMargin: app.isMobile ? 0 : ThemeController.style.homePageVerticalMargin
            anchors.rightMargin: app.isMobile ? 0 : ThemeController.style.homePageVerticalMargin
            anchors.topMargin: ThemeController.style.homePageVerticalMargin
            anchors.bottomMargin: s(40)
            spacing: ThemeController.style.margin.m16
            signal hideHint()

            onHideHint: {
                handleHideHint()
            }

            // Header
            Item {
                id: header
                Layout.fillWidth: true
                Layout.minimumHeight: s(24)
                Layout.maximumHeight: s(24)

                UIKit.BaseText {
                    text: tipFragment.headerText
                    font.weight: Font.DemiBold
                    anchors.centerIn: parent
                }

                UIKit.LargeIconButton {
                    id: closeButton
                    anchors.right: parent.right
                    icon.source: "qrc:/close-gray.svg"
                    onClicked: {
                        layout.hideHint()
                    }
                }
            }
            // Line
            Rectangle {
                Layout.topMargin: -ThemeController.style.margin.m4
                Layout.bottomMargin: -ThemeController.style.margin.m4
                Layout.fillWidth: true
                Layout.preferredHeight: s(1)
                color: ThemeController.style.whisperColor
            }
            // Swipe
            SwipeView {
                id: view
                Layout.fillWidth: true
                Layout.fillHeight: true
                clip: true

                Repeater {
                    model: itemsModel
                    Loader {
                        active: SwipeView.isCurrentItem
                        sourceComponent: UIKit.TipBox {
                            width: parent.width
                            height: parent.height
                            title: titleTxt
                            content: contentTxt
                            //info: infoTxt
                            imageSource: imageSrc
                        }
                    }
                }
            }

            UIKit.SwipeIndicator{
                swipeView: view
                modelSize: tipsModel.count
                Layout.fillWidth: true
                Layout.topMargin: ThemeController.style.margin.m8
            }
        }

        Component.onCompleted: {
            tipsModel.append({ titleTxt: Strings.whatIsPassphrase,
                                 contentTxt: Strings.passphraseDefinition,
                                 imageSrc: "qrc:/illustrations/r32.png"});
            tipsModel.append({ titleTxt: Strings.whyPassphrase,
                                 contentTxt: Strings.whyPassphraseAnswer,
                                 imageSrc: "qrc:/illustrations/r33.png"});
            tipsModel.append({ titleTxt: Strings.passphraseBetter,
                                 contentTxt: Strings.passphraseBetterAnswer,
                                 imageSrc: "qrc:/illustrations/r34.png"});
        }

        ListModel{
            id: tipsModel
        }
    }

}
