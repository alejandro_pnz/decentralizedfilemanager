import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

ColumnLayout {
  spacing: ThemeController.style.margin.m12
  height: parent.height
  width: parent.width
  signal closeClicked()

  Item {
    Layout.fillWidth: true
    Layout.preferredHeight: ThemeController.style.margin.m32

    UIKit.BaseText {
      text: Strings.deleteAccount
      font.weight: Font.Bold
      size: UIKit.BaseText.TextSize.H3
      anchors.centerIn: parent
    }

    UIKit.LargeIconButton {
      icon.source: "qrc:/close.svg"
      height: ThemeController.style.margin.m32
      width: ThemeController.style.margin.m32
      anchors.right: parent.right
      visible: !app.isPhone
      onClicked: closeClicked()
    }
  }

  UIKit.BaseText {
    text: Strings.wantToDeleteAccount
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.StyledButton {
    id: button
    text: Strings.yesDelete
    Layout.topMargin: ThemeController.style.margin.m20
    Layout.fillWidth: true
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Destructive
  }

  UIKit.StyledButton {
    id: cancelButton
    Layout.topMargin: ThemeController.style.margin.m4
    text: Strings.cancel
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Secondary
    Layout.fillWidth: true
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
