import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  spacing: ThemeController.style.margin.m16

  Rectangle {
    Layout.preferredHeight: s(app.isMobile ? 188 : 230)
    Layout.preferredWidth: parent.width
    radius: s(10)
    border.width: s(3)
    border.color: ThemeController.style.slatePurpleColor
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.BaseText {
    Layout.topMargin: ThemeController.style.margin.m24
    Layout.fillWidth: true
    text: Strings.areYouOwner
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.StyledButton {
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    text: Strings.yes
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: RegistrationManager.addNewDeviceMultipleAccountOwner()
  }

  UIKit.StyledButton {
    Layout.fillWidth: true
    text: Strings.no
    type: UIKit.StyledButton.ButtonStyle.Secondary
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: pageManager.enterPage(Pages.AccountOwnerEmailPage)
  }
}
