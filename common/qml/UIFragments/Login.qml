import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: columnLayout

  spacing: ThemeController.style.margin.m16
  readonly property bool error: emailInput.state === 'error' || passphraseInput.state === 'error'

  property bool busy: false

  enabled: !columnLayout.busy

  Connections {
    target: pageManager
    function onSetupLoginPage(email) {
      emailInput.textField.text = email
    }
  }

  Connections {
    target: RegistrationManager
    function onWrongCredentials(config) {
      if (config.emailError !== undefined) {
        emailInput.errorMessage = [config.emailError]
        emailInput.state = 'error'
      }
      if (config.passphraseError !== undefined) {
        passphraseInput.errorMessage = [config.passphraseError]
        passphraseInput.state = 'error'
      }
    }
  }

  Connections {
    target: columnLayout
    function onBusyChanged() {
      if (!columnLayout.busy) {
        if (columnLayout.error) {
          passphraseInput.textField.text = ""
        }
        else {
          emailInput.textField.text = ""
          passphraseInput.textField.text = ""
        }
      }
    }
  }

  UIKit.SvgImage {
    source: "qrc:/login-logo.svg"
    width: app.isMobile ? s(56) : s(64)
    height: app.isMobile ? s(64) : s(75)
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.BaseText {
    Layout.topMargin: ThemeController.style.margin.m8
    text: Strings.logIn
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.InputField {
    id: emailInput
    Layout.topMargin: ThemeController.style.margin.m8
    labelText: Strings.email
    placeholderTxt: Strings.typeEmail
    inputRequired: true
    fieldType: UIKit.InputField.FieldType.Email
    onTabPressed: passphraseInput.textField.forceActiveFocus()
    onEnterPressed: passphraseInput.textField.forceActiveFocus()
    onBacktabPressed: passphraseInput.textField.forceActiveFocus();
  }

  UIKit.InputField {
    id: passphraseInput
    labelText: Strings.passphrase
    placeholderTxt: Strings.typePassphrase
    inputRequired: true
    isPassPhrase: true
    eyeIconVisible: true
    onEnterPressed: loginButton.clicked()
    onTabPressed: emailInput.textField.forceActiveFocus();
    onBacktabPressed: emailInput.textField.forceActiveFocus();
  }

  UIKit.StyledButton {
    id: loginButton
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    text: Strings.logIn
    textItem.visible: !pulseLoader.visible
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      var emailOK = validateEmailInput(emailInput)
      var passOK = validateMandatoryField(passphraseInput)
      if (emailOK && passOK) RegistrationManager.login(emailInput.textField.text, passphraseInput.textField.text)
    }

    UIKit.PulseLoader {
      id: pulseLoader
      anchors.centerIn: parent
      visible: columnLayout.busy
    }
  }

  Rectangle {
    Layout.fillWidth: true
    Layout.preferredHeight: s(1)
    color: ThemeController.style.seashellColor
  }

  UIKit.BaseText {
    Layout.alignment: Qt.AlignHCenter
    text: RegistrationManager.registered ? Strings.readyForMore : Strings.newToQA
    color: ThemeController.style.charadeColor
  }

  UIKit.StyledButton {
    id: createAccount
    Layout.fillWidth: true
    text: RegistrationManager.registered ? Strings.addNewSubscription : Strings.signUp
    type: UIKit.StyledButton.ButtonStyle.Secondary
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      if (!RegistrationManager.registered)
        RegistrationManager.createAccount()
      else
        RegistrationManager.openLink("https://website-staging.test.com/sms/login-magic-link.html?locale=en")
    }
  }
}
