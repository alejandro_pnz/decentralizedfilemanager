import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0

ColumnLayout {
  id: content
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m16
  property bool showFooter: true

  property string email: AuthenticationManager.currentAccount.email

  QuodArcaSettings {
    id: settings
    preferencesType: SettingsManager.User

    property string language
  }

  UIKit.BaseText {
    text: Strings.user
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    visible: !app.isPhone
  }

  UIKit.InputField {
    id: nameInput
    Layout.topMargin: app.isPhone ? 0 : ThemeController.style.margin.m8
    labelText: Strings.name
    placeholderTxt: Strings.typeYourName
    inputRequired: false
    clearIconVisible: true
    onBacktabPressed: emailInput.textField.forceActiveFocus()
    onTabPressed: nicknameInput.textField.forceActiveFocus()
    onEnterPressed: nicknameInput.textField.forceActiveFocus()
  }

  UIKit.InputField {
    id: nicknameInput
    labelText: Strings.nickname
    placeholderTxt: Strings.typeYourNickname
    inputRequired: false
    clearIconVisible: true
    onBacktabPressed: nameInput.textField.forceActiveFocus()
    onTabPressed: emailInput.textField.forceActiveFocus()
    onEnterPressed: emailInput.textField.forceActiveFocus()
  }

  UIKit.InputField {
    id: emailInput
    labelText: Strings.email
    placeholderTxt: email.length > 0 ? email : Strings.typeEmail
    inputRequired: false
    clearIconVisible: true
    fieldType: UIKit.InputField.FieldType.Email
    onBacktabPressed: nameInput.textField.forceActiveFocus()
    onTabPressed: pickerDelegate.openPicker()
    onEnterPressed: pickerDelegate.openPicker()
  }

  UIKit.PickerDelegate {
    id: pickerDelegate
    labelText: Strings.language
    pickerTitle: Strings.language
    inputRequired: false
    leftEyeIcon: false
    objectsModel: [
      { text: Strings.system, value: "system" },
      { text: Strings.english, value: "en" },
      { text: Strings.french, value: "fr" },
      { text: Strings.deutsch, value: "de" },
      { text: Strings.espanol, value: "es" },
      { text: Strings.italian, value: "it" }
    ]
    onTabPressed: nameInput.textField.forceActiveFocus()
    onBacktabPressed: emailInput.textField.forceActiveFocus()
    comboBox.textRole: "text"
    comboBox.valueRole: "value"
    comboBox.currentIndex: comboBox.indexOfValue(settings.language)
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
