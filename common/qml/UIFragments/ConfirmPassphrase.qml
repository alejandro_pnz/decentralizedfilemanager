import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m24

  property bool busy: false

  enabled: !layout.busy

  Rectangle {
    Layout.preferredHeight: s(app.isMobile ? 222 : 230)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: "qrc:/illustrations/r27-confirm-password.png"
      mipmap: true
    }
  }

  UIKit.BaseText {
    text: Strings.confirmPassphrase
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    horizontalAlignment: Text.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.InputField {
    id: passphraseInput
    labelText: Strings.confirmPassphrase
    placeholderTxt: Strings.typePowerPassphrase
    inputRequired: true
    isPassPhrase: true
    clearIconVisible: true
    Layout.fillWidth: true
    onEnterPressed: actionButton.clicked()
  }

  UIKit.StyledCheckBox {
    id: acceptInfo
    Layout.topMargin: s(5)
    Layout.fillWidth: true
    text: Strings.understandInformation
    checkRequired: true
  }

  UIKit.StyledButton {
    id: actionButton
    Layout.fillWidth: true
    text: Strings.next
    textItem.visible: !pulseLoader.visible
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      var error = false
      if (!acceptInfo.checked) {
        acceptInfo.errorState = true
        error = true
      }
      var passOK = validatePassphraseInputText(passphraseInput, RegistrationManager.powerPassphrase)
      if (!error && passOK) {
        RegistrationManager.registration();
        passphraseInput.textField.text = ""
      }
    }

    UIKit.PulseLoader {
      id: pulseLoader
      anchors.centerIn: parent
      visible: layout.busy
    }
  }
}
