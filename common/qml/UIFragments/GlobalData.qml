import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m24

  property bool isMobile: app.isPhone

  UIKit.BaseText {
    text: Strings.dataStr
    visible: !app.isPhone
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
  }

  Flickable {
    id: flick
    Layout.fillHeight: true
    Layout.fillWidth: true
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: content.height
    clip: true

    ColumnLayout {
      id: content
      spacing: ThemeController.style.margin.m16
      width: parent.width

      UIKit.BaseText {
        text: Strings.unlockAuthorizationMethod
        font.weight: Font.Bold
      }

      ButtonGroup {
        id: authorizationGroup
        buttons: authoriztionMethod.children
        exclusive: true
      }

      Column {
        id: authoriztionMethod
        spacing: ThemeController.style.margin.m24
        Layout.topMargin: ThemeController.style.margin.m8

        UIKit.StyledRadioButton {
          checked: true
          text: Strings.email
        }

        UIKit.StyledRadioButton {
          text: Strings.sms
        }

        UIKit.StyledRadioButton {
          text: Strings.authenticator
        }

        UIKit.StyledRadioButton {
          text: Strings.userChoice
        }
      }

      Rectangle {
        Layout.topMargin: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m20
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: 1
      }

      UIKit.SwitchItem {
        Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m12
        text: Strings.allowPrivateData
        contentText: Strings.allPublicSync
        showSeparator: true
        separatorMargin: isMobile ? 0 : ThemeController.style.margin.m16
      }

      UIKit.SwitchItem {
        Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m12
        text: Strings.auditLogs
        contentText: Strings.logsOfActions
        showSeparator: true
        separatorMargin: isMobile ? 0 : ThemeController.style.margin.m16
      }

      UIKit.SwitchItem {
        Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m12
        text: Strings.exportInClear
        contentText: Strings.exportOnlyEncrypted
        showSeparator: true
        separatorMargin: isMobile ? 0 : ThemeController.style.margin.m16
      }

      UIKit.PickerDelegate {
        Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m12
        labelText: Strings.deleteItemsAfter
        pickerTitle: Strings.deleteItemsAfter
        inputRequired: false
        leftEyeIcon: false
        objectsModel: [Strings.day.arg("1"), Strings.days.arg("14"), Strings.days.arg("7"), Strings.days.arg("30"), Strings.never]
      }

      UIKit.PickerDelegate {
        labelText: Strings.maximumSyncable
        pickerTitle: Strings.maximumSyncable
        inputRequired: false
        leftEyeIcon: false
        objectsModel: ["1 GB", "2 GB", "5 GB", "10 GB", "20 GB", Strings.noLimit]
      }

      Rectangle {
        Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m8
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
      }

      UIKit.SwitchItem {
        Layout.topMargin: ThemeController.style.margin.m4
        text: Strings.alertsOnSend
        contentText: Strings.receiveAlerts
      }

      Item {
        Layout.fillWidth: true
        Layout.preferredHeight: s(4)
      }
    }
  }
}
