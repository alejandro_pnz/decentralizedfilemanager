import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m12
  width: parent.width

  property bool isMobile: app.isPhone && !app.isTablet

  Rectangle {
    Layout.preferredHeight: isMobile ? s(222) : s(155)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: "qrc:/illustrations/u4.png"
      mipmap: true
    }
  }

  UIKit.BaseText {
    Layout.topMargin: isMobile ? ThemeController.style.margin.m28 : ThemeController.style.margin.m12
    text: Strings.verifying
    alternativeFont: true
    font.weight: Font.Bold
    size: isMobile ? UIKit.BaseText.TextSize.H2 : UIKit.BaseText.TextSize.H3
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.BaseText {
    Layout.topMargin: isMobile ? ThemeController.style.margin.m4 : 0
    Layout.fillWidth: true
    text: Strings.pleaseWait
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.alignment: Qt.AlignHCenter
  }
}
