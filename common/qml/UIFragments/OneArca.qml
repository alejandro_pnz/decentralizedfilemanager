import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: root
  width: parent.width
  readonly property bool isMobile: app.isPhone
  property alias stackLayout: stackLayout
  property bool showFooter: stackLayout.currentIndex === 0 ? false :
                            !propertiesTab.generalDetailsEditMode
  spacing: ThemeController.style.margin.m16

  ObjectDelegate {
    Layout.topMargin: ThemeController.style.homePageVerticalMargin
    Layout.fillWidth: true
    Layout.preferredHeight: s(103)
    Layout.alignment: Qt.AlignHCenter

    background: Rectangle{
      anchors.fill: parent
    }
    topPadding: 0
    leftPadding: 0
    rightPadding: 0
    bottomPadding: 0
    avatar.size: ObjectAvatar.Size.Big
    avatar.shareMode: arcaEditor.object.syncProperty
    avatar.iconSrc: "image://item/type=" + SyncableObject.Arca + "&id=" + arcaEditor.object.id

    dropShadow.visible: false
    checkable: false
    title: arcaEditor.object.name
    titleComponent.color: ThemeController.style.font.defaultColor
    titleComponent.size: BaseText.TextSize.H3
    text: Utility.formatArcaContent(arcaEditor.object.quodCount, arcaEditor.object.filesCount)
  }

  RowLayout {
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true

    Item {
      Layout.fillWidth: isMobile
    }

    TabBar {
      id: bar
      spacing: ThemeController.style.margin.m12
      StyledTabButton {
        icon.source: "qrc:/linked-data.svg"
        text: Strings.linkedData
      }
      StyledTabButton {
        icon.source: "qrc:/properties.svg"
        text: Strings.properties
      }
    }

    Item {
      Layout.fillWidth: true
    }

    StyledButton {
      text: Strings.edit
      type: StyledButton.ButtonStyle.Text
      displayMode: StyledButton.DisplayMode.TextOnly
      onClicked: homeManager.enterPage(Pages.LinkObjectsPage)
      Layout.alignment: Qt.AlignVCenter
      Layout.preferredHeight: s(24)
      visible: !isMobile && stackLayout.currentIndex === 0
    }
  }

  Rectangle {
    id: spacer
    Layout.topMargin: isMobile ? ThemeController.style.margin.m8 : 0
    Layout.fillWidth: true
    Layout.leftMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    Layout.rightMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    color: ThemeController.style.mercuryColor
    height: s(1)
  }

  StackLayout {
    id: stackLayout
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    Layout.fillHeight: true
    currentIndex: bar.currentIndex

    LinkedDataTab {
      id: linkedDataTab
    }

    PropertiesTab {
      id: propertiesTab
    }
  }
}
