import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2
import QtQml.Models 2.12


import com.testintl 1.0

ColumnLayout {
  id: column
  spacing: 0
  anchors.fill: parent
  readonly property bool isMobile: app.isPhone
  property bool itemsPinned: filterModel.count > 0
  property alias ism: ism

  Connections {
    target: homeManager
    function onClearLibrarySelection() {
      column.ism.clearSelection()
    }
  }

  SearchModel {
    id: searchModel
  }

  AppliedSearchFilters {
    id: appliedFiltersPanel
    Layout.fillWidth: true
    Layout.leftMargin: -ThemeController.style.margin.m16
    Layout.rightMargin: -ThemeController.style.margin.m16
    Layout.bottomMargin: ThemeController.style.margin.m16
    visible: isMobile
  }

  UIKit.SearchInput {
    id: search
    implicitWidth: 0
    Layout.topMargin: ThemeController.style.homePageVerticalMargin
    Layout.fillWidth: true
    placeholderText: Strings.searchQuodArca
    Layout.preferredHeight: ThemeController.style.defaultInputHeight
    onSearchTextChanged: searchModel.search(text)
  }

  DesktopFiltersPanel {
    id: filtersPanel
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    visible: !isMobile
    sModel: searchModel

    onShowMore: {
      homeManager.enterMoreFiltersPage({"searchModel":searchModel})
    }
  }

  NoItemsComponent {
    id: contentItem
    Layout.fillHeight: true
    Layout.fillWidth: true
  }

  ColumnLayout {
    id: pinnedItemsColumn
    visible: itemsPinned
    width: parent.width
    Layout.fillHeight: true
    spacing: ThemeController.style.margin.m12
    Layout.topMargin: ThemeController.style.margin.m24

    Item {
      id: listViewHeader
      Layout.preferredHeight: s(24)

      UIKit.SvgImage {
        id: pinIcon
        height: s(16)
        width: s(16)
        source: "qrc:/pin-filled.svg"
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
      }

      UIKit.BaseText {
        anchors.left: pinIcon.right
        anchors.leftMargin: s(6)
        text: Strings.pinnedObjects
        font.weight: Font.DemiBold
        color: ThemeController.style.manateeColor
      }
    }

    UIKit.GenericListView {
      id: listView
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.topMargin: ThemeController.style.margin.m4
      clip: true
      spacing: ThemeController.style.margin.m12
      selectionMode: ism.hasSelection

      model: searchModel // filterModel
      delegate: UIKit.ObjectDelegate {
        property var locked: model.locked
        // Model data
        title: model.name
        text: model.content
        avatar.shareMode: model.syncProperty
        titleIcon: locked === undefined ? false : locked
        avatar.iconSrc: "image://item/type=" + model.objectType + "&id=" + model.id

        // Other properties
        width: listView.width
        actionButton: true
        checkboxOnTheLeft: !app.isMobile
        checkable: !app.isMobile
        delegateModel: listView.model
        selectionModel: ism
        selectionMode: listView.selectionMode

        onDelegateClicked: {
          // Open object
          switch(model.objectType) {
          case SyncableObject.Arca:
            homeManager.openOneArcaPage(model.id)
            break;
          case SyncableObject.Quod:
            homeManager.openOneQuodPage(model.id)
            break;
          case SyncableObject.File:
            homeManager.openOneFilePage(model.id)
            break;
          default:
            console.warn("Unsupported object type", model.objectType)
            break;
          }
        }

        onActionButtonClicked: {
          listView.currentIndex = index
          actionMenu.model.currentObject = object
          actionMenu.handleActionButton(listView)
        }

        Binding on actionButton {
          when: !app.isPhone
          value: !ism.hasSelection
        }

        onTextLinkActivated: {
          homeManager.openOneArcaPage(link)
        }

      }
    }

    SortFilterProxyModel {
      id: filterModel
      sourceModel: SyncableObjectListModel
      filters: ValueFilter {
        roleName: "pinned"
        value: true
      }
    }

    ItemSelectionModel {
      id: ism
      model: SyncableObjectListModel
    }

    OneObjectActionMenu {
      id: actionMenu
    }
  }

  states: [
    State {
      name: "home"
      when: app.isMobile ? search.preeditText.length === 0 && search.text.length === 0 : search.text.length === 0
      PropertyChanges {
        target: contentItem
        visible: !itemsPinned
      }
      PropertyChanges {
        target: pinnedItemsColumn
        visible: itemsPinned
      }
      PropertyChanges {
        target: appliedFiltersPanel
        visible: false
      }
      PropertyChanges {
        target: filtersPanel
        visible: false
      }
      PropertyChanges {
        target: listViewHeader
        visible: true
      }
      PropertyChanges {
        target: listView
        model: filterModel
      }
    },
    State {
      name: "search"
      when: search.preeditText.length > 0 || search.text.length > 0
      PropertyChanges {
        target: contentItem
        visible: false
      }
      PropertyChanges {
        target: pinnedItemsColumn
        visible: true
      }
      PropertyChanges {
        target: appliedFiltersPanel
        visible: isMobile && appliedFiltersPanel.filtersApplied > 0
      }
      PropertyChanges {
        target: filtersPanel
        visible: !isMobile
      }
      PropertyChanges {
        target: listViewHeader
        visible: false
      }
      PropertyChanges {
        target: listView
        model: searchModel
      }
    }
  ]
}

