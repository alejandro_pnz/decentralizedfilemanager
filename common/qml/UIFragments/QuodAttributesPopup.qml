import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import com.test 1.0
import UIFragments 1.0

UIKit.GenericPopup {
  id: popup
  implicitHeight: s(554)
  implicitWidth: s(394)

  modal: false
  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
  topPadding: 0
  bottomPadding: 0

  QuodAttributes {
    anchors.fill: parent
    buttonGroup.onClicked: {
      popup.close()
    }
  }
}
