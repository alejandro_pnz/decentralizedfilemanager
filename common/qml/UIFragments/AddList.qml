import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

Flickable {
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: addList.height
  clip: true
  property alias columnContent: addList

  ColumnLayout {
    id: addList
    width: parent.width
    spacing: ThemeController.style.margin.m12
    
    UIKit.CardDelegate {
      title: Strings.newArca
      text: Strings.linkData
      icon.source: "qrc:/arca-create.svg"
      Layout.preferredHeight: s(80)
      Layout.fillWidth: true
      pointingHandCursor: true
      onClicked: homeManager.enterCreateArca()
    }
    
    UIKit.CardDelegate {
      title: Strings.newQuod
      text: Strings.enterData
      icon.source: "qrc:/quod.svg"
      Layout.fillWidth: true
      Layout.preferredHeight: s(80)
      pointingHandCursor: true
      onClicked: homeManager.enterCreateQuod()
    }
    
    UIKit.CardDelegate {
      title: Strings.importArca
      text: Strings.addObjects
      icon.source: "qrc:/import-default.svg"
      Layout.fillWidth: true
      Layout.preferredHeight: s(80)
      pointingHandCursor: true
      onClicked: homeManager.enterFileDialog()
    }
    
    UIKit.CardDelegate {
      title: Strings.newTextFile
      text: Strings.createTextFile
      icon.source: "qrc:/new-text.svg"
      Layout.fillWidth: true
      Layout.preferredHeight: s(80)
      pointingHandCursor: true
      onClicked: homeManager.enterCreateFile()
    }
    
    UIKit.CardDelegate {
      title: Strings.inviteUsers
      text: Strings.collaborateOnData
      icon.source: "qrc:/invite-user.svg"
      Layout.fillWidth: true
      Layout.preferredHeight: s(80)
      pointingHandCursor: true
    }
  }
}
