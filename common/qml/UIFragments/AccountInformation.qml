import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0

import "../UIKit/Charts/PieChart"

ColumnLayout {
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m24
  property bool showFooter: false

  property string name: "Lauren Paulson"
  property string owner: AuthenticationManager.currentAccount.accountEmail
  property int usersCount: UserListModel.count
  property int activeSessionsCount: 3
  property int loggedInCount: 3
  property int loggedOutCount: 3
  property int quodNumber: 3
  property int arcaNumber: 3
  property int filesNumber: 3
  property string freeSpace: Utility.bytesToSize(Utility.getFreeSpace())
  property string usedSpace: Utility.bytesToSize(Utility.getUsedSpace())
  property string totalPublicSpace: "1.2 GB"
  property string ipLocation: "192.168.23.3 \u2022 London, UK"
  property string secondIpLocation: "192.168.23.3 \u2022 London, UK"
  property string topCountries: "Top 10 countries:\n \u2022 USA"

  UIKit.BaseText {
    text: Strings.accountInformation
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    visible: !app.isPhone
  }

  Flickable {
    Layout.fillHeight: true
    Layout.fillWidth: true
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : 0
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: content.height
    clip: true

    Flow {
      id: content
      width: parent.width
      height: app.isPhone ? undefined : s(972)
      flow: Flow.TopToBottom
      spacing: ThemeController.style.margin.m24

      Column {
        width: s(256)
        spacing: ThemeController.style.margin.m12
        UIKit.BaseText {
          text: Strings.icon
          color: ThemeController.style.shuttleColor
        }

        UIKit.Avatar {
          id: objectAvatar
          size: UIKit.Avatar.Size.Big
          text: name
        }
      }

      UIKit.InfoItem {
        width: s(256)
        labelText: Strings.accountOwnerName
        content: name
      }

      UIKit.InfoItem {
        width: s(256)
        labelText: Strings.accountOwnerEmail
        content: owner
      }

      Column {
        spacing: ThemeController.style.margin.m12
        width: s(256)
        UIKit.BaseText {
          text: Strings.accountUsers
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          text: Strings.totalUsers.arg(usersCount)
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: topCountries
          font.weight: Font.DemiBold
        }

      }

      Column {
        spacing: ThemeController.style.margin.m12
        width: s(256)
        UIKit.BaseText {
          text: Strings.sessions
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          text: Strings.activeSessions.arg(activeSessionsCount)
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: Strings.loggedIn.arg(loggedInCount)
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: Strings.loggedOut.arg(loggedOutCount)
          font.weight: Font.DemiBold
        }
      }

      Column {
        spacing: ThemeController.style.margin.m12
        width: s(256)
        UIKit.BaseText {
          text: Strings.mainIPLocations
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          text: ipLocation
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: secondIpLocation
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: Strings.otherLocations.arg(8)
          font.weight: Font.DemiBold
        }
      }

      Column {
        spacing: ThemeController.style.margin.m12
        width: s(256)
        UIKit.BaseText {
          text: Strings.accountDevices
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          text: Strings.accountOwnerDevices.arg(loggedOutCount)
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: Strings.userDevices.arg(loggedOutCount)
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: Strings.powerDevices.arg(loggedOutCount)
          font.weight: Font.DemiBold
        }
      }

      AccountInformationChart {
        dataModel: modelData
      }

      Column {
        spacing: ThemeController.style.margin.m12
        width: s(256)
        UIKit.BaseText {
          text: Strings.accountPublicObjects
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          text: quodNumber + " " + Strings.quod
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: filesNumber + " " + Strings.files
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: arcaNumber + " " + Strings.arca
          font.weight: Font.DemiBold
        }
      }

      UIKit.InfoItem {
        width: s(256)
        labelText: Strings.totalPublicData
        content: totalPublicSpace
      }

      Column {
        spacing: ThemeController.style.margin.m12
        width: s(256)
        bottomPadding: app.isPhone ? ThemeController.style.margin.m16 : 0
        UIKit.BaseText {
          text: Strings.localStorage
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          text: Strings.usedSpace.arg(usedSpace)
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: Strings.freeSpace.arg(freeSpace)
          font.weight: Font.DemiBold
        }
      }
    }
  }

  ListModel {
    id: modelData
  }

  Component.onCompleted: {
    modelData.append({label: "Windows", colorValue: "#BD0ADB", value: 11})
    modelData.append({label: "macOS", colorValue: "#22D396", value: 37})
    modelData.append({label: "iOS", colorValue: "#5C5AE8", value: 16})
    modelData.append({label: "Android", colorValue: "#5A5F6D", value: 10})
    modelData.append({label: "iPadOS", colorValue: "#2F3441", value: 11})
  }
}
