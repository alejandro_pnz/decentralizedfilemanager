import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2
import UIFragments 1.0 as UIFragments
import UIModel 1.0

LibraryTab {
  id: filesTab
  noItemTitle: Strings.noFilesInLibrary
  noItemDescription: Strings.noFilesInLibraryDescription
  noItemIcon: "qrc:/library/file.svg"
  noItem: FileListModel.count < 1
  objectTypeName: Strings.files
  ism.model: FileListModel
  objectType: SyncableObject.File

  SortModel {
    id: sortedProxyModel
    pinnedFilterEnabled: false
    sourceModel: FileListModel
    sorter: listView.headerItem.sorter
    searchPattern: search.text
  }

  ColumnLayout {
    spacing: ThemeController.style.homePageVerticalMargin
    width: parent.width
    height: parent.height

    UIKit.SearchInput {
      id: search
      Layout.fillWidth: true
      placeholderText: Strings.searchFiles
      micIconVisible: false
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
    }

    UIKit.GenericListView {
      id: listView
      Layout.fillWidth: true
      Layout.fillHeight: true
      clip: true
      spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16

      reuseItems: true
      ListView.onPooled: console.log("onPooled")
      ListView.onReused: console.log("onReused")

      header: Rectangle {
        property alias sorter: sortBtn.sorter
        color: ThemeController.style.whiteColor
        width: listView.width
        height: sortBtn.height
        z: 2

        UIFragments.ObjectListSortButton {
          id: sortBtn
          anchors.top: parent.top
          anchors.left: parent.left
          headerBottomMargin: 0

          Connections {
            target: filesTab
            function onChangeSorter(sorter) {
              sortBtn.init(sorter)
            }
          }
          objectType: Enums.File
        }
      }

      headerPositioning: ListView.OverlayHeader

      selectionMode: filesTab.ism.hasSelection

      model: sortedProxyModel.proxy
      delegate: UIKit.ObjectDelegate {
        // Model data

        title: model.name
        text: model.content
        titleIcon: !model.available //model.locked
        titleIconSource: "qrc:/out-of-sync.svg" // TODO: locked

        avatar.iconSrc: "image://item/type=" + SyncableObject.File + "&id=" + model.id
        avatar.shareMode: model.syncProperty
        //avatar.state: model.available ? "normal" : "out-of-sync"

        progress: model.progress

        // Other properties
        width: listView.width
        checkboxOnTheLeft: !app.isMobile
        checkable: !app.isMobile || selectionMode
        delegateModel: listView.model
        selectionModel: filesTab.ism
        selectionMode: listView.selectionMode

        onDelegateClicked: {
          // Open object
          console.log("Delegate clicked")
          if (model.available) {
            homeManager.openOneFilePage(model.id);
          }
        }

        onActionButtonClicked: {
          listView.currentIndex = index
          actionMenu.model.currentObject = model.object
          actionMenu.handleActionButton(listView)
        }

        onTextLinkActivated: {
          homeManager.openOneArcaPage(link)
        }

        actionButton: !filesTab.ism.hasSelection
      }
    }

  }

  OneObjectActionMenu {
    id: actionMenu
  }
}
