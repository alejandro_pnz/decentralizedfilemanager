import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: root
  width: parent.width
  readonly property bool isMobile: app.isPhone
  property alias stackLayout: stackLayout
  spacing: ThemeController.style.margin.m16
  property var device: deviceEditor.object

  DeviceDelegate {
    Layout.topMargin: ThemeController.style.margin.m24
    Layout.fillWidth: true
    Layout.preferredHeight: s(103)
    Layout.alignment: Qt.AlignHCenter
    visible: !isMobile

    background: Rectangle{
      anchors.fill: parent
    }
    topPadding: 0
    leftPadding: 0
    rightPadding: 0
    bottomPadding: 0
    avatar.deviceType: device.deviceType
    avatar.size: ObjectAvatar.Size.Big
    dropShadow.visible: false
    titleIcon: device.powerDevice
    checkable: false
    title: device.deviceModel
    titleComponent.color: ThemeController.style.font.defaultColor
    titleComponent.size: BaseText.TextSize.H3
    contentText: device.deviceOS
    syncMode: DeviceDelegate.SyncMode.Synced
    online: device.online
  }

  DeviceMobileDelegate {
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    Layout.preferredHeight: s(167)
    Layout.alignment: Qt.AlignVCenter
    visible: isMobile
    background: Rectangle{
      anchors.fill: parent
    }
    topPadding: 0
    leftPadding: 0
    rightPadding: 0
    bottomPadding: 0

    avatarObject.deviceType: device.deviceType
    avatarObject.size: ObjectAvatar.Size.Big
    dropShadow.visible: false
    titleIcon: device.powerDevice
    checkable: false
    title: device.deviceModel
    contentText: device.deviceOS
    syncMode: DeviceDelegate.SyncMode.Synced
    textObject.color: ThemeController.style.font.defaultColor
    textObject.size: BaseText.TextSize.H3
    states: []
    actionButton.visible: false
    checkboxObject.visible: false
  }

  TabBar {
    id: bar
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.alignment: isMobile ? Qt.AlignHCenter : Qt.AlignLeft
    spacing: ThemeController.style.margin.m12
    StyledTabButton {
      icon.source: "qrc:/properties.svg"
      text: Strings.properties
    }
  }

  Rectangle {
    id: spacer
    Layout.topMargin: isMobile ? ThemeController.style.margin.m8 : 0
    Layout.fillWidth: true
    Layout.leftMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    Layout.rightMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    color: ThemeController.style.mercuryColor
    height: s(1)
  }

  StackLayout {
    id: stackLayout
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    Layout.fillHeight: true
    currentIndex: bar.currentIndex

    DevicePropertiesTab {
      id: devicePropertiesTab
    }
  }
}
