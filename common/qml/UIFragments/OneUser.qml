import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: root
  width: parent.width
  readonly property bool isMobile: app.isPhone
  property alias stackLayout: stackLayout
  property var user: userEditor.object

  signal openGivePowerPopup()

  spacing: isMobile ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24

  OneUserDelegate {
    Layout.topMargin: ThemeController.style.margin.m24
    Layout.fillWidth: true
    Layout.alignment: Qt.AlignHCenter
    title: user.email
    online: user.online
    lastSeenDate: Utility.formatLastSeen(user.lastSeen)
    role: user.guest ? Strings.guest : ""
  }

  BlockedUserPanel {
    id: blockedUserPanel
    visible: user.blocked
  }

  TabBar {
    id: bar
    Layout.alignment: isMobile ? Qt.AlignHCenter : Qt.AlignLeft
    spacing: ThemeController.style.margin.m12
    StyledTabButton {
      icon.source: "qrc:/properties.svg"
      text: Strings.properties
    }
    StyledTabButton {
      text: Strings.devices
    }
  }

  Rectangle {
    id: spacer
    Layout.topMargin: isMobile ? ThemeController.style.margin.m8 : 0
    Layout.fillWidth: true
    Layout.leftMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    Layout.rightMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    color: ThemeController.style.mercuryColor
    height: s(1)
  }

  StackLayout {
    id: stackLayout
    Layout.topMargin: 8
    Layout.fillWidth: true
    Layout.fillHeight: true
    currentIndex: bar.currentIndex

    UserPropertiesTab {
      id: userPropertiesTab
    }

    UserDevicesTab {
      id: userDevicesTab
    }
  }
}
