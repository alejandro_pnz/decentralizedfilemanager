import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: content
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m16

  UIKit.InputField {
    id: currentPassphrase
    Layout.topMargin: app.isMobile ? ThemeController.style.margin.m16 :
                                     ThemeController.style.margin.m24
    labelText: Strings.archivePassphrase
    placeholderTxt: Strings.typeNewPassphrase
    inputRequired: false
    eyeIconVisible: true
    clearIconVisible: false
    isPassPhrase: true
    onBacktabPressed: repeatPassphrase.textField.forceActiveFocus()
    onTabPressed: repeatPassphrase.textField.forceActiveFocus()
    onEnterPressed: repeatPassphrase.textField.forceActiveFocus()
  }

  UIKit.InputField {
    id: repeatPassphrase
    labelText: Strings.repeatArchivePassphrase
    placeholderTxt: Strings.typeNewPassphrase
    inputRequired: false
    eyeIconVisible: true
    clearIconVisible: false
    isPassPhrase: true
    onBacktabPressed: currentPassphrase.textField.forceActiveFocus()
    onTabPressed: currentPassphrase.textField.forceActiveFocus()
    onEnterPressed: currentPassphrase.textField.forceActiveFocus()
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
