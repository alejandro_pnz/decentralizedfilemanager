import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import QtQml.Models 2.12

import com.testintl 1.0

ColumnLayout {  
  id: root
  width: parent.width
  property alias stackLayout: stackLayout
  property int horizontalMargins: 0
  property bool selectionMode: stackLayout.children[stackLayout.currentIndex].selectionMode
  property ItemSelectionModel ism: stackLayout.children[stackLayout.currentIndex].ism
  property string objectTypeName: stackLayout.children[stackLayout.currentIndex].objectTypeName
  property int objectType: stackLayout.children[stackLayout.currentIndex].objectType

  spacing: isMobile ? 0 : ThemeController.style.margin.m16

  TabBar {
    id: bar
//    Layout.top: ThemeController.style.margin.m16
    Layout.alignment: Qt.AlignLeft
    Layout.fillWidth: isMobile
    spacing: isMobile ? ThemeController.style.margin.m36 : ThemeController.style.margin.m12
    contentHeight: s(55)
    height: s(55)
    z: 1

    background: Rectangle {
        color: "transparent"
    }

    StyledTabButton {
      text: Strings.myDevices
      icon.source: "qrc:/devices-other.svg"
    }

    StyledTabButton {
      text: Strings.members
      icon.source: "qrc:/public-filled-gray.svg"
    }

    StyledTabButton {
      text: Strings.guests
      icon.source: "qrc:/guests.svg"
    }
  }

  Rectangle {
    id: spacer
    Layout.fillWidth: true
    Layout.leftMargin: isMobile ? -ThemeController.style.margin.m16 : -horizontalMargins
    Layout.rightMargin: isMobile ? -ThemeController.style.margin.m16 : -horizontalMargins
    Layout.topMargin: -s(2)
    color: ThemeController.style.mercuryColor
    height: s(1)
  }

  StackLayout {
    id: stackLayout
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    Layout.fillHeight: true
    currentIndex: bar.currentIndex

    MyDevicesTab {
      id: myDevicesTab
    }
    MyDevicesTab {
      id: membersTab
    }
    MyDevicesTab {
      id: guestsTab
    }
  }
}
