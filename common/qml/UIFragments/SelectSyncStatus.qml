import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import com.testintl 1.0

ColumnLayout {
  id: contentColumn
  spacing: ThemeController.style.margin.m12
  anchors.fill: parent

  property int mode: Enums.SyncStatus
  property bool publicSync: mode === Enums.PublicSync
  property bool publicSyncAllUsers: publicSync && allUsersSync.checked
  readonly property bool isMobile: app.isPhone

  property int objectSyncStatus: homeManager.currentEditor.syncProperty

  function save() {
    console.log()
    homeManager.currentEditor.syncProperty = syncButtonsGroup.checkedButton.syncProperty
  }

  ColumnLayout {
    id: syncStatusLayout
    spacing: ThemeController.style.margin.m12
    Layout.fillWidth: true
    Layout.topMargin: ThemeController.style.homePageVerticalMargin

    UIKit.CardDelegate {
      id: firstButton
      Layout.fillWidth: true
      title: Strings.privateStr
      text: Strings.syncOnlyYourDevices
      icon.source: "qrc:/private.svg"
      checkable: true
      property int syncProperty: SyncableObject.Private
      checked: objectSyncStatus == syncProperty
    }

    UIKit.CardDelegate {
      id: middleButton
      Layout.fillWidth: true
      title: Strings.publicStr
      text: Strings.syncWithOtherUsers
      icon.source: "qrc:/public-dark.svg"
      checkable: true
      property int syncProperty: SyncableObject.Public
      checked: objectSyncStatus == syncProperty
      onCheckedChanged: {
        if (!checked) mode = Enums.SyncStatus
      }
    }

    UIKit.CardDelegate {
      id: thirdButton
      Layout.fillWidth: true
      title: Strings.noSync
      text: Strings.arcaOnlyOnThisDevice
      property int syncProperty: SyncableObject.NoSync
      checked: objectSyncStatus == syncProperty
      icon.source: "qrc:/no-sync-dark.svg"
      checkable: true
    }
  }

  ButtonGroup {
    id: syncButtonsGroup
    buttons: syncStatusLayout.children
    exclusive: true
    property AbstractButton checkedBtn: checkedButton

    function acceptChangeToPublic() {
      mode = Enums.PublicSync
    }

    function rejectChangeToPublic() {
      syncButtonsGroup.checkedBtn.checked = true
    }

    onCheckedButtonChanged: {
      if (checkedButton == middleButton) {
        console.log("checked changed")
        homeManager.enterChangeSyncStatusPage({"accept": acceptChangeToPublic,
                                                "reject": rejectChangeToPublic})

      } else {
        console.log("Store previous checked button")
        checkedBtn = checkedButton
      }
    }
  }

  ColumnLayout {
    id: configurePublic
    visible: mode === Enums.PublicSync && !isMobile
    spacing: ThemeController.style.margin.m12
    Layout.topMargin: isMobile ? ThemeController.style.margin.m16 : 0

    Rectangle {
      visible: !isMobile
      Layout.topMargin: ThemeController.style.margin.m12
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
    }

    UIKit.BaseText {
      Layout.topMargin: ThemeController.style.margin.m12
      visible: !isMobile
      text: Strings.configurePublicSync
      font.weight: Font.Bold
    }

    UIKit.CardDelegate {
      id: allUsersSync
      Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m4
      Layout.fillWidth: true
      title: Strings.allUsers
      text: Strings.syncWithAllUsers
      icon.source: "qrc:/all-users.svg"
      checkable: true
      checked: true
    }

    UIKit.CardDelegate {
      id: configureWhitelist
      Layout.fillWidth: true
      title: Strings.configureAWhitelist
      text: Strings.syncOnlyList
      icon.source: "qrc:/whitelist.svg"
      checkable: true
    }

    UIKit.CardDelegate {
      id: configureBlacklist
      Layout.fillWidth: true
      title: Strings.configureABlacklist
      text: Strings.excludeFromSync
      icon.source: "qrc:/blacklist.svg"
      checkable: true
    }
  }

  ButtonGroup {
    id: publicButtonsGroup
    buttons: configurePublic.children
    exclusive: true
    onCheckedButtonChanged: {
    }
  }

  Item{
    Layout.fillHeight: true
    Layout.fillWidth: true
  }

  states: [
    State {
      name: "mobileSelectSync"
      when: mode === Enums.SyncStatus && isMobile
      PropertyChanges {
        target: syncStatusLayout
        visible: true
      }
      PropertyChanges {
        target: configurePublic
        visible: false
      }
    },
    State {
      name: "mobileConfigurePublic"
      when: mode === Enums.PublicSync && isMobile
      PropertyChanges {
        target: syncStatusLayout
        visible: false
      }
      PropertyChanges {
        target: configurePublic
        visible: true
      }
    }
  ]
}
