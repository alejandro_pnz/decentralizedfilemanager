import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2
import com.testintl 1.0

Flickable {
  id: flick
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true

  property string noteContent: ""
  signal generatePassword()
  signal enterPasswordHistory()

  signal focusSection(var idx)
  signal focusLastInSection(var idx)

  function ensureVisible(p, h) {
    if (flick.contentY + flick.height < p.y) {
      flick.contentY = p.y - flick.height
    } else if (contentY > p.y - h) {
      flick.contentY = p.y - h
    }
  }

  function focusPreviousSection(idx) {
    // check the section
    if (idx < 0) {
      //if next section does not exists focus quod name
      nameInput.textField.forceActiveFocus()
    } else {
      // Focus next section
      focusLastInSection(idx)
    }
  }

  function focusNextSection(idx) {
    // check the section
    if (idx  === sectionsView.count) {
      if (photosPanel.editMode && photosPanel.hasPhotos) {
        // Focus first photo name
        photosPanel.focusFirstInput()
      } else {
        //if next section does not exists focus quod name
        nameInput.textField.forceActiveFocus()
      }
    } else {
      // Focus next section
      focusSection(idx)
    }
  }

  // Helper variable to trigger binding update
  property bool __update: false
  function updateTooltipsPositions() {
    flick.__update = !flick.__update
  }

  // Open the tooltips recursively
  property var tooltips: []
  property int lastOpenedTooltip: 0
  function nextTooltip() {
    if (tooltips.length > lastOpenedTooltip) {
      tooltips[lastOpenedTooltip].open()
      tooltips[lastOpenedTooltip].aboutToHide.connect(nextTooltip)
      lastOpenedTooltip = lastOpenedTooltip + 1
    }
  }

  Timer {
    interval: 300
    running: true
    onTriggered: {
      if(!settings.tooltipsDisplayed){
        settings.tooltipsDisplayed = true
        nextTooltip()
      }
    }
  }

  Item {
    QuodArcaSettings {
      id: settings
      property bool tooltipsDisplayed: false
    }
  }

  function validate() {
    var ok = validateMandatoryField(nameInput)
    if (!ok) flick.contentY = 0 // Move to top to inform user about error
    console.log("TODO: implement mandatory fields validation", ok)

    for (var i = 0; i < sectionsView.count; ++i) {
      var inputView = sectionsView.itemAtIndex(i).inputView;
      if (inputView) {
        for (var j = 0; j < inputView.count; j++) {
          var input = inputView.itemAtIndex(j).loader.item;
          if (input) {
            if (input.fileToImport !== undefined && input.fileToImport)
            {
              console.log("SPOT FILE TO INPUT");
              var importedId = FileListModel.importFile(input.fileToImportPath)
              if(importedId !== ""){
                input.textChanged(importedId)
              }
            }
          }
        }
      }
    }
    return ok
  }

  function save() {
    // TODO: ?
  }

  function revertChanges() {
    // TODO
//    for (var i = 0; i < sectionsRepeater.count; ++i) {
//      var d = sectionsRepeater.itemAt(i);
//      if (d != undefined) {
//        console.log("Revert changes in attributes for section", d.sectionName)
//        d.attributeModel.revertChanges()
//      } else {
//        console.log("ITEM", d);
//      }
//    }
  }

  Component {
    id: selectWithMapsComponent
    UIKit.StyledButton {
     text: Strings.selectWithMaps
     displayMode: UIKit.StyledButton.DisplayMode.TextOnly
     type: UIKit.StyledButton.ButtonStyle.Text
    }
  }

  ColumnLayout {
    id: content
    width: parent.width
    spacing: 0

    // Quod Name Section
    UIKit.InputField {
      id: nameInput
      labelText: Strings.quodName
      placeholderTxt: Strings.typeQuodName
      textField.text: quodEditor.name
      Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
      Layout.fillWidth: (parent.width - ThemeController.style.margin.m16) * 0.5 < s(268)
      Layout.preferredWidth: Layout.maximumWidth
      Layout.maximumWidth: Layout.fillWidth ? -1 :(parent.width - ThemeController.style.margin.m16) * 0.5
      textField.onTextChanged: {
        quodEditor.name = textField.text
      }
      inputRequired: true
      clearIconVisible: true
      rightIconSource: "qrc:/close-icon.svg"
      rightIconBtn.style: ThemeController.style.fieldButton
      textField.maximumLength: 256
      visible: homeManager.creatingObject || !quodEditor.editOneSection
      onTabPressed: focusNextSection(0) // Focus first section
      onEnterPressed: focusNextSection(0)
      onBacktabPressed: {
        if (photosPanel.editMode && photosPanel.hasPhotos) {
          // Focus last photo name
          photosPanel.focusLastInput()
        } else {
          // Focus last input in last section
          focusLastInSection(sectionsView.count - 1)
        }
      }
      textField.onActiveFocusChanged: {
        if (textField.activeFocus) {
          ensureVisible(nameInput.mapToItem(content, 0, nameInput.height), nameInput.height)
        }
      }
    }

    Rectangle {
      visible: nameInput.visible
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
      Layout.bottomMargin: app.isPhone ? ThemeController.style.margin.m8 : ThemeController.style.margin.m16
      color: ThemeController.style.seashellColor
    }

    QuodSectionsModel {
      id: sectionsModel
      quod: quodEditor.object
    }

    SortFilterProxyModel {
      id: sectionsProxy
      sourceModel: sectionsModel
      filters: [
        ValueFilter { // filter out special sections like photos
          roleName: "specialSection"
          value: false
        },
        ValueFilter {
          enabled: quodEditor.editOneSection
          roleName: "id"
          value: quodEditor.editedSectionId
        }
      ]
    }

    ListView {
      id: sectionsView
      model: sectionsProxy
      Layout.fillWidth: true
      Layout.preferredHeight: contentHeight
      boundsBehavior: ListView.StopAtBounds
      spacing: 0
      property bool editMode: false


      delegate: Item {
        id: d
        width: sectionsView.width
        height: delegate.visible ? delegate.height + ThemeController.style.margin.m16 : 0
        property alias inputView: grid

        Connections {
          target: flick
          function onFocusSection(idx) {
            if (idx === delegate.sectionIndex) {
              // Focus first item
              grid.focusNextItem(0)
            }
          }
          function onFocusLastInSection(idx){
            if (idx === delegate.sectionIndex) {
              // Focus first item
              grid.focusNextItem(grid.count - 1)
            }
          }
        }

        EditPanel {
          id: delegate
          width: sectionsView.width
          editMode: true

          canEdit: false
          hasEdit: false
          hasSave: false

          property var sectionId: model.id
          property var section: model.object
          property int sectionIndex: model.index

          hasBottomSpacer: true
          title: name

          menuChildren: Loader {
            active: model.type === QuodSectionObject.Address
            sourceComponent: selectWithMapsComponent
          }

          Item {
            AttributeSectionModel {
              id: attributesModel
              section: delegate.section
            }
            SortFilterProxyModel {
              id: attributesProxy
              sourceModel: attributesModel
              filters: [
                ValueFilter {
                  roleName: "type"
                  value: AttributeObject.Address
                  inverted: true
                },
                ValueFilter {
                  roleName: "type"
                  enabled: grid.singleColumn
                  value: AttributeObject.Spacer
                  inverted: true
                }
              ]
            }
          }
          Item {
            Layout.fillWidth: true
            Layout.minimumHeight: grid.contentHeight - grid.rowSpacing
            Layout.preferredHeight: grid.contentHeight - grid.rowSpacing
            Layout.maximumHeight: grid.contentHeight - grid.rowSpacing
            GridView {
              boundsBehavior: Flickable.StopAtBounds
              id: grid
              height: grid.contentHeight
              width: delegate.width + columnSpacing
              model: attributesProxy

              property int columnSpacing: ThemeController.style.margin.m16
              property int rowSpacing: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m32
              property int delegateWidth: cellWidth - columnSpacing
              property int delegateHeight: cellHeight - rowSpacing
              property bool singleColumn: cellWidth == grid.width

              signal focusNextItem(var idx)

              cellWidth: {
                if (app.isPhone) return grid.width // one column on phones
                if ((grid.width - columnSpacing) * 0.5 < s(268)) return grid.width
                return grid.width / 2
              }
              cellHeight: s(80) + rowSpacing

              delegate: Item {
                id: gridDelegate
                height: grid.cellHeight
                width: grid.cellWidth
                property int i: index
                property alias loader: loader

                Connections {
                  target: grid
                  function onFocusNextItem(idx) {
                    if (idx === gridDelegate.i) {
                      loader.item.activateInput()
                    }
                  }
                }

                Loader {
                  id: loader
                  width: grid.delegateWidth
                  height: grid.delegateHeight
                  property int attributeIndex: index
                  property var attributeId: model.id
                  onWidthChanged: updateTooltipsPositions()
                  onHeightChanged: updateTooltipsPositions()
                  sourceComponent: {
                    console.log(model.type, model.name, model.placeholder)
                    switch(type) {
                    case AttributeObject.Name: return inputs.nameInput
                    case AttributeObject.Text: return inputs.loginInput
                    case AttributeObject.Password: return inputs.passwordInput
                    case AttributeObject.Authenticator: return inputs.authenticatorInput
                    case AttributeObject.Url: return inputs.websiteInput
                    case AttributeObject.Date: return inputs.dateInput
  //                case AttributeObject.Country: return inputs.countryInput
  //                case AttributeObject.Height: return inputs.heightInput
  //                case AttributeObject.Gender: return inputs.genderInput
                    case AttributeObject.Number: return inputs.numberInput
  //                case AttributeObject.Capitalized: return inputs.capitalizedInput
  //                case AttributeObject.ServerType: return inputs.serverTypeInput
                    case AttributeObject.File: return inputs.selectFileInput
  //                case AttributeObject.CertificateFile: return inputs.selectFileInput
  //                case AttributeObject.NetworkType: return inputs.networkInput
                    case AttributeObject.Values: return inputs.comboboxInput
                    case AttributeObject.Spacer: return inputs.emptyInput
                    }
                    return inputs.loginInput
                  }
                  onLoaded: {
                    if (model.type === AttributeObject.Spacer) return;
                    if (model.type === AttributeObject.Password) {
                      item.input.hasHistory = Qt.binding(function(){return model.history.length > 0})
                    }
                    else if (model.type === AttributeObject.Values) {
                      item.input.objectsModel = Qt.binding(function(){return model.values})
                    }

//                    console.log("ON LOADED", type, name, delegate.sectionIndex, attributeIndex, object.name, loader.implicitHeight, item.implicitHeight)
                    item.sectionId = delegate.sectionId
                    item.attributeId = loader.attributeId
                    item.labelText = model.name
                    item.placeholderText = model.placeholder
                    item.maxLength = model.maximumLength
                    item.sensitive = model.sensitive
                    item.removable = quodEditor.object.custom
                    item.value = Qt.binding(function(){return value})
                    item.readonly = false
                    item.onDeleteButtonClicked.connect(function(){QuodTypeModel.currentType.removeAttribute(object.id)})
                    item.onTextChanged.connect(function(text){
                      quodEditor.setAttributeValue(delegate.sectionId, attributeId, text, delegate.sectionIndex)
                    })
                    item.onGeneratePassword.connect(function(){
                      homeManager.enterPasswordGeneratorPage(delegate.sectionId, attributeId, delegate.sectionIndex);
                    })
                    item.onEnterPasswordHistory.connect(function(){
                      homeManager.enterPasswordHistoryPage(model.history);
                    })
                    item.input.onBacktabPressed.connect(function() {
                      if (index === 0) {
                        flick.focusPreviousSection(delegate.sectionIndex - 1)
                      } else {
                        grid.focusNextItem(index - 1)
                      }
                    })
                    item.input.onEnterPressed.connect(function() {
                      if (index + 1 == grid.count) {
                        flick.focusNextSection(delegate.sectionIndex + 1)
                      } else {
                        grid.focusNextItem(index + 1)
                      }
                    })
                    item.input.onTabPressed.connect(function() {
                      if (index + 1 == grid.count) {
                        flick.focusNextSection(delegate.sectionIndex + 1)
                      } else {
                        grid.focusNextItem(index + 1)
                      }
                    })
                    item.input.textField.activeFocusChanged.connect(function() {
                      if (item.input.textField.activeFocus) {
                        var point = item.input.mapToItem(content,0,item.height)
                        ensureVisible(point, item.height)
                      }
                    })
                    }
                  }
                }
              }
            }
          }
        }
      }

    // Custom fields section

    UIKit.StyledButton {
      id: addButton
      Layout.topMargin: app.isPhone ? ThemeController.style.margin.m8 : ThemeController.style.margin.m8
      Layout.preferredHeight: ThemeController.style.margin.m24
      visible: quodEditor.object.custom && homeManager.creatingObject
      type: UIKit.StyledButton.ButtonStyle.Text
      displayMode: UIKit.StyledButton.DisplayMode.TextRightSideIcon
      text: Strings.addAttribute
      icon.source: "qrc:/add.svg"
      onClicked:  app.isPhone ? homeManager.openPage(Pages.QuodAttributesPage) : attributesPopup.open()
      QuodAttributesPopup {
        id: attributesPopup
        // TODO fix the position
        y: addButton.height + ThemeController.style.margin.m8
        x: (addButton.width - width) * 0.5
        Connections {
          enabled: !app.isPhone
          target: homeManager
          function onCreateCustomAttribute() {
            attributesPopup.close()
          }
        }
      }
    }

    Rectangle {
      Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
      Layout.bottomMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
      visible: quodEditor.object.custom && homeManager.creatingObject
    }

    PhotosPanel {
      id: photosPanel
      Layout.fillWidth: true
      visible: (homeManager.creatingObject || !quodEditor.editOneSection) && quodEditor.hasPhotos
      Layout.preferredHeight: height
      Layout.bottomMargin: app.isPhone ? ThemeController.style.margin.m8 : ThemeController.style.margin.m16
      onFocusNextSection: nameInput.textField.forceActiveFocus()
      onFocusPreviousSection: focusLastInSection(sectionsView.count - 1)
      onInputActivated: {
        ensureVisible(content.mapFromGlobal(globalPos), height)
      }
    }

    UIKit.NoteDelegate {
      visible: homeManager.creatingObject || !quodEditor.editOneSection
      Layout.fillWidth: true
      onOpenNotes: homeManager.openPage(Pages.NotePage)
      noteContent: quodEditor.notes
      Layout.preferredHeight: height
      hasSpacer: false
    }


    UIKit.LockObjectPanel {
      Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
      visible: homeManager.creatingObject || !quodEditor.editOneSection
      tipTitle: Strings.lockQuodOrFileTipTitle
      tipContent: Strings.lockQuodOrFileTipContent
      tipInfo: Strings.lockQuodOrFileTipInfo
      showPlaceholder: true
      showDontShowAgain: true
      mobilePage: Pages.LockedQuodInfoPage
      imageSource: "qrc:/illustrations/u7.png"
    }

    Item {
      Layout.preferredHeight: ThemeController.style.margin.m16
      Layout.fillWidth: true
    }
  }

  QuodAttributesInputs {
    id: inputs
    tooltipsUpdateVar: flick.__update
  }
}
