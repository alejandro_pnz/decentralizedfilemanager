import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

import com.testintl 1.0

EditPanel {
  id: activityDelegate

  title: Strings.activityStatus
  hasSave: false
  hasSpacer: false
  hasEdit: true
  enabled: true

  onEditButtonClicked: homeManager.openPage(Pages.EditActivityStatusPage)

  property bool active: false
  property bool blocked: false
  property int activityStatus: {
    if (blocked) return ActivityPanel.ActivityStatus.Blocked
    else if(active) return ActivityPanel.ActivityStatus.Active
    return ActivityPanel.ActivityStatus.Inactive
  }

  enum ActivityStatus {
    Active, Inactive, Blocked
  }

  // Base delegate
  UIKit.CardDelegate {
    Layout.fillWidth: true
    actionButton: false
    textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    textComponent.elide: Text.ElideNone
    avatar.iconSize: s(24)
    avatar.pixelSize: s(48)
    topPadding: s(18)
    bottomPadding: s(18)
    implicitHeight: isMobile ? s(94) : s(80)

    title: {
      switch (activityStatus) {
      case ActivityPanel.ActivityStatus.Blocked: return Strings.blocked
      case ActivityPanel.ActivityStatus.Active: return Strings.active
      case ActivityPanel.ActivityStatus.Inactive: return Strings.inactive
      }
    }
    text: {
      switch (activityStatus) {
      case ActivityPanel.ActivityStatus.Blocked: return Strings.logsOutAllOtherDevices
      case ActivityPanel.ActivityStatus.Active: return Strings.allowToLogin
      case ActivityPanel.ActivityStatus.Inactive: return Strings.cannotLoginWithThisDevice
      }
    }
    icon.source: {
      switch (activityStatus) {
      case ActivityPanel.ActivityStatus.Blocked: return "qrc:/remove-circle.svg"
      case ActivityPanel.ActivityStatus.Active: return "qrc:/check-circle.svg"
      case ActivityPanel.ActivityStatus.Inactive: return "qrc:/delete-dark.svg"
      }
    }
  }
}
