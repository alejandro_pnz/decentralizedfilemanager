import QtQuick 2.15
import QtQml 2.15
import UIKit 1.0 as UIKit
import UIModel 1.0 as UIModel
import com.test 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2
import AppStyles 1.0

RowLayout {
  id: menu
  anchors.fill: parent
  spacing: ThemeController.style.margin.m24
  visible: ism ? ism.hasSelection : false

  property ItemSelectionModel ism: null
  property string objectTypeName: ""
  property int objectType
  property var rootPage: null
  property var actionModel: objectActionModel

  // This property holds the maximum number of buttons that can be shown in bottom menu
  property int maximumMenuButtons: 6

  Binding {
    when: rootPage !== null
    target: rootPage
    property: "footerVisible"
    value: menu.visible
  }

  UIKit.StyledCheckBox {
    id: checkBox
    Layout.alignment: Qt.AlignVCenter
    Layout.minimumWidth: s(130)
    Layout.maximumWidth: implicitWidth
    Layout.fillWidth: true
    checked: true
    text: ""
    Binding on text {
      restoreMode: Binding.RestoreNone
      when: menu.visible
      value: ism.selectedIndexes.length < ism.model.rowCount()
             ? Strings.xObjectsSelected.arg(ism.selectedIndexes.length).arg(objectTypeName)
             : Strings.allObjectsSelected.arg(objectTypeName)
    }

    onClicked: {
      if (ism.selectedIndexes.length < ism.model.rowCount()) {
        console.log("Select all case")
        for (var i = 0; i < ism.model.rowCount(); ++i)
          ism.select(ism.model.index(i,0), ItemSelectionModel.Select)
      } else {
        ism.clearSelection()
      }
      checked = true
    }
  }
  UIKit.GenericListView {
    id: listView
    Layout.fillHeight: true
    Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
    Layout.minimumWidth: contentWidth
    Layout.maximumWidth: contentWidth
    Layout.leftMargin: -ThemeController.style.margin.m12
    spacing: 0
    orientation: Qt.Horizontal
    clip: true
    model: bottomMenuProxy
    property int delegateWidth: s(83)
    delegate: UIKit.MenuItem {
      size: UIKit.MenuItem.Size.Big
      width: listView.delegateWidth
      anchors.verticalCenter: listView.contentItem.verticalCenter
      text: name
      iconSrc: iconSource
      checkable: false
      action: actionSource
    }
  }

  UIKit.ExportQuodPopup {
    id: exportQuodPopup
    property int xPosition: 0
    y: - height - s(8)
    x: xPosition
  }

  Rectangle {
    id: spacer
    width: s(1)
    height: s(60)
    color: ThemeController.style.separatorColor
    visible: moreMenuProxy.count !== 0
  }

  UIKit.MenuItem {
    id: moreButton
    Layout.alignment: Qt.AlignVCenter
    size: UIKit.MenuItem.Size.Big
    text: Strings.more
    checkable: false
    iconSrc: "qrc:/more-2.svg"
    onClicked: {
        moreMenu.open()
    }
    visible: moreMenuProxy.count !== 0

    ActionMenu {
      id: moreMenu
      model: moreMenuProxy
      y: -moreMenu.height - s(24)
      x: -moreMenu.width + parent.width
    }
  }


  UIModel.ObjectActionModel {
    id: objectActionModel

    objectType: menu.objectType
    selection: ism.selectedIndexes

    actionList: selectMenuAction

    onCleanSelection: {
      console.log("Clear selection")
      ism.clearSelection()
    }

    onModelLoaded: {
      updateModel()
    }

    onOpenExportPopup: {
      exportQuodPopup.xPosition = xPos + exportQuodPopup.width * 0.5 - listView.delegateWidth * 0.5
      exportQuodPopup.open()
    }
  }

  SortFilterProxyModel {
    id: bottomMenuProxy
    sourceModel: actionModel.model
    filters: [
      ValueFilter {
        enabled: true
        roleName: "expanded"
        value: true
      }
    ]
  }

  SortFilterProxyModel {
    id: moreMenuProxy
    sourceModel: actionModel.model
    filters: [
      ValueFilter {
        enabled: true
        roleName: "expanded"
        value: false
      }
    ]
  }

  function updateModel() {
    // Calculate maximum available space for button byt subtract the others components sizes from menu width.
    // 12 is a margin between checkbox and first menu button when window has minimum size (900x600dp).
    var maxAvailableSpace = menu.width - checkBox.Layout.minimumWidth
        - spacer.width
        - moreButton.width
        - 3 * menu.spacing + s(12)
    var maxVisDel = Math.floor(maxAvailableSpace / listView.delegateWidth)
    for (var i = 0; i < actionModel.sourceModel.count; ++i) {
      actionModel.sourceModel.setProperty(i, "expanded", i < maxVisDel && i < menu.maximumMenuButtons)
    }
  }

  onWidthChanged: updateModel()
}
