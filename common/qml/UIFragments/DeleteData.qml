import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  spacing: ThemeController.style.margin.m8

  state: "desktop"

  signal accept()
  signal reject()

  UIKit.BaseText {
    id: title
    text: Strings.deleteData
    size: UIKit.BaseText.TextSize.H3
    font.weight: Font.Bold
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.BaseText {
    id: content
    text: Strings.deleteDataAndAccountWarning
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.StyledButton {
    id: confirmButton
    text: Strings.yesDelete
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Destructive
    onClicked: accept()
    Layout.fillWidth: true
  }

  UIKit.StyledButton {
    id: cancelButton
    Layout.topMargin: ThemeController.style.margin.m8
    text: Strings.noKeepMyData
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Secondary
    onClicked: reject()
    Layout.fillWidth: true
  }


  states: [
    State {
      name: "mobile"
      PropertyChanges {
        target: content
        Layout.topMargin: ThemeController.style.margin.m4
      }
      PropertyChanges {
        target: confirmButton
        Layout.topMargin: ThemeController.style.margin.m24
      }
    },
    State {
      name: "desktop"
      PropertyChanges {
        target: content
        Layout.topMargin: ThemeController.style.margin.m8
      }
      PropertyChanges {
        target: confirmButton
        Layout.topMargin: 0
      }
    }
  ]
}
