import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.testintl 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import UIFragments 1.0
import SortFilterProxyModel 0.2

Flickable {
  id: flickable
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true

  property bool globalEditMode: false

  signal focusSection(var idx)
  signal focusLastInSection(var idx)

  function ensureVisible(p, h) {
    if (flickable.contentY + flickable.height < p.y) {
      flickable.contentY = p.y - flickable.height
    } else if (contentY > p.y - h) {
      flickable.contentY = p.y - h
    }
  }

  function focusPreviousSection(idx) {
    // check the section
    if (idx < 0) {
      //if next section does not exists focus photo if exist
      // or last in last section
      if (photosPanel.editMode && photosPanel.hasPhotos) {
        photosPanel.focusLastInput()
      } else {
        focusLastInSection(sectionsView.count - 1)
      }
    } else {
      // Focus next section
      focusLastInSection(idx)
    }
  }

  function focusNextSection(idx) {
    // check the section
    if (idx  === sectionsView.count) {
      if (photosPanel.editMode && photosPanel.hasPhotos) {
        // Focus first photo name
        photosPanel.focusFirstInput()
      } else {
        //if next section does not exists focus first section
        focusSection(0)
      }
    } else {
      // Focus next section
      focusSection(idx)
    }
  }

  // Helper variable to trigger binding update
  property bool __update: false
  function updateTooltipsPositions() {
    flickable.__update = !flickable.__update
  }

  function save() {
    if (globalEditMode) {
      if (validate()) {
        homeManager.currentEditor.save()
        globalEditMode = false
      }
    }
  }

  function validate() {
    console.log("TODO: implement mandatory fields validation")

    for (var i = 0; i < sectionsView.count; ++i) {
      var inputView = sectionsView.itemAtIndex(i).inputView;
      if (inputView) {
        for (var j = 0; j < inputView.count; j++) {
          var input = inputView.itemAtIndex(j).loader.item;
          if (input) {
            if (input.fileToImport !== undefined && input.fileToImport)
            {
              console.log("SPOT FILE TO INPUT");
              var importedId = FileListModel.importFile(input.fileToImportPath)
              if(importedId !== ""){
                input.textChanged(importedId)
              }
            }
          }
        }
      }
    }
    return true
  }

  EditGroup {
    id: editGroup
  }
  property string noteContent: "For all shared accounts;
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laboru..."
  property string authenticatorCode: "375 923"
  property bool authenticatorEditMode: false
  property int inputWidth: (width - ThemeController.style.margin.m32) * 0.5 < s(268) ? width : (width - ThemeController.style.margin.m32) * 0.5

  // Open the tooltips recursively
  property var tooltips: []
  property int lastOpenedTooltip: 0
  function nextTooltip() {
    if (tooltips.length > lastOpenedTooltip) {
      tooltips[lastOpenedTooltip].open()
      tooltips[lastOpenedTooltip].aboutToHide.connect(nextTooltip)
      lastOpenedTooltip = lastOpenedTooltip + 1
    }
  }

  Item {
    QuodArcaSettings {
      id: settings
      property bool tooltipsDisplayed: false
    }
    Connections {
      target: quodEditor
      function onGlobalEdit() {
        console.log("Global edit requested")
        if(!app.isPhone)
          globalEditMode = true;
      }

      function onAttributesSaved() {
        console.log("Global edit finished")
        globalEditMode = false;
      }
    }
  }

  Timer {
    interval: 300
    running: true
    onTriggered: {
      if(!settings.tooltipsDisplayed){
        settings.tooltipsDisplayed = true
        nextTooltip()
      }
    }
  }

  ColumnLayout {
    id: content
    spacing: 0
    width: parent.width

    QuodSectionsModel {
      id: sectionsModel
      quod: quodEditor.object
    }

    SortFilterProxyModel {
      id: sectionsProxy
      sourceModel: sectionsModel
      filters: [
        ValueFilter { // filter out special sections like photos
          roleName: "specialSection"
          value: false
        },
        ValueFilter {
          roleName: "type"
          value: QuodSectionObject.Generic
          enabled: !globalEditMode
        }
      ]
    }

    InfoDelegate {
      id: emptyDataInfo
      contentText: Strings.noDataFoundInQuod
      visible: sectionsView.contentHeight <= 0 // TODO
      Layout.fillWidth: true
    }

    Rectangle {
      visible: emptyDataInfo.visible
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
      Layout.bottomMargin: app.isPhone ? ThemeController.style.margin.m8 : ThemeController.style.margin.m16
      color: ThemeController.style.seashellColor
    }

    ListView {
      id: sectionsView
      model: sectionsProxy
      Layout.fillWidth: true
      Layout.preferredHeight: contentHeight
      boundsBehavior: ListView.StopAtBounds
      spacing: 0

      property bool editMode: false

      delegate: Item {
        id: d
        width: sectionsView.width
        height: delegate.visible ? delegate.height + ThemeController.style.margin.m16 : 0
        Connections {
          target: flickable
          function onFocusSection(idx) {
            if (idx === delegate.sectionIndex) {
              // Focus first item
              grid.focusNextItem(0)
            }
          }
          function onFocusLastInSection(idx){
            if (idx === delegate.sectionIndex) {
              // Focus first item
              grid.focusNextItem(grid.count - 1)
            }
          }
        }
        EditPanel {
          id: delegate
          width: sectionsView.width
          visible: editMode || attributesProxy.count > 0 || globalEditMode
          enable: !app.isPhone
          canEdit: !sectionsView.editMode || editMode
          hasEdit: false
          hasSave: false
          hasSpacer: index > 0

          Item {
            Connections {
              target: flickable
              function onGlobalEditModeChanged() {
                if (globalEditMode) {
                  delegate.edit()
                } else {
                  delegate.finishEditing()
                }
              }
            }
            Connections {
              target: quodEditor
              function onAttributesSaved() {
                if (delegate.editMode) {
                  console.log("single edit finished")
                  sectionsView.editMode = false
                  delegate.finishEditing()
                }
              }
            }
          }

          property var sectionId: model.id
          property var section: model.object
          property int sectionIndex: model.index
          property bool sectionIsAddress: model.type === QuodSectionObject.Address

          hasBottomSpacer: true
          title: name

          onEditButtonClicked: {
            sectionsView.editMode = true
            if (app.isPhone) {
              quodEditor.editedSectionId = section.id
              quodEditor.editedSectionName = delegate.title
              homeManager.openPage(Pages.EditQuodDetailsPage)
            }
          }
          onSaveButtonClicked: {
            sectionsView.editMode = false
            homeManager.currentEditor.save()
          }
          onCancelButtonClicked: {
            sectionsView.editMode = false
            attributesModel.revertChanges()
          }

          Item {
            AttributeSectionModel {
              id: attributesModel
              section: delegate.section
            }
            SortFilterProxyModel {
              id: attributesProxy
              sourceModel: attributesModel
              filters: [
                ValueFilter { // Filter out empty attributes
                  roleName: "isEmpty"
                  enabled: !delegate.editMode
                  value: false
                },
                ValueFilter {
                  roleName: "type"
                  enabled: delegate.editMode
                  value: AttributeObject.Address
                  inverted: true
                },
                ValueFilter {
                  roleName: "type"
                  enabled: grid.singleColumn
                  value: AttributeObject.Spacer
                  inverted: true
                }
              ]
            }
          }
          Item {
            Layout.fillWidth: true
            Layout.minimumHeight: grid.contentHeight - grid.rowSpacing
            Layout.preferredHeight: grid.contentHeight - grid.rowSpacing
            Layout.maximumHeight: grid.contentHeight - grid.rowSpacing
            GridView {
              boundsBehavior: Flickable.StopAtBounds
              id: grid
              height: grid.contentHeight
              width: delegate.width + columnSpacing
              model: attributesProxy
              property int columnSpacing: ThemeController.style.margin.m16
              property int rowSpacing: ThemeController.style.margin.m32
              property int delegateWidth: cellWidth - columnSpacing
              property int delegateHeight: cellHeight - rowSpacing
              property bool singleColumn: cellWidth == grid.width

              signal focusNextItem(var idx)

              cellWidth: {
                if (app.isPhone) return grid.width // one column on phones
                if ((grid.width - columnSpacing) * 0.5 < s(268)) return grid.width
                return grid.width / 2
              }
              cellHeight: s(80) + rowSpacing

              delegate: Item {
                id: gridDelegate
                property int i: index
                height: grid.cellHeight
                width: grid.cellWidth

                Connections {
                  target: grid
                  function onFocusNextItem(idx) {
                    if (idx === gridDelegate.i) {
                      loader.item.activateInput()
                    }
                  }
                }

                Loader {
                  id: loader
                  active: globalEditMode || sectionsView.editMode || model.value !== ""
                  width: grid.delegateWidth
                  height: grid.delegateHeight
                  property var attributeValue: model.value
                  onAttributeValueChanged: {
                    if (loader.item) {
                      console.log("QuodDataTab: Attribute value changed", attributeValue, model.name);
                      loader.item.value = attributeValue;
                    }
                  }
                  property int attributeIndex: index
                  property var attributeId: model.id
                  onWidthChanged: updateTooltipsPositions()
                  onHeightChanged: updateTooltipsPositions()
                  sourceComponent: {
                    console.log("QuodDataTab: ", model.type, model.name, model.placeholder)
                    switch(type) {
                    case AttributeObject.Name: return inputs.nameInput
                    case AttributeObject.Text: return inputs.loginInput
                    case AttributeObject.Password: return inputs.passwordInput
                    case AttributeObject.Authenticator: return inputs.authenticatorInput
                    case AttributeObject.Url: return inputs.websiteInput
                    case AttributeObject.Date: return inputs.dateInput
                      //                case AttributeObject.Country: return inputs.countryInput
                      //                case AttributeObject.Height: return inputs.heightInput
                      //                case AttributeObject.Gender: return inputs.genderInput
                    case AttributeObject.Number: return inputs.numberInput
                    case AttributeObject.Spacer: return inputs.emptyInput
                    }
                    return inputs.loginInput
                  }
                  onLoaded: {
                    if (model.type === AttributeObject.Spacer) return;
                    if (model.type === AttributeObject.Password) {
                      item.input.hasHistory = Qt.binding(function(){return model.history.length > 0})
                    }
                    console.log("QuodDataTab: ON LOADED", type, name, delegate.sectionIndex, attributeIndex, object.name, loader.attributeValue,
                                "readonly?", !delegate.editMode, model.maximumLength)
                    item.sectionId = delegate.sectionId
                    item.attributeId = loader.attributeId
                    item.labelText = model.name
                    item.placeholderText = model.placeholder
                    item.maxLength = model.maximumLength
                    item.sensitive = model.sensitive
                    item.removable = quodEditor.object.custom && (model.type !== AttributeObject.Name)
                    item.value = model.value
                    if (delegate.sectionIsAddress) {
                      item.readonly = false
                    } else {
                      item.readonly = Qt.binding(function(){return !delegate.editMode})
                    }
                    item.onTextChanged.connect(function(text){
                      quodEditor.setAttributeValue(delegate.sectionId, attributeId, text, delegate.sectionIndex)
                    })
                    item.onGeneratePassword.connect(function(){
                      homeManager.enterPasswordGeneratorPage(delegate.sectionId, attributeId, delegate.sectionIndex);
                    })
                    item.onEnterPasswordHistory.connect(function(){
                      homeManager.enterPasswordHistoryPage(model.history);
                    })
                    item.input.onBacktabPressed.connect(function() {
                      if (index === 0) {
                        flickable.focusPreviousSection(delegate.sectionIndex - 1)
                      } else {
                        grid.focusNextItem(index - 1)
                      }
                    })
                    item.input.onEnterPressed.connect(function() {
                      if (index + 1 === grid.count) {
                        flickable.focusNextSection(delegate.sectionIndex + 1)
                      } else {
                        grid.focusNextItem(index + 1)
                      }
                    })
                    item.input.onTabPressed.connect(function() {
                      if (index + 1 === grid.count) {
                        flickable.focusNextSection(delegate.sectionIndex + 1)
                      } else {
                        grid.focusNextItem(index + 1)
                      }
                    })
                    item.input.textField.activeFocusChanged.connect(function() {
                      if (item.input.textField.activeFocus) {
                        var point = item.input.mapToItem(content,0,item.height)
                        ensureVisible(point, item.height)
                      }
                    })
                  }
                }
              }
            }
          }
        }
      }
    }

    PhotosPanel {
      id: photosPanel
      Layout.bottomMargin: app.isPhone ? ThemeController.style.margin.m8 : ThemeController.style.margin.m16
      canEdit: !sectionsView.editMode || globalEditMode
      Layout.fillWidth: true
      Layout.minimumHeight: height
      Layout.preferredHeight: height
      visible: quodEditor.hasPhotos
      hasBottomSpacer: true
      onFocusNextSection: nameInput.textField.forceActiveFocus()
      onFocusPreviousSection: focusLastInSection(sectionsView.count - 1)
      onInputActivated: {
        ensureVisible(content.mapFromGlobal(globalPos), height)
      }
    }

    NoteDelegate {
      canEdit: !sectionsView.editMode || globalEditMode
      hasSpacer: false
      hasBottomSpacer: globalEditMode
      Layout.fillWidth: true
      Layout.preferredHeight: height
      noteContent: quodEditor.object.notes
      onOpenNotes: homeManager.editNote()
    }


    LockObjectPanel {
      Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
      Layout.bottomMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
      Layout.preferredHeight: implicitHeight
      tipTitle: Strings.lockQuodOrFileTipTitle
      tipContent: Strings.lockQuodOrFileTipContent
      tipInfo: Strings.lockQuodOrFileTipInfo
      showPlaceholder: true
      showDontShowAgain: true
      mobilePage: Pages.LockedQuodInfoPage
      visible: globalEditMode
      imageSource: "qrc:/illustrations/u7.png"
    }
  }

  QuodAttributesInputs {
    id: inputs
    tooltipsUpdateVar: flickable.__update
  }
}
