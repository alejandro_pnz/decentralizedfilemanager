import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import QtQml.Models 2.12

import com.testintl 1.0

ColumnLayout {
  id: root
  width: parent.width
  property alias stackLayout: stackLayout
  property int horizontalMargins: 0
  property bool selectionMode: stackLayout.children[stackLayout.currentIndex].selectionMode
  property ItemSelectionModel ism: stackLayout.children[stackLayout.currentIndex].ism
  property string objectTypeName: stackLayout.children[stackLayout.currentIndex].objectTypeName
  property int objectType: stackLayout.children[stackLayout.currentIndex].objectType

  property bool isAO: AuthenticationManager.currentAccount.accountOwner
  property bool isGuest: AuthenticationManager.currentAccount.guest

  property var givePowerPopup: null

  spacing: ThemeController.style.margin.m16

  TabBar {
    id: bar
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.alignment: isMobile ? Qt.AlignHCenter : Qt.AlignLeft
    spacing: ThemeController.style.margin.m12
    StyledTabButton {
      text: Strings.account
    }
    StyledTabButton {
      text: Strings.myContacts
    }
  }

  Rectangle {
    id: spacer
    Layout.fillWidth: true
    Layout.leftMargin: isMobile ? -ThemeController.style.margin.m16 : -horizontalMargins
    Layout.rightMargin: isMobile ? -ThemeController.style.margin.m16 : -horizontalMargins
    color: ThemeController.style.mercuryColor
    height: s(1)
  }

  StackLayout {
    id: stackLayout
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    Layout.fillHeight: true
    currentIndex: bar.currentIndex

    AllUsersTab {
      id: allUsersTab
    }

    ContactsTab {
      id: contactsTab
      visible: !isGuest
    }
  }
}
