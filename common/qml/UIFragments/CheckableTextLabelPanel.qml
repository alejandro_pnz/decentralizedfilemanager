import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import QtQml.Models 2.12
import UIModel 1.0 as UIModel

Column {
  id: panel
  spacing: ThemeController.style.margin.m16

  signal delegateClicked(var index)
  property string title
  property var model
  property var ism
  property var searchModel
  property alias exclusive: buttonsGroup.exclusive
  readonly property alias titleComponent: text
  property bool hideChecked: false

  property int selectedCount: ism ? ism.selectedIndexes.length : 0

  function clearAll() {
    if (ism) ism.clearSelection()
  }

  UIKit.BaseText {
    id: text
    text: panel.title
    anchors.left: parent.left
    anchors.right: parent.right
    font.capitalization: Font.AllUppercase
    font.weight: Font.DemiBold
    size: UIKit.BaseText.TextSize.Small
    color: ThemeController.style.charadeColor
  }

  Flow {
    anchors.left: parent.left
    anchors.right: parent.right
    spacing: ThemeController.style.margin.m12
    Repeater {
      id: repeater
      model: panel.model
      UIKit.CheckableTextLabelDelegate {
        visible: hideChecked ? !checked : true
        selectionModel: panel.ism ? panel.ism : null
        model: panel.model
        text: name
        onClicked: delegateClicked(index)
        ButtonGroup.group: buttonsGroup
        checked: {
          if (ism) {
            return ism.isSelected(model.index(index, 0))
          }
          return false
        }
      }
    }
  }

  ButtonGroup {
    id: buttonsGroup
    exclusive: false
  }
}
