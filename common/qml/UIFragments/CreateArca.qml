import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import UIFragments 1.0 as UIFragments
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import UIModel 1.0

Flickable {
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true

  function validate() {
    if (validateMandatoryField(nameInput)) {
      return true
    }
    return false
  }

  function save() {
    // TODO:
    arcaEditor.color = buttonsGroup.checkedButton.itemColor
  }

  ColumnLayout {
    id: content
    width: parent.width
    spacing: ThemeController.style.margin.m16

    UIKit.InputField {
      id: nameInput
      Layout.topMargin: ThemeController.style.homePageVerticalMargin
      labelText: Strings.name
      placeholderTxt: Strings.typeName
      inputRequired: true
      textField.text: arcaEditor.name
      onTextChanged: arcaEditor.name = text
    }

    UIKit.BaseText {
      Layout.topMargin: ThemeController.style.margin.m8
      text: Strings.colour
      font.weight: Font.Bold
    }

    ListView {
      id: listView
      Layout.fillWidth: true
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
      spacing: ThemeController.style.margin.m12
      orientation: ListView.Horizontal
      interactive: true
      clip: true
      model: colorModel
      delegate: UIKit.ColorItem {
        itemColor: colorValue
        ButtonGroup.group: buttonsGroup
        //set first element checked by default
        checked: index === 0 ? true : false
      }
    }

    ButtonGroup {
      id: buttonsGroup
      exclusive: true
    }

    UIKit.NoteDelegate {
      id: noteDelegate
      Layout.topMargin: ThemeController.style.margin.m4
      Layout.fillWidth: true
      onOpenNotes: homeManager.editNote()
      noteContent: arcaEditor.notes
    }

    UIKit.BaseText {
      Layout.topMargin: app.isPhone ? 0 : ThemeController.style.margin.m8
      text: Strings.linkData
      font.weight: Font.Bold
    }

    UIKit.BaseText {
      text: Strings.pickFromLibrary
      color: ThemeController.style.shuttleColor
    }

    UIKit.SearchInput {
      id: search
      Layout.fillWidth: true
      placeholderText: Strings.searchQuodOrFiles
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
      micIconVisible: false
    }

    UIFragments.ObjectListSortButton {
      id: sortButton
      headerBottomMargin: 0
      Layout.topMargin: isPhone ? s(8) : 0
    }

    UIKit.GenericListView {
      id: listViewLink
      Layout.fillWidth: true
      Layout.preferredHeight: contentHeight
      Layout.topMargin: isPhone ? ThemeController.style.margin.m4 : 0
      spacing: isPhone ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
      clip: true
      interactive: false

      model: sortModel.proxy
      delegate: UIKit.ObjectDelegate {
        title: model.name
        text: model.content
        titleIcon: model.locked
        avatar.shareMode: model.syncProperty
        avatar.iconSrc: "image://item/type=" + model.objectType + "&id=" + model.id

        width: listViewLink.width
        checkboxOnTheLeft: !isMobile
        checkable: true
        delegateModel: sortModel.proxy
        selectionMode: true
        selectionModel: arcaEditor.linkedObjectsModel
        selected: selectionModel.isSelected(delegateModel.mapToSource(delegateModel.index(index, 0)))
      }
    }

    SortModel {
      id: sortModel
      searchPattern: search.searchText
      sourceModel: QuodFileListModel
      sorter: sortButton.sorter
      pinnedFilterEnabled: false
    }

    ListModel {
      id: colorModel
    }   
  }

  Component.onCompleted: {

    arcaEditor.create();

    colorModel.append({ colorValue: "#5C5AE8"});
    colorModel.append({ colorValue: "#E042FB"});
    colorModel.append({ colorValue: "#4CBDFF"});
    colorModel.append({ colorValue: "#22D396"});
    colorModel.append({ colorValue: "#FFB904"});
    colorModel.append({ colorValue: "#2F3441"});
  }
}
