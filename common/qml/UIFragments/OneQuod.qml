import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

import com.testintl 1.0

ColumnLayout {
  id: root
  width: parent.width
  readonly property bool isMobile: app.isPhone
  property alias stackLayout: stackLayout
  property bool showFooter: stackLayout.currentIndex === 1 || showDataTabFooter
  property bool showDataTabFooter: stackLayout.currentIndex === 0 && globalEditMode
  spacing: ThemeController.style.margin.m16
  signal generatePassword()
  signal enterPasswordHistory()
  property alias globalEditMode: quodDataTab.globalEditMode

  function saveQuodData() {
    if (stackLayout.currentIndex == 0) {
      quodDataTab.save()
    } else {
      propertiesTab.save()
    }
  }

  ObjectDelegate {
    Layout.topMargin: ThemeController.style.homePageVerticalMargin
    Layout.fillWidth: true
    Layout.preferredHeight: s(103)
    Layout.alignment: Qt.AlignHCenter

    background: Rectangle{
      anchors.fill: parent
    }
    topPadding: 0
    leftPadding: 0
    rightPadding: 0
    bottomPadding: 0
    avatar.size: ObjectAvatar.Size.Big
    avatar.shareMode: quodEditor.object.syncProperty
    avatar.iconSrc: "image://item/type=" + SyncableObject.Quod + "&id=" + quodEditor.object.id
    dropShadow.visible: false
    titleIcon: quodEditor.object.locked
    checkable: false
    title: quodEditor.object.name
    titleComponent.color: ThemeController.style.font.defaultColor
    titleComponent.size: BaseText.TextSize.H3
    text: Utility.formatQuodContent(quodEditor.object.id)
    onTextLinkActivated: {
      homeManager.openOneArcaPage(link)
    }
  }

  Item {
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.preferredHeight: bar.height
    Layout.fillWidth: true
    TabBar {
      id: bar
      anchors {
        left: app.isPhone ? undefined : parent.left
        horizontalCenter: app.isPhone ? parent.horizontalCenter : undefined
      }

      spacing: ThemeController.style.margin.m12
      StyledTabButton {
        icon.source: "qrc:/linked-data.svg"
        text: Strings.dataStr
      }
      StyledTabButton {
        icon.source: "qrc:/properties.svg"
        text: Strings.properties
      }
    }

    StyledButton {
      id: editButton
      text: quodDataTab.globalEditMode ? Strings.cancel : Strings.edit
      type: StyledButton.ButtonStyle.Text
      displayMode: StyledButton.DisplayMode.TextOnly
      anchors.right: parent.right
      visible: stackLayout.currentIndex == 0 && !app.isPhone // QuodDataTab
      onClicked: {
        quodDataTab.globalEditMode = !quodDataTab.globalEditMode
      }
    }
  }

  Rectangle {
    id: spacer
    Layout.topMargin: isMobile ? ThemeController.style.margin.m8 : 0
    Layout.fillWidth: true
    Layout.leftMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    Layout.rightMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    color: ThemeController.style.mercuryColor
    height: s(1)
  }

  StackLayout {
    id: stackLayout
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    Layout.fillHeight: true
    currentIndex: bar.currentIndex

    QuodDataTab {
      id: quodDataTab
    }

    QuodPropertiesTab {
      id: propertiesTab
    }
  }
}
