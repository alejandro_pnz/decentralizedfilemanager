import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  spacing: ThemeController.style.margin.m16

  Rectangle {
    Layout.preferredHeight: app.isMobile ? s(222) : s(230)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: "qrc:/illustrations/r28.png"
      mipmap: true
    }
  }

  UIKit.BaseText {
    Layout.topMargin: ThemeController.style.margin.m8
    text: Strings.inviteUsersToAccount
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Text.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  Repeater {
    id: repeater
    model: 1
    UIKit.InputField {
      id: emailInput
      Layout.topMargin: index === 0 ? ThemeController.style.margin.m8 : 0
      Layout.fillWidth: true
      labelText: Strings.inviteUsersEmail
      placeholderTxt: Strings.typeUserEmail
      inputRequired: false
      fieldType: UIKit.InputField.FieldType.Email
    }
  }

  UIKit.StyledButton {
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    text: Strings.addUsers
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      var error = true
      for (var i = 0; i < repeater.count; ++i) {
        var item = repeater.itemAt(i)
        if (item.textField.text !== "") {
          error = false
          break
        }
      }
      if (!error) RegistrationManager.inviteUsers()
    }
  }

  UIKit.BaseText {
    Layout.topMargin: -ThemeController.style.margin.m4
    text: Strings.or
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.Small
    Layout.alignment: Qt.AlignHCenter
    color: ThemeController.style.shuttleColor
    font.capitalization: Font.AllUppercase
  }

  UIKit.StyledButton {
    id: skip
    Layout.topMargin: -ThemeController.style.margin.m4
    Layout.fillWidth: true
    text: Strings.skipForNow
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Secondary
    onClicked: RegistrationManager.skipInvitingUsers()
  }
}
