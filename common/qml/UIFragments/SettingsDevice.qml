import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: layout
  spacing: 0

  UIKit.InfoDelegate {
    contentText: Strings.onlyDeviceSettings
  }

  UIKit.SettingsDelegate {
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    title: Strings.device
    contentText: Strings.nameAbout
    onClicked: homeManager.openPage(Pages.DeviceSettingsPage)
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.loginAndInactivity
    contentText: Strings.settingsToControl
    onClicked: homeManager.enterLoginInactivitySettingsPage({"pageMode": Enums.DeviceSettings})
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.dataStr
    contentText: Strings.setTheAccessibility
    onClicked: homeManager.enterDataSettingsPage({"pageMode": Enums.DeviceSettings})
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.appPermissions
    contentText: Strings.filesPermissions
    onClicked: homeManager.openPage(Pages.AppPermissionsPage)
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }

  UIKit.StyledButton {
    id: button
    text: Strings.resetSettings
    Layout.bottomMargin: ThemeController.style.margin.m16
    Layout.alignment: Qt.AlignHCenter
    Layout.preferredWidth: app.isTablet ? s(352) : parent.width
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Secondary
    onClicked: homeManager.enterResetSettingsPage({"pageMode": Enums.DeviceSettings})
  }
}
