import QtQuick 2.15
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2
import UIKit 1.0 as UIKit
import QtQml.Models 2.12
import com.testintl 1.0

UIKit.GenericListView {
  id: listView
  anchors.fill: parent
  anchors.topMargin: ThemeController.style.homePageVerticalMargin
  // TODO use values from tag model
  property string tagName: Strings.business
  property color tagColor: "#5C5AE8"

  spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
  clip: true

  ItemSelectionModel {
    id: ism
    model: QuodListModel
  }

  SortFilterProxyModel {
    id: objectsProxyModel
    sourceModel: QuodListModel
    filters: ValueFilter {
      roleName: "pinned"
      value: false
    }
  }

  model: objectsProxyModel
  delegate: UIKit.ObjectDelegate {
    // Model data
    title: model.name
    text: model.content
    avatar.shareMode: model.syncProperty
    actionButton: true
    titleIcon: model.locked
    avatar.iconSrc: "image://item/type=" + model.objectType + "&id=" + model.id

    // Other properties
    width: parent.width
    checkboxOnTheLeft: !app.isMobile
    checkable: !app.isMobile
    delegateModel: listView.model
    selectionModel: ism
    selectionMode: listView.selectionMode

    onDelegateClicked: {
      // Open object
      console.log("Delegate clicked")
      // TODO handle also files
      homeManager.openQuodFromTag(object.id);
    }

    onActionButtonClicked: {
      listView.currentIndex = index
      actionMenu.model.currentObject = object
      actionMenu.handleActionButton(listView)
    }

    onTextLinkActivated: {
      homeManager.openOneArcaPage(link)
    }

    Binding on actionButton {
      when: !app.isPhone
      value: !ism.hasSelection
    }
  }

  OneObjectActionMenu {
    id: actionMenu
  }
}
