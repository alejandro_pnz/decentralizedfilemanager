import QtQuick 2.15
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0 as UIFragments
import QtQml.Models 2.12
import SortFilterProxyModel 0.2
import UIModel 1.0
import QtQuick.Controls 2.15

Item {
  id: menu
  property alias popup: singleObjectActionPopup
  property var currentItem
  property var selection: []

  // If true popup is placed on the right side of delegate
  property bool arcaMode: false

  property var model: null

  UIFragments.ActionMenu {
    id: singleObjectActionPopup
    model: menu.model.model

    currentItem: menu.model.currentObject
  }

  function handleActionButton(view) {
    // On mobile the action menu is Drawer but on Desktop and Tablets it's popup
    // This is a way to implement different logic
    if (singleObjectActionPopup instanceof Drawer) {
      console.log("Drawer!")
      if (singleObjectActionPopup.opened) {
        singleObjectActionPopup.close()
        if (singleObjectActionPopup.currentItem !== view.currentItem) {
          singleObjectActionPopup.currentItem = view.currentItem
          singleObjectActionPopup.open()
        }
      } else {
        singleObjectActionPopup.currentItem = view.currentItem
        singleObjectActionPopup.open()
      }
    } else {
      console.log("Popup")
      if (singleObjectActionPopup.opened) {
        singleObjectActionPopup.close()
        if (singleObjectActionPopup.currentItem !== view.currentItem) {
          singleObjectActionPopup.currentItem = view.currentItem
          singleObjectActionPopup.open()
        }
      } else {
        singleObjectActionPopup.currentItem = view.currentItem
        singleObjectActionPopup.open()
      }

      // TODO (jskorczynski) fix positioning.
      // Feel free to completly rewrite it, remove, fix or whatever
      var posInParent = view.currentItem.mapToItem(menu, 0, 0)
      var posInHomePage = view.currentItem.mapToItem(homePage, 0, 0)
      if (arcaMode) {
        singleObjectActionPopup.x = posInParent.x + view.currentItem.width

      } else {
        singleObjectActionPopup.x = posInParent.x  + view.currentItem.width - singleObjectActionPopup.width - s(64)
      }

      if(singleObjectActionPopup.height < applicationMainWindow.height - (homePage.homeHeader.height + 2 * singleObjectActionPopup.listViewVerticalPadding)) {
          var startY = posInHomePage.y - (homePage.homeHeader.height + singleObjectActionPopup.listViewVerticalPadding)
          if(startY < singleObjectActionPopup.height * 0.5 || applicationMainWindow.height - (homePage.homeHeader.height + 2 * singleObjectActionPopup.listViewVerticalPadding) - startY < singleObjectActionPopup.height * 0.5)
              singleObjectActionPopup.y = -(startY - posInParent.y)
          else
              singleObjectActionPopup.y = posInParent.y - (singleObjectActionPopup.height * 0.5)
      }
      else
          singleObjectActionPopup.y = -(posInHomePage.y - (homePage.homeHeader.height + singleObjectActionPopup.listViewVerticalPadding) - posInParent.y);
    }
  }
}
