import QtQuick 2.15
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0 as UIFragments
import QtQml.Models 2.12
import SortFilterProxyModel 0.2
import UIModel 1.0
import QtQuick.Controls 2.15

GenericActionMenu {
  model: oneUserActionModel
  property alias givePowerPopup: oneUserActionModel.givePowerPopup
  UserActionModel {
    id: oneUserActionModel
    actionList: oneUserActions
  }
}

