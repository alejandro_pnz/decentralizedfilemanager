import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: layout
  spacing: app.isPhone ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
  property alias buttonGroup: btnGroup
  signal buttonClicked()

  ButtonGroup {
    id: btnGroup
    buttons: layout.children
  }
  
  Rectangle {
    height: s(app.isPhone ? 222 : 230)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: "qrc:/illustrations/u9.png"
      mipmap: true
    }
  }
  
  UIKit.BaseText {
    id: titleComponent
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m12 : ThemeController.style.margin.m24
    alternativeFont: true
    font.weight: Font.Bold
    size: app.isMobile ? UIKit.BaseText.TextSize.H3 : UIKit.BaseText.TextSize.H2
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.alignment: Qt.AlignHCenter
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    text: Strings.successfullyAdded
  }
  
  UIKit.BaseText {
    id: contentComponent
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.alignment: Qt.AlignHCenter
    text: Strings.arcaHelps
  }
  
  UIKit.CardDelegate {
    Layout.topMargin: app.isMobile ? ThemeController.style.margin.m4 : 0
    title: Strings.newArca
    text: Strings.linkData
    icon.source: "qrc:/arca-create.svg"
    Layout.preferredHeight: s(80)
    Layout.fillWidth: true
    pointingHandCursor: true
    onClicked: homeManager.enterCreateArca()
  }
  
  UIKit.BaseText {
    Layout.topMargin: app.isMobile ? 0 : -ThemeController.style.margin.m4
    text: Strings.or
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.Small
    Layout.alignment: Qt.AlignHCenter
    color: ThemeController.style.shuttleColor
    font.capitalization: Font.AllUppercase
  }
  
  UIKit.StyledButton {
    id: button
    Layout.topMargin: app.isMobile ? 0 : -ThemeController.style.margin.m4
    Layout.fillWidth: true
    text: Strings.linkArca
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      homeManager.openPage(Pages.LinkToArcaPage, true)
    }
  }

  RowLayout {
    Layout.topMargin: ThemeController.style.margin.m4
    Layout.fillWidth: true
    UIKit.BaseText {
      text: Strings.dontShowItAgain
      font.weight: Font.DemiBold
      Layout.fillWidth: true
    }

    UIKit.SwitchButton {
      onCheckedChanged: {
        homeManager.setDontShowArca(checked)
      }
    }
  }
}
