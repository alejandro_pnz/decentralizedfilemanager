import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: content
  width: parent.width
  spacing: ThemeController.style.margin.m16

  UIKit.InfoDelegate {
    contentText: Strings.chooseUniqueName
  }

  UIKit.InputField {
    id: nameInput
    labelText: Strings.objectName
    placeholderTxt: Strings.typeName
    inputRequired: false
    clearIconVisible: true
  }
}
