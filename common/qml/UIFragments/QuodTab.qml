import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2
import UIFragments 1.0 as UIFragments
import UIModel 1.0

LibraryTab {
  id: quodTab
  noItemTitle: Strings.noQuodInLibrary
  noItemDescription: Strings.noQuodInLibraryDescription
  noItemIcon: "qrc:/library/quod.svg"
  noItem: QuodListModel.count < 1
  ism.model: QuodListModel
  objectTypeName: Strings.quod
  objectType: SyncableObject.Quod

  SortModel {
    id: sortedProxyModel
    pinnedFilterEnabled: false
    sourceModel: QuodListModel
    sorter: listView.headerItem.sorter
    searchPattern: search.text
  }

  ColumnLayout {
    spacing: ThemeController.style.homePageVerticalMargin
    width: parent.width
    height: parent.height

    UIKit.SearchInput {
      id: search
      Layout.fillWidth: true
      placeholderText: Strings.searchQuod
      micIconVisible: false
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
    }

    UIKit.GenericListView {
      id: listView
      Layout.fillWidth: true
      Layout.fillHeight: true
      clip: true
      spacing: app.isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
      header: Rectangle {
        property alias sorter: sortBtn.sorter
        color: ThemeController.style.whiteColor
        width: listView.width
        height: sortBtn.height
        z: 2

        UIFragments.ObjectListSortButton {
          id: sortBtn
          anchors.top: parent.top
          anchors.left: parent.left
          headerBottomMargin: 0

          Connections {
            target: quodTab
            function onChangeSorter(sorter) {
              sortBtn.init(sorter)
            }
          }

          objectType: Enums.Quod
        }
      }

      headerPositioning: ListView.OverlayHeader

      selectionMode: quodTab.ism.hasSelection

      model: sortedProxyModel.proxy
      delegate: UIKit.ObjectDelegate {
        // Model data
        title: model.name
        text: model.content
        avatar.shareMode: model.syncProperty
        titleIcon: model.locked
        avatar.iconSrc: "image://item/type=" + SyncableObject.Quod + "&id=" + model.id

        // Other properties
        actionButton: !quodTab.ism.hasSelection
        width: listView.width
        checkboxOnTheLeft: !app.isMobile
        checkable: !app.isMobile || selectionMode
        delegateModel: listView.model
        selectionModel: quodTab.ism
        selectionMode: listView.selectionMode

        onDelegateClicked: {
          // Open object
          console.log("Delegate clicked", model.id)
          homeManager.openOneQuodPage(model.id);
        }

        onTextLinkActivated: {
          homeManager.openOneArcaPage(link)
        }

        onActionButtonClicked: {
          listView.currentIndex = index
          actionMenu.model.currentObject = object
          actionMenu.handleActionButton(listView)
        }
      }
    }
  }

  OneObjectActionMenu {
    id: actionMenu
  }
}
