import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0

Flickable {
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true
  property var object: homeManager.currentEditor.object

  property int type: object.objectType

  property string name: object.name
  property string owner: Utility.formatUserName(object.ownerId)
  property string accountId: "TODO"
  property string createdDate: Utility.formatDateTime(object.created)
  property string lastModifiedDate: Utility.formatDateTime(object.modified)
  property string lastSyncDate: Utility.formatDateTime(object.synced)
  property int quodNumber: type === SyncableObject.Arca ? object.quodCount : 0
  property int filesNumber: type === SyncableObject.Arca ? object.filesCount : 0
  property string totalSize: {
    switch(type) {
    case SyncableObject.Arca: return Utility.bytesToSize(Utility.totalSize(object.files))
    case SyncableObject.File: return Utility.bytesToSize(object.size)
    }

    return 0
  }
  property string sender: "TODO" // "andrew@companyemail.com"
  property string receiver: "TODO"  //"johnanders@example.com"
  property string shared: "TODO" // "lauren.work@example.com, bellejk@example.com"
  property string tags: Utility.getTags(object)

  ScrollBar.vertical: ScrollBar {
    width: s(8)
    visible: !isMobile
  }

  ColumnLayout {
    id: content
    width: parent.width
    spacing: ThemeController.style.margin.m24

    UIKit.BaseText {
      Layout.topMargin: ThemeController.style.margin.m16
      text: Strings.icon
      color: ThemeController.style.shuttleColor
    }

    UIKit.ObjectAvatar {
      id: objectAvatar
      Layout.topMargin: -ThemeController.style.margin.m12
      size: UIKit.ObjectAvatar.Size.Small
      iconSrc: "image://item/type=" + object.objectType + "&id=" + object.id
    }

    UIKit.InfoItem {
      labelText: Strings.name
      content: name
    }

    UIKit.InfoItem {
      labelText: Strings.owner
      content: owner
    }

    UIKit.InfoItem {
      labelText: Strings.accountId
      content: accountId
    }

    UIKit.InfoItem {
      labelText: Strings.created
      content: createdDate
    }

    UIKit.InfoItem {
      labelText: Strings.lastModified
      content: lastModifiedDate
    }

    UIKit.InfoItem {
      labelText: Strings.lastSync
      content: lastSyncDate
    }

    UIKit.InfoItem {
      labelText: Strings.numberOfQuod
      content: quodNumber
      visible: type === SyncableObject.Arca
    }

    UIKit.InfoItem {
      labelText: Strings.tags
      content: tags
      visible: tags.length > 0
    }

    UIKit.InfoItem {
      labelText: Strings.numberOfFiles
      content: filesNumber
      visible: type === SyncableObject.Arca
    }

    UIKit.InfoItem {
      labelText: Strings.totalSize
      content: totalSize
    }

    UIKit.InfoItem {
      labelText: Strings.receivedFrom
      content: sender
    }

    UIKit.InfoItem {
      labelText: Strings.sentTo
      content: receiver
    }

    Item {
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
    }
  }
}
