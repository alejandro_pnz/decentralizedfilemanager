import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

Flickable {
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: layout.height
  clip: true

  ColumnLayout {
    id: layout
    spacing: ThemeController.style.margin.m12
    width: parent.width

    property bool isMobile: app.isPhone && !app.isTablet

    Rectangle {
      Layout.preferredHeight: s(isMobile ? 222 : 155)
      Layout.fillWidth: true
      radius: s(10)
      Layout.alignment: Qt.AlignHCenter

      Image {
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        source: "qrc:/illustrations/u6.png"
        mipmap: true
      }
    }

    UIKit.BaseText {
      Layout.topMargin: isMobile ? ThemeController.style.margin.m28 : ThemeController.style.margin.m12
      text: Strings.sthWentWrong
      alternativeFont: true
      font.weight: Font.Bold
      size: isMobile ? UIKit.BaseText.TextSize.H2 : UIKit.BaseText.TextSize.H3
      Layout.alignment: Qt.AlignHCenter
    }

    UIKit.BaseText {
      Layout.topMargin: isMobile ? ThemeController.style.margin.m4 : 0
      Layout.fillWidth: true
      text: Strings.instructionWasNotExecuted
      horizontalAlignment: Text.AlignHCenter
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      Layout.alignment: Qt.AlignHCenter
    }

    UIKit.InfoDelegate {
      Layout.topMargin: ThemeController.style.margin.m4
      contentText: Strings.unlockUnsuccessfulReason
    }

    UIKit.StyledButton {
      Layout.topMargin: ThemeController.style.margin.m20
      Layout.fillWidth: true
      text: Strings.retry
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    }

    UIKit.StyledButton {
      Layout.topMargin: ThemeController.style.margin.m4
      Layout.fillWidth: true
      text: Strings.cancel
      type: UIKit.StyledButton.ButtonStyle.Secondary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    }

    Row {
      Layout.topMargin: ThemeController.style.margin.m4
      Layout.alignment: Qt.AlignHCenter
      spacing: s(10)

      UIKit.BaseText {
        text: Strings.ifErrorPersists
      }

      UIKit.BaseText {
        text: Strings.contactSupport
        font.weight: Font.Bold
        color: ThemeController.style.slatePurpleColor
      }
    }
  }
}
