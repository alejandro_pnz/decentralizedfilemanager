import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQml.Models 2.12
import UIModel 1.0
import SortFilterProxyModel 0.2

ColumnLayout {
  id: content
  width: parent.width
  height: parent.height

  SortFilterProxyModel {
    id: pendingUsersModel
    sourceModel: UserListModel
    filters: ValueFilter {
      roleName: "pending"
      value: true
    }
  }

  ItemSelectionModel {
    id: ism
    model: pendingUsersModel
  }

  SortFilterProxyModel {
    id: objectsProxyModel
    sourceModel: pendingUsersModel
  }

  UIKit.InfoDelegate {
    Layout.topMargin: isMobile ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
    contentText: Strings.seeInvitationRequests
  }

  ListView {
    id: listView
    Layout.fillWidth: true
    Layout.topMargin: isMobile ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
    Layout.fillHeight: true
    spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
    interactive: true
    clip: true

    model: objectsProxyModel

    delegate: Component {
      Loader {
        id: loader
        sourceComponent: isMobile ? mobileDelegate : desktopDelegate
        width: listView.width
        property string emailText: model.email
        property string contentTxt: Utility.formatInvitationContent(model.invitedBy, model.created)

        property int idx: index
      }
    }
  }

  Component {
    id: mobileDelegate
    UIKit.UserRequestMobileDelegate {
      email: emailText
      contentText: contentTxt
    }
  }

  Component {
    id: desktopDelegate
    UIKit.UserRequestDesktopDelegate {
      email: emailText
      contentText: contentTxt
      selectionModel: ism
      model: objectsProxyModel
      index: idx
    }
  }
}
