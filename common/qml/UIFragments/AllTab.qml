import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2
import UIFragments 1.0 as UIFragments
import UIModel 1.0

LibraryTab {
  id: allTab
  noItemTitle: Strings.noQuodInLibrary
  noItemDescription: Strings.noQuodInLibraryDescription
  noItemIcon: "qrc:/library/quod.svg"
  noItem: SyncableObjectListModel.count < 1
  ism.model: SyncableObjectListModel

  objectTypeName: Strings.quod
  //objectType: SyncableObject.Quod

  SortModel {
    id: sortedProxyModel
    pinnedFilterEnabled: false
    sourceModel: SyncableObjectListModel
    sorter: listView.headerItem.sorter
    searchPattern: search.text
  }

  ColumnLayout {
    spacing: ThemeController.style.homePageVerticalMargin
    width: parent.width
    height: parent.height

    UIKit.SearchInput {
      id: search
      Layout.fillWidth: true
      placeholderText: Strings.search
      micIconVisible: false
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
    }

    UIKit.GenericListView {
      id: listView
      Layout.fillWidth: true
      Layout.fillHeight: true
      clip: true
      spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16

      reuseItems: true
      ListView.onPooled: console.log("onPooled")
      ListView.onReused: console.log("onReused")

      header: Rectangle {
        property alias sorter: sortBtn.sorter
        color: ThemeController.style.whiteColor
        width: listView.width
        height: sortBtn.height
        z: 2

        UIFragments.ObjectListSortButton {
          id: sortBtn
          anchors.top: parent.top
          anchors.left: parent.left
          headerBottomMargin: 0

          Connections {
            target: allTab
            function onChangeSorter(sorter) {
              sortBtn.init(sorter)
            }
          }

          objectType: Enums.Arca | Enums.Quod | Enums.File
        }
      }

      headerPositioning: ListView.OverlayHeader

      selectionMode: allTab.ism.hasSelection

      model: sortedProxyModel.proxy
      delegate: UIKit.ObjectDelegate {
        // Model data
        title: model.name
        text: model.content
        avatar.shareMode: model.syncProperty

        //titleIcon: model.locked === undefined ? false : model.locked
        titleIcon: !model.available //model.locked
        titleIconSource: "qrc:/out-of-sync.svg" // TODO: locked

        avatar.iconSrc: "image://item/type=" + model.objectType + "&id=" + model.id

        progress: model.progress

        // Other properties
        actionButton: !allTab.ism.hasSelection
        width: listView.width
        checkboxOnTheLeft: !app.isMobile
        checkable: !isMobile || selectionMode
        delegateModel: listView.model
        selectionModel: allTab.ism
        selectionMode: listView.selectionMode

        onDelegateClicked: {
          // Open object
          switch(model.objectType) {
          case SyncableObject.Arca:
            homeManager.openOneArcaPage(model.id)
            break;
          case SyncableObject.Quod:
            homeManager.openOneQuodPage(model.id)
            break;
          case SyncableObject.File:
            homeManager.openOneFilePage(model.id)
            break;
          default:
            console.warn("Unsupported object type", model.objectType)
            break;
          }
        }

        onTextLinkActivated: {
          homeManager.openOneArcaPage(link)
        }

        onActionButtonClicked: {
          listView.currentIndex = index
          actionMenu.model.currentObject = object
          actionMenu.handleActionButton(listView)
        }
      }
    }
  }

  OneObjectActionMenu {
    id: actionMenu
  }
}
