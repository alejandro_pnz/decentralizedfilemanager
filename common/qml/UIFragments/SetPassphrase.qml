import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import com.testintl 1.0

import QtPromise 1.1

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m16

  signal showHint(point globalPos)
  property bool isLogin: true
  readonly property bool error: passphraseInput.state === 'error' || repeatPassphraseInput.state === 'error'
  signal buttonClicked(string passphrase)

  Connections {
    target: pageManager
    function onSetupSetPassphrasePage(config) {
      layout.buttonClicked.connect(config.buttonHandler)
      layout.isLogin = config.isLogin
      if (config.email !== undefined) {
        loginEmailDelegate.email = config.email
      }
    }
  }

  UIKit.LoginEmailDelegate {
    id: loginEmailDelegate
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
  }

  UIKit.BaseText {
    text: isLogin ? Strings.setLoginPassphrase : Strings.setPowerPassphrase
    Layout.fillWidth: true
    Layout.topMargin: ThemeController.style.margin.m24
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere

    Item{
      anchors.right: parent.right
      anchors.verticalCenter: parent.verticalCenter
      height: tipsLabel.implicitHeight
      width: tipsLabel.implicitWidth
      UIKit.BaseText {
        id: tipsLabel
        anchors.fill: parent
        anchors.verticalCenter: parent.verticalCenter
        text: Strings.tips
        font.weight: Font.Bold
        color: ThemeController.style.slatePurpleColor
      }
      MouseArea {
        anchors.fill: parent
        onClicked: {
          showHint(mapToGlobal(tipsLabel.x, (tipsLabel.y + tipsLabel.height) * .5))
        }
      }
    }
  }

  UIKit.Infobox {
    Layout.fillWidth: true
    Layout.topMargin: ThemeController.style.margin.m40
    text: Strings.yourPassphraseDescription
    textSize: UIKit.BaseText.TextSize.Body
    iconSource: "qrc:/infoBox-circle-icon.svg"
    iconSize: s(32)
  }

  UIKit.InputField {
    id: passphraseInput
    labelText: isLogin ? Strings.passphrase : Strings.powerPassphrase
    placeholderTxt: Strings.examplePassphrase
    strengthIndicatorVisible: true
    eyeIconVisible: true
    inputRequired: true
    isPassPhrase: true
    Layout.fillWidth: true
    onTabPressed: repeatPassphraseInput.textField.forceActiveFocus()
    onEnterPressed: repeatPassphraseInput.textField.forceActiveFocus()
    onBacktabPressed: emailInput.textField.forceActiveFocus()

    onTextChanged: function(text) {
      var promise = Future.promise(PasswordStrengthEvaluator.strength(text))

      promise.then(function(strength) {
        if (strength === PasswordStrengthEvaluator.VeryStrongPassword ||
            strength === PasswordStrengthEvaluator.StrongPassword) {
          passphraseInput.passwordStrengthIndicator.state = 'high'
        }
        else if (strength === PasswordStrengthEvaluator.ReasonablePassword) {
          passphraseInput.passwordStrengthIndicator.state = 'medium'
        }
        else if (strength === PasswordStrengthEvaluator.WeakPassword) {
          passphraseInput.passwordStrengthIndicator.state = 'low'
        }
        else {
          passphraseInput.passwordStrengthIndicator.state = 'verylow'
        }
      });
    }
  }

  UIKit.InputField {
    id: repeatPassphraseInput
    labelText: isLogin ? Strings.repeatPassphrase : Strings.repeatPowerPassphrase
    placeholderTxt: Strings.examplePassphrase
    inputRequired: true
    isPassPhrase: true
    eyeIconVisible: true
    Layout.fillWidth: true
    onTabPressed: emailInput.textField.forceActiveFocus()
    onBacktabPressed: repeatPassphraseInput.textField.forceActiveFocus()
    onEnterPressed: actionButton.clicked()
  }

  UIKit.StyledButton {
    id: actionButton
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    text: isLogin ? Strings.createAccount : Strings.continueTxt
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      var passOK = validatePassphraseInput(passphraseInput, repeatPassphraseInput)
      if (passOK) {
        buttonClicked(passphraseInput.textField.text);
        // TODO do some SDK processing
      }
    }
  }
}
