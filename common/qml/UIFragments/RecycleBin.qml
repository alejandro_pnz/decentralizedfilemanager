import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import UIFragments 1.0 as UIFragments
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2
import QtQml.Models 2.12
import UIModel 1.0

import com.testintl 1.0

ColumnLayout {
  id: column
  spacing: 0
  anchors.fill: parent
  readonly property bool isMobile: app.isPhone
  property bool noItem: RecycleBinModel.count < 1
  property alias ism: ism
  property bool selectionMode: ism.hasSelection

  Connections {
    target: homeManager
    function onClearLibrarySelection() {
      column.ism.clearSelection()
    }
  }

  ItemSelectionModel {
    id: ism
    model: RecycleBinModel
  }

  SortModel {
    id: sortedProxyModel
    sourceModel: RecycleBinModel
    sorter: listView.headerItem.sorter
    searchPattern: search.text
  }

  UIKit.SearchInput {
    id: search
    implicitWidth: 0
    Layout.topMargin: ThemeController.style.homePageVerticalMargin
    Layout.fillWidth: true
    placeholderText: Strings.search
    micIconVisible: false
    Layout.preferredHeight: ThemeController.style.defaultInputHeight

  }

  NoItemsInfo {
    id: noItemsColumn
    Layout.fillWidth: true
    Layout.fillHeight: true
    title: Strings.emptyRecycleBin
    text: Strings.recycleBinDescription
    iconSource: "qrc:/bin-default.svg"
    topMargin: app.isPhone ? s(88) : s(140)
    visible: noItem
  }

  ColumnLayout {
    id: pinnedItemsColumn
    visible: !noItem
    width: parent.width
    spacing: ThemeController.style.margin.m12
    Layout.topMargin: ThemeController.style.margin.m24

    UIKit.GenericListView {
      id: listView
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.topMargin: ThemeController.style.margin.m4
      clip: true
      spacing: ThemeController.style.margin.m12
      selectionMode: ism.hasSelection
      header: Rectangle {
        property alias sorter: sortBtn.sorter
        color: ThemeController.style.whiteColor
        width: listView.width
        height: sortBtn.height
        z: 2

        UIFragments.ObjectListSortButton {
          id: sortBtn
          anchors.top: parent.top
          anchors.left: parent.left
          headerBottomMargin: 0

          objectType: Enums.Arca | Enums.Quod | Enums.File
        }
      }

      headerPositioning: ListView.OverlayHeader

      model: sortedProxyModel.proxy
      delegate: UIKit.ObjectDelegate {
        property var locked: model.locked
        // Model data
        title: model.name
        text: model.content
        avatar.shareMode: model.syncProperty
        titleIcon: locked === undefined ? false : locked
        avatar.iconSrc: "image://item/type=" + model.objectType + "&id=" + model.id

        // Other properties
        width: listView.width
        actionButton: false
        checkboxOnTheLeft: !isMobile
        checkable: !isMobile
        delegateModel: listView.model
        selectionModel: ism
        selectionMode: listView.selectionMode

        onDelegateClicked: homeManager.enterEditPage(model.id, Pages.ShowInfoPage,  model.objectType)

        onTextLinkActivated: {
          homeManager.openOneArcaPage(link)
        }
      }
    }

  }
}
