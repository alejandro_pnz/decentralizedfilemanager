import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2
import QtQml.Models 2.12

ColumnLayout {
  id: content
  width: parent.width
  height: parent.height
  spacing: isMobile ? ThemeController.style.margin.m16 :
                      ThemeController.style.margin.m8
  readonly property bool isMobile: app.isPhone

  signal infoButtonClicked()

  UIKit.SearchInput {
    id: search
    Layout.topMargin: isMobile ? ThemeController.style.margin.m16 : 0
    Layout.fillWidth: true
    placeholderText: Strings.searchUsers
    Layout.preferredHeight: ThemeController.style.defaultInputHeight
    micIconVisible: false
  }

  Row {
    spacing: ThemeController.style.margin.m8
    Layout.minimumHeight: s(24)
    Layout.maximumHeight: s(24)
    Layout.fillWidth: true
    UIKit.BaseText {
      text: Strings.selectUsersToShare
    }

    UIKit.LargeIconButton {
      icon.source: "qrc:/info-circle.svg"
      width: s(24)
      height: s(24)
      iconWidth: s(24)
      iconHeight: s(24)
      buttonStyle: ThemeController.style.sharePage.infoIconStyle
      onClicked: infoButtonClicked()
    }
  }

  // Selected users
  GridView {
    Layout.topMargin: ThemeController.style.margin.m4
    Layout.fillWidth: true
    Layout.minimumHeight: s(88)

    model: selectedModel

    delegate: UIKit.AvatarDelegate {
      email: emailTxt
    }
  }

  // The user list
  ListView {
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.leftMargin: -ThemeController.style.margin.m16
    Layout.rightMargin: -ThemeController.style.margin.m16
    spacing: ThemeController.style.margin.m12
    clip: true
    section.property: "accOwnerMail"
    section.delegate: Item {
      height: s(64)
      anchors {
        left: parent.left
        right: parent.right
      }
      Rectangle {
        color: ThemeController.style.sharePage.sectionBgColor
        height: ThemeController.style.defaultInputHeight
        anchors {
          top: parent.top
          left: parent.left
          right: parent.right
        }

        RowLayout {
          anchors.fill: parent
          anchors.leftMargin: ThemeController.style.margin.m16
          anchors.rightMargin: ThemeController.style.margin.m16
          spacing: ThemeController.style.margin.m12

          UIKit.BaseText {
            text: Strings.account
            font.weight: Font.DemiBold
            Layout.maximumWidth: s(76)
            elide: Text.ElideRight
            Layout.alignment: Qt.AlignVCenter
          }

          UIKit.BaseText {
//            text: accOwnerMail
            Layout.fillWidth: true
            elide: Text.ElideRight
            Layout.alignment: Qt.AlignVCenter
          }
        }
      }
    }

    model: contactsModel
    delegate: UIKit.ContactCard {
      anchors {
        left: parent.left
        right: parent.right
        leftMargin: ThemeController.style.margin.m16
        rightMargin: ThemeController.style.margin.m16
      }

      title: emailTxt
      role: roleTxt
      onCheckedChanged: {
      }
    }

  }

  ListModel {
    id: contactsModel
  }

  ListModel {
    id: selectedModel
  }

  Component.onCompleted: {
    contactsModel.append({ emailTxt: "bellejk@example.com", roleTxt: "Guest", isChecked: true, accOwnerMail: "johndoe@gmail.com"});
    contactsModel.append({ emailTxt: "ianjk@example.com", roleTxt: "", isChecked: false, accOwnerMail: "johndoe@gmail.com"});
    contactsModel.append({ emailTxt: "johnanders@example.com", roleTxt: "", isChecked: true, accOwnerMail: "johndoe@gmail.com"});
    contactsModel.append({ emailTxt: "lauren.work@example.com", roleTxt: "", isChecked: false, accOwnerMail: "johndoe@gmail.com"});
    contactsModel.append({ emailTxt: "michael.work@example.com", roleTxt: "", isChecked: false, accOwnerMail: "csmith@example.com"});
    contactsModel.append({ emailTxt: "example@example.com", roleTxt: "", isChecked: false,accOwnerMail: "csmith@example.com"});

    selectedModel.append({ emailTxt: "bellejk@example.com", roleTxt: "Guest", isChecked: true});
    selectedModel.append({ emailTxt: "ianjk@example.com", roleTxt: "", isChecked: false});
    selectedModel.append({ emailTxt: "johnanders@example.com", roleTxt: "", isChecked: true});
  }
}
