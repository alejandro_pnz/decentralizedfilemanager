import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

Row {
  id: row

  property alias model: repeater.model

  property Component delegate: defaultDelegate

  property bool bottomIcon: false

  property int maxCount: 0

  property real c: 1.3

  property real _width: s(60)

  property var _lastAvatar

  property bool alternativeColor: true

  property bool showLastAvatar: true

  signal showTooltip(int index, real xPos)
  signal hideTooltip()

  Component {
    id: defaultDelegate

    UIKit.Avatar {
      text: email
      size: UIKit.Avatar.Size.Small
      property bool hovered: false

      MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onHoveredChanged: {
          parent.hovered = !parent.hovered
          if(index >= 0 && parent.hovered){
            showTooltip(index, parent.x)
          }
          else {
            hideTooltip()
          }
        }
      }
    }
  }

  spacing: (_width / c) - _width

  Repeater {
    id: repeater
    delegate: row.delegate
    onItemAdded: function(index, item) {
      row._width = item.width;

      if (index === 0) {
        if (row.bottomIcon) item.bottomIcon = true
      } else if(alternativeColor){
        item.alternativeColor = true
      }

      item.z = repeater.count - index;

      if (row.maxCount === 0) {
        return;
      }

      item.visible = index < row.maxCount;

      if (row.maxCount < repeater.count) {
        if (index === repeater.count - 1 && showLastAvatar) {
          row._lastAvatar = row.delegate.createObject(row);
        }
      }
    }
  }

  onPositioningComplete: {
    if (_lastAvatar) {
      _lastAvatar.x = maxCount * (_lastAvatar.width / c);
      _lastAvatar.text = "+" + String(repeater.count - maxCount);
      _lastAvatar.len = _lastAvatar.text.length;
    }
  }

  UIKit.BaseText {
    text: Strings.plusMore.arg(model.length - maxCount)
    anchors.verticalCenter: parent.verticalCenter
    leftPadding: ThemeController.style.margin.m12
    horizontalAlignment: Qt.AlignHCenter
    visible: !showLastAvatar
  }
}
