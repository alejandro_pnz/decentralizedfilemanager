import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects


Rectangle {
  id: root
  color: ThemeController.style.whiteColor
  border.color: ThemeController.style.textInput.defaultBorderColor
  border.width: s(1)
  radius: s(4)
  implicitHeight: passwordInp.height + ThemeController.style.margin.m12 * 2

  signal passwordChanged()
  property alias text: passwordInp.text

  UIKit.BaseText {
    id: passwordInp
    font.pixelSize: s(16)
    font.family: ThemeController.style.font.codeFont
    font.weight: Font.DemiBold
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.leftMargin: ThemeController.style.margin.m12
    anchors.rightMargin: ThemeController.style.margin.m48
    anchors.topMargin: ThemeController.style.margin.m12
    wrapMode: Text.WrapAnywhere
    color: ThemeController.style.textInput.placeholderTextColor
    onTextChanged: root.passwordChanged(passwordInp.text)
  }
  UIKit.FieldIconButton {
    id: rightIconButton
    width: s(16)
    height: s(16)
    style: ThemeController.style.fieldButtonAlt
    anchors.top: parent.top
    anchors.right: parent.right
    anchors.topMargin: ThemeController.style.margin.m16
    anchors.rightMargin: ThemeController.style.margin.m16
    iconSrc: "qrc:/duplicate-icon.svg"
    onClicked: app.clipboard.copy(passwordInp.text)
    active: passwordInp.text.length > 0
    visible: active
  }
  TextEdit {
    id: copyable
    text: passwordInp.text
    visible: false
  }
}
