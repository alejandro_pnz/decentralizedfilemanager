import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2

ColumnLayout {
  id: content
  height: parent.height
  width: parent.width

  ListView {
    id: listView
    Layout.fillWidth: true
    Layout.preferredHeight: listView.contentHeight
    spacing: ThemeController.style.margin.m12
    clip: true

    model: devicesModel
    delegate: UIKit.DeviceDelegate {
      // Model data
      title: model.deviceModel
      titleIcon: model.powerDevice
      avatar.deviceType: model.deviceType
      avatar.blocked: model.blocked
      avatar.inactive: !model.active
      online: model.online
      contentText: model.deviceOS
      syncing: model.syncing
      outOfSync: model.outOfSync
      lastSeenDate: model.lastSeen
      objectsModel: listView.model

      width: listView.width
      checkboxOnTheLeft: false
      checkable: false
      actionButton: true
      actionButtonIconSource: "qrc:/chevron-forward.svg"
      onClicked: homeManager.openOneDevicePage(model.id)
      onActionButtonClicked: homeManager.openOneDevicePage(model.id)
    }
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }

  SortFilterProxyModel {
    id: devicesModel
    sourceModel: DeviceListModel
    filters: [
      ValueFilter {
        roleName: "email"
        value: deviceEditor.object.email
      }
    ]
  }

}
