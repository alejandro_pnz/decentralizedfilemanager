import QtQuick 2.15
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0
import AppStyles 1.0

import com.testintl 1.0

ToolBar {
  id: header
  height: ThemeController.style.pageHeaderHeight
  background: Item{}

  property int horizontalMargins: ThemeController.style.pageHorizontalMargin
  property string userEmail: AuthenticationManager.currentAccount ? AuthenticationManager.currentAccount.email : ""
  signal switchAccount()
  signal bellButton()
  property bool switchAccountVisible: false

  Rectangle {
    height: s(1)
    color: ThemeController.style.mercuryColor
    anchors {
      top: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }
  }

  RowLayout {
    anchors {
      fill: parent
    }

    Item {
      id: content
      Layout.fillWidth: true
      Layout.fillHeight: true
      RowLayout {
        spacing: ThemeController.style.margin.m12
        width: parent.width
        height: avatar.height
        anchors.verticalCenter: parent.verticalCenter

        UIKit.Avatar {
          id: avatar
          text: userEmail
          size: UIKit.Avatar.Size.Small
        }

        UIKit.BaseText {
          id: userLabel
          text: userEmail
          font.weight: Font.DemiBold
          elide: Text.ElideRight
          Layout.fillWidth: true
        }

        UIKit.LargeIconButton {
          id: iconButton
          icon.source: switchAccountVisible ? "qrc:/chevron-up.svg" : "qrc:/chevron-down.svg"
          onClicked: {
            switchAccount()
          }
        }

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true
        }
        UIKit.LargeIconButton {
          id: rightButton
          icon.source: "qrc:/bell-icon.svg"
          onClicked: homeManager.openPage(Pages.NotificationPopupPage)
        }
      }
    }
  }
}
