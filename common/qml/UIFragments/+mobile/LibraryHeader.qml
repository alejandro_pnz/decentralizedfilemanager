import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import QtQml 2.15

UIKit.PageHeader {
  id: header
  property bool selectionMode: libraryFragment.selectionMode
  property ItemSelectionModel ism: libraryFragment.ism
  property string objectTypeName: libraryFragment.objectTypeName
  property bool selectAllButton: true

  rightButton.icon.source: "qrc:/bell-icon.svg"
  onRightButtonClicked: {
    if (state === "normal")
      homeManager.openPage(Pages.NotificationPopupPage)
    else
      actionMenu.open()
  }
  searchButton.visible: true
  horizontalMargins: 0

  RowLayout {
    visible: header.selectionMode
    anchors.fill: parent
    UIKit.LargeIconButton {
      id: clearSelectionButton
      icon.source: "qrc:/close-gray.svg"
      Layout.alignment: Qt.AlignVCenter
      onClicked: header.ism.clearSelection()
    }
    UIKit.BaseText {
      id: text
      Layout.alignment: Qt.AlignVCenter
      font.weight: Font.Bold
      elide: Text.ElideRight
      color: ThemeController.style.arcaMobileDelegate.titleTextColor
      Binding on text {
        when: header.selectionMode
        value: (header.ism.selectedIndexes.length < header.ism.model.rowCount()
               ? Strings.xObjectsSelected.arg(header.ism.selectedIndexes.length).arg(objectTypeName)
               : Strings.allObjectsSelected.arg(objectTypeName))
               + (selectAllButton ? "  •" : "")
        restoreMode: Binding.RestoreNone
      }
    }
    UIKit.StyledButton {
      type: UIKit.StyledButton.ButtonStyle.Text
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      visible: selectAllButton
      Binding on text {
        when: header.selectionMode
        value: header.ism.selectedIndexes.length < header.ism.model.rowCount()
               ? Strings.selectAll
               : Strings.clearAll
        restoreMode: Binding.RestoreNone
      }
      onClicked: {
        if (header.ism.selectedIndexes.length < header.ism.model.rowCount()) {
          for (var i = 0; i < header.ism.model.rowCount(); ++i)
            header.ism.select(header.ism.model.index(i,0), ItemSelectionModel.Select)
        } else {
          header.ism.clearSelection()
        }
      }
    }
    Item {
      height: s(1)
      Layout.fillWidth: true
    }
  }

  states: [
    State {
      name: "selection"
      when: header.selectionMode
      PropertyChanges {
        target: header
        title: ""
        rightButton.icon.source: "qrc:/more-2.svg"
        searchButton.visible: false
      }

    },
    State {
      name: "normal"
      when: !header.selectionMode
      PropertyChanges {
        target: header
        title: root.title
        rightButton.icon.source: "qrc:/bell-icon.svg"
        searchButton.visible: true
      }
    }
  ]
}
