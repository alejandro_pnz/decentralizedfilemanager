import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import QtQml 2.15

UIKit.StyledDrawer {
  id: popup

  property ItemSelectionModel ism: null
  property string objectTypeName: ""

  contentItemHeight: parent.height * 0.33
  drawerFullHeight: parent.height
  headerCloseButtonVisible: fullHeight
  headerVisible: true

  property alias model: listView.model
  property alias allButton: allButton

  header: Item {
    Layout.fillWidth: true
    Layout.fillHeight: true
    RowLayout {
      anchors.fill: parent
      UIKit.BaseText {
        id: text
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignVCenter
        font.weight: Font.Bold
        elide: Text.ElideRight
        color: ThemeController.style.arcaMobileDelegate.titleTextColor

        Binding on text {
          when: popup.visible
          value: ism.selectedIndexes.length < ism.model.rowCount()
                 ? Strings.xObjectsSelected.arg(ism.selectedIndexes.length).arg(objectTypeName)
                 : Strings.allObjectsSelected.arg(objectTypeName)
          restoreMode: Binding.RestoreNone
        }
      }

      UIKit.StyledButton {
        id: allButton
        type: UIKit.StyledButton.ButtonStyle.Text
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        visible: true
        Binding on text {
          when: popup.ism
          value: popup.ism.selectedIndexes.length < popup.ism.model.rowCount()
                 ? Strings.selectAll
                 : Strings.clearAll
        }
        onClicked: {
          if (popup.ism.selectedIndexes.length < popup.ism.model.rowCount()) {
            for (var i = 0; i < popup.ism.model.rowCount(); ++i)
              popup.ism.select(popup.ism.model.index(i,0), ItemSelectionModel.Select)
          } else {
            popup.ism.clearSelection()
            popup.fullHeight = false
            popup.close()
          }
        }
      }
    }
  }

  ListView {
    id: listView
    height: parent.height
    width: parent.width
    clip: true
    delegate: UIKit.ActionListDelegate {
      leftPadding: 0
      rightPadding: 0
      width: parent.width
      text: name
      icon.source: iconSource
      onClicked: popup.closeDrawer()
      display: AbstractButton.TextBesideIcon
      separator: true
      action: actionSource
    }
  }
}
