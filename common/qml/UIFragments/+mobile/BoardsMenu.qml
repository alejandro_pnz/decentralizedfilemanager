import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

UIKit.StyledDrawer {
  id: drawer
  drawerFullHeight: parent.height
  headerCloseButtonVisible: fullHeight
  contentItemHeight: boardsList.height + s(24)
  headerVisible: true

  header: UIKit.BaseText {
    text: Strings.boards
    font.weight: Font.Bold
    Layout.fillWidth: true
  }

  BoardsList {
    id: boardsList
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
    }
  }
}
