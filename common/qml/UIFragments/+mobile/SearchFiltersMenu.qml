import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import QtQuick.Layouts 1.12
import UIFragments 1.0
import QtQml.Models 2.12
import UIModel 1.0 as UIModel

UIKit.StyledDrawer {
  id: popup

  contentItemHeight: parent.height * 0.33
  drawerFullHeight: parent.height
  headerCloseButtonVisible: fullHeight
  headerVisible: true

  property int selectedFilters: searchModel ? searchModel.filters.appliedFiltersModel.count : 0
  property var searchModel

  header: Item {
    id: header
    Layout.fillWidth: true
    Layout.fillHeight: true
    RowLayout {
      anchors.fill: parent

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
        visible: fullHeight
      }

      UIKit.BaseText {
        id: text
        text: fullHeight && selectedFilters > 0 ? Strings.filters + " \u2022 " + selectedFilters : Strings.filters
        Layout.alignment: Qt.AlignHCenter
        font.weight: Font.Bold
        elide: Text.ElideRight
        color: ThemeController.style.arcaMobileDelegate.titleTextColor
      }

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
      }

      UIKit.StyledButton {
        type: UIKit.StyledButton.ButtonStyle.Text
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        text: Strings.clearAll
        visible: true
        onClicked: {
          searchModel.filters.removeAll()
          popup.close()
        }
      }
    }

    DragHandler {
      id: handler
      onActiveChanged: {
        console.log("active changed", handler.active)
        if (handler.active) {
          if (popup.fullHeight) {
            popup.closeDrawer()
          } else {
            popup.fullHeight = true
          }
        }
      }
      target: popup.headerContent
    }
  }

  Flickable {
    width: parent.width
    height: parent.height
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: content.height
    clip: true
    ColumnLayout {
      id: content
      spacing: 16
      width: parent.width

      QuickFilters {
        Layout.fillWidth: true
        searchModel: popup.searchModel
        onActionTaken: popup.closeDrawer()
        hideChecked: true
        applyImmediately: true
      }

      UIKit.BaseText {
        Layout.topMargin: 8
        font.weight: Font.DemiBold
        elide: Text.ElideRight
        color: ThemeController.style.charadeColor
        font.capitalization: Font.AllUppercase
        text: Strings.moreFilters
      }

      ListView {
        id: listView
        Layout.preferredHeight: contentHeight
        Layout.fillWidth: true
        model: filtersModel.model
        interactive: false
        delegate: UIKit.ActionListDelegate {
          leftPadding: 0
          rightPadding: 0
          width: parent.width
          text: name
          icon.source: iconSource
          display: AbstractButton.TextBesideIcon
          separator: true
          actionIcon: true
          action: actionSource
          onClicked: popup.closeDrawer()
        }
      }

      Item {
        Layout.preferredHeight: 1
        Layout.fillWidth: true
      }
    }
  }

  UIModel.FiltersModel {
    id: filtersModel
    searchModel: popup.searchModel
  }
}
