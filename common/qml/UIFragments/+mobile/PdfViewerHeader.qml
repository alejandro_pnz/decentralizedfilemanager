import QtQuick 2.15
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0
import AppStyles 1.0

import com.testintl 1.0

ToolBar {
  id: header
  height: ThemeController.style.pageHeaderHeight
  background: Item{}

  property int horizontalMargins: ThemeController.style.pageHorizontalMargin
  property bool searchMode: false
  signal moreClicked()
  signal backClicked()
  signal doneClicked()

  Rectangle {
    height: s(1)
    color: ThemeController.style.mercuryColor
    anchors {
      top: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }
  }

  RowLayout {
    anchors {
      fill: parent
    }

    spacing: ThemeController.style.margin.m16

    UIKit.LargeIconButton {
      id: leftButton
      icon.source: "qrc:/chevron-back.svg"
      visible: !searchMode
      onClicked: backClicked()
    }

    Item {
      id: content
      Layout.fillWidth: true
      Layout.fillHeight: true


      RowLayout {
        spacing: ThemeController.style.margin.m12
        width: parent.width
        height: parent.height
        anchors.verticalCenter: parent.verticalCenter

        UIKit.BaseText {
          id: text
          Layout.alignment: Qt.AlignVCenter
          font.weight: Font.Bold
          text: fragment.fileName
          visible: !searchMode
        }

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true
          visible: !searchMode
        }

        UIKit.FileSearchInput {
          id: search
          Layout.fillWidth: true
          visible: searchMode
        }

        UIKit.StyledButton {
          Layout.alignment: Qt.AlignVCenter
          text: Strings.done
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          type: UIKit.StyledButton.ButtonStyle.Text
          buttonStyle: ThemeController.style.clearTextButtonStyle
          onClicked: doneClicked()
          visible: searchMode
        }
      }
    }

    UIKit.LargeIconButton {
      id: rightButton
      icon.source: "qrc:/more-2.svg"
      visible: !searchMode
      onClicked: moreClicked()
    }
  }
}
