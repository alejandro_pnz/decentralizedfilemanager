import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  spacing: ThemeController.style.margin.m12

  signal accept()

  UIKit.BaseText {
    id: title
    text: Strings.sharing
    size: UIKit.BaseText.TextSize.H3
    font.weight: Font.Bold
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.BaseText {
    text: Strings.shareInfoText1
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.BaseText {
    text: Strings.shareInfoText2
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.StyledButton {
    id: confirmButton
    text: Strings.gotIt
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Primary
    onClicked: accept()
    Layout.fillWidth: true
    Layout.topMargin: ThemeController.style.margin.m20
  }
}

