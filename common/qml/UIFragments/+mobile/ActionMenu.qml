import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import QtQuick.Layouts 1.12
import com.testintl 1.0

UIKit.StyledDrawer {
  id: popup

  contentItemHeight: shortModel ? listView.contentHeight : parent.height * 0.33
  fullHeightSwitcher: !shortModel
  drawerFullHeight: parent.height
  headerCloseButtonVisible: fullHeight
  headerVisible: true

  property bool shortModel: listView.contentHeight < parent.height * 0.33
  property alias headerText: text

  property alias model: listView.model

  // Object item
  property var currentItem
  property var customItemDataProvider: null

  onCurrentItemChanged: {
    // Update object name and icon
    if (currentItem) {
      console.log("CURRENT ITEM", currentItem, customItemDataProvider)
      if (customItemDataProvider) {
        var a = customItemDataProvider(currentItem)
        console.log("Using custom provider", a)
        headerIcon.source = a[0]
        description.text = a[1]
        text.text = a[2]
      } else if (currentItem.objectType === SyncableObject.Arca) {
        headerIcon.source = Utility.arcaIcon(currentItem.color)
        description.text = Utility.formatArcaContent(currentItem.quodCount, currentItem.filesCount)
        text.text = currentItem.name
      } else if (currentItem.objectType === SyncableObject.Quod) {
        headerIcon.source = Utility.quodIcon(currentItem.id)
        description.text = Utility.formatQuodContent(currentItem.id)
        text.text = currentItem.name
      } else if (currentItem.objectType === SyncableObject.File) {
        headerIcon.source = Utility.fileIcon(currentItem.fileType)
        description.text = Utility.formatFileContent(currentItem.id)
        text.text = currentItem.name
      } else if (currentItem.objectType === SyncableObject.Device) {
        headerIcon.source = currentItem.powerDevice ? "qrc:/power.svg" : ""
        description.text = currentItem.deviceOS
        text.text = currentItem.deviceModel // TODO email
      }
    }
  }

  header: Item {
    Layout.fillWidth: true
    Layout.fillHeight: true
    RowLayout {
      anchors.fill: parent
      UIKit.SvgImage {
        id: headerIcon
        Layout.alignment: Qt.AlignVCenter
        width: s(24)
        height: s(24)
        visible: source.toString() !== ""
      }

      UIKit.BaseText {
        id: text
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignVCenter
        font.weight: Font.Bold
        elide: Text.ElideRight
        color: ThemeController.style.arcaMobileDelegate.titleTextColor
      }

      UIKit.BaseText {
        id: description
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignVCenter
        size: UIKit.BaseText.TextSize.Small
        elide: Text.ElideRight
        horizontalAlignment: Text.AlignRight
        color: ThemeController.style.arcaMobileDelegate.textColor
      }
    }
  }

  ListView {
    id: listView
    height: parent.height
    width: parent.width
    model: sortModel
    clip: true
    delegate: UIKit.ActionListDelegate {
      leftPadding: 0
      rightPadding: 0
      width: listView.width
      text: name
      icon.source: iconSource
      display: AbstractButton.TextBesideIcon
      separator: true
      onClicked: popup.closeDrawer()
      action: actionSource
    }
  }
}
