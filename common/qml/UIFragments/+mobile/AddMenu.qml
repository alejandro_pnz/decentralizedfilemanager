import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

UIKit.StyledDrawer {
  id: drawer
  drawerFullHeight: parent.height
  headerCloseButtonVisible: fullHeight

  property bool hasItems: QuodListModel.count > 0 || FileListModel.count > 0
  states: [
    State {
      when: !hasItems
      name: "emptyLibrary"
      PropertyChanges {
        target: homeOnboarding
        visible: true
      }
      PropertyChanges {
        target: addList
        visible: false
      }
      PropertyChanges {
        target: drawer
        contentItemHeight: 0.88 * parent.height
        headerVisible: false
        closeButtonVisible: true
      }
    },
    State {
      when: hasItems
      name: "normal"
      PropertyChanges {
        target: homeOnboarding
        visible: false
      }
      PropertyChanges {
        target: addList
        visible: true
      }
      PropertyChanges {
        target: drawer
        contentItemHeight: parent.height * 0.33
        headerVisible: true
        closeButtonVisible: false
      }
    }
  ]

  header: UIKit.BaseText {
    text: Strings.add
    font.weight: Font.Bold
    Layout.fillWidth: true
  }

  HomeOnboarding{
    id: homeOnboarding
    anchors {
      top: parent.top
      topMargin: s(56)
      left: parent.left
      right: parent.right
      leftMargin: ThemeController.style.pageHorizontalMargin
      rightMargin: ThemeController.style.pageHorizontalMargin
    }
  }

  AddList {
    id: addList
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
    }

    ButtonGroup {
      id: buttonGroup
      buttons: addList.columnContent.children
      onClicked: drawer.close()
    }
  }
}
