import QtQuick 2.15
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0
import AppStyles 1.0

ToolBar {
  id: header
  height: ThemeController.style.pageHeaderHeight
  background: Item{}

  property var searchModel
  property alias filterButton: filterButton
  property alias backButton: backButton
  property alias searchInput: search
  property int horizontalMargins: ThemeController.style.pageHorizontalMargin

  signal rightButtonClicked()
  signal filterClicked()

  Rectangle {
    height: s(1)
    width: parent.width
    color: ThemeController.style.mercuryColor
    anchors {
      top: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }
  }

  RowLayout {
    anchors {
      fill: parent
    }

    UIKit.LargeIconButton {
      id: backButton
      icon.source: "qrc:/chevron-back.svg"
      height: s(32)
      width: s(32)
      onClicked: homeManager.back()
    }

    UIKit.SearchInput {
      id: search
      Layout.fillWidth: true
      placeholderText: Strings.searchQuodArca
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
      micIconVisible: false
      onSearchTextChanged: searchModel.search(searchText)
    }

    UIKit.FilterButton {
      id: filterButton
      height: s(32)
      width: s(32)
      Layout.leftMargin: ThemeController.style.margin.m12
      onClicked: filterClicked()
      indicatorVisible: searchModel && searchModel.filters.appliedFiltersModel.count > 0
    }
  }
}
