import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12

UIKit.PageHeader{
  id: header
  property string selectText: "1 Arca Selected"
  rightButton.visible: false
  RowLayout {
    anchors.fill: parent
    UIKit.LargeIconButton {
      id: clearSelectionButton
      icon.source: "qrc:/close-gray.svg"
      Layout.alignment: Qt.AlignVCenter
    }
    UIKit.BaseText {
      id: text
      Layout.alignment: Qt.AlignVCenter
      Layout.fillWidth: true
      font.weight: Font.Bold
      elide: Text.ElideRight
      color: ThemeController.style.arcaMobileDelegate.titleTextColor
      text: selectText + " • " + Strings.share
    }
    UIKit.StyledButton {
      Layout.alignment: Qt.AlignRight
      type: UIKit.StyledButton.ButtonStyle.Text
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      text: Strings.done
      onClicked: {

      }
    }
  }
}
