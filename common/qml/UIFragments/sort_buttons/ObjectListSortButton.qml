import QtQuick 2.15
import UIModel 1.0
import com.test 1.0

ListSortButton {
  id: root
  sortModel: menuModel.model
  sortModelComponent: menuModel
  property alias objectType: menuModel.objectType
  SortMenuModel {
    id: menuModel
    Component.onCompleted: root.init(Enums.LastUsed)
  }
}
