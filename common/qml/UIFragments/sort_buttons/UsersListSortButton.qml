import QtQuick 2.0
import UIModel 1.0

ListSortButton {
  id: root
  sortModel: menuModel.model
  UsersSortModel {
    id: menuModel
    Component.onCompleted: root.init()
  }
}
