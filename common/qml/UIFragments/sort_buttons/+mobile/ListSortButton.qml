import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import QtQuick.Window 2.12


Item {
  id: root
  property int headerBottomMargin: ThemeController.style.margin.m12
  height: s(40) + headerBottomMargin
  implicitWidth: button.width
  property alias sorter: sortMenu.currentSorter
  property alias sortModel: sortMenu.sortModel
  property string text: Strings.lastOpened
  signal clicked()

  // TODO: temporary fix
  property var sortModelComponent: undefined

  function init() {
    sortMenu.init()
  }

  UIKit.DropDownButton {
    id: button
    text: sortMenu.currentSorterName
    anchors.top: parent.top
    onClicked: {
      sortMenu.open()
    }
  }


  UIKit.SortListDrawer {
    id: sortMenu
  }
}
