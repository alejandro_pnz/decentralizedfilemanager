import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import UIModel 1.0

Item {
  id: root
  property int headerBottomMargin: ThemeController.style.margin.m12
  implicitHeight: s(40) + headerBottomMargin
  implicitWidth: button.width
  property alias sorter: sortMenu.currentSorter

  property string text: Strings.lastOpened
  property alias sortModel: sortMenu.sortModel
  property alias sortModelComponent: sortMenu.sortModelComponent
  signal clicked()

  function init(sorter) {
    sortMenu.init(sorter)
  }

  UIKit.DropDownButton {
    id: button
    text: sortMenu.currentSorterName
    anchors.top: parent.top
    onClicked: {
      if (sortMenu.opened)
        sortMenu.close()
      else
        sortMenu.open()
    }
  }

  UIKit.SortListMenu {
    id: sortMenu
    x: parent.width
    y: -(sortMenu.implicitHeight * 0.5) + button.height * 0.5

    function position() {
      var p = button.mapToItem(null, button.width, button.height * 0.5)

      if (!ApplicationWindow.window) return

      if (ApplicationWindow.window.height - p.y > sortMenu.implicitHeight * 0.5) {
          // there is enough space to show popup centered to the button
          sortMenu.y = -(sortMenu.implicitHeight * 0.5) + button.height * 0.5
      } else {
        sortMenu.y =  -sortMenu.implicitHeight + button.height * 0.5 + ApplicationWindow.window.height - p.y - s(10)
      }
    }

    onHeightChanged: position()
    onAboutToShow: position()
  }
}
