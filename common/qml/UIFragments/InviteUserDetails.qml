import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

Flickable {
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: layout.height
  clip: true

  property bool userJoinedQA: false

  ScrollBar.vertical: ScrollBar {
    visible: !app.isPhone && parent.contentHeight > parent.height

    contentItem: Rectangle {
      implicitWidth: s(8)
      anchors.right: parent.right
      anchors.rightMargin: ThemeController.style.margin.m12
      radius: s(100)
      color: ThemeController.style.seashellColor
    }
  }

  ColumnLayout {
    id: layout
    spacing: ThemeController.style.margin.m12
    width: parent.width
    anchors{
      left: parent.left
      right: parent.right
      leftMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
      rightMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
    }

    UIKit.BaseText {
      text: userJoinedQA ? Strings.userIsMember : Strings.userNotJoined
      font.weight: Font.Bold
      size: UIKit.BaseText.TextSize.H3
      Layout.alignment: Qt.AlignHCenter
      horizontalAlignment: Text.AlignHCenter
      Layout.fillWidth: true
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }

    UIKit.BaseText {
      Layout.fillWidth: true
      text: userJoinedQA ? Strings.userWillBeAdded : Strings.emailNotAssociated
      horizontalAlignment: Text.AlignHCenter
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      Layout.alignment: Qt.AlignHCenter
    }

    UIKit.AccountDelegate {
      accountName: "nia.gutkowski@example.com"
      Layout.fillWidth: true
      Layout.leftMargin: app.isPhone ? -ThemeController.style.margin.m16 : -ThemeController.style.margin.m24
      Layout.rightMargin: app.isPhone ? -ThemeController.style.margin.m16 : -ThemeController.style.margin.m24
      visible: userJoinedQA
    }

    UIKit.ContactCard {
      Layout.fillWidth: true
      Layout.preferredHeight: s(84)
      Layout.topMargin: ThemeController.style.margin.m4
      title: "glenz@example.com"
      stateVisible: false
      checkable: false
      checkBoxMode: false
      labelBottom.visible: false
      labelLeft.visible: false
      states: []
      rightCheckbox.visible: false
      leftCheckbox.visible: false
      iconObject.visible: false
      bg.border.width: s(1)
      bg.border.color: ThemeController.style.whisperColor
    }

    Rectangle {
      Layout.topMargin: ThemeController.style.margin.m4
      Layout.fillWidth: true
      color: ThemeController.style.mercuryColor
      Layout.preferredHeight: s(1)
    }

    UIKit.AttributesPickerDelegate {
      id: formatInput
      labelText: Strings.requestToInviteAs
      inputRequired: false
      pickerTitle: Strings.inviteAs
      pickerModel: objectsModel
      visible: !userJoinedQA
    }

    RowLayout {
      spacing: ThemeController.style.margin.m8
      UIKit.BaseText {
        text: Strings.setUserSyncStatus
        font.weight: Font.DemiBold
      }

      UIKit.BigTipButton {
      }

      visible: userJoinedQA || formatInput.comboBox.displayText === "Guest"
    }

    ColumnLayout {
      Layout.fillWidth: true
      visible: userJoinedQA || formatInput.comboBox.displayText === "Guest"

      UIKit.CardDelegate {
        id: firstButton
        Layout.topMargin: ThemeController.style.margin.m4
        Layout.fillWidth: true
        title: Strings.privateStr
        text: Strings.onlyOnPersonalList
        icon.source: "qrc:/private.svg"
        checkable: true
        checked: true
        ButtonGroup.group: syncButtonsGroup
      }

      UIKit.CardDelegate {
        id: middleButton
        Layout.fillWidth: true
        title: Strings.publicStr
        text: Strings.userVisibleToEveryone
        icon.source: "qrc:/public-dark.svg"
        checkable: true
        ButtonGroup.group: syncButtonsGroup
      }

      UIKit.CardDelegate {
        id: thirdButton
        Layout.fillWidth: true
        Layout.bottomMargin: ThemeController.style.margin.m24
        title: Strings.noSync
        text: Strings.onlyOnThisDevice
        icon.source: "qrc:/no-sync-dark.svg"
        checkable: true
        ButtonGroup.group: syncButtonsGroup
      }
    }

    ButtonGroup {
      id: syncButtonsGroup
      exclusive: true
    }
  }

  ListModel {
    id: objectsModel
  }

  Component.onCompleted: {
    objectsModel.append({ modelData: Strings.member, contentText: Strings.membersHaveAccess});
    objectsModel.append({ modelData: Strings.guest, contentText: Strings.guestsAccess});
  }
}
