import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import UIFragments 1.0 as UIFragments
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2
import QtQml.Models 2.12
import UIModel 1.0

ColumnLayout {
  spacing: ThemeController.style.homePageVerticalMargin
  width: parent.width
  height: parent.height
  property bool selectionMode: ism.hasSelection
  property ItemSelectionModel ism: selectionModel
  property string objectTypeName: Strings.devices
  property int objectType: Enums.Device
  property bool showPending: true
  property string emailFilter
  property bool emailFilterInverted: false

  DevicesSortModel {
    id: sortedProxyModel
    sourceModel: DeviceListModel
    sorter: listView.headerItem.sorter
    searchPattern: search.searchText
  }

  ItemSelectionModel {
    id: selectionModel
    model: devicesModel
  }

  SortFilterProxyModel {
    id: pendingDevicesModel
    sourceModel: DeviceListModel
    filters: ValueFilter {
      roleName: "pending"
      value: true
    }
  }

  SortFilterProxyModel {
    id: devicesModel
    sourceModel: DeviceListModel
    filters: [
      ValueFilter {
        roleName: "pending"
        value: false
      },
      ValueFilter {
        roleName: "email"
        value: emailFilter
        inverted: emailFilterInverted
      }
    ]
  }

  SortFilterProxyModel {
    id: pinnedModel
    sourceModel: devicesModel
    filters: ValueFilter {
      roleName: "pinned"
      value: true
    }
  }

  SortFilterProxyModel {
    id: objectsProxyModel
    sourceModel: devicesModel
    filters: ValueFilter {
      roleName: "pinned"
      value: false
    }
  }

  UIKit.SearchInput {
    id: search
    Layout.fillWidth: true
    placeholderText: Strings.searchDevices
    micIconVisible: false
    Layout.preferredHeight: ThemeController.style.defaultInputHeight
  }

  UIKit.DevicesPendingDelegate {
    id: pendingRequestsDelegate
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m8 : 0
    Layout.fillWidth: true
    mode: UIKit.DevicesPendingDelegate.Mode.Devices
    objectsModel: pendingDevicesModel
    requestsCount: pendingDevicesModel.count
    visible: pendingDevicesModel.count > 0 && showPending
    onButtonClicked: homeManager.openPage(Pages.PendingRequestsPage)
  }

  Flickable {
    clip: true
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m8 : 0
    contentHeight: column.height

    Column {
      id: column
      anchors.left: parent.left
      anchors.right: parent.right
      spacing: ThemeController.style.margin.m16
      bottomPadding: ThemeController.style.margin.m16

      UIKit.GenericListView {
        id: pinnedObjectList
        anchors.left: parent.left
        anchors.right: parent.right
        height: contentHeight
        clip: true
        spacing: app.isPhone ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
        visible: pinnedObjectList.count > 0

        header: PinHeader { }

        footer: ListFooter { }

        model: pinnedModel
        delegate: UIKit.DeviceDelegate {
          // Model data
          title: model.deviceModel
          titleIcon: model.powerDevice
          avatar.deviceType: model.deviceType
          avatar.blocked: model.blocked
          avatar.inactive: !model.active
          online: model.online
          contentText: model.deviceOS
          syncing: model.syncing
          outOfSync: model.outOfSync
          lastSeenDate: model.lastSeen
          objectsModel: pinnedObjectList.model

          width: pinnedObjectList.width
          actionButton: !ism.hasSelection
          checkboxOnTheLeft: !isMobile
          checkable: !isMobile || pinnedObjectList.selectionMode
          selectionModel: ism
          onPressAndHold: {
            if (isMobile)
              pinnedObjectList.selectionMode = !pinnedObjectList.selectionMode
          }

          onActionButtonClicked: {
            pinnedObjectList.currentIndex = index
            actionMenu.model.currentObject = object
            actionMenu.handleActionButton(pinnedObjectList)
          }
        }
      }

      UIKit.GenericListView {
        id: listView
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: ThemeController.style.margin.m8
        height: contentHeight
        clip: true
        spacing: app.isPhone ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
        header: UIFragments.DevicesListSortButton {}
        visible: listView.count > 0

        model: objectsProxyModel
        delegate: UIKit.DeviceDelegate {
          // Model data
          title: model.deviceModel
          titleIcon: model.powerDevice
          avatar.deviceType: model.deviceType
          avatar.blocked: model.blocked
          avatar.inactive: !model.active
          online: model.online
          contentText: model.deviceOS
          syncing: model.syncing
          outOfSync: model.outOfSync
          lastSeenDate: model.lastSeen

          actionButton: !ism.hasSelection
          width: listView.width
          objectsModel: listView.model

          checkboxOnTheLeft: !isMobile
          checkable: !isMobile || listView.selectionMode
          selectionMode: listView.selectionMode
          selectionModel: ism

          onDelegateClicked: {
            homeManager.openOneDevicePage(model.object.id)
          }

          onActionButtonClicked: {
            listView.currentIndex = index
            actionMenu.model.currentObject = model.object
            actionMenu.handleActionButton(listView)
          }
        }
      }
    }
  }

  function selectCurrent() {
    if (actionMenu.popup.opened && actionMenu.popup.currentItem) {
      actionMenu.popup.currentItem.check()
    } else {
      listView.itemAtIndex(0).check()
    }
  }

  OneDeviceActionMenu {
    id: actionMenu
    //givePowerPopup: fragment.givePowerPopup
    Connections {
      target: actionMenu.model
      function onSelectCurrent() {
        fragment.selectCurrent()
      }
    }
  }
}
