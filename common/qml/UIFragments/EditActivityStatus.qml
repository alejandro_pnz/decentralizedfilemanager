import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: content
  spacing: ThemeController.style.margin.m12
  width: parent.width
  height: parent.height

  function save() {
    if (activeBtn.checked) {
      deviceEditor.activate()
    } else if (deactivateButton.checked) {
      deviceEditor.deactivate()
    } else {
      deviceEditor.block()
    }
  }

  UIKit.CardDelegate {
    id: activeBtn
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    title: Strings.active
    text: Strings.allowToLogin
    icon.source: "qrc:/check-circle.svg"
    actionButton: false
    textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    textComponent.elide: Text.ElideNone
    avatar.iconSize: s(24)
    avatar.pixelSize: s(48)
    topPadding: s(18)
    bottomPadding: s(18)
    checkable: true
    checked: deviceEditor.object.active
    ButtonGroup.group: buttonsGroup
  }

  UIKit.CardDelegate {
    id: deactivateButton
    Layout.fillWidth: true
    title: Strings.inactive
    text: Strings.cannotLoginWithThisDevice
    icon.source: "qrc:/delete-dark.svg"
    actionButton: false
    textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    textComponent.elide: Text.ElideNone
    avatar.iconSize: s(24)
    avatar.pixelSize: s(48)
    topPadding: s(18)
    bottomPadding: s(18)
    checkable: true
    checked: !deviceEditor.object.active && !deviceEditor.object.blocked
    ButtonGroup.group: buttonsGroup
  }

  UIKit.CardDelegate {
    Layout.fillWidth: true
    title: Strings.blocked
    text: Strings.logsOutAllOtherDevices
    icon.source: "qrc:/remove-circle.svg"
    actionButton: false
    textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    textComponent.elide: Text.ElideNone
    avatar.iconSize: s(24)
    avatar.pixelSize: s(48)
    topPadding: s(18)
    bottomPadding: s(18)
    checkable: true
    checked: deviceEditor.object.blocked
    ButtonGroup.group: buttonsGroup
  }

  ButtonGroup {
    id: buttonsGroup
    exclusive: true
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
