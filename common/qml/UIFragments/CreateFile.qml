import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

Flickable {
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true
  property string noteContent: ""

  function validate() {
    if (validateMandatoryField(nameInput)) {
      return true
    }
    return false
  }

  function save() {
    // TODO: ?
  }

  ColumnLayout {
    id: content
    width: parent.width
    spacing: ThemeController.style.margin.m16

    UIKit.InputField {
      id: nameInput
      Layout.topMargin: ThemeController.style.homePageVerticalMargin
      labelText: Strings.name
      placeholderTxt: Strings.typeName
      inputRequired: true
      textField.text: fileEditor.name
      onTextChanged: fileEditor.name = text
    }

    UIKit.NoteDelegate {
      id: noteDelegate
      Layout.topMargin: ThemeController.style.margin.m4
      Layout.fillWidth: true
      onOpenNotes: homeManager.editNote()
      noteContent: fileEditor.notes
    }

    UIKit.LockObjectPanel {
      id: lockObjectPanel
      Layout.topMargin: app.isPhone ? 0 : s(8)
      titleText: Strings.lockFile
      Layout.fillWidth: true
    }
  }


  Component.onCompleted: {
    fileEditor.create();
  }
}
