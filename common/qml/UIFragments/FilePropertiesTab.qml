import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

Flickable {
  id: flickable
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true
  readonly property bool isMobile: app.isPhone
  //properties for tests
  property string uid: "42869254"
  property string lastModifiedDate: Qt.formatDateTime(currentEditor.object.modified, "d MMMM yyyy, h:mm") //"14 April 2021, 17:36"
  property string ownerName: "james@workemail.com"

  property bool generalDetailsEditMode: false

  readonly property var currentEditor: homeManager.currentEditor

  ColumnLayout {
    id: content
    spacing: 16
    width: parent.width

    RowLayout {
      UIKit.BaseText {
        font.weight: Font.Bold
        text: Strings.generalDetails
        Layout.alignment: Qt.AlignLeft
      }

      Item {
        Layout.preferredHeight: 1
        Layout.fillWidth: true
      }

      UIKit.StyledButton {
        text: generalDetailsEditMode ? Strings.cancel : Strings.edit
        type: UIKit.StyledButton.ButtonStyle.Text
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        onClicked: {
          if (isMobile) {
            homeManager.enterEditGeneralDetailsPage({ownerName:flickable.ownerName})
          } else {
            if (generalDetailsEditMode) {
              nameInput.textField.text = fileEditor.object.name
            }
            generalDetailsEditMode = !generalDetailsEditMode
          }
        }
      }
    }

    GridLayout {
      id: inputsLayout
      columns: isMobile ? s(1) : s(2)

      Layout.fillWidth: true
      columnSpacing: s(32)
      rowSpacing: s(16)

      UIKit.InputField {
        id: nameInput
        labelText: Strings.fileName
        placeholderTxt: fileEditor.object.name
        textField.text: generalDetailsEditMode ? fileEditor.object.name : ""
        inputRequired: generalDetailsEditMode
        clearIconVisible: false
        rightIconSource: generalDetailsEditMode ? "qrc:/close-icon.svg" : "qrc:/duplicate-icon.svg"
        rightIconBtn.iconColor: generalDetailsEditMode ? "" : ThemeController.style.slatePurpleColor
        implicitWidth: 394
        Layout.fillWidth: true
        rightIconBtn.states: []
        enabled: generalDetailsEditMode
      }

      UIKit.InputField {
        id: ownerInputDesktop
        labelText: Strings.owner
        placeholderTxt: ownerName
        textField.text: generalDetailsEditMode ? ownerName : ""
        inputRequired: false
        implicitWidth: s(394)
        Layout.fillWidth: true
        clearIconVisible: false
        enabled: generalDetailsEditMode
        textField.readOnly: true
        rightIconSource: generalDetailsEditMode ? "qrc:/chevron-down.svg" : "qrc:/duplicate-icon.svg"
        rightIconBtn.iconColor: generalDetailsEditMode ? "" : ThemeController.style.slatePurpleColor
        rightIconBtn.states: []

        textField.onFocusChanged: {
          if(textField.focus){
            usersList.open()
          }
        }
      }
    }

    UIKit.StyledButton {
      Layout.topMargin: s(8)
      text: Strings.done
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      onClicked: {
        fileEditor.object.name = nameInput.textField.text
        generalDetailsEditMode = false
      }
      visible: !isMobile && generalDetailsEditMode
    }

    UIKit.NoteDelegate {
      Layout.fillWidth: true
      noteContent: fileEditor.notes
      onOpenNotes: homeManager.editNote()
    }

    RowLayout {
      Layout.topMargin: isMobile ? 0 : s(8)
      UIKit.BaseText {
        font.weight: Font.Bold
        text: Strings.tags
        Layout.alignment: Qt.AlignLeft
      }

      Item {
        Layout.preferredHeight: s(1)
        Layout.fillWidth: true
      }

      UIKit.StyledButton {
        text: Strings.edit
        type: UIKit.StyledButton.ButtonStyle.Text
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        Layout.alignment: Qt.AlignRight
        onClicked: homeManager.openPage(Pages.AddTagsPage)
      }
    }

    ListView {
      id: listView
      Layout.fillWidth: true
      Layout.preferredHeight: 32
      spacing: 12
      orientation: ListView.Horizontal

      model: currentEditor.taglistModel
      delegate: UIKit.Tag {
        text: name
        tagColor: color
      }
    }

    Rectangle {
      Layout.topMargin: isMobile ? 0 : s(8)
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
    }

    SyncPanel {
      id: syncDelegate
      Layout.fillWidth: true
      syncStatus: fileEditor.object.syncProperty
      // hasWhitelist: true TODO
    }

    Rectangle {
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
      visible: isMobile
    }

    UIKit.ObjectInformationPanel {
      uid: flickable.uid
      lastModifiedDate: flickable.lastModifiedDate
      ownerName: flickable.ownerName
      visible: isMobile
      Layout.alignment: Qt.AlignHCenter
    }

    Item {
      Layout.fillWidth: true
      Layout.preferredHeight: isMobile ? s(1) : s(24)
    }
  }

  UIKit.UsersListPopup {
    id: usersList
    y: parent.height - usersList.height + s(81) - s(40)
    x: ownerInputDesktop.x

    onChangedOwner: {
      ownerName = owner
      usersList.close()
    }

    onVisibleChanged: {
      if(!visible)
        ownerInputDesktop.textField.focus = false
    }
  }
}
