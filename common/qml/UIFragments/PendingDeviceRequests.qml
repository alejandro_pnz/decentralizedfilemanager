import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import QtQml.Models 2.12
import UIModel 1.0
import SortFilterProxyModel 0.2

ColumnLayout {
  id: content
  width: parent.width
  height: parent.height

  property bool selectionMode: ism.hasSelection
  property ItemSelectionModel ism: selectionModel
  property string objectTypeName: Strings.devices
  property int objectType: Enums.Request

  ItemSelectionModel {
    id: selectionModel
    model: PendingDeviceListModel
  }

  ListView {
    id: listView
    Layout.fillWidth: true
    Layout.topMargin: isMobile ? s(21) : ThemeController.style.margin.m24
    Layout.fillHeight: true
    spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
    interactive: true
    clip: true

    model: PendingDeviceListModel

    delegate: Component {
      Loader {
        id: loader
        sourceComponent: isMobile ? mobileDelegate : desktopDelegate
        width: listView.width
        property var deviceId: model.id
        property string emailText: deviceModel // TODO email
        property int deviceType: model.deviceType
        property string contentTxt: "" // TODO
        property int idx: index
      }
    }
  }

  Component {
    id: mobileDelegate
    UIKit.DeviceInfoMobileDelegate {
      email: emailText
      contentText: contentTxt
      avatar.deviceType: deviceType
      onBlockClicked: PendingDeviceListModel.blockOne(deviceId)
      onApproveClicked: PendingDeviceListModel.activateOne(deviceId)
      onRejectClicked: PendingDeviceListModel.removeOne(deviceId)
    }
  }

  Component {
    id: desktopDelegate
    UIKit.DeviceInfoDesktopDelegate {
      email: emailText
      contentText: contentTxt
      avatar.deviceType: deviceType
      selectionModel: ism
      //model: listView.model
      index: idx
      onBlockClicked: PendingDeviceListModel.blockOne(deviceId)
      onApproveClicked: PendingDeviceListModel.activateOne(deviceId)
      onRejectClicked: PendingDeviceListModel.removeOne(deviceId)
    }
  }
}
