import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m12
  width: parent.width
  height: parent.height

  UIKit.GenericListView {
    id: objectsList
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
    Layout.fillWidth: true
    Layout.fillHeight: true
    clip: true
    spacing: app.isPhone ? ThemeController.style.margin.m12 : ThemeController.style.margin.m24

    model: QuodListModel
    delegate: UIKit.ObjectDelegate {
      title: name
      text: content
      width: parent.width
      actionButton: true
      avatar.iconSrc: iconSrc
      avatar.state: index % 3 === 0 ? "out-of-sync" : (index % 3 === 1 ? "syncing": "normal")
      checkboxOnTheLeft: !app.isMobile
      checkable: false

      onDelegateClicked: {
      }

      onActionButtonClicked: {
      }
    }
  }
}
