import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2
import QtQml.Models 2.12

Flickable {
  clip: true
  width: parent.width
  height: parent.height
  contentHeight: column.height

  property var userObject: userEditor.object

  SortFilterProxyModel {
    id: pinnedModel
    sourceModel: DeviceListModel
    filters: [
      ValueFilter {
        roleName: "pinned"
        value: true
      },
      ValueFilter {
        roleName: "email"
        value: userObject.email
      }
    ]
  }

  SortFilterProxyModel {
    id: objectsProxyModel
    sourceModel: DeviceListModel
    filters: [
      ValueFilter {
        roleName: "pinned"
        value: false
      },
      ValueFilter {
        roleName: "email"
        value: userObject.email
      }
    ]
  }

  ColumnLayout {
    id: column
    anchors.left: parent.left
    anchors.right: parent.right
    spacing: 16

    UIKit.BaseText {
      text: Strings.pendingDeviceRequests
      font.weight: Font.Bold
      visible: pinnedModel.count > 0
    }

    UIKit.GenericListView {
      id: pinnedObjectList
      Layout.fillWidth: true
      Layout.preferredHeight: contentHeight
      clip: true
      spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16

      model: pinnedModel
      delegate: UIKit.DeviceDelegate {
        objectsModel: pinnedObjectList.model
        width: pinnedObjectList.width
        actionButton: true
        actionButtonIconSource: "qrc:/chevron-forward.svg"
        checkable: false

        title: model.deviceModel
        titleIcon: model.powerDevice
        avatar.deviceType: model.deviceType
        avatar.blocked: model.blocked
        avatar.inactive: !model.active
        online: model.online
        contentText: model.deviceOS
        syncing: model.syncing
        outOfSync: model.outOfSync
        lastSeenDate: model.lastSeen
      }
    }

    Rectangle {
      id: spacer
      Layout.topMargin: isMobile ? ThemeController.style.margin.m0 : ThemeController.style.margin.m8
      Layout.fillWidth: true
      color: ThemeController.style.mercuryColor
      height: s(1)
      visible: pinnedModel.count > 0
    }

    UIKit.GenericListView {
      id: listView
      Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m8
      Layout.bottomMargin: ThemeController.style.margin.m16
      Layout.fillWidth: true
      Layout.preferredHeight: contentHeight
      clip: true
      spacing: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16

      model: objectsProxyModel
      delegate: UIKit.DeviceDelegate {
        objectsModel: listView.model
        width: listView.width
        actionButton: true
        actionButtonIconSource: "qrc:/chevron-forward.svg"
        checkable: false

        title: model.deviceModel
        titleIcon: model.powerDevice
        avatar.deviceType: model.deviceType
        avatar.blocked: model.blocked
        avatar.inactive: !model.active
        online: model.online
        contentText: model.deviceOS
        syncing: model.syncing
        outOfSync: model.outOfSync
        lastSeenDate: model.lastSeen
      }
    }
  }
}
