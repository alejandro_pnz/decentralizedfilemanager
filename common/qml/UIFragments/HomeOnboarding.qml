import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m12
  property int delegateHeight : s(80)
  Rectangle {
    height: app.isPhone ? s(222) : s(230)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: "qrc:/illustrations/u9.png"
      mipmap: true
    }
  }

  UIKit.BaseText {
    id: titleComponent
    Layout.topMargin: app.isMobile ? ThemeController.style.margin.m8 : ThemeController.style.margin.m28
    alternativeFont: true
    font.weight: Font.Bold
    size: app.isPhone ?
            UIKit.BaseText.TextSize.H3 : UIKit.BaseText.TextSize.H2
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.alignment: Qt.AlignHCenter
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    text: Strings.startBy
  }

  UIKit.BaseText {
    id: contentComponent
    Layout.topMargin: app.isMobile ? -ThemeController.style.margin.m4 : 0
    text: Strings.quodAndFile
    Layout.fillWidth: true
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.CardDelegate {
    id: quodDelegate
    Layout.topMargin: app.isMobile ? 0 : ThemeController.style.margin.m12
    Layout.fillWidth: true
    Layout.preferredHeight: layout.delegateHeight
    icon.source: hovered ? "qrc:/add-quod-filled.svg" : "qrc:/add-quod.svg"
    title: Strings.addQuod
    text: Strings.addQuodDescription
    onClicked: homeManager.enterCreateQuod()
    Layout.leftMargin: s(102)
    Layout.rightMargin: s(102)
  }

  UIKit.CardDelegate {
    id: fileDelegate
    Layout.topMargin: app.isMobile ? -ThemeController.style.margin.m4 : ThemeController.style.margin.m4
    Layout.fillWidth: true
    Layout.preferredHeight: layout.delegateHeight
    icon.source: hovered ?"qrc:/add-file-filled.svg" : "qrc:/add-file.svg"
    title: Strings.addFile
    text: Strings.addFileDescription
    Layout.leftMargin: s(102)
    Layout.rightMargin: s(102)
    onClicked: homeManager.enterFileDialog()
  }
}
