import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m16
  signal buttonClicked(string passphrase)
  property int deviceType: Enums.Laptop
  property bool input: true
  property bool deviceDelegate: true

  property bool busy: false
  property string imageSrc: ""
  property string deviceUUID: ""

  enabled: !layout.busy

  Connections {
    target: pageManager
    function onSetupAddDevicePage(config) {
      title.text = config.title
      content.text = config.contentText
      if (config.input !== undefined) {
        layout.input = config.input
      }
      if (config.deviceDelegate !== undefined)
        layout.deviceDelegate = config.deviceDelegate
      if (config.deviceType !== undefined) {
        layout.deviceType = config.deviceType
      }
      if (config.deviceName !== undefined) {
        deviceDelegate.txt = config.deviceName
      }

      button.text = config.buttonText
      if (config.icon !== undefined) {
        icon.source = config.icon
        icon.visible = true
      } else {
        icon.visible = false
      }
      if (config.buttonHandler !== undefined) {
        layout.buttonClicked.connect(config.buttonHandler)
      }
      if (config.inputLabel !== undefined) {
        passphraseInput.labelText = config.inputLabel
      }
      if (config.inputPlaceholder !== undefined) {
        passphraseInput.placeholderTxt = config.inputPlaceholder
      }
      if (config.imageSrc !== undefined) {
        imageSrc = config.imageSrc
      }
      if (config.deviceUUID !== undefined) {
        deviceUUID = config.deviceUUID
      }
    }
  }

  UIKit.SvgImage {
    id: icon
    visible: false
    Layout.alignment: Qt.AlignHCenter
  }

  Rectangle {
    visible: !icon.visible
    Layout.preferredHeight: s(app.isMobile ? 222 : 230)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: imageSrc
      mipmap: true
    }
  }

  UIKit.BaseText {
    id: title
    Layout.topMargin: ThemeController.style.margin.m24
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    horizontalAlignment: Text.AlignHCenter
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.BaseText {
    id: content
    horizontalAlignment: Text.AlignHCenter
    Layout.fillWidth: true
    Layout.alignment: Qt.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.IconDelegate {
    id: deviceDelegate
    visible: layout.deviceDelegate
    Layout.topMargin: ThemeController.style.margin.m32
    Layout.fillWidth: true
    Layout.preferredHeight: s(64)
    boldText: true
    iconBgCircle: true
    iconBgColor: ThemeController.style.deviceDelegate.iconBgColor
    iconSrc: {
      switch(layout.deviceType) {
      case Enums.Laptop: return "qrc:/laptop.svg"
      case Enums.Phone: return "qrc:/phone.svg"
      case Enums.Tablet: return "qrc:/tablet.svg"
      }
    }
  }

  UIKit.InfoDelegate {
    contentText: Strings.deviceUUID.arg(deviceUUID)
    color: ThemeController.style.whiteColor
    border.width: s(1)
    border.color: ThemeController.style.seashellColor
    textObject.anchors.leftMargin: 24
  }

  Rectangle {
    Layout.topMargin: ThemeController.style.margin.m8
    height: s(1)
    Layout.fillWidth: true
    color: ThemeController.style.seashellColor
  }

  UIKit.InputField {
    id: passphraseInput
    Layout.topMargin: ThemeController.style.margin.m8
    labelText: Strings.passphrase
    placeholderTxt: Strings.typePassphrase
    inputRequired: true
    isPassPhrase: true
    eyeIconVisible: true
    visible: input
    onEnterPressed: button.clicked()
  }

  UIKit.StyledButton {
    id: button
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    textItem.visible: !pulseLoader.visible
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: {
      var passOK = validateMandatoryField(passphraseInput)
      if (passOK) {
        buttonClicked(passphraseInput.textField.text)
        passphraseInput.textField.text = ""
      }
    }

    UIKit.PulseLoader {
      id: pulseLoader
      anchors.centerIn: parent
      visible: layout.busy
    }
  }
}
