import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2

Flickable {
  id: flick
  clip: true
  height: parent.height
  width: parent.width
  contentHeight: content.height

  Column {
    id: content
    anchors.top: parent.top
    anchors.topMargin: ThemeController.style.margin.m12
    anchors.left: parent.left
    anchors.right: parent.right
    spacing: ThemeController.style.margin.m24
    bottomPadding: ThemeController.style.margin.m24

    UIKit.ResponsiveGridLayout {
      id: idListView
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.rightMargin: -horizontalSpacing
      horizontalSpacing: ThemeController.style.margin.m32
      verticalSpacing: ThemeController.style.margin.m12
      delegateHeight: s(64)
      delegateMinimumWidth: s(268)
      delegateMaximumWidth: parent.width

      currentIndex: -1
      model: quodSubTypeModel
      delegate: Item {
        id: isDelegate
        width: idListView.cellWidth
        height: idListView.cellHeight
        property string titleTxt: name
        property string iconSource: iconPath
        UIKit.CardDelegate {
          icon.source: iconSource
          title: titleTxt
          checkable: false
          height: idListView.cellHeight - idListView.verticalSpacing
          width: idListView.maximumCellContentWidth
          avatar.pixelSize: ThemeController.style.margin.m48
          avatar.iconSize: ThemeController.style.margin.m24
          topPadding: ThemeController.style.margin.m8
          bottomPadding: ThemeController.style.margin.m8
          onClicked: {
            quodEditor.object = object
            homeManager.openPage(Pages.EditQuodDetailsPage)
          }
        }
      }

      cellWidth: {
        if((width - horizontalSpacing) * 0.5 < delegateMinimumWidth){
          width
        }
        else {
          width * 0.5
        }
      }
    }

    QuodTypeListModel {
      id: quodSubTypeModel
      source: quodEditor.subTypes
    }
  }
}
