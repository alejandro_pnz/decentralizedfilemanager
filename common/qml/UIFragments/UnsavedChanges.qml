import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: content
  spacing: ThemeController.style.margin.m12
  signal closePopup()

  UIKit.SvgImage {
    source: "qrc:/warning-icon.svg"
    Layout.preferredHeight: s(55)
    Layout.preferredWidth: s(55)
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.BaseText {
    text: Strings.unsavedChanges
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    font.weight: Font.Bold
    color: ThemeController.style.primaryColorOpacity90
  }

  UIKit.BaseText {
    text: Strings.proceedWithoutSaving
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    size: UIKit.BaseText.TextSize.XSmall
    color: ThemeController.style.primaryColorOpacity90
  }

  UIKit.StyledButton {
    text: Strings.save
    Layout.fillWidth: true
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Primary
    Layout.preferredHeight: s(28)
    onClicked: closePopup()
  }

  UIKit.StyledButton {
    Layout.topMargin: -s(6)
    text: Strings.discard
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Secondary
    Layout.fillWidth: true
    Layout.preferredHeight: s(28)
    onClicked: closePopup()
  }
}
