import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
ColumnLayout {
  id: content

  Connections {
    target: homeManager
    function onSetupQuodCheckedOut(config) {
      if (edit) {
        content.title = Strings.quodCheckedEdit
        content.contentText = Strings.quodCheckedEditContent
        content.primaryBtnText = Strings.gotIt
        content.secondaryBtnText = ""
      } else {
        content.title = Strings.quodCheckedOut
        content.contentText = Strings.quodCheckedOutContent
        content.primaryBtnText = Strings.gotIt
        content.secondaryBtnText = Strings.viewWhitelist
      }
    }
  }

  // Configurable properties
  property string title
  property string contentText
  property string primaryBtnText
  property string secondaryBtnText

  property int maximumHeight
  property int verticalPadding
  property alias listView: listView

  spacing: ThemeController.style.margin.m16
  anchors.centerIn: parent
  Column {
    id: topContent
    Layout.fillWidth: true
    spacing: ThemeController.style.margin.m16
    UIKit.PopupHeader {
      width: parent.width
      title: title
      onCloseClicked: popup.close()
    }

    UIKit.BaseText {
      text: contentText
      width: parent.width
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      horizontalAlignment: Text.AlignHCenter
    }
  }

  ListView {
    id: listView
    property int maximumContentHeight: content.maximumHeight - 2*content.verticalPadding
                                       - topContent.implicitHeight - button.implicitHeight
                                       - 2 * spacing - ThemeController.style.margin.m20
//    implicitHeight: Math.min(contentHeight, maximumContentHeight)
    Layout.fillWidth: true
    Layout.fillHeight: true
    clip: true
    spacing: ThemeController.style.margin.m12
    model: contactsModel
    delegate:   UIKit.ContactCard {
      width: parent.width
      implicitHeight: s(84)
      title: emailTxt
      checkBoxMode: false
      checkable: false
      bg.border.width: s(1)
      bg.border.color: config.bdColor
      avatarObject.borderless: true
      labelBottom.visible: roleTxt.length > 0
      labelLeft.visible: false
      role: roleTxt
      states: []
      rightCheckbox.visible: false
      leftCheckbox.visible: false
      iconObject.visible: false
      pressAndHoldHandler: function(){}
      onClicked: console.log(contactsModel.count)
    }

    states: [
      State {
        name: "desktop"
        when: !app.isMobile || app.isTablet
        PropertyChanges {
          target: listView
          implicitHeight: Math.min(contentHeight, maximumContentHeight)
          Layout.fillHeight: false
        }
      },
      State {
        name: "mobile"
        when: app.isMobile && !app.isTablet
        PropertyChanges {
          target: listView
          implicitHeight: -s(1)
          Layout.fillHeight: true
        }
      }
    ]
  }

  ListModel {
    id: contactsModel
  }

  Component.onCompleted: {
    contactsModel.append({ emailTxt: "bellejk@example.com", roleTxt: "Guest", isChecked: true, accOwnerMail: "johndoe@gmail.com"});
    contactsModel.append({ emailTxt: "ianjk@example.com", roleTxt: "", isChecked: false, accOwnerMail: "johndoe@gmail.com"});
    contactsModel.append({ emailTxt: "johnanders@example.cdasdom", roleTxt: "", isChecked: true, accOwnerMail: "johndoe@gmail.com"});
    contactsModel.append({ emailTxt: "lauren.work@example.dsascom", roleTxt: "", isChecked: false, accOwnerMail: "johndoe@gmail.com"});
    contactsModel.append({ emailTxt: "michael.work@example.com", roleTxt: "", isChecked: false, accOwnerMail: "csmith@example.com"});
    contactsModel.append({ emailTxt: "example@example.com", roleTxt: "", isChecked: false,accOwnerMail: "csmith@example.com"})
  }

  Column {
    id: button
    Layout.topMargin: ThemeController.style.margin.m20
    Layout.fillWidth: true
    spacing: ThemeController.style.margin.m12

    UIKit.StyledButton {
      width: parent.width
      text: primaryBtnText
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      onClicked: {
        popup.close()
      }
    }

    UIKit.StyledButton {
      visible: text.length > 0
      width: parent.width
      text: secondaryBtnText
      type: UIKit.StyledButton.ButtonStyle.Secondary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      onClicked: {
        popup.close()
      }
    }
  }
}
