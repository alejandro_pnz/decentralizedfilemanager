import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15


Item {
    width: parent.width
    height: s(17)
    Rectangle {
        anchors.bottom: parent.bottom
        width: parent.width
        height: s(1)
        color: ThemeController.style.seashellColor
    }
}
