import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

import com.testintl 1.0

ColumnLayout {
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m24

  property bool isMobile: app.isPhone
  property int pageMode: Enums.GlobalSettings
  property int globalMode: pageMode === Enums.GlobalSettings
  property int userMode: pageMode === Enums.UserSettings
  property int deviceMode: pageMode === Enums.DeviceSettings
  property bool isAO: AuthenticationManager.currentAccount.accountOwner

  UIKit.BaseText {
    text: Strings.dataStr
    visible: !app.isPhone
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
  }

  Flickable {
    id: flick
    Layout.fillHeight: true
    Layout.fillWidth: true
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: content.height
    clip: true

    ColumnLayout {
      id: content
      spacing: ThemeController.style.margin.m16
      width: parent.width

      UIKit.BaseText {
        text: globalMode ? Strings.unlockAuthorizationMethod
                         : Strings.authorizeUnlocking
        font.weight: Font.Bold
      }

      ButtonGroup {
        id: authorizationGroup
        buttons: globalMode ? authoriztionMethod.children
                            : authorizeUsing.children
        exclusive: true
      }

      Column {
        id: authoriztionMethod
        spacing: ThemeController.style.margin.m24
        Layout.topMargin: ThemeController.style.margin.m8
        visible: globalMode

        UIKit.StyledRadioButton {
          checked: true
          text: Strings.email
          enabled: isAO
        }

        UIKit.StyledRadioButton {
          text: Strings.sms
          enabled: isAO
        }

        UIKit.StyledRadioButton {
          text: Strings.authenticator
          enabled: isAO
        }

        UIKit.StyledRadioButton {
          text: Strings.userChoice
          enabled: isAO
        }
      }

      Column {
        id: authorizeUsing
        spacing: ThemeController.style.margin.m24
        Layout.topMargin: ThemeController.style.margin.m8
        visible: !globalMode

        UIKit.StyledRadioButton {
          text: Strings.authenticator
          checked: true
        }

        UIKit.StyledRadioButton {
          text: Strings.email
        }

        UIKit.StyledRadioButton {
          text: Strings.sms
        }
      }

      Rectangle {
        Layout.topMargin: isMobile ? ThemeController.style.margin.m12 : ThemeController.style.margin.m20
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
      }

      UIKit.SwitchItem {
        Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m12
        text: Strings.allowPrivateData
        contentText: Strings.allPublicSync
        showSeparator: true
        separatorMargin: isMobile ? ThemeController.style.margin.m8 : ThemeController.style.margin.m16
        visible: globalMode
        enabled: isAO
      }

      UIKit.SwitchItem {
        Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m12
        text: Strings.auditLogs
        contentText: Strings.logsOfActions
        showSeparator: true
        separatorMargin: isMobile ? ThemeController.style.margin.m8 : ThemeController.style.margin.m16
        visible: globalMode
        enabled: isAO
      }

      UIKit.SwitchItem {
        Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m12
        text: Strings.exportInClear
        contentText: Strings.exportOnlyEncrypted
        showSeparator: true
        separatorMargin: isMobile ? ThemeController.style.margin.m8 : ThemeController.style.margin.m16
        visible: globalMode
        enabled: isAO
      }

      UIKit.PickerDelegate {
        Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m12
        labelText: Strings.deleteItemsAfter
        pickerTitle: Strings.deleteItemsAfter
        inputRequired: false
        leftEyeIcon: false
        objectsModel: [Strings.day.arg("1"), Strings.days.arg("14"), Strings.days.arg("7"), Strings.days.arg("30"), Strings.never]
        enabled: !globalMode || isAO
      }

      UIKit.PickerDelegate {
        labelText: Strings.maximumSyncable
        pickerTitle: Strings.maximumSyncable
        inputRequired: false
        leftEyeIcon: false
        objectsModel: ["1 GB", "2 GB", "5 GB", "10 GB", "20 GB", Strings.noLimit]
        enabled: !globalMode || isAO
      }

      Rectangle {
        Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m8
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: 1
      }

      UIKit.BaseText {
        text: userMode ? Strings.purgeClipboardAfter
                       : Strings.purgeClipboard
        font.weight: Font.Bold
        visible: !globalMode
      }

      UIKit.BaseText {
        Layout.topMargin: -ThemeController.style.margin.m4
        text: Strings.settingRegulates
        visible: !globalMode
        size: UIKit.BaseText.TextSize.Small
        color: ThemeController.style.shuttleColor
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        Layout.fillWidth: true
      }

      ButtonGroup {
        id: purgeClipboardGroup
        buttons: purgeClipboard.children
        exclusive: true
      }

      Column {
        id: purgeClipboard
        spacing: ThemeController.style.margin.m24
        Layout.topMargin: ThemeController.style.margin.m12
        visible: !globalMode

        UIKit.StyledRadioButton {
          checked: true
          text: Strings.minutes.arg("2")
        }

        UIKit.StyledRadioButton {
          text: Strings.minutes.arg("5")
        }

        UIKit.StyledRadioButton {
          text: Strings.minutes.arg("10")
        }
      }

      Rectangle {
        Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m8
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
        visible: !globalMode
      }

      UIKit.StyledButton {
        text: Strings.privacyAndDataRights
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        type: UIKit.StyledButton.ButtonStyle.Text
        visible: userMode
        Layout.alignment: Qt.AlignLeft
      }

      UIKit.SwitchItem {
        Layout.topMargin: ThemeController.style.margin.m4
        text: Strings.alertsOnSend
        contentText: Strings.receiveAlerts
        visible: globalMode
        enabled: isAO
      }

      UIKit.SwitchItem {
        text: Strings.allFilesRemote
        contentText: Strings.enableThisToSaveSpace
        showSeparator: true
        visible: deviceMode
      }

      UIKit.SwitchItem {
        text: Strings.enablePasswordAutofill
        showSeparator: true
        Layout.topMargin: ThemeController.style.margin.m4
        visible: deviceMode && isMobile
      }

      UIKit.SwitchItem {
        text: Strings.allowScreenshots
        showSeparator: isMobile
        Layout.topMargin: ThemeController.style.margin.m4
        visible: deviceMode
      }

      UIKit.SwitchItem {
        text: Strings.receiveInAppNotifications
        contentText: Strings.inAppMessagesToKeepUpdated
        showSeparator: true
        Layout.topMargin: ThemeController.style.margin.m4
        visible: deviceMode && isMobile
      }

      UIKit.SwitchItem {
        text: Strings.receivePushNotofications
        contentText: Strings.pushNotificationsToKeepUpdated
        Layout.topMargin: ThemeController.style.margin.m4
        visible: deviceMode && isMobile
      }

      Item {
        Layout.fillWidth: true
        Layout.preferredHeight: s(25)
      }
    }
  }
}
