import QtQuick 2.15
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Layouts 1.12

Item {
  property var fileViewer

  onFileViewerChanged:  {
    if (fileViewer) {
      noteArea.text = fileViewer.content
    }
  }

  ScrollView {
    id: scrollView
    anchors{
      fill: parent
      topMargin: ThemeController.style.margin.m16
    }
    TextArea {
      id: noteArea
      color: ThemeController.style.font.defaultColor
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      text: noteContent
      textFormat: TextEdit.MarkdownText
      selectByMouse: true
      selectedTextColor: ThemeController.style.whiteColor
      persistentSelection: true
      readOnly: true

      font {
        pixelSize: ThemeController.style.font.bodySize
        family: ThemeController.style.font.primaryFontFamily
      }
    }
  }
}
