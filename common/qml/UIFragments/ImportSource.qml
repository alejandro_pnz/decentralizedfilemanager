import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

ListView {
  id: listView
  width: parent.width
  spacing: 0
  height: contentHeight
  model: actionModel
  clip: true
  delegate: UIKit.ActionListDelegate {
    leftPadding: 0
    rightPadding: 0
    width: listView.width
    text: name
    icon.source: iconSource
    display: AbstractButton.TextBesideIcon
    separator: index < listView.model.count - 1
    action: actionSource
  }
  Item {
    ImportFileDialog {
      id: fileDialog
      onAccepted: {
          console.log("IMPORT FILES", files)
          quodEditor.photoEditor.importFiles(files)
      }
    }

    Action {
      id: cameraAction
      onTriggered: {
        console.log("Home manager", homeManager)
        //              homeManager.back()
        homeManager.openPage(Pages.CameraCapturePage, true)
      }
    }

    Action {
      id: fileAction
      onTriggered: fileDialog.open()
    }

    ListModel {
      id: actionModel
      Component.onCompleted: {
        actionModel.append({name: "Camera", iconSource: "qrc:/camera.svg", actionSource: cameraAction })
        actionModel.append({name: "Files", iconSource: "qrc:/export-default.svg", actionSource: fileAction })
      }
    }
  }
}

