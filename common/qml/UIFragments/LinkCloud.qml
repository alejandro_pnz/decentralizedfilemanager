import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

Flickable {
  Layout.fillHeight: true
  Layout.fillWidth: true
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: contentColumn.height
  clip: true

  ColumnLayout {
    id: contentColumn
    spacing: ThemeController.style.margin.m12
    width: parent.width

    UIKit.CardDelegate {
      title: Strings.addGoogleDriveStorage
      icon.source: "qrc:/googledrive.svg"
      Layout.fillWidth: true
      Layout.preferredHeight: s(64)
      avatar.pixelSize: s(48)
      avatar.iconSize: s(24)
      topPadding: ThemeController.style.margin.m8
      bottomPadding: ThemeController.style.margin.m8
    }

    UIKit.CardDelegate {
      title: Strings.addDropboxStorage
      icon.source: "qrc:/dropbox.svg"
      Layout.fillWidth: true
      Layout.preferredHeight: s(64)
      avatar.pixelSize: s(48)
      avatar.iconSize: s(24)
      topPadding: ThemeController.style.margin.m8
      bottomPadding: ThemeController.style.margin.m8
    }

    UIKit.CardDelegate {
      title: Strings.addOneDriveStorage
      icon.source: "qrc:/onedrive.svg"
      Layout.fillWidth: true
      Layout.preferredHeight: s(64)
      avatar.pixelSize: s(48)
      avatar.iconSize: s(24)
      topPadding: ThemeController.style.margin.m8
      bottomPadding: ThemeController.style.margin.m8
    }

    UIKit.Infobox {
      Layout.topMargin: ThemeController.style.margin.m12
      Layout.fillWidth: true
      text: Strings.selectTheCloudStorage
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }
  }
}
