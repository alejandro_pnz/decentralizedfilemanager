import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2
import QtQml.Models 2.12
import UIModel 1.0

import com.testintl 1.0

Item {
  id: contentItem
  property string title: Strings.noPinnedItems
  property string content: Strings.noPinnedItemsInfo
  property string iconSource: "qrc:/pin-image.svg"
  property bool  libraryIsEmpty: (QuodListModel.count < 1 && FileListModel.count < 1)

  ColumnLayout {
    id: noItemsColumn
    visible: !itemsPinned
    anchors.fill: parent
    spacing: ThemeController.style.margin.m12

    Item {
      id: topMargin
      Layout.maximumHeight: app.isPhone? s(68) : s(121)
      Layout.minimumHeight: 0
      Layout.fillHeight: true
      Layout.fillWidth: true
    }

    UIKit.SvgImage {
      id: pinImage
      source: iconSource
      visible: implicitHeight > app.isTablet ? s(60) : s(80)
      Layout.maximumHeight: s(app.isPhone ? 100 : 140)
      Layout.preferredHeight: s(app.isPhone ? 100 : 140)
      Layout.fillHeight: true
      width: height
      Layout.alignment: Qt.AlignHCenter
      Component.onCompleted: console.log("IMPLICIT HEIGHT", implicitHeight, height)
    }

    ColumnLayout {
      id: textColumn
      Layout.fillWidth: true
      spacing: ThemeController.style.margin.m12
      UIKit.BaseText {
        Layout.topMargin: ThemeController.style.margin.m4
        text: title
        font.weight: Font.Bold
        size: UIKit.BaseText.TextSize.H3
        Layout.alignment: Qt.AlignHCenter
        horizontalAlignment: Qt.AlignHCenter
        Layout.fillWidth: true
      }
      UIKit.BaseText {
        text: content
        Layout.alignment: Qt.AlignHCenter
        Layout.preferredWidth: s(370)
        Layout.maximumWidth: s(370)
        Layout.fillWidth: true
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        horizontalAlignment: Qt.AlignHCenter
      }

      UIKit.StyledButton {
        Layout.alignment: Qt.AlignHCenter
        Layout.topMargin: ThemeController.style.margin.m20
        text: contentItem.libraryIsEmpty ? Strings.startBy : Strings.goToLibrary
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        onClicked: {
            if(contentItem.libraryIsEmpty)
                homeManager.openPage(Pages.HomeOnboardingPage)
            else
                homeManager.enterLibraryPage()
        }
      }
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }
  }
}
