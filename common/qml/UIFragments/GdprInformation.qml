import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: layout
  spacing: 0
  property bool showFooter: false

  UIKit.BaseText {
    text: Strings.gdprInformation
    visible: !app.isPhone
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
  }

  UIKit.SettingsDelegate {
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
    Layout.fillWidth: true
    title: Strings.eula
    contentText: Strings.readLicenseAgreement
    buttonText: Strings.read
    showTextButton: !app.isPhone
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.privacyPolicy
    contentText: Strings.readPrivacyPolicy
    buttonText: Strings.read
    showTextButton: !app.isPhone
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
