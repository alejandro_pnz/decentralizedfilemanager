import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtPromise 1.1

Flickable {
  id: flickable
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: layout.height
  clip: true

  property alias passwordText: pwdInput.text
  property alias refreshButton: refreshButton

  function generatePassword() {
    var option = passwordTypeModel.get(pickerDelegate.comboBox.currentIndex).option
    var pwd = app.generatePassword(option, passwordSlider.slider.value, switchDelegate.switchButton.checked)
    pwdInput.text = pwd;
  }

  ListModel {
    id: passwordTypeModel
  }

  ColumnLayout {
    id: layout
    spacing: ThemeController.style.margin.m8
    width: parent.width

    GeneratedPasswordInput {
      id: pwdInput
      Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 :
                                      ThemeController.style.margin.m24
      Layout.fillWidth: true
      Layout.minimumHeight: ThemeController.style.defaultInputHeight
      onPasswordChanged: {
        var promise = Future.promise(PasswordStrengthEvaluator.strength(text))

        promise.then(function(strength) {
          if (strength === PasswordStrengthEvaluator.VeryStrongPassword) {
            passwordStrengthIndicator.state = 'high'
          } else if (strength === PasswordStrengthEvaluator.StrongPassword) {
            passwordStrengthIndicator.state = 'medium'
          } else if (strength === PasswordStrengthEvaluator.ReasonablePassword) {
            passwordStrengthIndicator.state = 'low'
          } else {
            passwordStrengthIndicator.state = 'verylow'
          }
        });
      }
    }

    UIKit.PasswordStrengthIndicator {
      id: passwordStrengthIndicator
      Layout.fillWidth: true
      visible: pwdInput.text.length > 0
      displayedText.text: Strings.veryStrong
      veryWeakPassword: Strings.veryWeak
      weakPassword: Strings.weak
      strongPassword: Strings.strong
      veryStrongPassword: Strings.veryStrong
      tipIconVisible: true
    }

    UIKit.StyledButton {
      id: refreshButton
      Layout.topMargin: ThemeController.style.margin.m24
      Layout.fillWidth: true
      text: Strings.refresh
      Layout.alignment: Qt.AlignHCenter
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      onClicked: generatePassword()
    }

    Rectangle {
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
    }

    Item {
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.fillWidth: true
      Layout.preferredHeight: s(378)
      Rectangle {
        id: optionsRect
        anchors.fill: parent
        border.width: s(1)
        border.color: ThemeController.style.whisperColor
        radius: s(8)

        ColumnLayout {
          anchors {
            fill: parent
            leftMargin: ThemeController.style.margin.m16
            rightMargin: ThemeController.style.margin.m16
            topMargin: ThemeController.style.margin.m24
            bottomMargin: ThemeController.style.margin.m24
          }
          spacing: ThemeController.style.margin.m24

          UIKit.BaseText {
            text: Strings.options
            font.weight: Font.Bold
            size: UIKit.BaseText.TextSize.H3
          }

          UIKit.PickerDelegate {
            id: pickerDelegate
            leftEyeIcon: false
            labelText: Strings.type
            placeholderTxt: Strings.type
            objectsModel: passwordTypeModel
          }

          Rectangle {
            Layout.preferredHeight: s(1)
            Layout.fillWidth: true
            color: ThemeController.style.seashellColor
          }

          UIKit.PasswordLengthSlider {
            id: passwordSlider
          }

          Rectangle {
            Layout.preferredHeight: s(1)
            Layout.fillWidth: true
            color: ThemeController.style.seashellColor
          }

          UIKit.SwitchDelegate {
            id: switchDelegate
          }
        }
      }

      DropShadow {
        id: dropShadow
        anchors.fill: optionsRect
        horizontalOffset: 0
        verticalOffset: s(2)
        radius: s(10)
        //samples: s(21)
        color: Qt.rgba(0, 0, 0, 0.04)
        source: optionsRect
      }
    }

    Item {
      Layout.fillWidth: true
      Layout.fillHeight: true
    }
  }

  Component.onCompleted: {
    passwordTypeModel.append({option: Enums.LettersAndNumbers, modelData: Strings.lettersAndNumbers})
    passwordTypeModel.append({option: Enums.NumbersOnly, modelData: Strings.numbersOnly})
    passwordTypeModel.append({option: Enums.Passphrase, modelData: Strings.passphrase})
    passwordTypeModel.append({option: Enums.Random, modelData: Strings.random})
    generatePassword()
  }
}
