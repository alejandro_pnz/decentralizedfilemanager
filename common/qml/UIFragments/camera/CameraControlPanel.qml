import QtQuick 2.15
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0
import QtMultimedia 5.15

Item {
  id: control
  height: s(136)

  property bool isBackCamera: camera.position == Camera.BackFace
  property bool flashOn: camera.flash.mode == Camera.FlashOn

  function toogleFlash() {
    if (flashOn) {
      camera.flash.mode = Camera.FlashOff
    } else {
      camera.flash.mode = Camera.FlashOn
    }
  }

  function toogleCamera() {
    console.log("Toogle camera, is back", isBackCamera)
    if (isBackCamera) {
      camera.position = Camera.FrontFace
    } else {
      camera.position = Camera.BackFace
    }
  }

  // Background
  Rectangle {
    id: topRect
    anchors.fill: parent
    color: ThemeController.style.blackColor
    radius: s(30)
  }
  Rectangle {
    id: bg
    color: ThemeController.style.blackColor
    anchors.fill: parent
  }

  // Buttons
  Grid {
    id: grid
    spacing: ThemeController.style.margin.m40
    verticalItemAlignment: Grid.AlignVCenter
    horizontalItemAlignment: Grid.AlignHCenter
    UIKit.CameraButton {
      checked: isBackCamera
      icon.source: "qrc:/rotate-camera.svg"
      visible: Screen.orientation === Qt.LandscapeOrientation
      onToggled: toogleCamera()
    }
    UIKit.CameraButton {
      checked: flashOn
      visible: Screen.orientation !== Qt.LandscapeOrientation
      onToggled: toogleFlash()
    }
    UIKit.CameraCaptureButton {
      onClicked: camera.imageCapture.capture()
    }
    UIKit.CameraButton {
      checked: flashOn
      onToggled: toogleFlash()
      visible: Screen.orientation === Qt.LandscapeOrientation
    }
    UIKit.CameraButton {
      checked: isBackCamera
      icon.source: "qrc:/rotate-camera.svg"
      visible: Screen.orientation !== Qt.LandscapeOrientation
      onToggled: toogleCamera()
    }
  }

  states: [
    State {
      name: "portrait"
      when: Screen.orientation === Qt.PortraitOrientation
      AnchorChanges {
        target: grid
        anchors {
          top: parent.top
          horizontalCenter: parent.horizontalCenter
          verticalCenter: undefined
          left: undefined
          right: undefined
        }
      }
      PropertyChanges {
        target: grid
        height: s(64)
        width: implicitWidth
        columns: 3
        rows: 1
        layoutDirection: Grid.LeftToRight
        anchors {
          topMargin: ThemeController.style.margin.m24
          bottomMargin: 0
          leftMargin: 0
          rightMargin: 0
        }
      }
      AnchorChanges {
        target: control
        anchors {
          top: undefined
          right: parent.right
          left: parent.left
          bottom: parent.bottom
        }
      }
      PropertyChanges {
        target: control
        height: s(136)
        width: implicitWidth
      }
      PropertyChanges {
        target: bg
        anchors {
          topMargin: s(30)
          bottomMargin: 0
          leftMargin: 0
          rightMargin: 0
        }
      }
    },
    State {
      name: "landscape"
      when: Screen.orientation === Qt.LandscapeOrientation
      AnchorChanges {
        target: grid
        anchors {
          top: undefined
          horizontalCenter: undefined
          verticalCenter: parent.verticalCenter
          left: parent.left
          right: undefined
        }
      }
      PropertyChanges {
        target: grid
        height: implicitHeight
        width: s(64)
        columns: 1
        rows: 3
        anchors {
          topMargin: 0
          bottomMargin: 0
          leftMargin: ThemeController.style.margin.m24
          rightMargin: 0
        }
      }
      AnchorChanges {
        target: control
        anchors {
          top: parent.top
          right: parent.right
          left: undefined
          bottom: parent.bottom
        }
      }
      PropertyChanges {
        target: control
        height: implicitHeight
        width: s(136)
      }
      PropertyChanges {
        target: bg
        anchors {
          topMargin: 0
          bottomMargin: 0
          leftMargin: s(30)
          rightMargin: 0
        }
      }
    },
    State {
      name: "invlandscape"
      when: Screen.orientation === Qt.InvertedLandscapeOrientation
      AnchorChanges {
        target: grid
        anchors {
          top: undefined
          horizontalCenter: undefined
          verticalCenter: parent.verticalCenter
          left: undefined
          right: parent.right
        }
      }
      PropertyChanges {
        target: grid
        height: implicitHeight
        width: s(64)
        columns: 1
        rows: 3
        anchors {
          topMargin: 0
          bottomMargin: 0
          leftMargin: 0
          rightMargin: ThemeController.style.margin.m24
        }
      }
      AnchorChanges {
        target: control
        anchors {
          top: parent.top
          right: undefined
          left: parent.left
          bottom: parent.bottom
        }
      }
      PropertyChanges {
        target: control
        height: implicitHeight
        width: s(136)
      }
      PropertyChanges {
        target: bg
        anchors {
          topMargin: 0
          bottomMargin: 0
          leftMargin: 0
          rightMargin: s(30)
        }
      }
    }
  ]
}

