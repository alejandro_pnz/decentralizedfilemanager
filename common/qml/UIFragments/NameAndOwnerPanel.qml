import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import com.testintl 1.0

EditPanel {
  id: ownerPanel
  title: Strings.nameAndOwner
//  titleComponent.color: ThemeController.style.textInput.labelTextColor
  hasBottomSpacer: false
  onEditButtonClicked: {
    if (app.isPhone) {
      finishEditing()
      homeManager.enterEditGeneralDetailsPage({ownerName: ownerName})
    }
  }

  onSaveButtonClicked: {
      homeManager.currentEditor.save()
  }
  onCancelButtonClicked: {
    homeManager.currentEditor.name = homeManager.currentEditor.object.name
    nameInput.errorMessage = []
    nameInput.state = 'default'
    nameInput.textField.text = homeManager.currentEditor.name
  }

  signal focusNext()
  signal focusPrevious()
  property bool overrideTab: false
  property bool overrideBacktab: false

  property string ownerName: Utility.formatUserName(homeManager.currentEditor.object.ownerId)

  property alias nameInput: nameInput

  validatorMethod: function validate() {
    var ok = validateMandatoryField(nameInput)
    return ok
  }

  Grid {
    id: ownerGrid
    Layout.fillWidth: true
    columns: (ownerPanel.width - ThemeController.style.margin.m16) * 0.5 < s(268) || app.isPhone ? 1 : 2
    columnSpacing: ThemeController.style.margin.m16
    rowSpacing: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m32
    property int inputWidth: columns == 1 ? parent.width : (parent.width - ThemeController.style.margin.m16) * 0.5
    UIKit.InputField {
      id: nameInput
      textField.text: homeManager.currentEditor.name
      width: ownerGrid.inputWidth
      textField.onTextChanged: {
        homeManager.currentEditor.name = textField.text
      }
      inputRequired: true
      clearIconVisible: true
      rightIconSource: ownerPanel.editMode ? "qrc:/close-icon.svg" : "qrc:/duplicate-icon.svg"
      rightIconBtn.style: ownerPanel.editMode ? ThemeController.style.fieldButton : ThemeController.style.fieldButtonAlt
      overrideRightButton: !ownerPanel.editMode
      onRightButtonClicked: {
        if (overrideRightButton) {
          app.clipboard.copy(textField.text)
        }
      }
      textField.maximumLength: 256
      textField.readOnly: !ownerPanel.editMode
      onTabPressed: ownerInput.textField.forceActiveFocus()
      onBacktabPressed: {
        if (overrideBacktab) {
          focusPrevious();
        } else {
          ownerInput.textField.forceActiveFocus()
        }
      }
      onEnterPressed: ownerInput.textField.forceActiveFocus()
    }

    // TODO Refactor this input to use ComboBox
    UIKit.InputField {
      id: ownerInput
      width: ownerGrid.inputWidth

      labelText: Strings.owner
      placeholderTxt: ownerName
      textField.text: ownerName
      inputRequired: false
      clearIconVisible: false
      textField.enabled: ownerPanel.editMode
      textField.readOnly: true
      rightIconSource: "qrc:/chevron-down.svg"
      rightIconBtn.visible: ownerPanel.editMode && !app.isPhone

      textField.onPressed: {
        if (app.isPhone) {
          homeManager.enterChangeOwnerPage()
        } else {
          usersList.open()
        }
      }
      onTabPressed: {
        if (overrideTab) {
          focusNext();
        } else {
          nameInput.textField.forceActiveFocus()
        }
      }
      onBacktabPressed: nameInput.textField.forceActiveFocus()
    }
  }

  Item {
    UIKit.UsersListPopup {
      id: usersList
      y: ownerInput.y + ownerInput.height + s(8)
      x: ownerInput.x

      onChangedOwner: {
        ownerName = owner
        usersList.close()
      }

      onVisibleChanged: {
        if(!visible)
          ownerInput.textField.focus = false
      }
    }
  }

  //      Row {
  //        Layout.fillWidth: true
  //        UIKit.InputField {
  //          id: ownerInput
  //          property bool fill: (ownerPanel.width - ThemeController.style.margin.m16) * 0.5 < s(268) || app.isPhone
  //          width: fill ? parent.width : (parent.width - ThemeController.style.margin.m16) * 0.5

  //          labelText: ""
  //          placeholderTxt: ownerName
  //          textField.text: ownerName
  //          inputRequired: false
  //          clearIconVisible: false
  //          textField.enabled: ownerPanel.editMode
  //          textField.readOnly: true
  //          rightIconSource: "qrc:/chevron-down.svg"
  //          rightIconBtn.visible: ownerPanel.editMode && !app.isPhone

  //          textField.onPressed: usersList.open()
  //        }
  //      }
}
