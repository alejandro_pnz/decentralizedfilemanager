import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15


Item {
    property int headerBottomMargin: ThemeController.style.margin.m16
    height: s(24) + headerBottomMargin
    
    Item {
        height: s(24)
        anchors.top: parent.top
        UIKit.SvgImage {
            id: pinIcon
            height: s(16)
            width: s(16)
            source: "qrc:/pin.svg"
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
        }
        
        UIKit.BaseText {
            anchors.left: pinIcon.right
            anchors.leftMargin: s(6)
            text: Strings.pinned
            font.weight: Font.DemiBold
            color: ThemeController.style.manateeColor
        }
    }
}
