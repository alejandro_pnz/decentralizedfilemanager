import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import UIFragments 1.0 as UIFragments
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2
import QtQml.Models 2.12
import UIModel 1.0

ColumnLayout {
  spacing: ThemeController.style.homePageVerticalMargin
  width: parent.width
  height: parent.height
  property bool selectionMode: ism.hasSelection
  property ItemSelectionModel ism: selectionModel
  property string objectTypeName: Strings.contacts
  property int objectType: Enums.Contacts

  ItemSelectionModel {
    id: selectionModel
    model: ContactListModel
  }

  SortFilterProxyModel {
    id: pendingContactsModel
    sourceModel: ContactListModel
    filters: ValueFilter {
      roleName: "pending"
      value: true
    }
  }

  SortFilterProxyModel {
    id: pinnedModel
    sourceModel: ContactListModel
    filters: [
      ValueFilter {
        roleName: "pinned"
        value: true
      },
      ValueFilter {
        roleName: "pending"
        value: false
      }
     ]
  }

  SortFilterProxyModel {
    id: objectsProxyModel
    sourceModel: ContactListModel
    filters: [
      ValueFilter {
        roleName: "pinned"
        value: false
      },
      ValueFilter {
        roleName: "pending"
        value: false
      }
    ]
  }

  UIKit.SearchInput {
    id: search
    Layout.fillWidth: true
    placeholderText: Strings.searchContacts
    micIconVisible: false
    Layout.preferredHeight: ThemeController.style.defaultInputHeight
  }

  UIKit.DevicesPendingDelegate {
    id: devicesPendingDelegate
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m8 : 0
    Layout.fillWidth: true
    mode: UIKit.DevicesPendingDelegate.Mode.Contacts
    objectsModel: pendingContactsModel
    requestsCount: pendingContactsModel.count
    visible: pendingContactsModel.count > 0
  }

  NoItemsInfo {
    id: noItemsColumn
    Layout.fillWidth: true
    Layout.fillHeight: true
    title: Strings.emptyContactList
    text: Strings.contactListDescription
    iconSource: "qrc:/all-users.svg"
    topMargin: app.isPhone ? s(88) : s(140)
    visible: pinnedModel.count === 0 && objectsProxyModel.count === 0
  }

  Flickable {
    clip: true
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m8 : 0
    contentHeight: column.height
    visible: !noItemsColumn.visible

    Column {
      id: column
      anchors.left: parent.left
      anchors.right: parent.right
      spacing: ThemeController.style.margin.m16
      bottomPadding: ThemeController.style.margin.m16

      UIKit.GenericListView {
        id: pinnedObjectList
        anchors.left: parent.left
        anchors.right: parent.right
        height: contentHeight
        clip: true
        spacing: app.isPhone ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
        visible: pinnedObjectList.count > 0

        header: PinHeader { }

        footer: ListFooter { }

        model: pinnedModel
        delegate: UIKit.UserDelegate {
          width: pinnedObjectList.width
          title: model.email
          stateVisible: true
          online: model.online
          lastSeenDate: model.lastSeen
          selectionModel: ism
          objectsModel: pinnedObjectList.model

          checkable: !isMobile || pinnedObjectList.selectionMode
          onPressAndHold: {
            if (isMobile)
              pinnedObjectList.selectionMode = !pinnedObjectList.selectionMode
          }

          onDelegateClicked: {
            homeManager.openOneUserPage(model.id)
          }

          onActionButtonClicked: {
            pinnedObjectList.currentIndex = index
            actionMenu.model.currentObject = object
            actionMenu.handleActionButton(pinnedObjectList)
          }
        }
      }

      UIKit.GenericListView {
        id: listView
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: ThemeController.style.margin.m8
        height: contentHeight
        clip: true
        spacing: app.isPhone ? ThemeController.style.margin.m12 : ThemeController.style.margin.m16
        header: UIFragments.UsersListSortButton {}
        visible: objectsProxyModel.count > 0

        model: objectsProxyModel
        delegate: UIKit.UserDelegate {
          width: listView.width
          title: model.email
          stateVisible: true
          online: model.online
          lastSeenDate: model.lastSeen
          avatar.bottomIcon: true
          avatar.editIcon: false
          avatar.userMode: UIKit.Avatar.Mode.Public
          checkable: !isMobile || listView.selectionMode
          selectionModel: ism
          objectsModel: listView.model

          onPressAndHold: {
            if (isMobile)
              listView.selectionMode = !listView.selectionMode
          }

          onDelegateClicked: {
            homeManager.openOneUserPage(model.id)
          }

          onActionButtonClicked: {
            listView.currentIndex = index
            actionMenu.model.currentObject = object
            actionMenu.handleActionButton(listView)
          }
        }
      }
    }
  }

  OneUserActionMenu {
    id: actionMenu
  }
}
