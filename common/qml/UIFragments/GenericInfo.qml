import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m16

  property int type: Pages.Single
  property string title: ""
  property string contentText: ""
  property alias primaryButton: primaryButton
  property alias secondaryButton: secondaryButton
  property alias secondaryButtonLabel: secondaryButtonLabel
  signal primaryButtonClicked()
  signal secondaryButtonClicked()
  property string imageSrc: ""

  property bool showBackButton: true

  Connections {
    target: pageManager
    function onSetupGenericInfoPage(config) {
      setupPage(config)
      if (config.type !== undefined)
        layout.type = config.type
      layout.title = config.title
      layout.contentText = config.content
      if (layout.type !== Pages.None) {
        layout.primaryButton.text = config.primaryButtonText
        layout.primaryButtonClicked.connect(config.primaryButtonHandler)
        if (layout.type === Pages.Double) {
          layout.secondaryButtonLabel.text = config.secondaryButtonLabel
          layout.secondaryButton.text = config.secondaryButtonText
          layout.secondaryButtonClicked.connect(config.secondaryButtonHandler)
        }
      }
      if (config.errorMessage !== undefined) {
        infobox.errorMsg = config.errorMessage
      }
      if (config.showBackButton !== undefined) {
        showBackButton = config.showBackButton
      }
      if (config.imageSrc !== undefined) {
        imageSrc = config.imageSrc
      }
    }
  }

  UIKit.SvgImage {
    source: "qrc:/login-logo.svg"
    width: app.isPhone ? s(55) : s(64)
    height: app.isPhone ? s(64) : s(75)
    Layout.alignment: Qt.AlignHCenter
    visible: false
  }

  Rectangle {
    Layout.preferredHeight: s(app.isMobile ? 222 : 230)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: imageSrc
      mipmap: true
    }
  }

  UIKit.BaseText {
    id: titleComp
    Layout.topMargin: ThemeController.style.margin.m8
    text: title
    alternativeFont: true
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    horizontalAlignment: Text.AlignHCenter
    Layout.alignment: Qt.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.BaseText {
    id: contentComp
    Layout.fillWidth: true
    text: contentText
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.alignment: Qt.AlignHCenter
    visible: text.length > 0
  }

  UIKit.Errorbox {
    id: infobox
    Layout.fillWidth: true
    Layout.alignment: Qt.AlignHCenter
    visible: errorMsg.length > 0
  }

  UIKit.StyledButton {
    id: primaryButton
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: primaryButtonClicked()
  }

  UIKit.PulseLoader {
    id: pulseLoader
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.alignment: Qt.AlignHCenter
    color: ThemeController.style.button.primary.bgColor
    height: primaryButton.height
    barCount: 10
    spacing: s(5)
    width: primaryButton.width / 2
  }

  UIKit.BaseText {
    id: secondaryButtonLabel
    Layout.topMargin: -ThemeController.style.margin.m4
    Layout.alignment: Qt.AlignHCenter
    font.weight: Font.DemiBold
  }

  UIKit.StyledButton {
    id: secondaryButton
    Layout.topMargin: -ThemeController.style.margin.m4
    Layout.fillWidth: true
    type: UIKit.StyledButton.ButtonStyle.Secondary
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: secondaryButtonClicked()
  }

  Item {
      Layout.fillWidth: true
      Layout.fillHeight: true
  }

  states: [
    State {
      name: "singleButton"
      when: layout.type === Pages.Single
      PropertyChanges {
        target: primaryButton
        visible: true
      }
      PropertyChanges {
        target: secondaryButton
        visible: false
      }
      PropertyChanges {
        target: secondaryButtonLabel
        visible: false
      }
      PropertyChanges {
         target: pulseLoader
         visible: false
      }
    },
    State {
      name: "doubleButton"
      when: layout.type === Pages.Double
      PropertyChanges {
        target: primaryButton
        visible: true
      }
      PropertyChanges {
        target: secondaryButton
        visible: true
      }
      PropertyChanges {
        target: secondaryButtonLabel
        visible: secondaryButtonLabel.text.length
      }
      PropertyChanges {
         target: pulseLoader
         visible: false
      }
    },
    State {
      name: "progress"
      when: layout.type === Pages.Progress
      PropertyChanges {
        target: primaryButton
        visible: false
      }
      PropertyChanges {
        target: secondaryButton
        visible: false
      }
      PropertyChanges {
        target: secondaryButtonLabel
        visible: false
      }
      PropertyChanges {
         target: pulseLoader
         visible: true
      }
    },
    State {
      name: "noButton"
      when: layout.type === Pages.None
      PropertyChanges {
        target: primaryButton
        visible: false
      }
      PropertyChanges {
        target: secondaryButton
        visible: false
      }
      PropertyChanges {
        target: secondaryButtonLabel
        visible: false
      }
      PropertyChanges {
         target: pulseLoader
         visible: false
      }
    }
  ]
}

