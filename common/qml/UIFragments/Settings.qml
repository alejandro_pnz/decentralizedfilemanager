import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: root
  width: parent.width
  readonly property bool isMobile: app.isPhone
  property alias stackLayout: stackLayout
  spacing: ThemeController.style.margin.m16

  TabBar {
    id: bar
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.alignment: isMobile ? Qt.AlignHCenter : Qt.AlignLeft
    spacing: ThemeController.style.margin.m12
    StyledTabButton {
      text: Strings.global
    }
    StyledTabButton {
      text: Strings.user
    }
    StyledTabButton {
      text: Strings.device
    }
  }

  Rectangle {
    id: spacer
    Layout.fillWidth: true
    Layout.leftMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    Layout.rightMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    color: ThemeController.style.mercuryColor
    height: s(1)
  }

  StackLayout {
    id: stackLayout
    Layout.fillWidth: true
    Layout.fillHeight: true
    currentIndex: bar.currentIndex

    SettingsGlobal {
      id: settingsGlobalTab
    }

    SettingsUser {
      id: settingsUserTab
    }

    SettingsDevice {
      id: settingsDeviceTab
    }
  }
}
