import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  spacing: ThemeController.style.margin.m8

  Rectangle {
    Layout.preferredHeight: app.isMobile ? s(222) : s(230)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: "qrc:/illustrations/r11-personal-subsciption.png"
      mipmap: true
    }
  }

  UIKit.BaseText {
    text: Strings.subscriptionPlan
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    horizontalAlignment: Text.AlignHCenter
    Layout.alignment: Qt.AlignHCenter
    Layout.topMargin: ThemeController.style.margin.m8
  }

  UIKit.BaseText {
    text: Strings.personalPrice
    font.weight: Font.DemiBold
    horizontalAlignment: Text.AlignHCenter
    Layout.alignment: Qt.AlignHCenter
  }

  Column {
    id: column
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    Layout.alignment: Qt.AlignHCenter
    spacing: ThemeController.style.margin.m8

    UIKit.PaymentDelegate{
      id: applePay
      width: parent.width
      txt: Strings.applePay
      checked: true
      iconSrc: "qrc:/applepay-icon.svg"
    }

    UIKit.PaymentDelegate{
      id: creditCard
      width: parent.width
      txt: Strings.creditCard
      iconSrc: "qrc:/visa-icon.svg"
    }

    UIKit.PaymentDelegate{
      id: paypal
      width: parent.width
      txt: Strings.paypal
      iconSrc: "qrc:/paypal-icon.svg"
    }
  }

  ButtonGroup {
    id: buttonsGroup
    buttons: column.children
    exclusive: true
  }

  UIKit.StyledButton {
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    text: Strings.continuePayment
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    onClicked: RegistrationManager.payment()
  }

  Item {
    Layout.minimumHeight: ThemeController.style.margin.m16
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
