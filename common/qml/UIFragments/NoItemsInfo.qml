import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2
import QtQml.Models 2.12

import com.testintl 1.0

Item {
  id: contentItem
  property string title: Strings.noPinnedItems
  property string text: Strings.noPinnedItemsInfo
  property string iconSource: "qrc:/pin-image.svg"
  property int topMargin: s(app.isPhone ? 68 : 121)

  ColumnLayout {
    id: noItemsColumn
    anchors.fill: parent
    spacing: ThemeController.style.margin.m12

    Item {
      id: topMargin
      Layout.maximumHeight: contentItem.topMargin
      Layout.minimumHeight: 0
      Layout.fillHeight: true
      Layout.fillWidth: true
    }

    Rectangle {
      id: rect
      color: ThemeController.style.objectAvatar.bgColor
      radius: width * .5
      Layout.maximumHeight: s(app.isPhone ? 100 : 140)
      Layout.preferredHeight: s(app.isPhone ? 100 : 140)
      Layout.fillHeight: true
      height: s(app.isPhone ? 100 : 140)
      implicitWidth: height
      Layout.alignment: Qt.AlignHCenter
      visible: width > s(80)
      UIKit.SvgImage {
        id: pinImage
        source: contentItem.iconSource
        height: parent.height * (55/140)
        width: height
        anchors.centerIn: parent
      }
    }

    ColumnLayout {
      id: textColumn
      Layout.fillWidth: true
      spacing: ThemeController.style.margin.m12
      UIKit.BaseText {
        Layout.topMargin: ThemeController.style.margin.m4
        text: title
        font.weight: Font.Bold
        size: UIKit.BaseText.TextSize.H3
        Layout.alignment: Qt.AlignHCenter
        horizontalAlignment: Qt.AlignHCenter
        Layout.fillWidth: true
      }
      UIKit.BaseText {
        text: contentItem.text
        Layout.alignment: Qt.AlignHCenter
        Layout.preferredWidth: s(295)
        Layout.maximumWidth: s(295)
        Layout.fillWidth: true
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        horizontalAlignment: Qt.AlignHCenter
      }

      UIKit.StyledButton {
        Layout.alignment: Qt.AlignHCenter
        //width: s(100)
        //height: s(25)
        text: "Add new Arca"
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        onClicked: doneClicked()
      }
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }
  }
}
