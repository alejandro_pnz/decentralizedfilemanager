import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

import com.testintl 1.0

ColumnLayout {
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m24

  property bool isAO: AuthenticationManager.currentAccount.accountOwner

  UIKit.BaseText {
    text: Strings.devices
    visible: !app.isPhone
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
  }

  Flickable {
    id: flick
    Layout.fillHeight: true
    Layout.fillWidth: true
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: content.height
    clip: true

    ColumnLayout {
      id: content
      spacing: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
      width: parent.width

      UIKit.PickerDelegate {
        labelText: Strings.maxNumberOfDevices
        pickerTitle: Strings.maxNumberOfDevices
        inputRequired: false
        placeholderTxt: Strings.userChoice
        leftEyeIcon: false
        objectsModel: ["1", "2", "3", "4", Strings.noLimit]
        enabled: isAO
      }

      Rectangle {
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
      }

      UIKit.BaseText {
        text: Strings.makeDeviceInactive
        font.weight: Font.Bold
      }

      ButtonGroup {
        id: deviceInactiveGroup
        buttons: deviceInactive.children
        exclusive: true
      }

      Column {
        id: deviceInactive
        spacing: ThemeController.style.margin.m24
        Layout.topMargin: app.isPhone ? ThemeController.style.margin.m8 : 0

        UIKit.StyledRadioButton {
          checked: true
          text: Strings.daysSinceLastLogin.arg("14")
          enabled: isAO
        }

        UIKit.StyledRadioButton {
          text: Strings.daysSinceLastLogin.arg("30")
          enabled: isAO
        }

        UIKit.StyledRadioButton {
          text: Strings.daysSinceLastLogin.arg("60")
          enabled: isAO
        }

        UIKit.StyledRadioButton {
          text: Strings.never
          enabled: isAO
        }
      }

      Rectangle {
        Layout.topMargin: ThemeController.style.margin.m12
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        height: s(1)
      }

      Row {
        spacing: ThemeController.style.margin.m8
        UIKit.BaseText {
          text: Strings.deviceToDeviceSync
          font.weight: Font.DemiBold
        }

        UIKit.BigTipButton {
          tipTitle: Strings.deviceToDeviceSync
          tipContent: Strings.thisSettingRegulates
          showDontShowAgain: false
          showPlaceholder: false
          popupTopSpacing: ThemeController.style.margin.m20
        }
      }

      ButtonGroup {
        id: syncGroup
        buttons: deviceSync.children
        exclusive: true
      }

      Column {
        id: deviceSync
        spacing: ThemeController.style.margin.m24
        Layout.topMargin: app.isPhone ? ThemeController.style.margin.m12 : 0

        UIKit.StyledRadioButton {
          checked: true
          text: Strings.always
          enabled: isAO
        }

        UIKit.StyledRadioButton {
          text: Strings.wheneverPossible
          enabled: isAO
        }

        UIKit.StyledRadioButton {
          text: Strings.never
          enabled: isAO
        }
      }
    }
  }
}
