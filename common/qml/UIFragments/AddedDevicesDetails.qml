import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import com.test 1.0


AbstractButton {
    id: details
    width: parent.width
    property int myDevicesCount: DeviceListModel.myDevicesCount
    property int otherDevicesCount: RegistrationManager.accountOwner ? DeviceListModel.otherDevicesCount : 0
    property int allowedDevicesCount: DeviceListModel.allowedDevicesCount
    property var config: ThemeController.style.objectDelegate

    background: Item {
        Rectangle {
            id : bgRect
            anchors.fill: parent
            color: config.bgColor
            radius: s(4)
            border {
                color: config.bdColor
                width: s(1)
            }
            visible: !dropShadow.visible
        }

        DropShadow {
            id: dropShadow
            anchors.fill: bgRect
            source: bgRect
            color: config.dropShadowColor04
            radius: s(10)
            verticalOffset: s(2)
            transparentBorder: true
        }
    }

    topPadding : ThemeController.style.margin.m16
    bottomPadding: ThemeController.style.margin.m24
    rightPadding: ThemeController.style.margin.m24
    leftPadding : ThemeController.style.margin.m24

    contentItem: ColumnLayout {
        spacing: ThemeController.style.margin.m8
        RowLayout {
            UIKit.BaseText{
                text: Strings.devicesAdded
                font.weight: Font.DemiBold
            }
            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: s(24)
            }
            UIKit.BaseText{
                text: Strings.of.arg(myDevicesCount + otherDevicesCount).arg(allowedDevicesCount)
            }
        }
        Rectangle {
            Layout.fillWidth: true
            height: s(4)
            radius: s(2)
            color: ThemeController.style.mercuryColor
            Rectangle {
                id: myDevices
                anchors.left: parent.left
                width: (parent.width / allowedDevicesCount) * myDevicesCount
                height: s(4)
                radius: s(2)
                color: ThemeController.style.slatePurpleColor
            }
            Rectangle {
                id: otherDevices
                anchors.left: myDevices.right
                width: (parent.width / allowedDevicesCount) * otherDevicesCount + s(3)
                anchors.leftMargin: -s(3)
                height: s(4)
                radius: s(2)
                color: ThemeController.style.manateeColor
            }
        }
        RowLayout {
            spacing: s(6)
            visible: RegistrationManager.accountOwner
            UIKit.StatusItem {
                colorRectangle.color: ThemeController.style.slatePurpleColor
                Layout.alignment: Qt.AlignVCenter
            }
            UIKit.BaseText {
                Layout.alignment: Qt.AlignVCenter
                text: Strings.myDevicesStatus.arg(myDevicesCount)
                size: UIKit.BaseText.TextSize.Small
            }
            UIKit.StatusItem {
                Layout.leftMargin: ThemeController.style.margin.m16
                colorRectangle.color: ThemeController.style.manateeColor
                Layout.alignment: Qt.AlignVCenter
            }
            UIKit.BaseText {
                Layout.alignment: Qt.AlignVCenter
                text: Strings.otherDevicesStatus.arg(otherDevicesCount)
                size: UIKit.BaseText.TextSize.Small
            }
            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: s(16)
            }
        }
    }
}
