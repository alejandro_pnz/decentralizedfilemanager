import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2

ColumnLayout {
  id: content
  anchors.fill: parent
  spacing: ThemeController.style.margin.m16

  readonly property var currentEditor: homeManager.currentEditor

  signal editTag(var object)

  function addTag() {
    var newTag = TagListModel.addTag(tag.text, tag.tagColor)
    currentEditor.taglistModel.addTag(newTag)
    search.clear()
  }

  UIKit.SearchInput {
    id: search
    Layout.topMargin: ThemeController.style.homePageVerticalMargin
    Layout.fillWidth: true
    placeholderText: Strings.searchOrAddTags
    Layout.preferredHeight: ThemeController.style.defaultInputHeight
    onAccepted: {
      // If it is possible to add a new tag, add it
      if (createNewTagItem.visible) {
        addTag()
      }
    }
  }

  Flow {
    id: flow
    Layout.fillWidth: true
    clip: true
    spacing: ThemeController.style.margin.m8
    visible: repeater.count > 0

    Repeater {
      id: repeater
      model: currentEditor.taglistModel
      delegate: UIKit.Tag {
        deleteButtonVisible: true
        text: name
        tagColor: color
        onDeleteButtonClicked: currentEditor.taglistModel.deleteTag(id)
      }
    }
  }

  UIKit.BaseText {
    Layout.topMargin: app.isPhone ? 0 : s(8)
    text: Strings.selectTag
    font.weight: Font.Bold
  }

  Item {
    id: createNewTagItem
    Layout.fillWidth: true
    Layout.preferredHeight: s(56)
    visible: search.searchText !== ""
             && !TagListModel.tagNameExists(search.searchText)

    RowLayout {
      id: createTagRow
      spacing: ThemeController.style.margin.m12
      anchors.verticalCenter: parent.verticalCenter

      UIKit.BaseText {
        text: Strings.create
        font.weight: Font.Bold
        color: ThemeController.style.slatePurpleColor
        MouseArea {
          anchors.fill: parent
          onClicked: addTag()
        }
      }

      UIKit.Tag {
        id: tag
        text: search.searchText
        tagColor: ThemeController.style.slatePurpleColor

        onClicked: addTag()
      }

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
      }
    }

    Rectangle {
      anchors.bottom: parent.bottom
      height: s(1)
      width: parent.width
      color: ThemeController.style.seashellColor
    }
  }

  ListView {
    id: listView
    Layout.topMargin: s(createNewTagItem.visible ? -16 : 0)
    Layout.fillWidth: true
    Layout.preferredHeight: listView.contentHeight
    spacing: 0

    model: proxyModel
    delegate: UIKit.TagDelegate {
      width: listView.width
      tagText: model.name
      tagColor: model.color
      iconButton.onClicked: {
        if (app.isPhone)
          homeManager.enterEditTagPage(model.object)
        else
          editTag(model.object)
      }
      onClicked: {
        currentEditor.taglistModel.addTag(model.object)
      }
    }
  }

  Item{
    Layout.fillHeight: true
    Layout.fillWidth: true
  }

  SortFilterProxyModel {
    id: proxyModel
    sourceModel: TagListModel
    filters: [
      RegExpFilter {
        roleName: "name"
        pattern: search.searchText
        caseSensitivity: Qt.CaseInsensitive
      },
      ListFilter {
        roleName: "id"
        list: currentEditor.taglistModel.idList
        inverted: true
      }
    ]
  }
}
