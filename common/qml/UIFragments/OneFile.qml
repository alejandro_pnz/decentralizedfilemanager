import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: root
  width: parent.width
  readonly property bool isMobile: app.isPhone
  property alias stackLayout: stackLayout
  property bool showFooter: stackLayout.currentIndex === 1
  spacing: ThemeController.style.margin.m16

  ObjectDelegate {
    id: fileDelegate
    Layout.topMargin: ThemeController.style.homePageVerticalMargin
    Layout.fillWidth: true
    Layout.preferredHeight: s(103)
    Layout.alignment: Qt.AlignHCenter

    background: Rectangle{
      anchors.fill: parent
    }
    topPadding: 0
    leftPadding: 0
    rightPadding: 0
    bottomPadding: 0
    avatar.size: ObjectAvatar.Size.Big
    avatar.shareMode: fileEditor.object.syncProperty
    avatar.iconSrc: "image://item/type=" + SyncableObject.File + "&id=" + fileEditor.object.id
    dropShadow.visible: false
    titleIcon: fileEditor.object.locked
    checkable: false
    title: fileEditor.object.name
    titleComponent.color: ThemeController.style.font.defaultColor
    titleComponent.size: BaseText.TextSize.H3
    text: Utility.formatFileContent(fileEditor.object.id)
    onTextLinkActivated: {
      homeManager.openOneArcaPage(link)
    }
  }

  TabBar {
    id: bar
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.alignment: isMobile ? Qt.AlignHCenter : Qt.AlignLeft
    spacing: ThemeController.style.margin.m12

    StyledTabButton {
      icon.source: "qrc:/preview.svg"
      text: Strings.preview
    }
    StyledTabButton {
      id: propertiesButton
      icon.source: "qrc:/properties.svg"
      text: Strings.properties
    }
  }

  Rectangle {
    id: spacer
    Layout.topMargin: isMobile ? ThemeController.style.margin.m8 : 0
    Layout.fillWidth: true
    Layout.leftMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    Layout.rightMargin: isMobile ? -ThemeController.style.margin.m16 : 0
    color: ThemeController.style.mercuryColor
    height: s(1)
  }

  StackLayout {
    id: stackLayout
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    Layout.fillHeight: true
    currentIndex: bar.currentIndex

    FileViewerTab {
      id: fileViewerTab
    }

    PropertiesTab {
      id: propertiesTab
    }
  }
}
