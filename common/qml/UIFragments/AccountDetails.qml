import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

import com.testintl 1.0

ColumnLayout {
  id: content
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m16
  property bool showFooter: true
  property bool isAO: AuthenticationManager.currentAccount.accountOwner

  property string accountName: AuthenticationManager.currentAccount.name
  property string accountEmail: AuthenticationManager.currentAccount.accountEmail

  UIKit.BaseText {
    text: Strings.account
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    visible: !app.isMobile
  }

  UIKit.InputField {
    id: nameInput
    Layout.topMargin: ThemeController.style.margin.m16
    labelText: Strings.accountName
    placeholderTxt: {
      if (accountName.length > 0) {
        return accountName;
      }
      return Strings.typeAccountName;
    }
    inputRequired: false
    clearIconVisible: true
    enabled: isAO
    onTabPressed: emailInput.textField.forceActiveFocus()
    onEnterPressed: emailInput.textField.forceActiveFocus()
    onBacktabPressed: emailInput.textField.forceActiveFocus()
  }

  UIKit.InputField {
    id: emailInput
    labelText: Strings.accountEmail
    placeholderTxt: {
      if (accountEmail.length > 0) {
        return accountEmail;
      }
      return Strings.typeAccountEmail;
    }
    inputRequired: true
    clearIconVisible: true
    enabled: isAO
    fieldType: UIKit.InputField.FieldType.Email
    onTabPressed: nameInput.textField.forceActiveFocus()
    onEnterPressed: nameInput.textField.forceActiveFocus()
    onBacktabPressed: nameInput.textField.forceActiveFocus()
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
