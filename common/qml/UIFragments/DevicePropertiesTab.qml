import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

Flickable {
  id: flickable
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true
  readonly property bool isMobile: app.isPhone
  //properties for tests
  property var deviceObject: deviceEditor.object
  property string email: deviceObject.email
  property string lastLocation: qsTr("%1 \u2022 %2, %3").arg(deviceObject.ipAddress).arg(deviceObject.city).arg(deviceObject.countryCode)
  property string addedDate: Utility.formatDateTime(deviceObject.created)
  property string device: qsTr("%1").arg(deviceObject.deviceModel) //"Apple \u2022 iPhone XS"

  ColumnLayout {
    id: content
    spacing: ThemeController.style.margin.m16
    width: parent.width

    ActivityPanel {
      Layout.fillWidth: true
      active: deviceObject.active
      blocked: deviceObject.blocked
      enabled: true
    }

    Rectangle {
      Layout.topMargin: isMobile ? 0 : s(8)
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
    }

    UIKit.BaseText {
      Layout.topMargin: isMobile ? 0 : s(8)
      font.weight: Font.Bold
      text: Strings.details
    }

    GridLayout {
      id: detailsLayout
      rows: app.isMobile ? 4 : 2

      Layout.fillWidth: true
      Layout.bottomMargin: ThemeController.style.margin.m16
      Layout.preferredHeight: childrenRect.height
      columnSpacing: ThemeController.style.margin.m32
      rowSpacing: ThemeController.style.margin.m24
      flow: GridLayout.TopToBottom

      Item {
        Layout.preferredHeight: s(60)
        Layout.fillWidth: true

        UIKit.BaseText {
          id: emailLabel
          anchors.top: parent.top
          text: Strings.deviceEmail
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          anchors.top: emailLabel.bottom
          anchors.topMargin: ThemeController.style.margin.m12
          text: email
          font.weight: Font.DemiBold
        }
      }

      Item {
        Layout.preferredHeight: s(60)
        Layout.fillWidth: true

        UIKit.BaseText {
          anchors.top: parent.top
          id: modelLabel
          text: Strings.makeAndModel
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          anchors.top: modelLabel.bottom
          anchors.topMargin: ThemeController.style.margin.m12
          text: device
          font.weight: Font.DemiBold
        }
      }

      Item {
        Layout.preferredHeight: s(60)
        Layout.fillWidth: true

        UIKit.BaseText {
          anchors.top: parent.top
          id: locationLabel
          text: Strings.lastIPlocation
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          anchors.top: locationLabel.bottom
          anchors.topMargin: ThemeController.style.margin.m12
          text: lastLocation
          font.weight: Font.DemiBold
        }
      }

      Item {
        Layout.preferredHeight: s(60)
        Layout.fillWidth: true

        UIKit.BaseText {
          id: addedLabel
          anchors.top: parent.top
          text: Strings.addedOn
          color: ThemeController.style.shuttleColor
        }

        UIKit.BaseText {
          anchors.top: addedLabel.bottom
          anchors.topMargin: ThemeController.style.margin.m12
          text: addedDate
          font.weight: Font.DemiBold
        }
      }
    }
  }
}
