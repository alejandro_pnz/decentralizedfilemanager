import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

Flickable {
  id: userInfo
  width: parent.width
  height: parent.height
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true

  property var object: null

  Connections {
    target: homeManager
    function onSetupInfoPage(id, type) {
      // TODO: use SDK api
      console.log(id, type);
      object = UserListModel.qmlObject(id)
    }
  }

  onObjectChanged: {
    infoModel.clear()

    if (object) {
      infoModel.append({ label: Strings.accountOwnerName, contentTxt: "-"});
      infoModel.append({ label: Strings.accountOwnerEmail, contentTxt: ""});
      infoModel.append({ label: Strings.username, contentTxt: object.name});
      infoModel.append({ label: Strings.userEmail, contentTxt: object.email});
      infoModel.append({ label: Strings.addedOn, contentTxt: Utility.formatDateTime(object.created)});
    }
  }

  ScrollBar.vertical: ScrollBar {
    width: ThemeController.style.margin.m8
    visible: !isMobile
  }

  ColumnLayout {
    id: content
    width: parent.width
    spacing: ThemeController.style.margin.m24

    UIKit.BaseText {
      Layout.topMargin: ThemeController.style.margin.m16
      text: Strings.icon
      color: ThemeController.style.shuttleColor
    }

    UIKit.Avatar {
      id: iconContainer
      Layout.topMargin: -ThemeController.style.margin.m12
      text: object ? object.email : ""
    }

    Repeater {
      id: repeater
      model: infoModel
      delegate: UIKit.InfoItem {
        labelText: label
        content: contentTxt
      }
    }
  }

  ListModel {
    id: infoModel
  }
}
