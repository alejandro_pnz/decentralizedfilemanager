import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

import com.testintl 1.0

ColumnLayout {
  id: content
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m16
  property bool showFooter: true

  property string deviceEmail: AuthenticationManager.currentAccount.email

  QuodArcaSettings {
    id: deviceSettings
    preferencesType: SettingsManager.Device

    property string language
  }

  UIKit.BaseText {
    text: Strings.device
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    visible: !app.isPhone
  }

  UIKit.InputField {
    id: nameInput
    Layout.topMargin: app.isPhone ? 0 : ThemeController.style.margin.m8
    labelText: Strings.name
    placeholderTxt: Strings.typeDeviceName
    inputRequired: false
    clearIconVisible: true
    onTabPressed: aboutInput.textField.forceActiveFocus()
    onEnterPressed: aboutInput.textField.forceActiveFocus()
    onBacktabPressed: pickerDelegate.comboBox.forceActiveFocus()
  }

  UIKit.InputField {
    id: aboutInput
    labelText: Strings.about
    placeholderTxt: Strings.deviceDescription
    inputRequired: false
    clearIconVisible: true
    onTabPressed: emailInput.textField.forceActiveFocus()
    onEnterPressed: emailInput.textField.forceActiveFocus()
    onBacktabPressed: nameInput.textField.forceActiveFocus()
  }

  UIKit.InputField {
    id: emailInput
    labelText: Strings.email
    placeholderTxt: {
      if (deviceEmail.length > 0) {
        return deviceEmail;
      }
      return Strings.typeEmail;
    }
    inputRequired: false
    clearIconVisible: true
    fieldType: UIKit.InputField.FieldType.Email
    onBacktabPressed: aboutInput.textField.forceActiveFocus()
    onTabPressed: pickerDelegate.openPicker()
    onEnterPressed: pickerDelegate.openPicker()
  }

  UIKit.PickerDelegate {
    id: pickerDelegate
    labelText: Strings.language
    pickerTitle: Strings.language
    inputRequired: false
    leftEyeIcon: false
    objectsModel: [
      { text: Strings.system, value: "system" },
      { text: Strings.english, value: "en" },
      { text: Strings.french, value: "fr" },
      { text: Strings.deutsch, value: "de" },
      { text: Strings.espanol, value: "es" },
      { text: Strings.italian, value: "it" }
    ]
    onTabPressed: nameInput.textField.forceActiveFocus()
    onBacktabPressed: emailInput.textField.forceActiveFocus()
    comboBox.textRole: "text"
    comboBox.valueRole: "value"
    comboBox.currentIndex: comboBox.indexOfValue(deviceSettings.language)
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
