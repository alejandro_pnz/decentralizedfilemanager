import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0

StackLayout {
  id: stackLayout
  property alias url: webview.url

  signal webViewLoadingStatusChanged(var status)
  signal sendConsoleMessage(string message)

  Connections {
    target: pageManager
    function onSetupWebViewLinkPage(config) {
      webView.url = config.link
    }
  }

  currentIndex: 0

  Item {
    ColumnLayout {
      anchors.centerIn: parent
      spacing: 17

      UIKit.PulseLoader {
        id: pulseLoader
        height: s(30)
        barCount: 8
        Layout.fillWidth: true
        color: ThemeController.style.slatePurpleColor
      }

      UIKit.BaseText {
        text: Strings.loading
        font.weight: Font.DemiBold
        size: UIKit.BaseText.TextSize.H3
      }
    }
  }
  Item {
    QuodArcaWebView  {
      id: webview
      anchors.fill: parent
      //settings.showScrollBars: false
      onLoadingChanged: function(loadingInfo) {
        if(loadingInfo.status === QuodArcaWebView.LoadSucceededStatus) {
          stackLayout.currentIndex = 1
        }
        else if(loadingInfo.status === QuodArcaWebView.LoadFailedStatus) {
          stackLayout.currentIndex = 2
        }
        webViewLoadingStatusChanged(loadingInfo.status)
      }
      onJavaScriptConsoleMessage: function(message) {
        console.log("QuodArcaWebView JavaScriptConsoleMessage received:  ", message)
        sendConsoleMessage(message)
      }
    }
  }

  //Item to show for "no internet"
  Item {
    NoInternetConnection {
      width: s(384)
      height: s(486)
      anchors.centerIn: parent
      onTryAgainClicked: {
        stackLayout.currentIndex = 0
        webview.reload();
      }
    }
  }
}

