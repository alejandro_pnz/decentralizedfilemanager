import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

import com.testintl 1.0

Flickable {
  property bool advanced: true
  property string mainText: Strings.emailWithLink
  width: parent.width
  height: parent.height - (advanced ? 0 : s(24))
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: layout.height
  clip: true

  ColumnLayout {
    id: layout
    spacing: ThemeController.style.margin.m12
    width: parent.width

    property bool isMobile: app.isMobile && !app.isTablet

    Rectangle {
      Layout.preferredHeight: s(isMobile ? 222 : 230)
      Layout.fillWidth: true
      radius: s(10)
      Layout.alignment: Qt.AlignHCenter

      Image {
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        source: "qrc:/illustrations/u2.png"
        mipmap: true
      }
    }

    UIKit.BaseText {
      Layout.topMargin: s(isMobile ? 28 : 12)
      text: Strings.checkYourEmail
      alternativeFont: true
      font.weight: Font.Bold
      size: isMobile ? UIKit.BaseText.TextSize.H2 : UIKit.BaseText.TextSize.H3
      Layout.alignment: Qt.AlignHCenter
    }

    UIKit.BaseText {
      Layout.topMargin: isMobile ? ThemeController.style.margin.m4 : 0
      Layout.fillWidth: true
      text: mainText
      horizontalAlignment: Text.AlignHCenter
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      Layout.alignment: Qt.AlignHCenter
    }

    UIKit.StyledButton {
      id: openMailAppButton
      Layout.topMargin: ThemeController.style.margin.m20
      Layout.fillWidth: true
      text: Strings.openMailApp
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      visible: advanced
    }

    UIKit.BaseText {
      Layout.topMargin: ThemeController.style.margin.m12
      Layout.alignment: Qt.AlignHCenter
      text: Strings.didntReceiveEmail
      font.weight: Font.DemiBold
      visible: advanced
    }

    UIKit.StyledButton {
      Layout.fillWidth: true
      text: Strings.sendAgain
      type: UIKit.StyledButton.ButtonStyle.Secondary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      visible: advanced
    }
  }
}
