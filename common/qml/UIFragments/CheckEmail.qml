import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

import com.testintl 1.0

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m16

  property string email: "helenamorrison@email.com"

  property bool busy: false

  Connections {
    target: pageManager
    function onSetupCheckEmailPage(config) {
      if (config.email !== undefined) {
        layout.email = config.email
      }
    }
  }

  Connections {
    target: AuthenticationManager
    function onCheckEmailTokenReceived(token) {
      RegistrationManager.verifyEmail(token)
    }
  }

  Rectangle {
    Layout.preferredHeight: s(app.isMobile ? 222 : 230)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: "qrc:/illustrations/r20_26.png"
      mipmap: true
    }
  }

  UIKit.BaseText {
    Layout.topMargin: ThemeController.style.margin.m24
    text: Strings.checkYourEmail
    alternativeFont: true
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.BaseText {
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    text: Strings.weJustSentEmail.arg("<font color=\"%4\"><b>").arg(email)
            .arg("</b></font>").arg(ThemeController.style.slatePurpleColor)
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.BaseText {
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.alignment: Qt.AlignHCenter
    text: Strings.didntReceiveEmail
    font.weight: Font.DemiBold
    visible: !layout.busy
  }

  UIKit.StyledButton {
    id: openMailAppButton
    Layout.fillWidth: true
    text: Strings.sendAgain
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    visible: !layout.busy
  }

  UIKit.PulseLoader {
    Layout.alignment: Qt.AlignHCenter
    color: ThemeController.style.button.primary.bgColor
    visible: layout.busy
    height: openMailAppButton.height
    barCount: 10
    spacing: s(5)
    width: openMailAppButton.width / 2
  }
}
