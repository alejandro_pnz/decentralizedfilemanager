import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: content
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m16
  property bool showFooter: true

  property int mode: Passphrase.Mode.LoginPassphrase
  enum Mode { LoginPassphrase, PowerPassphrase }

  UIKit.BaseText {
    text: mode === Passphrase.Mode.LoginPassphrase ?
            Strings.loginPassphrase : Strings.powerPassphrase
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H2
    visible: !app.isMobile
  }

  UIKit.InfoDelegate {
    contentText: mode === Passphrase.Mode.LoginPassphrase ?
                 Strings.allowsChangeLoginPassphrase : Strings.allowsChangePowerPassphrase
    Layout.topMargin: app.isMobile ? 0 : s(8)
  }

  UIKit.InputField {
    id: currentPassphrase
    labelText: mode === Passphrase.Mode.LoginPassphrase ?
               Strings.currentPassphrase : Strings.currentPowerPassphrase
    placeholderTxt: mode === Passphrase.Mode.LoginPassphrase ?
                    Strings.typeCurrentPassphrase : Strings.typeCurrentPowerPassphrase
    inputRequired: false
    eyeIconVisible: true
    clearIconVisible: false
    isPassPhrase: true
    Layout.topMargin: app.isMobile ? 0 : s(8)
    onTabPressed: newPassphrase.textField.forceActiveFocus()
    onEnterPressed: newPassphrase.textField.forceActiveFocus()
    onBacktabPressed: repeatPassphrase.textField.forceActiveFocus()
  }

  UIKit.TipsInput {
    id: newPassphrase
    labelText: mode === Passphrase.Mode.LoginPassphrase ?
               Strings.newPassphrase : Strings.newPowerPassphrase
    placeholderTxt: mode === Passphrase.Mode.LoginPassphrase ?
                    Strings.typeNewPassphrase : Strings.typeNewPowerPassphrase
    inputRequired: false
    eyeIconVisible: true
    clearIconVisible: false
    isPassPhrase: true
    onTabPressed: repeatPassphrase.textField.forceActiveFocus()
    onEnterPressed: repeatPassphrase.textField.forceActiveFocus()
    onBacktabPressed: currentPassphrase.textField.forceActiveFocus()
  }

  UIKit.InputField {
    id: repeatPassphrase
    labelText: mode === Passphrase.Mode.LoginPassphrase ?
               Strings.repeatNewPassphrase : Strings.repeatNewPowerPassphrase
    placeholderTxt: mode === Passphrase.Mode.LoginPassphrase ?
                    Strings.typeNewPassphrase : Strings.typeNewPowerPassphrase
    inputRequired: false
    eyeIconVisible: true
    clearIconVisible: false
    isPassPhrase: true
    onTabPressed: currentPassphrase.textField.forceActiveFocus()
    onEnterPressed: currentPassphrase.textField.forceActiveFocus()
    onBacktabPressed: newPassphrase.textField.forceActiveFocus()
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
