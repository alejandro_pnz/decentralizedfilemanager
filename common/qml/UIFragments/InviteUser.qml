import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: content
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m16

  property string email: emailField.textField.text

  UIKit.InputField {
    id: emailField
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
    labelText: Strings.email
    placeholderTxt: Strings.typeUserEmail
    inputRequired: false
    eyeIconVisible: false
    clearIconVisible: true
    isPassPhrase: false
    fieldType: UIKit.InputField.FieldType.Email
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
