import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import QtPromise 1.1

Flickable {
  Layout.fillHeight: true
  Layout.fillWidth: true
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true

  ColumnLayout {
    id: content
    spacing: 0
    width: parent.width

    UIKit.ActionListDelegate {
      text: Strings.myAccount
      icon.source: "qrc:/account-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      separator: true
      leftPadding: 0
      rightPadding: 0
      actionIcon: true
      onClicked: homeManager.openPage(Pages.MyAccountPage)
    }

    UIKit.ActionListDelegate {
      text: Strings.recycleBin
      icon.source: "qrc:/bin-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      separator: true
      leftPadding: 0
      rightPadding: 0
      onClicked: homeManager.openPage(Pages.RecycleBinPage)

      Rectangle {
        id: numberContainer
        height: s(24)
        width: s(51)
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        color: ThemeController.style.mercuryColor
        radius: s(100)
        visible: RecycleBinModel.count > 0

        UIKit.BaseText {
          id: numberTxt
          anchors.centerIn: parent
          text: {
            if (RecycleBinModel.count >= 100) {
              return "99+"
            }
            if (RecycleBinModel.count >= 1000) {
              return "999+";
            }
            return RecycleBinModel.count
          }
        }
      }
    }

    UIKit.ActionListDelegate {
      text: Strings.settings
      icon.source: "qrc:/cog-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      separator: true
      leftPadding: 0
      rightPadding: 0
      onClicked: homeManager.openPage(Pages.SettingsPage)
    }

    UIKit.SidebarSectionLabel{
      txt: Strings.tools
      Layout.topMargin: ThemeController.style.margin.m24
    }

    Rectangle {
      height: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.mercuryColor
      Layout.topMargin: ThemeController.style.margin.m12
    }

    UIKit.ActionListDelegate {
      Layout.topMargin: ThemeController.style.margin.m16
      text: Strings.passwordGenerator
      icon.source: "qrc:/password-generator-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      separator: true
      leftPadding: 0
      rightPadding: 0
      onClicked: homeManager.openPage(Pages.PasswordGeneratorPage)
    }

    UIKit.ActionListDelegate {
      Layout.topMargin: ThemeController.style.margin.m16
      text: Strings.markdownEditor
      icon.source: "qrc:/markdown-editor-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      separator: true
      leftPadding: 0
      rightPadding: 0
      onClicked: homeManager.enterCreateFile()
    }

    UIKit.ActionListDelegate {
      Layout.topMargin: ThemeController.style.margin.m16
      text: Strings.importStr
      icon.source: "qrc:/import-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      separator: true
      leftPadding: 0
      rightPadding: 0
    }

    UIKit.ActionListDelegate {
      Layout.topMargin: ThemeController.style.margin.m16
      text: Strings.exportStr
      icon.source: "qrc:/export-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      separator: true
      leftPadding: 0
      rightPadding: 0
    }

    UIKit.SidebarSectionLabel {
      txt: Strings.support
      Layout.topMargin: ThemeController.style.margin.m24
    }

    Rectangle {
      height: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.mercuryColor
      Layout.topMargin: ThemeController.style.margin.m12
    }

    UIKit.ActionListDelegate {
      text: Strings.helpResources
      icon.source: "qrc:/help-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      separator: true
      leftPadding: 0
      rightPadding: 0
    }

    UIKit.ActionListDelegate {
      text: Strings.contactSupport
      icon.source: "qrc:/support-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      separator: true
      leftPadding: 0
      rightPadding: 0
    }

    UIKit.ActionListDelegate {
      text: Strings.aboutQuodArca
      icon.source: "qrc:/info-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      separator: false
      leftPadding: 0
      rightPadding: 0
      onClicked: homeManager.openPage(Pages.AboutPopupPage)
    }

    UIKit.ActionListDelegate {
      text: "Logs"
      icon.source: "qrc:/info-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      separator: false
      leftPadding: 0
      rightPadding: 0
      onClicked: homeManager.openPage(Pages.LogsPage)
    }

    Rectangle {
      height: s(1)
      Layout.fillWidth: true
      Layout.topMargin: ThemeController.style.margin.m16
      Layout.leftMargin: -horizontalMargins
      Layout.rightMargin: -horizontalMargins
      color: ThemeController.style.mercuryColor
    }

    UIKit.ActionListDelegate {
      Layout.topMargin: ThemeController.style.margin.m16
      Layout.bottomMargin: s(26)
      text: Strings.logout
      icon.source: "qrc:/sign-out-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: false
      separator: false
      leftPadding: 0
      rightPadding: 0
      onClicked: {
        var future = Future.promise(AuthenticationManager.logout())

        future.then(function() {
          pageManager.backToLogin()
        },
        function(error) {
          console.log(error)
        })
      }
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }
  }
}
