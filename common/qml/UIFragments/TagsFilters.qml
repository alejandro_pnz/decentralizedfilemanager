import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2

ColumnLayout {
  id: content
  anchors.fill: parent
  spacing: ThemeController.style.margin.m16
  property var searchModel

  property bool accepted: false
  property var selectionModel: null

  // Signal to store the current selection state before user do some changes
  function init() {
    searchModel.filters.filter(SFilter.TagFilter).init()
    selectionModel = searchModel.filters.filter(SFilter.TagFilter).selectedTagsModel
  }

  // Reverts all changes made by user
  function revertChanges() {
    searchModel.filters.filter(SFilter.TagFilter).revertChanges()
    cleanup()
  }

  // Clears the selection
  function clearAll() {
    searchModel.filters.filter(SFilter.TagFilter).selectedTagsModel.clearSelection()
  }

  // Applies filtering
  function applyFilters() {
    accepted = true
    searchModel.updateFilters();
    cleanup()
  }

  function cleanup() {
    search.text = ""
  }

  UIKit.SearchInput {
    id: search
    Layout.topMargin: app.isPhone ? ThemeController.style.homePageVerticalMargin : 0
    Layout.fillWidth: true
    placeholderText: Strings.searchTags
    Layout.preferredHeight: s(48)
    micIconVisible: false
  }

  ListView {
    id: listView
    Layout.topMargin: app.isPhone ? 0 : -ThemeController.style.margin.m8
    Layout.fillWidth: true
    Layout.preferredHeight: listView.contentHeight
    spacing: 0

    model: proxyModel
    delegate: UIKit.TagDelegate {
      selectionModel: content.selectionModel
      delegateModel: proxyModel
      width: listView.width
      tagText: object.name
      tagColor: object.color
      selectionMode: true
      separatorVisible: app.isPhone
      index: model.index
    }
  }

  Item{
    Layout.fillHeight: true
    Layout.fillWidth: true
  }

  SortFilterProxyModel {
    id: proxyModel
    sourceModel: TagListModel
    filters: [
      RegExpFilter {
        roleName: "name"
        pattern: search.searchText
        caseSensitivity: Qt.CaseInsensitive
      }
    ]
  }
}
