import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

Flickable {
  id: flickable
  Layout.fillHeight: true
  Layout.fillWidth: true
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: content.height
  clip: true
  readonly property bool isMobile: app.isPhone

  property var userObject: userEditor.object

  property string email: userObject.email
  property string name: userObject.name
  property string accountId: "23395812"
  property string nickname: userObject.nickname
  property string association: userObject.guest ? Strings.guest : Strings.member

  ColumnLayout {
    id: content
    spacing: ThemeController.style.margin.m16
    width: parent.width

    UIKit.BaseText {
      Layout.topMargin: isMobile ? 0 : s(8)
      font.weight: Font.Bold
      text: Strings.powerPriviliges
    }

    // TODO:
    UIKit.LockObjectPanel {
      titleText: Strings.powerUser
      contentText: Strings.enableThisUser
      icon.source: "qrc:/power.svg"
      icon.color: ThemeController.style.charadeColor
      checked: userObject.hasPower
      onSwitchButtonToggled: openGivePowerPopup()
      onCheckedChanged: {
        if (!checked) userEditor.switchPowerState();
        checked = userObject.hasPower
      }
    }

    Rectangle {
      Layout.topMargin: isMobile ? 0 : ThemeController.style.margin.m8
      Layout.preferredHeight: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
    }

    UIKit.BaseText {
      text: Strings.details
      font.weight: Font.Bold
    }

    Flow {
      Layout.fillWidth: true
      Layout.preferredHeight: isMobile ? undefined : s(312)
      flow: Flow.TopToBottom
      spacing: ThemeController.style.margin.m24

      UIKit.InfoItem {
        width: s(394)
        labelText: Strings.account
        content: email
      }

      UIKit.InfoItem {
        width: s(394)
        labelText: Strings.accountId
        content: accountId
      }

      UIKit.InfoItem {
        width: s(394)
        labelText: Strings.email
        content: email
      }

      UIKit.InfoItem {
        width: s(394)
        labelText: Strings.name
        content: name
      }

      UIKit.InfoItem {
        width: s(394)
        labelText: Strings.nickname
        content: nickname
      }

      UIKit.InfoItem {
        width: s(394)
        labelText: Strings.association
        content: association
      }

      UIKit.InfoItem {
        width: s(394)
        labelText: Strings.addedOn
        content: Utility.formatDateTime(userObject.created)
      }
    }

    Item {
      Layout.fillWidth: true
      Layout.preferredHeight: isMobile ? s(1) : s(66)
    }
  }
}
