import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

ColumnLayout {
  id: boardsList
  spacing: ThemeController.style.margin.m12

  UIKit.CardDelegate {
    title: Strings.userDashboard
    text: Strings.seeAndManageUsers
    icon.source: "qrc:/users.svg"
    Layout.preferredHeight: s(80)
    Layout.fillWidth: true
    onClicked: homeManager.openPage(Pages.UserDashboardPage)
  }

  UIKit.CardDelegate {
    title: Strings.deviceDashboard
    text: Strings.seeAndManageDevices
    icon.source: "qrc:/devices.svg"
    Layout.fillWidth: true
    Layout.preferredHeight: s(80)
    onClicked: homeManager.openPage(Pages.DeviceDashboardPage)
  }

  UIKit.CardDelegate {
    title: Strings.dataDashboard
    text: Strings.seeAndExamineData
    icon.source: "qrc:/data.svg"
    Layout.fillWidth: true
    Layout.preferredHeight: s(80)
    onClicked: homeManager.openPage(Pages.DataDashboardPage)
  }
}
