import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

ColumnLayout {
  spacing: ThemeController.style.margin.m16
  width: parent.width

  UIKit.BaseText {
    text: Strings.whatDoesItMean
    font.weight: Font.Bold
    size: UIKit.BaseText.TextSize.H3
    Layout.fillWidth: true
    Layout.topMargin: ThemeController.style.margin.m16
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.BaseText {
    text: Strings.userWillHaveAccess
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.Infobox {
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    text: Strings.asTheAccountOwner
  }

  UIKit.StyledButton {
    id: button
    text: Strings.okGotIt
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Primary
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
