import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m16

  UIKit.SwitchItem {
    text: Strings.allowScanning
    contentText: Strings.buildContextSyllabus
    showSeparator: true
  }

  UIKit.SwitchItem {
    text: Strings.deleteAfterImport
    contentText: Strings.allowDeleting
    showSeparator: true
  }
}
