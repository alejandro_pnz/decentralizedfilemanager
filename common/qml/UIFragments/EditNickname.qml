import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  id: content
  height: parent.height
  width: parent.width
  spacing: ThemeController.style.margin.m16

  UIKit.InputField {
    Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m24
    labelText: Strings.userNickname
    placeholderTxt: Strings.userNickname
    inputRequired: true
    eyeIconVisible: false
    clearIconVisible: true
    isPassPhrase: false
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
