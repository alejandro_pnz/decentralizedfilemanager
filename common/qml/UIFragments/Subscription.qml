import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

Flickable {
  id: flick
  height: parent.height
  width: parent.width
  boundsBehavior: Flickable.StopAtBounds
  contentHeight: layout.height
  clip: true
  property bool showFooter: true
  property bool isMobile: app.isPhone
  property alias cardColumns: gridLayout.columns

  ColumnLayout {
    id: layout
    spacing: 16
    width: parent.width

    UIKit.BaseText {
      text: Strings.subscriptionStr
      visible: !isMobile
      font.weight: Font.Bold
      size: UIKit.BaseText.TextSize.H2
    }

    UIKit.BaseText {
      Layout.topMargin: app.isPhone ? ThemeController.style.margin.m16 : ThemeController.style.margin.m8
      font.weight: Font.Bold
      text: Strings.currentPlan
    }

    UIKit.Card {
      title: Strings.freeTrial
      contentText: Strings.remainingDays.arg("3")
      Layout.fillWidth: true
      displayDetails: false
      checkable: false
      showDetailsButton: false
    }

    UIKit.BaseText {
      Layout.topMargin: ThemeController.style.margin.m8
      font.weight: Font.Bold
      text: Strings.upgrades
    }

    GridLayout {
      id: gridLayout
      columnSpacing: ThemeController.style.margin.m32
      rowSpacing: ThemeController.style.margin.m24
      columns: 1
      Layout.fillWidth: true

      UIKit.Card {
        title: Strings.personal
        price: "£ 2.99 / mo"
        Layout.fillWidth: true
        upgrade: false
        activePlan: true
        nextPaymentDate: "30/04/2021"
        checkable: false
        detailsButtonCentered: true
        ButtonGroup.group: cardGroup
      }

      UIKit.Card {
        title: Strings.professional
        price: "£ 4.99 / mo"
        Layout.fillWidth: true
        upgrade: true
        checked: true
        Layout.topMargin: 8
        detailsButtonCentered: true
        ButtonGroup.group: cardGroup
      }
    }

    ButtonGroup {
      id: cardGroup
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }
  }
}
