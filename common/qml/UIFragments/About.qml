import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

Rectangle {
  width: app.isMobile ? parent.width : s(540)
  height: contentRect.height

  Rectangle {
    id: contentRect
    anchors.centerIn: parent
    width: parent.width
    height: contentColumn.height + ThemeController.style.margin.m40
    radius: s(2)

    ColumnLayout {
      id: contentColumn
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.leftMargin: app.isMobile ? ThemeController.style.margin.m16 :
                                         ThemeController.style.margin.m40
      anchors.rightMargin: app.isMobile ? ThemeController.style.margin.m16 :
                                          ThemeController.style.margin.m40
      anchors.top: parent.top
      anchors.topMargin: ThemeController.style.margin.m24

      RowLayout {
        id: titleRow
        Layout.preferredHeight: s(85)
        Layout.fillWidth: true
        spacing: ThemeController.style.margin.m32

        UIKit.SvgImage {
          source: "qrc:/login-logo.svg"
          width: s(78)
          height: s(85)
        }

        ColumnLayout {
          Layout.fillWidth: true
          Layout.fillHeight: true
          spacing: 0

          UIKit.BaseText {
            text: "QuodArca\u2122"
            size: UIKit.BaseText.TextSize.H2
            font.weight: Font.Bold
          }

          UIKit.BaseText {
            Layout.topMargin: ThemeController.style.margin.m8
            //TODO: change to proper version
            text: Strings.version + " " + app.applicationVersion
          }

          UIKit.BaseText {
            text: "\u00A9 2020 - 2021  EISST International Ltd"
          }
        }
      }

      Rectangle {
        Layout.topMargin: ThemeController.style.margin.m28
        Layout.preferredHeight: childrenRect.height + ThemeController.style.margin.m16
        Layout.fillWidth: true

        ColumnLayout {
          anchors.margins: ThemeController.style.margin.m8
          anchors.top: parent.top
          anchors.left: parent.left
          anchors.right: parent.right
          spacing: ThemeController.style.margin.m8

          UIKit.BaseText {
            text: Strings.quodArcaInfo
            Layout.fillWidth: true
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
          }

          UIKit.BaseText {
            text: Strings.thirdPartyNotices.arg("<a href=\"http://www.quodarca.com/thirdparty\"> http://www.quodarca.com/thirdparty/</a>");
            Layout.fillWidth: true
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            textFormat: Text.RichText
            onLinkActivated: Qt.openUrlExternally(link)
          }

          UIKit.BaseText {
            text: Strings.quodArcaWarning
            Layout.fillWidth: true
            Layout.leftMargin: ThemeController.style.margin.m8
            Layout.topMargin: ThemeController.style.margin.m8
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
          }
        }
      }
    }
  }
}
