import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: layout
  spacing: 0

  property int pageMode: Enums.UserSettings

  UIKit.SettingsDelegate {
    Layout.topMargin: ThemeController.style.margin.m16
    Layout.fillWidth: true
    title: Strings.resetGlobalSettings
    contentText: Strings.resetAllGlobalSettings
    visible: pageMode === Enums.GlobalSettings
    onClicked: homeManager.openPage(Pages.ResetConfirmationPopupPage)
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.resetUserSettings
    contentText: Strings.resetAllUserSettings
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.resetDeviceSettings
    contentText: Strings.resetAllDeviceSettings
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.resetAllSettings
    contentText: pageMode === Enums.UserSettings ? Strings.resetAllUserAndDevice
                                                 : Strings.resetAllAccountSettings
  }

  UIKit.SettingsDelegate {
    Layout.fillWidth: true
    title: Strings.resetTips
    contentText: Strings.resetAllTips
  }

  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
