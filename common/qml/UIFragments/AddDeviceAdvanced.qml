import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0


ColumnLayout {
  id: layout
  spacing: ThemeController.style.margin.m16
  signal buttonClicked(string passphrase)
  property int deviceType: Enums.Laptop
  property bool deviceDelegate: true
  property bool powerPassphrase: false

  property bool busy: false
  property alias emailConfirmation: emailConfirmation

  signal showHint(point globalPos)

  enabled: !layout.busy

  Connections {
    target: pageManager
    function onSetupAddDeviceAdvancedPage(config) {
      if (config.deviceDelegate !== undefined)
        layout.deviceDelegate = config.deviceDelegate
      if (config.deviceType !== undefined) {
        layout.deviceType = config.deviceType
      }
      if (config.deviceName !== undefined) {
        deviceDelegate.txt = config.deviceName
      }
      if (config.buttonHandler !== undefined) {
        layout.buttonClicked.connect(config.buttonHandler)
      }
    }
  }

  Rectangle {
    Layout.preferredHeight: s(app.isMobile ? 222 : 230)
    Layout.fillWidth: true
    radius: s(10)
    Layout.alignment: Qt.AlignHCenter

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: "qrc:/illustrations/r5-add-new-device.png"
      mipmap: true
    }
  }

  UIKit.BaseText {
    id: title
    Layout.topMargin: ThemeController.style.margin.m24
    font.weight: Font.Bold
    text: Strings.addingNewDevice
    size: UIKit.BaseText.TextSize.H2
    horizontalAlignment: Text.AlignHCenter
    Layout.alignment: Qt.AlignHCenter
  }

  UIKit.BaseText {
    id: content
    horizontalAlignment: Text.AlignHCenter
    Layout.fillWidth: true
    Layout.alignment: Qt.AlignHCenter
    text: Strings.authorizeAddingNewDevice
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.IconDelegate {
    id: deviceDelegate
    visible: layout.deviceDelegate
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    Layout.preferredHeight: s(64)
    boldText: true
    iconBgCircle: true
    iconBgColor: ThemeController.style.deviceDelegate.iconBgColor
    iconSrc: {
      switch(layout.deviceType) {
      case Enums.Laptop: return "qrc:/laptop.svg"
      case Enums.Phone: return "qrc:/phone.svg"
      case Enums.Tablet: return "qrc:/tablet.svg"
      }
    }
  }

  UIKit.InputField {
    id: uuIdInput
    labelText: Strings.deviceUUIDLabel
    placeholderTxt: Strings.typeUUID
    onEnterPressed: button.clicked()
  }

  Rectangle {
    Layout.topMargin: layout.powerPassphrase ? 0 : ThemeController.style.margin.m8
    height: s(1)
    Layout.fillWidth: true
    color: ThemeController.style.seashellColor
  }

  UIKit.InputField {
    id: passphraseInput
    visible: layout.powerPassphrase
    Layout.topMargin: ThemeController.style.margin.m8
    labelText: Strings.powerPassphrase
    placeholderTxt: Strings.typePowerPassphrase
    inputRequired: true
    isPassPhrase: true
    eyeIconVisible: true
    onEnterPressed: button.clicked()
  }
  Item {
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.fillWidth: true
    height: button.implicitHeight
    UIKit.StyledButton {
      id: button
      anchors.fill: parent
      text: Strings.enterPowerPassphraseButton
      spacingBetweenIconAndText: s(8)
      icon.source: "qrc:/info-white.svg"
      displayMode: UIKit.StyledButton.DisplayMode.TextLeftSideIcon
      willShowTips: true
      onIconClicked: showHint(mapToGlobal(button.x, (button.y + button.height) * .5))
      onClicked: {
          if(!RegistrationManager.powerPassphraseConfigured){
              RegistrationManager.setPassphrasePage(false);
          }
          else if(!powerPassphrase){
              powerPassphrase = true
              text = Strings.addDevice
              displayMode = UIKit.StyledButton.DisplayMode.TextOnly
          }
          else{
              var passOK = validateMandatoryField(passphraseInput)
              if (passOK) {
                  RegistrationManager.newDeviceRequestAccepted();
              }
          }
      }
    }
  }

  UIKit.StyledButton {
    id: emailConfirmation
    Layout.fillWidth: true
    spacingBetweenIconAndText: s(10)
    type: UIKit.StyledButton.ButtonStyle.Secondary
    icon.source: "qrc:/mail-blue.svg"
    text: Strings.emailConfirmation
    onClicked: {
      RegistrationManager.showEmailConfirmation();
    }
  }
}
