import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

ColumnLayout {
  spacing: ThemeController.style.margin.m12
  width: parent.width
  signal closePopup()
  signal createPassphrase()

  Item {
    Layout.fillWidth: true
    Layout.preferredHeight: ThemeController.style.margin.m32

    UIKit.BaseText {
      text: Strings.createPassphrase
      font.weight: Font.Bold
      size: UIKit.BaseText.TextSize.H3
      anchors.centerIn: parent
    }

    UIKit.LargeIconButton {
      icon.source: "qrc:/close.svg"
      height: ThemeController.style.margin.m32
      width: ThemeController.style.margin.m32
      anchors.right: parent.right
      visible: !app.isMobile
      onClicked: closePopup()
    }
  }

  UIKit.BaseText {
    text: Strings.protectArchiveContents
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  UIKit.StyledButton {
    id: button
    text: Strings.yesCreatePassphrase
    Layout.topMargin: ThemeController.style.margin.m20
    Layout.fillWidth: true
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Primary
    onClicked: {
      createPassphrase()
    }
  }

  UIKit.StyledButton {
    id: cancelButton
    Layout.topMargin: ThemeController.style.margin.m4
    text: Strings.saveUnprotected
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Secondary
    Layout.fillWidth: true
  }

  Item {
    Layout.preferredHeight: ThemeController.style.margin.m4
    Layout.fillWidth: true
  }
}
