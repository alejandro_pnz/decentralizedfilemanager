import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2

import com.testintl 1.0
import QtPromise 1.1

Item {
  id: content
  property var popup

  ScrollBar {
    id: bar
    active: true
    visible: app.isPhone
    anchors {
      left: listView.right
      top: listView.top
      bottom: listView.bottom
      leftMargin: s(7)
    }

    contentItem: Rectangle {
      implicitWidth: s(4)
      color: ThemeController.style.mercuryColor
    }
  }

  ListView {
    id: listView
    width: parent.width
    height: parent.height
    spacing: ThemeController.style.margin.m12
    clip: true
    ScrollBar.vertical: bar

    BaseListModel {
      id: accountsModel
      keyField: "id"
      fields: ["id", "email", "accountEmail", "accountOwner", "guest"]
      source: AccountManager.accounts
    }

    model: accountsModel
    delegate: UIKit.ContactCard {
      title: model.email
      width: listView.width
      descriptionText: {
        if (model.accountOwner) {
          return Strings.accountOwner
        }
        else if (model.guest) {
          return Strings.guestIn.arg(model.accountEmail)
        }
        else {
          return Strings.memberIn.arg(model.accountEmail)
        }
      }

      checkBoxMode: false
      avatarObject.bottomIcon: true
      avatarObject.editIcon: false
      ButtonGroup.group: buttonsGroup
      checked: AuthenticationManager.currentAccount ? model.email === AuthenticationManager.currentAccount.email : false
      displayMobileMode: true
      onClicked: {
        var future = Future.promise(AuthenticationManager.logout())

        future.then(function() {
          RegistrationManager.switchAccount(model.email)
          popup.close()
        },
        function(error) {
          console.log(error)
        })
      }
    }
  }

  ButtonGroup {
    id: buttonsGroup
    exclusive: true
  }
}
