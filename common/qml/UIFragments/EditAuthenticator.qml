import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  id: root
  spacing: ThemeController.style.margin.m16
  width: parent.width

  property string authenticatorCode: "375 923"

  RemovableInput {
    id: removableInput
    visible: authenticatorCode !== ""
    Layout.fillWidth: true
    Layout.preferredHeight: s(80)
    InputField {
      id: authenticatorInput
      labelText: Strings.authenticatorCode
      placeholderTxt: authenticatorCode
      inputRequired: false
      clearIconVisible: false
      rightIconSource: "qrc:/duplicate-icon.svg"
      rightIconBtn.iconColor: ThemeController.style.slatePurpleColor
      leftIconBtn.iconColor: ThemeController.style.slatePurpleColor
      leftIconSource: "qrc:/refresh.svg"
      Layout.fillWidth: true
      rightIconBtn.states: []
      leftIconBtn.states: []
      enabled: false

      BaseText {
        text: Strings.expiresIn.arg("3 s")
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        size: BaseText.TextSize.Small
        color: ThemeController.style.shuttleColor
      }
    }
    onDeleteButtonClicked:{
      authenticatorCode = ""
    }
  }

  AuthenticatorInput {
    visible: !removableInput.visible
    Layout.fillWidth: true
  }
}
