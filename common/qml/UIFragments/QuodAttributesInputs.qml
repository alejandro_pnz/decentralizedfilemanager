import QtQuick 2.15
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0
import AppStyles 1.0
import QSyncable 1.0
import QtQuick.Controls 2.15
import com.testintl 1.0

Item {
  property alias nameInput: nameInput
  property alias loginInput: loginInput
  property alias passwordInput: passwordInput
  property alias websiteInput: websiteInput
  property alias authenticatorInput: authenticatorInput
  property alias countryInput: countryInput
  property alias heightInput: heightInput
  property alias genderInput: genderInput
  property alias dateInput: dateInput
  property alias numberInput: numberInput
  property alias capitalizedInput: capitalizedInput
  property alias serverTypeInput: serverTypeInput
  property alias selectFileInput: selectFileInput
  property alias networkInput: networkInput
  property alias comboboxInput: comboboxInput
  property alias emptyInput: emptyInput
  property bool tooltipsUpdateVar: false

  Component {
    id: emptyInput
    Item {
      width: parent.width
    }
  }

  Component {
    id: nameInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: input
      property bool changeText: true

      function activateInput() {
        input.textField.forceActiveFocus()
      }

      onReadonlyChanged: {
        if(readonly){
          changeText = false
          if(input.isPassPhrase){
            input.textField.text = "************"
          }
        } else {
          input.textField.text = value
          changeText = true
        }
      }

      UIKit.InputField {
        id: input
        labelText: parent.labelText
        placeholderTxt: parent.placeholderText
        textField.text: parent.value
        Layout.fillWidth: true
        textField.onTextChanged: {
          if(parent.changeText){
            parent.textChanged(textField.text)
            parent.value = textField.text
          }
        }
        textField.readOnly: parent.readonly
        inputRequired: parent.readonly // TODO add required property from model
        clearIconVisible: true
        rightIconSource: parent.readonly ? "qrc:/duplicate-icon.svg" : "qrc:/close-icon.svg"
        rightIconBtn.style: parent.readonly ? ThemeController.style.fieldButtonAlt : ThemeController.style.fieldButton
        overrideRightButton: parent.readonly
        textField.maximumLength: parent.maxLength ? parent.maxLength : 32767
        isPassPhrase: parent.sensitive
        onRightButtonClicked: {
          if (overrideRightButton) {
            app.clipboard.copy(textField.text)
          }
        }
      }
    }
  }

  Component {
    id: loginInput
    UIKit.RemovableInput {
      width: parent.width
      property bool hasToolTip: !readonly
      property alias tooltip: loginTooltip.tip
      property alias input: loginInputField
      property bool changeText: true

      function activateInput() {
        input.textField.forceActiveFocus()
      }

      onReadonlyChanged: {
        if(readonly){
          changeText = false
          if(loginInputField.isPassPhrase){
            loginInputField.textField.text = "************"
          }
        } else {
          loginInputField.textField.text = value
          changeText = true
        }
      }

      UIKit.InputField {
        id: loginInputField
        labelText: parent.labelText
        placeholderTxt: parent.placeholderText
        textField.text: parent.value
        inputRequired: false
        leftEyeIcon: !parent.readonly
        Layout.fillWidth: true
        textField.onTextChanged: {
          if(parent.changeText){
            parent.textChanged(textField.text)
            parent.value = textField.text
          }
        }
        textField.readOnly: parent.readonly
        clearIconVisible: !parent.readonly
        rightIconSource: parent.readonly ? "qrc:/duplicate-icon.svg" : "qrc:/close-icon.svg"
        rightIconBtn.style: parent.readonly ? ThemeController.style.fieldButtonAlt : ThemeController.style.fieldButton

//        rightIconBtn.width: s(24)
//        rightIconBtn.height: s(24)
//        rightIconBtn.iconWidth: s(24)
//        rightIconBtn.iconHeight: s(24)

        textField.maximumLength: parent.maxLength ? parent.maxLength : 32767
        isPassPhrase: parent.sensitive
        overrideRightButton: parent.readonly
        onRightButtonClicked: {
          if (overrideRightButton) {
            app.clipboard.copy(textField.text)
          }
        }
        UIKit.InputToolTip {
          id: loginTooltip
          input: loginInputField
          inputIcon: loginInputField.leftIconBtn
          updateVariable: tooltipsUpdateVar
          tip.tipContent: Strings.loginInputTip
          tip.confirmTxt: Strings.gotIt
        }
      }
    }
  }

  Component {
    id: passwordInput
    UIKit.RemovableInput {
      id: parentInput
      width: parent.width
      property bool hasToolTip: !readonly
      property alias tooltip: pwdTooltip.tip
      property alias input: passwordInputField
      property bool changeText: true
      function activateInput() {
        input.textField.forceActiveFocus()
      }

      Connections {
        target: quodEditor
        function onPasswordGenerated(sectionId, attributeId, password) {
          if (parentInput.sectionId === sectionId && parentInput.attributeId === attributeId) {
            parentInput.value = password;
          }
        }
      }

      onReadonlyChanged: {
        if(readonly){
          changeText = false
          if(input.isPassPhrase){
            input.textField.text = "************"
          }
        } else {
          input.textField.text = value
          changeText = true
        }
      }

      UIKit.InputField {
        id: passwordInputField
        property bool hasHistory: false
        labelText: parent.labelText
        placeholderTxt: parent.placeholderText
        textField.text: parent.value
        inputRequired: false
        Layout.fillWidth: true
        eyeIconVisible: !parent.readonly
        clearIconVisible: false
        isPassPhrase: true
        textField.onTextChanged: {
          if(parent.changeText){
            parent.textChanged(textField.text)
            parent.value = textField.text
          }
        }
        textField.readOnly: parent.readonly
        rightIconSource: parent.readonly ? "qrc:/duplicate-icon.svg" : (textField.echoMode === TextInput.Normal ? "qrc:/view-off.svg" : "qrc:/dark-eye.svg")
        rightIconBtn.style: parent.readonly ? ThemeController.style.fieldButtonAlt : ThemeController.style.fieldButton

//        rightIconBtn.width: s(24)
//        rightIconBtn.height: s(24)
//        rightIconBtn.iconWidth: s(24)
//        rightIconBtn.iconHeight: s(24)

        overrideRightButton: parent.readonly
        textField.maximumLength: parent.maxLength ? parent.maxLength : 32767
        onRightButtonClicked: {
          if (overrideRightButton) {
            app.clipboard.copy(textField.text)
          }
        }
        UIKit.StyledButton {
          type: UIKit.StyledButton.ButtonStyle.Text
          displayMode: UIKit.StyledButton.DisplayMode.TextRightSideIcon
          text: parentInput.readonly ? Strings.history : Strings.generate
          icon.source: parentInput.readonly ? "qrc:/history.svg" : "qrc:/refresh.svg"
          anchors.right: parent.right
          anchors.verticalCenter: parent.verticalCenter
          onClicked: parentInput.readonly ? parentInput.enterPasswordHistory() : parentInput.generatePassword()
          visible: parentInput.readonly ? passwordInputField.hasHistory : true
        }

        UIKit.InputToolTip {
          id: pwdTooltip
          input: passwordInputField
          inputIcon: passwordInputField.leftIconBtn
          updateVariable: tooltipsUpdateVar
          tip.tipContent: Strings.passwordInputTip
          tip.confirmTxt: Strings.gotIt
        }
      }
    }
  }
  Component {
    id: websiteInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: website
      property bool hasToolTip: readonly
      property alias tooltip: websiteTooltip.tip
      property bool changeText: true

      function activateInput() {
        input.textField.forceActiveFocus()
      }

      onReadonlyChanged: {
        if(readonly){
          changeText = false
          if(website.isPassPhrase){
            website.textField.text = "************"
          }
        } else {
          website.textField.text = value
          changeText = true
        }
      }

      UIKit.InputField {
        id: website
        labelText: parent.labelText
        placeholderTxt: parent.placeholderText
        textField.text: parent.value
        inputRequired: false
        Layout.fillWidth: true
        leftEyeIcon: false
        clearIconVisible: false
        eyeIconVisible: true
        textField.onTextChanged: {
          if(parent.changeText){
            parent.textChanged(textField.text)
            parent.value = textField.text
          }
        }
        textField.readOnly: parent.readonly
        rightIconSource: parent.readonly ? "qrc:/duplicate-icon.svg" : (textField.echoMode === TextInput.Normal ? "qrc:/view-off.svg" : "qrc:/dark-eye.svg")
        rightIconBtn.style: parent.readonly ? ThemeController.style.fieldButtonAlt : ThemeController.style.fieldButton
        overrideRightButton: parent.readonly

//        rightIconBtn.width: s(24)
//        rightIconBtn.height: s(24)
//        rightIconBtn.iconWidth: s(24)
//        rightIconBtn.iconHeight: s(24)

        textField.maximumLength: parent.maxLength ? parent.maxLength : 32767
        isPassPhrase: parent.sensitive
        onRightButtonClicked: {
          if (overrideRightButton) {
            app.clipboard.copy(textField.text)
          }
        }

        textField.onPressAndHold: {
          Qt.openUrlExternally(textField.text)
        }

        UIKit.InputToolTip {
          id: websiteTooltip
          input: website
          inputIcon: website.leftIconBtn
          updateVariable: tooltipsUpdateVar
          tip.tipContent: Strings.websiteTooltip
          tip.confirmTxt: Strings.gotIt
        }
      }
    }
  }
  Component {
    id: authenticatorInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: auth
      property bool changeText: true
      function activateInput() {
        input.textField.forceActiveFocus()
      }

      onReadonlyChanged: {
        if(readonly){
          changeText = false
          if(input.isPassPhrase){
            input.textField.text = "************"
          }
        } else {
          input.textField.text = value
          changeText = true
        }
      }

      UIKit.AuthenticatorInput {
        id: auth
        labelText: parent.labelText
        placeholderTxt: parent.placeholderText
        textField.text: parent.value
        Layout.fillWidth: true
        isPassPhrase: parent.sensitive
        clearIconVisible: false

        rightIconSource: parent.readonly ? "qrc:/duplicate-icon.svg" : (textField.echoMode === TextInput.Normal ? "qrc:/view-off.svg" : "qrc:/dark-eye.svg")
        rightIconBtn.style: parent.readonly ? ThemeController.style.fieldButtonAlt : ThemeController.style.fieldButton

        textField.onTextChanged: {
          if(parent.changeText){
            parent.textChanged(textField.text)
            parent.value = textField.text
          }
        }
        textField.readOnly: parent.readonly
        textField.maximumLength: parent.maxLength ? parent.maxLength : 32767

        UIKit.BaseText {
          id: textSecondsLeft

          property int seconds: 10

          text: Strings.expiresIn.arg(seconds)
          anchors.right: parent.right
          anchors.verticalCenter: parent.verticalCenter
          size: UIKit.BaseText.TextSize.Small
          color: ThemeController.style.shuttleColor

          Timer {
            interval: 1000
            repeat: true
            triggeredOnStart: true
            running: true
            onTriggered: {
              if ((--textSecondsLeft.seconds) === 0) {
                textSecondsLeft.seconds = 10;
              }
            }
          }
        }
      }
    }
  }

  Component {
    id: countryInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: countryInp
      function activateInput() {
        input.openPicker()
      }
      UIKit.CountryPickerDelegate {
        id: countryInp
        Layout.fillWidth: true
        labelText: Strings.country
        pickerTitle: Strings.country
        placeholderTxt: Strings.selectCountryPlaceholder
        inputRequired: false
        leftEyeIcon: true
        objectsModel: countryModel
        comboBox.editable: !parent.readonly
      }
    }
  }

  Component {
    id: heightInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: heightInp
      function activateInput() {
        input.openPicker()
      }
      UIKit.PickerDelegate {
        id: heightInp
        Layout.fillWidth: true
        labelText: Strings.heightTxt
        pickerTitle: Strings.heightTxt
        placeholderTxt: Strings.typeHeight
        inputRequired: false
        leftEyeIcon: true
        objectsModel: [Strings.inches, Strings.centimeters]
        mainTextPickable: false
        autocheck: false
        comboBox.editable: !parent.readonly
        textField.validator: IntValidator {
          bottom: 0
        }
      }
    }
  }

  Component {
    id: dateInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: dateInp
      function activateInput() {
        input.openPicker()
      }
      UIKit.DateInput {
        id: dateInp
        Layout.fillWidth: true
        labelText: parent.labelText
        placeholderTxt: parent.placeholderText
        inputRequired: false
        leftEyeIcon: true
        comboBox.editable: !parent.readonly
      }
    }
  }

  Component {
    id: genderInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: genderInp
      function activateInput() {
        input.openPicker()
      }
      UIKit.PickerDelegate {
        id: genderInp
        Layout.fillWidth: true
        labelText: Strings.gender
        pickerTitle: Strings.gender
        placeholderTxt: Strings.selectGender
        inputRequired: false
        leftEyeIcon: true
        objectsModel: [Strings.male, "X", Strings.female]
        autocheck: false
        comboBox.editable: !parent.readonly
      }
    }
  }

  Component {
    id: numberInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: numberInputField

      property bool changeText: true
      function activateInput() {
        input.textField.forceActiveFocus()
      }

      onReadonlyChanged: {
        if(readonly){
          changeText = false
          if(numberInputField.isPassPhrase){
            numberInputField.textField.text = "************"
          }
        } else {
          numberInputField.textField.text = value
          changeText = true
        }
      }

      UIKit.InputField {
        id: numberInputField
        labelText: parent.labelText
        placeholderTxt: parent.placeholderText
        textField.text: parent.value
        inputRequired: false
        leftEyeIcon: !parent.readonly
        Layout.fillWidth: true
        textField.validator: IntValidator {
        }

        textField.onTextChanged: {
          if(parent.changeText){
            parent.textChanged(textField.text)
            parent.value = textField.text
          }
        }

        textField.readOnly: parent.readonly
        clearIconVisible: !parent.readonly
        textField.maximumLength: parent.maxLength ? parent.maxLength : 32767
        isPassPhrase: parent.sensitive
      }
    }
  }

  Component {
    id: capitalizedInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: input
      property bool changeText: true
      function activateInput() {
        input.textField.forceActiveFocus()
      }

      onReadonlyChanged: {
        if(readonly){
          changeText = false
          if(input.isPassPhrase){
            input.textField.text = "************"
          }
        } else {
          input.textField.text = value
          changeText = true
        }
      }

      UIKit.InputField {
        id: input
        labelText: parent.labelText
        placeholderTxt: parent.placeholderText
        textField.text: parent.value
        Layout.fillWidth: true
        textField.onTextChanged: {
          if(parent.changeText){
            parent.textChanged(textField.text)
            parent.value = textField.text
          }
        }
        textField.readOnly: parent.readonly
        inputRequired: parent.readonly // TODO add required property from model
        leftEyeIcon: !parent.readonly
        textField.maximumLength: parent.maxLength ? parent.maxLength : 32767
        isPassPhrase: parent.sensitive
        textField.font.capitalization: Font.AllUppercase
      }
    }
  }

  Component {
    id: serverTypeInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: serverInput
      function activateInput() {
        input.openPicker()
      }
      UIKit.PickerDelegate {
        id: serverInput
        Layout.fillWidth: true
        labelText: Strings.incomingServerType
        pickerTitle: Strings.incomingServerType
        placeholderTxt: Strings.selectServerType
        inputRequired: false
        leftEyeIcon: true
        objectsModel: ["POP3", "IMAP"]
        autocheck: false
        comboBox.editable: !parent.readonly
      }
    }
  }
  Component {
    id: networkInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: networkInp
      function activateInput() {
        input.openPicker()
      }
      UIKit.PickerDelegate {
        id: networkInp
        Layout.fillWidth: true
        labelText: parent.labelText
        placeholderTxt: parent.placeholderText
        pickerTitle: parent.labelText
        inputRequired: false
        leftEyeIcon: true
        objectsModel: [Strings.router, Strings.server, Strings.switchStr]
        autocheck: false
        comboBox.editable: !parent.readonly
      }
    }
  }

  Component {
    id: selectFileInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: fileInput
      property bool fileToImport: fileInput.fileToImport
      property var fileToImportPath: fileInput.fileStoragePath
      function activateInput() {
        input.openPicker()
      }

      UIKit.FilePickerDelegate {
        id: fileInput
        Layout.fillWidth: true
        pickerTitle: parent.labelText
        labelText: parent.labelText
        placeholderTxt: parent.placeholderText
        inputRequired: false
        leftEyeIcon: false
        autocheck: false
        comboBox.editable: !parent.readonly
        textField.text: parent.value
        textField.onTextChanged: parent.textChanged(fileId)
        enabled: !parent.readonly
        filterCertFiles: parent.attributeType === AttributeObject.CertificateFile
        selectFileTitle: parent.attributeType === AttributeObject.CertificateFile ?
                         Strings.selectCertificateFile : Strings.selectFile
      }
    }
  }

  Component {
    id: comboboxInput
    UIKit.RemovableInput {
      width: parent.width
      property alias input: internalInput
      function activateInput() {
        input.openPicker()
      }

      UIKit.PickerDelegate {
        id: internalInput
        Layout.fillWidth: true
        labelText: parent.labelText
        placeholderTxt: parent.placeholderText
        pickerTitle: parent.labelText
        inputRequired: false
        leftEyeIcon: true
        autocheck: false
        comboBox.editable: !parent.readonly
        pickerDelegateCapitalized: false
      }
    }
  }
}
