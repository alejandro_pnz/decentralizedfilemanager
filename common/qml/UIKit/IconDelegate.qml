import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import Qt5Compat.GraphicalEffects


Item {
  implicitWidth: parent.width
  implicitHeight: Qt.platform.os === "android" ||
                  Qt.platform.os === "ios" ? s(80) : s(70)

  property string iconSrc: ""
  property color iconBgColor: ThemeController.style.slatePurpleColor
  property string txt: ""
  property bool boldText: false
  property alias text: textComponent
  /*!
    \qmlproperty bool IconDelegate::iconBgCircle

    When set to true, sets the icon's background to be a circle.
    Note that if set to true IconDelegate::iconBgRadius is ignored
  */
  property bool iconBgCircle: false
  property int iconBgRadius: s(2)

  Rectangle {
    id: delegate
    width: parent.width
    height: parent.height
    border.width: s(1)
    border.color: ThemeController.style.whisperColor
    radius: s(4)
    color: ThemeController.style.whiteColor

    Row {
      spacing: ThemeController.style.margin.m12
      height: s(40)
      anchors {
        left: parent.left
        right: parent.right
        leftMargin: ThemeController.style.margin.m16
        rightMargin: ThemeController.style.margin.m16
        verticalCenter: parent.verticalCenter
      }

      Rectangle {
        id: imgContainer
        height: s(40)
        width: s(40)
        radius: iconBgCircle ? height * 0.5 : iconBgRadius
        color: iconBgColor

        UIKit.SvgImage {
          anchors.centerIn: parent
          source: iconSrc
          asynchronous: true
          width: s(16)
          height: s(16)
          fillMode: Image.PreserveAspectFit
        }
      }

      UIKit.BaseText {
        id: textComponent
        width: parent.width - imgContainer.width - s(28)
        text: txt
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        horizontalAlignment: Qt.AlignLeft
        anchors.verticalCenter: imgContainer.verticalCenter
        font.weight: boldText ? Font.Bold : Font.Normal
      }
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: delegate
    horizontalOffset: 0
    verticalOffset: 0
    radius: s(15)
    //samples: s(31)
    color: Qt.rgba(0, 0, 0, 0.05)
    source: delegate
  }
}
