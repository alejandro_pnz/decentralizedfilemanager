import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import Qt5Compat.GraphicalEffects

import QtQuick.Controls 2.15

RadioDelegate {
  id: control

  implicitWidth: parent.width
  implicitHeight: s(72)
  property string iconSrc: ""
  property string txt: ""
  rightPadding: ThemeController.style.margin.m16
  leftPadding: ThemeController.style.margin.m16

  contentItem: Row {
    spacing: ThemeController.style.margin.m12
    height: s(32)
    anchors.verticalCenter: background.verticalCenter

    UIKit.SvgImage {
      source: iconSrc
      asynchronous: true
      width: s(46)
      height: s(32)
      fillMode: Image.PreserveAspectFit
      anchors.verticalCenter: parent.verticalCenter
    }

    UIKit.BaseText {
      text: txt
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      horizontalAlignment: Qt.AlignLeft
      font.weight: Font.DemiBold
      anchors.verticalCenter: parent.verticalCenter
    }
  }

  indicator: Rectangle {
    x: control.width - width - control.rightPadding
    anchors.verticalCenter: background.verticalCenter
    implicitWidth: ThemeController.style.radioButton.height
    implicitHeight: ThemeController.style.radioButton.height
    radius: s(100)
    border.color: ThemeController.style.radioButton.defaultBdColor
    border.width: control.checked ? 0 : 1.5
    color: control.checked ? ThemeController.style.radioButton.checkedBgColor
                           : ThemeController.style.radioButton.defaultBgColor
    property alias middle: indicatorMiddle

    Rectangle {
      id: indicatorMiddle
      anchors.centerIn: parent
      width: ThemeController.style.radioButton.middleHeight
      height: ThemeController.style.radioButton.middleHeight
      radius: 100
      color: ThemeController.style.radioButton.middleColor
      visible: control.checked
    }
  }

  background: Item {
    Rectangle {
      id: delegateBg
      width: parent.width
      height: parent.height
      border.width: s(1)
      border.color: ThemeController.style.whiteSmokeColor
      color: ThemeController.style.whiteColor
      radius: s(8)
    }

    DropShadow {
      id: dropShadow
      anchors.fill: delegateBg
      horizontalOffset: 0
      verticalOffset: s(8)
      radius: s(16)
      //samples: s(32)
      color: Qt.rgba(0, 0, 0, 0.05)
      source: delegateBg
    }
  }

  states: [
    State {
      name: "selected"
      when: control.checked
      PropertyChanges {
        target: delegateBg;
        border.width: s(2)
        border.color: ThemeController.style.slatePurpleColor
      }
    },
    State {
      name: "default"
      when: !control.checked
      PropertyChanges {
        target: delegateBg;
        border.width: s(1)
        border.color: ThemeController.style.whiteSmokeColor
      }
    }
  ]
}
