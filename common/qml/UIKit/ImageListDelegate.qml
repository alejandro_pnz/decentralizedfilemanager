import QtQuick 2.15
import Qt5Compat.GraphicalEffects

import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Controls 2.15

AbstractButton {
  id: delegate

  signal actionButtonClicked()

  property bool showMenu: true

  readonly property bool isMobile: Qt.platform.os === "android" || Qt.platform.os === "ios"
  readonly property var titleComponent: text
  readonly property var bgRect: bg
  readonly property var actionBtn: btn
//  implicitHeight: s(isMobile ? 170 : 181)
//  implicitWidth: s(isMobile ? 167 : 170)

  clip: true
  padding: 0

  background: Item {
    Rectangle {
      id: bg
      visible: false
      color: ThemeController.style.imageListDelegate.bgColor
      anchors.fill: parent
      radius: s(8)
      border.color: ThemeController.style.imageListDelegate.bdColor
      border.width: s(1)
    }

    DropShadow {
      color: ThemeController.style.imageListDelegate.dropShadowColor
      source: bg
      anchors.fill: parent
    }
  }

  contentItem: Item {
    anchors.fill: parent

    Item {
      id: content
      anchors.fill: parent
      visible: false
      SvgImage {
        id: image
        anchors.fill: parent
        anchors.bottomMargin: showMenu ? s(44) : 0
        fillMode: Image.PreserveAspectCrop
        source: delegate.icon.source
      }
      Rectangle {
        id: bottom
        visible: showMenu
        color: ThemeController.style.imageListDelegate.bgColor
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: s(1)
        anchors.rightMargin: s(1)
        anchors.bottomMargin: s(1)
        height: s(43)
      }
    }

    OpacityMask {
      anchors.fill: parent
      maskSource: bg
      source: content
    }

    Item {
      visible: showMenu
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.right: parent.right
      height: s(44)
      UIKit.BaseText {
        id: text
        text: delegate.text
        font.weight: Font.DemiBold
        elide: Text.ElideRight
        horizontalAlignment: Text.AlignHCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: ThemeController.style.margin.m32
        anchors.leftMargin: ThemeController.style.margin.m32
      }

      UIKit.LargeIconButton {
        anchors.verticalCenter: parent.verticalCenter
        id: btn
        icon.source: "qrc:/more-2.svg"
        width: s(24)
        height: s(24)
        anchors.right: parent.right
        anchors.rightMargin: ThemeController.style.margin.m4
        onClicked: {
          delegate.actionButtonClicked()
        }
      }
    }
  }
}
