import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

Item {
  implicitWidth: parent.width
  implicitHeight: s(44)

  property alias iconObject: iconObject
  property alias titleItem: titleItem
  property alias switchButton: switchButton

  RowLayout {
    spacing: ThemeController.style.margin.m12
    anchors.fill: parent
    ObjectAvatar {
      id: iconObject
      color: ThemeController.style.charadeColor
      iconSize: s(24)
      Layout.preferredHeight: s(44)
      Layout.preferredWidth: s(44)
      iconSrc: "qrc:/characters.svg"
    }

    UIKit.BaseText {
      id: titleItem
      text: Strings.specialCharacters
      font.weight: Font.DemiBold
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }

    UIKit.SwitchButton {
      id: switchButton
    }
  }
}
