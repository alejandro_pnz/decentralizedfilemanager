import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12

Item {
  id: button

  enum DisplayMode {
    LargeIconButton,
    StyledButton
  }

  implicitHeight: s(24)
  implicitWidth: mode === NavigationBarButton.DisplayMode.LargeIconButton ?
                   largeIconButton.width : styledBtn.width

  property int mode: NavigationBarButton.DisplayMode.LargeIconButton
  property alias iconButton: largeIconButton
  property alias styledButton: styledBtn

  signal clicked()

  UIKit.LargeIconButton {
    id: largeIconButton
    visible: mode === NavigationBarButton.DisplayMode.LargeIconButton
    anchors.verticalCenter: parent.verticalCenter
    onClicked: button.clicked()
  }

  UIKit.StyledButton {
    id: styledBtn
    type: UIKit.StyledButton.ButtonStyle.Text
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    visible: mode === NavigationBarButton.DisplayMode.StyledButton
    anchors.verticalCenter: parent.verticalCenter
    onClicked: button.clicked()
  }
}
