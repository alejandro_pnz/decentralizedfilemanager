import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects

import AppStyles 1.0
import com.test 1.0

AbstractButton {
  id: control

  default property alias children: tip.children
  property int popupTopSpacing: 72

  property var clickHandler: function() {
    openTip()
  }

  function closeTip() {
    tip.close()
  }

  function openTip() {
    tip.open()
    if(!app.isMobile || app.isTablet){
      var middlePoint = control.mapToItem(null, control.width, control.height / 2)
      tip.x = middlePoint.x + tip.triangleLeftMargin
      var tipY = (tip.parent.height - tip.height) * 0.5
      if (middlePoint.y < tipY) {
        // icon is above the centered tip
        tip.y = control.mapToItem(null, control.width, 0).y
      } else if (middlePoint.y > tipY + tip.height) {
        // Icon is below the tip
        tip.y = control.mapToItem(null, control.width, control.height).y - tip.height
      } else {
        tip.y = tipY
      }
      tip.triangleY = middlePoint.y - tip.y
    }
  }

  implicitHeight: s(24)
  implicitWidth: s(24)
  padding: 0
  icon.source: "qrc:/tips-icon.svg"
  background: SvgImage {
    source: control.icon.source
    height: s(24)
    width: s(24)
  }

  onClicked: {
    clickHandler()
  }

  BigInputTip {
    id: tip
    topPadding: popupTopSpacing
  }
}
