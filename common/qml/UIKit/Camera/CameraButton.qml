import QtQuick 2.15
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0
import QtMultimedia 5.15

Button {
  id: control
  width: s(48)
  height: s(48)
  icon.source: control.checked ? "qrc:/power-icon.svg" : "qrc:/revoke-power.svg"
  checkable: true
  contentItem: Item {
    anchors.fill: parent
    UIKit.ColoredImage {
      anchors.centerIn: parent
      id: icon
      width: s(24)
      height: s(24)
      source: control.icon.source
      color: ThemeController.style.ironColor
    }
  }
  background: Rectangle {
    radius: width / 2
    anchors.fill: parent
    color: ThemeController.style.mirageColor
  }
}
