import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0

Button {
  id: control
  width: s(64)
  height: s(64)

  background: Rectangle {
    radius: width / 2
    anchors.fill: parent
    border.color: ThemeController.style.whiteColor
    border.width: s(3)
    color: ThemeController.style.transparent
    Rectangle {
      anchors.centerIn: parent
      radius: width / 2
      width: s(48)
      height: s(48)
      color: ThemeController.style.whiteColor
    }
  }
}
