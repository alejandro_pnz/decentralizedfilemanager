import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import com.test 1.0
import com.testintl 1.0
import QtMultimedia 5.15
import UIKit 1.0 as UIKit
import UIFragments 1.0
import AppStyles 1.0
import QtQuick.Window 2.12

UIKit.LargeIconButton {
  id: largeIconButton
  icon.source: "qrc:/close.svg"
  iconHeight: s(32)
  iconWidth: s(32)
  buttonStyle: ThemeController.style.closeCameraButton
  onClicked: homeManager.back()
  property int tM: ThemeController.style.margin.m48
  property int rM: ThemeController.style.margin.m16
  states: [
    State {
      name: "portrait"
      when: Screen.orientation === Qt.PortraitOrientation
      AnchorChanges {
        target: largeIconButton
        anchors {
          top: largeIconButton.parent.top
          right: largeIconButton.parent.right
          left: undefined
          bottom: undefined
        }
      }

      PropertyChanges {
        target: largeIconButton
        anchors {
          topMargin: tM
          rightMargin: rM
          leftMargin: 0
          bottomMargin: 0
        }
      }
    },
    State {
      name: "landscape"
      when: Screen.orientation === Qt.LandscapeOrientation
      AnchorChanges {
        target: largeIconButton
        anchors {
          top: largeIconButton.parent.top
          right: undefined
          left: largeIconButton.parent.left
          bottom: undefined
        }
      }
      PropertyChanges {
        target: largeIconButton
        anchors {
          topMargin: rM
          rightMargin: 0
          leftMargin: tM
          bottomMargin: 0
        }
      }
    },
    State {
      name: "invlandscape"
      when: Screen.orientation === Qt.InvertedLandscapeOrientation
      AnchorChanges {
        target: largeIconButton
        anchors {
          top: undefined
          right: largeIconButton.parent.right
          left: undefined
          bottom: largeIconButton.parent.bottom
        }
      }
      PropertyChanges {
        target: largeIconButton
        anchors {
          topMargin: 0
          rightMargin: tM
          leftMargin: 0
          bottomMargin: rM
        }
      }
    }
  ]
}
