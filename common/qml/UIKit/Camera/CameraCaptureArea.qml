import QtQuick 2.15
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0
import Qt5Compat.GraphicalEffects


Item {
  id: root
  property alias area: area
  property alias tooltip: tooltip
  property bool isPortrait: Screen.orientation === Qt.PortraitOrientation

  Component.onCompleted: {
    if (Screen.orientation === Qt.PortraitOrientation) {
      tooltip.open()
    }
  }

  onIsPortraitChanged: {
    if (!isPortrait) {
      if (tooltip.opened) {
        tooltip.close()
      }
    }
  }

  // Overlay
  Canvas {
    id: canvas
    anchors.fill: parent

    onPaint: {
        var ctx = getContext("2d");
        ctx.fillStyle = Qt.rgba(0, 0, 0, 0.4)

        ctx.beginPath();
        ctx.fillRect(0, 0, width, height);
        ctx.fill();

        ctx.clearRect(area.x + s(2), area.y + s(2), area.width - s(4), area.height - s(4))
    }
  }

  UIKit.ToolTip {
    id: tooltip
//    tipContent: Strings.selectAnyText
    confirmTxt: Strings.gotIt
    config: ThemeController.style.lightTooltip
    x: (root.width - width) * 0.5
    y: area.y - height - ThemeController.style.margin.m32
    triangle.pointing: UIKit.CanvasTriangle.Pointing.Down
    triangle.width: s(12)
    triangle.height: s(9)
    triangle.anchors {
      right: undefined
      left: undefined
      top: triangle.parent.bottom
      bottom: undefined
      horizontalCenter: triangle.parent.horizontalCenter
      topMargin: 0
      bottomMargin: 0
      leftMargin: 0
      rightMargin: 0
    }
  }

  Item {
    id: area
    anchors {
      centerIn: parent
    }
    onXChanged: canvas.requestPaint()
    onYChanged: canvas.requestPaint()
    onWidthChanged: canvas.requestPaint()
    onHeightChanged: canvas.requestPaint()
    width: parent.width - 2 * ThemeController.style.margin.m16
    height: width * 0.75

    UIKit.ColoredImage {
      id: topleft
      anchors {
        top: parent.top
        left: parent.left
      }
      rotation: 90
      source: "qrc:/corner-image.svg"
    }
    UIKit.ColoredImage {
      id: topright
      anchors {
        top: parent.top
        right: parent.right
      }
      rotation: -180
      source: "qrc:/corner-image.svg"
    }
    UIKit.ColoredImage {
      id: bottomRight
      anchors {
        bottom: parent.bottom
        right: parent.right
      }
      rotation: -90
      source: "qrc:/corner-image.svg"
    }
    UIKit.ColoredImage {
      id: bottomLeft
      anchors {
        bottom: parent.bottom
        left: parent.left
      }
      source: "qrc:/corner-image.svg"
    }

    states: [
      State {
        name: "portrait"
        when: Screen.orientation === Qt.PortraitOrientation
        PropertyChanges {
          target: area
          width: parent.width - 2 * ThemeController.style.margin.m16
          height: width * 0.75
        }
      },
      State {
        name: "landscape"
        when: Screen.orientation === Qt.LandscapeOrientation
        PropertyChanges {
          target: area
          width: height * 4 / 3
          height: parent.height - 2 * ThemeController.style.margin.m16
        }
      },
      State {
        name: "invlandscape"
        when: Screen.orientation === Qt.InvertedLandscapeOrientation
        PropertyChanges {
          target: area
          width: height * 4 / 3
          height: parent.height - 2 * ThemeController.style.margin.m16
        }
      }
    ]
  }

}
