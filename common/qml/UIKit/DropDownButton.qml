import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12

Button {
  id: button
  implicitHeight: s(40)
  state: 'default'

  property alias bg: bgRect
  property alias textObject: displayedText
  property alias iconObject: icon

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      radius: s(4)
    }
  }

  contentItem: RowLayout {
    anchors.verticalCenter: parent.verticalCenter
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.leftMargin: ThemeController.style.margin.m12
    anchors.rightMargin: ThemeController.style.margin.m12
    height: s(24)
    spacing: ThemeController.style.margin.m4

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }

    UIKit.BaseText {
      id: displayedText
      text: button.text
      font.weight: Font.DemiBold
      horizontalAlignment: Qt.AlignHCenter
    }

    SvgImage {
      id: icon
      asynchronous: true
      Layout.preferredWidth: s(24)
      Layout.preferredHeight: s(24)
      fillMode: Image.PreserveAspectFit
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }
  }

  states: [
    State {
      name: "default"
      when: !button.hovered && !button.pressed
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.whiteColor
      }
      PropertyChanges {
        target: displayedText
        color: ThemeController.style.charadeColor
      }
      PropertyChanges {
        target: icon
        source: "qrc:/chevron-default.svg"
      }
    },

    State {
      name: "hovered"
      when: button.hovered && !button.pressed
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.whiteSmokeColor
      }
      PropertyChanges {
        target: displayedText
        color: ThemeController.style.blackColor
      }
      PropertyChanges {
        target: icon
        source: "qrc:/chevron-hover.svg"
      }
    },

    State {
      name: "pressedDesktop"
      when: button.pressed && (Qt.platform.os === "windows" ||
                               Qt.platform.os === "osx")
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.whisperColor
      }
      PropertyChanges {
        target: displayedText
        color: ThemeController.style.blackColor
      }
      PropertyChanges {
        target: icon
        source: "qrc:/chevron-hover.svg"
      }
    },

    State {
      name: "pressedMobile"
      when: button.pressed && (Qt.platform.os === "android" ||
                               Qt.platform.os === "ios")
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.whiteColor
      }
      PropertyChanges {
        target: displayedText
        color: ThemeController.style.charadeColor
        opacity: 0.5
      }
      PropertyChanges {
        target: icon
        source: "qrc:/chevron-default.svg"
        opacity: 0.5
      }
    }
  ]
}
