import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0

Rectangle {
  implicitWidth: parent.width
  implicitHeight: ThemeController.style.defaultInputHeight
  color: ThemeController.style.whiteSmokeColor

  property string accountName: ""

  BaseText {
    id: label
    anchors {
      left: parent.left
      leftMargin: app.isPhone ? s(28) : s(16)
      verticalCenter: parent.verticalCenter
    }
    font.weight: Font.DemiBold
    text: Strings.account
  }

  BaseText {
    id: emailLabel
    anchors {
      left: label.right
      leftMargin: ThemeController.style.margin.m12
      verticalCenter: parent.verticalCenter
    }
    text: accountName
  }
}
