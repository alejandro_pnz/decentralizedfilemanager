import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import UIFragments 1.0
import Qt.labs.platform 1.1

Item {
  id: pickerDelegate
  property string labelText: ""
  property bool inputRequired: false
  property string placeholderTxt: ""
  property bool isPassPhrase: false
  property bool leftEyeIcon: true
  property string leftIconSource: leftEyeIcon ? (textItem.echoMode === TextInput.Normal ? "qrc:/view-off.svg" : "qrc:/dark-eye.svg") : ""
  property real borderWidth: 1.0
  property color borderColor: ThemeController.style.textInput.defaultBorderColor
  property bool mainTextPickable: true
  property bool showIndicator: false
  property string pickerTitle: ""
  property bool autocheck: true
  property string fileId: ""
  property var fileStoragePath: ""
  property bool fileToImport: false
  property string selectFileTitle: Strings.selectCertificateFile
  property bool filterCertFiles: true

  property alias comboBox: combobox
  property alias textField: textItem
  property alias textRight: rightText
  property alias lbl: label
  property alias lblRequired: labelRequired
  property alias leftIconBtn: leftIconButton
  property alias pickerIndicator: indicator
  property alias picker: picker
  property alias buttonGroup: buttonGroup

  property var objectsModel
  implicitHeight: childrenRect.height
  implicitWidth: parent.width
  state: 'default'

  signal fieldButtonClicked()
  signal tabPressed()
  signal enterPressed()
  signal backtabPressed()

  function leftButtonClicked(){
    if(leftEyeIcon){
      isPassPhrase = !isPassPhrase
    }
  }

  function openPicker() {
    // Forcing active focus on textField make it possible to navigate via tab/backtab
    textField.forceActiveFocus()
    picker.open()
  }

  ColumnLayout {
    width: parent.width
    spacing: ThemeController.style.margin.m8

    RowLayout {
      spacing: ThemeController.style.margin.m4
      width: parent.width

      BaseText {
        id: label
        text: labelText
        font.weight: Font.DemiBold
        color: ThemeController.style.textInput.labelTextColor
      }

      BaseText {
        id: labelRequired
        text: "*"
        color: ThemeController.style.textInput.asteriskColor
        visible: inputRequired
      }

      Item {
        Layout.fillWidth: true
        Layout.preferredHeight: s(24)
      }
    }

    ComboBox {
      id: combobox

      model: objectsModel
      implicitWidth: parent.width
      implicitHeight: ThemeController.style.defaultInputHeight
      currentIndex: autocheck ? 0 : -1
      indicator: Item {
      }

      contentItem: RowLayout {
        spacing: ThemeController.style.margin.m4
        width: combobox.width
        height: combobox.height
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: ThemeController.style.margin.m12
        anchors.verticalCenter: parent.verticalCenter

        TextField {
          id: textItem

          text: mainTextPickable && combobox.currentIndex > -1 ? combobox.displayText : ""
          color: pickerDelegate.enabled ? ThemeController.style.textInput.textColor :
                                          ThemeController.style.textInput.disabledTextColor
          verticalAlignment: Text.AlignVCenter
          placeholderText: placeholderTxt
          placeholderTextColor: ThemeController.style.textInput.placeholderTextColor
          font.pixelSize: ThemeController.style.textInput.fontSize
          readOnly: mainTextPickable
          echoMode: isPassPhrase ? TextInput.Password : TextInput.Normal
          Layout.fillWidth: true

          onAccepted: {
            if (picker.visible) {
              picker.close()
            }
            tabPressed()
          }

          Keys.onTabPressed:  {
            if (picker.visible) {
              picker.close()
            }
            tabPressed()
          }

          Keys.onBacktabPressed: {
            if (picker.visible) {
              picker.close()
            }
            backtabPressed()
          }

          onPressed : {
            if(!picker.opened && readOnly) {
              picker.open()
            }
            else {
              picker.close()
            }
          }

          background: Item {
          }
        }

        BaseText {
          id: rightText
          text: combobox.displayText
          visible: !mainTextPickable
          color: ThemeController.style.textInput.placeholderTextColor
          rightPadding: s(8)
        }

        FieldIconButton {
          id: leftIconButton
          width: s(16)
          height: s(16)
          Layout.alignment: Qt.AlignVCenter
          iconSrc: leftIconSource
          active: true
          onClicked: {
            fieldButtonClicked()
            leftButtonClicked()
          }
        }

        Rectangle {
          id: separator
          height: s(24)
          width: s(1)
          color: ThemeController.style.seashellColor
          Layout.alignment: Qt.AlignVCenter
          visible: leftIconSource !== "" && showIndicator
        }

        UIKit.SvgImage {
          id: indicator
          width: s(16)
          height: s(16)
          Layout.alignment: Qt.AlignVCenter
          source: "qrc:/chevron-down.svg"
          visible: showIndicator
        }
      }

      background: Rectangle {
        radius: s(4)
        width: combobox.width
        height: combobox.height
        color: "#FFFFFF"
        border.color: borderColor
        border.width: s(borderWidth)
      }

      popup: UIKit.Picker {
        id: picker
        pickerModel: comboBox.delegateModel
        title: pickerTitle
        pickerHeight: contentItem.implicitHeight + s(122)
      }

      delegate: SortListDelegate {
        ButtonGroup.group: buttonGroup
        width: app.isPhone ? picker.width - s(32) : combobox.width
        txt: modelData
        separator: app.isPhone
        noVerticalMargins: app.isPhone
        action: actionSource
        checked: {
          if (autocheck) {
            return index === 0
          } else {
            return false
          }
        }
      }

      ButtonGroup {
        id: buttonGroup
        exclusive: true

        onClicked: {
          picker.close()
        }
      }
    }
  }

  Action {
    id: fromLibrary
    onTriggered: homeManager.enterSelectObjectPage({"title": selectFileTitle,
                                                    "subtitle": "",
                                                    "multipleSelection": false,
                                                    "shownObjects": SelectObjects.ShownObjects.File,
                                                    "showCertFiles": filterCertFiles})
  }

  Action {
    id: fromStorage
    onTriggered: fileDialog.open()
  }

  function basename(str)
  {
     return (String(str).slice(String(str).lastIndexOf("/")+1))
  }

  FileDialog {
    id: fileDialog
    acceptLabel: Strings.import
    folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
    fileMode: FileDialog.OpenFiles
    onAccepted: {
      fileStoragePath = files
      fileToImport = true
      textItem.text = basename(file)
    }
  }

  ListModel {
    id: objectsModel
  }

  Connections {
    target: homeManager
    function onFileChosen(name, id) {
      fileToImport = false
      fileId = id
      textItem.text = name
    }
  }

  Component.onCompleted: {
    objectsModel.append({ modelData: Strings.selectFromLibrary,
                          actionSource: fromLibrary
                       })
    objectsModel.append({ modelData: Strings.selectFromDeviceStorage,
                          actionSource: fromStorage
                       })
  }

  states: [
    State {
      name: "default"
      when: !combobox.hovered && !(picker.visible || (!mainTextPickable && textItem.activeFocus))
      PropertyChanges {
        target: pickerDelegate
        borderColor: ThemeController.style.textInput.defaultBorderColor
        borderWidth: 1
      }
    },

    State {
      name: "hovered"
      when: combobox.hovered && !(picker.visible || (!mainTextPickable && textItem.activeFocus))
      PropertyChanges {
        target: pickerDelegate
        borderColor: ThemeController.style.textInput.hoveredBorderColor
        borderWidth: 1
      }
    },

    State {
      name: "active"
      when: picker.visible || (!mainTextPickable && textItem.activeFocus)  || textItem.activeFocus
      PropertyChanges {
        target: pickerDelegate
        borderColor: ThemeController.style.textInput.activeBorderColor
        borderWidth: 1.5
      }
    }
  ]
}
