import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import com.test 1.0

Item {
  implicitWidth: parent.width
  implicitHeight: contentColumn.height
  property string email: ""
  property string contentText: ""

  signal blockClicked()
  signal rejectClicked()
  signal approveClicked()

  property alias bgRect: bgRect
  property alias avatar: iconContainer
  property alias title: userEmail
  property alias content: content
  property alias blockButton: buttonsRow.blockButton
  property alias rejectButton: buttonsRow.rejectButton
  property alias approveButton: buttonsRow.approveButton

  Rectangle {
    id : bgRect
    anchors.fill: parent
    color: ThemeController.style.deviceInformationDelegate.bgColor
    radius: s(4)
    border {
      color: ThemeController.style.deviceInformationDelegate.bdColor
      width: s(1)
    }

    ColumnLayout {
      id: contentColumn
      anchors {
        top: parent.top
        left: parent.left
        right: parent.right
      }
      spacing: ThemeController.style.margin.m8

      DeviceAvatar {
        id: iconContainer
        Layout.topMargin: ThemeController.style.margin.m16
        Layout.alignment: Qt.AlignHCenter
        size: ObjectAvatar.Size.Small
      }

      BaseText {
        id: userEmail
        Layout.alignment: Qt.AlignHCenter
        text: email
        font.weight: Font.DemiBold
      }

      BaseText {
        id: content
        Layout.alignment: Qt.AlignHCenter
        horizontalAlignment: Qt.AlignHCenter
        Layout.fillWidth: true
        Layout.leftMargin: ThemeController.style.margin.m16
        Layout.rightMargin: ThemeController.style.margin.m16
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        text: contentText
        size: BaseText.TextSize.Small
        color: ThemeController.style.deviceInformationDelegate.contentTextColor
      }

      Rectangle {
        Layout.topMargin: ThemeController.style.margin.m8
        Layout.fillWidth: true
        Layout.preferredHeight: s(1)
        color: ThemeController.style.seashellColor
      }

      Item {
        Layout.fillWidth: true
        Layout.preferredHeight: s(32)
        Layout.topMargin: ThemeController.style.margin.m8

        DeviceInfoButtons {
          id: buttonsRow
          anchors.centerIn: parent
        }
      }

      Item {
        Layout.preferredHeight: s(8)
        Layout.fillWidth: true
      }
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: bgRect
    source: bgRect
    color: ThemeController.style.deviceInformationDelegate.dropShadowColor
    radius: s(10)
    verticalOffset: s(2)
    //samples: radius * 2 + 1
  }
}
