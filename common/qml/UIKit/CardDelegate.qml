import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12

AbstractButton {
  id: control

  implicitHeight: mainLayout.height + topPadding + bottomPadding + (column.children.length > 0 ?
                  (column.childrenRect.height + column.anchors.topMargin) : 0)
  implicitWidth: s(343)

  property bool checkboxOnTheLeft: true
  property string title: ""
  property var config: ThemeController.style.cardDelegate
  property int textSpacing: ThemeController.style.margin.m4

  default property alias children: column.children

  // Switch visibility of action button
  property bool actionButton: false

  property alias avatar: objectAvatar
  readonly property alias bgRect: bgRect
  readonly property alias titleComponent: titleComponent
  readonly property alias textComponent: textComponent
  readonly property alias dropShadow: dropShadow
  readonly property alias actionButtonComponent: actionButtonComponent

  property bool pointingHandCursor: false
  signal actionButtonClicked()
  signal textLinkActivated(var link)

  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
  topPadding: ThemeController.style.margin.m16
  bottomPadding: ThemeController.style.margin.m16

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      color: (app.isMobile && control.pressed) ? config.bgColorPressed : config.bgColor
      radius: s(4)
      border {
        color: control.checked ? config.bdColorSelected : config.bdColor
        width: (app.isMobile && control.pressed) ? 0 : control.checked ? s(2) : s(1)
      }

      visible: !dropShadow.visible
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      source: bgRect
      transparentBorder: true
      color: control.hovered || control.checked ? config.dropShadowColor05 : config.dropShadowColor04
      visible: !control.pressed
      spread: 0.2
      radius: control.hovered ? s(30) : control.checked ? s(16) : s(10)
      verticalOffset: s(2)
    }
  }


  contentItem: Item {
    anchors.fill: parent
    anchors.leftMargin: control.leftPadding
    anchors.rightMargin: control.rightPadding
    anchors.topMargin: control.topPadding
    anchors.bottomMargin: control.bottomPadding

    RowLayout {
      id: mainLayout
      anchors.top: parent.top
      anchors.left: parent.left
      anchors.right: parent.right
      spacing: ThemeController.style.margin.m12

      ObjectAvatar {
        id: objectAvatar
        Layout.alignment: Qt.AlignTop
        size: ObjectAvatar.Size.Smallest
        iconSrc: control.icon.source
        color: (control.hovered || control.checked) ? ThemeController.style.slatePurpleColorOpacity10 : ThemeController.style.whiteSmokeColor
      }

      Column {
        spacing: textSpacing
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignVCenter
        BaseText {
          id: titleComponent
          width: parent.width - (actionButtonComponent.visible ? (actionButtonComponent.width + s(8)) : 0)
          elide: Text.ElideRight
          font.weight: Font.DemiBold
          text: control.title
          color: (control.hovered || control.checked) ? ThemeController.style.blackColor : config.titleTextColor
        }
        BaseText {
          id: textComponent
          width: parent.width
          elide: Text.ElideRight
          text: control.text
          size: BaseText.TextSize.Small
          color: config.textColor
          visible: text.length > 0
          onLinkActivated: textLinkActivated(link)
        }
      }

    }

    Column {
      id: column
      anchors {
        left: parent.left
        right: parent.right
        top: mainLayout.bottom

        topMargin: ThemeController.style.margin.m12
        leftMargin: objectAvatar.width + mainLayout.spacing
      }
    }

    StyledButton {
      id: actionButtonComponent
      type: StyledButton.ButtonStyle.Text
      displayMode: StyledButton.DisplayMode.TextOnly
      visible: control.actionButton
      onClicked: actionButtonClicked()
      buttonStyle: config.actionButtonStyle
      text: "Edit"

      anchors.top: parent.top
      anchors.right: parent.right
    }
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: pointingHandCursor ? Qt.PointingHandCursor : Qt.ArrowCursor
  }
}
