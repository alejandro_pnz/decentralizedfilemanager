import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import com.test 1.0
import com.testintl 1.0

AbstractButton {
    id: control

    implicitHeight: s(184)

    checked: selected

    property ItemSelectionModel selectionModel: null
    property var objectsModel: null
    property bool selectionMode: false
    property bool selected: false


    property string deviceName: ""
    property string deviceOSAndVersion: ""
    property var config: ThemeController.style.objectDelegate

    property bool actionButton: false
    property string actionButtonIconSource: "qrc:/more-2.svg"

    // This method will be invoked on press and hold event
    property var pressAndHoldHandler: function handlePressAndHold() {
        // Press&hold is supported only on mobiles
        if (app.isMobile) {
            check()
        }
    }

    property bool online: true
    property bool pending: false
    property bool deleted: false

    property alias avatar: objectAvatar
    property alias bgRect: bgRect
    readonly property alias actionButtonComponent: actionButtonComponent

    property string deviceTypeToString: {
      switch (avatar.deviceType) {
      case DeviceObject.Tablet: return Strings.deviceDesktop
      case DeviceObject.Laptop: return Strings.deviceTablet
      case DeviceObject.Phone: return  Strings.deviceMobile
      }
    }

    signal actionButtonClicked()
    signal delegateClicked()

    enum SyncMode {
        Syncing,
        OutOfSync,
        Synced
    }

    property bool syncing: false
    property bool outOfSync: false
    property int syncMode: {
        if (syncing) return DeviceDelegate.SyncMode.Syncing
        else if (outOfSync) return DeviceDelegate.SyncMode.OutOfSync
        return DeviceDelegate.SyncMode.Synced
    }

    onPressAndHold: pressAndHoldHandler()

    onClicked: {
        console.log("Clicked", selectionMode)
        if (selectionMode) {
            check()
        } else {
            delegateClicked()
        }
    }

    background: Item {
        Rectangle {
            id : bgRect
            anchors.fill: parent
            color: control.pressed ? config.bgColorPressed : config.bgColor
            radius: s(8)
            border {
                color: control.selected ? config.bdColorSelected : config.bdColor
                width: s(control.selected ? 2 : 1)
            }
            visible: !dropShadow.visible
        }
        DropShadow {
          id: dropShadow
          anchors.fill: bgRect
          source: bgRect
          transparentBorder: true
          color: control.hovered || control.selected ? config.dropShadowColor05 : config.dropShadowColor04
          visible: !control.pressed
          spread: 0.2
          radius: control.hovered ? s(30) : control.selected ? s(16) : s(10)
          verticalOffset: s(2)
        }
    }

    contentItem: Item {
        id: item
        anchors.fill: parent

        CheckIndicator {
            id: checkBox
            visible: !control.pending && !control.deleted
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: ThemeController.style.margin.m12
            anchors.leftMargin: ThemeController.style.margin.m16
            checked: control.selected
            implicitHeight: s(24)
            implicitWidth: s(24)
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (selectionMode)
                        control.clicked()
                    else
                        check()
                }
            }
        }

        ColumnLayout {
            id: column
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: ThemeController.style.margin.m12
            anchors.bottomMargin: ThemeController.style.margin.m12
            anchors.rightMargin: isMobile ? 0 : ThemeController.style.margin.m40
            anchors.leftMargin: isMobile ? 0 : ThemeController.style.margin.m40
            spacing: ThemeController.style.margin.m8
            DeviceAvatar {
                id: objectAvatar
                Layout.alignment: Qt.AlignHCenter
                size: DeviceAvatar.Size.Big
                syncing: control.syncing
                outOfSync: control.outOfSync
                //blocked: true
                bottomIcon: true
            }
            RowLayout {
                spacing: ThemeController.style.margin.m4
                Layout.alignment: Qt.AlignHCenter
                StatusItem {
                    id: status
                    isOnline: control.online
                    visible: !control.pending
                    Layout.alignment: Qt.AlignVCenter
                }
                BaseText {
                    id: titleComponent
                    elide: Text.ElideRight
                    font.weight: Font.DemiBold
                    horizontalAlignment: Qt.AlignHCenter
                    Layout.alignment: Qt.AlignVCenter
                    text: control.pending ? control.deviceTypeToString : control.deviceName
                    color: config.titleTextColor
                    property int availableWidth: status.visible ? column.width - status.width - s(4) : column.width
                    Layout.preferredWidth: textMetricsTitle.advanceWidth(text) > availableWidth ? availableWidth : textMetricsTitle.advanceWidth(text)
                }
            }

            RowLayout {
                Layout.alignment: Qt.AlignHCenter
                spacing: ThemeController.style.margin.m4
                StatusItem {
                    id: pendingStatus
                    colorRectangle.color: ThemeController.style.carnationColor
                    visible: control.pending
                    Layout.alignment: Qt.AlignVCenter
                }

                BaseText {
                    id: deviceOSAndVersionText
                    elide: Text.ElideRight
                    size: BaseText.TextSize.Small
                    font.weight: control.pending ? Font.DemiBold : Font.Normal
                    color: control.pending ? ThemeController.style.carnationColor : ThemeController.style.shuttleColor
                    visible: text.length > 0
                    text: control.pending ? "Pending" : deviceOSAndVersion
                    property int availableWidth: pendingStatus.visible ? column.width - pendingStatus.width - s(4) : column.width
                    Layout.preferredWidth: textMetricsStatus.advanceWidth(text) > availableWidth ? availableWidth : textMetricsStatus.advanceWidth(text)
                }
            }
        }

        LargeIconButton {
            id: actionButtonComponent
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin: ThemeController.style.margin.m12
            anchors.rightMargin: ThemeController.style.margin.m12
            implicitWidth: s(24)
            implicitHeight: s(24)
            buttonStyle: config.actionButton
            visible: control.actionButton
            icon.source: control.actionButtonIconSource
            onClicked: actionButtonClicked()
            onPressAndHold: control.pressAndHold()
        }
    }

    FontMetrics {
      id: textMetricsTitle
      font.pixelSize: titleComponent.pixelSize
      font.weight: titleComponent.font.weight
      font.family: titleComponent.font.family
    }

    FontMetrics {
      id: textMetricsStatus
      font.pixelSize: deviceOSAndVersionText.pixelSize
      font.weight: deviceOSAndVersionText.font.weight
      font.family: deviceOSAndVersionText.font.family
    }

    function check() {
        if (objectsModel){
            selectionModel.select(objectsModel.mapToSource(objectsModel.index(index, 0)), ItemSelectionModel.Toggle)
        }
    }

    Connections {
        target: selectionModel
        function onSelectionChanged() {
            control.selected = selectionModel.isSelected(objectsModel.mapToSource(objectsModel.index(index, 0)))
        }
    }
}

