import QtQuick 2.15
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import QtQuick.Controls 2.15
import QtQml.Models 2.15

Item {
  id: tagDelegate
  implicitHeight: s(56)
  implicitWidth: s(100)

  property string tagText: ""
  property string tagColor: ""
  property bool selectionMode: false
  property bool separatorVisible: true

  property alias tagObject: tag
  property alias iconButton: iconButton
  property alias iconObject: selectionImage

  property int index: -1
  property var selectionModel : null
  property var delegateModel: null
  property bool selected: false

  signal clicked()

  Tag {
    id: tag
    text: tagText
    tagColor: tagDelegate.tagColor
    anchors.left: parent.left
    anchors.verticalCenter: parent.verticalCenter

    onClicked: {
      if(selectionMode){
        console.log("Clicked")
        check()
      } else {
        tagDelegate.clicked()
      }
    }
  }

  Rectangle {
    anchors.bottom: parent.bottom
    height: s(1)
    width: parent.width
    color: ThemeController.style.seashellColor
    visible: separatorVisible
  }

  UIKit.SvgImage {
    id: selectionImage
    width: s(16)
    height: s(16)
    anchors.right: parent.right
    anchors.verticalCenter: parent.verticalCenter
  }

  UIKit.LargeIconButton {
    id: iconButton
    anchors.verticalCenter: parent.verticalCenter
    anchors.right: parent.right
    height: s(24)
    width: s(24)
    onClicked: {
    }
  }

  function check() {
    if (delegateModel){
      console.log(tagDelegate.index)
      selectionModel.select(delegateModel.mapToSource(delegateModel.index(tagDelegate.index, 0)), ItemSelectionModel.Toggle)
    }
  }

  Connections {
    target: selectionModel
    function onSelectionChanged() {
      var idx = delegateModel.mapToSource(delegateModel.index(tagDelegate.index, 0))
      tagDelegate.selected = selectionModel.isSelected(idx)
      console.log("Selection changed", tagDelegate.index, tagDelegate.selected, idx, selectionModel)
    }
  }

  states: [
    State {
      name: "default"
      when: selectionMode && !selected
      PropertyChanges {
        target: iconButton
        visible: false
      }
      PropertyChanges {
        target: selectionImage
        visible: false
      }
    },

    State {
      name: "selected"
      when: selectionMode && selected
      PropertyChanges {
        target: selectionImage
        source: "qrc:/check-mark.svg"
        visible: true
      }
      PropertyChanges {
        target: iconButton
        visible: false
      }
    },

    State {
      name: "more"
      when: !selectionMode
      PropertyChanges {
        target: iconButton
        icon.source: "qrc:/more-2.svg"
        visible: true
      }
      PropertyChanges {
        target: selectionImage
        visible: false
      }
    }
  ]
}
