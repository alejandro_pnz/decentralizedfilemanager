import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import AppStyles 1.0

QQC2.Button {
  id: button

  enum Size {
    Normal,
    Big
  }

  padding: ThemeController.style.margin.m4
  property int size: MenuItem.Size.Normal
  property string iconColor: "transparent"
  property bool hasPressedState: !(Qt.platform.os === "ios"
                                 || Qt.platform.os === "android")

  // Helper property
  readonly property bool _normalSize: size === MenuItem.Size.Normal

  property alias iconObject: icon
  property string iconDomain
  property string iconSrc
  property alias nameText: name
  readonly property alias bg: bgRect

  implicitHeight: s(_normalSize ? 49 : 65)
  implicitWidth: s(_normalSize ? 75 : 83)
  checkable: true

  background: Rectangle {
    id : bgRect
    radius: s(4)
  }

  contentItem: Item {
    anchors.fill: parent
    anchors.margins: padding

    ColoredImage {
      id: icon
      icon.asynchronous: true
      width: s(24)
      height: s(24)
      source: button.icon.source
      icon.fillMode: Image.PreserveAspectFit
      anchors.top: parent.top
      anchors.horizontalCenter: parent.horizontalCenter
    }

    BaseText {
      id: name
      text: button.text
      font.weight: _normalSize ? Font.DemiBold : Font.Normal
      anchors.bottom: parent.bottom
      anchors.horizontalCenter: parent.horizontalCenter
      horizontalAlignment: Text.AlignHCenter
      size: _normalSize ? BaseText.TextSize.XSmall : BaseText.TextSize.Body
      width: parent.width
      elide: Text.ElideRight
    }
  }

  states: [
    State {
      when: pressed && hasPressedState
      name: "pressed"
      PropertyChanges {
        target: name
        color: ThemeController.style.menuItem.textColor
      }
      PropertyChanges {
        target: icon
        color: ThemeController.style.menuItem.iconColorPressed
        source: {
          if (button.iconDomain.length > 0) {
            return "qrc:/" + button.iconDomain + "-selected.svg";
          }
          return button.iconSrc;
        }
      }
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.menuItem.bgColorPressed
      }
    },
    State {
      name: "selected"
      when: checked
      PropertyChanges {
        target: name
        color: ThemeController.style.menuItem.textColorChecked
      }
      PropertyChanges {
        target: icon
        source: {
          if (button.iconDomain.length > 0) {
            return "qrc:/" + button.iconDomain + "-selected.svg";
          }
          return button.iconSrc;
        }
      }
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.menuItem.bgColor
      }
    },
    State {
      name: "default"
      when: !checked
      PropertyChanges {
        target: name
        color: ThemeController.style.menuItem.textColor
      }
      PropertyChanges {
        target: icon
        color: ThemeController.style.menuItem.iconColor
        source: {
          if (button.iconDomain.length > 0) {
            return "qrc:/" + button.iconDomain + "-default.svg";
          }
          return button.iconSrc;
        }
      }
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.menuItem.bgColor
      }
    }
  ]
}
