import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import QtQml.Models 2.12

Button {
  id: button

  implicitHeight: s(183)
  implicitWidth: s(167)

  property ItemSelectionModel selectionModel: null
  property var model: null
  property bool selected: false

  property string txt: ""
  property string contentTxt: ""
  property bool titleIcon: false
  property string titleIconSource: "qrc:/lock.svg"

  property alias avatarObject: objectAvatar
  property alias checkboxObject: checkbox
  property alias bg: bgRect
  property alias contentTextComponent: contentTextComponent
  property alias textObject: textLabel
  property alias actionButton: iconButton
  property alias dropShadow: dropShadow

  property bool outOfSync: false
  property bool selectionMode: false
  property var config: ThemeController.style.arcaMobileDelegate

  signal checkBoxClicked()
  signal actionButtonClicked()
  signal delegateClicked()
  signal textLinkActivated(var link)

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      radius: s(8)
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      horizontalOffset: 0
      verticalOffset: button.state === 'selected' ? 0 : s(2)
      radius: button.state === 'selected' ? s(16) : s(10)
      //samples: 2 * radius + 1
      color: button.state === 'selected' ? config.dropShadowSelectedColor
                                         : config.dropShadowColor
      source: bgRect
    }
  }

  contentItem: Item{
    id: item
    anchors.fill: parent

    CheckIndicator {
      id: checkbox
      anchors.top: parent.top
      anchors.topMargin: ThemeController.style.margin.m16
      anchors.left: parent.left
      anchors.leftMargin: ThemeController.style.margin.m16
      width: s(24)
      height: s(24)
      z: column.z + 1
      checked: button.selected
      MouseArea {
        id: checkArea
        anchors.fill: parent
        onClicked: {
          if (selectionMode)
            button.clicked()
          else
            check()
        }
      }
    }

    ColumnLayout {
      id: column
      anchors.top: parent.top
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.topMargin: ThemeController.style.margin.m12
      anchors.bottomMargin: ThemeController.style.margin.m12
      anchors.rightMargin: ThemeController.style.margin.m32
      anchors.leftMargin: ThemeController.style.margin.m32
      spacing: ThemeController.style.margin.m8

      ObjectAvatar {
        id: objectAvatar
        Layout.alignment: Qt.AlignHCenter
        state: outOfSync ? "out-of-sync" : "normal"
      }

      RowLayout {
        Layout.preferredWidth: childrenRect.width
        Layout.preferredHeight: childrenRect.height
        Layout.alignment: Qt.AlignHCenter
        spacing: ThemeController.style.margin.m8

        ColoredImage {
          id: lockIcon
          icon.width: s(16)
          icon.height: s(16)
          visible: button.titleIcon
          source: button.titleIconSource
          color: ThemeController.style.manateeColor
          Layout.alignment: Qt.AlignVCenter
        }

        UIKit.BaseText {
          id: textLabel
          text: txt
          font.weight: Font.DemiBold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignVCenter

          color: config.titleTextColor
          elide: Text.ElideRight
          property int availableWidth: lockIcon.visible ? column.width - lockIcon.width - s(8) :
                                                          column.width
          Layout.preferredWidth: textMetrics.advanceWidth(txt) > availableWidth ? availableWidth
                                 : textMetrics.advanceWidth(txt)
        }
      }

      UIKit.BaseText {
        id: contentTextComponent
        text: contentTxt
        color: config.textColor
        size: BaseText.TextSize.Small
        Layout.alignment: Qt.AlignHCenter
        elide: Text.ElideRight
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
        onLinkActivated: textLinkActivated(link)
        textFormat: Text.RichText
      }
    }
    UIKit.LargeIconButton {
      id: iconButton
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      anchors.bottomMargin: ThemeController.style.margin.m20
      icon.source: "qrc:/more-2.svg"
      buttonStyle: config.actionButton
      onClicked: actionButtonClicked()
    }
  }


  function check() {
    if (model){
      selectionModel.select(model.mapToSource(model.index(index, 0)), ItemSelectionModel.Toggle)
    }
  }

  onClicked: {
    if (selectionMode) {
      check()
    } else {
      delegateClicked()
    }
  }

  onPressAndHold: {
    if (app.isMobile) {
      check()
    }
  }

  FontMetrics {
    id: textMetrics
    font.pixelSize: textLabel.pixelSize
    font.weight: textLabel.font.weight
    font.family: textLabel.font.family
  }

  states: [
    State {
      name: "defaultMobile"
      when: !checkable && !selectionMode && !pressed
      PropertyChanges {
        target: bgRect
        color: config.bgColor
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: checkbox
        visible: false
      }
    },
    State {
      name: "default"
      when: checkable && !selectionMode && !pressed
      PropertyChanges {
        target: bgRect
        color: config.bgColor
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: checkbox
        visible: true
      }
    },

    State {
      name: "selected"
      when: selectionMode && checkbox.checked
      PropertyChanges {
        target: bgRect
        color: config.bgColor
        border.width: s(2)
        border.color: config.bdColorSelected
      }
      PropertyChanges {
        target: checkbox
        visible: true
      }
    },


    State {
      name: "edit"
      when: selectionMode && !checkbox.checked
      PropertyChanges {
        target: bgRect
        color: config.bgColor
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: checkbox
        visible: true
      }
    },

    State {
      name: "pressed"
      when: button.pressed
      PropertyChanges {
        target: bgRect
        color: config.bgColorPressed
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: checkbox
        visible: selectionMode
      }
    }
  ]

  Connections {
    target: selectionModel
    function onSelectionChanged() {
      button.selected = selectionModel.isSelected(model.mapToSource(model.index(index, 0)))
    }
  }
}
