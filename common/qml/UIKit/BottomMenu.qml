import QtQuick 2.15
import UIKit 1.0 as UIKit
import com.test 1.0
import Qt5Compat.GraphicalEffects


Item {
  implicitHeight: s(80)
  implicitWidth: parent.width
  default property alias children: footer.children
  property int menuMargins: 0

  UIKit.BaseFooter {
    id: footer
    anchors.fill: parent    
  }

  DropShadow {
    id: dropShadow
    anchors.fill: footer
    horizontalOffset: 0
    verticalOffset: s(8)
    radius: s(16)
    //samples: s(32)
    color: Qt.rgba(0, 0, 0, 0.05)
    source: footer
  }
}
