import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import com.test 1.0

Rectangle {
  id: roleLabel
  property string text: ""
  height: ThemeController.style.margin.m24
  width: roleText.width + ThemeController.style.margin.m24
  color: ThemeController.style.mercuryColor
  radius: s(100)

  BaseText {
    id: roleText
    anchors.centerIn: parent
    text: roleLabel.text
  }
}
