import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0

InputField {
  property int tipsButtonWidth: hintItem.width
  signal handleShowHint(point p)
  property alias tipButton: hintItem

  Item {
    id: hintItem
    width: s(84)
    height: s(24)
    anchors.right: parent.right

    SvgImage{
      source: "qrc:/tips-icon.svg"
      height: s(24)
      width: s(24)
      anchors.left: parent.left
      anchors.leftMargin: s(18)
    }

    BaseText {
      id: tipsLabel
      anchors.right: parent.right
      anchors.rightMargin: s(3)
      text: Strings.tips
      font.weight: Font.Bold
      color: ThemeController.style.textInput.tipTextColor
    }

    MouseArea {
      anchors.fill: parent
      onClicked: {
        handleShowHint(mapToGlobal(hintItem.x, (hintItem.y + hintItem.height) * .5))
      }
    }
  }
}
