import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import QtQml.Models 2.12

Button {
  id: button

  implicitHeight: s(183)
  implicitWidth: s(167)

  property ItemSelectionModel selectionModel: null
  property var model: null
  property bool selected: false

  property string title: ""
  property string contentText: ""
  property date lastSeenDate

  property bool titleIcon: false
  property string titleIconSource: "qrc:/power.svg"

  property alias avatarObject: objectAvatar
  property alias checkboxObject: checkbox
  property alias bg: bgRect
  property alias contentTextComponent: contentTextComponent
  property alias textObject: textLabel
  property alias actionButton: iconButton
  property alias dropShadow: dropShadow
  property alias statusTextComponent: statusText
  property alias stateRect: stateRect

  property bool online: true
  enum SyncMode {
    Syncing,
    OutOfSync,
    Synced
  }

  property int syncMode: DeviceDelegate.SyncMode.Synced

  property bool selectionMode: false
  property var config: ThemeController.style.arcaMobileDelegate

  signal actionButtonClicked()

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      radius: s(8)
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      horizontalOffset: 0
      verticalOffset: s(button.state === 'selected' ? 0 : 2)
      radius: s(button.state === 'selected' ? 16 : 10)
      //samples: 2 * radius + 1
      color: button.state === 'selected' ? config.dropShadowSelectedColor
                                         : config.dropShadowColor
      source: bgRect
    }
  }

  contentItem: Item{
    anchors.fill: parent

    CheckIndicator {
      id: checkbox
      anchors.top: parent.top
      anchors.topMargin: ThemeController.style.margin.m16
      anchors.left: parent.left
      anchors.leftMargin: ThemeController.style.margin.m16
      width: s(32)
      height: s(32)
      z: column.z + 1
      checkIcon.height: s(16)
      checkIcon.width: s(18)
      checked: button.selected
    }

    ColumnLayout {
      id: column
      anchors.top: parent.top
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.topMargin: ThemeController.style.margin.m12
      anchors.bottomMargin: ThemeController.style.margin.m12
      anchors.rightMargin: ThemeController.style.margin.m32
      anchors.leftMargin: ThemeController.style.margin.m32
      spacing: ThemeController.style.margin.m8

      DeviceAvatar {
        id: objectAvatar
        Layout.alignment: Qt.AlignHCenter
        size: DeviceAvatar.Size.Small
      }

      RowLayout {
        Layout.preferredWidth: childrenRect.width
        Layout.preferredHeight: childrenRect.height
        Layout.alignment: Qt.AlignHCenter
        spacing: ThemeController.style.margin.m8

        ColoredImage {
          id: titleIcon
          icon.width: s(16)
          icon.height: s(16)
          visible: button.titleIcon
          source: button.titleIconSource
          color: ThemeController.style.manateeColor
          Layout.alignment: Qt.AlignVCenter
        }

        UIKit.BaseText {
          id: textLabel
          text: button.title
          font.weight: Font.DemiBold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignVCenter

          color: config.titleTextColor
          elide: Text.ElideRight
          property int availableWidth: titleIcon.visible ? column.width - titleIcon.width - s(8) :
                                                           column.width
          Layout.preferredWidth: textMetrics.advanceWidth(button.title) > availableWidth ? availableWidth
                                                                                : textMetrics.advanceWidth(button.title)
        }
      }

      RowLayout {
        Layout.preferredWidth: childrenRect.width
        Layout.alignment: Qt.AlignHCenter

        UIKit.BaseText {
          id: contentTextComponent
          text: button.contentText
          color: config.textColor
          size: BaseText.TextSize.Small
        }

        Rectangle {
          Layout.preferredHeight: s(3)
          Layout.preferredWidth: s(3)
          radius: s(100)
          color: ThemeController.style.shuttleColor
        }

        Item {
          Layout.preferredHeight: s(10)
          Layout.preferredWidth: s(10)

          Rectangle {
            id: stateRect
            anchors.fill: parent
            border.width: s(1)
            radius: s(3)
            border.color: ThemeController.style.whiteColor
            color: online ? ThemeController.style.shamrockColor : ThemeController.style.manateeColor
          }

          DropShadow {
            id: stateShadow
            anchors.fill: stateRect
            horizontalOffset: 0
            verticalOffset: 0
            radius: s(4)
            //samples: s(9)
            color: Qt.rgba(0, 0, 0, 0.08)
            source: stateRect
          }
        }

        BaseText {
          id: statusText
          size: BaseText.TextSize.Small
          font.weight: button.syncMode === DeviceDelegate.SyncMode.Syncing ||
                       button.syncMode === DeviceDelegate.SyncMode.OutOfSync ?
                       Font.DemiBold : Font.Normal
          color: button.syncMode === DeviceDelegate.SyncMode.Syncing ?
                 ThemeController.style.slatePurpleColor : button.syncMode === DeviceDelegate.SyncMode.OutOfSync ?
                 ThemeController.style.carnationColor : ThemeController.style.shuttleColor
          visible: text.length > 0
          text: button.syncMode === DeviceDelegate.SyncMode.Syncing ?
                Strings.syncing : button.syncMode === DeviceDelegate.SyncMode.OutOfSync ?
                Strings.outOfSync : online ? Strings.onlineNow : Utility.formatLastSeen(lastSeenDate)
        }
      }
    }
    UIKit.LargeIconButton {
      id: iconButton
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      anchors.bottomMargin: ThemeController.style.margin.m20
      icon.source: "qrc:/more-2.svg"
      buttonStyle: config.actionButton
      onClicked: actionButtonClicked()
    }
  }

  onClicked: {
    if (selectionMode && model){
      selectionModel.select(model.mapToSource(model.index(index, 0)), ItemSelectionModel.Toggle)
    }
  }

  onPressAndHold: {
    if (Qt.platform.os === "android" ||
        Qt.platform.os === "ios") {
      selectionMode = true
      button.state = 'edit'
    }
  }

  FontMetrics {
    id: textMetrics
    font.pixelSize: textLabel.pixelSize
    font.weight: textLabel.font.weight
    font.family: textLabel.font.family
  }

  states: [
    State {
      name: "default"
      when: !selectionMode && !pressed
      PropertyChanges {
        target: bgRect
        color: config.bgColor
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: checkbox
        visible: false
      }
    },

    State {
      name: "selected"
      when: selectionMode && checkbox.checked
      PropertyChanges {
        target: bgRect
        color: config.bgColor
        border.width: s(2)
        border.color: config.bdColorSelected
      }
      PropertyChanges {
        target: checkbox
        visible: true
      }
    },


    State {
      name: "edit"
      when: selectionMode && !checkbox.checked
      PropertyChanges {
        target: bgRect
        color: config.bgColor
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: checkbox
        visible: true
      }
    },

    State {
      name: "pressed"
      when: button.pressed
      PropertyChanges {
        target: bgRect
        color: config.bgColorPressed
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: checkbox
        visible: selectionMode
      }
    }
  ]

  Connections {
    target: selectionModel
    onSelectionChanged: {
      button.selected = selectionModel.isSelected(model.mapToSource(model.index(index, 0)))
    }
  }
}
