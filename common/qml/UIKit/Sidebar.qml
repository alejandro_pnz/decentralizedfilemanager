import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0

import com.testintl 1.0

Rectangle {
  id: sidebar
  property string userEmail: AuthenticationManager.currentAccount ? AuthenticationManager.currentAccount.email : ""
  property bool smallMode: false
  property bool shrinked: smallMode

  color: ThemeController.style.whiteSmokeColor
  property alias libraryButton: libraryButton

  signal addButtonClicked()
  signal enterSwitchAccount()
  implicitHeight: parent.height
  implicitWidth: shrinked ? s(72) : s(300)

  onSmallModeChanged: {
    shrinked = smallMode
  }

  Rectangle {
    anchors.right: parent.right
    height: parent.height
    width: s(1)
    color: ThemeController.style.mercuryColor
    z: mainColumn.z + 1
  }

  Flickable {
    id: sidebarFlick
    anchors.fill: parent
    flickableDirection: Flickable.HorizontalFlick
    pressDelay: 0
    boundsMovement: Flickable.StopAtBounds
    onHorizontalVelocityChanged: console.log("HorizontalVelocity:", horizontalVelocity, horizontalOvershoot)
    onFlickStarted: {
      if (horizontalVelocity < 0) {
        if (shrinked && smallMode && app.isTablet) {
          shrinked = false
        }
        console.log("swiped right")
      }
      if (horizontalVelocity > 0) {
        if (!shrinked && smallMode && app.isTablet) {
          shrinked = true
        }
        console.log("swiped left")
      }
    }

  ColumnLayout {
    id: mainColumn
    anchors.fill: parent
    spacing: 0

    Item {
      id: logoItem
      Layout.preferredHeight: s(64)
      Layout.fillWidth: true
      Layout.leftMargin: shrinked ? s(16) : ThemeController.style.margin.m24
      Layout.rightMargin: Layout.leftMargin

      RowLayout {
        id: sidebarHeader
        height: s(40)
        anchors {
          left: parent.left
          right: parent.right
          verticalCenter: parent.verticalCenter
        }

        spacing: 0

        Item {
//          id: logoItem
          Layout.preferredHeight: parent.height
          Layout.preferredWidth: s(135)
          visible: !sidebar.shrinked

          UIKit.SvgImage {
            source: "qrc:/logo-horizontal.svg"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit            
          }

          MouseArea{
            anchors.fill: parent
            onClicked: Qt.openUrlExternally(app.companyWebsite())
            cursorShape: Qt.PointingHandCursor
          }
        }

        Rectangle {
          Layout.fillWidth: visible
          implicitHeight: 1
          visible: !shrinked
          color: ThemeController.style.transparent
        }

        UIKit.Avatar{
          id: avatarObject
          text: userEmail
          size: Avatar.Size.Small
          Layout.alignment: shrinked
                            ? Qt.AlignCenter
                            : Qt.AlignRight
          borderless: true

          StatusItem {
            isOnline: AuthenticationManager.online
            visible: true
            width: s(15)
            height: s(15)
            colorRectangle.radius: width / 2
            colorRectangle.border.width: s(2)
            colorRectangle.border.color: sidebar.color
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.rightMargin: -3
            anchors.bottomMargin: -3
          }

          MouseArea {
            anchors.fill: parent
            enabled: sidebar.shrinked
            onClicked: dropDownMenu.open()
          }

          UIKit.DropDownMenu {
            id: dropDownMenu
            x: 0
            y: avatarObject.height + s(8)
            currentUserEmail: userEmail
            onSwitchAccount: enterSwitchAccount()
          }
        }
        UIKit.LargeIconButton {
          id: dropDownButton
          Layout.leftMargin: ThemeController.style.margin.m4
          icon.source: "qrc:/chevron-hover.svg"
          onClicked: dropDownMenu.open()
          visible: !shrinked
        }
      }
    }

    ButtonGroup {
      id: buttonsGroup
      exclusive: true
    }


    Flickable {
      id: mainFlick
      boundsBehavior: Flickable.StopAtBounds
      Layout.topMargin: s(40)
      Layout.fillWidth: true
      Layout.fillHeight: true
      state: "active"
      clip: true


      property int spaceForSecondMenu: height - mainButtonsLayout.height
      property bool scrollAll: spaceForSecondMenu < s(150)
      states: [
        State {
          name: "normal"
          when: !mainFlick.scrollAll
          PropertyChanges {
            target: mainFlick
            contentHeight: height
          }
          PropertyChanges {
            target: flickable
            Layout.fillHeight: true
            Layout.preferredHeight: -1
          }
        },
        State {
          name: "active"
          when: mainFlick.scrollAll
          PropertyChanges {
            target: mainFlick
            contentHeight: buttonsColumn.implicitHeight
          }
          PropertyChanges {
            target: flickable
            Layout.fillHeight: false
            Layout.preferredHeight: content.height
          }
        }
      ]

      Item {
        id: buttonsItem
        anchors.fill: parent
        ColumnLayout {
          id: buttonsColumn
          anchors.fill: parent
          spacing: s(12)
          ColumnLayout {
            id: mainButtonsLayout
            spacing: s(2)
            UIKit.SidebarDelegate {
              id: homeButton
              Layout.fillWidth: true
              text: Strings.home
              iconDomain: "home"
              checked: true
              onClicked: homeManager.openPage(Pages.HomePage)
              ButtonGroup.group: buttonsGroup
              onCheckedChanged: console.log("Checked", checked, buttonsGroup.buttons.length)
              shrinked: sidebar.shrinked
            }

            UIKit.SidebarDelegate {
              id: libraryButton
              Layout.fillWidth: true
              text: Strings.library
              iconDomain: "library"
              onClicked: homeManager.enterLibraryPage()
              ButtonGroup.group: buttonsGroup
              shrinked: sidebar.shrinked
            }

            UIKit.SidebarDelegate {
              id: recycleBinButton
              Layout.fillWidth: true
              text: Strings.recycleBin
              iconDomain: "bin"
              valueVisible: RecycleBinModel.count > 0
              value: {
                if (RecycleBinModel.count >= 100) {
                  return "99+"
                }
                if (RecycleBinModel.count >= 1000) {
                  return "999+";
                }
                return RecycleBinModel.count
              }

              onClicked: homeManager.openPage(Pages.RecycleBinPage)
              ButtonGroup.group: buttonsGroup
              shrinked: sidebar.shrinked

              Rectangle {
                anchors.top: recycleBinButton.top
                anchors.right: recycleBinButton.right
                anchors.topMargin: s(4)
                anchors.rightMargin: s(4)

                width: s(20)
                height: s(20)
                radius: width / 2

                color: ThemeController.style.alizarinCrimson
                visible: shrinked && recycleBinButton.valueVisible

                UIKit.BaseText {
                  text: recycleBinButton.value
                  anchors.centerIn: parent
                  color: ThemeController.style.whiteColor
                  size: BaseText.TextSize.Small
                }
              }
            }

            Rectangle {
              height: s(1)
              Layout.topMargin: ThemeController.style.margin.m24
              Layout.leftMargin: shrinked ? 0 : ThemeController.style.margin.m24
              Layout.rightMargin: shrinked ? 0 : ThemeController.style.margin.m24
              Layout.fillWidth: true
              color: ThemeController.style.mercuryColor
            }
          }

          Flickable {
            id: flickable
            boundsBehavior: Flickable.StopAtBounds
            Layout.fillWidth: true
            Layout.fillHeight: true
            contentHeight: content.height
            clip: true

            ScrollBar.vertical: ScrollBar {
              parent: flickable
              anchors {
                top: flickable.top
                bottom: flickable.bottom
                right: flickable.right
              }

              width: s(8)
            }

            ColumnLayout {
              id: content
              width: parent.width
              spacing: 0

              UIKit.SidebarButtonsSection {
                id: boardsSection
                Layout.topMargin: ThemeController.style.margin.m12
                sectionName: Strings.boards
                Layout.fillWidth: true
                buttonsSpacing: s(2)
                contentHidden: homeManager.boardsSectionExpanded
                onSidebarSectionExpanded: {
                   homeManager.boardsSectionExpanded = expand
                }

                UIKit.SidebarDelegate {
                  width: parent.width
                  id: usersButton
                  text: Strings.users
                  iconDomain: "users"
                  onClicked: homeManager.openPage(Pages.UserDashboardPage)
                  ButtonGroup.group: buttonsGroup
                  onCheckedChanged: console.log("Checked users", checked)
                  shrinked: sidebar.shrinked
                }

                UIKit.SidebarDelegate {
                  width: parent.width
                  id: devicesButton
                  text: Strings.devices
                  iconDomain: "devices"
                  onClicked: homeManager.openPage(Pages.DeviceDashboardPage)
                  ButtonGroup.group: buttonsGroup
                  shrinked: sidebar.shrinked
                }

                UIKit.SidebarDelegate {
                  width: parent.width
                  id: dataButton
                  text: Strings.dataStr
                  iconDomain: "data"
                  onClicked: homeManager.openPage(Pages.DataDashboardPage)
                  ButtonGroup.group: buttonsGroup
                  shrinked: sidebar.shrinked
                }
              }

              UIKit.SidebarButtonsSection {
                id: toolsSection
                sectionName: Strings.tools
                Layout.topMargin: ThemeController.style.margin.m24
                Layout.fillWidth: true
                buttonsSpacing: s(2)
                contentHidden: homeManager.toolsSectionExpanded
                onSidebarSectionExpanded: {
                   homeManager.toolsSectionExpanded = expand
                }
                UIKit.SidebarDelegate {
                  width: parent.width
                  id: passwordGeneratorButton
                  text: Strings.passwordGenerator
                  iconDomain: "password-generator"
                  onClicked: homeManager.openPage(Pages.PasswordGeneratorPage)
                  ButtonGroup.group: buttonsGroup
                  shrinked: sidebar.shrinked
                }

                UIKit.SidebarDelegate {
                  width: parent.width
                  id: markdownEditorButton
                  text: Strings.markdownEditor
                  iconDomain: "markdown-editor"
                  onClicked: homeManager.enterCreateFile()
                  ButtonGroup.group: buttonsGroup
                  shrinked: sidebar.shrinked
                }

                UIKit.SidebarDelegate {
                  width: parent.width
                  id: importButton
                  text: Strings.importStr
                  iconDomain: "import"
                  ButtonGroup.group: buttonsGroup
                  shrinked: sidebar.shrinked
                }

                UIKit.SidebarDelegate {
                  width: parent.width
                  id: exportButton
                  text: Strings.exportStr
                  iconDomain: "export"
                  ButtonGroup.group: buttonsGroup
                  shrinked: sidebar.shrinked
                }
              }

              UIKit.SidebarButtonsSection {
                id: supportSection
                sectionName: Strings.supportAndInfo
                Layout.topMargin: ThemeController.style.margin.m20
                Layout.fillWidth: true
                itemsTopMargin: ThemeController.style.margin.m24
                buttonsSpacing: s(2)
                contentHidden: homeManager.supportSectionExpanded
                onSidebarSectionExpanded: {
                   homeManager.supportSectionExpanded = expand
                }
                UIKit.SidebarItem {
                  id: helpButton
                  text: Strings.helpResources
                  iconDomain: "help"
                  onClicked: homeManager.openPage(Pages.HelpPage)
                  ButtonGroup.group: buttonsGroup
                  shrinked: sidebar.shrinked
                }

                UIKit.SidebarItem {
                  id: supportButton
                  text: Strings.contactSupport
                  iconDomain: "support"
                  onClicked: homeManager.openPage(Pages.SupportPage)
                  ButtonGroup.group: buttonsGroup
                  shrinked: sidebar.shrinked
                }

                UIKit.SidebarItem {
                  id: aboutButton
                  text: Strings.aboutQuodArca
                  iconDomain: "info"
                  onClicked: homeManager.openPage(Pages.AboutPopupPage)
                  ButtonGroup.group: buttonsGroup
                  shrinked: sidebar.shrinked
                }

                UIKit.SidebarItem {
                  id: logsButton
                  text: "Logs"
                  iconDomain: "info"
                  onClicked: homeManager.openPage(Pages.LogsPage)
                  ButtonGroup.group: buttonsGroup
                  shrinked: sidebar.shrinked
                }
              }
            }
          }
        }

      }

    }

    Rectangle {
      id: bottomButton
      Layout.fillWidth: true
      Layout.preferredHeight: s(80)
      color: ThemeController.style.whiteSmokeColor

      Rectangle {
        height: 1
        width: parent.width
        color: ThemeController.style.mercuryColor
        anchors.bottom: parent.top
      }

      UIKit.StyledButton {
        id: addButton
        icon.source: "qrc:/plus-icon.svg"
        displayMode: UIKit.StyledButton.DisplayMode.IconOnly
        anchors.centerIn: sidebar.shrinked ? parent : undefined
        anchors.left: sidebar.shrinked ? undefined : parent.left
        anchors.leftMargin: sidebar.shrinked ? 0 : ThemeController.style.margin.m24
        anchors.verticalCenter: sidebar.shrinked ? undefined : parent.verticalCenter
      }

      UIKit.BaseText {
        text: Strings.add
        anchors.left: addButton.right
        anchors.leftMargin: ThemeController.style.margin.m12
        anchors.verticalCenter: parent.verticalCenter
        font.weight: Font.Bold
        visible: !sidebar.shrinked
      }

      MouseArea{
        id: addButtonArea
        anchors.fill: parent
        onClicked: {
          addButtonClicked()
        }
      }
    }
  }
  }
}
