import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0
//import Qt.labs.calendar 1.0
import "Calendar"

Item {
  id: calendar
  width: col.width
  height: col.implicitHeight + menu.implicitHeight

  property var locale: Qt.locale("en_US")

  // The range of calendar
  property date from: new Date(1900, 0, 1)
  property date to: new Date(2050, 11, 31)

  // The min/max dates that can be choosen
  property date minDate: new Date(1900, 0, 1)
  property date maxDate: new Date(2050, 11, 31)

  property date today: new Date()
  property var selectedDate: null

  signal dateChoosen(date d)

  // moves current view to desired year.
  // Hides year panel
  function switchToYear(year) {
    var date = new Date(year, listview.currentItem.month, 1)
    var index = calendarModel.indexOf(date)

    if (date > calendar.to) {
      index = calendarModel.count
    } else if (date < calendar.from) {
      index = 0
    }
    listview.currentIndex = index
    dropDown.checked = false
  }

  function dateInRange(date) {
    return (date >= minDate && date <= maxDate)
  }

  Component.onCompleted: {
    listview.currentIndex = Qt.binding(function(){
      return calendarModel.indexOf(calendar.selectedDate != null
                                   ? calendar.selectedDate : calendar.today);
    })
  }

  // Menu
  RowLayout {
    id: menu
    width: parent.width
    DropDownButton {
      id: dropDown
      text: new Date(listview.currentItem.year, listview.currentItem.month, 1).toLocaleString(calendar.locale, "MMM yyyy")
    }

    Item{
      height: s(1)
      Layout.fillWidth: true
    }

    NavigationButtons {
      onBack: {
        if (dropDown.checked) {
          yearPanel.back()
        } else {
          listview.currentIndex = Math.max(0, listview.currentIndex - 1)
        }
      }
      onForward: {
        if (dropDown.checked) {
          yearPanel.forward()
        } else {
          listview.currentIndex = Math.min(listview.count - 1, listview.currentIndex + 1)
        }
      }
    }
  }

  // Month view
  Column {
    id: col
    anchors.top: menu.bottom
    width: listview.width
    spacing: ThemeController.style.margin.m8
//    DayOfWeekRow {
//      locale: calendar.locale
//      width: listview.width
//      height: ThemeController.style.defaultInputHeight
//      delegate: Item {
//        width: s(48)
//        height: ThemeController.style.defaultInputHeight
//        UIKit.BaseText {
//          text: model.shortName
//          anchors.fill: parent
//          horizontalAlignment: Text.AlignHCenter
//          verticalAlignment: Text.AlignVCenter
//          color: ThemeController.style.calendar.dayOfWeekColor
//          size: UIKit.BaseText.TextSize.Small
//        }
//      }
//    }
    ListView {
      id: listview
      width: s(7*48 + 6 * 4); height: s(6*48 + 5 * 8)
      snapMode: ListView.SnapOneItem
      orientation: ListView.Horizontal
      highlightRangeMode: ListView.StrictlyEnforceRange
      highlightMoveDuration: -1
      highlightMoveVelocity: -1
      clip: true      

      ButtonGroup {
        id: buttonGroup
        exclusive: true
      }

//      model: CalendarModel {
//        id: calendarModel
//        from: calendar.from
//        to: calendar.to
//      }

//      delegate: MonthGrid {
//        spacing: ThemeController.style.margin.m4
//        width: listview.width
//        height: listview.height

//        month: model.month
//        year: model.year
//        locale: calendar.locale

//        delegate:  Item {
//          width: s(48)
//          height: s(52)
//          Button {
//            id: control
//            width: s(48)
//            height: ThemeController.style.defaultInputHeight
//            anchors.centerIn: parent
//            property bool isCurrentDate: model.year === calendar.today.getFullYear() && model.month === calendar.today.getMonth() && model.day === calendar.today.getDate()
//            enabled: dateInRange(new Date(model.year, model.month, model.day))
//            checked: calendar.selectedDate !== null
//                     ? model.year === calendar.selectedDate.getFullYear()
//                       && model.month === calendar.selectedDate.getMonth()
//                       && model.day === calendar.selectedDate.getDate()
//                     : false
//            onClicked: {
//              var date = new Date(model.year, model.month, model.day)
//              calendar.selectedDate =  new Date(model.year, model.month, model.day)
//              dateChoosen(new Date(model.year, model.month, model.day))
//            }
//            background: Item {
//              Rectangle {
//                anchors.fill: parent
//                color: control.checked ? ThemeController.style.calendar.bgColorChecked
//                                       : ThemeController.style.calendar.bgColor
//                border.color: control.hovered && !control.checked ? ThemeController.style.calendar.bdColorHover
//                                                                  : ThemeController.style.calendar.bdColor
//                border.width: s(1)
//                radius: s(24)
//              }
//            }

//            UIKit.BaseText {
//              text: model.day
//              anchors.fill: parent
//              horizontalAlignment: Text.AlignHCenter
//              verticalAlignment: Text.AlignVCenter
//              color: {
//                if (control.isCurrentDate && ! control.checked)
//                  return ThemeController.style.calendar.textColorCurrentDate
//                if (control.enabled) {
//                  if (control.checked) {
//                    return ThemeController.style.calendar.textColorChecked
//                  } else {
//                    return ThemeController.style.calendar.textColor
//                  }
//                } else {
//                  return ThemeController.style.calendar.textColorDisabled
//                }
//              }
//            }
//          }
//        }
//      }
    }
  }


  ButtonGroup {
    id: yearGroup
  }

  // Year
  YearPanel {
    id: yearPanel
    anchors.top: menu.bottom
    anchors.bottom: parent.bottom
    visible: dropDown.checked
    width: parent.width
    currentYear: listview.currentItem.year
    minDate: calendar.minDate
    maxDate: calendar.maxDate
    startFrom: calendar.from.getFullYear()
    fromYear: calendar.from.getFullYear()
    toYear: calendar.to.getFullYear()
    onVisibleChanged: {
      if (visible) {
        if (calendar.selectedDate != null) {
          yearPanel.centerOnYear(calendar.selectedDate.getFullYear())
        } else {
          yearPanel.centerOnYear(calendar.today.getFullYear())
        }
      }
    }
  }
}

