import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15


TextField {
  implicitHeight: ThemeController.style.defaultInputHeight
  implicitWidth: s(325)
  placeholderText: Strings.typeOrPasteAddress
  color: ThemeController.style.htmlLinkEditor.textColor
  font.pixelSize: ThemeController.style.font.bodySize
  font.family: ThemeController.style.font.primaryFontFamily
  placeholderTextColor: ThemeController.style.htmlLinkEditor.placeholderColor

  background: Rectangle {
    color: ThemeController.style.htmlLinkEditor.bgColor
    radius: s(2)
  }

  UIKit.CanvasTriangle {
    id: triangle
    width: s(12)
    height: s(9)
    color: ThemeController.style.htmlLinkEditor.bgColor
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.bottom: parent.top
    pointing: UIKit.CanvasTriangle.Pointing.Up
  }
}
