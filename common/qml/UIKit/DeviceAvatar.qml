import QtQuick 2.15
import AppStyles 1.0
import com.testintl 1.0

Rectangle {
  id: control
  enum Size { Big, Small }
  enum Mode { NoMode, Inactive, Blocked}

  property int size: DeviceAvatar.Size.Big

  property bool inactive: false
  property bool blocked: false
  property int avatarMode: {
    if (inactive) return DeviceAvatar.Mode.Inactive
    else if (blocked) return DeviceAvatar.Mode.Blocked
    return DeviceAvatar.Mode.NoMode
  }
  property bool syncing: false
  property bool outOfSync: false
  property int deviceType: DeviceObject.Unknown
  property bool bottomIcon: false

  Component.onCompleted: console.log(deviceType)
  property int pixelSize: {
    switch (control.size) {
    case DeviceAvatar.Size.Big: return s(103)
    case DeviceAvatar.Size.Small: return s(60)
    }
  }

  property int iconSize: {
    switch (control.size) {
    case DeviceAvatar.Size.Big: return s(55)
    case DeviceAvatar.Size.Small: return s(32)
    }
  }

  property int bottomIconSize: {
    switch (control.size) {
    case DeviceAvatar.Size.Big: return s(32)
    case DeviceAvatar.Size.Small: return s(24)
    }
  }

  property string bottomIconSource: {
    switch (control.avatarMode) {
    case DeviceAvatar.Mode.NoMode: return ""
    case DeviceAvatar.Mode.Inactive: return "qrc:/inactive-circle.svg"
    case DeviceAvatar.Mode.Blocked: return "qrc:/blocked-circle.svg"
    }
  }

  implicitHeight: pixelSize
  implicitWidth: pixelSize
  radius: width * 0.5

  readonly property alias iconComponent: iconComponent
  readonly property alias icon: icon

  ColoredImage {
    id: icon
    anchors.centerIn: parent
    icon.height: width
    icon.fillMode: Image.PreserveAspectFit
  }

  SvgImage {
    id: iconComponent
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    width: control.bottomIconSize
    height: width
    visible: bottomIcon
    source: bottomIconSource
  }

  states: [
    State {
      name: "tablet"
      when: deviceType == DeviceObject.Tablet
      PropertyChanges {
        target: icon
        source: "qrc:/tablet-white.svg"
        color: ThemeController.style.transparent
        icon.width: control.iconSize
      }
      PropertyChanges {
        target: control
        color: ThemeController.style.deviceAvatar.tabletBgColor
      }
    },
    State {
      name: "desktop"
      when: deviceType == DeviceObject.Laptop
      PropertyChanges {
        target: icon
        source: "qrc:/desktop.svg"
        color: ThemeController.style.transparent
        icon.width: control.iconSize
      }
      PropertyChanges {
        target: control
        color: ThemeController.style.deviceAvatar.desktopBgColor
      }
    },
    State {
      name: "mobile"
      when: deviceType == DeviceObject.Phone
      PropertyChanges {
        target: icon
        source: "qrc:/mobile.svg"
        color: ThemeController.style.transparent
        icon.width: control.iconSize
      }
      PropertyChanges {
        target: control
        color: ThemeController.style.deviceAvatar.mobileBgColor
      }
    },
    State {
      name: "virtual"
      //when: deviceType === DeviceObject.CloudStorage //TODO backend not implemented
      PropertyChanges {
        target: icon
        source: "qrc:/googledrive.svg"
        color: ThemeController.style.transparent
        icon.width: control.iconSize
      }
      PropertyChanges {
        target: control
        color: ThemeController.style.deviceAvatar.virtualBgColor
      }
    },
    State {
      name: "out-of-sync"
      when: outOfSync && !syncing
      PropertyChanges {
        target: icon
        source: "qrc:/out-of-sync.svg"
        color: ThemeController.style.deviceAvatar.outOfSyncIconColor
        icon.width: s(24)
      }
      PropertyChanges {
        target: control
        color: ThemeController.style.deviceAvatar.outOfSyncBgColor
      }
    },
    State {
      name: "syncing"
      when: syncing
      PropertyChanges {
        target: icon
        source: "qrc:/sync.svg"
        color: ThemeController.style.deviceAvatar.syncingIconColor
        icon.width: s(24)
      }
      PropertyChanges {
        target: control
        color: ThemeController.style.deviceAvatar.syncingBgColor
      }
    }
  ]
}
