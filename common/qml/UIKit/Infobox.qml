import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

Rectangle {
  id: box
  property string text
  property int textSize: BaseText.TextSize.Small
  property int iconSize: s(24)
  property string iconSource: "qrc:/info-circle.svg"
  implicitHeight: s(28 + 16) + contentText.contentHeight
  color: ThemeController.style.whiteSmokeColor
  radius: s(4)

  UIKit.SvgImage {
    source: iconSource
    width: iconSize
    height: iconSize
    anchors.top: parent.top
    anchors.topMargin: -ThemeController.style.margin.m12
    anchors.horizontalCenter: parent.horizontalCenter
  }

  UIKit.BaseText {
    id: contentText
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
      topMargin: ThemeController.style.margin.m24
      leftMargin: ThemeController.style.margin.m20
      rightMargin: ThemeController.style.margin.m20
    }
    text: box.text
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    horizontalAlignment: Qt.AlignHCenter
    size: textSize
  }
}
