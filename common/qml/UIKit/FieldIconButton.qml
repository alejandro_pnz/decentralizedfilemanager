import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import Qt5Compat.GraphicalEffects


Button {
  id: control

  property int radius: 0
  property alias iconSrc: control.icon.source
  property int iconWidth: s(16)
  property int iconHeight: s(16)
  property color iconColor
  property string _lastState: "default"
  property bool active: false
  property ButtonStyle style: ThemeController.style.fieldButton

  implicitWidth: s(16)
  implicitHeight: s(16)
  hoverEnabled: true
  state: "default"

  background: Rectangle {
    width: control.width
    height: control.height
    color: ThemeController.style.transparent
    border.color: ThemeController.style.transparent
    radius: control.radius
  }

  contentItem: Item {
    anchors.fill: parent
    SvgImage {
      id: imageIcon
      anchors.centerIn: parent
      source: control.iconSrc
      asynchronous: true
      width: control.iconWidth //* Utility.scaleFactor
      height: control.iconHeight //* Utility.scaleFactor
      fillMode: Image.PreserveAspectFit
      visible: false
    }
    ColorOverlay {
      id: imgOverlay
      anchors.fill: imageIcon
      source: imageIcon
      color: control.iconColor
//      scale: 1 / Utility.scaleFactor
    }
  }

  // Note: Order matters! Do not change it
  states: [
    State {
      name: "disabled"
      when: !enabled
      PropertyChanges {
        target: control;
        iconColor: style.iconColorDisabled
      }
    },
    State {
      name: "hover"
      when: hovered && enabled
      PropertyChanges {
        target: control;
        iconColor: style.iconColorHover
      }
    },
    State {
      name: "pressed"
      when: pressed && enabled
      PropertyChanges {
        target: control;
        iconColor: style.iconColorPressed
      }
    },
    State {
      name: "active"
      when: active && enabled
      PropertyChanges {
        target: control;
        iconColor: style.iconColorFocus
      }
    },
    State {
      name: "default"
      when: enabled
      PropertyChanges {
        target: control;
        iconColor: style.iconColor
      }
    }
  ]
}
