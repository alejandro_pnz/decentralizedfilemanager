import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12
import com.test 1.0
import QtQml.Models 2.12

AbstractButton {
  id: control

  implicitHeight: smallMode && !checked ? s(160) : s(92)
  implicitWidth: parent.width

  property string email: ""
  property string contentText: ""
  property string role: ""
  property int index: -1

  signal inviteClicked()
  signal rejectClicked()

  property alias bgRect: bgRect
  property alias avatar: iconContainer
  property alias title: userEmail
  property alias content: content
  property alias rejectButton: buttonsRow.rejectButton
  property alias inviteButton: buttonsRow.inviteButton
  property alias buttonsRow: buttonsRow
  property alias checkBox: checkBox
  property alias roleLabel: roleLabel

  property bool checkBoxMode: true
  property bool smallMode: false
  checkable: true
  property ItemSelectionModel selectionModel: null
  property var model: null

  onWidthChanged: {
    if((width  - leftPadding - checkBox.width - iconContainer.width - ThemeController.style.margin.m28
        - content.width - ThemeController.style.margin.m24) > buttonsRow.width){
      smallMode = false
    }
    else {
      smallMode = true
    }
  }

  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
  topPadding: ThemeController.style.margin.m16
  bottomPadding: ThemeController.style.margin.m16

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      color: ThemeController.style.userRequestDelegate.bgColor
      radius: s(4)
      border {
        color: control.checked ? ThemeController.style.userRequestDelegate.bdCheckedColor
                               : ThemeController.style.userRequestDelegate.bdColor
        width: s(control.checked ? 1.5 : 1)
      }
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      source: bgRect
      color: ThemeController.style.userRequestDelegate.dropShadowColor
      radius: control.checked ? ThemeController.style.margin.m16 : s(10)
      verticalOffset: control.checked && checkBoxMode ? ThemeController.style.margin.m8 : s(2)
      //samples: radius * 2 + 1
    }
  }

  contentItem: ColumnLayout {
    RowLayout {
      spacing: ThemeController.style.margin.m12

      CheckIndicator {
        id: checkBox
        Layout.alignment: Qt.AlignVCenter
        checked: control.checked
        implicitHeight: s(32)
        implicitWidth: s(32)
        checkIcon.width: s(18)
        checkIcon.height: s(15)
      }

      Avatar {
        id: iconContainer
        Layout.leftMargin: ThemeController.style.margin.m4
        Layout.alignment: Qt.AlignVCenter
        text: email
        bottomIcon: true
        editIcon: false
      }

      ColumnLayout {
        id: textColumn
        spacing: ThemeController.style.margin.m8
        Layout.fillWidth: true
        Layout.preferredWidth: Math.max(content.width, userEmail.width)

        Row {
          spacing: ThemeController.style.margin.m8
          BaseText {
            id: userEmail
            text: email
            font.weight: Font.DemiBold
          }

          RoleLabel {
            id: roleLabel
            text: role
            visible: role !== ""
          }
        }

        BaseText {
          id: content
          Layout.fillWidth: true
          Layout.maximumWidth: s(454)
          Layout.minimumWidth: s(286)
          wrapMode: Text.WrapAtWordBoundaryOrAnywhere
          text: contentText
          size: BaseText.TextSize.Small
          color: ThemeController.style.userRequestDelegate.contentTextColor
        }
      }

      Item {
        Layout.leftMargin: ThemeController.style.margin.m12
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.preferredWidth: s(182)
        visible: !smallMode

        UserRequestButtons {
          id: buttonsRow
          width: s(182)
          anchors.right: parent.right
          anchors.verticalCenter: parent.verticalCenter
          Layout.alignment: Qt.AlignVCenter
          visible: selectionModel ? !selectionModel.hasSelection && !smallMode : !smallMode
        }
      }
    }

    UserRequestButtons {
      id: bottomButtonsRow
      spacing: ThemeController.style.margin.m16
      Layout.preferredWidth: s(182)
      Layout.alignment: Qt.AlignVCenter
      Layout.leftMargin: s(64)
      visible: selectionModel ? !selectionModel.hasSelection && smallMode : smallMode
    }
  }

  onClicked: {
    if (checkable && model){
      selectionModel.select(model.mapToSource(model.index(index, 0)), ItemSelectionModel.Toggle)
    }
  }

  Connections {
    target: selectionModel
    function onSelectionChanged() {
      control.checked = selectionModel.isSelected(model.mapToSource(model.index(index, 0)))
    }
  }
}
