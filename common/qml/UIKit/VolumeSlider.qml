import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects


Slider {
  id: control
  from: 100
  to: 0
  value: 50
  stepSize: 1
  orientation: Qt.Vertical
  implicitWidth: s(4)
  implicitHeight: s(120)
  rotation: 180

  property alias bgRect: bgRect
  property alias selectedRect: selectedRect
  property alias handleRect: handleRect
  property var style: ThemeController.style.mediaSlider

  background: Rectangle {
    id: bgRect
    implicitWidth: parent.width
    implicitHeight: parent.height
    radius: s(100)
    color: style.bgColor

    Rectangle {
      id: selectedRect
      width: parent.width
      height: control.visualPosition * parent.height
      color: style.bgSelectedColor
      radius: s(100)
    }
  }

  handle: Item {
    visible: control.hovered

    Rectangle {
      id: handleRect
      x: control.implicitWidth * 0.5 - implicitWidth * 0.5
      y: control.visualPosition * control.availableHeight
      implicitWidth: ThemeController.style.margin.m16
      implicitHeight: ThemeController.style.margin.m16
      radius: s(100)
      color: style.handleBgColor
      border.color: style.handleBdColor
    }

    DropShadow {
      id: dropShadow
      anchors.fill: handleRect
      horizontalOffset: ThemeController.style.margin.m4
      verticalOffset: ThemeController.style.margin.m4
      radius: ThemeController.style.margin.m20
      //samples: 2 * radius + 1
      color: style.dropShadowColor
      source: handleRect
    }
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: Qt.PointingHandCursor
  }
}
