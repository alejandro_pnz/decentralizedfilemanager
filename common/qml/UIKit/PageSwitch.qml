import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import com.test 1.0
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12

Item {
  implicitHeight: app.isMobile ? s(56) : s(32)
  implicitWidth: app.isMobile ? s(173) : s(206)

  property int pagesCount: 0
  property int currentPage: 0

  property alias contentRect: contentRect
  property alias textFrame: textFrame
  property alias firstPageBtn: firstPageBtn
  property alias backBtn: backBtn
  property alias forwardBtn: forwardBtn
  property alias lastPageBtn: lastPageBtn

  signal firstPage()
  signal back()
  signal forward()
  signal lastPage()

  Rectangle {
    id: contentRect
    width: parent.width
    height: parent.height
    radius: s(16)
    color: ThemeController.style.pageSwitch.bgColor
    border.color: ThemeController.style.pageSwitch.bdColor
    border.width: app.isMobile ? s(1) : 0


    RowLayout {
      anchors.centerIn: parent
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.leftMargin: app.isMobile ? ThemeController.style.margin.m12 : 0
      anchors.rightMargin: app.isMobile ? ThemeController.style.margin.m12 : 0
      spacing: s(10)

      LargeIconButton {
        id: firstPageBtn
        icon.source: "qrc:/first-page.svg"
        iconWidth: s(14)
        iconHeight: s(14)
        onClicked: firstPage()
        enabled: currentPage > 1
      }

      LargeIconButton {
        id: backBtn
        visible: !app.isMobile
        icon.source: "qrc:/chevron-back.svg"
        onClicked: back()
        enabled: currentPage > 1
      }

      TextFrame {
        id: textFrame
        text: Strings.of.arg(currentPage).arg(pagesCount)
      }

      LargeIconButton {
        id: forwardBtn
        visible: !app.isMobile
        icon.source: "qrc:/chevron-forward.svg"
        onClicked: forward()
        enabled: currentPage < pagesCount
      }

      LargeIconButton {
        id: lastPageBtn
        icon.source: "qrc:/last-page.svg"
        iconWidth: s(14)
        iconHeight: s(14)
        onClicked: lastPage()
        enabled: currentPage < pagesCount
      }
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: contentRect
    visible: app.isMobile
    horizontalOffset: ThemeController.style.margin.m4
    verticalOffset: ThemeController.style.margin.m4
    radius: s(20)
    //samples: 2 * radius + 1
    color: ThemeController.style.pageSwitch.dropShadowColor
    source: contentRect
  }
}
