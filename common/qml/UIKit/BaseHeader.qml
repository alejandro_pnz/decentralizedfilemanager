import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0

ToolBar {
  id: header
  height: ThemeController.style.pageHeaderHeight
  background: Item{}

  default property alias children: content.children

  Item {
    id: content
    anchors {
      fill: parent
      leftMargin: ThemeController.style.margin.m24
      rightMargin: ThemeController.style.margin.m24
    }
  }

  Rectangle {
    height: s(1)
    width: parent.width
    color: ThemeController.style.mercuryColor
    anchors.top: parent.bottom
  }
}
