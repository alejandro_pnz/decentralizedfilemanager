import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

UIKit.PopupScreen {
  id: popup
  visible: false
  width: s(384)
  height: givePowerFragment.height + s(24)
  showCloseButton: false
  showBackButton: false
  parent: Overlay.overlay

  signal accepted()
  signal closed()

  Item {
    anchors{
      centerIn: parent
    }
    height: popup.height
    width: popup.width

    GivePower {
      id: givePowerFragment
      anchors {
        top: parent.top
        left: parent.left
        right: parent.right
        margins: ThemeController.style.margin.m24
      }
      onCloseClicked: {
        popup.closed()
        popup.close()
      }
      onAccept: {
        popup.accepted()
        popup.close()
      }
    }
  }
}
