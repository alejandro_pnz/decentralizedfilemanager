import QtQuick 2.15
import QtQuick.Controls 2.15

GenericPopup {
  Overlay.modal: Rectangle {
    color: Qt.rgba(0,0,0,0.4)
  }
}
