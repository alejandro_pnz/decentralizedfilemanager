import QtQuick 2.15
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects
import AppStyles 1.0


Popup {
  modal: true
  property color bdColor: ThemeController.style.whisperColor

  Overlay.modal: Item {}

  background: Item {
    id: bgComponent
    Rectangle {
      id: contentRect
      anchors.fill: parent
      visible: false
      radius: s(8)
      border {
        color: bdColor
        width: s(1)
      }
    }
    DropShadow {
      id: dropShadow
      anchors.fill: contentRect
      transparentBorder: true
      radius: s(32)
      spread: 0.2
      color: Qt.rgba(0, 0, 0, 0.08)
      source: contentRect
    }
  }
}
