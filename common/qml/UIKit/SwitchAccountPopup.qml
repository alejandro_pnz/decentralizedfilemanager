import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

UIKit.PopupScreen {
  id: popup
  visible: false
  width: s(588)
  height: Math.min(parent.height - 2 * s(80), s(460))
  showCloseButton: false
  showBackButton: false
  parent: Overlay.overlay

  Item {
    id: popupItem
    anchors{
      centerIn: parent
    }
    height: popup.height
    width: popup.width

    Item {
      id: popupHeader
      anchors {
        top: parent.top
        right: parent.right
        left: parent.left
      }
      height: s(80)

      UIKit.BaseText {
        id: titleComponent
        text: Strings.switchAccount
        font.weight: Font.DemiBold
        size: UIKit.BaseText.TextSize.H3
        anchors.centerIn: parent
      }

      UIKit.LargeIconButton {
        id: closeButton
        icon.source: "qrc:/close.svg"
        height: s(32)
        width: s(32)
        anchors {
          right: parent.right
          rightMargin: ThemeController.style.margin.m24
          verticalCenter: parent.verticalCenter
        }

        onClicked: popup.close()
      }
    }

    Rectangle {
      id: separator
      height: s(1)
      width: parent.width
      color: ThemeController.style.seashellColor
      anchors.top: popupHeader.bottom
    }

    SwitchAccount {
      popup: popup
      anchors {
        top: popupHeader.bottom
        bottom: bottomContent.top
        bottomMargin: ThemeController.style.margin.m24
        left: parent.left
        right: parent.right
        topMargin: ThemeController.style.margin.m24
        leftMargin: ThemeController.style.margin.m24
        rightMargin: ThemeController.style.margin.m24
      }
    }

    Item {
      id: bottomContent
      anchors {
        bottom: parent.bottom
        left: parent.left
        right: parent.right
        bottomMargin: ThemeController.style.margin.m24
        leftMargin: ThemeController.style.margin.m24
        rightMargin: ThemeController.style.margin.m24
      }
      height: s(24)
    }
  }
}
