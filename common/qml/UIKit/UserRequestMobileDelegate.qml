import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import com.test 1.0

Item {
  implicitWidth: parent.width
  implicitHeight: contentColumn.height
  property string email: ""
  property string contentText: ""
  property string role: ""

  signal inviteClicked()
  signal rejectClicked()

  property alias bgRect: bgRect
  property alias avatar: iconContainer
  property alias title: userEmail
  property alias content: content
  property alias rejectButton: buttonsRow.rejectButton
  property alias inviteButton: buttonsRow.inviteButton
  property alias roleLabel: roleLabel

  Rectangle {
    id : bgRect
    anchors.fill: parent
    color: ThemeController.style.userRequestDelegate.bgColor
    radius: ThemeController.style.margin.m4
    border {
      color: ThemeController.style.userRequestDelegate.bdColor
      width: s(1)
    }

    ColumnLayout {
      id: contentColumn
      anchors {
        top: parent.top
        left: parent.left
        right: parent.right
      }
      spacing: ThemeController.style.margin.m8

      Avatar {
        id: iconContainer
        Layout.topMargin: 16
        Layout.alignment: Qt.AlignHCenter
        text: email
        bottomIcon: true
        editIcon: false
      }

      Row {
        spacing: ThemeController.style.margin.m8
        Layout.alignment: Qt.AlignHCenter

        BaseText {
          id: userEmail
          Layout.alignment: Qt.AlignHCenter
          text: email
          font.weight: Font.DemiBold
        }

        RoleLabel {
          id: roleLabel
          text: role
          visible: role !== ""
        }
      }

      BaseText {
        id: content
        Layout.alignment: Qt.AlignHCenter
        horizontalAlignment: Qt.AlignHCenter
        Layout.fillWidth: true
        Layout.leftMargin: ThemeController.style.margin.m16
        Layout.rightMargin: ThemeController.style.margin.m16
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        text: contentText
        size: BaseText.TextSize.Small
        color: ThemeController.style.userRequestDelegate.contentTextColor
      }

      Rectangle {
        Layout.topMargin: ThemeController.style.margin.m8
        Layout.fillWidth: true
        Layout.preferredHeight: s(1)
        color: ThemeController.style.seashellColor
      }

      Item {
        Layout.fillWidth: true
        Layout.preferredHeight: ThemeController.style.margin.m32
        Layout.topMargin: ThemeController.style.margin.m8

        UserRequestButtons {
          id: buttonsRow
          anchors.centerIn: parent
        }
      }

      Item {
        Layout.preferredHeight: ThemeController.style.margin.m8
        Layout.fillWidth: true
      }
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: bgRect
    source: bgRect
    color: ThemeController.style.userRequestDelegate.dropShadowColor
    radius: s(10)
    verticalOffset: s(2)
    //samples: radius * 2 + 1
  }
}
