import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import com.test 1.0
import Qt5Compat.GraphicalEffects


Item {
  id: root
  implicitHeight: s(32)
  implicitWidth: contentRect.width

  property string text: ""
  property alias contentRect: contentRect
  property alias textItem: textItem

  Rectangle {
    id: contentRect
    width: textItem.width + ThemeController.style.margin.m32
    height: parent.height
    radius: s(16)
    color: ThemeController.style.textFrame.bgColor
    border.color: ThemeController.style.textFrame.bdColor
    border.width: s(1)

    BaseText {
      id: textItem
      anchors.centerIn: parent
      text: root.text
      font.weight: Font.Bold
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: contentRect
    horizontalOffset: ThemeController.style.margin.m4
    verticalOffset: ThemeController.style.margin.m4
    radius: s(20)
    //samples: 2 * radius + 1
    color: ThemeController.style.textFrame.dropShadowColor
    source: contentRect
  }
}
