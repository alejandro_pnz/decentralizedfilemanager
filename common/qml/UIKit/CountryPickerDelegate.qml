import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import SortFilterProxyModel 0.2

Item {
  id: pickerDelegate
  property string labelText: Strings.country
  property bool inputRequired: false
  property string placeholderTxt: Strings.selectCountry + "..."
  property bool isPassPhrase: false
  property bool leftEyeIcon: true
  property string leftIconSource: leftEyeIcon ? (textItem.echoMode === TextInput.Normal ? "qrc:/view-off.svg" : "qrc:/dark-eye.svg") : ""
  property real borderWidth: 1.0
  property color borderColor: ThemeController.style.textInput.defaultBorderColor
  property bool mainTextPickable: true
  property bool showIndicator: true
  property string pickerTitle: ""

  property alias comboBox: combobox
  property alias textField: textItem
  property alias textRight: rightText
  property alias lbl: label
  property alias lblRequired: labelRequired
  property alias leftIconBtn: leftIconButton
  property alias pickerIndicator: indicator
  property alias picker: picker
  property alias buttonGroup: buttonGroup

  property var objectsModel
  implicitHeight: childrenRect.height
  implicitWidth: parent.width
  state: 'default'

  signal fieldButtonClicked()
  signal tabPressed()
  signal enterPressed()
  signal backtabPressed()

  function leftButtonClicked(){
    if(leftEyeIcon){
      isPassPhrase = !isPassPhrase
    }
  }

  function openPicker() {
    // Forcing active focus on textField make it possible to navigate via tab/backtab
    textField.forceActiveFocus()
    picker.open()
  }


  ColumnLayout {
    width: parent.width
    spacing: 8

    RowLayout {
      spacing: 4
      width: parent.width

      BaseText {
        id: label
        text: labelText
        font.weight: Font.DemiBold
        color: ThemeController.style.textInput.labelTextColor
      }

      BaseText {
        id: labelRequired
        text: "*"
        color: ThemeController.style.textInput.asteriskColor
        visible: inputRequired
      }

      Item {
        Layout.fillWidth: true
        Layout.preferredHeight: 24
      }
    }

    ComboBox {
      id: combobox

      model: objectsModel
      implicitWidth: parent.width
      implicitHeight: 48

//      SortFilterProxyModel {
//        id: proxy
//        sourceModel: objectsModel
//        filters: [
//          RegExpFilter {
//            roleName: "name"
//            pattern: picker.searchText
//            caseSensitivity: Qt.CaseInsensitive
//          }
//        ]
//      }

      indicator: Item {
      }

      contentItem: RowLayout {
        spacing: 4
        width: combobox.width
        height: combobox.height
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: 12
        anchors.verticalCenter: parent.verticalCenter

        TextField {
          id: textItem
          text: mainTextPickable ? combobox.displayText : ""
          color: ThemeController.style.textInput.textColor
          verticalAlignment: Text.AlignVCenter
          placeholderText: placeholderTxt
          placeholderTextColor: ThemeController.style.textInput.placeholderTextColor
          font.pixelSize: ThemeController.style.textInput.fontSize
          readOnly: mainTextPickable
          echoMode: isPassPhrase ? TextInput.Password : TextInput.Normal
          Layout.fillWidth: true

          onAccepted: {
            if (picker.visible) {
              picker.close()
            }
            tabPressed()
          }

          Keys.onTabPressed:  {
            if (picker.visible) {
              picker.close()
            }
            tabPressed()
          }

          Keys.onBacktabPressed: {
            if (picker.visible) {
              picker.close()
            }
            backtabPressed()
          }

          onPressed : {
            if(!picker.opened && readOnly) {
              picker.open()
            }
            else {
              picker.close()
            }
          }

          background: Item {
          }
        }

        BaseText {
          id: rightText
          text: combobox.displayText
          visible: !mainTextPickable
          color: ThemeController.style.textInput.placeholderTextColor
          rightPadding: 8
        }

        FieldIconButton {
          id: leftIconButton
          width: 16
          height: 16
          Layout.alignment: Qt.AlignVCenter
          iconSrc: leftIconSource
          active: true
          onClicked: {
            fieldButtonClicked()
            leftButtonClicked()
          }
        }

        Rectangle {
          id: separator
          height: 24
          width: 1
          color: ThemeController.style.seashellColor
          Layout.alignment: Qt.AlignVCenter
          visible: leftIconSource !== "" && showIndicator
        }

        UIKit.SvgImage {
          id: indicator
          width: 16
          height: 16
          Layout.alignment: Qt.AlignVCenter
          source: "qrc:/chevron-down.svg"
          visible: showIndicator
        }
      }

      background: Rectangle {
        radius: 4
        width: combobox.width
        height: combobox.height
        color: "#FFFFFF"
        border.color: borderColor
        border.width: borderWidth
      }

      popup: UIKit.SelectCountryPicker {
        id: picker
        pickerModel: combobox.delegateModel
      }

      delegate: SortListDelegate {
        implicitHeight: app.isPhone ? s(56) : s(48)
        ButtonGroup.group: buttonGroup
        width: app.isPhone ? picker.width - s(32) : combobox.width
        txt: modelData
        bg.color: "transparent"
        separator: app.isPhone
        noVerticalMargins: app.isPhone
      }

      ButtonGroup {
        id: buttonGroup
        exclusive: true

        onClicked: {
          picker.close()
        }
      }
    }
  }

  states: [
    State {
      name: "default"
      when: !combobox.hovered && !(picker.visible || (!mainTextPickable && textItem.activeFocus))
      PropertyChanges {
        target: pickerDelegate
        borderColor: ThemeController.style.textInput.defaultBorderColor
        borderWidth: 1
      }
    },

    State {
      name: "hovered"
      when: combobox.hovered && !(picker.visible || (!mainTextPickable && textItem.activeFocus))
      PropertyChanges {
        target: pickerDelegate
        borderColor: ThemeController.style.textInput.hoveredBorderColor
        borderWidth: 1
      }
    },

    State {
      name: "active"
      when: picker.visible || (!mainTextPickable && textItem.activeFocus) || textItem.activeFocus
      PropertyChanges {
        target: pickerDelegate
        borderColor: ThemeController.style.textInput.activeBorderColor
        borderWidth: 1.5
      }
    }
  ]

  //example model
  ListModel {
    id: objectsModel
  }

  Component.onCompleted: {
    objectsModel.append({ modelData: "Afghanistan"});
    objectsModel.append({ modelData: "Albania"});
    objectsModel.append({ modelData: "Algeria"});
    objectsModel.append({ modelData: "Angola"});
    objectsModel.append({ modelData: "Antigua and Barbuda"});
    objectsModel.append({ modelData: "Argentina"});
    objectsModel.append({ modelData: "Georgia"});
    objectsModel.append({ modelData: "Afghanistan"});
    objectsModel.append({ modelData: "Albania"});
    objectsModel.append({ modelData: "Algeria"});
    objectsModel.append({ modelData: "Angola"});
    objectsModel.append({ modelData: "Antigua and Barbuda"});
    objectsModel.append({ modelData: "Argentina"});
    objectsModel.append({ modelData: "Georgia"});
  }
}
