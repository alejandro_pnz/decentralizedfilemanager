import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects

import AppStyles 1.0
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0

ComboBox {
  id: control

  property string comboBoxType: ""
  property var comboBoxModel

  property alias bgRect: bgRect
  property alias indicatorIcon: indicatorIcon
  property alias textLabel: textLabel
  property alias picker: picker

  state: 'default'

  rightPadding: ThemeController.style.margin.m12

  implicitWidth: parent.width
  implicitHeight: parent.height

  ListModel {
    id: chartsModel
  }

  Component.onCompleted:
  {
    if(comboBoxModel){
      chartsModel.append({ name: displayText })
      for(var i = 0; i< comboBoxModel.count; ++i){
        chartsModel.append({ name: comboBoxModel.get(i).label })
      }
    }
  }

  model: comboBoxModel
  background: Rectangle {
    id: bgRect
    implicitWidth: parent.width
    implicitHeight: parent.height
    radius: s(4)
  }

  contentItem: UIKit.BaseText {
    id: textLabel
    leftPadding: ThemeController.style.margin.m12
    rightPadding: control.indicator.width + control.spacing
    text: comboBoxType + ": " + control.displayText
    verticalAlignment: Text.AlignVCenter
    elide: Text.ElideRight
  }

  indicator: UIKit.SvgImage {
    id: indicatorIcon
    x: control.width - width - control.rightPadding
    y: control.topPadding + (control.availableHeight - height) / 2
    width: s(16)
    height: s(16)
    source: "qrc:/chevron-down.svg"
  }

  delegate: Item {}

  popup: UIKit.PanelPicker {
    id: picker
    model: chartsModel
    title: comboBoxType
    onItemClicked: {
      control.displayText = chartsModel.get(index).name
      control.popup.close()
    }
  }

  states: [
    State {
      name: "default"
      when: !control.hovered && !picker.visible
      PropertyChanges {
        target: bgRect
        border.color: ThemeController.style.textInput.defaultBorderColor
        border.width: s(1)
      }
    },

    State {
      name: "hovered"
      when: control.hovered && !picker.visible
      PropertyChanges {
        target: bgRect
        border.color: ThemeController.style.textInput.hoveredBorderColor
        border.width: s(1)
      }
    },

    State {
      name: "active"
      when: picker.visible
      PropertyChanges {
        target: bgRect
        border.color: ThemeController.style.textInput.activeBorderColor
        border.width: s(1.5)
      }
    }
  ]
}
