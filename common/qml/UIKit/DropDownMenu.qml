import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import com.testintl 1.0

import QtPromise 1.1

UIKit.GenericPopup {
  id: popup
  implicitWidth: s(397)
  implicitHeight: s(403)
  property string currentUserEmail: ""
  bdColor: ThemeController.style.whiteSmokeColor
  padding: 1
  signal switchAccount()


  ColumnLayout {
    id: columnContent
    anchors.fill: parent
    spacing: 0

    Item {
      id: headerItem
      Layout.fillWidth: true
      Layout.preferredHeight: s(56)
      Layout.leftMargin: ThemeController.style.margin.m16
      Layout.rightMargin: ThemeController.style.margin.m16
      Layout.topMargin: ThemeController.style.margin.m8

      RowLayout {
        id: headerRow
        spacing: ThemeController.style.margin.m12
        width: parent.width
        height: avatar.height
        anchors.verticalCenter: parent.verticalCenter

        Avatar {
          id: avatar
          text: currentUserEmail
          size: Avatar.Size.Custom
          borderless: true
        }

        BaseText {
          id: userLabel
          text: currentUserEmail
        }

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true
        }

        UIKit.LargeIconButton {
          id: iconButton
          icon.source: "qrc:/switch-account.svg"
          onClicked: {
            close()
            switchAccount()
          }
        }
      }
    }

    Rectangle {
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.fillWidth: true
      Layout.preferredHeight: s(1)
      color: ThemeController.style.whisperColor
      Layout.leftMargin: ThemeController.style.margin.m16
      Layout.rightMargin: ThemeController.style.margin.m16
    }

    UIKit.ActionListDelegate {
      id: myAccountButton
      Layout.topMargin: ThemeController.style.margin.m8
      text: Strings.myAccount
      icon.source: "qrc:/account-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      onClicked: {
        homeManager.openPage(Pages.MyAccountPage)
        popup.close()
      }
    }

    UIKit.BaseText {
      Layout.topMargin: ThemeController.style.margin.m16
      Layout.leftMargin: ThemeController.style.margin.m16
      size: BaseText.TextSize.Small
      font.weight: Font.DemiBold
      font.capitalization: Font.AllUppercase
      text: Strings.settings
      color: ThemeController.style.charadeColor
    }

    Rectangle {
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.fillWidth: true
      Layout.preferredHeight: s(1)
      color: ThemeController.style.whisperColor
      Layout.leftMargin: ThemeController.style.margin.m16
      Layout.rightMargin: ThemeController.style.margin.m16
    }

    UIKit.ActionListDelegate {
      id: globalSettingsButton
      Layout.topMargin: ThemeController.style.margin.m16
      text: Strings.global
      icon.source: "qrc:/cog-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      onClicked: {
        homeManager.enterSettingsPage(Enums.GlobalFragment)
        popup.close()
      }
    }

    UIKit.ActionListDelegate {
      id: userSettingsButton
      text: Strings.user
      icon.source: "qrc:/user-settings-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      onClicked:{
        homeManager.enterSettingsPage(Enums.UserFragment)
        popup.close()
      }
    }

    UIKit.ActionListDelegate {
      id: deviceSettingsButton
      text: Strings.device
      icon.source: "qrc:/device-settings-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      actionIcon: true
      onClicked: {
        homeManager.enterSettingsPage(Enums.DeviceFragment)
        popup.close()
      }
    }

    Rectangle {
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.fillWidth: true
      Layout.preferredHeight: s(1)
      color: ThemeController.style.whisperColor
      Layout.leftMargin: ThemeController.style.margin.m16
      Layout.rightMargin: ThemeController.style.margin.m16
    }

    UIKit.ActionListDelegate {
      id: logoutButton
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.bottomMargin: ThemeController.style.margin.m8
      text: Strings.logout
      icon.source: "qrc:/sign-out-default.svg"
      display: AbstractButton.TextBesideIcon
      Layout.fillWidth: true
      onClicked: {
        var future = Future.promise(AuthenticationManager.logout())

        future.then(function() {
          pageManager.backToLogin()
        },
        function(error) {
          console.log(error)
        })
      }
    }
  }
}
