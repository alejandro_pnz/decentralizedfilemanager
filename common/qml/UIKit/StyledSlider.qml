import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects


Slider {
  id: control
  from: 0
  to: 40
  value: 15
  stepSize: 1
  implicitWidth: parent.width

  property alias bgRect: bgRect
  property alias selectedRect: selectedRect
  property alias handleRect: handleRect
  property alias handleIcon: handleIcon

  background: Rectangle {
    id: bgRect
    y: control.topPadding + control.availableHeight / 2 - height / 2
    implicitWidth: parent.width
    implicitHeight: s(6)
    height: implicitHeight
    radius: s(100)
    color: ThemeController.style.styledSlider.bgColor

    Rectangle {
      id: selectedRect
      width: control.visualPosition * parent.width
      height: parent.height
      color: ThemeController.style.styledSlider.bgSelectedColor
      radius: s(100)
    }
  }

  handle: Item {
    Rectangle {
      id: handleRect
      x: control.visualPosition * (control.implicitWidth - width)
      y: control.topPadding + control.availableHeight / 2 - height / 2
      implicitWidth: s(32)
      implicitHeight: s(32)
      radius: s(100)
      color: ThemeController.style.styledSlider.handleBgColor
      border.color: ThemeController.style.styledSlider.handleBdColor

      UIKit.SvgImage {
        id: handleIcon
        width: s(10)
        height: s(10)
        source: "qrc:/slider-icon.svg"
        anchors.centerIn: parent
      }
    }

    DropShadow {
      id: dropShadow
      anchors.fill: handleRect
      horizontalOffset: 0
      verticalOffset: 0
      radius: s(4)
      //samples: 2 * radius + 1
      color: ThemeController.style.styledSlider.dropShadowColor
      source: handleRect
    }
  }
}
