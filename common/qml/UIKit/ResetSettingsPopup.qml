import QtQuick 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects


UIKit.GenericPopup {
  id: popup
  implicitWidth: s(375)
  implicitHeight: s(436)
  padding: ThemeController.style.margin.m16

  ColumnLayout {
    id: layout
    anchors{
      fill: parent
    }
    spacing: ThemeController.style.margin.m12

    UIKit.CardDelegate {
      Layout.fillWidth: true
      title: Strings.resetGlobalSettings
      text: Strings.resetAllGlobalSettings
      actionButton: false
      textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      textComponent.elide: Text.ElideNone
      topPadding: s(18)
      bottomPadding: s(18)
      avatar.visible: false
      textSpacing: ThemeController.style.margin.m4
      onClicked: homeManager.openPage(Pages.ResetConfirmationPopupPage)
    }

    UIKit.CardDelegate {
      Layout.fillWidth: true
      title: Strings.resetUserSettings
      text: Strings.resetAllUserSettings
      actionButton: false
      textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      textComponent.elide: Text.ElideNone
      topPadding: s(18)
      bottomPadding: s(18)
      avatar.visible: false
      textSpacing: ThemeController.style.margin.m4
    }

    UIKit.CardDelegate {
      Layout.fillWidth: true
      title: Strings.resetAllSettings
      text: Strings.resetAllAccountSettings
      actionButton: false
      textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      textComponent.elide: Text.ElideNone
      topPadding: s(18)
      bottomPadding: s(18)
      avatar.visible: false
      textSpacing: ThemeController.style.margin.m4
    }

    UIKit.CardDelegate {
      Layout.fillWidth: true
      title: Strings.resetTips
      text: Strings.resetAllTips
      actionButton: false
      textComponent.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      textComponent.elide: Text.ElideNone
      topPadding: s(18)
      bottomPadding: s(18)
      avatar.visible: false
      textSpacing: ThemeController.style.margin.m4
    }
  }
}
