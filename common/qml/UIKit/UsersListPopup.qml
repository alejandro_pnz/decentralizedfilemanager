import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import com.test 1.0
import UIFragments 1.0

Popup {
  id: popup
  implicitHeight: Math.min(s(436), fragment.componentsHeight + topPadding)
  implicitWidth: s(394)
  signal changedOwner(string owner)

  modal: false
  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
  topPadding: ThemeController.style.margin.m16
  bottomPadding: 0

  background: Item {
    Rectangle {
      id: bgComponent
      anchors.fill: parent
      radius: s(8)
    }
    DropShadow {
      id: dropShadow
      anchors.fill: bgComponent
      horizontalOffset: 0
      verticalOffset: 0
      radius: s(32)
      //samples: s(63)
      color: Qt.rgba(0, 0, 0, 0.08)
      source: bgComponent
    }
  }

  ChangeOwner {
    id: fragment
    anchors.fill: parent

    onOwnerChanged: {
      changedOwner(ownerName)
    }
  }
}
