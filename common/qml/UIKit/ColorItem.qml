import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit

Button {
  id: button

  property string itemColor: ""

  implicitHeight: ThemeController.style.defaultInputHeight
  implicitWidth: s(48)
  state: 'default'
  checkable: true

  property alias iconObject: icon
  property alias borderObject: outerRect
  property alias colorCircle: middleRect

  background: Item {
    Rectangle {
      id: outerRect
      anchors.fill: parent
      border.color: itemColor
      radius: s(100)

      Rectangle {
        id : middleRect
        height: s(40)
        width: s(40)
        radius: s(100)
        color: itemColor
        anchors.centerIn: parent

        SvgImage{
          id: icon
          height: s(16)
          width: s(16)
          anchors.centerIn: parent
          fillMode: Image.PreserveAspectFit
          source: "qrc:/check-icon.svg"
        }
      }
    }
  }

  onCheckedChanged: {
    if(checked) {
      button.state = 'selected'
    }
    else {
      button.state = 'default'
    }
  }

  states: [
    State {
      name: "selected"
      PropertyChanges {
        target: outerRect
        border.width: s(1.5)
      }
      PropertyChanges {
        target: icon
        visible: true
      }
    },

    State {
      name: "default"
      PropertyChanges {
        target: outerRect
        border.width: 0
      }
      PropertyChanges {
        target: icon
        visible: false
      }
    }
  ]
}
