import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit

Rectangle{
  id: infoBox
  property string errorMsg: ""
  color: ThemeController.style.infobox.bgColor
  radius: s(4)
  implicitHeight: contentRow.height + s(17)
  implicitWidth: parent.width

  property alias icon: infoIcon
  property alias label: errorLabel

  Row{
    id: contentRow
    anchors.verticalCenter: parent.verticalCenter
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.leftMargin: ThemeController.style.margin.m12
    anchors.rightMargin: ThemeController.style.margin.m12
    spacing: ThemeController.style.margin.m8

    SvgImage{
      id: infoIcon
      source: "qrc:/infobox-icon.svg"
      width: s(15)
      height: s(15)
      fillMode: Image.PreserveAspectFit
    }

    UIKit.BaseText {
      id: errorLabel
      text: errorMsg
      size: UIKit.BaseText.TextSize.Small
      font.weight: Font.Bold
      color: ThemeController.style.infobox.textColor
      width: parent.width - infoIcon.width
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      lineHeight: s(16)
    }
  }
}
