import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects

import QtQuick.Controls 2.15

Popup {
  implicitWidth: s(40)
  implicitHeight: s(136)
  modal: false
  Overlay.modal: Item{}
  property alias slider: slider
  property var style: ThemeController.style.volumeControl
  background: Item {
    Rectangle {
      id: bgRect
      anchors{
        fill: parent
      }
      radius: s(8)
      color: style.bgColor
      border.color: style.bdColor
      border.width: s(1)
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      source: bgRect
      color: style.dropShadowColor
      radius: s(32)
      verticalOffset: 0
      horizontalOffset: 0
      //samples: radius * 2 + 1
    }
  }

  VolumeSlider {
    id: slider
    anchors {
      top: parent.top
      topMargin: ThemeController.style.margin.m8
      bottom: parent.bottom
      bottomMargin: ThemeController.style.margin.m8
      horizontalCenter: parent.horizontalCenter
    }
  }
}
