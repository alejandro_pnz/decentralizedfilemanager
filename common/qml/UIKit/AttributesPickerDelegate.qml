import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

Item {
  id: pickerDelegate
  property string labelText: ""
  property bool inputRequired: false
  property string placeholderTxt: ""
  property bool isPassPhrase: false
  property real borderWidth: s(1.0)
  property color borderColor: ThemeController.style.textInput.defaultBorderColor
  property bool mainTextPickable: true
  property bool showIndicator: true
  property string pickerTitle: ""

  property alias comboBox: combobox
  property alias textField: textItem
  property alias lbl: label
  property alias lblRequired: labelRequired
  property alias pickerIndicator: indicator
  property alias picker: picker
  property alias buttonGroup: buttonGroup
  property var pickerModel: objectsModel

  property bool isMobile: Qt.platform.os === "android" || Qt.platform.os === "ios"

  implicitHeight: childrenRect.height
  implicitWidth: parent.width
  state: 'default'

  signal fieldButtonClicked()

  ColumnLayout {
    width: parent.width
    spacing: ThemeController.style.margin.m8

    RowLayout {
      spacing: ThemeController.style.margin.m4
      width: parent.width

      BaseText {
        id: label
        text: labelText
        font.weight: Font.DemiBold
        color: ThemeController.style.textInput.labelTextColor
      }

      BaseText {
        id: labelRequired
        text: "*"
        color: ThemeController.style.textInput.asteriskColor
        visible: inputRequired
      }

      Item {
        Layout.fillWidth: true
        Layout.preferredHeight: ThemeController.style.margin.m24
      }
    }

    ComboBox {
      id: combobox

      model: pickerModel
      implicitWidth: parent.width
      implicitHeight: ThemeController.style.defaultInputHeight

      indicator: Item {
      }

      contentItem: RowLayout {
        spacing: ThemeController.style.margin.m4
        width: combobox.width
        height: combobox.height
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter

        TextField {
          id: textItem

          text: mainTextPickable ? combobox.displayText : ""
          color: ThemeController.style.textInput.textColor
          verticalAlignment: Text.AlignVCenter
          placeholderText: placeholderTxt
          placeholderTextColor: ThemeController.style.textInput.placeholderTextColor
          font.pixelSize: ThemeController.style.textInput.fontSize
          readOnly: mainTextPickable ? true : false
          echoMode: isPassPhrase ? TextInput.Password : TextInput.Normal
          Layout.fillWidth: true

          onPressed : {
            if(!picker.opened && readOnly) {
              picker.open()
            }
            else {
              picker.close()
            }
          }

          background: Item {
          }
        }

        UIKit.SvgImage {
          id: indicator
          Layout.preferredWidth: s(16)
          Layout.preferredHeight: s(16)
          Layout.alignment: Qt.AlignVCenter
          Layout.rightMargin: ThemeController.style.margin.m12
          source: "qrc:/chevron-down.svg"
          visible: showIndicator
        }
      }

      background: Rectangle {
        radius: s(4)
        width: combobox.width
        height: combobox.height
        color: "#FFFFFF"
        border.color: borderColor
        border.width: borderWidth
      }

      popup: UIKit.Picker {
        id: picker
        pickerModel: combobox.delegateModel
        title: pickerTitle
      }

      delegate: SortListDelegate {
        ButtonGroup.group: buttonGroup
        width: isMobile ? picker.width - s(32) : picker.width
        txt: modelData
        contentTxt: contentText
        separator: isMobile ? true : false
        checkable: true
        noVerticalMargins: isMobile
      }

      ButtonGroup {
        id: buttonGroup
        exclusive: true

        onClicked: {
          picker.close()
        }
      }
    }
  }

  states: [
    State {
      name: "default"
      when: !combobox.hovered && !(picker.visible || (!mainTextPickable && textItem.activeFocus))
      PropertyChanges {
        target: pickerDelegate
        borderColor: ThemeController.style.textInput.defaultBorderColor
        borderWidth: s(1)
      }
    },

    State {
      name: "hovered"
      when: combobox.hovered && !(picker.visible || (!mainTextPickable && textItem.activeFocus))
      PropertyChanges {
        target: pickerDelegate
        borderColor: ThemeController.style.textInput.hoveredBorderColor
        borderWidth: s(1)
      }
    },

    State {
      name: "active"
      when: picker.visible || (!mainTextPickable && textItem.activeFocus) || textItem.activeFocus
      PropertyChanges {
        target: pickerDelegate
        borderColor: ThemeController.style.textInput.activeBorderColor
        borderWidth: s(1.5)
      }
    }
  ]

  ListModel {
    id: objectsModel
  }

  Component.onCompleted: {
    objectsModel.append({ modelData: Strings.alphanumeric, contentText: Strings.standardTextFormat});
    objectsModel.append({ modelData: Strings.date, contentText: Strings.dateFormat});
  }
}
