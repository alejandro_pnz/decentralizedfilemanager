import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0

Item {
  property SwipeView swipeView
  property int modelSize: 0
  implicitHeight: indicator.height

  UIKit.LargeIconButton {
    anchors.verticalCenter: parent.verticalCenter
    anchors.left: parent.left
    icon.source: "qrc:/chevron-back.svg"
    onClicked: swipeView.decrementCurrentIndex()
    visible: swipeView.currentIndex !== 0
  }

  PageIndicator {
    id: indicator
    count: swipeView.count
    currentIndex: swipeView.currentIndex
    anchors.centerIn: parent
    spacing: ThemeController.style.margin.m16
    leftPadding: ThemeController.style.margin.m12
    rightPadding: ThemeController.style.margin.m12
    topPadding: ThemeController.style.margin.m8
    bottomPadding: ThemeController.style.margin.m8

    background: Rectangle {
      height: s(24)
      radius: s(100)
      color: ThemeController.style.whisperColor
    }

    delegate: Rectangle {
      width: s(8)
      height: s(8)
      radius: s(100)
      color: index === swipeView.currentIndex ?
               ThemeController.style.slatePurpleColor :
               ThemeController.style.ironColor
    }
  }

  UIKit.LargeIconButton {
    anchors.verticalCenter: parent.verticalCenter
    anchors.right: parent.right
    icon.source: "qrc:/chevron-forward.svg"
    onClicked: swipeView.incrementCurrentIndex()
    visible: swipeView.currentIndex !== modelSize - 1
  }
}
