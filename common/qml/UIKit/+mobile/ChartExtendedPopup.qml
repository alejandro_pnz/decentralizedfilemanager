import QtQuick 2.15
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

import "../UIKit/Charts/CustomComponents"

Popup {
  id: extendedDialogRect
  parent: Overlay.overlay
  width: parent.width
  height: parent.height
  anchors.centerIn: parent
  topPadding: 0
  bottomPadding: 0
  leftPadding: 0
  rightPadding: 0

  property alias extendedDialog: extendedLoader
  property int numberOfChartsInCurrentPage: 0
  property var pageChartsObjects
  property string title: ""

  signal closeOpenDialog()

  onVisibleChanged: {
    // Reset the indicator index when extended chart is closed
    if(!visible)
      customPageIndicator.currentIndex = 0
  }

  MainToolbar {
    id: header
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
    }
    height: s(68)
    leftButton.visible: true
    leftButton.iconButton.icon.source: "qrc:/close.svg"
    rightButton.visible: false
    leftButtonHandler: function() {
      close()
    }
    bottomBorderVisible: true

    UIKit.BaseText {
      text: title
      anchors.centerIn: parent
      font.weight: Font.Bold
      horizontalAlignment: Qt.AlignHCenter
    }
  }

  Item {
    anchors {
      top: header.bottom
      bottom: parent.bottom
      left: parent.left
      right: parent.right
    }

    anchors.margins: ThemeController.style.margin.m16

    Flickable {
      id: swipeArea
      anchors {
        fill: parent
      }
      flickableDirection: Flickable.HorizontalFlick
      onFlickStarted: {
        if (horizontalVelocity < 0) {
          customPageIndicator.swipeLeft()
        }
        if (horizontalVelocity > 0) {
          customPageIndicator.swipeRight()
        }
      }
      boundsMovement: Flickable.StopAtBounds
      pressDelay: 0

      Loader {
        id: extendedLoader

        anchors {
          fill: parent
          bottomMargin: ThemeController.style.margin.m40
        }

        MouseArea {
          anchors.fill: parent
        }
      }
    }

    CustomPageIndicator {
      id: customPageIndicator
      anchors {
        bottom: parent.bottom
        bottomMargin: ThemeController.style.margin.m16
        horizontalCenter: parent.horizontalCenter
      }
      width: parent.width
      pageIndicatorCount: extendedDialogRect.numberOfChartsInCurrentPage
      pageChartsObjects: extendedDialogRect.pageChartsObjects
    }
  }
}
