import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import UIFragments 1.0
import UIModel 1.0 as UIModel
import QtQml.Models 2.12

UIKit.StyledDrawer {
  id: popup

  property var model
  property string title: ""
  interactive: true
  signal itemClicked(int index)
  contentItemHeight: contentItem.implicitHeight + s(16)
  headerCloseButtonVisible: false
  closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
  content.anchors.topMargin: ThemeController.style.margin.m16

  headerVisible: true
  header: Item {
    Layout.fillWidth: true
    Layout.fillHeight: true
    UIKit.BaseText {
      width: parent.width
      anchors.verticalCenter: parent.verticalCenter
      text: title
      font.weight: Font.Bold
    }
  }

  CheckableTextLabelPanel {
    id: contentItem
    titleComponent.visible: false
    model: itemModel
    width: parent.width
    onDelegateClicked:{
      itemClicked(index)
    }
  }

  Item {
    id: itemModel
    property var model: popup.model
    property alias ism: ism

    function select(index) {
      ism.select(popup.model.index(index,0), ItemSelectionModel.Toggle)
    }
    ItemSelectionModel {
      id: ism
      model: popup.model
    }
  }
}
