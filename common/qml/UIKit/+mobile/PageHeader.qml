import QtQuick 2.15
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0
import AppStyles 1.0

//Screen Header with right button and content aligned to the left side
ToolBar {
  id: header
  height: ThemeController.style.pageHeaderHeight
  background: Item{}

  default property alias children: content.children
  property alias title: titleComponent.text
  property alias rightButton: rightButton
  property alias searchButton: searchButton
  property int marginLeft: 0
  property int horizontalMargins: ThemeController.style.pageHorizontalMargin
  property bool separatorVisible: false

  signal rightButtonClicked()
  signal searchButtonClicked()

  Rectangle {
    height: 1
    width: parent.width
    anchors.top: parent.bottom
    color: ThemeController.style.mercuryColor
    visible: separatorVisible
  }

  RowLayout {
    anchors {
      leftMargin: horizontalMargins
      rightMargin: horizontalMargins
      fill: parent
    }

    Item {
      id: content
      Layout.fillWidth: true
      Layout.fillHeight: true
      UIKit.BaseText {
        id: titleComponent
        anchors.left: parent.left
        font.weight: Font.Bold
        horizontalAlignment: Text.AlignHCenter
        anchors.verticalCenter: parent.verticalCenter
        size: BaseText.TextSize.H2
        visible: text.length > 0
      }
    }

    UIKit.LargeIconButton {
      id: searchButton
      visible: false
      icon.source: "qrc:/search.svg"
      height: s(32)
      width: s(32)
      onClicked: searchButtonClicked()
    }

    UIKit.LargeIconButton {
      id: rightButton
      height: s(32)
      width: s(32)
      onClicked: rightButtonClicked()
    }
  }
}
