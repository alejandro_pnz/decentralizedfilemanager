import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import QtQuick.Layouts 1.12

UIKit.StyledDrawer {
  id: popup
  contentItemHeight: box.childrenRect.height + headerContent.height
  headerVisible: true
  headerContent.height: 30
  headerSeparatorVisible: false
  interactive: true
  fullHeightSwitcher: false

  default property alias children: box.children

  Item {
    id: box
     width: parent.width
  }
}
