import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

UIKit.StyledDrawer {
  id: drawer
  drawerFullHeight: parent.height
  contentItemHeight: 0.88 * parent.height
  headerVisible: false
  closeButtonVisible: true

  EnterAuthorizationCode{
    id: enterAuthorization
    anchors {
      top: parent.top
      topMargin: ThemeController.style.margin.m24
      left: parent.left
      right: parent.right
    }
  }
}
