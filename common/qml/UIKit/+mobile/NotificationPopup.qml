import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects


UIKit.StyledDrawer {
  id: popup
  drawerFullHeight: parent.height
  fullHeightSwitcher: false
  fullHeight: true
  padding: ThemeController.style.margin.m16
  headerVisible: true
  header: Item {
    Layout.fillWidth: true
    Layout.fillHeight: true
    RowLayout {
      anchors.fill: parent
      UIKit.LargeIconButton {
        id: backButton
        icon.source: "qrc:/chevron-back.svg"
        onClicked: popup.close()
      }
      UIKit.BaseText {
        id: text
        Layout.alignment: Qt.AlignVCenter
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
        font.weight: Font.Bold
        elide: Text.ElideRight
        text: Strings.notifications
      }
      UIKit.StyledButton {
        type: UIKit.StyledButton.ButtonStyle.Text
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        text: Strings.markAsRead
        onClicked: {
          popup.close()
        }
      }
    }
  }

  UIKit.NotificationView {
    id: listView
    width: parent.width
    height: maximumContentHeight
  }
}
