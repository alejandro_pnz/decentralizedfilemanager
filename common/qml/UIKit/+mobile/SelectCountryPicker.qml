import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects


UIKit.StyledDrawer {
  id: picker
  drawerFullHeight: parent.height
  fullHeight: true
  property var pickerModel

  headerVisible: true
  header: Item {
    Layout.fillWidth: true
    Layout.fillHeight: true
    UIKit.BaseText {
      id: text
      horizontalAlignment: Text.AlignHCenter
      font.weight: Font.Bold
      elide: Text.ElideRight
      text: Strings.selectCountry
      anchors.centerIn: parent
    }

    UIKit.StyledButton {
      anchors.right: parent.right
      anchors.verticalCenter: parent.verticalCenter
      type: UIKit.StyledButton.ButtonStyle.Text
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      text: Strings.done
      onClicked: {
        picker.close()
      }
    }
  }

  ColumnLayout {
    width: parent.width
    height: parent.height
    spacing: 16
    UIKit.SearchInput {
      id: search
      Layout.fillWidth: true
      placeholderText: Strings.searchCountries
      Layout.preferredHeight: 48
      micIconVisible: false
    }

    ListView {
      id: listView
      Layout.fillHeight: true
      Layout.fillWidth: true
      model: pickerModel
      clip: true
    }
  }
}
