import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

UIKit.StyledDrawer {
  id: givePowerDrawer
  contentItemHeight: givePower.height
  fullHeightSwitcher: false
  headerVisible: true
  headerContent.height: s(30)
  headerSeparatorVisible: false
  interactive: true

  signal accepted()

  GivePower {
    id: givePower
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
    }
    onCloseClicked: givePowerDrawer.closeDrawer()
    onAccept: {
      givePowerDrawer.accepted()
      givePowerDrawer.closeDrawer()
    }
  }
}
