import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0

//Toolbar with right and left buttons(LargeIconButton or StyledButton) and
//any middle content

ToolBar {
  id: toolbar

  property alias leftButton: leftButton
  property alias rightButton: rightBtn
  property alias content: content
  height: ThemeController.style.toolbarHeight
  background: Item {}
  default property alias children: content.children
  property bool bottomBorderVisible: false

  property var leftButtonHandler: function() {
    if (pageManager.isPageOnStack(Pages.HomePage)) {
      homeManager.back()
    } else {
      pageManager.back()
    }
  }
  property var rightButtonHandler: function() {
    if (pageManager.isPageOnStack(Pages.HomePage)) {
      homeManager.back()
    } else {
      pageManager.back()
    }
  }

  Rectangle {
    height: 1
    width: parent.width
    color: ThemeController.style.mercuryColor
    anchors.top: parent.bottom
    visible: bottomBorderVisible
  }

  Item {
    anchors {
      leftMargin: ThemeController.style.pageHorizontalMargin
      rightMargin: ThemeController.style.pageHorizontalMargin
      fill: parent
    }

    UIKit.NavigationBarButton {
      id: leftButton
      anchors.left: parent.left
      anchors.verticalCenter: parent.verticalCenter
      iconButton.icon.source: "qrc:/chevron-back.svg"
      onClicked: leftButtonHandler()
    }

    Item {
      id: content
//      anchors.centerIn: parent
      anchors.left: leftButton.right
      anchors.right: rightBtn.left
      anchors.leftMargin: ThemeController.style.margin.m16
      anchors.rightMargin: ThemeController.style.margin.m16
      height: parent.height
    }

    UIKit.NavigationBarButton {
      id: rightBtn
      anchors.right: parent.right
      anchors.verticalCenter: parent.verticalCenter
      iconButton.icon.source: "qrc:/close-gray.svg"
      onClicked: rightButtonHandler()
    }
  }
}

