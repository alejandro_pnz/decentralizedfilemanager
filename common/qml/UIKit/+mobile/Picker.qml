import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

UIKit.StyledDrawer {
  id: popup
  property var pickerModel
  property string title: ""
  property int pickerHeight: parent.height * 0.33

  interactive: true
  contentItemHeight: pickerHeight
  drawerFullHeight: parent.height
  headerCloseButtonVisible: fullHeight
  closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

  headerVisible: true
  header: Item {
    Layout.fillWidth: true
    Layout.fillHeight: true
    UIKit.BaseText {
      width: parent.width
      anchors.verticalCenter: parent.verticalCenter
      text: title
      font.weight: Font.Bold
    }
  }

  ListView {
    id: listView
    height: parent.height
    clip: true
    width: parent.width
    model: pickerModel
  }
}
