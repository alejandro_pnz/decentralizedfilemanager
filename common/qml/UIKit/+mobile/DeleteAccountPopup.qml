import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

UIKit.StyledDrawer {
  id: deleteAccountDrawer
  contentItemHeight: s(276)
  fullHeightSwitcher: false
  headerVisible: true
  headerContent.height: s(30)
  headerSeparatorVisible: false
  interactive: true

  DeleteAccount {
    id: deleteAccount
    height: s(276)
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
    }
  }
}
