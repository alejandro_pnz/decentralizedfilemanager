import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0

UIKit.BaseFooter {
  height: s(60)

  property alias home: homeButton
  property alias library: libraryButton
  property alias add: addButton
  property alias boards: boardsButton
  property alias more: moreButton
  property alias group: buttonsGroup
  signal addButtonClicked()
  signal boardsButtonClicked()

  RowLayout {
    id: buttonsRow
    anchors.fill: parent
    anchors.topMargin: s(7)
    anchors.bottomMargin: s(7)

    UIKit.MenuItem {
      id: homeButton
      text: Strings.home
      iconDomain: "home"
      Layout.fillWidth: true
      onClicked: homeManager.openPage(Pages.HomePage)
    }

    UIKit.MenuItem {
      id: libraryButton
      text: Strings.library
      iconDomain: "library"
      Layout.fillWidth: true
      onClicked: homeManager.enterLibraryPage()
    }

    UIKit.StyledButton {
      id: addButton
      icon.source: "qrc:/plus-icon.svg"
      displayMode: UIKit.StyledButton.DisplayMode.IconOnly
      Layout.leftMargin: s(14)
      Layout.rightMargin: s(14)
      Layout.preferredWidth: s(48)
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
      onClicked: addButtonClicked()
    }

    UIKit.MenuItem {
      id: boardsButton
      text: Strings.boards
      iconDomain: "boards"
      Layout.fillWidth: true
      onClicked: boardsButtonClicked()
    }

    UIKit.MenuItem {
      id: moreButton
      text: Strings.more
      iconDomain: "more"
      Layout.fillWidth: true
      onClicked: homeManager.openPage(Pages.MorePage)
    }

    ButtonGroup {
      id: buttonsGroup
      buttons: buttonsRow.children
      exclusive: true
      checkedButton: homeButton
    }
  }
}

