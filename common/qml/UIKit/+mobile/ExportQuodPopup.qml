import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

UIKit.StyledDrawer {
  id: deleteAccountDrawer
  contentItemHeight: exportQuod.height + s(12)
  fullHeightSwitcher: false
  headerVisible: true
  interactive: true

  header: UIKit.BaseText {
    text: Strings.exportFormat
    font.weight: Font.Bold
    Layout.fillWidth: true
  }

  ExportQuod {
    id: exportQuod
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
    }
  }
}
