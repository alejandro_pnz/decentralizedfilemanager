import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects


UIKit.StyledDrawer {
  id: picker
  drawerFullHeight: parent.height
  fullHeight: true
  property var dateInput
  property alias calendar: calendar

  headerVisible: true
  header: Item {
    Layout.fillWidth: true
    Layout.fillHeight: true
    RowLayout {
      anchors.fill: parent
      UIKit.StyledButton {
        type: UIKit.StyledButton.ButtonStyle.Text
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        text: Strings.clear
        buttonStyle: ThemeController.style.clearDateInputButtonStyle
        onClicked: {
          calendar.selectedDate = null
        }
      }
      UIKit.BaseText {
        id: text
        Layout.alignment: Qt.AlignVCenter
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
        font.weight: Font.Bold
        elide: Text.ElideRight
        text: dateInput.labelText
      }
      UIKit.StyledButton {
        type: UIKit.StyledButton.ButtonStyle.Text
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        text: Strings.done
        onClicked: {
          if (calendar.selectedDate === null)
            dateInput.comboBox.displayText = ""
          else
            dateInput.comboBox.displayText = calendar.selectedDate.toLocaleString(calendar.locale, "dd/MM/yyyy")
          picker.close()
        }
      }
    }
  }

  Column {
    width: parent.width
    spacing: ThemeController.style.margin.m16
    TextField {
      id: textItem
      text: calendar.selectedDate === null ? "" : calendar.selectedDate.toLocaleString(calendar.locale, "dd/MM/yyyy")
      color: ThemeController.style.textInput.textColor
      verticalAlignment: Text.AlignVCenter
      placeholderText: placeholderTxt
      placeholderTextColor: ThemeController.style.textInput.placeholderTextColor
      font.pixelSize: ThemeController.style.textInput.fontSize
      readOnly: true
      echoMode: TextInput.Normal
      width: parent.width
      padding: ThemeController.style.margin.m12
      background: Rectangle {
        border.color: "#D6D9E1"
        border.width: s(1)
        radius: s(4)
      }
    }
    UIKit.Calendar {
      id: calendar
      anchors.horizontalCenter: parent.horizontalCenter
    }
  }
}
