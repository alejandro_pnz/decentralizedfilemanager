import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

// TODO: move to PopupPage implementation

UIKit.StyledDrawer {
  id: accountsDrawer
  drawerFullHeight: parent.height
  contentItemHeight: s(281)
  fullHeightSwitcher: false
  headerVisible: true

  header: RowLayout {
    Layout.fillWidth: true
    UIKit.BaseText {
    text: Strings.switchAccount
    font.weight: Font.Bold
    Layout.fillWidth: true
    }

    UIKit.LargeIconButton {
      icon.source: "qrc:/add-circle.svg"
      overlay.visible: false
      onClicked: {
      }
    }
  }

  SwitchAccount {
    popup: accountsDrawer
    id: switchAccount
    height: s(281)
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
    }
  }
}
