import QtQuick 2.15
import AppStyles 1.0

Text {
  id: root

  enum TextSize { H1, H2, H3, Body, XBody, Small, XSmall }

  property bool alternativeFont: false
  property int size: BaseText.TextSize.Body
  readonly property int pixelSize: {
    switch (root.size) {
    case BaseText.TextSize.H1: return ThemeController.style.font.h1Size
    case BaseText.TextSize.H2: return ThemeController.style.font.h2Size
    case BaseText.TextSize.H3: return ThemeController.style.font.h3Size
    case BaseText.TextSize.Body: return ThemeController.style.font.bodySize
    case BaseText.TextSize.XBody: return ThemeController.style.font.xbodySize
    case BaseText.TextSize.Small: return ThemeController.style.font.smallSize
    case BaseText.TextSize.XSmall: return ThemeController.style.font.xsmallSize
    }
  }

  readonly property int fontLineHeight: {
    switch (root.size) {
    case BaseText.TextSize.H1: return ThemeController.style.font.h1LineHeight
    case BaseText.TextSize.H2: return ThemeController.style.font.h2LineHeight
    case BaseText.TextSize.H3: return ThemeController.style.font.h3LineHeight
    case BaseText.TextSize.Body: return ThemeController.style.font.bodyLineHeight
    case BaseText.TextSize.XBody: return ThemeController.style.font.xbodyLineHeight
    case BaseText.TextSize.Small: return ThemeController.style.font.smallLineHeight
    case BaseText.TextSize.XSmall: return ThemeController.style.font.xsmallLineHeight
    }
  }


  font {
    pixelSize: root.pixelSize
    family: alternativeFont ? ThemeController.style.font.secondaryFontFamily
                            : ThemeController.style.font.primaryFontFamily
  }
  color: ThemeController.style.font.defaultColor
  horizontalAlignment: Qt.AlignLeft
  verticalAlignment: Qt.AlignVCenter
  lineHeightMode: Text.FixedHeight
  lineHeight: fontLineHeight

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton // Do not steal clicks
    cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
  }

  // Accessibility
  Accessible.role: Accessible.StaticText
  Accessible.name: root.text
}
