import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import com.test 1.0
import UIFragments 1.0

UIKit.GenericPopup {
  id: popup
  implicitHeight: s(625)
  implicitWidth: s(394)

  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
  topPadding: ThemeController.style.margin.m16
  bottomPadding: 0

  property alias sModel: fragment.searchModel
  property alias filterType: fragment.dateFilterType

  onClosed: {
    fragment.cleanup()
  }

  onOpened: {
    fragment.init()
  }

  DateFilter {
    id: fragment
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
      bottom: footer.top
    }
  }

  Rectangle {
    id: footer
    anchors {
      left: parent.left
      right: parent.right
      bottom: parent.bottom
    }
    height: s(80)
    radius: s(8)

    Rectangle {
      id: separator
      height: 1
      width: parent.width
      color: ThemeController.style.seashellColor
      anchors.top: footer.top
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.leftMargin: -leftPadding
      anchors.rightMargin: -rightPadding
    }

    UIKit.StyledButton {
      text: Strings.clearAll
      type: UIKit.StyledButton.ButtonStyle.Text
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors.left: parent.left
      anchors.verticalCenter: parent.verticalCenter
      buttonStyle: ThemeController.style.clearTextButtonStyle
      onClicked: {
        fragment.clearAll()
      }
    }

    UIKit.StyledButton {
      text: Strings.apply
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors.right: parent.right
      anchors.verticalCenter: parent.verticalCenter
      onClicked: {
        fragment.applyFilters()
        popup.close()
      }
    }
  }
}
