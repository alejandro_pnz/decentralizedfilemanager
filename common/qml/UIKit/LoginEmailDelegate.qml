import QtQuick 2.15
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects
import com.test 1.0

Item {
  implicitWidth: parent.width
  implicitHeight: s(84)

  property string email

  Rectangle {
    id: delegate
    width: parent.width
    height: parent.height
    border.width: s(1)
    border.color: ThemeController.style.whisperColor
    radius: s(8)
    color: ThemeController.style.whiteColor

    Row {
      spacing: ThemeController.style.margin.m12
      height: s(60)
      anchors {
        left: parent.left
        right: parent.right
        leftMargin: ThemeController.style.margin.m16
        rightMargin: ThemeController.style.margin.m16
        verticalCenter: parent.verticalCenter
      }

      Avatar {
        id: avatar
        text: email
        topIcon: false
        color: ThemeController.style.charadeColor
      }

      Column {
        anchors.verticalCenter: parent.verticalCenter

        RowLayout {
          spacing: ThemeController.style.margin.m8
          BaseText {
            id: loginEmail
            text: Strings.loginEmail
            horizontalAlignment: Qt.AlignLeft
            font.weight: Font.Bold
          }

          SvgImage {
            id: tipIcon
            property bool closedMyself: false
            source: "qrc:/info-default.svg"
            width: s(24)
            height: s(24)
            MouseArea {
              anchors.fill: parent
              cursorShape: Qt.PointingHandCursor
              hoverEnabled: true
              onPressed: {
                if(tooltip.opened ) {
                  tooltip.close()
                  tipIcon.closedMyself = true
                }
                else
                  tipIcon.closedMyself = false

              }
              onReleased: {
                if(!tipIcon.closedMyself)
                  tooltip.open()
              }
            }
          }

          UIKit.ToolTip {
            id: tooltip
            modal: false
            tipContent: Strings.loginEmailToolTip
            triangle.anchors.verticalCenter: triangle.parent.verticalCenter
            triangle.anchors.top: undefined
            triangle.anchors.bottom: undefined
            triangle.width: s(6)
            triangle.height: s(12)
            closePolicy: Popup.CloseOnReleaseOutside
            x: tipIcon.x + tipIcon.width + ThemeController.style.margin.m8
            y: tipIcon.y  + tipIcon.height * 0.5 - tooltip.height * 0.5
          }
        }


        BaseText {
          id: emailText
          width: delegate.width - avatar.width - (ThemeController.style.margin.m16  * 2 + ThemeController.style.margin.m12)
          text: email
          elide: Text.ElideRight
          horizontalAlignment: Qt.AlignLeft
        }
      }
    }

  }

  DropShadow {
    id: dropShadow
    anchors.fill: delegate
    horizontalOffset: 0
    verticalOffset: 0
    radius: s(15)
    color: Qt.rgba(0, 0, 0, 0.05)
    source: delegate
  }
}
