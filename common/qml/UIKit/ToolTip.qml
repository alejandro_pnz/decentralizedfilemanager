import QtQuick 2.15
import QtQuick.Controls 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

Popup {
  id: tip

  property string tipContent: ""
  property string confirmTxt: ""

  property alias triangle: triangle

  property var config: ThemeController.style.darkTooltip

  implicitWidth: s(288)
  implicitHeight: content.height + 2 * verticalPadding
  verticalPadding: ThemeController.style.margin.m12
  horizontalPadding: ThemeController.style.margin.m16
  closePolicy: Popup.NoAutoClose
  modal: true
  Overlay.modal: Item{}
  background: Item {
    Rectangle {
      id: bg
      anchors.fill: parent
      color: config.bgColor
      radius: s(2)
    }
    UIKit.CanvasTriangle {
      id: triangle
      color: bg.color
      anchors.right: parent.left
      anchors.top: parent.top
      anchors.topMargin: s(18)
    }
  }

  ColumnLayout {
    id: content
    anchors {
      left: parent.left
      right: parent.right
    }
    spacing: ThemeController.style.margin.m8

    UIKit.BaseText {
      Layout.fillWidth: true
      text: tipContent
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      color: config.textColor
    }

    UIKit.BaseText {
      id: textButton
      Layout.fillWidth: true
      text: confirmTxt
      font.weight: Font.Bold
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      color: config.confirmButtonColor
      visible: text !== ""

      MouseArea {
        id: clickArea
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        onClicked: {
          tip.close()
        }
      }
    }
  }
}

