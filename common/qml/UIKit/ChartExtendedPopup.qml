import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

import "../UIKit/Charts/CustomComponents"

UIKit.PopupScreen {
  id: extendedDialogRect
  parent: Overlay.overlay
  width: parent.width - s(80)
  height: parent.height - s(80)
  anchors.centerIn: parent
  showBackButton: false
  showCloseButton: false
  topPadding: 0
  leftPadding: 0
  rightPadding: 0
  bottomPadding: 0

  property alias extendedDialog: extendedLoader
  property int numberOfChartsInCurrentPage: 0
  property var pageChartsObjects
  property string title: ""

  signal closeOpenDialog()

  onVisibleChanged: {
    // Reset the indicator index when extended chart is closed
    if(!visible)
      customPageIndicator.currentIndex = 0
  }

  Item {
    anchors.fill: parent

    Rectangle {
      id: header
      width: parent.width
      height: s(64)
      radius: s(8)

      UIKit.BaseText {
        text: title
        anchors.centerIn: parent
        size: UIKit.BaseText.TextSize.H3
        font.weight: Font.DemiBold
      }

      RowLayout {
        anchors.fill: parent

        RowLayout {
          Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
          Layout.rightMargin: ThemeController.style.margin.m24
          Layout.preferredHeight: s(32)
          spacing: ThemeController.style.margin.m8

          UIKit.LargeIconButton {
            id: actionButtonComponent
            icon.source: "qrc:/download.svg"
            onClicked: downloadPopup.open()
            Layout.alignment: Qt.AlignVCenter
          }

          Rectangle {
            Layout.alignment: Qt.AlignVCenter
            Layout.preferredWidth: s(1)
            Layout.preferredHeight: s(24)
            color: ThemeController.style.seashellColor
          }

          UIKit.LargeIconButton {
            icon.source: "qrc:/close.svg"
            onClicked: {
              close()
            }
            Layout.alignment: Qt.AlignVCenter
          }
        }
      }

      UIKit.DownloadPopup {
        id: downloadPopup
        y: parent.y + parent.height + s(8)
        x: parent.width - downloadPopup.width - s(6)
      }

      Rectangle {
        width: parent.width
        height: s(1)
        color: ThemeController.style.seashellColor
        anchors.top: parent.bottom
      }
    }

    Flickable {
      id: swipeArea
      anchors {
        top: header.bottom
        topMargin: s(1)
        bottom: parent.bottom
        left: parent.left
        right: parent.right
        bottomMargin: s(106)
      }

      flickableDirection: Flickable.HorizontalFlick
      onFlickStarted: {
        if (horizontalVelocity < 0) {
          customPageIndicator.swipeLeft()
        }
        if (horizontalVelocity > 0) {
          customPageIndicator.swipeRight()
        }
      }
      boundsMovement: Flickable.StopAtBounds
      pressDelay: 0

      Loader {
        id: extendedLoader

        anchors.fill: parent

        MouseArea {
          anchors.fill: parent
        }
      }
    }
    Rectangle {
      color: "white"
      width: extendedLoader.width
      height: s(80)
      radius: s(8)
      anchors {
        bottom: parent.bottom
        left: parent.left
        right: parent.right
      }

      CustomPageIndicator {
        id: customPageIndicator
        anchors.centerIn: parent
        pageIndicatorCount: extendedDialogRect.numberOfChartsInCurrentPage
        pageChartsObjects: extendedDialogRect.pageChartsObjects
      }

      Rectangle {
        anchors.top: parent.top
        width: parent.width
        height: s(1)
        color: ThemeController.style.seashellColor
      }
    }
  }
}
