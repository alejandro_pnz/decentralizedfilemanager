import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12

Button {
  id: button

  implicitHeight: s(40)
  implicitWidth: s(192)
  state: 'default'
  checkable: true

  property alias bg: bgRect
  property alias textObject: displayedText
  property int idx: 0

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      radius: s(4)
    }
  }

  contentItem: UIKit.BaseText {
    id: displayedText
    anchors {
      verticalCenter: parent.verticalCenter
      left: parent.left
      right: parent.right
      leftMargin: ThemeController.style.margin.m16
      rightMargin: ThemeController.style.margin.m16
    }
    text: button.text
  }

  states: [
    State {
      name: "default"
      when: !button.hovered && !button.checked
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.whiteColor
      }
      PropertyChanges {
        target: displayedText
        color: ThemeController.style.charadeColor
      }
    },

    State {
      name: "selected"
      when: button.hovered || button.checked
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.whiteSmokeColor
      }
      PropertyChanges {
        target: displayedText
        color: ThemeController.style.blackColor
      }
    }
  ]
}
