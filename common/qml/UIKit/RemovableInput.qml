import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0

RowLayout {
  id: root
  spacing: ThemeController.style.margin.m16
  layoutDirection: Qt.RightToLeft
  property string labelText
  property string placeholderText
  property string value
  property int maxLength
  property bool removable: true
  property bool readonly: false
  property bool sensitive: false
  property var sectionId
  property var attributeId
  property int attributeType
  property int attributeIndex
  property var attributeValue

  property int removeButtonWidth: iconButton.width + spacing

  signal deleteButtonClicked()
  signal textChanged(string text)
  signal generatePassword()
  signal enterPasswordHistory()

  Item {
    Layout.fillHeight: true
    Layout.minimumWidth: iconButton.width
    implicitWidth: iconButton.width
    visible: removable
    LargeIconButton {
      id: iconButton
      iconHeight: s(32)
      iconWidth: s(32)
      anchors.bottom: parent.bottom
      anchors.bottomMargin: (ThemeController.style.defaultInputHeight - height) * 0.5
      overlay.color: ThemeController.style.transparent
      icon.source: "qrc:/delete-circle.svg"
      onClicked: deleteButtonClicked()
    }
  }
}
