import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0

TextField {
  id: inputField
  property color bgColor: ThemeController.style.searchInput.bgColor
  placeholderTextColor: ThemeController.style.searchInput.placeholderTextColor
  font.pixelSize: ThemeController.style.searchInput.fontSize
  color: ThemeController.style.searchInput.textColor
  height: ThemeController.style.defaultInputHeight
  implicitWidth: s(200)

  leftPadding: searchIconVisible ? s(12 + searchIcon.width + 8) : s(12)
  rightPadding: iconButtonVisible ? s(12 + iconButton.width + 8) : s(12)

  // Signal sent when microphone button has been clicked
  signal microphoneClicked()

  property string searchText: app.isMobile ? (preeditText.trim().length > 0 ? preeditText.trim() : text.trim()): text.trim()
  property bool searchIconVisible: true
  property color searchIconColor: ThemeController.style.searchInput.searchIconColor
  property string searchIconSource: "qrc:/search.svg"
  property color cursorColor: ThemeController.style.searchInput.cursorColor

  property bool iconButtonVisible: clearIconVisible || micIconVisible
  property bool clearIconVisible: inputField.text.length > 0
  property bool micIconVisible: inputField.text.length == 0
  property string inputIconSource: micIconVisible ? "qrc:/microphone.svg"
                                                  : clearIconVisible
                                                  ? "qrc:/close-icon.svg" : ""

  property alias searchIcon: searchIcon
  property alias fieldButton: iconButton

  selectByMouse: true
  selectedTextColor: ThemeController.style.whiteColor

  inputMethodHints: Qt.ImhNoPredictiveText

  cursorDelegate: Component {
    Rectangle {
      id: cursor
      color: inputField.cursorColor
      height: inputField.font.pixelSize
      implicitWidth: s(1)

      Timer {
          interval: 500
          repeat: true
          running: inputField.cursorVisible
          onRunningChanged: {
              if (!running) {
                  cursor.visible = false
              }
          }
          onTriggered: cursor.visible = !cursor.visible
      }
    }
  }

  background: Rectangle {
    radius: s(4)
    width: parent.width
    height: parent.height
    color: bgColor

    ColoredImage {
      id: searchIcon
      anchors.left: parent.left
      anchors.leftMargin: ThemeController.style.margin.m12
      anchors.verticalCenter: parent.verticalCenter
      color: inputField.searchIconColor
      icon.source: searchIconSource
    }

    FieldIconButton {
      id: iconButton
      width: s(24)
      height: s(24)
      iconWidth: s(micIconVisible ? 24 : 16)
      iconHeight: s(micIconVisible ? 24 : 16)
      anchors.verticalCenter: parent.verticalCenter
      anchors.right: parent.right
      anchors.rightMargin: ThemeController.style.margin.m12
      iconSrc: inputIconSource
      onClicked: micIconVisible ? microphoneClicked() : inputField.clear()
    }
  }

  // Accessibility
  Accessible.role: Accessible.EditableText
  Accessible.name: inputField.placeholderText
  Accessible.searchEdit: true
}
