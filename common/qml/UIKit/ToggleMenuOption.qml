import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12

AbstractButton {
  id: control

  implicitHeight: s(56)
  implicitWidth: parent.width

  property bool showSeparator: false
  property var config: ThemeController.style.toggleMenuOption

  readonly property alias bgRect: bgRect
  readonly property alias textLabel: textLabel
  property alias descriptionText: descriptionLabel.text
  property alias switchComponent: switchComponent
  checkable: true

  background: Rectangle {
    id : bgRect
    color: control.pressed && app.isPhone ? config.bgColorPressed : config.bgColor

    Rectangle {
      id: separatorComponent
      anchors {
        bottom: parent.bottom
        right: parent.right
        left: parent.left
      }
      height: s(1)
      color: config.separatorColor
      visible: control.showSeparator
    }
  }

  contentItem: RowLayout {
    id: mainLayout
    anchors.verticalCenter: parent.verticalCenter
    height: control.height

    ColumnLayout {
      spacing: 0
      Layout.alignment: Qt.AlignVCenter

      BaseText {
        id: textLabel
        text: control.text
      }

      BaseText {
        id: descriptionLabel
        size: BaseText.TextSize.Small
        color: ThemeController.style.shuttleColor
      }
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }

    SwitchButton {
      id: switchComponent
      checked: control.checked
      checkable: false
      onClicked:{ control.toggle() }
    }
  }
}
