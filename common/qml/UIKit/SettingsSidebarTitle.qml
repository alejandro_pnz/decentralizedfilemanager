import QtQuick 2.15
import UIKit 1.0 as UIKit
import com.test 1.0
import QtQuick.Layouts 1.12
import AppStyles 1.0

RowLayout {
  id: contentRow
  spacing: ThemeController.style.margin.m8
  height: s(24)
  width: parent.width
  property string title: ""

  property alias titleLabel: titleLabel
  property alias tipIcon: tipIcon
  property alias tooltip: tooltip

  enum TooltipDirection { Up, Down }
  property int tooltipDirection: SettingsSidebarTitle.TooltipDirection.Down

  UIKit.BaseText {
    id: titleLabel
    text: title
    size: UIKit.BaseText.TextSize.Small
    font.weight: Font.DemiBold
    font.capitalization: Font.AllUppercase
    Layout.alignment: Qt.AlignVCenter
  }

  UIKit.SvgImage {
    id: tipIcon
    source: "qrc:/tips-icon.svg"
    width: s(24)
    height: s(24)

    MouseArea {
      anchors.fill: parent
      cursorShape: Qt.PointingHandCursor
      acceptedButtons: Qt.NoButton
      hoverEnabled: true
      onEntered: tooltip.open()
      onExited: tooltip.close()
    }
  }

  UIKit.ToolTip {
    id: tooltip
    modal: false
    tipContent: Strings.onlyDeviceSettings
    triangle.pointing: tooltipDirection === SettingsSidebarTitle.TooltipDirection.Down ?
                         UIKit.CanvasTriangle.Pointing.Down : UIKit.CanvasTriangle.Pointing.Up
    triangle.anchors.top: tooltipDirection === SettingsSidebarTitle.TooltipDirection.Down ?
                            triangle.parent.bottom : undefined
    triangle.anchors.bottom: tooltipDirection === SettingsSidebarTitle.TooltipDirection.Down ?
                               undefined : triangle.parent.top
    triangle.anchors.horizontalCenter: triangle.parent.horizontalCenter
    triangle.anchors.right: undefined
    triangle.anchors.topMargin: 0
    triangle.width: s(12)
    triangle.height: s(9)
    x: tipIcon.x + tipIcon.width * 0.5 - tooltip.width * 0.5
    y: tooltipDirection === SettingsSidebarTitle.TooltipDirection.Down
       ? tipIcon.y - tooltip.height - ThemeController.style.margin.m16
       : tipIcon.y + tipIcon.height + ThemeController.style.margin.m16
  }
}
