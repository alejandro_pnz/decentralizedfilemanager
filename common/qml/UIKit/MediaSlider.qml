import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects


Slider {
  id: control
  from: 0.0
  to: 1.0
  value: 0
  stepSize: 0.01
  implicitWidth: parent.width
  property bool playing: false
  Behavior on value {
    enabled: !control.pressed && playing
    NumberAnimation { duration: 1000 }
  }

  property alias bgRect: bgRect
  property alias selectedRect: selectedRect
  property alias handleRect: handleRect
  property var componentStyle: ThemeController.style.mediaSlider

  background: Rectangle {
    id: bgRect
    y: control.topPadding + control.availableHeight / 2 - height / 2
    implicitWidth: parent.width
    implicitHeight: ThemeController.style.margin.m4
    height: implicitHeight
    radius: s(100)
    color: componentStyle.bgColor

    Rectangle {
      id: selectedRect
      width: control.visualPosition * parent.width
      height: parent.height
      color: componentStyle.bgSelectedColor
      radius: s(100)      
    }
  }

  handle: Item {
    visible: control.hovered

    Rectangle {
      id: handleRect
      x: control.visualPosition * control.availableWidth
      y: control.topPadding + control.availableHeight / 2 - height / 2
      implicitWidth: ThemeController.style.margin.m16
      implicitHeight: ThemeController.style.margin.m16
      radius: s(100)
      color: componentStyle.handleBgColor
      border.color: componentStyle.handleBdColor
    }

    DropShadow {
      id: dropShadow
      anchors.fill: handleRect
      horizontalOffset: ThemeController.style.margin.m4
      verticalOffset: ThemeController.style.margin.m4
      radius: s(20)
      //samples: 2 * radius + 1
      color: componentStyle.dropShadowColor
      source: handleRect
    }
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: Qt.PointingHandCursor
  }
}
