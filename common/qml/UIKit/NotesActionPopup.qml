import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import com.test 1.0

UIKit.GenericPopup {
  id: popup
  implicitWidth: listView.contentWidth + leftPadding + rightPadding
  implicitHeight: s(64)

  modal: false
  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
  topPadding: 0
  bottomPadding: 0

  property alias model: listView.model
  // Object item
  property var currentItem


  ListView {
    id: listView
    height: 32
    width: contentWidth
    orientation: ListView.Horizontal
    spacing: ThemeController.style.margin.m12
    anchors.verticalCenter: parent.verticalCenter

    delegate: UIKit.LargeIconButton {
      icon.source: iconSource
      iconWidth: s(32)
      iconHeight: s(32)
      checkable: true
      overlay.color: checked ? ThemeController.style.slatePurpleColor :
                               ThemeController.style.transparent
    }

    section.property: "sectionId"
    section.criteria: ViewSection.FirstCharacter
    section.delegate: Item {
      width: section != 2 ? s(12) : 0
      height: s(32)
      Rectangle {
        width: s(1)
        height: parent.height
        color: ThemeController.style.seashellColor
        visible: section != 2
        anchors.left: parent.left
      }
    }
  }
}
