import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit

Item {
  property string email: ""
  implicitHeight: s(88)
  implicitWidth: s(64)

  property alias avatarObject: avatar
  property alias textObject: userEmail

  Avatar {
    id: avatar
    text: email
    anchors.top: parent.top
    anchors.topMargin: ThemeController.style.margin.m4
    topIcon: true
  }

  UIKit.BaseText {
    id: userEmail
    text: email
    anchors.bottom: parent.bottom
    width: parent.width
    color: ThemeController.style.avatarDelegate.textColor
    elide: Text.ElideRight
    size: BaseText.TextSize.Small
  }
}
