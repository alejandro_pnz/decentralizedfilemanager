import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12

AbstractButton {
  id: control

  topPadding: ThemeController.style.margin.m8
  bottomPadding: ThemeController.style.margin.m8
  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16

  signal closeButtonClicked()

  readonly property alias titleComponent: text
  readonly property alias bgRect: bg
  readonly property alias closeBtn: closeBtn

  property var style: closeBtn.visible ? ThemeController.style.closeableTextLabelDelegate : ThemeController.style.textLabelDelegate
  background: Item {
    Rectangle {
      id: bg
      anchors.fill: parent
      color: control.checked ? control.style.bgColorPressed : control.style.bgColor
      border.width: s(1)
      border.color: control.checked ? control.style.bdColorPressed : control.hovered ?
                    control.style.bdColorHover : control.style.bdColor
      radius: ThemeController.style.margin.m4
    }
  }

  contentItem: Row {
    spacing: ThemeController.style.margin.m8
    BaseText {
      id: text
      color: control.checked ? control.style.textColorPressed : control.style.textColor
      text: control.text
      font.weight: control.checked ? Font.DemiBold : Font.Normal
      anchors.verticalCenter: parent.verticalCenter
    }

    SvgImage {
      id: closeBtn
      width: s(16)
      height: s(16)
      source: "qrc:/close-icon.svg"
      anchors.verticalCenter: parent.verticalCenter
      visible: false
      MouseArea {
        anchors.fill: parent
        onClicked: closeButtonClicked()
      }
    }
  }
}
