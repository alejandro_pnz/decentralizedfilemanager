import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import AppStyles 1.0

import Qt5Compat.GraphicalEffects

TabButton {
  id: control
  enum TabStyle {
    Simple,
    Closable
  }

  signal closeClicked()

  property int type: StyledTabButton.TabStyle.Simple

  readonly property color bgColor: bgRect.color

  readonly property color textColor: txt.color

  readonly property color iconColor: imgOverlay.color

  readonly property color bottomLineColor: line.color

  hoverEnabled: isMobile ? false : true
  property bool overlayVisible: control.type === StyledTabButton.TabStyle.Simple

  // Get proper style for button depending on type and display mode
  property var tabButtonStyle: {
    switch(control.type) {
    case StyledTabButton.TabStyle.Simple: return ThemeController.style.tabButton.simple
    case StyledTabButton.TabStyle.Closable: return ThemeController.style.tabButton.closable
    }
  }

  background: Item {
    Rectangle {
      id: bgRect
      height: parent.height
      width: contentLayout.width
      radius: s(4)
      color: {
        if (control.enabled) {
          if (control.checked) {
            return control.tabButtonStyle.bgColorSelected
          }
          else if (control.hovered) {
            return control.tabButtonStyle.bgColorHover
          }
          else if (control.activeFocus) {
            return button.tabButtonStyle.bgColorFocus
          }
          else {
            return control.tabButtonStyle.bgColor
          }
        }
        else {
          return control.tabButtonStyle.bgColorDisabled
        }
      }

      Rectangle {
        id: line
        anchors.horizontalCenter: bgRect.horizontalCenter
        anchors.bottom: parent.bottom

        width: contentLayout.width

        height: s(3)
        visible: control.type === StyledTabButton.TabStyle.Simple
        color: {
          if (control.enabled) {
            if (control.checked) {
              return control.tabButtonStyle.lineColorSelected
            }
            else if (control.hovered) {
              return control.tabButtonStyle.lineColorHover
            }
            else if (control.activeFocus) {
              return button.tabButtonStyle.lineColorFocus
            }
            else {
              return control.tabButtonStyle.lineColor
            }
          }
          else {
            return control.tabButtonStyle.lineColorDisabled
          }
        }
      }
    }
  }

  contentItem: Item {
    height: control.height
    width: contentLayout.width
    anchors.top: control.top
    anchors.left: control.left
    anchors.bottom: control.bottom

    RowLayout {
      id: contentLayout
      height: parent.height
      width: iconLayout.width + txt.width + styledTabCloseButton.width + s(8)

      spacing: s(8)

      Item {
        id: iconLayout
        height: img.height
        width: img.width
        Layout.alignment: Qt.AlignVCenter
        SvgImage {
          id: img
          anchors.centerIn: parent
          source: control.icon.source
          width: control.type === StyledTabButton.TabStyle.Closable ? s(20) : s(16)
          height: control.type === StyledTabButton.TabStyle.Closable ? s(20) : s(16)
          visible: !imgOverlay.visible
        }
        ColorOverlay {
          id: imgOverlay
          anchors.fill: img
          source: img
          visible: control.overlayVisible
          color: {
            if (control.enabled) {
              if (control.checked) {
                return control.tabButtonStyle.iconColorSelected
              }
              else if (control.hovered) {
                return control.tabButtonStyle.iconColorHover
              }
              else if (control.activeFocus) {
                return control.tabButtonStyle.iconColorFocus
              }
              else {
                return control.tabButtonStyle.iconColor
              }
            }
            else {
              return control.tabButtonStyle.iconColorDisabled
            }
          }
        }
      }

      BaseText {
        id: txt
        text: control.text
        Layout.alignment: Qt.AlignVCenter
        font.weight: Font.DemiBold

        color: {
          if (control.enabled) {
            if (control.checked) {
              return control.tabButtonStyle.textColorSelected
            }
            else if (control.hovered) {
              return control.tabButtonStyle.textColorHover
            }
            else if (control.activeFocus) {
              return button.tabButtonStyle.textColorFocus
            }
            else {
              return control.tabButtonStyle.textColor
            }
          }
          else {
            return control.tabButtonStyle.textColorDisabled
          }
        }

        Accessible.ignored: true
      }

      LargeIconButton {
        id: styledTabCloseButton
        property bool isClosable: (control.type === StyledTabButton.TabStyle.Closable)
        implicitWidth: isClosable ? s(32) : 0
        implicitHeight: isClosable ? s(32) : 0
        icon.source: "qrc:/close.svg"
        iconWidth: isClosable ? s(16) : 0
        iconHeight: isClosable ? s(16) : 0
        onClicked: control.closeClicked()
        visible: isClosable
      }
    }
  }
}
