import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0

Item {
  implicitWidth: input.width + s(48)
  implicitHeight: childrenRect.height
  signal deleteButtonClicked()

  property string labelTxt: ""
  property string textFieldText: ""
  property alias input: input
  property alias iconButton: iconButton
  property bool isMobile: Qt.platform.os === "android" || Qt.platform.os === "ios"
  property bool showBottomSeparator: false

  InputField {
    id: input
    anchors.left: parent.left
    implicitWidth: s(isMobile ? 295 : 346)
    labelText: labelTxt
    textField.text: textFieldText
    placeholderTxt: textFieldText
    isPassPhrase: true
    clearIconVisible: false
    rightIconSource: "qrc:/duplicate-icon.svg"
    rightIconBtn.style: ThemeController.style.fieldButtonAlt
    leftEyeIcon: true
    textField.readOnly: true
  }

  Rectangle {
    anchors.top: input.bottom
    anchors.topMargin: ThemeController.style.margin.m16
    height: 1
    width: input.width
    color: ThemeController.style.seashellColor
    visible: showBottomSeparator
  }

  LargeIconButton {
    id: iconButton
    anchors.right: parent.right
    y: input.textField.y + input.textField.height * 0.5 - iconButton.height * 0.5
    overlay.color: ThemeController.style.transparent
    icon.source: "qrc:/delete-circle.svg"
    onClicked: {
      deleteButtonClicked()
    }
  }
}
