import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit

Button {
  id: button

  implicitHeight: ThemeController.style.defaultInputHeight
  width: parent.width
  checkable: true
  hoverEnabled: true

  property alias iconObject: icon
  property string iconDomain
  property alias nameText: name
  property bool shrinked: false

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
    }
  }

  contentItem:  RowLayout {
    anchors.fill: parent
    spacing: ThemeController.style.margin.m24

    ColoredImage{
      id: icon
      Layout.leftMargin: ThemeController.style.margin.m24
      Layout.preferredHeight: ThemeController.style.margin.m24
      Layout.preferredWidth: ThemeController.style.margin.m24
      Layout.alignment: Qt.AlignVCenter
      icon.fillMode: Image.PreserveAspectFit
      source: button.icon.source
    }

    UIKit.BaseText {
      id: name
      text: button.text
      Layout.alignment: Qt.AlignVCenter
      color: ThemeController.style.charadeColor
      visible: !shrinked
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }
  }

  states: [
    State {
      name: "default"
      when: !hovered && !checked
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.whiteSmokeColor
      }
      PropertyChanges {
        target: icon
        color: ThemeController.style.shuttleColor
        source: "qrc:/" + button.iconDomain + "-default.svg"
      }
      PropertyChanges {
        target: name
        color: ThemeController.style.charadeColor
      }
    },
    State {
      name: "hover"
      when: hovered || checked
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.seashellColor
      }
      PropertyChanges {
        target: icon
        source: "qrc:/" + button.iconDomain + "-selected.svg"
      }
      PropertyChanges {
        target: name
        color: ThemeController.style.blackColor
      }
    }
  ]
}
