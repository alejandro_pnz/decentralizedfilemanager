import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12

Button {
  id: button

  property string txt: ""

  implicitHeight: ThemeController.style.margin.m16
  implicitWidth: sectionText.width + ThemeController.style.margin.m16
  state: 'default'
  property bool contentHidden: true

  property alias textObject: sectionText
  property alias iconObject: icon

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      color: ThemeController.style.transparent
    }
  }

  contentItem: RowLayout {
    anchors.fill: parent
    spacing: 0
    UIKit.BaseText {
      id: sectionText
      text: txt
      font.weight: Font.DemiBold
      font.capitalization: Font.AllUppercase
      size: BaseText.TextSize.XBody
    }

    UIKit.SvgImage {
      id: icon
      source: contentHidden ? "qrc:/chevron-forward.svg" : "qrc:/chevron-down.svg"
      Layout.preferredWidth: ThemeController.style.margin.m16
      Layout.preferredHeight: ThemeController.style.margin.m16
    }
  }

  onClicked: {
    contentHidden = !contentHidden
  }

  MouseArea{
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: Qt.PointingHandCursor
  }

  states: [
    State {
      name: "default"
      when: !button.hovered
      PropertyChanges{
        target: icon
        visible: false
      }
    },

    State {
      name: "hovered"
      when: button.hovered
      PropertyChanges{
        target: icon
        visible: true
      }
    }
  ]
}
