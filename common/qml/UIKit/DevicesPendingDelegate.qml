import QtQuick 2.15
import Qt5Compat.GraphicalEffects

import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import com.test 1.0

AbstractButton {
  id: delegate
  property int requestsCount: 0
  property alias textItem: text
  property alias bgRect: bg
  property alias actionBtn: btn
  signal buttonClicked()
  enum Mode { Devices, Users, Contacts }
  property int mode: DevicesPendingDelegate.Mode.Devices
  property alias objectsModel: repeater.model
  property alias linearGradient: linearGradient

  implicitHeight: s(76)
  implicitWidth: parent.width

  background: Item {
    Rectangle {
      id: bg
      anchors.fill: parent
      radius: s(8)
    }

    LinearGradient {
      id: linearGradient
      anchors {
        fill: parent
      }
      source: bg
      gradient: ThemeController.style.devicesPendingDelegate.gradient
    }
  }

  onClicked: {
    buttonClicked()
  }

  contentItem: Item {
    anchors.fill: parent

    RowLayout {
      id: content
      anchors {
        fill: parent
        leftMargin: ThemeController.style.margin.m16
        rightMargin: ThemeController.style.margin.m16
      }
      spacing: ThemeController.style.margin.m8

      Row {
        spacing: -ThemeController.style.margin.m8

        Repeater {
          id: repeater
          //model: devicesModel
          delegate: Rectangle {
            property string txt: email ? email : ""
            width: s(40)
            height: s(40)
            color: ThemeController.style.devicesPendingDelegate.deviceCircleColor
            border.width: s(2)
            border.color: ThemeController.style.devicesPendingDelegate.deviceCircleBdColor
            radius: s(100)

            UIKit.SvgImage {
              width: s(24)
              height: s(24)
              source: visible ? modelData : ""
              anchors.centerIn: parent
              visible: mode === DevicesPendingDelegate.Mode.Devices
            }

            BaseText {
              anchors.centerIn: parent
              text: txt.length === 0 ? " " : txt.substr(0, 1);
              size: BaseText.TextSize.H3
              color: ThemeController.style.avatar.textColor
              font.weight: Font.Bold
              font.capitalization: Font.AllUppercase
              visible: mode === DevicesPendingDelegate.Mode.Users ||
                       mode === DevicesPendingDelegate.Mode.Contacts
            }
          }
        }
      }

      UIKit.BaseText {
        id: text
        text: mode === DevicesPendingDelegate.Mode.Devices ||
              mode === DevicesPendingDelegate.Mode.Contacts ?
              Strings.pendingRequests.arg(requestsCount) :
              Strings.pendingInvitations.arg(requestsCount)
        font.weight: Font.DemiBold
        Layout.alignment: Qt.AlignVCenter
        Layout.fillWidth: true
        color: ThemeController.style.devicesPendingDelegate.fontColor
      }

      UIKit.LargeIconButton {
        id: btn
        Layout.alignment: Qt.AlignVCenter
        icon.source: "qrc:/forward-white.svg"
        width: s(24)
        height: s(24)
        buttonStyle: ThemeController.style.devicesPendingDelegate.actionButton
        overlay.visible: false
        onClicked: {
        }
      }
    }
  }

  ListModel {
    id: devicesModel
  }

  Component.onCompleted: {
    devicesModel.append({ iconSrc: "qrc:/desktop-white.svg"});
    devicesModel.append({ iconSrc: "qrc:/tablet-white.svg"});
    devicesModel.append({ iconSrc: "qrc:/phone-white.svg"});
  }

  states: [
    State {
      name: "default"
      when: !delegate.hovered && !delegate.pressed
      PropertyChanges {
        target: linearGradient
        visible: true
      }
    },

    State {
      name: "hovered"
      when: delegate.hovered && !delegate.pressed
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.devicesPendingDelegate.bgColorHovered
      }
      PropertyChanges {
        target: linearGradient
        visible: false
      }
    },

    State {
      name: "pressed"
      when: delegate.pressed
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.devicesPendingDelegate.bgColorHovered
      }
      PropertyChanges {
        target: linearGradient
        visible: false
      }
    }
  ]
}
