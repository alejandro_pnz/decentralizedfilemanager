import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit

Button {
  id: button

  property string txt: ""
  property int sorter: type
  property string contentTxt: ""
  property var config: ThemeController.style.sortListDelegate

  implicitHeight: contentRow.height + topPadding + bottomPadding
  implicitWidth: parent.width
  checkable: true

  topPadding: app.isPhone && contentTxt === "" ? ThemeController.style.margin.m16 : ThemeController.style.margin.m12
  bottomPadding: app.isPhone && contentTxt === "" ? ThemeController.style.margin.m16 : ThemeController.style.margin.m12
  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16

  property bool separator: false
  property bool noVerticalMargins: false
  property alias iconObject: icon
  property alias textObject: textLabel
  property alias bg: bgRect

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      color: button.pressed ? config.bgColorPressed : button.hovered
                              ? config.bgColorHover : config.bgColor

      Rectangle {
        id: separatorComponent
        anchors {
          bottom: parent.bottom
          right: parent.right
          left: parent.left
        }
        height: s(1)
        color: config.separatorColor
        visible: button.separator
      }
    }
  }

  contentItem: RowLayout {
    id: contentRow
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.verticalCenter: parent.verticalCenter
    height: childrenRect.height
    anchors.leftMargin: noVerticalMargins ? 0 : leftPadding
    anchors.rightMargin: noVerticalMargins ? 0 : rightPadding

    ColumnLayout {
      Layout.alignment: Qt.AlignVCenter
      Layout.fillWidth: true
      spacing: ThemeController.style.margin.m4

      UIKit.BaseText {
        id: textLabel
        text: txt
        Layout.alignment: Qt.AlignVCenter
        color: button.hovered ? config.textColorHover : config.textColor
      }

      BaseText {
        id: subTitle
        text: contentTxt
        Layout.alignment: Qt.AlignVCenter
        visible: text !== ""
        size: BaseText.TextSize.Small
        color: ThemeController.style.shuttleColor
        Layout.fillWidth: true
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      }
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }

    SvgImage{
      id: icon
      Layout.preferredHeight: s(16)
      Layout.preferredWidth: s(16)
      Layout.alignment: Qt.AlignVCenter
      fillMode: Image.PreserveAspectFit
      source: "qrc:/check-mark.svg"
      Layout.rightMargin: noVerticalMargins ? ThemeController.style.margin.m8 : 0
      visible: button.checked
    }
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: Qt.PointingHandCursor
  }
}
