import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import Qt5Compat.GraphicalEffects


Button {
  id: button

  implicitWidth: s(32)
  implicitHeight: s(32)

  hoverEnabled: true
  property var buttonStyle: ThemeController.style.largeIconButton
  property int iconWidth: s(24)
  property int iconHeight: s(24)

  // Readonly properties for unit tests
  readonly property color bgColor: bgRect.color
  readonly property color iconColor: imgOverlay.color
  readonly property color dropShadowColor: dropShadow.color
  readonly property bool dropShadowVisible: dropShadow.visible
  property alias bgRect: bgRect
  property alias overlay: imgOverlay

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      color: {
        if (button.enabled) {
          if (button.down) {
            return button.buttonStyle.bgColorPressed
          } else if (button.hovered) {
            return button.buttonStyle.bgColorHover
          } else if (button.activeFocus) {
            return button.buttonStyle.bgColorFocus
          } else {
            return button.buttonStyle.bgColor
          }
        } else {
          return button.buttonStyle.bgColorDisabled
        }
      }
      visible: !dropShadow.visible
      radius: s(4)
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      horizontalOffset: 0
      verticalOffset: 0
      radius: s(8.0)
      //samples: radius * 2 + 1
      color: button.buttonStyle.dropShadowColor
      source: bgRect
      visible: button.activeFocus && !button.down
    }
  }

  contentItem: Item {
    anchors.fill: parent
    SvgImage {
      id: img
      anchors.centerIn: parent
      source: button.icon.source
      asynchronous: true
      width: button.iconWidth
      height: button.iconHeight
      fillMode: Image.PreserveAspectFit
    }
    ColorOverlay {
      id: imgOverlay
      anchors.fill: img
      source: img
      color: {
        if (button.enabled) {
          if (button.down) {
            return button.buttonStyle.iconColorPressed
          } else if (button.hovered) {
            return button.buttonStyle.iconColorHover
          } else if (button.checked) {
            return button.buttonStyle.iconColorChecked
          } else if (button.activeFocus) {
            return button.buttonStyle.iconColorFocus
          } else {
            return button.buttonStyle.iconColor
          }
        } else {
          return button.buttonStyle.iconColorDisabled
        }
      }
    }
  }

  // Accessibility
  Accessible.role: Accessible.Button
}
