import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects


import QmlVlc 0.1
import QtMultimedia 5.12

VideoOutput {
  property var vlcPlayer
  property var objectId
  property bool hasVideo: true
  fillMode: VideoOutput.PreserveAspectFit
  Image {
    anchors.fill: parent
    fillMode: Image.PreserveAspectFit
    // TODO remove temporary placeholder for mp3 when waves generator will be ready
    source: hasVideo ? "image://photo/" + objectId : "qrc:/audio_preview_waves.png"
    visible: vlcPlayer.state === VlcPlayer.NothingSpecial ||
             vlcPlayer.state === VlcPlayer.Opening
  }

  UIKit.PulseLoader {
    id: busyIndicator
    anchors.centerIn: parent
    color: ThemeController.style.button.primary.bgColor
    width: 100
    height: 50
    visible: running
    running: vlcPlayer.state === VlcPlayer.Opening ||
             vlcPlayer.state === VlcPlayer.Buffering
  }
}
