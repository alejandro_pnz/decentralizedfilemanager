import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects

import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

BaseTipButton {
  id: control

  property alias tipTitle: box.title
  property alias tipContent: box.content
  property alias tipInfo: box.info
  property alias imageSource: box.imageSource
  property bool showDontShowAgain: true
  property bool showPlaceholder: true

  TipBox {
    anchors.left: parent.left
    anchors.right: parent.right
    id: box
    showPlaceholder: control.showPlaceholder

    RowLayout {
      visible: control.showDontShowAgain
      Layout.topMargin: ThemeController.style.margin.m4
      Layout.fillWidth: true
      BaseText {
        text: Strings.dontShowItAgain
        font.weight: Font.DemiBold
        Layout.fillWidth: true
      }

      SwitchButton {
      }
    }

    StyledButton {
      text: Strings.gotIt
      Layout.fillWidth: true
      Layout.topMargin: ThemeController.style.margin.m20
      displayMode: StyledButton.DisplayMode.TextOnly
      onClicked: closeTip()
    }
  }
}
