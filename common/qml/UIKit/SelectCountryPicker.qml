import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import SortFilterProxyModel 0.2

Popup {
  y: combobox.height + s(8)
  width: combobox.width
  implicitHeight: Math.min(contentItemHeight, s(480))

  padding: s(1)
  topPadding: 0
  property alias searchText: search.searchText
  property var pickerModel
  property int contentItemHeight: search.height + listView.contentHeight + s(32)

  contentItem: ColumnLayout {
    width: parent.width
    height: parent.height
    spacing: ThemeController.style.margin.m8
    UIKit.SearchInput {
      id: search
      Layout.topMargin: ThemeController.style.margin.m16
      Layout.fillWidth: true
      Layout.leftMargin: ThemeController.style.margin.m16
      Layout.rightMargin: ThemeController.style.margin.m16
      placeholderText: Strings.searchCountries
      Layout.preferredHeight: s(48)
      micIconVisible: false
    }

    ListView {
      id: listView
      Layout.fillHeight: true
      Layout.fillWidth: true
      model: pickerModel
      clip: true
      ScrollIndicator.vertical: ScrollIndicator { }
    }
  }

  background: Item {
    Rectangle {
      id: contentRect
      radius: s(8)
      anchors.fill: parent
    }

    DropShadow {
      id: dropShadow
      anchors.fill: contentRect
      horizontalOffset: 0
      verticalOffset: 0
      radius: s(32)
      //samples: radius * 2 + 1
      color: Qt.rgba(0, 0, 0, 0.08)
      source: contentRect
    }
  }
}
