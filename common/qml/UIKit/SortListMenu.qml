import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import com.test 1.0
import UIModel 1.0

UIKit.GenericPopup {
  id: popup
  implicitHeight: listView.contentHeight + bottomPadding + topPadding
  implicitWidth: s(262)
  property var sortModel: undefined
  property var sortModelComponent: undefined

  leftPadding: 0
  rightPadding: 0
  topPadding: s(8)
  bottomPadding: s(8)

  leftMargin: ThemeController.style.margin.m12

  property int currentSorter: listView.currentItem ? listView.currentItem.sorter : Enums.LastUsed
  property string currentSorterName: listView.currentItem ? listView.currentItem.txt : ""

  function init(sorter){
    if (sortModelComponent !== undefined) {
      var idx = sortModelComponent.getIndexOfSorter(sorter)
      if (idx > -1) {
        listView.currentIndex = idx
        return
      }
    }

    listView.currentIndex = 0;

  }

//  property alias objectType: menuModel.objectType

  ListView {
    id: listView
    height: contentHeight
    width: parent.width
    model: sortModel
    currentIndex: -1
    delegate: SortListDelegate {
      ButtonGroup.group: buttonsGroup
      txt: name
      checked: listView.currentIndex === index
      onClicked: listView.currentIndex = index
    }
    onCurrentItemChanged: {

    }
  }

  ButtonGroup {
    id: buttonsGroup
    exclusive: true

    onClicked: {
      popup.close()
    }
  }
}
