import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

Item {
    id: popupHeader
    implicitHeight: titleComponent.contentHeight

    property string title
    signal closeClicked()

    UIKit.BaseText {
        id: titleComponent
        text: title
        font.weight: app.isPhone ? Font.Bold : Font.DemiBold
        size: UIKit.BaseText.TextSize.H3
        anchors.centerIn: parent
        horizontalAlignment: Text.AlignHCenter
    }

    UIKit.LargeIconButton {
        visible: !app.isMobile || app.isTablet
        icon.source: "qrc:/close.svg"
        height: s(32)
        width: s(32)
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }

        onClicked: closeClicked()
    }
}
