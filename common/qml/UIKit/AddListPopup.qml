import QtQuick 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects


UIKit.GenericPopup {
  id: popup
  implicitWidth: s(375)
  implicitHeight: layout.height + 2 * padding
  padding: ThemeController.style.margin.m16
  signal openFileDialog()

  ButtonGroup {
    buttons: layout.children
    onClicked: popup.close()
  }

  ColumnLayout {
    id: layout
    anchors{
      left: parent.left
      right: parent.right
    }
    spacing: ThemeController.style.margin.m12
    property int delegateHeight : s(80)
    UIKit.CardDelegate {
      id: addArcaCard
      icon.source: hovered ? "qrc:/add-arca-filled.svg" : "qrc:/add-arca.svg"
      title: Strings.addArca
      text: Strings.addArcaDescription
      Layout.preferredHeight: layout.delegateHeight
      pointingHandCursor: true
      onClicked: homeManager.enterCreateArca()
    }

    UIKit.CardDelegate {
      id: addQuodCard
      icon.source: hovered ? "qrc:/add-quod-filled.svg" : "qrc:/add-quod.svg"
      title: Strings.addQuod
      text: Strings.addQuodDescription
      Layout.preferredHeight: layout.delegateHeight
      pointingHandCursor: true
      onClicked: homeManager.enterCreateQuod()
    }

    UIKit.CardDelegate {
      id: addFileCard
      icon.source: hovered ?"qrc:/add-file-filled.svg" : "qrc:/add-file.svg"
      title: Strings.addFile
      text: Strings.addFileDescription
      Layout.preferredHeight: layout.delegateHeight
      pointingHandCursor: true
      onClicked: openFileDialog()
    }
  }
}
