import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import SortFilterProxyModel 0.2

Item {
  property var input
  property var inputIcon
  property bool updateVariable
  property alias tip: tip
  UIKit.ToolTip {
    id: tip
    parent: Overlay.overlay
    x: {
      if (updateVariable){}
      var iconPos = inputIcon.mapToItem(null, inputIcon.width * 0.5,0)
      return (iconPos.x + s(24) - tip.width)
    }
    y: {
      if (updateVariable) {}
      var iconPos = inputIcon.mapToItem(null, 0,0)
      var inputPos = input.mapToItem(null, 0, input.height)
      return inputPos.y + s(17)
    }
    triangle.pointing: UIKit.CanvasTriangle.Pointing.Up
    triangle.width: s(12)
    triangle.height: s(9)
    triangle.anchors {
      right: triangle.parent.right
      left: undefined
      rightMargin: s(18)
      top: undefined
      bottom: triangle.parent.top
    }
  }
}
