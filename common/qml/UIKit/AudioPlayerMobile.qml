import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects


Item {
  implicitWidth: parent.width
  implicitHeight: s(85)

  property alias bgRect: bgRect
  property alias slider: slider
  property alias startTxt: startTxt
  property alias endTxt: endTxt
  property alias backwardButton: backwardButton
  property alias pauseButton: pauseButton
  property alias forwardButton: forwardButton

  signal backward()
  signal forward()
  signal pause()

  Rectangle {
    id: bgRect
    anchors{
      fill: parent
    }
    radius: ThemeController.style.margin.m16
    color: ThemeController.style.audioPlayer.bgColor

    MediaSlider {
      id: slider
      anchors {
        top: parent.top
        topMargin: ThemeController.style.margin.m16
        left: parent.left
        right: parent.right
        leftMargin: ThemeController.style.margin.m16
        rightMargin: ThemeController.style.margin.m16
      }
    }

    UIKit.BaseText {
      id: startTxt
      text: "1:11"
      anchors {
        top: slider.bottom
        topMargin: 4
        left: slider.left
      }
      size: UIKit.BaseText.TextSize.Small
      horizontalAlignment: Text.AlignHCenter
      Layout.alignment: Qt.AlignHCenter
      color: ThemeController.style.audioPlayer.textColor
    }

    UIKit.BaseText {
      id: endTxt
      text: "5:28"
      anchors {
        top: slider.bottom
        topMargin: 4
        right: slider.right
      }
      size: UIKit.BaseText.TextSize.Small
      horizontalAlignment: Text.AlignHCenter
      Layout.alignment: Qt.AlignHCenter
      color: ThemeController.style.audioPlayer.textColor
    }

    Row {
      spacing: 24
      anchors {
        bottom: parent.bottom
        bottomMargin: 16
        horizontalCenter: parent.horizontalCenter
      }

      UIKit.LargeIconButton {
        id: backwardButton
        icon.source: "qrc:/backward.svg"
        onClicked: backward()
      }

      UIKit.LargeIconButton {
        id: pauseButton
        icon.source: "qrc:/pause.svg"
        onClicked: pause()
      }

      UIKit.LargeIconButton {
        id: forwardButton
        icon.source: "qrc:/forward.svg"
        onClicked: forward()
      }
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: bgRect
    source: bgRect
    color: ThemeController.style.audioPlayer.dropShadowColor
    radius: 20
    verticalOffset: 4
    horizontalOffset: 4
    //samples: radius * 2 + 1
  }
}
