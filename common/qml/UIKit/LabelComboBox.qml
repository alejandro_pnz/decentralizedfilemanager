import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ComboBox {
  id: combobox

  model: [Strings.myAccount, Strings.myLogin]
  width: content.childrenRect.width
  height: isMobile ? s(24) : s(32)

  property string title: Strings.dataDashboard + ": " + combobox.displayText
  property string pickerTitle: Strings.dataDashboard
  property bool isMobile: Qt.platform.os === "android" || Qt.platform.os === "ios"

  property alias textLabel: textLabel
  property alias icon: icon
  property alias picker: picker

  indicator: Item {}

  contentItem: Item {
    id: content

    BaseText {
      id: textLabel
      text: title
      font.weight: Font.Bold
      anchors {
        left: parent.left
        verticalCenter: parent.verticalCenter
      }
      size: isMobile ? BaseText.TextSize.Body : BaseText.TextSize.H3
    }

    UIKit.SvgImage {
      id: icon
      height: s(24)
      width: s(24)
      source: "qrc:/chevron-down.svg"
      anchors {
        left: textLabel.right
        leftMargin: ThemeController.style.margin.m4
        verticalCenter: parent.verticalCenter
      }
    }
  }

  background: Rectangle {
    width: combobox.width
    height: combobox.height
  }

  popup: UIKit.Picker {
    id: picker
    pickerModel: combobox.delegateModel
    title: pickerTitle
  }

  delegate: SortListDelegate {
    ButtonGroup.group: buttonGroup
    width: isMobile ? picker.width - s(32) : combobox.width
    txt: modelData
    bg.color: ThemeController.style.transparent
    separator: isMobile ? true : false
    noVerticalMargins: isMobile ? true : false
    checked: index === 0 ? true : false
  }

  ButtonGroup {
    id: buttonGroup
    exclusive: true

    onClicked: {
      picker.close()
    }
  }
}

