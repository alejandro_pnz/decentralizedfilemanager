import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0

ToolTip {
  id: control
  padding: s(12)
  background: Rectangle {
    color: ThemeController.style.htmlLinkEditor.bgColor
    radius: s(4)
  }
  contentItem: BaseText {
    text: control.text
    color: ThemeController.style.htmlLinkEditor.textColor
  }
}
