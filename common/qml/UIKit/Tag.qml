import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0

AbstractButton {
  id: control
  property string tagColor: ""
  property alias colorObject: colorIndicator
  property alias tagLabel: tagName
  property bool deleteButtonVisible: false
  signal deleteButtonClicked()

  implicitHeight: s(32)
  implicitWidth: contentRow.width + s(32)

  background: Rectangle {
    radius: s(100)
    border.width: s(1)
    border.color: ThemeController.style.ironColor
  }

  Row {
    id: contentRow
    spacing: ThemeController.style.margin.m8
    anchors.left: parent.left
    anchors.leftMargin: ThemeController.style.margin.m16
    anchors.verticalCenter: parent.verticalCenter
    Rectangle {
      id: colorIndicator
      radius: s(100)
      height: s(8)
      width: s(8)
      color: tagColor
      anchors.verticalCenter: parent.verticalCenter
    }

    UIKit.BaseText {
      id: tagName
      text: control.text
      anchors.verticalCenter: parent.verticalCenter
    }

    UIKit.ColoredImage {
      visible: control.deleteButtonVisible
      source: "qrc:/close.svg"
      icon.width: s(16)
      icon.height: s(16)
      color: ThemeController.style.shuttleColor
      anchors.verticalCenter: parent.verticalCenter
      MouseArea {
        anchors.fill: parent
        onClicked: deleteButtonClicked()
      }
    }
  }
}
