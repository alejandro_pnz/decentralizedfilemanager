import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit

RadioButton {
  id: control
  height: ThemeController.style.radioButton.height
  enabled: true

  indicator: Rectangle {
    implicitWidth: ThemeController.style.radioButton.height
    implicitHeight: ThemeController.style.radioButton.height
    radius: s(100)
    border.color: control.enabled ? ThemeController.style.radioButton.defaultBdColor :
                                    ThemeController.style.radioButton.disabledBdColor
    border.width: control.checked ? 0 : 1.5
    color: control.checked ? control.enabled ? ThemeController.style.radioButton.checkedBgColor
    : ThemeController.style.radioButton.disabledCheckedBgColor
    : ThemeController.style.radioButton.defaultBgColor
    property alias middle: indicatorMiddle

    Rectangle {
      id: indicatorMiddle
      anchors.centerIn: parent
      width: ThemeController.style.radioButton.middleHeight
      height: ThemeController.style.radioButton.middleHeight
      radius: s(100)
      color: ThemeController.style.radioButton.middleColor
      visible: control.checked
    }
  }

  contentItem: UIKit.BaseText {
    text: control.text
    anchors.verticalCenter: indicator.verticalCenter
    leftPadding: indicator.width + ThemeController.style.margin.m12
  }
}
