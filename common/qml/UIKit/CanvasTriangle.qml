import QtQuick 2.15
import AppStyles 1.0

Canvas {
  id: canvas

  enum Pointing {
    Up, Down, Left, Right
  }

  property int pointing: CanvasTriangle.Pointing.Left
  property color color: "white"
  width: s(9)
  height: s(12)

  onPointingChanged:  requestPaint()
  antialiasing: true
  smooth: true
  contextType: "2d"

  // Note (jskorczynski): I'm aware that this is not the best way to rotate
  // the triangle but it was the fastest way. Another posibility is to use
  // context(rotate), but hten you have to play with translations
  // and component sizing
  onPaint: {
    getContext(contextType)
    context.reset();
    context.beginPath();
    switch (canvas.pointing) {
    case CanvasTriangle.Pointing.Left:
      context.moveTo(width, 0);
      context.lineTo(width, height);
      context.lineTo(0, height * .5);
      break;
    case CanvasTriangle.Pointing.Right:
      context.moveTo(0, 0);
      context.lineTo(0, height);
      context.lineTo(width, height * .5);
      break;
    case CanvasTriangle.Pointing.Up:
      context.moveTo(0, height);
      context.lineTo(width, height);
      context.lineTo(width * .5, 0);
      break;
    case CanvasTriangle.Pointing.Down:
      context.moveTo(0, 0);
      context.lineTo(width, 0);
      context.lineTo(width * .5, height);
      break;
    }
    context.closePath();
    context.fillStyle = canvas.color;
    context.fill();
  }
}
