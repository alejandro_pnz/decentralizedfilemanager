import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import Qt5Compat.GraphicalEffects


AbstractButton {
  id: root
  implicitHeight: content.height + 2 * content.anchors.margins
  state: "default"
  checkable: true

  signal detailsButtonClicked()

  property string title: ""
  property string price: ""
  property string contentText: ""
  property string nextPaymentDate: ""
  property bool displayDetails: true
  property bool showDetailsButton: true
  property bool upgrade: false
  property bool activePlan: false
  property bool detailsButtonCentered: false

  background: Item {
    Rectangle {
      anchors.fill: parent
      id: bg
      radius: s(8)
      color: "transparent"
      border.width: root.checked ? s(2) : s(1)
      border.color: root.checked ? ThemeController.style.slatePurpleColor : ThemeController.style.mercuryColor
      visible: !dropShadow.visible
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bg
      source: bg
      color: Qt.rgba(0,0,0,0.05)
      radius: root.checked ? s(16) : s(10)
      verticalOffset: root.checked ? s(8) : s(2)
      //samples: radius * 2 + 1
    }
  }

  ColumnLayout {
    id: content
    spacing: ThemeController.style.margin.m16
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
    }

    RowLayout {
      id: titleLayout
      Layout.fillWidth: true
      spacing: s(25)
      UIKit.BaseText {
        id: titleComponent
        text: title
        size: UIKit.BaseText.TextSize.H3
        font.weight: Font.Bold
        Layout.fillWidth: true
      }
      UIKit.BaseText {
        text: price
        font.weight: Font.DemiBold
        horizontalAlignment: Qt.AlignRight
      }
    }

    Rectangle {
      Layout.fillWidth: true
      Layout.leftMargin: -s(23)
      Layout.rightMargin: -s(23)
      Layout.preferredHeight: s(40)
      visible: nextPaymentDate !== ""
      color: ThemeController.style.whiteSmokeColor

      UIKit.BaseText {
        id: nextPayment
        text: Strings.nextPayment.arg(nextPaymentDate)
        anchors {
          left: parent.left
          leftMargin: ThemeController.style.margin.m24
          verticalCenter: parent.verticalCenter
        }
        color: ThemeController.style.shuttleColor
        size: UIKit.BaseText.TextSize.Small
      }
    }

    UIKit.BaseText {
      id: infoText
      text: contentText
      Layout.fillWidth: true
      Layout.topMargin: -ThemeController.style.margin.m4
      visible: text !== ""
    }

    Rectangle {
      id: line
      height: s(1)
      Layout.fillWidth: true
      color: ThemeController.style.seashellColor
    }

    Repeater {
      model: displayDetails ? 5 : 0
      RowLayout {
        Layout.topMargin: ThemeController.style.margin.m8
        spacing: ThemeController.style.margin.m12
        UIKit.SvgImage {
          Layout.alignment: Qt.AlignTop
          source: "qrc:/check-circle.svg"
          width: s(24)
          height: s(24)
        }
        UIKit.BaseText {
          text: "Lorem ipsum dolor si amet"
          Layout.alignment: Qt.AlignTop
          Layout.fillWidth: true
        }
      }
    }

    UIKit.StyledButton {
      id: upgradeButton
      text: Strings.upgrade
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.fillWidth: true
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      type: UIKit.StyledButton.ButtonStyle.Primary
      visible: upgrade
    }

    UIKit.StyledButton {
      id: cancelButton
      text: Strings.cancelSubscription
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      type: UIKit.StyledButton.ButtonStyle.Secondary
      Layout.fillWidth: true
      visible: activePlan
    }

    UIKit.StyledButton {
      id: detailsButton
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.alignment: detailsButtonCentered ? Qt.AlignHCenter : Qt.AlignLeft
      text: Strings.seeAllBenefits
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      type: UIKit.StyledButton.ButtonStyle.Text
      onClicked: detailsButtonClicked(title, price)
    }
  }

  states: [
    State {
      name: "default"
      PropertyChanges {
        target: detailsButton
        visible: showDetailsButton
      }
      PropertyChanges {
        target: bg
        border.width: root.checked ? s(2) : s(1)
        border.color: root.checked ? ThemeController.style.slatePurpleColor : ThemeController.style.mercuryColor
      }
      PropertyChanges {
        target: line
        visible: false
      }
      PropertyChanges {
        target: titleLayout
        Layout.fillWidth: true
      }
      PropertyChanges {
        target: titleComponent
        Layout.fillWidth: true
      }
      PropertyChanges {
        target: content
        anchors.margins: ThemeController.style.margin.m24
      }
      PropertyChanges {
        target: dropShadow
        visible: true
      }
    },
    State {
      name: "details"
      PropertyChanges {
        target: detailsButton
        visible: false
      }
      PropertyChanges {
        target: bg
        border.width: 0
      }
      PropertyChanges {
        target: line
        visible: true
      }
      PropertyChanges {
        target: titleLayout
        Layout.fillWidth: false
      }
      PropertyChanges {
        target: titleComponent
        Layout.fillWidth: false
      }
      PropertyChanges {
        target: content
        anchors.margins: 0
      }
      PropertyChanges {
        target: dropShadow
        visible: false
      }
    },
    State {
      name: "detailsMobile"
      PropertyChanges {
        target: detailsButton
        visible: false
      }
      PropertyChanges {
        target: bg
        border.width: 0
      }
      PropertyChanges {
        target: line
        visible: true
      }
      PropertyChanges {
        target: titleLayout
        Layout.fillWidth: true
      }
      PropertyChanges {
        target: titleComponent
        Layout.fillWidth: true
      }
      PropertyChanges {
        target: content
        anchors.margins: 0
      }
      PropertyChanges {
        target: dropShadow
        visible: false
      }
    }
  ]
}
