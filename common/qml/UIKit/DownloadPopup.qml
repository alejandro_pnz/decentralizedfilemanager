import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects


UIKit.GenericPopup {
  implicitWidth: s(166)
  implicitHeight: s(112)
  modal: false
  padding: s(1)

  contentItem: ColumnLayout {
    Layout.topMargin: ThemeController.style.margin.m8
    Layout.bottomMargin: ThemeController.style.margin.m8
    Layout.fillHeight: true
    Layout.fillWidth: true
    spacing: 0

    UIKit.ActionListDelegate {
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
      leftPadding: ThemeController.style.margin.m16
      rightPadding: 0
      Layout.fillWidth: true
      text: Strings.downloadAsImage
      actionIcon: false
      display: AbstractButton.TextOnly
      separator: false
      checkIndicator.visible: false
      pointingHandCursor: true
      onClicked: close()
    }

    UIKit.ActionListDelegate {
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
      leftPadding: ThemeController.style.margin.m16
      rightPadding: 0
      Layout.fillWidth: true
      text: Strings.downloadAsPdf
      actionIcon: false
      display: AbstractButton.TextOnly
      separator: false
      checkIndicator.visible: false
      pointingHandCursor: true
      onClicked: close()
    }
  }
}
