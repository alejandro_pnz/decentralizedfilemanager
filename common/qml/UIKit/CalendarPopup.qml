import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects


UIKit.GenericPopup {
  id: picker
  implicitHeight: calendar.height + 2 * padding
  implicitWidth: calendar.width + 2 * padding
  padding: ThemeController.style.margin.m16
  modal: false
  property alias calendar: calendar
  property var dateInput

  UIKit.Calendar {
    id: calendar
  }
}
