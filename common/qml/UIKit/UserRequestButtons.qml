import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import com.test 1.0

RowLayout {
  spacing: ThemeController.style.margin.m16
  implicitHeight: s(32)
  property alias rejectButton: rejectButton
  property alias inviteButton: inviteButton

  StyledButton {
    id: inviteButton
    type: StyledButton.ButtonStyle.Text
    displayMode: StyledButton.DisplayMode.TextRightSideIcon
    text: Strings.invite
    icon.source: "qrc:/check-purple.svg"
    onClicked: inviteClicked()
    Layout.alignment: Qt.AlignVCenter
    overlayVisible: false
  }

  Rectangle {
    Layout.preferredWidth: s(1)
    Layout.preferredHeight: s(32)
    color: ThemeController.style.seashellColor
    Layout.alignment: Qt.AlignVCenter
  }

  StyledButton {
    id: rejectButton
    type: StyledButton.ButtonStyle.Text
    displayMode: StyledButton.DisplayMode.TextRightSideIcon
    text: Strings.reject
    icon.source: "qrc:/pending-circle.svg"
    onClicked: rejectClicked()
    Layout.alignment: Qt.AlignVCenter
  }
}
