import QtQuick 2.0
import QtQuick.Controls 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import AppStyles 1.0

Popup {
  id: popup
  width: root.getWidth(8)
  anchors.centerIn: parent
  verticalPadding: ThemeController.style.margin.m40
  modal: true
  closePolicy: Popup.NoAutoClose
  opacity: visible ? 1.0 : 0.5

  property int cntHeight: contentItem.height
  property int maximumHeight: parent.height - 2 * s(80)
  property bool showBackButton: true
  property bool showCloseButton: true
  property bool autoback: true
  property alias dropShadow: dropShadow

  signal closeButtonClicked()

  function closeButtonHandler() {
    closeButtonClicked()
    if (autoback) back()
  }

  Overlay.modal: Rectangle {
    color: Qt.rgba(0,0,0,0.4)
  }


  background: Item {
    id: bgContent
    Rectangle {
      id: bg
      radius: s(8)
      visible: false
      anchors.fill: parent
    }
    DropShadow {
      id: dropShadow
      anchors.fill: parent
      source: bg
      color: Qt.rgba(0,0,0,0.12)
      verticalOffset: 0
      radius: s(16)
      //samples: radius * 2 + 1
    }
    UIKit.StyledButton {
      id: backButton
      type: ThemeController.style.backButtonDisplayMode
      displayMode: UIKit.StyledButton.DisplayMode.IconOnly
      icon.source: "qrc:/chevron-back.svg"
      height: ThemeController.style.toolbarHeight
      width: height
      onClicked: back()
      anchors.left: parent.left
      anchors.top: parent.top
      anchors.topMargin: ThemeController.style.margin.m24
      anchors.leftMargin: ThemeController.style.margin.m24
      visible: showBackButton
    }
    UIKit.StyledButton {
      id: closeButton
      type: ThemeController.style.backButtonDisplayMode
      displayMode: UIKit.StyledButton.DisplayMode.IconOnly
      icon.source: "qrc:/close.svg"
      height: ThemeController.style.toolbarHeight
      width: height
      onClicked: closeButtonHandler()
      anchors.right: parent.right
      anchors.top: parent.top
      anchors.topMargin: ThemeController.style.margin.m24
      anchors.rightMargin: ThemeController.style.margin.m24
      visible: showCloseButton
    }
  }

  Behavior on opacity {
    NumberAnimation { duration: 200 }
  }
}
