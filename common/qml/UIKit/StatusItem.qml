import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import com.test 1.0

Item {
  height: s(10)
  width: s(10)
  property bool isOnline: true
  property alias colorRectangle: statusComponent

  Rectangle {
    id: statusComponent
    anchors.fill: parent
    border.width: s(1)
    radius: s(3)
    border.color: ThemeController.style.whiteColor
    color: isOnline ? ThemeController.style.shamrockColor : ThemeController.style.manateeColor
  }

  DropShadow {
    id: stateShadow
    anchors.fill: statusComponent
    horizontalOffset: 0
    verticalOffset: 0
    radius: s(4)
    //samples: radius * 2 + 1
    color: Qt.rgba(0, 0, 0, 0.08)
    source: statusComponent
  }
}
