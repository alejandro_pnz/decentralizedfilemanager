import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects


Item {
  id: itemContainer
  implicitWidth: parent.width
  implicitHeight: Math.max(iconContainer.height, textItem.height) + s(24)

  property int minWidth: s(268)
  property string contentText: ""
  property alias bgRect: contentRect
  property alias iconContainer: iconContainer
  property alias icon: icon
  property alias textItem: textItem
  property alias interval: timer.interval
  property var notificationStyle: ThemeController.style.notificationDelegate

  function show() {
    visible = true
    timer.start()
  }

  Timer {
    id: timer
    interval: 5000
    onTriggered: itemContainer.visible = false
  }

  Rectangle {
    id: contentRect
    anchors {
      top: parent.top
      bottom: parent.bottom
      horizontalCenter: parent.horizontalCenter
    }

    TextMetrics {
      id: textMetrics
      text: contentText
      font.pixelSize: ThemeController.style.font.bodySize
      font.weight: Font.DemiBold
      font.family: ThemeController.style.font.primaryFontFamily
    }

    property int componentsWidth: iconContainer.width + s(32) + textMetrics.width
    width: componentsWidth < itemContainer.implicitWidth ? componentsWidth < minWidth ? minWidth :
           componentsWidth : itemContainer.implicitWidth
    color: notificationStyle.bgColor
    radius: s(6)

    Rectangle {
      id: iconContainer
      width: s(40)
      height: s(40)
      color: notificationStyle.iconBgColor
      radius: s(100)
      anchors {
        left: parent.left
        leftMargin: ThemeController.style.margin.m12
        top: parent.top
        topMargin: ThemeController.style.margin.m12
      }

      UIKit.SvgImage {
        id: icon
        height: s(16)
        width: s(16)
        source: "qrc:/email-white.svg"
        anchors.centerIn: parent
      }
    }

    UIKit.BaseText {
      id: textItem
      text: contentText
      font.weight: Font.DemiBold
      color: notificationStyle.fontColor
      anchors {
        left: iconContainer.right
        leftMargin: ThemeController.style.margin.m8
        right: parent.right
        rightMargin: ThemeController.style.margin.m12
        verticalCenter: parent.verticalCenter
      }
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: contentRect
    horizontalOffset: 0
    verticalOffset: 0
    radius: s(16)
    //samples: 2 * radius + 1
    color: notificationStyle.dropShadowColor
    source: contentRect
  }
}
