import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

Rectangle {
  id: tip
  implicitWidth: s(mode === ContactTooltip.Mode.Big ? 360 : 252)
  implicitHeight: s(mode === ContactTooltip.Mode.Big ? 108 : 88)
  color: ThemeController.style.charadeColor
  radius: s(2)
  property string userEmail: ""
  property string roleTxt: ""
  property alias triangle: triangle
  property int mode: ContactTooltip.Mode.Big

  enum Mode {
    Big, Small
  }

  UIKit.CanvasTriangle {
    id: triangle
    color: tip.color
    anchors.horizontalCenter: parent.horizontalCenter
    pointing: UIKit.CanvasTriangle.Pointing.Down
    anchors.top: parent.bottom
    visible: true
    width: s(12)
    height: s(9)
  }

  UIKit.ContactCard {
    title: userEmail
    checkBoxMode: false
    checkable: false
    anchors.fill: parent
    anchors.margins: ThemeController.style.margin.m12
    avatarObject.size: mode === ContactTooltip.Mode.Big ? UIKit.Avatar.Size.Big
                                                        : UIKit.Avatar.Size.Small
    avatarObject.borderless: true
    bg.color: ThemeController.style.mirageColor
    bg.radius: 0
    email.color: ThemeController.style.whiteColor
    labelBottom.color: ThemeController.style.charadeColor
    bottomLabelContent.color: ThemeController.style.whiteColor
    labelBottom.visible: roleTxt !== ""
    labelLeft.visible: false
    role: roleTxt
    states: []
    rightCheckbox.visible: false
    leftCheckbox.visible: false
    iconObject.visible: false
  }
}
