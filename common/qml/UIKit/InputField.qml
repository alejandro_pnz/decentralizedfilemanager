import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0

Item {
  id: inputField
  property color bgColor: ThemeController.style.textInput.bgColor
  property string labelText
  property string placeholderTxt
  property real borderWidth: s(1.0)
  property color borderColor: ThemeController.style.textInput.defaultBorderColor
  property bool inputRequired: false
  property bool isPassPhrase: false
  property var errorMessage: []
  property bool clearIconVisible: !eyeIconVisible
  property bool eyeIconVisible: false
  property bool leftEyeIcon: false
  property string rightIconSource: eyeIconVisible ? (textInput.echoMode === TextInput.Normal ? "qrc:/view-off.svg" : "qrc:/dark-eye.svg") : clearIconVisible ? "qrc:/close-icon.svg" : ""
  property bool strengthIndicatorVisible: false
  property bool capsLockWarning: isPassPhrase
  property string leftIconSource: leftEyeIcon ? (textInput.echoMode === TextInput.Normal ? "qrc:/view-off.svg" : "qrc:/dark-eye.svg") : ""
  property alias textField: textInput
  property alias rightIconBtn: rightIconButton
  property alias leftIconBtn: leftIconButton
  property alias infoBoxModel: infoBoxModel
  property alias capsLockInfoBox: capsLockInfoBox
  property alias lbl: label
  property alias validator: textInput.validator
  property alias inputMethodHints: textInput.inputMethodHints
  property bool isMobile: Qt.platform.os === "android" || Qt.platform.os === "ios"
  property bool overrideRightButton: false
  property bool overrideLeftButton: false
  property int fieldType: InputField.FieldType.Default
  property bool hasErrors: inputField.state === 'error'
  property string passwordMask: "************"

  property alias passwordStrengthIndicator: strengthIndicator

  signal textChanged(string text)
  signal rightButtonClicked()
  signal leftButtonClicked()
  signal tabPressed()
  signal backtabPressed()
  signal enterPressed()

  default property alias children: labelRightContent.children

  implicitHeight: childrenRect.height
  implicitWidth: parent.width
  state: 'default'

  enum FieldType {
    Default,
    Email
  }

  onRightButtonClicked: {
    if (overrideRightButton) return
    if(eyeIconVisible){
      isPassPhrase = !isPassPhrase
    } else {
      textInput.clear()
    }
  }

  onLeftButtonClicked: {
    if (overrideLeftButton) return
    if(leftEyeIcon){
      isPassPhrase = !isPassPhrase
    }
  }

  ColumnLayout {
    id: columnLayout
    width: parent.width
    spacing: ThemeController.style.margin.m8

    RowLayout {
      id: labelLayout
      spacing: ThemeController.style.margin.m4
      width: parent.width
      visible: labelText !== ""

      BaseText {
        id: label
        text: labelText + (inputRequired ? " <font color=\"%1\">*</font>".arg(ThemeController.style.textInput.asteriskColor) : "")
        font.weight: Font.DemiBold
        color: ThemeController.style.textInput.labelTextColor
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
//        Layout.maximumWidth: implicitWidth
        Layout.fillWidth: true

        Accessible.ignored: true
      }

      Item {
        id: labelRightContent
        Layout.fillWidth: true
        Layout.preferredHeight: 24
      }
    }

    Rectangle {
      Layout.fillWidth: true
      Layout.preferredHeight: ThemeController.style.defaultInputHeight
      id: inputBg
      radius: s(4)
      width: parent.width
      height: parent.height
      color: bgColor
      border.color: borderColor
      border.width: borderWidth

      TextField {
        id: textInput
        anchors {
          left: parent.left
          right: itemsRow.left
          rightMargin: ThemeController.style.margin.m12
          top: parent.top
          bottom: parent.bottom
        }
        echoMode: isPassPhrase ? TextInput.Password : TextInput.Normal
        hoverEnabled: true
        selectByMouse: true
        selectedTextColor: ThemeController.style.whiteColor
        inputMethodHints: {
          switch(fieldType) {
          case InputField.FieldType.Default: return Qt.ImhNone
          case InputField.FieldType.Email: return Qt.ImhEmailCharactersOnly
            |Qt.ImhPreferLowercase |Qt.ImhNoAutoUppercase |Qt.ImhNoPredictiveText
          }
        }

        validator: RegularExpressionValidator {
          regularExpression:
              switch(fieldType) {
              case InputField.FieldType.Default: return /.*/
              case InputField.FieldType.Email: return  Strings.emailRegExp
              }
        }

        background: Item{}

        color: readOnly || !enabled ? ThemeController.style.textInput.disabledTextColor
                                    : ThemeController.style.textInput.textColor
        font.pixelSize: ThemeController.style.textInput.fontSize
        placeholderText: placeholderTxt
        placeholderTextColor: ThemeController.style.textInput.placeholderTextColor
        leftPadding: ThemeController.style.margin.m12
        rightPadding: 0

        onActiveFocusChanged: {
          if (activeFocus) {
            if (inputField.state === 'error'){
              return
            }
            if (focusReason === Qt.TabFocusReason){
              inputField.state = 'focused'
            }
            else {
              inputField.state = 'active'
            }
          } else {
            inputField.state = 'default'
          }
        }

        onHoveredChanged: {
          if (activeFocus || inputField.state === 'error'){
            return
          }
          else if(hovered){
            inputField.state = 'hovered'
          }
          else{
            inputField.state = 'default'
          }
        }

        onPressed: {
          if (activeFocus || inputField.state === 'error'){
            return
          }
          if (inputField.state === 'focused'){
            inputField.state = 'active'
          }
        }

        onTextChanged: inputField.textChanged(textInput.text)

        Keys.onBacktabPressed: backtabPressed()
        Keys.onTabPressed: tabPressed()
        Keys.onReturnPressed: enterPressed()
      }

      RowLayout {
        id: itemsRow
        spacing: ThemeController.style.margin.m4
        anchors.right: parent.right
        anchors.rightMargin: ThemeController.style.margin.m12
        anchors.verticalCenter: parent.verticalCenter

        FieldIconButton {
          id: leftIconButton
          width: s(16)
          height: s(16)
          Layout.alignment: Qt.AlignVCenter
          iconSrc: leftIconSource
          active: inputField.state === "active" || inputField.state === "error"
                  || isMobile ? textInput.preeditText.length > 0 : textInput.text.length > 0
          onClicked: leftButtonClicked()
          visible: leftIconSource !== ""
        }

        Rectangle {
          id: separator
          height: s(24)
          width: s(1)
          color: ThemeController.style.seashellColor
          Layout.alignment: Qt.AlignVCenter
          visible: leftIconSource !== "" && rightIconButton.visible
        }

        FieldIconButton {
          id: rightIconButton
          width: s(16)
          height: s(16)
          Layout.alignment: Qt.AlignVCenter
          iconSrc: rightIconSource
          onClicked: rightButtonClicked()
          active: inputField.state === "active" || inputField.state === "error"
                  || isMobile ? textInput.preeditText.length > 0 : textInput.text.length > 0
          visible: clearIconVisible ? (isMobile ? textInput.preeditText.length > 0
                                                : textInput.text.length > 0) : rightIconSource !== ""
        }
      }
    }

    Errorbox {
      id: capsLockInfoBox
      errorMsg: Strings.capsLockIsActive
      visible: capsLockWarning && inputField.state !== 'default' && inputField.state !== 'hovered' && app.capsLockOn
    }

    Repeater {
      id: infoBoxModel
      model: inputField.errorMessage !== undefined ? inputField.errorMessage : undefined
      Errorbox {
        errorMsg: modelData
        visible: inputField.state === 'error'
      }
    }
    PasswordStrengthIndicator{
      id: strengthIndicator
      visible: strengthIndicatorVisible && textInput.text !== ""
    }
  }

  states: [
    State {
      name: "error"
      PropertyChanges {
        target: inputField
        borderColor: ThemeController.style.textInput.errorBorderColor
        borderWidth: s(1.5)
      }
    },

    State {
      name: "focused"
      PropertyChanges {
        target: inputField;
        borderColor: ThemeController.style.textInput.focusedBorderColor
        borderWidth: s(1)
      }
      PropertyChanges {
        target: textInput
        readOnly: true
      }
    },

    State {
      name: "default"
      PropertyChanges {
        target: inputField
        borderColor: ThemeController.style.textInput.defaultBorderColor
        borderWidth: s(1)
      }
    },

    State {
      name: "hovered"
      PropertyChanges {
        target: inputField
        borderColor: ThemeController.style.textInput.hoveredBorderColor
        borderWidth: s(1)
      }
    },

    State {
      name: "active"
      PropertyChanges {
        target: inputField
        borderColor: ThemeController.style.textInput.activeBorderColor
        borderWidth: s(1.5)
      }
    }
  ]

  // Accessibility
  Accessible.role: Accessible.EditableText
  Accessible.name: inputField.labelText
  Accessible.passwordEdit: inputField.isPassPhrase
  Accessible.readOnly: textInput.readOnly
}
