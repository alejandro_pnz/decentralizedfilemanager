import QtQuick 2.15
import Qt5Compat.GraphicalEffects

import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import com.test 1.0

AbstractButton {
  id: delegate

  text: Strings.changeYourWeakPasswords
  property string title: Strings.weakPasswordsDetected.arg(passwordsCount)
  property int passwordsCount: 0
  property alias titleItem: titleItem
  property alias textItem: contentText
  property alias bgRect: bg
  property alias actionBtn: btn
  property alias avatar: avatar
  property alias avatarIcon: avatarIcon
  property alias linearGradient: linearGradient
  signal buttonClicked()

  implicitHeight: content.height + s(32)
  implicitWidth: parent.width
  padding: ThemeController.style.margin.m16

  background: Item {
    Rectangle {
      id: bg
      anchors.fill: parent
      radius: s(8)
    }

    LinearGradient {
      id: linearGradient
      anchors {
        fill: parent
      }
      source: bg
      gradient: ThemeController.style.weakPasswordsDelegate.gradient
    }
  }

  onClicked: {
    buttonClicked()
  }

  contentItem: Item {
    RowLayout {
      id: content
      anchors {
        top: parent.top
        left: parent.left
        right: parent.right
      }
      spacing: ThemeController.style.margin.m12

      Rectangle {
        id: avatar
        width: s(52)
        height: s(52)
        color: ThemeController.style.weakPasswordsDelegate.avatarColor
        border.width: ThemeController.style.margin.m4
        border.color: ThemeController.style.weakPasswordsDelegate.avatarBdColor
        radius: s(100)

        UIKit.SvgImage {
          id: avatarIcon
          width: ThemeController.style.margin.m24
          height: ThemeController.style.margin.m24
          source: "qrc:/rotating-light.svg"
          anchors.centerIn: parent
        }
      }

      ColumnLayout {
        Layout.fillWidth: true
        spacing: ThemeController.style.margin.m4

        UIKit.BaseText {
          id: titleItem
          text: title
          font.weight: Font.DemiBold
          Layout.alignment: Qt.AlignVCenter
          Layout.fillWidth: true
          color: ThemeController.style.weakPasswordsDelegate.fontColor
        }

        UIKit.BaseText {
          id: contentText
          text: delegate.text
          Layout.alignment: Qt.AlignVCenter
          Layout.fillWidth: true
          wrapMode: Text.WrapAtWordBoundaryOrAnywhere
          color: ThemeController.style.weakPasswordsDelegate.fontColor
        }
      }

      UIKit.LargeIconButton {
        id: btn
        Layout.alignment: Qt.AlignVCenter
        icon.source: "qrc:/forward-white.svg"
        Layout.preferredWidth: ThemeController.style.margin.m24
        Layout.preferredHeight: ThemeController.style.margin.m24
        buttonStyle: ThemeController.style.devicesPendingDelegate.actionButton
        overlay.visible: false
        onClicked: {
        }
      }
    }
  }

  states: [
    State {
      name: "default"
      when: !delegate.hovered && !delegate.pressed
      PropertyChanges {
        target: linearGradient
        visible: true
      }
    },

    State {
      name: "hovered"
      when: delegate.hovered && !delegate.pressed
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.weakPasswordsDelegate.bgColorHovered
      }
      PropertyChanges {
        target: linearGradient
        visible: false
      }
    },

    State {
      name: "pressed"
      when: delegate.pressed
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.weakPasswordsDelegate.bgColorPressed
      }
      PropertyChanges {
        target: linearGradient
        visible: false
      }
    }
  ]
}
