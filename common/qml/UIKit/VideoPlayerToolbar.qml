import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit

Rectangle {
  id: root
  property bool playing: false
  property alias slider: slider
  property alias startTxt: startTxt
  property alias endTxt: endTxt
  property alias pauseButton: pauseButton

  property var componentStyle: ThemeController.style.videoPlayerDark

  signal play()
  signal back()
  signal forward()

  height: s(83)
  color: componentStyle.toolBarColor
  radius: ThemeController.style.margin.m16
  width: s(328)
  border.width: s(1)
  border.color: componentStyle.toolbarBorderColor

  MediaSlider {
    id: slider
    componentStyle: root.componentStyle.slider
    anchors {
      left: parent.left
      right: parent.right
      top: parent.top
      topMargin: ThemeController.style.margin.m16
      leftMargin: ThemeController.style.margin.m16
      rightMargin: ThemeController.style.margin.m16
    }
    playing: root.playing
  }

  UIKit.BaseText {
    id: startTxt
    anchors {
      left: slider.left
      top: slider.bottom
    }

    text: "1:11"
    size: UIKit.BaseText.TextSize.Small
    color: componentStyle.textColor
  }

  UIKit.BaseText {
    id: endTxt
    anchors {
      right: slider.right
      top: slider.bottom
    }

    text: "5:28"
    size: UIKit.BaseText.TextSize.Small
    color: componentStyle.textColor
  }

  Row {
    id: buttons
    anchors {
      horizontalCenter: parent.horizontalCenter
      top: endTxt.bottom
    }
    spacing: ThemeController.style.margin.m16

    UIKit.LargeIconButton {
      height: s(24)
      width: s(24)
      buttonStyle: componentStyle.buttonStyle
      icon.source: "qrc:/gobackward.15.svg"
      anchors.verticalCenter: parent.verticalCenter
      onClicked: back()
    }

    UIKit.LargeIconButton {
      height: s(24)
      width: s(24)
      id: pauseButton
      buttonStyle: componentStyle.buttonStyle
      icon.source: playing ? "qrc:/pause.svg" : "qrc:/play.svg"
      anchors.verticalCenter: parent.verticalCenter
      onClicked: play()
    }

    UIKit.LargeIconButton {
      height: s(24)
      width: s(24)
      buttonStyle: componentStyle.buttonStyle
      icon.source: "qrc:/goforward.15.svg"
      anchors.verticalCenter: parent.verticalCenter
      onClicked: forward()
    }
  }
}

