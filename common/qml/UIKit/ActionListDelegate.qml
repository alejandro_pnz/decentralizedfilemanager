import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12

AbstractButton {
  id: control

  property bool isMobile: Qt.platform.os === "android" || Qt.platform.os === "ios"

  implicitHeight: isMobile ? s(56) : s(48)
  implicitWidth: s(343)
  display: AbstractButton.TextOnly

  property bool separator: false
  property var config: ThemeController.style.actionListDelegate
  property string contentTxt: ""
  property bool checkMarkIcon: false

  property bool actionIcon: false
  readonly property alias bgRect: bgRect
  readonly property alias titleComponent: titleComponent
  property alias iconComponent: icon
  readonly property alias checkIndicator: checkIndicator
  readonly property alias actionIconComponent: actionIconComponent
  readonly property alias separatorComponent: separatorComponent
  default property alias children: extraItem.children

  property bool pointingHandCursor: false

  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
  topPadding: isMobile ? ThemeController.style.margin.m16 : ThemeController.style.margin.m12
  bottomPadding: isMobile ? ThemeController.style.margin.m16 : ThemeController.style.margin.m12

  background: Rectangle {
    id : bgRect
    color: control.pressed ? config.bgColorPressed : control.hovered
                             ? config.bgColorHover : config.bgColor

    Rectangle {
      id: separatorComponent
      anchors {
        bottom: parent.bottom
        right: parent.right
        left: parent.left
        leftMargin: leftPadding + (icon.visible ? icon.width + mainLayout.spacing : 0)
        rightMargin: rightPadding
      }
      height: s(1)
      color: config.separatorColor
      visible: control.separator
    }
  }


  contentItem: RowLayout {
    id: mainLayout
    anchors.verticalCenter: parent.verticalCenter
    spacing: ThemeController.style.margin.m20

    ColoredImage {
      id: icon
      Layout.alignment: Qt.AlignVCenter
      width: s(24)
      height: s(24)
      source: control.icon.source
      visible: control.display !== AbstractButton.TextOnly
      color: control.hovered ? config.iconColorHover : config.iconColor
    }

    ColumnLayout {
      Layout.alignment: Qt.AlignVCenter
      Layout.fillHeight: true

      BaseText {
        id: titleComponent
        elide: Text.ElideRight
        text: control.text
        color: control.hovered ? config.textColorHover : config.textColor
        Layout.alignment: Qt.AlignVCenter
        verticalAlignment: Text.AlignVCenter
      }

      BaseText {
        id: subTitle
        text: contentTxt
        Layout.alignment: Qt.AlignVCenter
        visible: text !== ""
        size: BaseText.TextSize.Small
        color: ThemeController.style.shuttleColor
      }
    }

    Item {
      id: extraItem
      Layout.fillHeight: true
      Layout.fillWidth: true
      Layout.leftMargin: -ThemeController.style.margin.m8
    }

    Rectangle {
      id: checkIndicator
      width: s(24)
      height: s(24)
      color: config.checkBgColor
      radius: s(12)
      visible: control.checked && !checkMarkIcon
      SvgImage{
        height: s(12)
        width: s(14)
        anchors.centerIn: parent
        source: "qrc:/check-icon.svg"
      }
    }

    SvgImage{
      id: checkMark
      height: 16
      width: 16
      source: "qrc:/check-mark.svg"
      visible: control.checked && checkMarkIcon
    }

    ColoredImage {
      id: actionIconComponent
      Layout.alignment: Qt.AlignVCenter
      width: s(24)
      height: s(24)
      source: "qrc:/chevron-forward.svg"
      visible: control.actionIcon
      color: control.hovered ? config.iconColorHover : config.iconColor
    }
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: pointingHandCursor ? Qt.PointingHandCursor : Qt.ArrowCursor
  }
}

