import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12

ToolBar {
    id: toolbar
    property alias leftButton: leftButton
    property alias rightButton: rightButton
    property var leftButtonHandler: function() { pageManager.back() }
    property var rightButtonHandler: function() { pageManager.back() }
    height: ThemeController.style.toolbarHeight
    background: Item{}
    RowLayout {
      id: row
        anchors {
          leftMargin: ThemeController.style.pageHorizontalMargin
          rightMargin: ThemeController.style.pageHorizontalMargin
          top: parent.top
          right: parent.right
          left: parent.left
          topMargin: ThemeController.style.pageTopMargin
        }
        UIKit.StyledButton {
            id: leftButton
            type: ThemeController.style.backButtonDisplayMode
            displayMode: UIKit.StyledButton.DisplayMode.IconOnly
            icon.source: "qrc:/chevron-back.svg"
            height: ThemeController.style.toolbarHeight
            width: height
            onClicked: leftButtonHandler()
        }

        Item{
          Layout.fillWidth: true
          Layout.fillHeight: true
        }

        UIKit.StyledButton {
            id: rightButton
            type: ThemeController.style.backButtonDisplayMode
            displayMode: UIKit.StyledButton.DisplayMode.IconOnly
            icon.source: "qrc:/close.svg"
            height: ThemeController.style.toolbarHeight
            width: height
            onClicked: rightButtonHandler()
        }
    }
}

