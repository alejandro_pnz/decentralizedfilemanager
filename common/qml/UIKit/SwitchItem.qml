import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import QtQuick.Layouts 1.12

Item {
  id: root
  implicitWidth: parent.width
  implicitHeight: contentColumn.height

  property alias textLabel: textLabel
  property alias contentLabel: contentLabel
  property alias switchButton: switchButton
  property string text: ""
  property string contentText: ""
  property bool showSeparator: false
  property int separatorMargin: ThemeController.style.margin.m8

  ColumnLayout {
    id: contentColumn
    spacing: ThemeController.style.margin.m8
    width: parent.width
    RowLayout {
      width: parent.width
      height: switchButton.height

      UIKit.BaseText {
        id: textLabel
        text: root.text
        font.weight: Font.DemiBold
        Layout.alignment: Qt.AlignVCenter
      }

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
      }

      UIKit.SwitchButton {
        id: switchButton
      }
    }

    UIKit.BaseText {
      id: contentLabel
      text: contentText
      size: UIKit.BaseText.TextSize.Small
      Layout.fillWidth: true
      Layout.rightMargin: switchButton.width + ThemeController.style.margin.m12
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      color: ThemeController.style.shuttleColor
      visible: text !== ""
    }

    Rectangle {
      Layout.topMargin: separatorMargin
      Layout.fillWidth: true
      color: ThemeController.style.mercuryColor
      height: s(1)
      visible: showSeparator
    }
  }
}
