import QtQuick 2.0
import AppStyles 1.0

Rectangle {
  id: avatar

  enum Size { Big, Small, Custom}
  enum Mode { NoMode, NoSync, Blocked, Public, Private }

  property int size: Avatar.Size.Big
  property bool borderless: false
  property bool topIcon: false
  property bool bottomIcon: false
  property bool alternativeColor: false
  property bool editIcon: true
  property int userMode: Avatar.Mode.NoMode
  property int diameter: s(40)
  property int labelFontSize: BaseText.TextSize.H3

  property string bottomIconSource: {
    if(editIcon){
      return "qrc:/edit-circle.svg"
    }
    else {
      switch (avatar.userMode) {
      case Avatar.Mode.NoMode: return ""
      case Avatar.Mode.NoSync: return "qrc:/no-sync.svg"
      case Avatar.Mode.Public: return "qrc:/public.svg"
      case Avatar.Mode.Private: return "qrc:/private.svg"
      case Avatar.Mode.Blocked: return "qrc:/blocked-circle.svg"
      }
    }
  }

  property string text
  property int len: 1
  property var colors: [ ThemeController.style.slatePurpleColor,
    ThemeController.style.amberColor,
    ThemeController.style.shamrockColor,
    ThemeController.style.malibuColor,
    ThemeController.style.charadeColor ]
  property var alternativeColors: [ ThemeController.style.portageColor,
    ThemeController.style.dandelionColor,
    ThemeController.style.mintColor,
    ThemeController.style.mabelColor,
    ThemeController.style.altoColor ]


  signal bottomIconClicked()
  signal topIconClicked()


  readonly property alias label: label
  readonly property alias topIconComponent: topIconComponent
  readonly property alias bottomIconComponent: bottomIconComponent


  radius: height / 2
  color: getColor(label.text)
  border {
    color: ThemeController.style.avatar.borderColor
    width: borderless ? 0 : s(2)
  }

  // Top Icon
  Rectangle {
    id: topIconComponent
    visible: topIcon
    width: s(24)
    height: s(24)
    radius: width / 2
    color: ThemeController.style.avatar.borderColor
    anchors.top: parent.top
    anchors.topMargin: -s(4)
    anchors.right: parent.right
    anchors.rightMargin: -s(4)

    SvgImage {
      source: "qrc:/close-icon.svg"
      anchors.centerIn: parent
      width: s(20)
      height: s(20)
    }

    MouseArea {
      anchors.fill: parent
      onClicked: topIconClicked()
    }
  }

  // Bottom icon
  // Note we can't use FieldIcon because it has different states defined
  // on mouse hover, press etc. What's more it colors the icon by default
  // which is a no go here due to icon structure.
  SvgImage {
    id: bottomIconComponent
    visible: bottomIcon
    anchors.bottom: parent.bottom
    anchors.right: parent.right
    width: editIcon ? s(16) : s(24)
    height: editIcon ? s(16) : s(24)
    source: bottomIconSource
    MouseArea {
      anchors.fill: parent
      onClicked: {
        if(editIcon){
          bottomIconClicked()
        }
      }
    }
  }

  BaseText {
    id: label
    anchors.centerIn: parent
    text: getText(avatar.text)
    size: BaseText.TextSize.H3
    color: ThemeController.style.avatar.textColor
    font.weight: Font.Bold
    font.capitalization: Font.AllUppercase
  }

  function getText(text) {
    if (text.length === 0) {
      return " ";
    }

    return text.substr(0, len);
  }

  function getColor(text) {
    var hash = 0;
    for (var i = 0; i < text.length; i++) {
      hash = text.charCodeAt(i) + ((hash << 5) - hash);
    }
    var colors = avatar.alternativeColor ? avatar.alternativeColors : avatar.colors
    return colors[hash % (colors.length - 1)];
  }

  states: [
    State {
      name: "big"
      when: avatar.size === Avatar.Size.Big
      PropertyChanges {
        target: avatar
        implicitWidth: s(60)
        implicitHeight: s(60)
      }
      PropertyChanges {
        target: label
        size: BaseText.TextSize.H3
      }
    },
    State {
      name: "small"
      when: avatar.size === Avatar.Size.Small
      PropertyChanges {
        target: avatar
        implicitWidth: s(40)
        implicitHeight: s(40)
      }
      PropertyChanges {
        target: label
        size: BaseText.TextSize.Body
      }
    },
      State {
        name: "custom"
        when: avatar.size === Avatar.Size.Custom
        PropertyChanges {
          target: avatar
          implicitWidth: diameter
          implicitHeight: diameter
        }
        PropertyChanges {
          target: label
          size: labelFontSize
        }
      }
  ]
}
