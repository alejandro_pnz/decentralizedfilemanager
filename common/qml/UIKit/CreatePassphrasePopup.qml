import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

UIKit.PopupScreen {
  id: popup
  visible: false
  width: s(384)
  height: s(308)
  showCloseButton: false
  showBackButton: false
  parent: Overlay.overlay
  signal enterCreatePassphrase()

  Item {
    anchors{
      centerIn: parent
    }
    height: popup.height
    width: popup.width

    CreatePassphrase {
      anchors {
        fill: parent
        margins: ThemeController.style.margin.m24
      }
      onClosePopup: popup.close()
      onCreatePassphrase: {
        popup.close()
        enterCreatePassphrase()
      }
    }
  }
}
