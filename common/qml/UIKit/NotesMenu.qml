import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import com.test 1.0
import QtQuick.Layouts 1.12
import SortFilterProxyModel 0.2
import QtQml.Models 2.12
import DocumentEditor 1.0

Item {
  id: menu
  implicitWidth: parent.width
  implicitHeight: s(64)

  property DocumentEditor documentEditor: null

  property int itemWidth: s(44)
  property int fullMenuWidth: s(952)
  property int horizontalMargin: ThemeController.style.margin.m16

  signal addLinkRequest()

  MouseArea {
    anchors.fill: parent
    drag.target: parent
    drag.axis: Drag.XAndYAxis
    drag.minimumX: -horizontalMargins + s(24)
    drag.maximumX: root.width - parent.width + horizontalMargins - s(24)
    drag.minimumY: s(24)
    drag.maximumY: root.height - parent.height - s(24)
    drag.filterChildren: true
    propagateComposedEvents: true
  }

  onWidthChanged: {
    updateModel()
  }

  RowLayout {
    id: menuMainRow
    anchors.fill: parent
    spacing: 0
    Item {
      id: menuItem
      Layout.fillHeight: true
      Layout.maximumWidth: s(952)
      Layout.minimumWidth: s(552)
      Layout.fillWidth: true

      Rectangle {
        id: contentRect
        color: ThemeController.style.noteMenu.bgColor
        border.width: s(1)
        border.color: ThemeController.style.noteMenu.bdColor
        radius: s(4)
        anchors.fill: parent

        RowLayout {
          height: s(32)
          spacing: 0
          anchors {
            left: parent.left
            right: parent.right
            leftMargin: horizontalMargin
            rightMargin: horizontalMargin
            verticalCenter: parent.verticalCenter
          }

          ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true

            spacing: ThemeController.style.margin.m12
            orientation: ListView.Horizontal
            interactive: false

            model: menuProxy
            clip: true
            delegate: UIKit.LargeIconButton {
              id: delegate
              icon.source: iconSource
              iconWidth: s(32)
              iconHeight: s(32)
              checkable: true
              overlay.color: checked ? ThemeController.style.slatePurpleColor :
                                       ThemeController.style.transparent

              action: model.action
              focusPolicy: Qt.NoFocus
              MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.NoButton
                cursorShape: Qt.PointingHandCursor
              }
            }

            section.property: "sectionId"
            section.criteria: ViewSection.FirstCharacter
            section.delegate: Item {
              width: section != 0 ? s(12) : 0
              height: s(32)

              Rectangle {
                width: s(1)
                height: parent.height
                color: ThemeController.style.seashellColor
                visible: section != 0
                anchors.left: parent.left
              }
            }
          }

          Item {
            id: moreButton
            visible: contentRect.width < fullMenuWidth
            Layout.preferredHeight: s(32)
            Layout.preferredWidth: row.width
            RowLayout {
              id: row
              spacing: ThemeController.style.margin.m12

              Rectangle {
                Layout.preferredWidth: s(1)
                Layout.preferredHeight: parent.height
                color: ThemeController.style.seashellColor
              }

              UIKit.LargeIconButton {
                icon.source: "qrc:/more-2.svg"
                onClicked: {
                  moreMenu.open()
                }

                NotesActionPopup {
                  id: moreMenu
                  model: moreMenuProxy
                  x: -moreMenu.width + parent.width + horizontalMargin
                  y: -moreMenu.height - s(20)
                }
              }
            }
          }
        }
      }

      DropShadow {
        id: dropShadow
        anchors.fill: contentRect
        horizontalOffset: 0
        verticalOffset: 0
        radius: s(30)
        //samples: s(61)
        color: Qt.rgba(0, 0, 0, 0.05)
        source: contentRect
      }
    }
  }

  // Model stuff
  ListModel {
    id: actionModel
  }

  SortFilterProxyModel {
    id: menuProxy
    sourceModel: actionModel
    filters: [
      ValueFilter {
        enabled: true
        roleName: "expanded"
        value: true
      },
      ValueFilter {
        enabled: true
        roleName: "visible"
        value: true
      }
    ]
  }

  SortFilterProxyModel {
    id: moreMenuProxy
    sourceModel: actionModel
    filters: [
      ValueFilter {
        enabled: true
        roleName: "visible"
        value: true
      },
      ValueFilter {
        enabled: true
        roleName: "expanded"
        value: false
      }
    ]
  }

  function applyHeading(heading) {
    if (documentEditor.heading === heading)
      documentEditor.heading = DocumentEditor.None
    else
      documentEditor.heading = heading
  }

  Action {
    id: bulletListAction
    checked: documentEditor.bulletList
    onTriggered: documentEditor.bulletList = !documentEditor.bulletList
  }
  Action {
    id: numberedListAction
    checked: documentEditor.numberedList
    onTriggered: documentEditor.numberedList = !documentEditor.numberedList
  }
  Action {
    id: linkAction
    checked: documentEditor.anchor
    onTriggered: addLinkRequest()
  }
  Action {
    id: codeBlockAction
    checked: documentEditor.codeBlock
    onTriggered: documentEditor.codeBlock = !documentEditor.codeBlock
  }
  Action {
    id: codeInlineAction
  }
  Action {
    id: dividerAction
    checked: documentEditor.horizontalLine
    onTriggered: documentEditor.horizontalLine = !documentEditor.horizontalLine
  }
  Action {
    id: quoteAction
  }
  Action {
    id: boldAction
    onTriggered: documentEditor.bold = !documentEditor.bold
    checked: documentEditor.bold
  }
  Action {
    id: italicAction
    onTriggered: documentEditor.italic = !documentEditor.italic
    checked: documentEditor.italic
  }
  Action {
    id: h1Action
    onTriggered: applyHeading(DocumentEditor.H1)
    checked: documentEditor.heading === DocumentEditor.H1

  }
  Action {
    id: h2Action
    onTriggered: applyHeading(DocumentEditor.H2)
    checked: documentEditor.heading === DocumentEditor.H2
  }
  Action {
    id: h3Action
    onTriggered: applyHeading(DocumentEditor.H3)
    checked: documentEditor.heading === DocumentEditor.H3
  }
  Action {
    id: h4Action
    onTriggered: applyHeading(DocumentEditor.H4)
    checked: documentEditor.heading === DocumentEditor.H4
  }
  Action {
    id: h5Action
    onTriggered: applyHeading(DocumentEditor.H5)
    checked: documentEditor.heading === DocumentEditor.H5
  }
  Action {
    id: h6Action
    onTriggered: applyHeading(DocumentEditor.H6)
    checked: documentEditor.heading === DocumentEditor.H6
  }
  Action {
    id: strikeThroughAction
    onTriggered: documentEditor.strike = !documentEditor.strike
    checked: documentEditor.strike
  }
  Action {
    id: underlineAction
    onTriggered: documentEditor.underline = !documentEditor.underline
    checked: documentEditor.underline
  }
  Action {
    id: alignLeftAction
    onTriggered: documentEditor.alignment = Qt.AlignLeft
    checked: documentEditor.alignment === Qt.AlignLeft
  }
  Action {
    id: alignCenterAction
    onTriggered: documentEditor.alignment = Qt.AlignHCenter
    checked: documentEditor.alignment === Qt.AlignHCenter
  }
  Action {
    id: alignRightAction
    onTriggered: documentEditor.alignment = Qt.AlignRight
    checked: documentEditor.alignment === Qt.AlignRight
  }

  Component.onCompleted: {
    actionModel.append({ iconSource: "qrc:/bullet-list-icon.svg",
                         expanded: true,
                         action: bulletListAction,
                         sectionId: "0",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/number-list.svg",
                         expanded: true,
                         action: numberedListAction,
                         sectionId: "0",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/link.svg",
                         expanded: true,
                         action: linkAction,
                         sectionId: "1",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/code-off.svg",
                         expanded: true,
                         action: codeBlockAction,
                         sectionId: "1",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/code-on.svg",
                         expanded: true,
                         action: codeInlineAction,
                         sectionId: "1",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/divider.svg",
                         expanded: true,
                         action: dividerAction,
                         sectionId: "1",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/quote.svg",
                         expanded: true,
                         action: quoteAction,
                         sectionId: "1",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/b-icon.svg",
                         expanded: true,
                         action: boldAction,
                         sectionId: "2",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/italic.svg",
                         expanded: true,
                         action: italicAction,
                         sectionId: "2",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/h1-icon.svg",
                         expanded: true,
                         action: h1Action,
                         sectionId: "2",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/h2-icon.svg",
                         expanded: true,
                         action: h2Action,
                         sectionId: "2",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/h3-icon.svg",
                         expanded: true,
                         action: h3Action,
                         sectionId: "2",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/h4-icon.svg",
                         expanded: true,
                         action: h3Action,
                         sectionId: "2",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/h5-icon.svg",
                         expanded: true,
                         action: h5Action,
                         sectionId: "2",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/h6-icon.svg",
                         expanded: true,
                         action: h6Action,
                         sectionId: "2",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/strikethrough.svg",
                         expanded: true,
                         action: strikeThroughAction,
                         sectionId: "3",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/underline.svg",
                         expanded: true,
                         action: underlineAction,
                         sectionId: "3",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/align-left.svg",
                         expanded: true,
                         action: alignLeftAction,
                         sectionId: "4",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/align-center.svg",
                         expanded: true,
                         action: alignCenterAction,
                         sectionId: "4",
                         visible: true
                       })
    actionModel.append({ iconSource: "qrc:/align-right.svg",
                         expanded: true,
                         action: alignRightAction,
                         sectionId: "4",
                         visible: true
                       })
    updateModel()
  }

  function updateModel() {

    //Calculate common menu width
    //Substract horizontal margins, 2 listView sections that are always visible,
    //moreButton width if it is visible
    var commonAvailableWidth = menuItem.width - horizontalMargin * 2
        - s(24) + listView.spacing
        - (menuItem.width < fullMenuWidth ? moreButton.width : 0)

    //approximate number of visible buttons
    var items = Math.floor(commonAvailableWidth / itemWidth)

    var maxAvailableSpace = 0

    //Count detailed width - substract visible sections width
    if(items < 16){
      maxAvailableSpace = commonAvailableWidth
    }
    else if(items < 18){
      maxAvailableSpace = commonAvailableWidth - s(12)
    }
    else {
      maxAvailableSpace = commonAvailableWidth - s(24)
    }

    var maxVisDel = Math.floor(maxAvailableSpace / itemWidth)
    for (var i = 0; i < actionModel.count; ++i) {
      actionModel.setProperty(i, "expanded", i < maxVisDel)
    }
    if(moreMenuProxy.count === 0){
      moreMenu.visible = false
    }
  }
}
