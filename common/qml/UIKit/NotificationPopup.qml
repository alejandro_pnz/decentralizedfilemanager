import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects


import com.testintl 1.0

Popup {
  id: popup
  // If popup should be scrollable use this property to set maximum height
  // Also explicitly set popup.height to the same value
  property int maxHeight: s(700)
  implicitHeight: content.height + 2 * padding
  implicitWidth: s(412)
  padding: ThemeController.style.margin.m16
  background: Item {
    Rectangle {
      id: bgComponent
      anchors.fill: parent
      radius: s(8)
    }
    DropShadow {
      id: dropShadow
      anchors.fill: bgComponent
      transparentBorder: true
      radius: s(32)
      spread: 0.2
      color: Qt.rgba(0, 0, 0, 0.08)
      source: bgComponent
    }
  }

  InAppMessagesModel {
    id: inappMessagesModel
  }

  ColumnLayout {
    id: content
    spacing: ThemeController.style.margin.m16
    height: Math.min(maxHeight - 2 * padding,
                     listView.contentHeight + content.spacing + topRow.height)
    width: popup.implicitWidth - 2 * padding
    RowLayout {
      id: topRow
      Layout.fillWidth: true
      Layout.minimumHeight: s(24)
      UIKit.BaseText {
        Layout.fillWidth: true
        text: Strings.notifications
        font.weight: Font.Bold
      }
      UIKit.StyledButton {
        id: markButton
        type: UIKit.StyledButton.ButtonStyle.Text
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        text: Strings.markAsRead
        visible: InAppMessagesManager.unreadMessagesCount > 0
        onClicked: {
          popup.close()
          InAppMessagesManager.markAllAsRead()
        }
      }
    }
    UIKit.NotificationView {
      id: listView
      Layout.fillHeight: true
      Layout.fillWidth: true
      model: inappMessagesModel
    }
  }
}
