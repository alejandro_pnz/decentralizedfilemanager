import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0

Item {
  implicitWidth: parent.width
  implicitHeight: childrenRect.height

  property string labelText: ""
  property string content: ""

  Column {
    spacing: ThemeController.style.margin.m12
    UIKit.BaseText {
      text: labelText
      color: ThemeController.style.shuttleColor
    }

    UIKit.BaseText {
      text: content
      font.weight: Font.DemiBold
    }
  }
}
