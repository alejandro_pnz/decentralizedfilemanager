import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0

Rectangle {
  id: root
  implicitWidth: parent.width
  implicitHeight: s(60)
  color: ThemeController.style.strengthIndicator.bgColor
  radius: s(4)
  property string veryWeakPassword: Strings.passwordStrengthVeryLow
  property string weakPassword: Strings.passwordStrengthLow
  property string strongPassword: Strings.passwordStrengthMedium
  property string veryStrongPassword: Strings.passwordStrengthHigh
  property alias displayedText: txt
  property alias rectsRepeater: repeater
  property bool tipIconVisible: false

  Column {
    anchors {
      left: parent.left
      right: parent.right
      leftMargin: ThemeController.style.margin.m12
      rightMargin: ThemeController.style.margin.m12
      verticalCenter: parent.verticalCenter
    }
    spacing: ThemeController.style.margin.m8

    Row {
      width: parent.width
      spacing: ThemeController.style.margin.m8

      BaseText {
        id: txt
//        text: contentText
      }

      SvgImage {
        id: tipIcon
        source: "qrc:/tips-icon.svg"
        width: s(24)
        height: s(24)
        visible: tipIconVisible
      }
    }

    RowLayout {
      spacing: ThemeController.style.margin.m4
      width: parent.width
      Repeater {
        id: repeater
        model: 4
        Rectangle {
          Layout.fillWidth: true
          Layout.preferredHeight: s(4)
          radius: s(1)
          color: ThemeController.style.strengthIndicator.strengthDefaultColor
        }
      }
    }
  }

  states: [
    State {
      name: "verylow"
      PropertyChanges { target: repeater.itemAt(0); color: ThemeController.style.strengthIndicator.strengthLowColor}
      PropertyChanges { target: repeater.itemAt(1); color: ThemeController.style.strengthIndicator.strengthDefaultColor}
      PropertyChanges { target: repeater.itemAt(2); color: ThemeController.style.strengthIndicator.strengthDefaultColor}
      PropertyChanges { target: repeater.itemAt(3); color: ThemeController.style.strengthIndicator.strengthDefaultColor}
      PropertyChanges { target: txt; text: root.veryWeakPassword }
    },
    State {
      name: "low"
      PropertyChanges { target: repeater.itemAt(0); color: ThemeController.style.strengthIndicator.strengthLowColor}
      PropertyChanges { target: repeater.itemAt(1); color: ThemeController.style.strengthIndicator.strengthLowColor}
      PropertyChanges { target: repeater.itemAt(2); color: ThemeController.style.strengthIndicator.strengthDefaultColor}
      PropertyChanges { target: repeater.itemAt(3); color: ThemeController.style.strengthIndicator.strengthDefaultColor}
      PropertyChanges { target: txt; text: root.weakPassword }
    },

    State {
      name: "medium"
      PropertyChanges { target: repeater.itemAt(0); color: ThemeController.style.strengthIndicator.strengthMediumColor}
      PropertyChanges { target: repeater.itemAt(1); color: ThemeController.style.strengthIndicator.strengthMediumColor}
      PropertyChanges { target: repeater.itemAt(2); color: ThemeController.style.strengthIndicator.strengthMediumColor}
      PropertyChanges { target: repeater.itemAt(3); color: ThemeController.style.strengthIndicator.strengthDefaultColor}
      PropertyChanges { target: txt; text: root.strongPassword }
    },

    State {
      name: "high"
      PropertyChanges { target: repeater.itemAt(0); color: ThemeController.style.strengthIndicator.strengthHighColor}
      PropertyChanges { target: repeater.itemAt(1); color: ThemeController.style.strengthIndicator.strengthHighColor}
      PropertyChanges { target: repeater.itemAt(2); color: ThemeController.style.strengthIndicator.strengthHighColor}
      PropertyChanges { target: repeater.itemAt(3); color: ThemeController.style.strengthIndicator.strengthHighColor}
      PropertyChanges { target: txt; text: root.veryStrongPassword }
    }
  ]
}
