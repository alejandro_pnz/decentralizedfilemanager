import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import com.test 1.0

AbstractButton {
  id: control

  property string contentText: ""
  property string date: ""
  property bool notificationRead: false
  property string userEmail: ""

  property alias leftButton: leftButton
  property alias rightButton: rightButton
  property alias userAvatar: objectAvatar
  property alias logoImage: logoImage
  property alias textComponent: textComponent
  property alias dateComponent: dateComponent
  property alias bgRect: bgRect

  state: 'unread'
  padding: ThemeController.style.margin.m16

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      radius: s(8)
    }
  }

  contentItem: RowLayout {
    id: mainLayout
    width: parent.width
    height: contentColumn.height
    spacing: ThemeController.style.margin.m12

    Avatar {
      id: objectAvatar
      text: userEmail
      Layout.alignment: Qt.AlignTop
      borderless: true
      visible: userEmail !== ""
    }

    Image {
      id: logoImage
      asynchronous: true
      Layout.preferredHeight: s(60)
      Layout.preferredWidth: s(60)
      sourceSize: Qt.size(s(60), s(60))
      fillMode: Image.PreserveAspectFit
      visible: !objectAvatar.visible
      source: "qrc:/notification-logo.svg"
      Layout.alignment: Qt.AlignTop
    }

    ColumnLayout {
      id: contentColumn
      spacing: ThemeController.style.margin.m8
      Layout.fillWidth: true

      BaseText {
        id: textComponent
        Layout.fillWidth: true
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        text: contentText
      }

      BaseText {
        id: dateComponent
        text: Strings.ago.arg(date)
        Layout.fillWidth: true
        size: BaseText.TextSize.Small
        color: ThemeController.style.notificationCard.dateFontColor
      }

      RowLayout {
        Layout.topMargin: ThemeController.style.margin.m16
        Layout.fillWidth: true
        Layout.preferredHeight: leftButton.height
        spacing: ThemeController.style.margin.m12
        visible: leftButton.visible || rightButton.visible

        StyledButton {
          id: leftButton
          type: StyledButton.ButtonStyle.Secondary
          displayMode: StyledButton.DisplayMode.TextOnly
          text: Strings.decline
          onClicked: notificationRead = true
        }

        StyledButton {
          id: rightButton
          displayMode: StyledButton.DisplayMode.TextOnly
          text: Strings.accept
          onClicked: notificationRead = true
        }

        Item {
          Layout.fillWidth: true
          Layout.preferredHeight: s(1)
        }
      }
    }
  }

  onClicked: {
    notificationRead = true
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: Qt.PointingHandCursor
  }

  states: [
    State {
      name: "hovered"
      when: control.hovered
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.notificationCard.hoveredBgColor
      }
    },
    State {
      name: "unread"
      when: !notificationRead
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.notificationCard.unreadBgColor
      }
    },
    State {
      name: "read"
      when: notificationRead
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.notificationCard.bgColor
      }
    }
  ]
}
