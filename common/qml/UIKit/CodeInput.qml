import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import Qt5Compat.GraphicalEffects


TextField {
  id: control
  implicitWidth: s(48)
  implicitHeight: ThemeController.style.defaultInputHeight

  property alias bgRect: bgRect
  property bool cursorVisible: false

  background: Item {
    Rectangle {
      id: bgRect
      radius: s(4)
      width: parent.width
      height: parent.height
      color: ThemeController.style.codeInput.bgColor
      border.color: control.activeFocus ? ThemeController.style.codeInput.bdColorActive
                                        : ThemeController.style.codeInput.bdColor
      border.width: control.activeFocus ? s(1.5) : s(1)
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      horizontalOffset: 0
      verticalOffset: s(2)
      radius: s(10)
      //samples: s(21)
      source: bgRect
      color: Qt.rgba(0, 0, 0, 0.08)
      visible: control.activeFocus
    }
  }

  cursorDelegate: Rectangle {
    width: s(2)
    height: s(24)
    color: ThemeController.style.codeInput.cursorColor
    radius: s(100)
    visible: cursorVisible
  }

  color: ThemeController.style.textInput.textColor
  font.pixelSize: s(18)
  maximumLength: 1
  inputMethodHints: Qt.ImhDigitsOnly
  horizontalAlignment: Qt.AlignHCenter
  leftPadding: 0
  rightPadding: 0
  validator: IntValidator {bottom: 0; top: 9}

  onActiveFocusChanged: {
    if (activeFocus) {
      cursorVisible = true
    }
    else {
      cursorVisible = false
    }
  }
}
