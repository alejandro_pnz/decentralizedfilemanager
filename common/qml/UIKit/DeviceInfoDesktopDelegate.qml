import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12
import com.test 1.0
import QtQml.Models 2.12

AbstractButton {
  id: control

  implicitHeight: s(smallMode && !checked ? 160 : 104)
  implicitWidth: parent.width

  property string email: ""
  property string contentText: ""
  property int index: -1

  signal blockClicked()
  signal rejectClicked()
  signal approveClicked()

  property alias bgRect: bgRect
  property alias avatar: iconContainer
  property alias title: userEmail
  property alias content: content
  property alias blockButton: buttonsRow.blockButton
  property alias rejectButton: buttonsRow.rejectButton
  property alias approveButton: buttonsRow.approveButton
  property alias buttonsRow: buttonsRow
  property alias checkBox: checkBox

  property bool checkBoxMode: true
  property bool smallMode: false
  checkable: true

  property ItemSelectionModel selectionModel: null
  property var sourceModel: null

  onWidthChanged: {
    if((width  - leftPadding - checkBox.width - iconContainer.width - s(28)
        - content.width - s(24)) > buttonsRow.width){
      smallMode = false
    }
    else {
      smallMode = true
    }
  }

  onClicked: {
    if (checkable && sourceModel){
      selectionModel.select(sourceModel.index(index, 0), ItemSelectionModel.Toggle)
    }
  }

  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
//  topPadding: ThemeController.style.margin.m16
//  bottomPadding: ThemeController.style.margin.m16


  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      color: ThemeController.style.deviceInformationDelegate.bgColor
      radius: s(4)
      border {
        color: control.checked ? ThemeController.style.deviceInformationDelegate.bdCheckedColor
                               : ThemeController.style.deviceInformationDelegate.bdColor
        width: s(control.checked ? 1.5 : 1)
      }
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      source: bgRect
      color: ThemeController.style.deviceInformationDelegate.dropShadowColor
      radius: s(control.checked ? 16 : 10)
      verticalOffset: s(control.checked && checkBoxMode ? 8 : 2)
      //samples: radius * 2 + 1
    }
  }

  contentItem: Item {
    height: s(smallMode && !checked ? 160 : 104)
    width: parent.width
    ColumnLayout {
      anchors.fill: parent
      anchors.topMargin: ThemeController.style.margin.m16
      anchors.bottomMargin: ThemeController.style.margin.m16
      RowLayout {
        spacing: ThemeController.style.margin.m12

        CheckIndicator {
          id: checkBox
          Layout.alignment: Qt.AlignVCenter
          checked: control.checked
          implicitHeight: s(32)
          implicitWidth: s(32)
          checkIcon.width: s(18)
          checkIcon.height: s(15)
        }

        DeviceAvatar {
          id: iconContainer
          Layout.leftMargin: ThemeController.style.margin.m4
          Layout.alignment: Qt.AlignVCenter
          size: ObjectAvatar.Size.Small
        }

        ColumnLayout {
          id: textColumn
          spacing: ThemeController.style.margin.m8
          Layout.fillWidth: true
          Layout.preferredWidth: Math.max(content.width, userEmail.width)

          BaseText {
            id: userEmail
            text: email
            font.weight: Font.DemiBold
          }

          BaseText {
            id: content
            Layout.fillWidth: true
            Layout.maximumWidth: s(338)
            Layout.minimumWidth: s(286)
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            text: contentText
            size: BaseText.TextSize.Small
            color: ThemeController.style.deviceInformationDelegate.contentTextColor
          }
        }

        Item {
          Layout.leftMargin: ThemeController.style.margin.m12
          Layout.fillHeight: true
          Layout.fillWidth: true
          Layout.preferredWidth: s(308)
          visible: !smallMode

          DeviceInfoButtons {
            id: buttonsRow
            width: s(308)
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            Layout.alignment: Qt.AlignVCenter
            visible: selectionModel ? !selectionModel.hasSelection && !smallMode : !smallMode
          }
        }
      }

      DeviceInfoButtons {
        id: bottomButtonsRow
        spacing: ThemeController.style.margin.m16
        Layout.preferredWidth: s(308)
        Layout.alignment: Qt.AlignVCenter
        Layout.leftMargin: s(64)
        visible: selectionModel ? !selectionModel.hasSelection && smallMode : smallMode
      }
    }
  }

  Connections {
    target: selectionModel
    function onSelectionChanged() {
      control.checked = selectionModel.isSelected(sourceModel.index(index, 0))
    }
  }
}
