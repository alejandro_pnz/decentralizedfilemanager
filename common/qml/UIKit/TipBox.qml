import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

ColumnLayout {
  spacing: ThemeController.style.margin.m12

  property string title: ""
  property string content: ""
  property string info: ""
  property string imageSource: ""
  property bool showPlaceholder: true
  property bool spacerVisible: true

  // Image placeholder
  Rectangle {
    Layout.topMargin: ThemeController.style.margin.m4
    Layout.preferredHeight: s(Qt.platform.os === "android" ||
                            Qt.platform.os === "ios" ? 222 : 180)
    Layout.fillWidth: true
    radius: s(10)
    visible: showPlaceholder

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectFit
      source: imageSource
      mipmap: true
    }
  }

  // Tip title
  UIKit.BaseText {
    Layout.fillWidth: true
    Layout.topMargin: ThemeController.style.margin.m4
    horizontalAlignment: Qt.AlignHCenter
    text: title
    font.weight: Font.Bold
    size: BaseText.TextSize.H3
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }

  // Tip content
  UIKit.BaseText {
    Layout.alignment: Qt.AlignHCenter
    Layout.fillWidth: true
    text: content
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    horizontalAlignment: Qt.AlignHCenter
  }

  // Spacer
  Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
    visible: spacerVisible
  }

  // Info box
  Infobox {
    Layout.fillWidth: true
    text: info
    visible: text !== ""
  }
}
