import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import QtQuick.Layouts 1.12
import UIModel 1.0

UIKit.StyledDrawer {
  id: popup

  contentItemHeight: parent.height * 0.33
  drawerFullHeight: parent.height
  headerCloseButtonVisible: fullHeight

  property var sortModel: undefined
  property int currentSorter: listView.currentItem ? listView.currentItem.sorter : Enums.LastUsed
  property string currentSorterName: listView.currentItem ? listView.currentItem.txt : ""

  function init(){
    listView.currentIndex = 0;
  }

  headerVisible: true
  header: Item {
    Layout.fillWidth: true
    Layout.fillHeight: true
    UIKit.BaseText {
      width: parent.width
      anchors.verticalCenter: parent.verticalCenter
      text: Strings.sortBy
      font.weight: Font.Bold
    }
  }

  ListView {
    id: listView
    height: contentHeight
    width: parent.width
    model: sortModel
    delegate: SortListDelegate {
      ButtonGroup.group: buttonsGroup
      txt: name
      checked: listView.currentIndex === index
      onClicked: listView.currentIndex = index
      bg.color: "transparent"
      separator: true
      noVerticalMargins: true
    }
  }

  Item {
    ButtonGroup {
      id: buttonsGroup
      exclusive: true

      onClicked: {
        popup.close()
      }
    }
  }
}
