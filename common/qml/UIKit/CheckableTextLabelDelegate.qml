import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12
import QtQml.Models 2.12

TextLabelDelegate {
  id: control
  property ItemSelectionModel selectionModel: null
  property var model: null
  style: ThemeController.style.checkableTextLabelDelegate
  checkable: true

  Connections {
    target: selectionModel
    enabled: selectionModel !== null
    function onSelectionChanged() {
      control.checked = selectionModel.isSelected(model.index(index, 0))
    }
  }
}
