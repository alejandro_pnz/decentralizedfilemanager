import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12
import com.test 1.0
import QtQml.Models 2.12

AbstractButton {
  id: control

  implicitHeight: displayMobileMode || checkBoxMode ? s(84) : s(64)

  property ItemSelectionModel selectionModel: null
  property var objectModel: null

  property string title: ""
  property string role: ""
  property var config: ThemeController.style.contactCard
  property int btnId: -1

  property bool checkBoxMode: true
  property bool stateVisible: false
  property bool displayMobileMode: app.isPhone
  property bool online: true
  property string lastSeenDate: ""
  property string descriptionText: stateVisible ? online ? Strings.onlineNow :
                                                           Strings.lastSeen.arg(lastSeenDate) : ""
  checkable: true
  property bool actionButton: false
  property string actionButtonIconSource: "qrc:/more-2.svg"

  property alias avatarObject: avatar
  property alias bg: bgRect
  property alias email: emailText
  property alias bottomLabelContent: labelContent
  readonly property alias labelBottom: roleLabelBottom
  readonly property alias labelLeft: roleLabelLeft
  readonly property alias rightCheckbox: rightCheckBox
  readonly property alias leftCheckbox: checkBoxLeft
  readonly property alias iconObject: selectionImage
  readonly property alias stateComponent: statusText
  readonly property alias stateRect: statusComponent
  readonly property alias actionButtonComponent: actionButtonComponent

  property var pressAndHoldHandler: function handlePressAndHold() {
    // Press&hold is supported only on mobiles
    if (app.isMobile && stateVisible) {
      if (control.checkable) {
        control.checkable = false
        control.checked = false
      } else {
        control.checkable = true
      }
    }
  }
  signal actionButtonClicked()

  onClicked: {
    if (checkable && objectModel){
      selectionModel.select(objectModel.mapToSource(objectModel.index(index, 0)), ItemSelectionModel.Toggle)
    }
  }

  onPressAndHold: pressAndHoldHandler()

  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
  topPadding: ThemeController.style.margin.m12
  bottomPadding: ThemeController.style.margin.m12

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      color: config.bgColor
      radius: s(8)
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      source: bgRect
      color: config.dropShadowColor
      radius: control.checked ? s(16) : s(10)
      verticalOffset: control.checked && checkBoxMode ? s(8) : s(2)
      //samples: radius * 2 + 1
    }
  }

  contentItem: RowLayout {
    spacing: ThemeController.style.margin.m12

    CheckIndicator {
      id: checkBoxLeft
      Layout.alignment: Qt.AlignVCenter
      checked: control.checked
      implicitHeight: s(32)
      implicitWidth: s(32)
      checkIcon.width: s(18)
      checkIcon.height: s(15)
    }

    Avatar {
      id: avatar
      text: title
    }

    Column{
      Layout.fillWidth: true
      spacing: ThemeController.style.margin.m4

      Row {
        spacing: ThemeController.style.margin.m12
        BaseText {
          id: emailText
          text: control.title
          color: config.titleTextColor
        }
        Rectangle {
          id: roleLabelLeft
          height: s(24)
          width: role === "" ? 0 : content.width + s(24)
          color: config.labelColor
          radius: s(100)
          BaseText {
            id: content
            anchors.centerIn: parent
            text: role
            color: config.titleTextColor
          }
        }
      }

      RowLayout {
        width: parent.width
        spacing: ThemeController.style.margin.m4

        Rectangle {
          id: roleLabelBottom
          Layout.preferredHeight: role === "" ? 0 : s(24)
          Layout.preferredWidth: role === "" ? 0 : labelContent.width + 24
          color: config.labelColor
          radius: s(100)
          BaseText {
            id: labelContent
            anchors.centerIn: parent
            text: role
            color: config.titleTextColor
          }
        }

        StatusItem {
          id: statusComponent
          isOnline: online
          visible: stateVisible
        }

        BaseText {
          id: statusText
          Layout.fillWidth: true
          elide: Text.ElideRight
          size: BaseText.TextSize.Small
          color: ThemeController.style.shuttleColor
          text: descriptionText
          visible: text !== ""
        }
      }
    }

    Item{
      Layout.fillHeight: true
      Layout.fillWidth: true
    }

    CheckIndicator {
      id: rightCheckBox
      Layout.alignment: Qt.AlignVCenter
      checked: control.checked
      implicitHeight: s(32)
      implicitWidth: s(32)
      checkIcon.width: s(18)
      checkIcon.height: s(15)
    }

    SvgImage {
      id: selectionImage
      width: s(16)
      height: s(16)
      Layout.alignment: Qt.AlignVCenter
      source: "qrc:/check-mark.svg"
    }

    LargeIconButton {
      id: actionButtonComponent
      visible: control.actionButton
      icon.source: control.actionButtonIconSource
      onClicked: actionButtonClicked()
    }
  }

  states: [
    State {
      name: "mobileCheckboxModeDefault"
      when: checkable && !control.checked && checkBoxMode && displayMobileMode
      PropertyChanges {
        target: bgRect
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: leftCheckbox
        visible: false
      }
      PropertyChanges {
        target: rightCheckBox
        visible: true
      }
      PropertyChanges {
        target: roleLabelBottom
        visible: true
      }
      PropertyChanges {
        target: roleLabelLeft
        visible: false
      }
      PropertyChanges {
        target: selectionImage
        visible: false
      }
      PropertyChanges {
        target: avatar
        size: Avatar.Size.Big
      }
    },

    State {
      name: "mobileCheckboxModeSelected"
      when: checkable && control.checked && checkBoxMode && displayMobileMode
      PropertyChanges {
        target: bgRect
        border.width: s(2)
        border.color: config.bdColorSelected
      }
      PropertyChanges {
        target: leftCheckbox
        visible: false
      }
      PropertyChanges {
        target: rightCheckBox
        visible: true
      }
      PropertyChanges {
        target: roleLabelBottom
        visible: true
      }
      PropertyChanges {
        target: roleLabelLeft
        visible: false
      }
      PropertyChanges {
        target: selectionImage
        visible: false
      }
      PropertyChanges {
        target: avatar
        size: Avatar.Size.Big
      }
    },

    State {
      name: "desktopCheckboxModeDefault"
      when: checkable && !control.checked && !stateVisible
            && checkBoxMode && !displayMobileMode
      PropertyChanges {
        target: bgRect
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: dropShadow
        visible: true
      }
      PropertyChanges {
        target: leftCheckbox
        visible: true
      }
      PropertyChanges {
        target: rightCheckBox
        visible: false
      }
      PropertyChanges {
        target: roleLabelBottom
        visible: false
      }
      PropertyChanges {
        target: roleLabelLeft
        visible: true
      }
      PropertyChanges {
        target: selectionImage
        visible: false
      }
      PropertyChanges {
        target: avatar
        size: Avatar.Size.Big
      }
    },

    State {
      name: "desktopCheckboxModeSelected"
      when: checkable && control.checked && checkBoxMode && !stateVisible
            && !displayMobileMode
      PropertyChanges {
        target: bgRect
        border.width: s(2)
        border.color: config.bdColorSelected
      }
      PropertyChanges {
        target: dropShadow
        visible: true
      }
      PropertyChanges {
        target: leftCheckbox
        visible: true
      }
      PropertyChanges {
        target: rightCheckBox
        visible: false
      }
      PropertyChanges {
        target: roleLabelBottom
        visible: false
      }
      PropertyChanges {
        target: roleLabelLeft
        visible: true
      }
      PropertyChanges {
        target: selectionImage
        visible: false
      }
      PropertyChanges {
        target: avatar
        size: Avatar.Size.Big
      }
    },

    State {
      name: "mobileIconModeDefault"
      when: checkable && !control.checked && !checkBoxMode && displayMobileMode
      PropertyChanges {
        target: bgRect
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: leftCheckbox
        visible: false
      }
      PropertyChanges {
        target: rightCheckBox
        visible: false
      }
      PropertyChanges {
        target: roleLabelBottom
        visible: true
      }
      PropertyChanges {
        target: roleLabelLeft
        visible: false
      }
      PropertyChanges {
        target: selectionImage
        visible: false
      }
      PropertyChanges {
        target: avatar
        size: Avatar.Size.Big
      }
    },

    State {
      name: "mobileIconModeSelected"
      when: checkable && control.checked && !checkBoxMode && displayMobileMode
      PropertyChanges {
        target: bgRect
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: leftCheckbox
        visible: false
      }
      PropertyChanges {
        target: rightCheckBox
        visible: false
      }
      PropertyChanges {
        target: roleLabelBottom
        visible: true
      }
      PropertyChanges {
        target: roleLabelLeft
        visible: false
      }
      PropertyChanges {
        target: selectionImage
        visible: true
      }
      PropertyChanges {
        target: avatar
        size: Avatar.Size.Big
      }
    },

    State {
      name: "desktopIconModeDefault"
      when: checkable && !control.checked && !checkBoxMode &&
            !displayMobileMode
      PropertyChanges {
        target: bgRect
        border.width: 0
      }
      PropertyChanges {
        target: dropShadow
        visible: false
      }
      PropertyChanges {
        target: leftCheckbox
        visible: false
      }
      PropertyChanges {
        target: rightCheckBox
        visible: false
      }
      PropertyChanges {
        target: roleLabelBottom
        visible: false
      }
      PropertyChanges {
        target: roleLabelLeft
        visible: true
      }
      PropertyChanges {
        target: selectionImage
        visible: false
      }
      PropertyChanges {
        target: avatar
        size: Avatar.Size.Small
      }
    },

    State {
      name: "desktopIconModeSelected"
      when: checkable && control.checked && !checkBoxMode && !displayMobileMode
      PropertyChanges {
        target: bgRect
        border.width: 0
      }
      PropertyChanges {
        target: dropShadow
        visible: false
      }
      PropertyChanges {
        target: leftCheckbox
        visible: false
      }
      PropertyChanges {
        target: rightCheckBox
        visible: false
      }
      PropertyChanges {
        target: roleLabelBottom
        visible: false
      }
      PropertyChanges {
        target: roleLabelLeft
        visible: true
      }
      PropertyChanges {
        target: selectionImage
        visible: true
      }
      PropertyChanges {
        target: avatar
        size: Avatar.Size.Small
      }
    },
    State {
      name: "userStatusMobile"
      when: !checkable && stateVisible && displayMobileMode
      PropertyChanges {
        target: bgRect
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: leftCheckbox
        visible: false
      }
      PropertyChanges {
        target: rightCheckBox
        visible: false
      }
      PropertyChanges {
        target: roleLabelBottom
        visible: true
      }
      PropertyChanges {
        target: roleLabelLeft
        visible: false
      }
      PropertyChanges {
        target: selectionImage
        visible: false
      }
      PropertyChanges {
        target: avatar
        size: Avatar.Size.Big
      }
      PropertyChanges {
        target: control
        actionButton: true
      }
    },
    State {
      name: "userStatusDesktop"
      when: checkable && stateVisible && !control.checked && !displayMobileMode
      PropertyChanges {
        target: bgRect
        border.width: s(1)
        border.color: config.bdColor
      }
      PropertyChanges {
        target: leftCheckbox
        visible: true
      }
      PropertyChanges {
        target: rightCheckBox
        visible: false
      }
      PropertyChanges {
        target: roleLabelBottom
        visible: true
      }
      PropertyChanges {
        target: roleLabelLeft
        visible: false
      }
      PropertyChanges {
        target: selectionImage
        visible: false
      }
      PropertyChanges {
        target: avatar
        size: Avatar.Size.Big
      }
      PropertyChanges {
        target: control
        actionButton: true
      }
    },
    State {
      name: "userStatusDesktopSelected"
      when: checkable && stateVisible && control.checked && !displayMobileMode
      PropertyChanges {
        target: bgRect
        border.width: s(2)
        border.color: config.bdColorSelected
      }
      PropertyChanges {
        target: dropShadow
        visible: true
      }
      PropertyChanges {
        target: leftCheckbox
        visible: true
      }
      PropertyChanges {
        target: rightCheckBox
        visible: false
      }
      PropertyChanges {
        target: roleLabelBottom
        visible: true
      }
      PropertyChanges {
        target: roleLabelLeft
        visible: false
      }
      PropertyChanges {
        target: selectionImage
        visible: false
      }
      PropertyChanges {
        target: avatar
        size: Avatar.Size.Big
      }
      PropertyChanges {
        target: control
        actionButton: true
      }
    }
  ]

  Connections {
    target: selectionModel
    function onSelectionChanged() {
      control.checked = selectionModel.isSelected(objectModel.mapToSource(objectModel.index(index, 0)))
    }
  }
}
