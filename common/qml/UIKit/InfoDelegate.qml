import QtQuick 2.15
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit

Rectangle {
  implicitHeight: content.height + s(32)
  implicitWidth: parent.width
  color: ThemeController.style.slatePurpleColorOpacity10
  radius: s(4)

  property string contentText: ""
  property alias borderRect: leftBorder
  property alias textObject: content

  Rectangle {
    id: leftBorder
    anchors.left: parent.left
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    width: s(4)
    color: ThemeController.style.slatePurpleColor
  }

  UIKit.BaseText {
    id: content
    anchors {
      verticalCenter: parent.verticalCenter
      left: leftBorder.right
      right: parent.right
      leftMargin: ThemeController.style.margin.m12
      rightMargin: ThemeController.style.margin.m16
    }
    text: contentText
    size: UIKit.BaseText.TextSize.Small
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
  }
}
