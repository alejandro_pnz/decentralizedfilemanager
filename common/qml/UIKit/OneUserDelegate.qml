import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12
import com.test 1.0

AbstractButton {
  id: control

  implicitWidth: parent.width
  implicitHeight: app.isPhone ? s(175) : s(103)

  property string title: ""
  property string role: ""
  property int mode: Avatar.Mode.NoMode
  property var config: ThemeController.style.oneUserDelegate
  property string iconSource: "qrc:/edit.svg"
  property bool showIcon: true

  property bool online: true
  property string lastSeenDate: ""
  property string descriptionText: online ? Strings.onlineNow :
                                            Strings.lastSeen.arg(lastSeenDate)
  property alias avatarObject: avatar
  property alias name: nameText
  property alias roleLabel: roleLabel
  readonly property alias iconObject: titleIcon
  readonly property alias stateText: statusText
  readonly property alias stateRect: statusComponent

  contentItem: Item {
    anchors.fill: parent

    Avatar {
      id: avatar
      anchors{
        left: app.isPhone ? undefined : parent.left
        top: app.isPhone ? parent.top : undefined
        horizontalCenter: app.isPhone ? parent.horizontalCenter : undefined
      }
      text: title
      bottomIcon: true
      editIcon: false
      width: s(103)
      height: s(103)
      label.size: BaseText.TextSize.H2
      states: []
      userMode: mode
      bottomIconComponent.width: s(32)
      bottomIconComponent.height: s(32)
    }

    Column{
      anchors{
        left: app.isPhone ? undefined : avatar.right
        top: app.isPhone ? avatar.bottom : undefined
        topMargin: ThemeController.style.margin.m8
        leftMargin: ThemeController.style.margin.m16
        horizontalCenter: app.isPhone ? parent.horizontalCenter : undefined
        verticalCenter: app.isPhone ? undefined : parent.verticalCenter
      }

      height: childrenRect.height
      spacing: ThemeController.style.margin.m8

      Row {
        spacing: ThemeController.style.margin.m8
        BaseText {
          id: nameText
          text: control.title
          color: config.titleTextColor
          font.weight: Font.DemiBold
          size: BaseText.TextSize.H3
        }

        SvgImage {
          id: titleIcon
          width: s(16)
          height: s(16)
          anchors.verticalCenter: parent.verticalCenter
          source: iconSource
          visible: showIcon
        }
      }

      RowLayout {
        spacing: ThemeController.style.margin.m4
        anchors.horizontalCenter: app.isPhone ? parent.horizontalCenter : undefined

        Rectangle {
          id: roleLabel
          Layout.preferredHeight: ThemeController.style.margin.m24
          Layout.preferredWidth: labelContent.width + ThemeController.style.margin.m24
          color: config.labelColor
          visible: role !== ""
          radius: s(100)
          BaseText {
            id: labelContent
            anchors.centerIn: parent
            text: role
            color: config.titleTextColor
          }
        }

        StatusItem {
          id: statusComponent
          isOnline: online
        }

        BaseText {
          id: statusText
          Layout.fillWidth: true
          elide: Text.ElideRight
          size: BaseText.TextSize.Small
          color: ThemeController.style.shuttleColor
          text: descriptionText
          visible: text !== ""
        }
      }
    }
  }
}
