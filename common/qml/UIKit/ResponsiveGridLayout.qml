import QtQuick 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15


GridView {
  id: grid
  boundsBehavior: Flickable.StopAtBounds
  clip: true
  height: contentHeight

  // grid spacing
  property int horizontalSpacing: 0
  property int verticalSpacing: 0
  property int delegateHeight: s(100)
  property int delegateMinimumWidth: s(100)
  property int delegateMaximumWidth: s(200)

  property bool selectionMode: false


  readonly property real maximumCellWidth: grid.delegateMaximumWidth + grid.horizontalSpacing
  readonly property real cellCount: Math.floor((grid.width + grid.horizontalSpacing)
                                               / (grid.delegateMinimumWidth + grid.horizontalSpacing))
  readonly property real maximumCellContentWidth: grid.cellCount === 1
                                                  ? grid.cellWidth - grid.horizontalSpacing
                                                  : Math.min(grid.cellWidth - grid.horizontalSpacing, grid.delegateMaximumWidth)

  property int cellCountInRow: width / cellWidth

  cellWidth: {
    if (count <=0) {
      return width
    }
    if (count < grid.cellCount) {
      if (width >= count * maximumCellWidth) {
        return maximumCellWidth
      } else {
        return width / count
      }
    } else {
      width / cellCount
    }
  }

  cellHeight: delegateHeight + verticalSpacing
}
