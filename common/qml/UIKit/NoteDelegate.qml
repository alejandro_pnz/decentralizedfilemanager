import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import UIFragments 1.0

EditPanel {
  property string noteContent: ""
  signal openNotes()
  hasAdd: noteContent.length <= 0
  hasBottomSpacer: true

  onEditButtonClicked: {
    openNotes()
    finishEditing()
  }

  onAddButtonClicked: {
    openNotes()
    finishEditing()
  }

  title: Strings.notes
  hasSave: false
  hasSpacer: true

  enabled: canEdit

  InfoDelegate {
    contentText: Strings.addAnyText
    visible: noteContent.length <= 0
    Layout.preferredHeight: visible ? implicitHeight : 0
    Layout.minimumHeight: Layout.preferredHeight
    Layout.fillWidth: true
  }

  RowLayout {
    Layout.fillWidth: true
    Layout.preferredHeight: visible ? ThemeController.style.defaultInputHeight : 0
    Layout.minimumHeight: Layout.preferredHeight
    visible: noteContent.length > 0
    BaseText {
      Layout.fillHeight: true
      Layout.fillWidth: true
      text: noteContent
      maximumLineCount: 2
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      elide: Text.ElideRight
      textFormat: Text.RichText
    }

    LargeIconButton {
      Layout.alignment: Qt.AlignBottom
      icon.source: "qrc:/chevron-forward.svg"
      width: s(24)
      height: s(24)
      visible: isMobile
      onClicked: {
      }
    }

    StyledButton {
      text: Strings.readMore
      type: StyledButton.ButtonStyle.Text
      displayMode: StyledButton.DisplayMode.TextOnly
      Layout.alignment: Qt.AlignBottom
      visible: !isMobile
    }

  }
}
