import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

UIKit.PopupScreen {
  id: popup
  visible: false
  width: s(260)
  height: unsavedChangesFragment.height + s(32)
  showCloseButton: false
  showBackButton: false
  parent: Overlay.overlay
  dropShadow.radius: s(30)
  dropShadow.verticalOffset: s(20)
  dropShadow.color: Qt.rgba(0,0,0,0.2)

  Item {
    anchors{
      centerIn: parent
    }
    height: popup.height
    width: popup.width

    UnsavedChanges {
      id: unsavedChangesFragment
      anchors {
        top: parent.top
        left: parent.left
        right: parent.right
        margins: ThemeController.style.margin.m16
      }
      onClosePopup: close()
    }
  }
}
