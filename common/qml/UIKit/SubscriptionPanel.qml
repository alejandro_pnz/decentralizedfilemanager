import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import com.test 1.0
import QtQuick.Layouts 1.12

Rectangle {
  implicitWidth: parent.width
  implicitHeight: s(84)
  color: ThemeController.style.subscriptionPanel.bgColor
  signal upgradeClicked()

  property string subscriptionType: Strings.freeTrial
  property int daysLeft: 0

  property alias title: titleComponent
  property alias textItem: textComponent
  property alias button: button
  property alias icon: icon

  RowLayout {
    width: parent.width
    height: parent.height

    spacing: ThemeController.style.margin.m8
    Rectangle {
      id: iconContainer
      Layout.alignment: Qt.AlignVCenter
      Layout.leftMargin: ThemeController.style.margin.m16
      width: s(52)
      height: s(52)
      color: ThemeController.style.governorBayColor
      radius: s(100)

      Rectangle {
        width: s(44)
        height: s(44)
        color: ThemeController.style.blueVioletColor
        radius: s(100)
        anchors.centerIn: parent

        SvgImage {
          id: icon
          source: "qrc:/subscription.svg"
          height: s(24)
          width: s(24)
          anchors.centerIn: parent
        }
      }
    }

    Column {
      spacing: ThemeController.style.margin.m8
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignVCenter

      BaseText {
        id: titleComponent
        width: parent.width
        font.weight: Font.DemiBold
        text: Strings.subscription.arg(subscriptionType)
        color: ThemeController.style.subscriptionPanel.fontColor
        size: BaseText.TextSize.Small
      }

      BaseText {
        id: textComponent
        width: parent.width
        text: Strings.endsIn.arg(daysLeft)
        size: BaseText.TextSize.Small
        color: ThemeController.style.subscriptionPanel.fontColor
      }
    }

    UIKit.StyledButton {
      id: button
      Layout.alignment: Qt.AlignVCenter
      Layout.rightMargin: ThemeController.style.margin.m16
      Layout.leftMargin: ThemeController.style.margin.m4

      text: Strings.upgrade
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      type: UIKit.StyledButton.ButtonStyle.Secondary
      onClicked: upgradeClicked()
    }
  }
}
