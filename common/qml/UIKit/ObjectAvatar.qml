import QtQuick 2.15
import AppStyles 1.0

Rectangle {
  id: control
  enum Size { Big, Small, Smallest }
  // Todo probably move to c++ in the future
  enum ShareMode { Private, Public, NoSync }

  property bool blocked: false
  property int size: ObjectAvatar.Size.Big
  property string iconSrc: "qrc:/arca/blue.svg"

  property int pixelSize: {
    switch (control.size) {
    case ObjectAvatar.Size.Big: return s(103)
    case ObjectAvatar.Size.Small: return s(60)
    case ObjectAvatar.Size.Smallest: return s(48)
    }
  }

  property int iconSize: {
    switch (control.size) {
    case ObjectAvatar.Size.Big: return s(55)
    case ObjectAvatar.Size.Small: return s(32)
    case ObjectAvatar.Size.Smallest: return s(24)
    }
  }

  property int shareModeIconSize: {
    switch (control.size) {
    case ObjectAvatar.Size.Big: return s(32)
    case ObjectAvatar.Size.Small:
    case ObjectAvatar.Size.Smallest: return s(24)
    }
  }

  property string shareModeIcon: {
    switch (control.shareMode) {
    case ObjectAvatar.ShareMode.Private: return ""
    case ObjectAvatar.ShareMode.NoSync: return "qrc:/no-sync.svg"
    case ObjectAvatar.ShareMode.Public: return "qrc:/public.svg"
    default: return ""
    }    
  }

  property int shareMode: ObjectAvatar.ShareMode.Private
  color: ThemeController.style.objectAvatar.bgColor

  state: "normal"

  implicitHeight: pixelSize
  implicitWidth: pixelSize
  radius: width * 0.5

  readonly property alias shareModeIconComponent: shareModeIconComponent
  readonly property alias icon: icon

  ColoredImage {
    id: icon
    anchors.centerIn: parent
    icon.height: width
    source: control.iconSrc
  }

  SvgImage {
    id: shareModeIconComponent
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    width: control.shareModeIconSize
    height: width
    visible: control.shareMode === ObjectAvatar.ShareMode.NoSync ||
             control.shareMode === ObjectAvatar.ShareMode.Public
    source: control.shareModeIcon
  }


  states: [
    State {
      name: "normal"
      PropertyChanges {
        target: icon
        source: control.iconSrc
        color: "transparent"
        icon.width: control.iconSize
      }
    },
    State {
      name: "out-of-sync"
      PropertyChanges {
        target: icon
        source: "qrc:/out-of-sync.svg"
        color: ThemeController.style.objectAvatar.outOfSyncIconColor
        icon.width: s(24)
      }
    },
    State {
      name: "syncing"
      PropertyChanges {
        target: icon
        source: "qrc:/sync.svg"
        color: ThemeController.style.objectAvatar.syncingIconColor
        icon.width: s(24)
      }
    }
  ]
}
