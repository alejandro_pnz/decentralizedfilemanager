import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects

import AppStyles 1.0
import com.test 1.0
import QtQuick.Window 2.12

import com.testintl 1.0

Popup {
  id: tip
  width: s(376)
  property int popupContentHeight: box.implicitHeight + bottomPadding + topPadding
  height: app.isTablet ? Math.min(ApplicationWindow.window.height - ThemeController.style.pageHeaderHeight
          -ThemeController.style.pageFooterHeight - 16, popupContentHeight) : popupContentHeight
  padding: ThemeController.style.margin.m24
  topPadding: s(72)
  modal: true
  parent: Overlay.overlay

  property int triangleY: 0
  property int triangleLeftMargin: triangle.width + ThemeController.style.margin.m24
  default property alias children: box.children

  Overlay.modal: Item { }

  background: Item {
    Rectangle {
      id: bg
      radius: s(2)
      visible: false
      anchors.fill: parent
      CanvasTriangle {
        id: triangle
        x: -width
        y: tip.triangleY - height * 0.5
        pointing: CanvasTriangle.Pointing.Left
      }
    }
    DropShadow {
      anchors.fill: parent
      source: bg
      color: Qt.rgba(0,0,0,0.12)
      verticalOffset: 0
      radius: s(16)
      //samples: s(33)
    }
    LargeIconButton {
      id: closeButton
      icon.source: "qrc:/close.svg"
      onClicked: tip.close()
      anchors.right: parent.right
      anchors.top: parent.top
      anchors.topMargin: ThemeController.style.margin.m24
      anchors.rightMargin: ThemeController.style.margin.m24
    }
  }

  Flickable {
    id: flick
    width: parent.width
    height: parent.height
    boundsBehavior: Flickable.StopAtBounds
    contentHeight: box.height
    clip: true

    Column {
      id: box
      width: parent.width
    }
  }
}
