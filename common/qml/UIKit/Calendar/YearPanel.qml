import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0
//import Qt.labs.calendar 1.0

Rectangle {
  id: yearPanelContainer
  height: yearPanel.height

  // The min/max year that is displayed
  property int fromYear: 2000
  property int toYear: 2050

  property int startFrom: 2011

  // Number of years on page
  property int years: 21

  // The min/max year that can be choosen
  property date minDate
  property date maxDate
  property int currentYear: -1

  function back() {
    if (startFrom - years < fromYear) {
      startFrom = fromYear
    } else {
      startFrom = startFrom - years
    }
  }

  function forward() {
    if (startFrom + years > toYear) {
      startFrom = toYear - years
    } else {
      startFrom = startFrom + years
    }
  }

  function centerOnYear(year) {
    var page = Math.floor((year - fromYear + 1.0) / years)
    startFrom = fromYear + page * years
  }

  Grid {
    id: yearPanel
    width: parent.width
    columns: 3
    Repeater {
      model: yearPanelContainer.years
      Item {
        width: s(118)
        height: s(52)
        Button {
          id: control
          width: s(118)
          height: ThemeController.style.defaultInputHeight
          anchors.centerIn: parent
          property bool disabled: false
          ButtonGroup.group: yearGroup
          enabled: minDate.getFullYear() <= (index + startFrom) && maxDate.getFullYear() >= (index + startFrom)
          checked: currentYear === (index + startFrom)
          onClicked: calendar.switchToYear(index + startFrom)
          background: Item {
            Rectangle {
              anchors.fill: parent
              color: control.checked ? ThemeController.style.calendar.bgColorChecked
                                         : ThemeController.style.calendar.bgColor
              border.color: control.hovered && !control.checked ? ThemeController.style.calendar.bdColorHover
                                                                        : ThemeController.style.calendar.bdColor
              border.width: s(1)
              radius: s(4)
            }
          }

          UIKit.BaseText {
            text: modelData + startFrom
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: {
              if (control.isCurrentDate && ! control.checked)
                return ThemeController.style.calendar.textColorCurrentDate
              if (control.enabled) {
                if (control.checked) {
                  return ThemeController.style.calendar.textColorChecked
                } else {
                  return ThemeController.style.calendar.textColor
                }
              } else {
                return ThemeController.style.calendar.textColorDisabled
              }
            }
          }
        }
      }
    }
  }
}
