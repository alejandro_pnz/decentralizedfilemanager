import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12

Button {
  id: button

  implicitHeight: s(32)
  checkable: true

  property alias textObject: sectionText
  property alias iconObject: icon

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
    }
  }

  contentItem: Row {
    spacing: ThemeController.style.margin.m8
    UIKit.BaseText {
      id: sectionText
      text: button.text
      font.weight: Font.Bold
      size: UIKit.BaseText.TextSize.H3
      anchors.verticalCenter: parent.verticalCenter
    }

    UIKit.SvgImage {
      id: icon
      source: button.checked ? "qrc:/chevron-down.svg" : "qrc:/chevron-forward.svg"
      width: s(24)
      height: s(24)
      anchors.verticalCenter: parent.verticalCenter
    }
  }

  MouseArea{
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: Qt.PointingHandCursor
  }
}
