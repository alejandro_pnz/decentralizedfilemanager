import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0

Rectangle {
    width: s(105)
    height: ThemeController.style.defaultInputHeight
    color: ThemeController.style.whiteSmokeColor
    radius: s(4)
    signal back()
    signal forward()
    Rectangle {
        width: s(1)
        height: s(24)
        anchors.centerIn: parent
        color: ThemeController.style.separatorColor
    }
    
    UIKit.LargeIconButton {
        icon.source: "qrc:/chevron-back.svg"
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: ThemeController.style.margin.m8
        onClicked: back()
        buttonStyle: ThemeController.style.calendar.navigationButton
    }
    UIKit.LargeIconButton {
        icon.source: "qrc:/chevron-forward.svg"
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: ThemeController.style.margin.m8
        onClicked: forward()
        buttonStyle: ThemeController.style.calendar.navigationButton
    }
}
