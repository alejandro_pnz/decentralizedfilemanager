import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0

CheckBox {
  id: checkBox
  property bool checkRequired: false
  property bool errorState: false
  property string link: 'link'
  property color boldedTextColor: ThemeController.style.checkBox.highlightedTextColor
  property color requiredMarkerColor: ThemeController.style.checkBox.highlightedTextColor
  property alias indicatorControl: indicator
  width: text !== "" ? parent.width : ThemeController.style.checkBox.width
  spacing: ThemeController.style.margin.m8

  implicitHeight: Math.max(ThemeController.style.checkBox.height, textComp.implicitHeight)

  indicator: CheckIndicator {
    id: indicator
    errorState: checkBox.errorState
    checked: checkBox.checked
  }

  contentItem: BaseText {
    id: textComp
    color: ThemeController.style.checkBox.textColor
    anchors.top: indicator.top
    width: parent.width
    leftPadding: indicator.width + spacing
    verticalAlignment: Qt.AlignVCenter
    font.pixelSize: ThemeController.style.checkBox.textSize
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    textFormat: Text.RichText
    lineHeight: s(24)
    text: checkBox.text.indexOf("%1") !== -1 ? checkBox.text
    .arg("<a href=\"%3\" style=\"text-decoration:none;\"><font color=\"%4\"><b>")
    .arg("</b></font></a><font color=\"%5\">%6</font>").arg(link).arg(boldedTextColor)
    .arg(requiredMarkerColor).arg(checkRequired ? "*" : "") : checkRequired ?
    checkBox.text + "<font color=\"%1\"> *</font>".arg(requiredMarkerColor) :
    checkBox.text

    onLinkActivated: {
      RegistrationManager.openLink(link)
    }

    Accessible.ignored: true
  }

  onCheckedChanged: {
    if(checked && errorState){
      errorState = false
    }
  }

  // Accessibility
  Accessible.role: Accessible.CheckBox
  Accessible.name: checkBox.text
  Accessible.checkable: checkBox.checkable
  Accessible.checked: checkBox.checked
}
