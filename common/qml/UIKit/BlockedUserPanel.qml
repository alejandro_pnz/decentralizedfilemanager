import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import com.test 1.0

Item {
  implicitWidth: parent.width
  implicitHeight: column.height + s(32)

  property string titleText: Strings.userBlocked
  property string contentText: Strings.userBlockedInfo
  property alias icon: icon
  property alias title: title
  property alias unblockButton: unblockButton
  property alias content: content
  property alias bgRect: bgRect

  signal unblockClicked()

  Rectangle {
    id : bgRect
    anchors.fill: parent
    color: ThemeController.style.blockedUserPanel.bgColor
    radius: s(4)
    border {
      color: ThemeController.style.blockedUserPanel.bdColor
      width: s(1)
    }

    SvgImage {
      id: icon
      height: s(24)
      width: s(24)
      source: "qrc:/remove.svg"
      anchors {
        top: parent.top
        topMargin: ThemeController.style.margin.m20
        left: parent.left
        leftMargin: ThemeController.style.margin.m16
      }
    }

    ColumnLayout {
      id: column
      anchors{
        top: parent.top
        left: icon.right
        right: parent.right
        leftMargin: ThemeController.style.margin.m12
        rightMargin: ThemeController.style.margin.m16
        topMargin: ThemeController.style.margin.m16
      }
      spacing: ThemeController.style.margin.m4

      RowLayout {
        spacing: ThemeController.style.margin.m8
        UIKit.BaseText {
          id: title
          text: titleText
          font.weight: Font.DemiBold
        }

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true
        }

        UIKit.StyledButton {
          id: unblockButton
          type: UIKit.StyledButton.ButtonStyle.Text
          text: Strings.unblock
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          onClicked: unblockClicked()
        }
      }

      UIKit.BaseText {
        id: content
        text: contentText
        color: ThemeController.style.blockedUserPanel.contentTextColor
        size: UIKit.BaseText.TextSize.Small
        Layout.preferredWidth: parent.width - unblockButton.width - s(16)
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      }
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: bgRect
    source: bgRect
    color: ThemeController.style.blockedUserPanel.dropShadowColor
    radius: s(10)
    verticalOffset: s(2)
    //samples: radius * 2 + 1
  }
}
