import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects


Item {
  implicitHeight: ThemeController.style.defaultInputHeight
  implicitWidth: contentRect.width

  property string name: ""
  property string colorValue: ""
  property int value: 0

  property alias nameLabel: nameLabel
  property alias colorRect: colorRect
  property alias valueLabel: valueLabel

  Rectangle {
    id: contentRect
    height: parent.height
    width: contentRow.width + s(32)
    radius: s(2)

    Row {
      id: contentRow
      spacing: ThemeController.style.margin.m8
      height: s(24)
      anchors.verticalCenter: parent.verticalCenter
      anchors.horizontalCenter: parent.horizontalCenter

      Rectangle {
        id: colorRect
        height: s(12)
        width: s(12)
        color: colorValue
        radius: s(1)
        anchors.verticalCenter: parent.verticalCenter
      }

      BaseText {
        id: nameLabel
        text: name
      }

      BaseText {
        id: valueLabel
        text: value + "%"
      }
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: contentRect
    source: contentRect
    color: Qt.rgba(0, 0, 0, 0.12)
    radius: s(16)
    verticalOffset: 0
    //samples: radius * 2 + 1
  }
}
