import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  signal selectFromMap()
  spacing: ThemeController.style.margin.m16
  width: parent.width

  property alias titleLabel: titleLabel
  property alias titleButton: titleButton
  property alias pickerDelegate: pickerDelegate

  RowLayout {
    width: parent.width

    BaseText {
      id: titleLabel
      text: Strings.address
      font.weight: Font.Bold
    }

    Item {
      Layout.fillHeight: true
      Layout.fillWidth: true
    }

    StyledButton {
      id: titleButton
      type: StyledButton.ButtonStyle.Text
      displayMode: StyledButton.DisplayMode.TextOnly
      text: Strings.selectWithMaps
      onClicked: selectFromMap()
    }
  }

  PickerDelegate {
    id: pickerDelegate
    labelText: Strings.country
    placeholderTxt: Strings.country
  }
}
