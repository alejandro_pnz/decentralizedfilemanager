import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0

import Qt5Compat.GraphicalEffects

Button {
  id: button
  enum ButtonStyle {
    Primary,
    Secondary,
    SecondaryCircular,
    Text,
    Destructive
  }

  enum DisplayMode {
    IconOnly,
    TextOnly,
    TextRightSideIcon,
    TextLeftSideIcon
  }

  hoverEnabled: true
  leftPadding: button.displayMode === StyledButton.DisplayMode.IconOnly ? ThemeController.style.margin.m12 : buttonStyle.leftPadding
  rightPadding: button.displayMode === StyledButton.DisplayMode.IconOnly ? ThemeController.style.margin.m12 : buttonStyle.rightPadding
  topPadding: button.displayMode === StyledButton.DisplayMode.IconOnly ? ThemeController.style.margin.m12 : buttonStyle.topPadding
  bottomPadding: button.displayMode === StyledButton.DisplayMode.IconOnly ? ThemeController.style.margin.m12 : buttonStyle.bottomPadding
  icon.source: "qrc:/chevron-back.svg"
  implicitHeight: button.type === StyledButton.ButtonStyle.Text ? ThemeController.style.margin.m24 : ThemeController.style.margin.m48
//  width: button.displayMode === StyledButton.DisplayMode.IconOnly ? ThemeController.style.margin.m48 : implicitWidth
//  height: button.displayMode === StyledButton.DisplayMode.IconOnly ? ThemeController.style.margin.m48 : implicitHeight

  // Readonly properties for unit tests
  readonly property color bgColor: bgRect.color
  readonly property color textColor: txt.color
  readonly property color bdColor: bgRect.border.color
  readonly property color iconColor: imgOverlay.color
  readonly property bool iconVisible: iconLayout.visible
  readonly property bool textVisible: txt.visible
  property alias textItem: txt
  property alias iconImg: img

  property int displayMode: StyledButton.DisplayMode.TextRightSideIcon
  property int type: StyledButton.ButtonStyle.Primary
  property int spacingBetweenIconAndText: s(6)
  property bool overlayVisible: true
  property bool willShowTips: false
  signal iconClicked()

  // Get proper style for button depending on type and display mode
  property var buttonStyle: {
    switch(button.type) {
    case StyledButton.ButtonStyle.Primary: return ThemeController.style.button.primary
    case StyledButton.ButtonStyle.Secondary: return ThemeController.style.button.secondary
    case StyledButton.ButtonStyle.SecondaryCircular: return ThemeController.style.button.secondaryCircular
    case StyledButton.ButtonStyle.Text: return button.displayMode === StyledButton.DisplayMode.TextOnly
                                        ? ThemeController.style.button.textNoIcon : ThemeController.style.button.text
    case StyledButton.ButtonStyle.Destructive: return ThemeController.style.button.destructive
    }
  }

  // Background component definition
  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      color: {
        if (button.enabled) {
          if (button.down) {
            return button.buttonStyle.bgColorPressed
          } else if (button.hovered) {
            return button.buttonStyle.bgColorHover
          } else if (button.activeFocus) {
            return button.buttonStyle.bgColorFocus
          } else {
            return button.buttonStyle.bgColor
          }
        } else {
          return button.buttonStyle.bgColorDisabled
        }
      }
      radius: button.displayMode === StyledButton.DisplayMode.IconOnly ? width * 0.5 : ThemeController.style.margin.m4
      border {
        width: button.enabled && button.activeFocus
               && !button.hovered && !button.down ? s(3) : s(2)
        color: {
          if (button.enabled) {
            if (button.down) {
              return button.buttonStyle.bdColorPressed
            } else if (button.hovered) {
              return button.buttonStyle.bdColorHover
            } else if (button.activeFocus) {
              return button.buttonStyle.bdColorFocus
            } else {
              return button.buttonStyle.bdColor
            }
          } else {
            return button.buttonStyle.bdColorDisabled
          }
        }
      }
    }
  }

  contentItem: Item {
    implicitWidth: contentLayout.implicitWidth
    implicitHeight: contentLayout.implicitHeight
    RowLayout {
      id: contentLayout
      anchors.centerIn: parent
      spacing: button.spacingBetweenIconAndText
      layoutDirection: displayMode === StyledButton.DisplayMode.TextLeftSideIcon ? Qt.RightToLeft : Qt.LeftToRight
      Item {
        id: iconLayout
        Layout.minimumHeight: img.height
        Layout.minimumWidth: img.width
        Layout.alignment: Qt.AlignVCenter
        visible: displayMode !== StyledButton.DisplayMode.TextOnly
        MouseArea {
            anchors.fill: parent
            onClicked: willShowTips ? iconClicked() : button.clicked()
        }

        SvgImage {
          id: img
          anchors.centerIn: parent
          source: button.icon.source
          width: s(24)
          height: s(24)
          visible: !imgOverlay.visible
        }
        ColorOverlay {
          id: imgOverlay
          anchors.fill: img
          source: img
          visible: overlayVisible
          color: {
            if (button.enabled) {
              if (button.down) {
                return button.buttonStyle.iconColorPressed
              } else if (button.hovered) {
                return button.buttonStyle.iconColorHover
              } else if (button.activeFocus) {
                return button.buttonStyle.iconColorFocus
              } else {
                return button.buttonStyle.iconColor
              }
            } else {
              return button.buttonStyle.iconColorDisabled
            }
          }
        }
      }

      BaseText {
        id: txt
        Layout.alignment: Qt.AlignVCenter
        text: button.text
        visible: displayMode !== StyledButton.DisplayMode.IconOnly
        font.weight: Font.Bold

        color: {
          if (button.enabled) {
            if (button.down) {
              return button.buttonStyle.textColorPressed
            } else if (button.hovered) {
              return button.buttonStyle.textColorHover
            } else if (button.activeFocus) {
              return button.buttonStyle.textColorFocus
            } else {
              return button.buttonStyle.textColor
            }
          } else {
            return button.buttonStyle.textColorDisabled
          }
        }

        Accessible.ignored: true
      }
    }
  }

  // Accessibility
  Accessible.role: Accessible.Button
  Accessible.name: button.text
}
