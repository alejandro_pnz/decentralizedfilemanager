import QtQuick 2.12
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

Popup {
  y: combobox.height + 8
  width: combobox.width
  implicitHeight: Math.min(contentItemHeight, 480)

  padding: 1
  topPadding: 0
  property var pickerModel
  property int contentItemHeight: search.height + listView.contentHeight + 32

  contentItem: ColumnLayout {
    width: parent.width
    height: parent.height
    spacing: 8
    UIKit.SearchInput {
      id: search
      Layout.topMargin: 16
      Layout.fillWidth: true
      Layout.leftMargin: 16
      Layout.rightMargin: 16
      placeholderText: Strings.searchCountries
      Layout.preferredHeight: 48
      micIconVisible: false
    }

    ListView {
      id: listView
      Layout.fillHeight: true
      Layout.fillWidth: true
      model: pickerModel
      clip: true
      ScrollIndicator.vertical: ScrollIndicator { }
    }
  }

  background: Item {
    Rectangle {
      id: contentRect
      radius: 8
      anchors.fill: parent
    }

    DropShadow {
      id: dropShadow
      anchors.fill: contentRect
      horizontalOffset: 0
      verticalOffset: 0
      radius: 32
      samples: 65
      color: Qt.rgba(0, 0, 0, 0.08)
      source: contentRect
    }
  }
}
