import QtQuick 2.12
import QtQuick.Controls 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

UIKit.PopupScreen {
  id: popup
  visible: false
  width: s(588)
  height: Math.min(parent.height - 2 * s(80), s(740))
  showCloseButton: false
  showBackButton: false
  parent: Overlay.overlay

  Item {
    anchors{
      centerIn: parent
    }
    height: popup.height
    width: popup.width

    UIKit.LargeIconButton {
      id: closeButton
      icon.source: "qrc:/close.svg"
      height: s(32)
      width: s(32)
      anchors {
        top: parent.top
        topMargin: ThemeController.style.margin.m24
        right: parent.right
        rightMargin: ThemeController.style.margin.m24
      }
      z: parent.z + 1

      onClicked: popup.close()
    }

    UnlockAuthorization {
      id: fragment
      anchors {
        top: parent.top
        bottom: footer.top
        left: parent.left
        right: parent.right
        topMargin: ThemeController.style.margin.m32
        leftMargin: ThemeController.style.margin.m24
        rightMargin: ThemeController.style.margin.m24
      }
    }

    Rectangle {
      id: footer
      anchors.bottom: parent.bottom
      width: parent.width
      height: s(96)
      color: ThemeController.style.whiteSmokeColor
      radius: s(8)

      Rectangle {
        anchors.top: parent.top
        height: s(4)
        width: parent.width
        color: ThemeController.style.whiteSmokeColor
      }

      UIKit.StyledButton {
        text: Strings.continueTxt
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: ThemeController.style.margin.m24
        anchors.leftMargin: ThemeController.style.margin.m24
        anchors.verticalCenter: parent.verticalCenter
        onClicked: {
          popup.close()
        }
      }
    }
  }
}
