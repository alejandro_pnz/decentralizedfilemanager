import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import AppStyles 1.0
import UIKit 1.0
import com.test 1.0

Popup {
  id: tip
  width: 376
  height: box.implicitHeight + bottomPadding + topPadding
  padding: 24
  topPadding: 72
  modal: true
  parent: Overlay.overlay

  property int triangleY: 0
  property int triangleLeftMargin: triangle.width + 24

  property alias tipTitle: box.title
  property alias tipContent: box.content
  property alias tipInfo: box.info
  property bool showDontShowAgain: true
  property bool showPlaceholder: true


  Overlay.modal: Item { }

  background: Item {
    Rectangle {
      id: bg
      radius: 2
      visible: false
      anchors.fill: parent
      CanvasTriangle {
        id: triangle
        x: -width
        y: tip.triangleY - height * 0.5
        pointing: CanvasTriangle.Pointing.Left
      }
    }
    DropShadow {
      anchors.fill: parent
      source: bg
      color: Qt.rgba(0,0,0,0.12)
      verticalOffset: 0
      radius: 16
      samples: 33
    }
    LargeIconButton {
      id: closeButton
      icon.source: "qrc:/close.svg"
      onClicked: tip.close()
      anchors.right: parent.right
      anchors.top: parent.top
      anchors.topMargin: 24
      anchors.rightMargin: 24
    }
  }

  TipBox {
    anchors.left: parent.left
    anchors.right: parent.right
    id: box
    showPlaceholder: tip.showPlaceholder
    RowLayout {
      visible: showDontShowAgain
      Layout.topMargin: 4
      Layout.fillWidth: true
      BaseText {
        text: Strings.dontShowItAgain
        font.weight: Font.DemiBold
        Layout.fillWidth: true
      }
      SwitchButton {
      }
    }

    StyledButton {
      text: Strings.gotIt
      Layout.fillWidth: true
      Layout.topMargin: 20
      displayMode: StyledButton.DisplayMode.TextOnly
      onClicked: tip.close()
    }
  }
}
