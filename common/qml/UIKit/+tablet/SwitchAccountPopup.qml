import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit
import QtGraphicalEffects 1.0
import UIFragments 1.0

Popup {
  id: popup
  implicitWidth: s(375)
  implicitHeight: s(373)
  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
  topPadding: 0

  background: Item {
    Rectangle {
      id: bgComponent
      anchors.fill: parent
      radius: s(8)
    }
    DropShadow {
      id: dropShadow
      anchors.fill: bgComponent
      horizontalOffset: 0
      verticalOffset: 0
      radius: s(32)
      samples: 2 * radius + 1
      color: Qt.rgba(0, 0, 0, 0.08)
      source: bgComponent
    }
  }

  Item {
    id: popupHeader
    anchors {
      top: parent.top
      right: parent.right
      left: parent.left
    }
    height: s(68)

    RowLayout {
      anchors {
        top: parent.top
        topMargin: ThemeController.style.margin.m20
        left: parent.left
        right: parent.right
      }

      UIKit.BaseText {
        text: Strings.switchAccount
        font.weight: Font.Bold
        Layout.fillWidth: true
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/add-circle.svg"
        overlay.visible: false
        onClicked: {
        }
      }
    }

    Rectangle {
      anchors {
        bottom: parent.bottom
        left: parent.left
        right: parent.right
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
      }
      color: ThemeController.style.mercuryColor
      height: s(1)
    }
  }

  SwitchAccount {
    id: switchAccount
    anchors {
      top: popupHeader.bottom
      topMargin: ThemeController.style.margin.m12
      bottom: parent.bottom
      bottomMargin: ThemeController.style.margin.m8
      left: parent.left
      right: parent.right
    }
  }
}
