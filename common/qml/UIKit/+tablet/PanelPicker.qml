import QtQuick 2.12
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.12
import UIFragments 1.0
import UIModel 1.0 as UIModel
import QtQml.Models 2.12
import QtGraphicalEffects 1.0

Popup {
  id: picker
  y: control.height + s(8)
  width: control.width
  implicitHeight: contentItem.implicitHeight + s(32)
  padding: ThemeController.style.margin.m16
  property string title: ""
  property var model
  signal itemClicked(int index)

  Item {
    id: itemModel
    property var model: popup.model
    property alias ism: ism

    function select(index) {
      ism.select(popup.model.index(index,0), ItemSelectionModel.Toggle)
    }
    ItemSelectionModel {
      id: ism
      model: popup.model
    }
  }

  contentItem: CheckableTextLabelPanel {
    titleComponent.visible: false
    model: itemModel
    width: parent.width
    onDelegateClicked:{
      itemClicked(index)
    }
  }

  background: Item {
    id: popupBackground

    Rectangle {
      id: contentRect
      radius: s(8)
      anchors.fill: parent
    }

    DropShadow {
      id: dropShadow
      anchors.fill: contentRect
      horizontalOffset: 0
      verticalOffset: 0
      radius: s(32)
      samples: s(65)
      color: Qt.rgba(0, 0, 0, 0.08)
      source: contentRect
    }
  }
}
