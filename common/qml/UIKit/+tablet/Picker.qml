import QtQuick 2.12
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

Popup {
  y: combobox.height + s(8)
  width: combobox.width
  implicitHeight: contentItem.implicitHeight + s(16)
  padding: s(1)
  property var pickerModel
  property string title: ""

  contentItem: ListView {
    topMargin: ThemeController.style.margin.m8
    bottomMargin: ThemeController.style.margin.m8
    id: listView
    clip: true
    implicitHeight: contentHeight
    model: pickerModel

    ScrollIndicator.vertical: ScrollIndicator { }
  }

  background: Item {
    Rectangle {
      id: contentRect
      radius: s(8)
      anchors.fill: parent
    }

    DropShadow {
      id: dropShadow
      anchors.fill: contentRect
      horizontalOffset: 0
      verticalOffset: 0
      radius: s(32)
      samples: s(65)
      color: Qt.rgba(0, 0, 0, 0.08)
      source: contentRect
    }
  }
}
