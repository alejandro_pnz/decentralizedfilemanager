import QtQuick 2.12
import QtQuick.Controls 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

UIKit.PopupScreen {
  id: popup
  visible: false
  width: s(588)
  height: fragment.linkedAuthenticatorApp ? s(220) : s(244)
  showCloseButton: false
  showBackButton: false
  parent: Overlay.overlay

  Item {
    anchors{
      centerIn: parent
    }
    height: popup.height
    width: popup.width

    UIKit.LargeIconButton {
      id: closeButton
      icon.source: "qrc:/close.svg"
      height: s(32)
      width: s(32)
      anchors {
        top: parent.top
        topMargin: ThemeController.style.margin.m24
        right: parent.right
        rightMargin: ThemeController.style.margin.m24
      }
      z: parent.z + 1

      onClicked: popup.close()
    }

    EnterAuthorizationCode {
      id: fragment
      anchors {
        top: parent.top
        bottom: parent.bottom
        left: parent.left
        right: parent.right
        topMargin: ThemeController.style.margin.m32
        leftMargin: ThemeController.style.margin.m24
        rightMargin: ThemeController.style.margin.m24
      }
    }
  }
}
