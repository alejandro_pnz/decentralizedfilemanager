import QtQuick 2.12
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

Popup {
  id: popup
  // If popup should be scrollable use this property to set maximum height
  // Also explicitly set popup.height to the same value
  property int maxHeight: s(700)

  implicitHeight: content.height + 2 * padding
  implicitWidth: s(412)
  padding: ThemeController.style.margin.m16
  background: Item {
    Rectangle {
      id: bgComponent
      anchors.fill: parent
      radius: s(8)
    }
    DropShadow {
      id: dropShadow
      anchors.fill: bgComponent
      horizontalOffset: 0
      verticalOffset: 0
      radius: s(30)
      samples: s(61)
      color: Qt.rgba(0, 0, 0, 0.08)
      source: bgComponent
    }
  }

  ColumnLayout {
    id: content
    spacing: ThemeController.style.margin.m16
    height: {
      console.log(listView.contentHeight + content.spacing + topRow.height, maxHeight - 2 * padding)
      Math.min(maxHeight - 2 * padding,
                     listView.contentHeight + content.spacing + topRow.height)
    }
    width: popup.implicitWidth - 2 * padding
    RowLayout {
      id: topRow
      Layout.fillWidth: true
      Layout.minimumHeight: s(24)
      UIKit.BaseText {
        Layout.fillWidth: true
        text: Strings.notifications
        font.weight: Font.Bold
      }
      UIKit.StyledButton {
        type: UIKit.StyledButton.ButtonStyle.Text
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        text: Strings.markAsRead
        onClicked: {
          popup.close()
        }
      }
    }
    UIKit.NotificationView {
      id: listView
      Layout.fillHeight: true
      Layout.fillWidth: true
    }
  }
}
