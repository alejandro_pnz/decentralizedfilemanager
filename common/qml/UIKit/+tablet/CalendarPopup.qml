import QtQuick 2.12
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

Popup {
  id: picker
  implicitHeight: calendar.height + 2 * padding
  implicitWidth: calendar.width + 2 * padding
  padding: ThemeController.style.margin.m16
  property var dateInput
  property alias calendar: calendar
  background: Item {
    Rectangle {
      id: bgComponent
      anchors.fill: parent
      radius: s(4)
    }
    DropShadow {
      id: dropShadow
      anchors.fill: bgComponent
      horizontalOffset: 0
      verticalOffset: 0
      radius: s(30)
      samples: s(61)
      color: Qt.rgba(0, 0, 0, 0.05)
      source: bgComponent
    }
  }
  UIKit.Calendar {
    id: calendar
  }
}
