import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

import com.testintl 1.0

Item {
  id: root
  implicitWidth: parent.width
  implicitHeight: childrenRect.height
  readonly property bool isMobile: app.isPhone

  property string titleText: Strings.lockQuod
  property string contentText: Strings.lockTxt
  property alias icon: icon
  property alias title: title
  property alias switchButton: switchButton
  property alias content: content
  property string tipTitle: Strings.whatDoesItMean
  property string tipContent: Strings.userWillHaveAccess
  property string tipInfo: Strings.asTheAccountOwner
  property string imageSource: ""
  property bool showPlaceholder: false
  property bool showDontShowAgain: false
  property int mobilePage: Pages.PowerDevicesPage
  property alias checked: switchButton.checked


  signal switchButtonToggled()

  Rectangle {
    id: lockIcon
    anchors.top: parent.top
    anchors.left: parent.left
    color: ThemeController.style.whiteSmokeColor
    radius: s(100)
    width: s(48)
    height: ThemeController.style.defaultInputHeight

    ColoredImage {
      id: icon
      height: s(24)
      width: s(24)
      source: "qrc:/lock.svg"
      anchors.centerIn: parent
    }
  }

  ColumnLayout {
    id: contentColumn
    anchors.top: parent.top
    anchors.left: lockIcon.right
    anchors.right: parent.right
    anchors.leftMargin: ThemeController.style.margin.m12

    RowLayout {
      id: rowLayout
      spacing: ThemeController.style.margin.m8
      UIKit.BaseText {
        id: title
        text: titleText
        font.weight: Font.DemiBold
      }

      BigTipButton {
        tipTitle: root.tipTitle
        tipContent: root.tipContent
        tipInfo: root.tipInfo
        imageSource: root.imageSource
        showDontShowAgain: root.showDontShowAgain
        showPlaceholder: root.showPlaceholder
        clickHandler: function() {
          if(app.isPhone){
            homeManager.openPage(mobilePage)
          }
          else{
            openTip()
          }
        }
      }

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
      }

      SwitchButton {
        id: switchButton
        onToggled: {
          if(checked){
            switchButtonToggled()
          }
        }
      }
    }

    UIKit.BaseText {
      id: content
      text: contentText
      color: ThemeController.style.shuttleColor
      size: isMobile ? UIKit.BaseText.TextSize.Small : UIKit.BaseText.TextSize.Body
      Layout.maximumWidth: isMobile ? parent.width - switchButton.width - s(12) : s(476)
      Layout.fillWidth: isMobile
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }
  }
}
