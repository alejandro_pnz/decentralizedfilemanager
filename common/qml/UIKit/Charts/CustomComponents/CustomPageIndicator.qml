import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0

Rectangle {
  id: root

  property int pageIndicatorCount
  property var pageChartsObjects
  property alias currentIndex: indicatorControl.currentIndex

  property bool isMobile: Qt.platform.os === "android" || Qt.platform.os === "ios"

  radius: s(8)
  width: s(792)

  function swipeRight() {
    if(indicatorControl.currentIndex < indicatorControl.count - 1) {
      indicatorControl.currentIndex += 1
      pageChartsObjects[indicatorControl.currentIndex].loadExtendedChart()
    }
  }

  function swipeLeft() {
    if(indicatorControl.currentIndex > 0) {
      indicatorControl.currentIndex -= 1
      pageChartsObjects[indicatorControl.currentIndex].loadExtendedChart()
    }
  }

  UIKit.LargeIconButton {
    id: leftButton
    anchors.verticalCenter: parent.verticalCenter
    anchors.left: parent.left
    icon.source: "qrc:/chevron-back.svg"
    onClicked: {
      swipeLeft()
    }
    visible: indicatorControl.currentIndex !== 0 && !isMobile
  }

  PageIndicator {
    id: indicatorControl

    anchors.centerIn: parent
    count: pageIndicatorCount
    spacing: ThemeController.style.margin.m16
    leftPadding: ThemeController.style.margin.m12
    rightPadding: ThemeController.style.margin.m12
    topPadding: ThemeController.style.margin.m8
    bottomPadding: ThemeController.style.margin.m8

    background: Rectangle {
      height: s(24)
      radius: s(100)
      color: ThemeController.style.whisperColor
    }

    delegate: Rectangle {
      width: s(8)
      height: s(8)
      radius: s(100)
      color: index === indicatorControl.currentIndex ?
               ThemeController.style.slatePurpleColor :
               ThemeController.style.ironColor
    }
  }

  UIKit.LargeIconButton {
    id: rightButton
    anchors.verticalCenter: parent.verticalCenter
    anchors.right: parent.right
    icon.source: "qrc:/chevron-forward.svg"
    onClicked: {
      swipeRight()
    }
    visible: indicatorControl.currentIndex !== pageIndicatorCount - 1 && !isMobile
  }
}
