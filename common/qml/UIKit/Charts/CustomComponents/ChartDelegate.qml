import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import Qt5Compat.GraphicalEffects


import "../Chart.js" as Chart
import ".."

Button {
  id: button

  implicitHeight: parent.height
  implicitWidth: parent.width

  property alias extendChartMA: extendChartMA

  property string title

  property int maxValue: 10

  property var categoriesLabels: []
  property var dataSets: []

  property alias dropShadow: dropShadow
  property alias bgRect: bgRect
  property alias titleComponent: titleComponent
  property alias iconImage: iconImage

  signal iconClicked()

  default property alias children: chartRectangle.children

  state: 'default'

  background: Item {
    Rectangle {
      id: bgRect
      anchors.fill: parent
      border.width: s(1)
      border.color: ThemeController.style.chartDelegate.bdColor
      radius: s(8)
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      horizontalOffset: 0
      verticalOffset: s(2)
      radius: s(10)
      //samples: s(21)
      source: bgRect
      color: Qt.rgba(0, 0, 0, 0.04)
    }
  }

  contentItem: Item {
    id: root
    anchors.fill: parent

    Item {
      id: header
      width: parent.width
      height: ThemeController.style.defaultInputHeight

      UIKit.BaseText {
        id: titleComponent
        text: title
        font.weight: Font.DemiBold
        anchors {
          left: parent.left
          leftMargin: ThemeController.style.margin.m12
          verticalCenter: parent.verticalCenter
        }
      }

      Rectangle {
        anchors.right: iconArea.left
        width: s(1)
        height: parent.height
        color: ThemeController.style.chartDelegate.bdColor
      }

      Item {
        id: iconArea
        width: s(48)
        height: parent.height
        anchors {
          right: parent.right
          verticalCenter: parent.verticalCenter
        }
        UIKit.SvgImage {
          id: iconImage
          anchors.centerIn: parent
          source: "qrc:/expand_icon.svg"
          width: s(24)
          height: s(24)
        }

        MouseArea {
          id: extendChartMA
          anchors.fill: parent
          onClicked: iconClicked()
        }
      }
    }

    Rectangle {
      id: spacer
      width: parent.width
      height: s(1)
      anchors.top: header.bottom
      color: ThemeController.style.chartDelegate.bdColor
    }

    Item {
      id: chartRectangle

      width: parent.width
      anchors.top: spacer.bottom
      anchors.bottom: parent.bottom

      Chart {
        id: chartView

        anchors {
          fill: parent
          leftMargin: ThemeController.style.margin.m12
          rightMargin: ThemeController.style.margin.m12
          topMargin: ThemeController.style.margin.m12
          bottomMargin: 0
        }

        chartType: "bar"

        chartData: {return {
            labels: categoriesLabels,
            datasets: dataSets
          }
        }

        chartOptions: {return {
            maintainAspectRatio: false,
            responsive: true,
            legend: {
              display: false
            },
            tooltips: {
              enabled: false
            },
            scales: {
              xAxes: [{
                  stacked: true,
                  gridLines: {
                    display: false
                  },
                  ticks: {
                    callback: function(value, index, values) {
                      return '';
                    }
                  }
                }],
              yAxes: [
                {
                  stacked: true,
                  gridLines: {
                    display: false
                  },
                  ticks: {
                    callback: function(value, index, values) {
                      return '';
                    }
                  }
                }
              ]
            }
          }
        }
      }
    }
  }

  states: [
    State {
      name: "default"
      when: !button.hovered && !button.pressed
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.chartDelegate.bgColor
      }
      PropertyChanges {
        target: dropShadow
        verticalOffset: s(2)
        radius: s(10)
        //samples: s(21)
        color: ThemeController.style.chartDelegate.dropShadowColor
      }
    },

    State {
      name: "hovered"
      when: button.hovered && !button.pressed
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.chartDelegate.bgColor
      }
      PropertyChanges {
        target: dropShadow
        verticalOffset: 0
        radius: s(30)
        //samples: s(62)
        color: ThemeController.style.chartDelegate.dropShadowHoveredColor
      }
    },

    State {
      name: "pressed"
      when: button.pressed
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.chartDelegate.bgColorPressed
      }
      PropertyChanges {
        target: dropShadow
        verticalOffset: s(2)
        radius: s(10)
        //samples: s(21)
        color: ThemeController.style.chartDelegate.dropShadowColor
      }
    }
  ]
}
