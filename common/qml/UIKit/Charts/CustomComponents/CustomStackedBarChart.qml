import QtQuick 2.15

import "../Chart.js" as Chart
import ".."

Chart {
  id: rootChart

  property var dataSets: []
  property var barsCategoriesLabels

  Connections {
    target: root

    function onNewDataSet() {
      console.log("Chart Model Updated!")
      chartView.chartData = {
        labels: barsCategoriesLabels,
        datasets: dataSets
      }
      chartView.requestPaint()
    }
  }

  chartType: "bar"

  chartData: {return {
      labels: barsCategoriesLabels,
      datasets: dataSets
    }
  }

  chartOptions: {return {
      maintainAspectRatio: false,
      responsive: true,
      legend: {
        display: false,
        position: 'bottom',
        labels: {
          boxWidth: s(12),
        }
      },
      elements: {
        rectangle: {
          hoverBorderWidth: s(2),
          hoverBorderColor: "#000",
          hoverBackgroundColor: function(context) {
            var index = context.dataIndex;
            var value = context.dataset.backgroundColor[index];
            return value;
          }
        }
      },
      scales: {
        xAxes: [{
            stacked: true,
            gridLines: {
              display: false,
              drawTicks: true
            }
          }],
        yAxes: [
          {
            stacked: true,
            gridLines: {
              display: false,
              drawTicks: true
            },
            ticks: {
              min: 0,
              max: 100,
              maxTicksLimit: 5,
              stepSize: 25,
              callback: function(value, index, values) {
                return root.cbDisplayText !== root.title ? '' : value + '% -';
              }
            }
          }
        ]
      },
      tooltips: {
        backgroundColor: "#fff",
        titleFontColor: "#000",
        bodyFontColor: "#000",
        yPadding: s(12),
        xPadding: s(16),
        cornerRadius: s(2),
        titleFontSize: s(14),
        bodyFontSize: s(14),
      }
    }
  }
}
