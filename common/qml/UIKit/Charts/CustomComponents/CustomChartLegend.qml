import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0

Flow {
  property ListModel legendModel
  spacing: ThemeController.style.margin.m24

  Repeater {
    Layout.alignment: Qt.AlignHCenter
    model: legendModel

    Row {
      spacing: ThemeController.style.margin.m8
      Layout.alignment: Qt.AlignHCenter
      height: s(12)

      Rectangle {
        id: rectColor
        color: model.color
        width: s(12)
        height: s(12)
      }

      UIKit.BaseText {
        id: nameColor
        text: model.label
        size: UIKit.BaseText.TextSize.Small
      }
    }
  }
}
