import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12

import "../CustomComponents"
import "../Chart.js" as Chart
import ".."

Rectangle {
  id: root

  property bool isTablet: false

  property ListModel chartModel

  property var categoriesLabels: ["Public", "Private", "No-sync"]

  property var pageChartsObjects
  property int numberOfChartsInCurrentPage: 0

  property var dataSets: []

  property string contentTitle: ""
  property string componentTitle: "Sync Property"
  property string componentType: {
    if(contentTitle === "All Quod")
      return "Quod Type"
    else if(contentTitle === "All Arca")
      return "Arca Type"
    else
      return "File Type"
  }

  radius: s(8)
  width: parent.width
  height: parent.height

  function loadExtendedChart() {
    chartExtended.visible = true
    if(chartExtended.extendedDialog.active === false)
      chartExtended.extendedDialog.active = true
    chartExtended.extendedDialog.sourceComponent = extendedProperyBarChart
    chartExtended.numberOfChartsInCurrentPage = root.numberOfChartsInCurrentPage
    chartExtended.pageChartsObjects = root.pageChartsObjects
    chartExtended.title = root.componentTitle
  }

  function initPropertyBar() {
    var maxValue = 0
    var i
    var currentItem
    dataSets = []

    for(i=0; i < chartModel.count; i++) {
      currentItem = chartModel.get(i)

      maxValue += currentItem.publicValue
      maxValue += currentItem.privateValue
      maxValue += currentItem.noSyncValue
    }

    for(i=0; i < chartModel.count; i++) {
      currentItem = chartModel.get(i)

      var itemData = []
      itemData[0] = Math.round((currentItem.publicValue / maxValue) * 100)
      itemData[1] = Math.round((currentItem.privateValue / maxValue) * 100)
      itemData[2] = Math.round((currentItem.noSyncValue / maxValue) * 100)

      var setMap = {label: currentItem.label, data: itemData, backgroundColor: currentItem.color}
      dataSets[i] = setMap
    }
  }

  onChartModelChanged: {
    console.log("Data Model PropertyBarChart!", chartModel.count)
    if(chartModel.count > 0) {
      initPropertyBar()
    }
  }

  ChartDelegate {
    id: chartViewBar

    title: componentTitle
    categoriesLabels: root.categoriesLabels
    dataSets: root.dataSets
    extendChartMA.onClicked: {
      console.log("Open Extended Property BAR Chart!")
      loadExtendedChart()
    }
  }

  Component {
    id: extendedProperyBarChart

    PropertyBarChartExtended {
      anchors.fill: parent
      numberOfObjects: chartViewBar.maxValue
      dataModel: chartModel
      title: componentTitle
      cbType: componentType
      barsCategoriesLabels: categoriesLabels
      targetComp: chartExtended
      isTablet: root.isTablet
    }
  }
}
