import QtQuick 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

import "../CustomComponents"

Rectangle {
  id: root

  property int numberOfObjects: 0
  property int mouseXpos: 0
  property int mouseYpos: 0

  property string title: "All Quod"
  property string cbType: ""
  property string cbDisplayText: ""

  property var barsCategoriesLabels
  property var dataSets: []
  property var targetComp

  property ListModel dataModel

  property alias barChartViewComp: chartView
  property alias barChartLegendComp: legendRow.legendModel
  property alias barChartComboBoxComp: control

  property bool isMobile: Qt.platform.os === "android" || Qt.platform.os === "ios"
  property bool isTablet: false

  signal newDataSet()

  anchors.fill: parent
  radius: s(8)

  function initAllTypesBarChart() {
    var maxValue = 0
    var i
    var currentItem
    dataSets = []

    for(i=0; i < dataModel.count; i++) {
      currentItem = dataModel.get(i)

      maxValue += currentItem.outOfSync
      maxValue += currentItem.upToDate
    }

    for(i=0; i < dataModel.count; i++) {
      currentItem = dataModel.get(i)

      var itemData = []
      itemData[0] = Math.round((currentItem.outOfSync / maxValue) * 100)
      itemData[1] = Math.round((currentItem.upToDate / maxValue) * 100)

      var setMap = {label: currentItem.label, data: itemData, backgroundColor: currentItem.color}
      dataSets[i] = setMap
    }
    barChartComboBoxComp.comboBoxModel = dataModel
    newDataSet()
    numberOfObjects = maxValue
  }

  function updateBarChart(type) {
    if(dataModel.count > 0) {
      if(type === title) {
        initAllTypesBarChart()
        return
      }

      var maxValue = 0
      var i
      var currentItem

      dataSets = []
      for(i=0; i < dataModel.count; i++) {
        currentItem = dataModel.get(i)
        if(currentItem.label === type) {
          currentItem = dataModel.get(i)

          maxValue += currentItem.outOfSync
          maxValue += currentItem.upToDate

          var itemData = []
          itemData[0] = Math.round((currentItem.outOfSync / maxValue) * 100)
          itemData[1] = Math.round((currentItem.upToDate / maxValue) * 100)

          var setMap = {label: currentItem.label, data: itemData, backgroundColor: currentItem.color}
          dataSets[0] = setMap
        }
      }
      newDataSet()
      numberOfObjects = maxValue
    }
  }

  onDataModelChanged: {
    if(dataModel.count > 0) {
      initAllTypesBarChart()
    }
  }

  Connections {
    target: targetComp

    function onCloseOpenDialog() {
      targetComp.visible = false
      targetComp.extendedDialog.active = false
    }
  }

  ColumnLayout {
    id: mainColumn

    anchors.fill: parent
    spacing: 0

    /*-- ComboBox and Objects Label --*/
    GridLayout {
      Layout.topMargin: root.isMobile ? s(5) : s(30)
      Layout.alignment: Qt.AlignHCenter
      columns: root.isMobile ? 1 : 2
      rows: root.isMobile ? 2 : 1
      rowSpacing: root.isMobile ? s(10) : 0

      UIKit.PanelComboBox {
        id: control

        Layout.preferredWidth: root.isMobile ? mainColumn.width : s(282)
        Layout.preferredHeight: ThemeController.style.defaultInputHeight
        displayText: title
        comboBoxType: cbType
        comboBoxModel: dataModel

        onDisplayTextChanged: {
          console.log("Status Bar ComboBox new item selected:", displayText)
          root.cbDisplayText = displayText
          if((displayText.length > 1) && dataModel)
            updateBarChart(displayText)
        }
      }

      UIKit.BaseText {
        Layout.alignment: Qt.AlignHCenter
        Layout.leftMargin: ThemeController.style.margin.m12
        text: Strings.objects.arg(numberOfObjects)
        font.weight: Font.Bold
      }
    }

    /*-- ChartView and Chart Legend --*/
    Rectangle {
      Layout.fillHeight: true
      Layout.topMargin: ThemeController.style.margin.m20
      Layout.alignment: Qt.AlignCenter
      color: ThemeController.style.transparent
      Layout.preferredWidth: app.isPhone ? parent.width : s(792)

      CustomStackedBarChart {
        id: chartView

        anchors {
          fill: parent
          bottomMargin: legendRow.visible ? s(90) : s(30)
        }
        dataSets: root.dataSets
        barsCategoriesLabels: root.barsCategoriesLabels
      }

      Rectangle {
        id: spacer
        anchors {
          top: chartView.bottom
          topMargin: ThemeController.style.homePageVerticalMargin
          left: chartView.left
          right: chartView.right
          leftMargin: isMobile ? - ThemeController.style.margin.m16 : 0
          rightMargin: isMobile ? -ThemeController.style.margin.m16 : 0
        }
        height: s(1)
        color: ThemeController.style.seashellColor
        visible: control.displayText === title
      }

      CustomChartLegend {
        id: legendRow

        anchors {
          top: spacer.bottom
          topMargin: ThemeController.style.homePageVerticalMargin
          horizontalCenter: parent.horizontalCenter
        }
        width: parent.width
        visible: control.displayText === title
        legendModel: dataModel
      }
    }
  }
}
