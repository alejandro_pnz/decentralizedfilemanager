import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0

import "..//Chart.js" as Chart
import ".."
import "../CustomComponents"

Rectangle {
  implicitWidth: app.isPhone ? s(280) : s(160)
  implicitHeight: childrenRect.height
  property var pieLabels: []
  property var pieData: []
  property var pieBGColor: []
  property ListModel dataModel
  property int mobileChartTopMargin: 32

  UIKit.BaseText {
    id: header
    text: Strings.operatingSystems
    color: ThemeController.style.shuttleColor
    anchors.top: parent.top
    anchors.left: parent.left
  }

  Rectangle {
    id: chartRectangle

    height: s(100)
    width: s(100)
    anchors.left: app.isPhone ? listView.right : parent.left
    anchors.leftMargin: app.isPhone ? ThemeController.style.margin.m16 : 0
    anchors.top: app.isPhone ? parent.top : header.bottom
    anchors.topMargin: app.isPhone ? mobileChartTopMargin : ThemeController.style.margin.m12

    Chart {
      id: chartView
      anchors.top: parent.top
      anchors.left: parent.left
      width: s(100)
      height: s(100)

      chartType: "doughnut"

      chartData: {
        return { datasets: [{
              data: pieData,
              backgroundColor: pieBGColor,
              label: pieLabels
            }],
          labels: pieLabels
        }}

      chartOptions: {
        return {
          maintainAspectRatio: false,
          responsive: true,
          borderWidth: s(5),
          borderColor: "#000",
          cutoutPercentage: 55,
          width: s(100),
          height: s(100),
          layout: {
            padding: {
              left: 0,
              right: 0,
              top: 0,
              bottom: 0
            }
          },
          legend: {
            display: false,
          },
          elements: {
            arc: {
              borderWidth: 0,
              hoverBorderWidth: s(2),
              hoverBorderColor: function(context) {
                var index = context.dataIndex;
                var value = context.dataset.backgroundColor[index];
                return value;
              },
              hoverBackgroundColor: function(context) {
                var index = context.dataIndex;
                var value = context.dataset.backgroundColor[index];
                return value;
              }
            }
          },
          tooltips: {
            enabled: false
          },
          animation: {
            animateRotate: false
          }
        }
      }
    }
  }

  ListView {
    id: listView
    height: contentHeight
    width: s(160)
    anchors.top: app.isPhone ? header.bottom : chartRectangle.bottom
    anchors.topMargin: app.isPhone ? ThemeController.style.margin.m12 : s(18)
    anchors.left: parent.left
    spacing: ThemeController.style.margin.m12
    model: dataModel
    interactive: false

    delegate: Row {
      id: contentRow
      spacing: ThemeController.style.margin.m8
      height: s(24)
      width: parent.width
      Rectangle {
        id: colorRect
        height: s(12)
        width: s(12)
        color: colorValue
        radius: s(1)
        anchors.verticalCenter: parent.verticalCenter
      }

      UIKit.BaseText {
        id: nameLabel
        text: label + ":"
      }

      UIKit.BaseText {
        id: valueLabel
        text: percent + "%"
        font.weight: Font.Bold
      }
    }
  }

  Component.onCompleted: {
    if(dataModel.count !== 0){
      var i
      var maxValue = 0
      var currentItem

      for(i=0; i < dataModel.count; i++) {
        currentItem = dataModel.get(i)
        maxValue += currentItem.value
      }

      for(i=0; i < dataModel.count; i++) {
        currentItem = dataModel.get(i)

        pieLabels[i] = currentItem.label
        pieData[i] = Math.round((currentItem.value / maxValue) * 100)
        dataModel.setProperty(i, "percent", pieData[i])
        pieBGColor[i] = currentItem.colorValue
      }
    }
  }
}
