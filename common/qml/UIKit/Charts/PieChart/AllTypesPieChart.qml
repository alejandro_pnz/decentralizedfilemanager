import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12

import "..//Chart.js" as Chart
import ".."
import "../CustomComponents"

Rectangle {
  id: root

  property ListModel chartModel

  property string contentTitle: ""

  property var pageChartsObjects
  property int numberOfChartsInCurrentPage: 0

  property var pieLabels: []
  property var pieData: []
  property var pieBGColor: []

  radius: s(8)
  width: parent.width
  height: parent.height

  function loadExtendedChart() {
    chartExtended.visible = true
    if(chartExtended.extendedDialog.active === false)
      chartExtended.extendedDialog.active = true
    chartExtended.extendedDialog.sourceComponent = extendedPieChart
    chartExtended.numberOfChartsInCurrentPage = root.numberOfChartsInCurrentPage
    chartExtended.pageChartsObjects = root.pageChartsObjects
    chartExtended.title = root.contentTitle
  }

  onChartModelChanged: {
    console.log("Data Pie Model!", chartModel.count)
    if(chartModel.count > 0) {
      var i
      var maxValue = 0
      var currentItem

      for(i=0; i < chartModel.count; i++) {
        currentItem = chartModel.get(i)
        maxValue += currentItem.value
      }

      for(i=0; i < chartModel.count; i++) {
        currentItem = chartModel.get(i)

        pieLabels[i] = currentItem.label
        pieData[i] = Math.round((currentItem.value / maxValue) * 100)
        pieBGColor[i] = currentItem.color
      }
    }
  }

  ChartDelegate {
    title: contentTitle
    onIconClicked: loadExtendedChart()

    Chart {
      id: chartView
      anchors.fill: parent

      chartType: "doughnut"

      chartData: {
        return { datasets: [{
              data: pieData,
              backgroundColor: pieBGColor,
              label: pieLabels
            }],
          labels: pieLabels
        }}

      chartOptions: {
        return {
          maintainAspectRatio: false,
          responsive: true,
          borderWidth: s(5),
          borderColor: "#000",
          cutoutPercentage: 55,
          width: s(100),
          height: s(100),
          layout: {
            padding: {
              left: 0,
              right: 0,
              top: s(14),
              bottom: s(18)
            }
          },
          legend: {
            display: false,
          },
          elements: {
            arc: {
              borderWidth: 0,
              hoverBorderWidth: s(2),
              hoverBorderColor: function(context) {
                var index = context.dataIndex;
                var value = context.dataset.backgroundColor[index];
                return value;
              },
              hoverBackgroundColor: function(context) {
                var index = context.dataIndex;
                var value = context.dataset.backgroundColor[index];
                return value;
              }
            }
          },
          tooltips: {
            enabled: false
          },
          animation: {
            animateRotate: false
          }
        }
      }
    }
  }

  Component {
    id: extendedPieChart

    AllTypesPieChartExtend {
      anchors.fill: parent
      dataModel: chartModel
      title: contentTitle
      targetComp: chartExtended
    }
  }
}
