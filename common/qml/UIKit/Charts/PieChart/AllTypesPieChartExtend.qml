import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0

import "../CustomComponents"
import "../Chart.js" as Chart
import ".."

Rectangle {
  id: root

  property int numberOfObjects: 0
  property int numberOfPages: 4

  property string title: "All Quod"

  property ListModel dataModel

  property var pieLabels: []
  property var pieData: []
  property var pieBGColor: []
  property var targetComp

  property alias pieChartViewComp: chartView
  property alias pieChartLegendComp: legendRow

  property bool isMobile: Qt.platform.os === "android" || Qt.platform.os === "ios"

  anchors.fill: parent
  radius: s(8)

  onDataModelChanged: {
    if(dataModel.count > 0) {
      var i
      var maxValue = 0
      var currentItem

      for(i=0; i < dataModel.count; i++) {
        currentItem = dataModel.get(i)
        maxValue += currentItem.value
      }

      for(i=0; i < dataModel.count; i++) {
        currentItem = dataModel.get(i)

        pieLabels[i] = currentItem.label
        pieData[i] = Math.round((currentItem.value / maxValue) * 100)
        pieBGColor[i] = currentItem.color
      }
      numberOfObjects = maxValue

      console.log("Chart Model Updated!")
    }
  }

  Connections {
    target: targetComp

    function onCloseOpenDialog() {
      targetComp.visible = false
      targetComp.extendedDialog.active = false
    }
  }

  ColumnLayout {
    id: mainColumn

    anchors.fill: parent
    spacing: 0

    UIKit.BaseText {
      Layout.topMargin: ThemeController.style.margin.m24
      Layout.alignment: Qt.AlignHCenter
      Layout.leftMargin: ThemeController.style.margin.m12
      text: Strings.objects.arg(numberOfObjects)
      font.weight: Font.Bold
    }

    Rectangle {
      Layout.fillHeight: true
      Layout.fillWidth: true
      color: ThemeController.style.transparent

      Chart {
        id: chartView

        anchors {
          fill: parent
          topMargin: ThemeController.style.margin.m24
          bottomMargin: s(90)
        }

        chartType: "doughnut"

        chartData: {return {
            datasets: [{
                data: pieData,
                backgroundColor: pieBGColor,
                label: pieLabels
              }],
            labels: pieLabels
          }}

        chartOptions: {return {
            maintainAspectRatio: false,
            responsive: true,
            borderWidth: s(5),
            borderColor: "#000",
            cutoutPercentage: 55,
            layout: {
              padding: {
                left: 0,
                right: 0,
                top: 0,
                bottom: 0
              }
            },
            legend: {
              display: false,
              position: 'bottom',
              labels: {
                boxWidth: s(12),
              }
            },
            elements: {
              arc: {
                borderWidth: 0,
                hoverBorderWidth: s(2),
                hoverBorderColor: function(context) {
                  var index = context.dataIndex;
                  var value = context.dataset.backgroundColor[index];
                  return value;
                },
                hoverBackgroundColor: function(context) {
                  var index = context.dataIndex;
                  var value = context.dataset.backgroundColor[index];
                  return value;
                }
              }
            },
            tooltips: {
              backgroundColor: "#fff",
              titleFontColor: "#000",
              bodyFontColor: "#000",
              yPadding: s(12),
              xPadding: s(16),
              cornerRadius: s(2),
              titleFontSize: s(14),
              bodyFontSize: s(14),
            }
          }
        }
      }

      Rectangle {
        id: spacer
        anchors {
          top: chartView.bottom
          topMargin: ThemeController.style.homePageVerticalMargin
          horizontalCenter: parent.horizontalCenter
        }
        width: app.isPhone ? parent.width : s(792)
        height: s(1)
        color: ThemeController.style.seashellColor
      }

      CustomChartLegend {
        id: legendRow

        anchors {
          top: spacer.bottom
          topMargin: ThemeController.style.homePageVerticalMargin
          horizontalCenter: parent.horizontalCenter
        }
        width: app.isPhone ? parent.width : s(792)
        legendModel: dataModel
      }
    }
  }
}
