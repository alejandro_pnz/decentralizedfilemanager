import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0

InputField {
  labelText: Strings.password
  textField.text: Strings.password
  placeholderTxt: Strings.password
  isPassPhrase: true
  rightIconSource: "qrc:/duplicate-icon.svg"
  rightIconBtn.style: ThemeController.style.fieldButtonAlt
  clearIconVisible: false
  textField.readOnly: true
  property alias labelRowButton: labelRowButton

  signal historyButtonClicked()

  StyledButton {
    id: labelRowButton
    type: StyledButton.ButtonStyle.Text
    displayMode: StyledButton.DisplayMode.TextRightSideIcon
    text: Strings.history
    icon.source: "qrc:/history.svg"
    anchors.right: parent.right
    onClicked: historyButtonClicked()
  }
}
