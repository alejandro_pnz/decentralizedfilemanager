import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

UIKit.GenericListView {
  id: listView
  spacing: ThemeController.style.margin.m12
  clip: true
  delegate: UIKit.NotificationCard {
    width: listView.width
    contentText: model.text
  }
}
