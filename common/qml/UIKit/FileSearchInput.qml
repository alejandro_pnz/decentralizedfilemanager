import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import Qt5Compat.GraphicalEffects


import com.testintl 1.0

TextField {
  id: inputField
  property color bgColor: ThemeController.style.fileSearchInput.bgColor
  placeholderTextColor: ThemeController.style.fileSearchInput.placeholderTextColor
  font.pixelSize: ThemeController.style.fileSearchInput.fontSize
  color: ThemeController.style.fileSearchInput.textColor
  height: ThemeController.style.fileSearchInput.height
  implicitWidth: s(291)

  leftPadding: s(16)
  rightPadding: s(12)

  property string searchText: app.isMobile ? text.trim(): text.trim()
  property color cursorColor: ThemeController.style.fileSearchInput.cursorColor

  property bool clearIconVisible: inputField.text.length > 0
  property string inputIconSource: "qrc:/remove-circle.svg"

  property alias fieldButton: iconButton

  inputMethodHints: Qt.ImhNoPredictiveText
  placeholderText: Strings.search

  cursorDelegate: Component {
    Rectangle {
      id: cursor
      color: inputField.cursorColor
      height: inputField.font.pixelSize
      implicitWidth: s(1)

      Timer {
        interval: 500
        repeat: true
        running: inputField.cursorVisible
        onRunningChanged: {
          if (!running) {
            cursor.visible = false
          }
        }
        onTriggered: cursor.visible = !cursor.visible
      }
    }
  }

  background: Item {
    Rectangle {
      id: bgRect
      radius: s(16)
      width: parent.width
      height: parent.height
      color: bgColor
      border.width: s(1)
      border.color: ThemeController.style.fileSearchInput.bdColor

      FieldIconButton {
        id: iconButton
        width: s(24)
        height: s(24)
        iconWidth: s(20)
        iconHeight: s(20)
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: ThemeController.style.margin.m16
        iconSrc: inputIconSource
        onClicked: inputField.clear()
        iconColor: ThemeController.style.fileSearchInput.iconColor
        states: []
        visible: true
      }
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      horizontalOffset: ThemeController.style.margin.m4
      verticalOffset: ThemeController.style.margin.m4
      radius: s(20)
      //samples: 2 * radius + 1
      color: ThemeController.style.fileSearchInput.dropShadowColor
      source: bgRect
    }
  }
}
