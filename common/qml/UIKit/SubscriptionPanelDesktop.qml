import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import com.test 1.0
import QtQuick.Layouts 1.12

Rectangle {
  implicitWidth: s(192)
  implicitHeight: s(204)
  signal upgradeClicked()

  property string subscriptionType: Strings.freeTrial
  property int daysLeft: 0

  property alias title: titleComponent
  property alias textItem: textComponent
  property alias button: button
  property alias icon: icon

  Rectangle {
    id: borderCircle
    anchors.bottom: contentRect.top
    anchors.bottomMargin: -height * 0.5
    anchors.horizontalCenter: parent.horizontalCenter
    z: contentRect.z + 1

    width: s(70)
    height: s(70)
    radius: s(100)
    color: ThemeController.style.whiteColor
    border.width: s(1)
    border.color: ThemeController.style.whisperColor

    Rectangle {
      height: parent.height * 0.5
      width: parent.width
      color: ThemeController.style.whiteColor
      anchors.top: parent.top
    }

    Item {
      anchors.centerIn: parent
      width: s(52)
      height: s(52)

      Rectangle {
        id: iconContainer
        anchors.fill: parent
        color: ThemeController.style.governorBayColor
        radius: s(100)

        Rectangle {
          width: s(44)
          height: s(44)
          color: ThemeController.style.blueVioletColor
          radius: s(100)
          anchors.centerIn: parent

          SvgImage {
            id: icon
            source: "qrc:/subscription.svg"
            height: s(24)
            width: s(24)
            anchors.centerIn: parent
          }
        }
      }
    }
  }

  OpacityMask {
    anchors.fill: contentRect
    source: contentRect
    maskSource: borderCircle
    invert: true
  }

  Rectangle {
    id: contentRect
    height: s(174)
    width: parent.width
    anchors.bottom: parent.bottom
    border.width: s(1)
    border.color: ThemeController.style.whisperColor
    color: ThemeController.style.whiteColor
    radius: s(8)

    ColumnLayout {
      anchors {
        top: parent.top
        topMargin: s(50)
        bottom: parent.bottom
        bottomMargin: ThemeController.style.margin.m16
        left: parent.left
        right: parent.right
        leftMargin: ThemeController.style.margin.m16
        rightMargin: ThemeController.style.margin.m16
      }

      spacing: ThemeController.style.margin.m4

      BaseText {
        id: titleComponent
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter
        text: Strings.subscription.arg(subscriptionType)
      }

      BaseText {
        id: textComponent
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter
        text: Strings.endsIn.arg(daysLeft)
        size: BaseText.TextSize.Small
      }

      UIKit.StyledButton {
        id: button
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter
        Layout.topMargin: ThemeController.style.margin.m16
        text: Strings.upgrade
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        type: UIKit.StyledButton.ButtonStyle.Primary
        onClicked: upgradeClicked()
      }
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: contentRect
    source: contentRect
    color: Qt.rgba(0, 0, 0, 0.04)
    radius: s(20)
    verticalOffset: ThemeController.style.margin.m4
    horizontalOffset: ThemeController.style.margin.m4
    //samples: radius * 2 + 1
  }
}
