import QtQuick 2.15
import AppStyles 1.0

LargeIconButton {
  id: control
  property bool indicatorVisible: false
  property alias indicatorComp: indicatorCom

  icon.source: "qrc:/filters.svg"
  Rectangle {
    id: indicatorCom
    width: s(10)
    height: s(10)
    border.color: ThemeController.style.buttonIndicator.bdColor
    border.width: s(2)
    radius: width * 0.5
    visible: indicatorVisible
    color: ThemeController.style.buttonIndicator.bgColor
    y: 0
    x: control.width - width
  }
}
