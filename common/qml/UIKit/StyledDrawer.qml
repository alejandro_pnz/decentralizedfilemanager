import QtQuick 2.15
import UIFragments 1.0
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import QtQuick.Controls 2.15
import AppStyles 1.0

Drawer {
  id: drawer
  width: parent.width
  edge: Qt.BottomEdge
  verticalPadding: ThemeController.style.margin.m24
  interactive: visible
  height: fullHeight ? drawerFullHeight : contentItemHeight + header.height + content.anchors.topMargin
  property bool fullHeightSwitcher: true
  property bool fullHeight: false
  property int drawerFullHeight: 0
  property int drawerPartHeight: 0

  // Keeps the maximum available height for content
  readonly property int maximumContentHeight: height - header.height - content.anchors.topMargin - verticalPadding

  property int contentItemHeight: 0
  property bool closeButtonVisible: false
  property bool headerVisible: false
  property bool headerCloseButtonVisible: false
  property bool headerSeparatorVisible: true
  property alias content: content
  default property alias children: content.children
  property alias header: headerContent.children
  property alias states: stateItem.states
  property alias state: stateItem.state
  property alias headerContent: header

  function closeDrawer() {
    drawer.fullHeight = false
    drawer.close()
  }

  Item {
    id: stateItem
  }

  background: Item {
    implicitHeight: drawer.height
    Rectangle {
      anchors {
        top: parent.top
        left: parent.left
        right: parent.right
      }
      height: ThemeController.style.defaultInputHeight
      radius: fullHeight ? 0 : s(24)
    }

    Rectangle {
      anchors {
        top: parent.top
        left: parent.left
        right: parent.right
        bottom: parent.bottom
        topMargin: ThemeController.style.margin.m24
      }
    }

    UIKit.LargeIconButton {
      anchors.top: parent.top
      anchors.topMargin: ThemeController.style.margin.m20
      anchors.right: parent.right
      anchors.rightMargin: ThemeController.style.margin.m16
      icon.source: "qrc:/close.svg"
      visible: closeButtonVisible
      enabled: visible
      onClicked: {
        closeDrawer()
      }
    }
  }

  Item {
    id: header
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
      leftMargin: ThemeController.style.pageHorizontalMargin
      rightMargin: ThemeController.style.pageHorizontalMargin
      topMargin: fullHeight ? safeMargins.top : 0
    }
    height: headerVisible ? s(fullHeight ? 84 : 90) : 0
    visible: headerVisible

    // Swipe indicator
    Rectangle {
      anchors.top: parent.top
      anchors.topMargin: ThemeController.style.margin.m12
      anchors.horizontalCenter: parent.horizontalCenter
      height: s(6)
      width: s(35)
      radius: width * 0.5
      color: ThemeController.style.mercuryColor
      visible: !fullHeight
    }

    RowLayout {
      id: headerContent
      height: s(32)
      anchors {
        bottom: parent.bottom
        left: parent.left
        right: parent.right
        bottomMargin: ThemeController.style.margin.m16
      }
      spacing: ThemeController.style.margin.m16
      UIKit.LargeIconButton {
        id: headerCloseBtn

        icon.source: "qrc:/close.svg"
        visible: headerCloseButtonVisible
        onClicked: {
          closeDrawer()
        }
      }
    }

    Rectangle {
      anchors {
        bottom: parent.bottom
        left: parent.left
        right: parent.right
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
      }
      color: ThemeController.style.mercuryColor
      height: s(1)
      visible: headerSeparatorVisible
    }
  }

  Item {
    id: content
    anchors {
      top: headerVisible ? header.bottom : parent.top
      left: parent.left
      right: parent.right
      topMargin: headerVisible ? ThemeController.style.margin.m12 : ThemeController.style.margin.m24
      leftMargin: ThemeController.style.pageHorizontalMargin
      rightMargin: ThemeController.style.pageHorizontalMargin
      bottom: parent.bottom
    }
  }

  Overlay.modal: Rectangle {
    color: Qt.rgba(0,0,0,0.4)
  }

  Behavior on height {
    NumberAnimation { duration: 150 }
  }
}
