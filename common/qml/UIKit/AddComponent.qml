import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

Item {
  implicitWidth: parent.width
  implicitHeight: isMobile ? s(56) : s(72)

  property string buttonText: ""
  property bool isMobile: Qt.platform.os === "android" || Qt.platform.os === "ios"
  signal addButtonClicked()
  property alias addButton: addButton

  Rectangle {
    anchors.top: parent.top
    width: parent.width
    color: ThemeController.style.mercuryColor
    height: s(1)
  }

  StyledButton {
    id: addButton
    type: StyledButton.ButtonStyle.Text
    displayMode: StyledButton.DisplayMode.TextRightSideIcon
    text: buttonText
    icon.source: "qrc:/add.svg"
    anchors.verticalCenter: parent.verticalCenter
    anchors.left: parent.left
    onClicked: addButtonClicked()
  }

  Rectangle {
    anchors.bottom: parent.bottom
    width: parent.width
    color: ThemeController.style.mercuryColor
    height: s(1)
  }
}
