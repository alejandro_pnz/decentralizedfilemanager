import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

Item {
  implicitWidth: parent.width
  implicitHeight: s(60)

  property alias iconObject: iconObject
  property alias titleItem: titleItem
  property alias contentItem: contentItem
  property alias slider: slider

  RowLayout {
    spacing: ThemeController.style.margin.m12
    anchors.fill: parent
    ObjectAvatar {
      id: iconObject
      color: ThemeController.style.passwordLengthSlider.bgColor
      iconSize: s(24)
      Layout.preferredHeight: s(44)
      Layout.preferredWidth: s(44)
      iconSrc: "qrc:/ruler.svg"
    }

    ColumnLayout {
      Layout.fillWidth: true

      RowLayout {
        Layout.fillWidth: true

        UIKit.BaseText {
          id: titleItem
          text: Strings.length
          font.weight: Font.DemiBold
        }

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true
        }

        UIKit.BaseText {
          id: contentItem
          text: Strings.characters.arg(slider.value)
          color: ThemeController.style.passwordLengthSlider.contentFontColor
        }
      }

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
      }

      StyledSlider {
        id: slider
        from: 8
        to: 128
        value: 16
        Layout.fillWidth: true
      }
    }
  }
}
