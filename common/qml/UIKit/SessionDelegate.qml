import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import com.test 1.0

AbstractButton {
  id: control

  implicitHeight: s(84)
  implicitWidth: parent.width

  property string title: ""
  property string ipAddress: ""
  property string date: ""
  property string buttonText: Strings.terminate
  property bool actionButton: true

  property var config: ThemeController.style.objectDelegate

  property alias bgRect: bgRect
  property alias titleComponent: titleComponent
  property alias ipComponent: ipComponent
  property alias dateComponent: dateComponent
  property alias dropShadow: dropShadow
  property alias actionButtonComponent: actionButtonComponent

  signal actionButtonClicked()
  signal delegateClicked()

  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
  topPadding: s(18)
  bottomPadding: s(18)

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      color: config.bgColor
      radius: s(8)
      border {
        color: config.bdColor
        width: s(1)
      }
      visible: !dropShadow.visible
    }

    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      source: bgRect
      color: config.dropShadowColor05
      radius: s(10)
      verticalOffset: s(2)
      //samples: radius * 2 + 1
    }
  }

  contentItem: RowLayout {
    spacing: ThemeController.style.margin.m12

    Column {
      spacing: ThemeController.style.margin.m8
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignVCenter

      BaseText {
        id: titleComponent
        Layout.fillWidth: true
        elide: Text.ElideRight
        font.weight: Font.DemiBold
        text: control.title
        color: config.titleTextColor
      }

      RowLayout {
        spacing: ThemeController.style.margin.m4
        Layout.fillWidth: true

        BaseText {
          id: ipComponent
          text: control.ipAddress
          size: BaseText.TextSize.Small
          color: config.textColor
          visible: text.length > 0
        }

        Rectangle {
          color: config.textColor
          Layout.preferredWidth: s(3)
          Layout.preferredHeight: s(3)
          radius: s(100)
          visible: ipAddress !== "" && date !== ""
          Layout.alignment: Qt.AlignVCenter
        }

        BaseText {
          id: dateComponent
          text: control.date
          size: BaseText.TextSize.Small
          color: config.textColor
          visible: text.length > 0
        }

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true
        }
      }
    }

    StyledButton {
      id: actionButtonComponent
      type: StyledButton.ButtonStyle.Text
      displayMode: StyledButton.DisplayMode.TextRightSideIcon
      text: buttonText
      icon.source: "qrc:/icon-close.svg"
      visible: control.actionButton
      onClicked: actionButtonClicked()
    }
  }
}

