import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12
import QtQml.Models 2.12

AbstractButton {
  id: control

  implicitHeight: s(84)
  implicitWidth: s(343)

  property ItemSelectionModel selectionModel: null
  property var delegateModel: null
  property bool selectionMode: false
  property bool selected: false

  property bool checkboxOnTheLeft: true
  property string title: ""
  property var config: ThemeController.style.objectDelegate

  // Switch visibility of title icon
  property bool titleIcon: false
  property string titleIconSource: "qrc:/lock-filled.svg"

  // Switch visibility of action button
  property bool actionButton: false
  property string actionButtonIconSource: "qrc:/more-2.svg"

  // This method will be invoked on press and hold event
  property var pressAndHoldHandler: handlePressAndHold

  property var progress: null

  property alias avatar: objectAvatar
  property alias bgRect: bgRect
  readonly property alias titleComponent: titleComponent
  readonly property alias textComponent: textComponent
  property alias dropShadow: dropShadow
  readonly property alias rightCheckbox: rightCheckBox
  readonly property alias leftCheckbox: checkBox
  readonly property alias actionButtonComponent: actionButtonComponent

  signal actionButtonClicked()
  signal textLinkActivated(var link)
  signal delegateClicked()

  checked: selected
  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
  topPadding: ThemeController.style.margin.m12
  bottomPadding: ThemeController.style.margin.m12

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      color: control.pressed ? config.bgColorPressed : config.bgColor
      radius: s(8)
      border {
        color: control.selected ? config.bdColorSelected : config.bdColor
        width: s(control.selected ? 2 : 1)
      }
      visible: !dropShadow.visible
    }
    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      source: bgRect
      transparentBorder: true
      color: control.hovered || control.selected ? config.dropShadowColor05 : config.dropShadowColor04
      visible: !control.pressed
      spread: 0.2
      radius: control.hovered ? s(30) : control.selected ? s(16) : s(10)
      verticalOffset: s(2)
    }
  }

  contentItem: RowLayout {
    spacing: ThemeController.style.margin.m12
    CheckIndicator {
      id: checkBox
      visible: control.checkboxOnTheLeft && control.checkable
      Layout.alignment: Qt.AlignVCenter
      checked: control.selected
      implicitHeight: s(24)
      implicitWidth: s(24)
      MouseArea {
        anchors.fill: parent
        onClicked: {
          if (selectionMode)
            control.clicked()
          else
            check()
        }
      }
    }

    ObjectAvatar {
      id: objectAvatar
      Layout.alignment: Qt.AlignVCenter
      size: ObjectAvatar.Size.Small

      CircularProgressBar {
        size: objectAvatar.width
        visible: progress != null
        value: {
          if (progress == null) {
            return 0
          }
          return progress.value / progress.maximum
        }
        secondaryColor: ThemeController.style.circularProgressBar.secondaryColor
        primaryColor: ThemeController.style.circularProgressBar.primaryColor
      }
    }

    Column {
      spacing: ThemeController.style.margin.m8
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignVCenter
      RowLayout {
        spacing: ThemeController.style.margin.m8
        width: parent.width
        ColoredImage {
          id: lockIcon
          icon.width: s(16)
          icon.height: s(16)
          visible: control.titleIcon
          source: control.titleIconSource
          color: config.titleIconColor
        }
        BaseText {
          id: titleComponent
          Layout.fillWidth: true
          elide: Text.ElideRight
          font.weight: Font.DemiBold
          text: control.title
          color: config.titleTextColor
        }
      }
      BaseText {
        id: textComponent
        width: parent.width
        elide: Text.ElideRight
        text: control.text
        size: BaseText.TextSize.Small
        color: config.textColor
        visible: text.length > 0
        onLinkActivated: textLinkActivated(link)
        textFormat: Text.StyledText
      }
    }

    CheckIndicator {
      id: rightCheckBox
      visible: !control.checkboxOnTheLeft && control.checkable
      Layout.alignment: Qt.AlignVCenter
      checked: control.selected
      implicitHeight: s(32)
      implicitWidth: s(32)
      checkIcon.width: s(18)
      checkIcon.height: s(15)
      MouseArea {
        anchors.fill: parent
        onClicked: {
          if (selectionMode)
            control.clicked()
          else
            check()
        }
      }
    }

    LargeIconButton {
      id: actionButtonComponent
      buttonStyle: config.actionButton
      visible: control.actionButton && !rightCheckBox.visible
      icon.source: control.actionButtonIconSource
      onClicked: actionButtonClicked()
      onPressAndHold: control.pressAndHold()
    }
  }

  function check() {
    if (delegateModel){
      selectionModel.select(delegateModel.mapToSource(delegateModel.index(index, 0)), ItemSelectionModel.Toggle)
    }
  }

  function handlePressAndHold() {
    // Press&hold is supported only on mobiles
    if (app.isMobile) {
      check()
    }
  }

  onPressAndHold: pressAndHoldHandler()

  onClicked: {
    console.log("Clicked", selectionMode)
    if (selectionMode) {
      check()
    } else {
      delegateClicked()
    }
  }

  Connections {
    target: selectionModel
    function onSelectionChanged() {
      control.selected = selectionModel.isSelected(delegateModel.mapToSource(delegateModel.index(index, 0)))
    }
  }
}

