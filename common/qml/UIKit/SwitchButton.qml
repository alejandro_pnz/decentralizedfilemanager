import QtQuick 2.15
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import AppStyles 1.0
import UIKit 1.0 as UIKit

Switch {
  id: root
  implicitWidth: s(52)
  implicitHeight: s(32)

  indicator: Rectangle {
    width: root.implicitWidth
    height: root.implicitHeight
    radius: s(20)
    color: root.checked ? ThemeController.style.slatePurpleColor :
                          ThemeController.style.whisperColor

    Rectangle {
      id: middleRect
      x: root.checked ? parent.width - width - ThemeController.style.margin.m4 : ThemeController.style.margin.m4
      width: s(24)
      height: s(24)
      radius: s(100)
      anchors.verticalCenter: parent.verticalCenter
      color: ThemeController.style.whiteColor
    }

    DropShadow {
      id: dropShadow
      anchors.fill: middleRect
      horizontalOffset: 0
      verticalOffset: 0
      radius: s(30)
      //samples: s(61)
      color: Qt.rgba(0, 0, 0, 0.05)
      source: middleRect
    }
  }
}
