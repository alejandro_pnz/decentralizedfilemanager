import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0

import com.testintl 1.0

InputField {
  labelText: Strings.authenticator
  placeholderTxt: Strings.pasteAuthenticatorKey
  isPassPhrase: true
  clearIconVisible: false
  property alias labelRowButton: labelRowButton
  property alias labelRowIcon: icon.icon

  signal scanQrCode()

  Item {
    anchors.fill: parent

    BigTipButton {
      id: icon
      icon.source: "qrc:/tips-icon.svg"
      anchors.left: parent.left
      anchors.leftMargin: ThemeController.style.margin.m4
      anchors.verticalCenter: parent.verticalCenter
      tipTitle: Strings.howDoesAuthenticatorWork
      tipContent: Strings.configuringAuthenticator
      imageSource: "qrc:/illustrations/u8.png"
      showDontShowAgain: false
      clickHandler: function() {
        if (app.isPhone && !app.isTablet) {
          homeManager.openPage(Pages.AuthenticatorInfoPopupPage)
        }
        else {
          openTip()
        }
      }
    }

    StyledButton {
      id: labelRowButton
      type: StyledButton.ButtonStyle.Text
      displayMode: StyledButton.DisplayMode.TextOnly
      text: Strings.scanQr
      visible: app.isMobile
      anchors.right: parent.right
      anchors.verticalCenter: parent.verticalCenter
      onClicked: scanQrCode()
    }
  }
}
