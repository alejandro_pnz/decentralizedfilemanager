import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0

Rectangle {
    id: indicator
    property bool errorState: false
    property bool checked: false

    implicitWidth: ThemeController.style.checkBox.width
    implicitHeight: ThemeController.style.checkBox.height
    radius: s(4)
    border.color: errorState ? ThemeController.style.checkBox.errorBdColor
                             : ThemeController.style.checkBox.uncheckedBdColor
    border.width: s(1.5)
    color: ThemeController.style.checkBox.defaultBgColor
    property alias checkedRec: checkedRec
    property alias checkIcon: checkIcon
    
    Rectangle {
        id: checkedRec
        visible: indicator.checked
        color: ThemeController.style.checkBox.checkedBgColor
        radius: s(4)
        anchors.fill: parent
        
        SvgImage{
            id: checkIcon
            source: "qrc:/check-icon.svg"
            height: s(12)
            width: s(14)
            anchors.centerIn: parent
        }
    }
}
