import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

Item {
  id: dateInput
  property string labelText: Strings.dateOfBirth
  property bool inputRequired: false
  property string placeholderTxt: "dd/mm/yyyy"
  property bool isPassPhrase: false
  property bool leftEyeIcon: true
  property string leftIconSource: leftEyeIcon ? (textItem.echoMode === TextInput.Normal
                                 ? "qrc:/view-off.svg" : "qrc:/dark-eye.svg") : ""
  property real borderWidth: s(1.0)
  property color borderColor: ThemeController.style.textInput.defaultBorderColor
  property bool showIndicator: true
  property string pickerTitle: ""

  property alias comboBox: combobox
  property alias textField: textItem
  property alias lbl: label
  property alias lblRequired: labelRequired
  property alias leftIconBtn: leftIconButton
  property alias pickerIndicator: indicator
  property alias picker: picker

  property bool isMobile: Qt.platform.os === "android" || Qt.platform.os === "ios"

  signal tabPressed()
  signal backtabPressed()
  signal enterPressed()

//  property var objectsModel

  function openPicker() {
    // Forcing active focus on textField make it possible to navigate via tab/backtab
    textField.forceActiveFocus()
    console.log("OPEN PICKER", textField.activeFocus)
    if (!app.isPhone) {
      picker.open()
    }
  }

  implicitHeight: childrenRect.height
  implicitWidth: parent.width
  state: 'default'

  function leftButtonClicked(){
    if(leftEyeIcon){
      isPassPhrase = !isPassPhrase
    }
  }

  ColumnLayout {
    width: parent.width
    spacing: ThemeController.style.margin.m8

    RowLayout {
      spacing: ThemeController.style.margin.m4
      width: parent.width

      BaseText {
        id: label
        text: labelText
        font.weight: Font.DemiBold
        color: ThemeController.style.textInput.labelTextColor
      }

      BaseText {
        id: labelRequired
        text: "*"
        color: ThemeController.style.textInput.asteriskColor
        visible: inputRequired
      }

      Item {
        Layout.fillWidth: true
        Layout.preferredHeight: ThemeController.style.margin.m24
      }
    }

    ComboBox {
      id: combobox

      implicitWidth: parent.width
      implicitHeight: ThemeController.style.defaultInputHeight

      indicator: Item {
      }

      contentItem: RowLayout {
        spacing: ThemeController.style.margin.m4
        width: combobox.width
        height: combobox.height
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: ThemeController.style.margin.m12
        anchors.verticalCenter: parent.verticalCenter

        TextField {
          id: textItem

          text: combobox.displayText
          color: placeholderTextColor
          verticalAlignment: Text.AlignVCenter
          placeholderText: placeholderTxt
          placeholderTextColor: ThemeController.style.textInput.placeholderTextColor
          font.pixelSize: ThemeController.style.textInput.fontSize
          readOnly: true
          echoMode: isPassPhrase ? TextInput.Password : TextInput.Normal
          Layout.fillWidth: true

          onAccepted: {
            if (picker.visible) {
              picker.close()
            }
            enterPressed()
          }

          Keys.onTabPressed:  {
            if (picker.visible) {
              picker.close()
            }
            tabPressed()
          }

          Keys.onBacktabPressed: {
            if (picker.visible) {
              picker.close()
            }
            backtabPressed()
          }

          onPressed : {
            if(!picker.visible) {
              picker.open()
            }
            else {
              picker.close()
            }
          }

          background: Item {
          }
        }

        FieldIconButton {
          id: leftIconButton
          width: s(16)
          height: s(16)
          Layout.alignment: Qt.AlignVCenter
          iconSrc: leftIconSource
          active: true
          onClicked: leftButtonClicked()
        }

        Rectangle {
          id: separator
          height: s(24)
          width: s(1)
          color: ThemeController.style.seashellColor
          Layout.alignment: Qt.AlignVCenter
          visible: leftIconButton.iconSrc !== "" && showIndicator
        }

        UIKit.SvgImage {
          id: indicator
          width: s(16)
          height: s(16)
          Layout.alignment: Qt.AlignVCenter
          source: "qrc:/chevron-down.svg"
          visible: showIndicator
        }
      }

      background: Rectangle {
        radius: s(4)
        width: combobox.width
        height: combobox.height
        color: "#FFFFFF"
        border.color: borderColor
        border.width: borderWidth
      }

      popup: CalendarPopup {
        id: picker
        dateInput: dateInput
        y: combobox.height + ThemeController.style.margin.m8

        Item{
          Connections {
            target: picker.calendar
            function onDateChoosen(d) {
              comboBox.displayText = d.toLocaleString(picker.calendar.locale, "dd/MM/yyyy")
              picker.close()
            }
          }
        }
      }
    }
  }

  states: [
    State {
      name: "active"
      when: picker.visible || textField.activeFocus
      PropertyChanges {
        target: dateInput
        borderColor: ThemeController.style.textInput.activeBorderColor
        borderWidth: s(1.5)
      }
    },
    State {
      name: "default"
      when: !combobox.hovered && !picker.visible
      PropertyChanges {
        target: dateInput
        borderColor: ThemeController.style.textInput.defaultBorderColor
        borderWidth: s(1)
      }
    },

    State {
      name: "hovered"
      when: combobox.hovered && !picker.visible
      PropertyChanges {
        target: dateInput
        borderColor: ThemeController.style.textInput.hoveredBorderColor
        borderWidth: s(1)
      }
    }
  ]
}
