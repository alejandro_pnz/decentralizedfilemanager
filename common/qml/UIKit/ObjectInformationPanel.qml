import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import QtQuick.Controls 2.15

ColumnLayout {
  spacing: ThemeController.style.margin.m16

  property string uid: ""
  property string lastModifiedDate: ""
  property string ownerName: ""

  property alias upperText: upperText
  property alias avatarText: avatarText
  property alias bottomText: bottomText

  UIKit.BaseText {
    id: upperText
    text: Strings.lastModifiedArg.arg(lastModifiedDate)
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
  }

  Item {
    Layout.preferredWidth: childrenRect.width
    Layout.preferredHeight: avatar.height
    Layout.alignment: Qt.AlignHCenter
    Layout.topMargin: -ThemeController.style.margin.m4

    UIKit.Avatar {
      id: avatar
      text: ownerName
      size: UIKit.Avatar.Size.Small
    }

    UIKit.BaseText {
      id: avatarText
      text: ownerName
      anchors.left: avatar.right
      anchors.leftMargin: ThemeController.style.margin.m12
      anchors.verticalCenter: avatar.verticalCenter
      horizontalAlignment: Qt.AlignHCenter
    }
  }

  UIKit.BaseText {
    id: bottomText
    text: Strings.uniqueId.arg(uid)
    Layout.alignment: Qt.AlignHCenter
    horizontalAlignment: Qt.AlignHCenter
    color: ThemeController.style.manateeColor
  }
}
