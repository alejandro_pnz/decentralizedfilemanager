import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0

Item {
  default property alias children: content.children
  property alias title: titleComponent.text
  property alias rightButton: rightButton
  property alias leftButton: leftButton
  property alias searchButton: searchButton
  property alias subtitle: subtitleComponent.text
  property int marginLeft: 0
  signal searchButtonClicked()
  signal rightButtonClicked()
  signal leftButtonClicked()

  RowLayout {
    id: headerRow
    spacing: 0
    anchors {
      right: parent.right
      left: parent.left
      verticalCenter: parent.verticalCenter
    }

    UIKit.LargeIconButton {
      id: leftButton
      icon.source: "qrc:/chevron-back.svg"
      height: s(32)
      width: s(32)
      onClicked: leftButtonClicked()
      visible: false
    }

    Item {
      id: content
      Layout.leftMargin: s(leftButton.visible ? marginLeft - 56 : marginLeft - 24)
      Layout.fillWidth: true
      Layout.fillHeight: true

      UIKit.BaseText {
        id: titleComponent
        anchors.left: parent.left
        text: Strings.home
        font.weight: Font.DemiBold
        horizontalAlignment: Text.AlignHCenter
        anchors.verticalCenter: parent.verticalCenter
        size: BaseText.TextSize.H3
        visible: text.length > 0
      }

      UIKit.BaseText {
        id: subtitleComponent
        anchors.left: titleComponent.right
        anchors.leftMargin: ThemeController.style.margin.m16
        horizontalAlignment: Text.AlignHCenter
        anchors.verticalCenter: parent.verticalCenter
        color: ThemeController.style.shuttleColor
        visible: text.length > 0
      }
    }

    UIKit.LargeIconButton {
      id: searchButton
      visible: false
      icon.source: "qrc:/search.svg"
      iconHeight: s(24)
      iconWidth: s(24)
      height: s(32)
      width: s(32)
      onClicked: searchButtonClicked()
    }

    Item {
      width: s(16)
      height: s(1)
    }

    UIKit.LargeIconButton {
      id: rightButton
      icon.source: "qrc:/bell-icon.svg"
      iconHeight: s(24)
      iconWidth: s(24)
      height: s(32)
      width: s(32)
      onClicked: rightButtonClicked()
    }
  }
}
