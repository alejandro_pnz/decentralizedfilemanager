import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12

AbstractButton {
  id: control

  topPadding: s(8)
  bottomPadding: s(8)
  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16

  signal closeButtonClicked(int fenum)

  property string details: ""

  property int filterEnum: -1

  readonly property alias titleComponent: text
  readonly property alias bgRect: bg
  readonly property alias closeBtn: closeBtn
  property alias detailsComponent: detailsCom

  property var style: ThemeController.style.desktopSearchFilterDelegate
  background: Item {
    Rectangle {
      id: bg
      anchors.fill: parent
      color: control.pressed ? control.style.bgColorPressed : control.style.bgColor
      border.color: control.pressed ? control.style.bdColorPressed : control.style.bdColor
      border.width: s(1)
      radius: s(4)
      visible: !control.pressed
    }

    DropShadow {
      visible: control.pressed
      source: bg
      anchors.fill: parent
      color: control.style.dropShadowColor
    }
  }

  contentItem: Row {
    spacing: ThemeController.style.margin.m8
    ColoredImage {
      id: icon
      source: control.icon.source
      icon.width: s(16)
      icon.height: s(16)
      anchors.verticalCenter: parent.verticalCenter
      color: control.pressed ? control.style.textColorPressed : control.style.textColor
    }
    Row {
      spacing: ThemeController.style.margin.m4
      BaseText {
        id: text
        color: control.pressed ? control.style.textColorPressed : control.style.textColor
        text: control.text
        anchors.verticalCenter: parent.verticalCenter
      }

      ColoredImage {
        id: dot
        icon.width: s(4)
        icon.height: s(4)
        source: "qrc:/ellipse-77.svg"
        color: control.pressed ? control.style.textColorPressed : control.style.textColor
        anchors.verticalCenter: parent.verticalCenter
        visible: detailsCom.visible
      }

      BaseText {
        id: detailsCom
        color: control.pressed ? control.style.textColorPressed : control.style.textColor
        text: control.details
        anchors.verticalCenter: parent.verticalCenter
        visible: detailsCom.text.length > 0
      }
    }


    ColoredImage {
      id: closeBtn
      icon.width: s(16)
      icon.height: s(16)
      source: "qrc:/16px/close.svg"
      color: control.pressed ? control.style.textColorPressed : control.style.textColor
      anchors.verticalCenter: parent.verticalCenter
      visible: false
      MouseArea {
        anchors.fill: parent
        onClicked: closeButtonClicked(filterEnum)
      }
    }
  }
}

