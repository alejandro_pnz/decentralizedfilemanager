import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit

Button {
  id: button

  property string value: ""
  property bool valueVisible: false

  implicitHeight: ThemeController.style.defaultInputHeight
  state: 'default'
  checkable: true
  hoverEnabled: true
  property alias iconObject: icon
  property string iconDomain
  property alias nameText: name
  property alias selectionIndicator: selectedIndicator
  property alias valueContainer: numberContainer
  property alias valueText: numberTxt
  property alias bg: bgRect

  property bool shrinked: false

  background: Item {
     Rectangle {
      id : bgRect
      anchors.fill: parent
    }
  }

  contentItem:  RowLayout {
    anchors.fill: parent
    spacing: ThemeController.style.margin.m20

    Rectangle {
      id: selectedIndicator
      Layout.preferredHeight: parent.height
      Layout.preferredWidth: ThemeController.style.margin.m4
    }

    ColoredImage {
      id: icon
      width: s(24)
      height: s(24)
      //Layout.preferredHeight: s(24)
      //Layout.preferredWidth: s(24)
      Layout.alignment: Qt.AlignVCenter
      icon.fillMode: Image.PreserveAspectFit
      source: button.icon.source
    }

    UIKit.BaseText {
      id: name
      text: button.text
      Layout.leftMargin: ThemeController.style.margin.m4
      Layout.alignment: Qt.AlignVCenter
      visible: !shrinked
    }

    Item {
//      visible: !shrinked
      Layout.fillHeight: true
      Layout.fillWidth: true
    }

    Rectangle {
      id: numberContainer
      Layout.preferredHeight: s(24)
      Layout.preferredWidth: s(51)
      Layout.rightMargin: ThemeController.style.margin.m24
      Layout.alignment: Qt.AlignVCenter
      color: ThemeController.style.mercuryColor
      radius: s(100)
      visible: valueVisible && !shrinked

      UIKit.BaseText {
        id: numberTxt
        anchors.centerIn: parent
        text: value
      }
    }
  }

  states: [
    State {
      name: "selected"
      when: checked
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.seashellColor
      }
      PropertyChanges {
        target: selectedIndicator
        color: ThemeController.style.whiteSmokeColor
      }
      PropertyChanges {
        target: icon
        source: "qrc:/" + button.iconDomain + "-selected.svg"
      }
      PropertyChanges {
        target: name
        color: ThemeController.style.font.defaultColor
      }
    },
    State {
      name: "hovered"
      when: hovered && !checked
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.seashellColor
      }
      PropertyChanges {
        target: selectedIndicator
        color: ThemeController.style.whiteSmokeColor
      }
      PropertyChanges {
        target: icon
        source: "qrc:/" + button.iconDomain + "-selected.svg"
      }
      PropertyChanges {
        target: name
        color: ThemeController.style.font.defaultColor
      }
    },
    State {
      name: "default"
      when: !checked && !hovered
      PropertyChanges {
        target: bgRect
        color: ThemeController.style.whiteSmokeColor
      }
      PropertyChanges {
        target: icon
        color: ThemeController.style.shuttleColor
        source: "qrc:/" + button.iconDomain + "-default.svg"
      }
      PropertyChanges {
        target: name
        color: ThemeController.style.charadeColor
      }
    }
  ]
}
