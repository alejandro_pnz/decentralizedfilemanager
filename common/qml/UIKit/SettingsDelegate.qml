import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import com.test 1.0
import UIKit 1.0 as UIKit

Button {
  id: control
  implicitWidth: parent.width
  implicitHeight: contentColumn.height

  property string title: ""
  property string contentText: ""
  property string buttonText: ""
  property bool showTextButton: false

  property alias titleItem: titleItem
  property alias contentLabel: contentLabel
  property alias iconButton: iconButton

  background: Rectangle {
    id : bgRect
    color: control.pressed ? ThemeController.style.slatePurpleColorOpacity10
                           : control.hovered ? ThemeController.style.whiteSmokeColor
                                             : ThemeController.style.whiteColor
  }

  ColumnLayout {
    id: contentColumn
    width: parent.width
    spacing: ThemeController.style.margin.m4

    UIKit.BaseText {
      id: titleItem
      Layout.topMargin: s(11)
      text: title
      font.weight: Font.DemiBold
      Layout.preferredWidth: contentColumn.width - s(40)
    }

    UIKit.BaseText {
      id: contentLabel
      text: contentText
      Layout.preferredWidth: contentColumn.width - s(40)
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      size: BaseText.TextSize.Small
      color: ThemeController.style.shuttleColor
    }

    Rectangle {
      Layout.topMargin: ThemeController.style.margin.m8
      Layout.fillWidth: true
      height: s(1)
      color: ThemeController.style.mercuryColor
    }
  }

  ColoredImage {
    id: iconButton
    anchors.right: parent.right
    anchors.verticalCenter: contentColumn.verticalCenter
    icon.source: "qrc:/chevron-forward.svg"
    visible: !showTextButton
  }

  UIKit.StyledButton {
    id: textButton
    anchors.right: parent.right
    anchors.verticalCenter: contentColumn.verticalCenter
    text: buttonText
    visible: showTextButton
    displayMode: UIKit.StyledButton.DisplayMode.TextOnly
    type: UIKit.StyledButton.ButtonStyle.Text
  }
}
