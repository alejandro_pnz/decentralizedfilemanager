import QtQuick 2.15
import AppStyles 1.0
import QtQuick.Controls 2.15
import Qt5Compat.GraphicalEffects

import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import com.test 1.0

AbstractButton {
  id: control

  implicitHeight: s(84)
  checked: selected

  property ItemSelectionModel selectionModel: null
  property var objectsModel: null
  property bool selectionMode: false
  property bool selected: false

  property bool checkboxOnTheLeft: true
  property string title: ""
  property string contentText: ""
  property date lastSeenDate
  property bool stateVisible: true
  property var config: ThemeController.style.objectDelegate
  property bool showStatusText: true

  // Switch visibility of title icon
  property bool titleIcon: false
  property string titleIconSource: "qrc:/power.svg"

  // Switch visibility of action button
  property bool actionButton: false
  property string actionButtonIconSource: "qrc:/more-2.svg"

  // This method will be invoked on press and hold event
  property var pressAndHoldHandler: function handlePressAndHold() {
    // Press&hold is supported only on mobiles
    if (app.isMobile) {
      check()
    }
  }

  property bool online: true

  property alias avatar: objectAvatar
  property alias bgRect: bgRect
  readonly property alias titleComponent: titleComponent
  readonly property alias textComponent: textComponent
  readonly property alias stateComponent: statusText
  readonly property alias stateRect: statusComponent
  property alias dropShadow: dropShadow
  readonly property alias rightCheckbox: rightCheckBox
  readonly property alias leftCheckbox: checkBox
  readonly property alias actionButtonComponent: actionButtonComponent

  signal actionButtonClicked()
  signal delegateClicked()

  enum SyncMode {
    Syncing,
    OutOfSync,
    Synced
  }

  property bool syncing: false
  property bool outOfSync: false
  property int syncMode: {
    if (syncing) return DeviceDelegate.SyncMode.Syncing
    else if (outOfSync) return DeviceDelegate.SyncMode.OutOfSync
    return DeviceDelegate.SyncMode.Synced
  }

  onPressAndHold: pressAndHoldHandler()

  onClicked: {
    console.log("Clicked", selectionMode)
    if (selectionMode) {
      check()
    } else {
      delegateClicked()
    }
  }

  leftPadding: ThemeController.style.margin.m16
  rightPadding: ThemeController.style.margin.m16
  topPadding: ThemeController.style.margin.m12
  bottomPadding: ThemeController.style.margin.m12

  background: Item {
    Rectangle {
      id : bgRect
      anchors.fill: parent
      color: control.pressed ? config.bgColorPressed : config.bgColor
      radius: s(8)
      border {
        color: control.selected ? config.bdColorSelected : config.bdColor
        width: s(control.selected ? 2 : 1)
      }
      visible: !dropShadow.visible
    }
    DropShadow {
      id: dropShadow
      anchors.fill: bgRect
      source: bgRect
      transparentBorder: true
      color: control.hovered || control.selected ? config.dropShadowColor05 : config.dropShadowColor04
      visible: !control.pressed
      spread: 0.2
      radius: control.hovered ? s(30) : control.selected ? s(16) : s(10)
      verticalOffset: s(2)
    }
  }

  contentItem: RowLayout {
    spacing: ThemeController.style.margin.m12
    CheckIndicator {
      id: checkBox
      visible: control.checkboxOnTheLeft && control.checkable
      Layout.alignment: Qt.AlignVCenter
      checked: control.selected
      implicitHeight: s(24)
      implicitWidth: s(24)
      MouseArea {
        anchors.fill: parent
        onClicked: {
          if (selectionMode)
            control.clicked()
          else
            check()
        }
      }
    }

    DeviceAvatar {
      id: objectAvatar
      Layout.alignment: Qt.AlignVCenter
      size: DeviceAvatar.Size.Small
      syncing: control.syncing
      outOfSync: control.outOfSync
    }

    Column {
      spacing: ThemeController.style.margin.m8
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignVCenter
      RowLayout {
        spacing: ThemeController.style.margin.m8
        width: parent.width
        ColoredImage {
          id: titleIcon
          icon.width: s(16)
          icon.height: s(16)
          visible: control.titleIcon
          source: control.titleIconSource
          color: config.titleIconColor
        }
        BaseText {
          id: titleComponent
          Layout.fillWidth: true
          elide: Text.ElideRight
          font.weight: Font.DemiBold
          text: control.title
          color: config.titleTextColor
        }
      }
      RowLayout {
        width: parent.width

        BaseText {
          id: textComponent
          text: control.contentText
          size: BaseText.TextSize.Small
          color: config.textColor
          visible: text.length > 0
        }

        Rectangle {
          Layout.preferredHeight: s(3)
          Layout.preferredWidth: s(3)
          radius: s(100)
          color: ThemeController.style.shuttleColor
          visible: showStatusText
        }

        StatusItem {
          id: statusComponent
          isOnline: online
          visible: stateVisible
        }

        BaseText {
          id: statusText
          Layout.fillWidth: true
          elide: Text.ElideRight
          size: BaseText.TextSize.Small
          font.weight: control.syncMode === DeviceDelegate.SyncMode.Syncing ||
                       control.syncMode === DeviceDelegate.SyncMode.OutOfSync ?
                         Font.DemiBold : Font.Normal
          color: control.syncMode === DeviceDelegate.SyncMode.Syncing ?
                   ThemeController.style.slatePurpleColor : control.syncMode === DeviceDelegate.SyncMode.OutOfSync ?
                     ThemeController.style.carnationColor : ThemeController.style.shuttleColor
          visible: text.length > 0 && showStatusText
          text: control.syncMode === DeviceDelegate.SyncMode.Syncing ?
                  Strings.syncing : control.syncMode === DeviceDelegate.SyncMode.OutOfSync ?
                    Strings.outOfSync : online ? Strings.onlineNow : Utility.formatLastSeen(lastSeenDate)
        }
      }
    }

    CheckIndicator {
      id: rightCheckBox
      visible: !control.checkboxOnTheLeft && control.checkable
      Layout.alignment: Qt.AlignVCenter
      checked: control.selected
      implicitHeight: s(32)
      implicitWidth: s(32)
      checkIcon.width: s(18)
      checkIcon.height: s(15)
      MouseArea {
        anchors.fill: parent
        onClicked: {
          if (selectionMode)
            control.clicked()
          else
            check()
        }
      }
    }

    LargeIconButton {
      id: actionButtonComponent
      buttonStyle: config.actionButton
      visible: control.actionButton && !rightCheckBox.visible
      icon.source: control.actionButtonIconSource
      onClicked: actionButtonClicked()
      onPressAndHold: control.pressAndHold()
    }
  }

  function check() {
    if (objectsModel){
      selectionModel.select(objectsModel.mapToSource(objectsModel.index(index, 0)), ItemSelectionModel.Toggle)
    }
  }

  Connections {
    target: selectionModel
    function onSelectionChanged() {
      control.selected = selectionModel.isSelected(objectsModel.mapToSource(objectsModel.index(index, 0)))
    }
  }
}

