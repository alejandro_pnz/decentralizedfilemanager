import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects


Item {
  id: root
  implicitWidth: parent.width
  implicitHeight: s(56)

  enum DisplayMode {
    Audio,
    Video
  }

  property int mode: MediaPlayerToolbar.DisplayMode.Video
  property bool showBg: true
  property bool playing: false

  property alias bgRect: bgRect
  property alias slider: slider
  property alias startTxt: startTxt
  property alias endTxt: endTxt
  property alias pauseButton: pauseButton
  property alias volumeButton: volumeButton
  property alias volumeSlider: volumeSlider
  property alias expandButton: expandButton

  signal play()
  signal expand()

  Rectangle {
    id: bgRect
    anchors{
      fill: parent
    }
    radius: 4
    color: ThemeController.style.audioPlayer.bgColor
    border.color: ThemeController.style.audioPlayer.bdColor
    border.width: showBg ? s(1) : 0

    Rectangle {
      anchors{
        top: parent.top
        left: parent.left
        right: parent.right
      }
      height: ThemeController.style.margin.m4
      color: ThemeController.style.audioPlayer.bgColor
    }

    RowLayout {
      spacing: ThemeController.style.margin.m12
      height: s(32)
      anchors {
        verticalCenter: parent.verticalCenter
        left: parent.left
        right: parent.right
        leftMargin: ThemeController.style.margin.m16
        rightMargin: ThemeController.style.margin.m16
      }

      UIKit.StyledButton {
        id: pauseButton
        type: ThemeController.style.backButtonDisplayMode
        displayMode: UIKit.StyledButton.DisplayMode.IconOnly
        icon.source: playing ? "qrc:/pause.svg" : "qrc:/play.svg"
        iconImg.width: s(16)
        iconImg.height: s(16)
        Layout.preferredHeight: s(32)
        Layout.preferredWidth: s(32)
        Layout.alignment: Qt.AlignVCenter
        onClicked: play()
      }

      UIKit.BaseText {
        id: startTxt
        text: "1:11"
        size: UIKit.BaseText.TextSize.Small
        horizontalAlignment: Text.AlignHCenter
        color: ThemeController.style.audioPlayer.textColor
        Layout.alignment: Qt.AlignVCenter
      }

      MediaSlider {
        id: slider
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignVCenter
        playing: root.playing
      }

      UIKit.BaseText {
        id: endTxt
        text: "5:28"
        size: UIKit.BaseText.TextSize.Small
        horizontalAlignment: Text.AlignHCenter
        color: ThemeController.style.audioPlayer.textColor
        Layout.alignment: Qt.AlignVCenter
      }

      UIKit.StyledButton {
        id: volumeButton
        type: ThemeController.style.backButtonDisplayMode
        displayMode: UIKit.StyledButton.DisplayMode.IconOnly
        icon.source: "qrc:/volume.svg"
        iconImg.width: s(16)
        iconImg.height: s(16)
        Layout.preferredHeight: s(32)
        Layout.preferredWidth: s(32)
        Layout.alignment: Qt.AlignVCenter
        onClicked: volumeSlider.open()

        UIKit.VolumeControl {
          id: volumeSlider
          y: -volumeSlider.height - ThemeController.style.margin.m20
          x: volumeButton.width * 0.5 - volumeSlider.width * 0.5
        }
      }

      UIKit.StyledButton {
        id: expandButton
        type: ThemeController.style.backButtonDisplayMode
        displayMode: UIKit.StyledButton.DisplayMode.IconOnly
        icon.source: checked ? "qrc:/minimize.svg" : "qrc:/expand.svg"
        checkable: true
        iconImg.width: s(16)
        iconImg.height: s(16)
        Layout.preferredHeight: s(32)
        Layout.preferredWidth: s(32)
        Layout.alignment: Qt.AlignVCenter
        onClicked: expand()
        visible: mode === MediaPlayerToolbar.DisplayMode.Video
      }
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: bgRect
    source: bgRect
    color: ThemeController.style.audioPlayer.dropShadowColor
    radius: s(10)
    verticalOffset: s(2)
    horizontalOffset: 0
    //samples: radius * 2 + 1
    visible: showBg
  }
}
