import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import com.test 1.0

Rectangle {
  implicitHeight: s(64)
  implicitWidth: parent.width

  color: ThemeController.style.whiteColorOpacity88

  property int contentHorizontalMargins: 0
  default property alias children: content.children

  Rectangle {
    id: border
    anchors.top: parent.top
    width: parent.width
    height: s(1)
    color: ThemeController.style.mercuryColor
  }

  Item {
    id: content
    anchors {
      top: border.bottom
      left: parent.left
      right: parent.right
      bottom: parent.bottom
      leftMargin: contentHorizontalMargins
      rightMargin: contentHorizontalMargins
    }
  }
}
