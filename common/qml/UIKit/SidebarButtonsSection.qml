import QtQuick 2.15
import UIKit 1.0 as UIKit
import QtQuick.Layouts 1.12
import AppStyles 1.0

ColumnLayout {
  id: sidebarButtonsSection
  default property alias children: itemsColumn.children
  property string sectionName: ""
  property bool contentHidden: true
  property int buttonsSpacing: ThemeController.style.margin.m24
  property int itemsTopMargin: ThemeController.style.margin.m20
  spacing: 0

  property bool shrinked: sidebar.shrinked
  signal sidebarSectionExpanded(var expand)

  UIKit.SidebarSectionLabel{
    id: section
    txt: sectionName
    contentHidden: sidebarButtonsSection.contentHidden
    Layout.leftMargin: ThemeController.style.margin.m24
    visible: !shrinked
    onContentHiddenChanged: {
        sidebarButtonsSection.sidebarSectionExpanded(contentHidden)
    }
  }

  Column {
    id: itemsColumn
    Layout.topMargin: itemsTopMargin
    Layout.fillWidth: true
    visible: section.contentHidden
    spacing: buttonsSpacing
  }
}
