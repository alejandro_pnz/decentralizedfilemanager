import QtQuick 2.15
import QtQuick.Window 2.12
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0

Window {
  id: splash
  visible: true

  //x: Screen.width / 2 - width / 2
  //y: Screen.height / 2 - height / 2
  width: smartScale.width
  height: smartScale.height
  flags: Qt.SplashScreen
  title: Strings.quodArca
  onWidthChanged: console.log("smart w:", width, smartScale.width, smartScale.height)

  Connections {
    target: app
    function onCloseSplash() {
      splash.close()
    }
  }

  Column {
    spacing: Utility.s(25)
    anchors.centerIn: parent
    UIKit.SvgImage {
      id: logo
      source: "qrc:/logo-vertical.svg"
      width: Utility.s(255 * 0.75)
      height: Utility.s(199 * 0.75)
      anchors.horizontalCenter: parent.horizontalCenter
    }
    UIKit.BaseText {
      text: Strings.loading
      anchors.horizontalCenter: parent.horizontalCenter
      color: ThemeController.style.manateeColor
    }
  }
}
