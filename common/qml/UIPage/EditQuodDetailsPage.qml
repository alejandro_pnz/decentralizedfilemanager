import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  page: Pages.EditQuodDetailsPage
  footerComponent: bottomMenu
  headerComponent: pageHeader
  footerVisible: true

  EditQuodDetails {
    id: fragment
  }

  Component {
    id: bottomMenu
    CreateArcaBottomMenu {
      leftButton.text: Strings.saveQuod
      onLeftButtonClicked: {
        if (fragment.validate()) {
          fragment.save()
          homeManager.saveCurrentFlow()
        }
      }
      onRightButtonClicked: {
        if (fragment.validate()) {
          homeManager.openPage(Pages.AddTagsPage)
        }
      }
    }
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.editDetails
      subtitle: Strings.createQuodHeader.arg(quodEditor.object.name)
      rightButton.icon.source: "qrc:/close.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      onLeftButtonClicked: homeManager.back()
      onRightButtonClicked: homeManager.abortCurrentFlow()
    }
  }

  CustomAttributePage {
    id: customAttributePopup
  }

  Connections {
    target: homeManager
    function onCreateCustomAttribute(name) {
      customAttributePopup.popup.open()
      customAttributePopup.attributeName.text = name
    }
  }
}
