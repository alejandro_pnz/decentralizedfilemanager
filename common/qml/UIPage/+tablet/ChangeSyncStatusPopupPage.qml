import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page

  Connections {
    target: homeManager
    function onSetupChangeSyncStatusPage(config) {
      page.acceptHandler = config.accept
      page.rejectHandler = config.reject
    }
  }

  function accept() {
    acceptHandler()
    close()
  }

  function reject() {
    rejectHandler()
    close()
  }

  property var acceptHandler
  property var rejectHandler
  popup: UIKit.PopupScreen {
    id: popup
    visible: false
    width: s(464)
    height: Math.min(parent.height - 2 * s(80), cntHeight)
    showCloseButton: false
    showBackButton: false
    parent: Overlay.overlay
    verticalPadding: ThemeController.style.margin.m24
    horizontalPadding: ThemeController.style.margin.m24

    property int cntHeight: content.implicitHeight + footer.height + content.anchors.bottomMargin + 2 * verticalPadding
    PopupInfoLayoutContent {
      id: content
      title: Strings.changeSyncStatus
      content: Strings.changeSyncStatusContent
      infoboxText: ""
      anchors{
        top: parent.top
        right: parent.right
        left: parent.left
        bottom: footer.top
        bottomMargin: ThemeController.style.margin.m24
      }
      onCloseClicked: page.reject()
    }

    ColumnLayout {
      id: footer
      anchors.bottom: parent.bottom
      width: parent.width
      spacing: ThemeController.style.margin.m12

      UIKit.StyledButton {
        Layout.topMargin: ThemeController.style.margin.m20
        text: Strings.yesChangeStatus
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        Layout.fillWidth: true
        onClicked: {
          page.accept()
        }
      }

      UIKit.StyledButton {
        text: Strings.noDontChangeStatus
        type: UIKit.StyledButton.ButtonStyle.Secondary
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        Layout.fillWidth: true
        onClicked: {
          page.reject()
        }
      }
    }
  }
}
