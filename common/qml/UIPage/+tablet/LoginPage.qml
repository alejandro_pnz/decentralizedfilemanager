import QtQuick 2.0
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0

BasePage {
  id: root
  hasLeftButton: false

  property bool busy: false

  Login {
    id: loginForm
    anchors.centerIn: parent
    width: root.getWidth(4)
    busy: root.busy
  }

  UIKit.NotificationDelegate {
    id: notification
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.bottom: parent.bottom
    anchors.bottomMargin: ThemeController.style.margin.m24
    contentText: Strings.youHaveBeenLoggedOut
    visible: false
    Connections {
      target: pageManager
      function onSetupLoginPage() {
        notification.show()
      }
    }
  }
}
