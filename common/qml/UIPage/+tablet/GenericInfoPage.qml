import QtQuick 2.0
import UIFragments 1.0

BasePage {
  id: root
  hasLeftButton: genericInfo.showBackButton

  GenericInfo {
    id: genericInfo
    anchors.centerIn: parent
    width: root.getWidth(4)
  }
}
