import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.PopupScreen {
    id: popup
    visible: false
    width: 384
    height: 335
    showCloseButton: false
    showBackButton: false
    parent: Overlay.overlay
    verticalPadding: 24
    horizontalPadding: 24

    UIKit.LargeIconButton {
      id: closeButton
      icon.source: "qrc:/close.svg"
      height: 32
      width: 32
      anchors {
        top: parent.top
        right: parent.right
      }
      onClicked: popup.close()
    }

    Verifying {
      id: fragment
      anchors {
        right: parent.right
        left: parent.left
        top: closeButton.bottom
        topMargin: 8
        bottom: parent.bottom
      }
    }
  }
}
