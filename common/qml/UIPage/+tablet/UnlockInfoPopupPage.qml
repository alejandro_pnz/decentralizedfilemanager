import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.PopupScreen {
    id: popup
    visible: false
    width: 384
    height: Math.min(parent.height - 2 * 80, cntHeight)
    showCloseButton: false
    showBackButton: false
    parent: Overlay.overlay
    verticalPadding: 24
    horizontalPadding: 24

    property int cntHeight: content.implicitHeight + footer.height + 2 * verticalPadding
    PopupInfoLayoutContent {
      id: content
      title: Strings.unlockAuthorization
      content: Strings.unlockAuthorizationExpires
      infoboxText: ""
      anchors{
        top: parent.top
        right: parent.right
        left: parent.left
        bottom: footer.top
      }
    }

    ColumnLayout {
      id: footer
      anchors.bottom: parent.bottom
      width: parent.width
      spacing: 12

      Rectangle {
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        Layout.preferredHeight: 1
      }

      Row {
        spacing: 12
        Layout.alignment: Qt.AlignHCenter
        Layout.topMargin: -4

        UIKit.SvgImage {
          width: 24
          height: 24
          source: "qrc:/clock.svg"
        }

        UIKit.BaseText {
          text: Strings.hourMinutes.arg("1").arg("20")
          font.weight: Font.Bold
        }
      }

      Rectangle {
        Layout.topMargin: -4
        Layout.fillWidth: true
        color: ThemeController.style.mercuryColor
        Layout.preferredHeight: 1
      }

      UIKit.StyledButton {
        Layout.topMargin: 20
        text: Strings.okGotIt
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        Layout.fillWidth: true
        onClicked: {
          page.close()
        }
      }

      UIKit.StyledButton {
        text: Strings.cancelUnlock
        type: UIKit.StyledButton.ButtonStyle.Secondary
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        Layout.fillWidth: true
        onClicked: {
          page.close()
        }
      }
    }
  }
}
