import QtQuick 2.0
import QtQuick.Controls 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import AppStyles 1.0
BasePage {
  id: root
  Onboarding {
    id: onboarding
    anchors{
      top: parent.top
      bottom: parent.bottom
      bottomMargin: ThemeController.style.margin.m40
      horizontalCenter: parent.horizontalCenter
    }

    width: root.getWidth(4)
  }
}
