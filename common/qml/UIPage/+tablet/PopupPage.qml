import QtQuick 2.0
import "../"

AbstractHomePage {
  property var popup
  isPopup: true
  Component.onCompleted: popup.open()
  function close() {
    popup.close()
    homeManager.back()
  }
}
