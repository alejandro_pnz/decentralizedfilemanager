import QtQuick 2.12
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  page: Pages.ExportQuodPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: false

      leftButtonHandler: function() {
        homeManager.back()
      }
      bottomBorderVisible: true

      UIKit.BaseText {
        anchors.centerIn: parent
        text: Strings.exportFormat
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
        Layout.alignment: Qt.AlignHCenter
      }
    }
  }

  ExportQuod {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: pageHeader.bottom
      topMargin: 16
    }
  }
}
