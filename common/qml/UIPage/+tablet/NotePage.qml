import QtQuick 2.12
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.NotePage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      height: ThemeController.style.pageHeaderHeight
      width: parent.width

      ColumnLayout{
        anchors.centerIn: parent
        spacing: 0
        visible: !noteFragment.editMode

        UIKit.BaseText {
          text: homeManager.flow === HomeManager.CreateFile ? Strings.newTextFile :
                                                              Strings.addNotes
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        UIKit.BaseText {
          text: Strings.newArca
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }

      Row {
        anchors.centerIn: parent
        visible: noteFragment.editMode

        UIKit.StyledTabButton {
          checked: true
          text: Strings.richtextMode
          onClicked: {
          }
        }

        UIKit.StyledTabButton {
          checked: false
          text: Strings.markdownMode
          onClicked: {
          }
        }
      }
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: noteFragment.editMode ? Strings.done : Strings.edit
      leftButton.mode: noteFragment.editMode ? UIKit.NavigationBarButton.DisplayMode.StyledButton
                                         : UIKit.NavigationBarButton.DisplayMode.LargeIconButton
      leftButton.styledButton.text: Strings.discard
      leftButton.styledButton.textItem.color: ThemeController.style.charadeColor
      bottomBorderVisible: true
      leftButtonHandler: function() {
        homeManager.back()
      }
      rightButtonHandler: function() {
        if(!noteFragment.editMode){
          noteFragment.editMode = true
        }
        else{
          homeManager.saveNote(noteFragment.noteContent)
        }
      }
    }
  }

  UIKit.BaseFooter {
    id: footer
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }
    visible: noteFragment.editMode

    RowLayout {
      height: s(32)
      anchors{
        bottom: parent.bottom
        left: parent.left
        right: parent.right
        leftMargin: ThemeController.style.margin.m16
        rightMargin: ThemeController.style.margin.m16
        verticalCenter: parent.verticalCenter
      }
      spacing: ThemeController.style.margin.m24

      UIKit.LargeIconButton {
        icon.source: "qrc:/h1-icon.svg"
        iconWidth: s(32)
        iconHeight: s(32)
        Layout.alignment: Qt.AlignVCenter
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/h2-icon.svg"
        iconWidth: s(32)
        iconHeight: s(32)
        Layout.alignment: Qt.AlignVCenter
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/b-icon.svg"
        iconWidth: s(32)
        iconHeight: s(32)
      }

      Rectangle {
        Layout.preferredHeight: s(32)
        Layout.preferredWidth: s(1)
        color: ThemeController.style.seashellColor
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/bullet-list.svg"
        iconWidth: s(32)
        iconHeight: s(32)
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/link.svg"
        iconWidth: s(32)
        iconHeight: s(32)
      }

      Rectangle {
        Layout.preferredHeight: s(32)
        Layout.preferredWidth: s(1)
        color: ThemeController.style.seashellColor
      }

      UIKit.StyledButton {
        id: addButton
        icon.source: "qrc:/chevron-white.svg"
        displayMode: UIKit.StyledButton.DisplayMode.IconOnly
        width: s(32)
        height: s(32)
        implicitHeight: s(32)
        implicitWidth: s(32)
      }
    }
  }

  Note {
    id: noteFragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: footer.top
    }
  }
}
