import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit

Page {
  id: page
  // Common logic and properties should be placed here
  property bool isLandscapeMode: Screen.orientation === Qt.LandscapeOrientation
                                 || Screen.orientation === Qt.InvertedLandscapeOrientation
  property bool hasLeftButton: true
  property bool hasRightButton: false
  property int columnCount: ThemeController.style.pageColumns
  property int gutter: ThemeController.style.pageHorizontalGutter
  property real columnWidth: (content.width - (columnCount-1) * gutter) / columnCount
  // Design decided that for 4 column components the maximum width should be 544dp
  // Here is a calculation for maximum column width based on this requirement
  // Take maximum 4 column width (544), remove the gutters (3 * gutter) and divide
  // by the number of column (/4)
  property real maximumColumnWidth: (s(544) - 3 * gutter) / 4

  property real horizontalMargins: ThemeController.style.pageHorizontalMargin
  property alias page: page
  default property alias children: content.children
  property bool sidebarVisible: false


  function setupPage (config){
    if (config.leftButton !== undefined)
      hasLeftButton = config.leftButton
    if (config.rightButton !== undefined)
      hasRightButton = config.rightButton
  }


  header: UIKit.MainToolbar {
    id: toolbar
    leftButton.visible: hasLeftButton
    rightButton.visible: hasRightButton
  }

  Item {
    id: content
    anchors {
      fill: parent
      leftMargin: horizontalMargins
      rightMargin: horizontalMargins
    }
  }

  // This method return the width of component
  // that span columnSpan columns
  function getWidth(columnSpan=1) {
    return Math.min(columnWidth * columnSpan + gutter * (columnSpan - 1), getMaximumWidth(columnSpan))
  }

  // Calculates the maximum width of the component with given column span
  function getMaximumWidth(columnSpan=1) {
    return maximumColumnWidth * columnSpan + gutter * (columnSpan - 1)
  }

  // Helper method that return desired margin
  // assuming that component will be centered
  function getMargin(columnSpan=1) {
    return (content.width - getWidth(columnSpan)) * 0.5
  }

  onStateChanged: console.log("State changed", state)
}
