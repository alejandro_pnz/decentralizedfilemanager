import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import UIModel 1.0 as UIModel
import QtQuick.Layouts 1.12
import com.testintl 1.0

AbstractHomePage {
  id: root
  page: Pages.MorePage
  headerComponent: pageHeader
  footerVisible: true

  NavigationMore {
    id: moreFragment
    anchors {
      fill: parent
      topMargin: ThemeController.style.margin.m16
    }
  }

  Component {
    id: pageHeader
    MoreHeader {
      id: header
      userEmail: AuthenticationManager.currentAccount ? AuthenticationManager.currentAccount.email : ""
      onSwitchAccount: {
        accountsDrawer.open()
      }
      switchAccountVisible: accountsDrawer.visible
    }
  }

  Item {
    anchors.fill: parent
    UIKit.SwitchAccountPopup {
      id: accountsDrawer
      y: s(8)
    }
  }
}
