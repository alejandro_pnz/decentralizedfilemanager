import QtQuick 2.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import UIModel 1.0 as UIModel

AbstractHomePage {
  id: root
  title: Strings.search
  page: Pages.SearchPage
  headerComponent: pageHeader
  footerVisible: false

  Search {
    id: searchFragment
    anchors {
      fill: parent
    }
  }

  Component {
    id: pageHeader
    SearchHeader {
      id: header
      onFilterClicked: filters.open()
    }
  }

  SearchFiltersMenu {
    id: filters
    x: parent.width - filters.width
    y: -8
    height: Math.min(filters.implicitHeight, parent.height)
  }
}
