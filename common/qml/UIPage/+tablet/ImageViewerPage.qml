import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import QtQuick.Controls 2.15
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

import "../"

AbstractHomePage {
  page: Pages.PdfViewerPage
  //objectName: "myAccountPage"

  BasePage {
    id: root
    hasLeftButton: false
    hasRightButton: false
    horizontalMargins: (parent.width - content.width) * 0.5
    parent: Overlay.overlay
    anchors.fill: parent

    header: Item {
      id: headerItem
      width: parent.width
      height: s(56)

      UIKit.LargeIconButton {
        id: largeIconButton
        icon.source: "qrc:/close-gray.svg"
        iconHeight: s(32)
        iconWidth: s(32)
        anchors {
          top: parent.top
          topMargin: ThemeController.style.margin.m16
          right: parent.right
          rightMargin: ThemeController.style.margin.m24
        }
        onClicked: homeManager.back()
      }
    }

    footer: UIKit.BaseFooter {
      height: s(80)
      contentHorizontalMargins: root.horizontalMargins
      color: ThemeController.style.whiteColor
      visible: stackLayout.children[stackLayout.currentIndex].showFooter


    }

    RowLayout {
      id: content
      parent: root.noScroll
      anchors.horizontalCenter: parent.horizontalCenter
      width: s(720)
      height: parent.height
      spacing: ThemeController.style.margin.m32

      ImageViewer {
        id: fragment
        fileViewer: fileEditor.fileViewer
        anchors {
          left: parent.left
          right: parent.right
          bottom: parent.bottom
          top: parent.top
          leftMargin: -ThemeController.style.pageHorizontalMargin
          rightMargin: -ThemeController.style.pageHorizontalMargin
        }
      }
    }
  }
}
