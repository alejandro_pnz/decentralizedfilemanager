import QtQuick 2.12
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.SelectQuodSubTypePage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true
      leftButtonHandler: function() {
        homeManager.back()
      }
      rightButtonHandler: function() {
        homeManager.abortCurrentFlow()
      }
      ColumnLayout {
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          text: Strings.selectType
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        UIKit.BaseText {
          text: Strings.idQuod
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }
    }
  }

  UIKit.BaseFooter {
    id: footer
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    UIKit.StyledButton {
      text: Strings.continueTxt
      type: UIKit.StyledButton.ButtonStyle.Primary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors{
        verticalCenter: parent.verticalCenter
        right: parent.right
        rightMargin: horizontalMargins
      }
      onClicked: {
        if (selectIdQuod.hasSelection)
          homeManager.openPage(Pages.EditQuodDetailsPage)
      }
    }
  }

  SelectQuodSubType {
    id: selectIdQuod
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
      bottom: footer.top
    }
  }
}
