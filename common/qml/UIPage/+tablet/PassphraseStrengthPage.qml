import QtQuick 2.0
import UIFragments 1.0
import com.test 1.0
import Qt5Compat.GraphicalEffects

import UIKit 1.0

BasePage {
  id: root

  PassphraseStrength {
    id: passphraseStrength
    anchors.centerIn: parent
    width: root.getWidth(4)
    onShowHint: {
      tip.pointToGlobal = globalPos
      tip.visible = true
    }
  }

  Tip {
    id: tip
    visible: false
    anchors {
      right: passphraseStrength.right
      rightMargin: passphraseStrength.tipButtonWidth + 8
      verticalCenter: parent.verticalCenter
    }
    pointing: CanvasTriangle.Pointing.Right

    width: root.getWidth(4)
    onHandleHideHint: {
      tip.visible = false
    }
    headerText: Strings.quodArcaPowerPassphrase
    itemsModel: objectsModel

    Component.onCompleted: {
      objectsModel.append({ titleTxt: Strings.gainExclusiveAccess,
                            contentTxt: Strings.extraFeatures,
                            infoTxt: Strings.yourPowerPassphrase});
      objectsModel.append({ titleTxt: Strings.manageUsers,
                            contentTxt: Strings.usingThePowePassphrase,
                            infoTxt: Strings.yourPowerPassphrase});
      objectsModel.append({ titleTxt: Strings.loginReset,
                            contentTxt: Strings.powerPassphraseKnowledge,
                            infoTxt: Strings.yourPowerPassphrase});
    }

    ListModel{
      id: objectsModel
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: tip
    horizontalOffset: 0
    verticalOffset: 4
    radius: 40
    //samples: 81
    color: Qt.rgba(0, 0, 0, 0.08)
    source: tip
    visible: tip.visible
  }
}
