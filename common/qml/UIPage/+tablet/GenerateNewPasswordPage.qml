import QtQuick 2.12
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  page: Pages.GenerateNewPasswordPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      leftButton.iconButton.icon.source: "qrc:/close.svg"
      rightButton.visible: false
      leftButtonHandler: function() {
          homeManager.back()
      }
      bottomBorderVisible: true

        UIKit.BaseText {
          text: Strings.passwordGenerator
          font.weight: Font.Bold
          anchors.centerIn: parent
        }
    }
  }

  UIKit.BaseFooter {
    id: footer
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    UIKit.StyledButton {
      text: Strings.usePassword
      type: UIKit.StyledButton.ButtonStyle.Primary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors{
        verticalCenter: parent.verticalCenter
        left: parent.left
        right: parent.right
        leftMargin: horizontalMargins
        rightMargin: horizontalMargins
      }
    }
  }

  PasswordGenerator {
    id: fragment
    anchors {
      top: parent.top
      bottom: footer.top
      horizontalCenter: parent.horizontalCenter
    }
    width: root.getWidth(4)
    refreshButton.type: UIKit.StyledButton.ButtonStyle.Secondary
  }
}
