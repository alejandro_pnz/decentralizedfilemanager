import QtQuick 2.12
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.OneUserPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader

    UIKit.MainToolbar {
      id: toolbar
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true
      rightButton.iconButton.icon.source: "qrc:/more-2.svg"
      rightButtonHandler: function() {
        actionPopup.open()
      }

      ColumnLayout{
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          text: Strings.userDashboard
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        UIKit.BaseText {
          text: Strings.oneUser
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }
    }
  }

  OneUser {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }

  ActionMenu {
    id: actionPopup
    model: actionModel.model
    x: parent.width - actionPopup.width - root.horizontalMargins
    y: toolbar.height - 8
  }

  SingleUserActionModel {
    id: actionModel
    userDashboardMode: false
  }
}
