import QtQuick 2.0
import UIFragments 1.0

BasePage {
  id: root

  property bool busy: false

  CheckEmail {
    id: checkEmailForm
    anchors.centerIn: parent
    width: root.getWidth(4)
    busy: root.busy
  }
}
