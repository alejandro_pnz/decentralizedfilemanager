import QtQuick 2.12
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.DeviceDashboardPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      id: toolbar
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      rightButton.iconButton.icon.source: "qrc:/more-2.svg"
      bottomBorderVisible: true
      rightButtonHandler: function() {
        actionPopup.open()
      }

      UIKit.BaseText {
        text: Strings.deviceDashboard
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  DeviceDashboard {
    id: deviceDashboard
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
      bottom: parent.bottom
    }
  }

  ActionMenu {
    id: actionPopup
    model: actionModel.model
    x: parent.width - actionPopup.width - root.horizontalMargins
    y: toolbar.height - 8
  }

  DevicesActionModel {
    id: actionModel
  }
}
