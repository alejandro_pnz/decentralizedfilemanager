import QtQuick 2.12
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.SubscriptionPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done
      leftButtonHandler: function() {
          homeManager.back()
      }
      bottomBorderVisible: true

      UIKit.BaseText {
        text: Strings.subscriptionStr
        font.weight: Font.Bold
        anchors.centerIn: parent
      }
    }
  }

  Subscription {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
    cardColumns: root.isLandscapeMode ? 2 : 1
  }
}
