import QtQuick 2.0
import UIFragments 1.0
import com.test 1.0
import Qt5Compat.GraphicalEffects


BasePage {
  id: root

  property bool busy: false

  NewAccount {
    id: newAccount
    title: Strings.createAnAccount
    buttonText: Strings.createAccount
    anchors.centerIn: parent
    width: root.getWidth(4)
    busy: root.busy
    onShowHint: {
      tip.visible = true
    }
  }

  Tip {
    id: tip
    visible: false
    anchors {
      right: newAccount.right
      rightMargin: newAccount.tipButtonWidth + s(8)
      verticalCenter: parent.verticalCenter
    }

    width: root.getWidth(4)
    onHandleHideHint: {
      tip.visible = false
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: tip
    horizontalOffset: 0
    verticalOffset: s(4)
    radius: s(40)
    //samples: s(81)
    color: Qt.rgba(0, 0, 0, 0.08)
    source: tip
    visible: tip.visible
  }
}
