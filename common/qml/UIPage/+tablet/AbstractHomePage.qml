import QtQuick 2.12
import QtQuick.Window 2.12
import AppStyles 1.0

Item {
  property int page
  property string title: ""
  property bool searchButton: false
  property bool footerVisible: false
  property string rightBtnSrc: ""
  property bool leftButtonVisible: false
  property string subtitle: ""
  property var footerComponent:  null
  property var headerComponent:  null
  property bool isLandscapeMode: Screen.orientation === Qt.LandscapeOrientation
                                 || Screen.orientation === Qt.InvertedLandscapeOrientation
  property real horizontalMargins: ThemeController.style.pageHorizontalMargin
  property bool isPopup: false
}
