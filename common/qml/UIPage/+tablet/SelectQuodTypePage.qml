import QtQuick 2.12
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  title: Strings.selectQuodType
  page: Pages.SelectQuodTypePage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: false
      rightButton.visible: true
      bottomBorderVisible: true
      leftButtonHandler: function() {
        homeManager.back()
      }
      rightButtonHandler: function() {
        homeManager.abortCurrentFlow()
      }

      UIKit.BaseText {
        text: Strings.selectQuodType
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  UIKit.BaseFooter{
    id: footer
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    UIKit.StyledButton {
      text: Strings.continueTxt
      type: UIKit.StyledButton.ButtonStyle.Primary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors{
        verticalCenter: parent.verticalCenter
        right: parent.right
        rightMargin: horizontalMargins
      }
      onClicked: {
        if (selectQuod.hasSubtypes) {
          homeManager.openPage(Pages.SelectQuodSubTypePage)
        } else {
          homeManager.openPage(Pages.EditQuodDetailsPage)
        }
      }
    }
  }

  SelectQuodType {
    id: selectQuod
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
      bottom: footer.top
    }
    onShowSubTypeSelection: homeManager.openPage(Pages.SelectQuodSubTypePage)
  }
}
