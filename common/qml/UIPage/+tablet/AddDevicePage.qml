import QtQuick 2.15
import UIFragments 1.0

BasePage {
  id: root

  property bool busy: false

  AddDevice {
    id: addDevice
    anchors.centerIn: parent
    width: root.getWidth(4)
    busy: root.busy
  }
}
