import QtQuick 2.12
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  page: Pages.DateFilterPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done

      leftButtonHandler: function() {
        homeManager.back()
      }
      bottomBorderVisible: true

      UIKit.BaseText {
        anchors.centerIn: parent
        text: Strings.modified
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
        Layout.alignment: Qt.AlignHCenter
      }
    }
  }

  DateFilter {
    id: fragment
    width: root.getWidth(4)
    anchors {
      horizontalCenter: parent.horizontalCenter
      top: parent.top
      topMargin: 12
      bottom: parent.bottom
    }
  }
}
