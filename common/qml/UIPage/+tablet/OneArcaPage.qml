import QtQuick 2.12
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.OneArcaPage
  headerComponent: pageHeader

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      id: toolbar
      height: ThemeController.style.pageHeaderHeight
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true
      rightButton.iconButton.icon.source: "qrc:/more-2.svg"
      rightButtonHandler: function() {
        actionPopup.open()
      }

      Avatars {
        model: ["J", "B", "J", "A", "M", "A"]
        maxCount: 4
        bottomIcon: true
        anchors.centerIn: parent
      }

      Item{
        ActionMenu {
          id: actionPopup
          model: actionModel.model
          parent: toolbar.rightButton
          x: parent.width - actionPopup.width
          y: parent.height + s(25)
          //x: parent.width - actionPopup.width - root.horizontalMargins
          //y: pageHeader.height - 8
          height: s(448)
          maxHeight: s(448)
        }
      }
    }
  }

  OneArca {
    id: arcaContent
    anchors {
      fill: parent
    }
  }

  ObjectActionModel {
    id: actionModel
    currentObject: arcaEditor.object
    actionList: singleActions
  }
}
