import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page

  popup: UIKit.PopupScreen {
    id: popup
    visible: false
    width: s(544)
    height: onboardingFragment.height + s(120)
    showBackButton: false
    showCloseButton: false
    verticalPadding: ThemeController.style.margin.m24

    Item {
      anchors.centerIn: parent
      implicitWidth: parent.width
      implicitHeight: parent.height

      UIKit.LargeIconButton {
        id: closeIcon
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.rightMargin: ThemeController.style.margin.m24
        icon.source: "qrc:/close.svg"
        onClicked: {
          popup.close()
        }
      }

      HomeOnboarding{
        id: onboardingFragment
        anchors {
          top: closeIcon.bottom
          topMargin: ThemeController.style.margin.m24
          left: parent.left
          right: parent.right

          leftMargin: ThemeController.style.margin.m40
          rightMargin: ThemeController.style.margin.m40
        }
      }
    }
  }
}
