import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  id: root
  objectName: "libraryPage"

  page: Pages.LibraryPage
  footerComponent: selectionMenu
  headerComponent: pageHeader
  footerVisible: true

  signal showSearchScreen()

  Library {
    id: libraryFragment
    horizontalMargins: homePage.horizontalMargins
    horizontalSpacing: ThemeController.style.margin.m32
    verticalSpacing: ThemeController.style.margin.m24
    selectionMode: true
  }

  Component {
    id: selectionMenu
    BottomSelectMenu {
      id: bottomSelectionMenu
      ism: libraryFragment.stackLayout.children[libraryFragment.stackLayout.currentIndex].ism
      objectTypeName: libraryFragment.stackLayout.children[libraryFragment.stackLayout.currentIndex].objectTypeName
      objectType: libraryFragment.stackLayout.children[libraryFragment.stackLayout.currentIndex].objectType
      rootPage: root
    }
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.library
      rightButton.icon.source: "qrc:/bell-icon.svg"
      searchButton.visible: true
      marginLeft: s(smallLayoutMode ? 80 : 160)
      onSearchButtonClicked: showSearchScreen()
      onRightButtonClicked: notificationPopup.open()
    }
  }

  UIKit.CreatePassphrasePopup {
    id: createPassphrasePopup
    anchors.centerIn: parent
    onEnterCreatePassphrase: enterPassphrasePopup.open()
  }

  EnterPassphrasePage {
    id: enterPassphrasePopup
    anchors.centerIn: parent
  }
}
