import QtQuick 2.0
import AppStyles 1.0

Item {
  property int page
  property string title: ""
  property bool searchButton: false
  property bool footerVisible: false
  property string rightBtnSrc: ""
  property bool leftButtonVisible: false
  property string subtitle: ""
  property var footerComponent:  null
  property var headerComponent:  null
  readonly property bool isMobile: app.isMobile
  readonly property bool isPhone: app.isPhone
  property color footerBgColor: ThemeController.style.whiteColorOpacity88
  property bool isPopup: false
  property int horizontalMargin: smallLayoutMode ? s(80) : s(160)
}
