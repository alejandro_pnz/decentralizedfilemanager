import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import UIFragments 1.0
import QtQuick.Controls 2.15

AbstractHomePage {
  id: root
  property int page: Pages.HomePage
  headerComponent: pageHeader
  footerComponent: selectionMenu
  footerVisible: true

//  ScrollView {
//    clip: true
//    id: scrollArea
//    anchors.fill: parent
//    contentHeight: Math.max(fullHeight.height, homeFragment.implicitHeight)
//  }

  Home {
    id: homeFragment
  }
//  Item {
//    id: fullHeight
//    anchors.fill: parent
//  }


  Component {
    id: selectionMenu
    BottomSelectMenu {
      id: bottomSelectionMenu
      ism: homeFragment.ism
      objectTypeName: "Objects"
      objectType: SyncableObject.Generic
      rootPage: root
    }
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      id: homeHeader
      title: Strings.home
      rightButton.icon.source: "qrc:/bell-icon.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      onRightButtonClicked: notificationPopup.open()
    }
  }
}
