import QtQuick 2.15
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  page: Pages.OneArcaPage
  objectName: "oneArcaPage"
  headerComponent: pageHeader
  footerVisible: oneArcaFragment.showFooter
  footerComponent: bottomMenu
  footerBgColor: ThemeController.style.whiteSmokeColor

  OneArca {
    id: oneArcaFragment
    height: parent.height
    width: parent.width
  }

  Component {
    id: pageHeader

    RowLayout {
      id: headerRow
      anchors {
        right: parent.right
        left: parent.left
        verticalCenter: parent.verticalCenter
      }

      LargeIconButton {
        id: backButton
        icon.source: "qrc:/chevron-back.svg"
        height: s(32)
        width: s(32)
        onClicked: homeManager.back()
      }

      BaseText {
        id: titleComponent
        text: Strings.oneArca
        font.weight: Font.DemiBold
        size: BaseText.TextSize.H3
        Layout.leftMargin: s(smallLayoutMode ? 24 : 104)
      }

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
      }

      RowLayout {
        spacing: ThemeController.style.margin.m16

        Avatars {
          model: UserListModel
          maxCount: 4
          bottomIcon: true
        }

        LargeIconButton {
          icon.source: "qrc:/more-2.svg"
          height: s(32)
          width: s(32)
          onClicked: actionPopup.open()

          ActionMenu {
            id: actionPopup
            model: actionModel.model
            x: parent.width - actionPopup.width
            y: parent.height + s(25)
          }
        }
      }
    }
  }

  Component {
    id: bottomMenu

    RowLayout {
      width: parent.width
      height: parent.height
      spacing: ThemeController.style.margin.m12

      BaseText {
        text: Strings.lastModifiedArg.arg(Utility.formatDateTime(homeManager.currentEditor.object.modified))
        Layout.alignment: Qt.AlignHCenter
        horizontalAlignment: Qt.AlignHCenter
      }

      BaseText {
        text: Strings.uniqueId.arg(Utility.uniqueId(homeManager.currentEditor.object.id))
        Layout.alignment: Qt.AlignHCenter
        horizontalAlignment: Qt.AlignHCenter
        color: ThemeController.style.manateeColor
      }

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
      }
    }
  }

  ObjectActionModel {
    id: actionModel
    currentObject: arcaEditor.object
    actionList: singleActions
  }
}
