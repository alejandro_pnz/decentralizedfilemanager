import QtQuick 2.15
import UIFragments 1.0

BasePage {
  id: root
  objectName: "checkEmailPage"

  property bool busy: false

  CheckEmail {
    id: checkEmail
    width: root.getWidth(defaultComponentColumnSpan)
    busy: root.busy
  }
}
