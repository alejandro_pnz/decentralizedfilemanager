import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: p
  UIKit.PopupScreen {
    id: p
    visible: false
    width: homePage.getWidth(6)
    height: parent.height - s(160)
    parent: Overlay.overlay
    showCloseButton: false
    showBackButton: false

    Item {
      anchors{
        centerIn: parent
      }
      height: p.height
      width: p.width

      Item {
        id: popupHeader
        anchors {
          top: parent.top
          right: parent.right
          left: parent.left
        }
        height: s(80)

        UIKit.BaseText {
          id: titleComponent
          text: Strings.showInfo
          font.weight: Font.DemiBold
          size: UIKit.BaseText.TextSize.H3
          anchors.centerIn: parent
        }

        UIKit.LargeIconButton {
          icon.source: "qrc:/close.svg"
          height: s(32)
          width: s(32)
          anchors {
            right: parent.right
            rightMargin: ThemeController.style.margin.m24
            verticalCenter: parent.verticalCenter
          }

          onClicked: page.close()
        }
      }

      Rectangle {
        id: separator
        height: s(1)
        width: parent.width
        color: ThemeController.style.seashellColor
        anchors.top: popupHeader.bottom
      }

      ShowInfo {
        id: showInfoFragment
        anchors {
          top: separator.bottom
          bottom: parent.bottom
          left: parent.left
          right: parent.right
          leftMargin: ThemeController.style.margin.m24
          rightMargin: ThemeController.style.margin.m12
        }
      }
    }
  }
}
