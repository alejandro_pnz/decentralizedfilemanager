import QtQuick 2.0
import UIFragments 1.0
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0

AbstractHomePage {
  page: Pages.ArcaPromptPage

  BasePage {
    id: root
    hasLeftButton: false
    hasRightButton: true
    parent: Overlay.overlay
    anchors.fill: parent
    headerItem.leftButtonHandler: function() {
      homeManager.back()
    }
    headerItem.rightButtonHandler: function() {
      homeManager.back()
    }

    ArcaPrompt {
      width: root.getWidth(root.defaultComponentColumnSpan)
      anchors.verticalCenter: parent.verticalCenter
      anchors.horizontalCenter: parent.horizontalCenter
    }
  }
}
