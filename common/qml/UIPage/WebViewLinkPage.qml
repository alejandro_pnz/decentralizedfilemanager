import QtQuick 2.15
import UIFragments 1.0
import com.test 1.0

BasePage {
  id: root
  hasLeftButton: false
  objectName: "webViewLinkPage"
  Component.onCompleted: console.log("WebViewLinkPage Completed")

  GenericWebView {
    id: webView
    parent: root.noScroll
    anchors.fill: parent
    onWebViewLoadingStatusChanged: {}
    onSendConsoleMessage: {}
  }
}
