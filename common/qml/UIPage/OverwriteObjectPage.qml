import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

UIKit.PopupScreen {
  id: popup
  visible: false
  width: s(588)
  height: Math.min(parent.height - 2 * s(80), fragment.height + popupHeader.height)

  showCloseButton: false
  showBackButton: false
  parent: Overlay.overlay

  property string titleArg: fragment.conflictsNumber > 1 ?
                            "1/" + fragment.conflictsNumber : ""
  Item {
    anchors{
      centerIn: parent
    }
    height: popup.height
    width: popup.width

    Item {
      id: popupHeader
      anchors {
        top: parent.top
        right: parent.right
        left: parent.left
      }
      height: s(80)

      UIKit.BaseText {
        id: titleComponent
        text: Strings.nameConflict.arg(titleArg)
        font.weight: Font.DemiBold
        size: UIKit.BaseText.TextSize.H3
        anchors.centerIn: parent
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/close.svg"
        height: s(32)
        width: s(32)
        anchors {
          right: parent.right
          rightMargin: ThemeController.style.margin.m24
          verticalCenter: parent.verticalCenter
        }

        onClicked: popup.close()
      }
    }

    Rectangle {
      id: separator
      height: s(1)
      width: parent.width
      color: ThemeController.style.seashellColor
      anchors.top: popupHeader.bottom
    }

    OverwriteObject {
      id: fragment

      anchors {
        top: popupHeader.bottom
        left: parent.left
        right: parent.right
        leftMargin: ThemeController.style.margin.m24
        rightMargin: ThemeController.style.margin.m24
      }
    }
  }
}
