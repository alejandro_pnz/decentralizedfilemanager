import QtQuick 2.15
import UIFragments 1.0

BasePage {
  id: root
  objectName: "confirmPassphrasePage"
  property bool busy: false

  ConfirmPassphrase {
    id: confirmPassphrase
    width: root.getWidth(defaultComponentColumnSpan)
    busy: root.busy
  }
}
