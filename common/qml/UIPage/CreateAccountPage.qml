import QtQuick 2.0
import UIFragments 1.0
import com.test 1.0
import Qt5Compat.GraphicalEffects

import UIKit 1.0 as UIKit

BasePage {
  id: root
  objectName: "newAccountPage"

  property bool busy: false

  NewAccount {
    id: newAccount
    anchors.verticalCenter: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter
    width: root.getWidth(defaultComponentColumnSpan)
    busy: root.busy
    onShowHint: {
      tip.pointToGlobal = globalPos
      tip.show()
    }
  }

  Tip {
    id: tip
    visible: false
    anchors {
      left: newAccount.right
      leftMargin: root.gutter
      verticalCenter: parent.verticalCenter
    }

    width: root.getWidth(defaultComponentColumnSpan)
    onHandleHideHint: {
      tip.visible = false
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: tip
    horizontalOffset: 0
    verticalOffset: s(4)
    radius: s(40)
    //samples: s(81)
    color: Qt.rgba(0, 0, 0, 0.08)
    source: tip
    visible: tip.visible
  }

  states: [
    State {
      name: "normal"
      when: !smallLayoutMode
      PropertyChanges {
        target: tip
        anchors.left: newAccount.right
        anchors.leftMargin: root.gutter
        anchors.right: undefined
        anchors.rightMargin: 0
        pointing: UIKit.CanvasTriangle.Pointing.Left
      }
    },
    State {
      name: "smallLayout"
      when: smallLayoutMode
      PropertyChanges {
        target: tip
        anchors.right: newAccount.right
        anchors.rightMargin: newAccount.tipButtonWidth + root.gutter
        anchors.left: undefined
        anchors.leftMargin: 0
        pointing: UIKit.CanvasTriangle.Pointing.Right
      }
    }
  ]
}
