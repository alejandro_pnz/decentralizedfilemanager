import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  page: Pages.EditQuodDetailsPage
  footerComponent: bottomMenu
  headerComponent: pageHeader
  footerVisible: true

  PasswordHistory {
    id: fragment
  }

  Component {
    id: bottomMenu
    CreateArcaBottomMenu {
      leftButton.visible: false
      rightButton.text: Strings.done
      onRightButtonClicked: {
        fragment.save()
      }
    }
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.passwordHistory
      subtitle: Strings.createQuodHeader.arg(quodEditor.object.name)
      rightButton.icon.source: "qrc:/close.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      onLeftButtonClicked: fragment.close()
      onRightButtonClicked: fragment.close()
    }
  }
}
