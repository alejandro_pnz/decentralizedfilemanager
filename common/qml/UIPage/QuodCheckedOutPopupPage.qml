import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
PopupPage {
  id: popup
  popup: UIKit.PopupScreen {
    id: p
    visible: false
    width: s(384)
    height: Math.min(maximumHeight, cntHeight)
    showCloseButton: false
    showBackButton: false
    parent: Overlay.overlay
    verticalPadding: ThemeController.style.margin.m24
    horizontalPadding: ThemeController.style.margin.m24

    cntHeight: verticalPadding * 2 + content.implicitHeight

    QuodCheckedOut {
      id: content
      maximumHeight: p.maximumHeight
      verticalPadding: p.verticalPadding
      width: p.width - 2 * p.horizontalPadding
    }
  }
}


