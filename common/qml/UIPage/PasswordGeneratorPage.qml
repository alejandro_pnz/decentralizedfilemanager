import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  objectName: "passwordGeneratorPage"
  page: Pages.PasswordGeneratorPage
  headerComponent: pageHeader
  footerVisible: false


  PasswordGenerator {
    id: fragment
    width: s(394)
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.passwordGenerator

      rightButton.visible: false
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: false
    }
  }
}
