import QtQuick 2.15
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.OneDevicePage
  headerComponent: pageHeader
  footerVisible: false

  OneDevice {
    id: oneDeviceFragment
    height: parent.height
    width: parent.width
  }

  Component {
    id: pageHeader
    PageHeader {
      id: header
      title: Strings.oneDevice
      rightButton.icon.source: "qrc:/more-2.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      onLeftButtonClicked: homeManager.back()
      onRightButtonClicked: {
        actionPopup.open()
      }
    }
  }

  ActionMenu {
    id: actionPopup
    model: singleDeviceActionModel.model
    x: parent.width - actionPopup.width + horizontalMargin - ThemeController.style.margin.m24
    y: s(8)
  }

  DeviceActionModel {
    id: singleDeviceActionModel
    currentObject: deviceEditor.object
    actionList: oneDeviceActions
  }
}
