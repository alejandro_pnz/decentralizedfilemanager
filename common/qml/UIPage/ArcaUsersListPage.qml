import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  page: Pages.ArcaUsersListPage
  footerComponent: bottomMenuArca
  headerComponent: pageHeader
  footerVisible: true

  ArcaUsersList {
    id: arcaUsersListFragment
    horizontalMargins: homePage.horizontalMargins
  }

  Component {
    id: bottomMenuArca
    CreateArcaBottomMenu {
      leftButton.visible: !homeManager.creatingObject
      leftButton.text: {
        switch(homeManager.flow) {
        case HomeManager.CreateQuod:
          return Strings.saveQuod
        case HomeManager.CreateArca:
          return Strings.saveArca
        case HomeManager.CreateFile:
          return Strings.saveFile
        default:
          return Strings.clearAll
        }
      }
      rightButton.text: homeManager.creatingObject ? Strings.continueTxt : Strings.done

      onLeftButtonClicked: {
        if (homeManager.creatingObject) {
          // Save the list
        } else {
          // Clear the selection
        }
      }

      onRightButtonClicked: {
        if (homeManager.flow === HomeManager.CreateQuod)
          homeManager.openPage(Pages.ArcaPromptPage)
        else {
          // Save and back
          homeManager.back()
        }
      }
    }
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      rightButton.icon.source: "qrc:/close.svg"
      subtitle:{
        switch(homeManager.flow) {
        case HomeManager.CreateQuod:
          return Strings.createQuodHeader.arg(QuodTypeModel.currentType.name)
        case HomeManager.CreateArca:
          return Strings.newArca
        case HomeManager.CreateFile:
          return Strings.newFile
        case HomeManager.ManageArca:
          return arcaEditor.object.name
        case HomeManager.ManageQuod:
          return quodEditor.object.name
        case HomeManager.ManageFile:
          return fileEditor.object.name
        }
      }
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      title: arcaUsersListFragment.whitelistMode ? Strings.configureWhitelist :
                                                   Strings.configureBlacklist
      onLeftButtonClicked: homeManager.back()
      onRightButtonClicked: {
        if (homeManager.creatingObject) {
          homeManager.abortCurrentFlow()
        } else {
          // Save the changes
          homeManager.back()
        }
      }
    }
  }
}
