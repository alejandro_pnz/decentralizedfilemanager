import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12

AbstractHomePage {
  id: root
  page: Pages.EnterPassphrasePage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader

    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      leftButton.iconButton.icon.source: "qrc:/close.svg"
      rightButton.visible: false
      bottomBorderVisible: true
      leftButtonHandler: function() { homeManager.back() }

      UIKit.BaseText {
        text: Strings.enterPassphrase
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
        anchors.centerIn: parent
      }
    }
  }

  UIKit.BaseFooter {
    id: footer
    height: s(80)
    anchors {
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    UIKit.StyledButton {
      text: Strings.exportStr
      type: UIKit.StyledButton.ButtonStyle.Primary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors{
        verticalCenter: parent.verticalCenter
        left: parent.left
        right: parent.right
        leftMargin: horizontalMargins
        rightMargin: horizontalMargins
      }
    }
  }

  EnterPassphrase {
    id: enterPassphraseFragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: footer.top
    }
  }
}
