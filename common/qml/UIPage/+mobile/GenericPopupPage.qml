import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  overrideClose: true

  property string title: ""
  property string content: ""
  property bool dontShowAgainSwitch: false
  property alias acceptButton: acceptButton
  property alias rejectButton: rejectButton
  property alias switchButton: switchButton

  signal accept()
  signal reject()

  Connections {
    target: homeManager
    function onSetupGenericPopupPage(config) {
      page.accept.connect(config.accept)
      page.onReject.connect(config.reject)
      acceptButton.text = config.positiveText
      rejectButton.text = config.negativeText
      page.title = config.title
      page.content = config.content
    }
  }

  popup: UIKit.StyledDrawer {
    id: popup
    visible: false
    contentItemHeight: cntHeight
    verticalPadding: ThemeController.style.margin.m24
    horizontalPadding: ThemeController.style.margin.m24
    headerVisible: false
    headerSeparatorVisible: false
    interactive: true
    onClosed: {
      console.log("PAGE REJECTED")
      page.reject()
    }

    property int cntHeight: content.implicitHeight + footer.height + content.anchors.bottomMargin + 2 * verticalPadding

    ColumnLayout {
      anchors{
        right: parent.right
        left: parent.left
      }
      spacing: ThemeController.style.margin.m24
      PopupInfoLayoutContent {
        id: content
        title: page.title
        content: page.content
        infoboxText: ""
        Layout.fillWidth: true
        onCloseClicked: {
          page.reject()
        }
      }
      ColumnLayout {
        id: footer
        Layout.fillWidth: true
        spacing: ThemeController.style.margin.m12

        RowLayout {
          Layout.topMargin: ThemeController.style.margin.m4
          Layout.fillWidth: true
          visible: dontShowAgainSwitch
          UIKit.BaseText {
            text: Strings.dontShowItAgain
            font.weight: Font.DemiBold
            Layout.fillWidth: true
          }

          UIKit.SwitchButton {
            id: switchButton
          }
        }

        UIKit.StyledButton {
          id: acceptButton
          Layout.topMargin: ThemeController.style.margin.m20
          text: Strings.yes
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          Layout.fillWidth: true
          onClicked: page.accept()
        }

        UIKit.StyledButton {
          id: rejectButton
          text: Strings.no
          type: UIKit.StyledButton.ButtonStyle.Secondary
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          Layout.fillWidth: true
          onClicked: page.reject()
        }
      }
    }
  }
}
