import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.15
import QtQml.Models 2.15
import UIModel 1.0
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

import QmlVlc 0.1
import QtMultimedia 5.12

AbstractHomePage {
  id: root
  page: Pages.VideoViewerPage
  headerComponent: null
  footerVisible: false

  BasePage {
    hasLeftButton: false
    hasRightButton: false
    horizontalMargins: 0
    parent: Overlay.overlay
    anchors.fill: parent

    header: Item{}

    Rectangle {
      color: ThemeController.style.videoPlayerDark.background
      anchors.fill: parent

      VlcPlayer {
        id: vlcPlayer
        onVolumeChanged: console.log("Video volume changed", vlcPlayer.volume)
        onPositionChanged: console.log("Position changed", vlcPlayer.position)
      }

      UIKit.CVideoOutput {
        //      source: fileViewer.player
        vlcPlayer: vlcPlayer
        objectId: fileEditor.fileViewer.id
        source: vlcPlayer
        anchors.fill: parent
      }

      UIKit.LargeIconButton {
        id: closeButton
        anchors {
          left: parent.left
          top: parent.top
          topMargin: ThemeController.style.margin.m20 + safeMargins.top
          leftMargin: ThemeController.style.margin.m24
        }

        height: s(40)
        width: s(56)
        bgRect.radius: ThemeController.style.margin.m16
        buttonStyle: ThemeController.style.videoPlayerDark.buttonStyle
        icon.source: "qrc:/close.svg"
        onClicked: homeManager.back()
      }

      UIKit.LargeIconButton {
        id: volumeButton
        anchors {
          right: parent.right
          top: parent.top
          topMargin: ThemeController.style.margin.m20 + safeMargins.top
          rightMargin: ThemeController.style.margin.m24
        }

        height: s(40)
        width: s(56)
        bgRect.radius: ThemeController.style.margin.m16
        buttonStyle: ThemeController.style.videoPlayerDark.buttonStyle
        icon.source: "qrc:/volume.svg"

        onClicked: volumeSlider.open()

        UIKit.VolumeControl {
          id: volumeSlider
          y: volumeButton.height + ThemeController.style.margin.m8
          x: volumeButton.width * 0.5 - volumeSlider.width * 0.5
          slider.style: ThemeController.style.videoPlayerDark.slider
          style: ThemeController.style.videoPlayerDark.volumeControl
          slider.value: {
            console.log("VOLUME", vlcPlayer.volume)
            return vlcPlayer.volume
          }
          slider.onMoved: {
            console.log("Set new volume", volumeSlider.slider.value)
            vlcPlayer.volume = volumeSlider.slider.value
          }
        }
      }

      UIKit.VideoPlayerToolbar {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: ThemeController.style.margin.m24
        anchors.horizontalCenter: parent.horizontalCenter
        width: Math.min(s(328), parent.width - 2 * ThemeController.style.margin.m24)
        playing: vlcPlayer.playing
        onPlay: {
          if (vlcPlayer.state === VlcPlayer.Paused || vlcPlayer.state === VlcPlayer.Playing)
            vlcPlayer.togglePause()
          else
            vlcPlayer.play(fileEditor.fileViewer.device)
        }
        slider.value: vlcPlayer.state === VlcPlayer.Ended ? 1.0 : vlcPlayer.position
        slider.onMoved: {
          console.log("Slider moved", slider.value)
          vlcPlayer.position = slider.value
        }
        startTxt.text: vlcPlayer.state === VlcPlayer.Ended
                       ? Utility.formatPlayerTime(vlcPlayer.length)
                       : Utility.formatPlayerTime(vlcPlayer.time)
        endTxt.text: Utility.formatPlayerTime(vlcPlayer.length)
        onBack: {
          vlcPlayer.position = Math.max((vlcPlayer.time - 15000.0), 0.0) / vlcPlayer.length
        }
        onForward: {
          vlcPlayer.position = Math.min((vlcPlayer.time + 15000.0), vlcPlayer.length) / vlcPlayer.length
        }
      }
    }
  }
}
