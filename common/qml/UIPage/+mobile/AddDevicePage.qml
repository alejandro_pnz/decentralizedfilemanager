import QtQuick 2.15
import UIFragments 1.0

BasePage {
  id: root

  property bool busy: false

  AddDevice {
    id: addDevice
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
    }
    busy: root.busy
  }
}
