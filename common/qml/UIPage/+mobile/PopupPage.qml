import QtQuick 2.15

AbstractHomePage {
  property var popup
  property bool overrideClose: false
  isPopup: true
  Component.onCompleted: popup.open()
  function close() {
    popup.close()
  }

 Connections {
   target: popup
   enabled: !overrideClose
   ignoreUnknownSignals: true
   function onClosed() {
     homeManager.back()
   }
  }

 DragHandler {
   id: handler
   onActiveChanged: {
     console.log("active changed", handler.active)
     if (handler.active) {
       if (popup.fullHeight) {
         popup.closeDrawer()
       } else {
         popup.fullHeight = true
       }
     }
   }
   target: popup.headerContent
 }
}
