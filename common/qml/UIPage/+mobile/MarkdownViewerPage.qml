import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects


import QmlVlc 0.1
import QtMultimedia 5.12

AbstractHomePage {
  id: root
  page: Pages.AudioViewerPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      height: ThemeController.style.pageHeaderHeight
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true

      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        left: parent.left
        right: parent.right
      }

      rightButton.iconButton.icon.source: ""
      rightButtonHandler: function () {}

      leftButton.iconButton.icon.source: "qrc:/close-gray.svg"
      leftButtonHandler: function() {
        homeManager.back()
      }

      UIKit.BaseText {
        text: fileEditor.name
        font.weight: Font.Bold
        width: parent.width
        elide: Text.ElideRight
        anchors.verticalCenter: parent.verticalCenter
      }
    }
  }

  Item {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
    clip: true

    MarkdownPreview {
      anchors.fill: parent
      fileViewer: fileEditor.fileViewer
    }
  }
}
