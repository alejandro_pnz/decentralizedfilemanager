import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.LinkCloudPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: false
      bottomBorderVisible: true
      leftButtonHandler: function() {
        homeManager.back()
      }

      UIKit.BaseText {
        text: Strings.linkCloudStorage
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  LinkCloud {
    id: fragment
    anchors {
      top: parent.top
      topMargin: 16
      left: parent.left
      right: parent.right
      bottom: parent.bottom
    }
  }
}
