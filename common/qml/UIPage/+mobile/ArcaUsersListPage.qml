import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.ArcaUsersListPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      id: toolbar
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      bottomBorderVisible: true
      leftButtonHandler: function() {
        homeManager.back()
      }
      rightButtonHandler: function() {
        homeManager.abortCurrentFlow()
      }

      Row {
        anchors.centerIn: parent

        UIKit.StyledTabButton {
          checked: true
          text: Strings.whitelist
          onClicked: {
            userListContent.whitelistMode = true
          }
        }

        UIKit.StyledTabButton {
          checked: false
          text: Strings.blacklist
          onClicked: {
            userListContent.whitelistMode = false
          }
        }
      }
      states: [
        State {
          name: "create"
          when: homeManager.creatingObject
          PropertyChanges {
            target: toolbar
            rightButton.mode: UIKit.NavigationBarButton.DisplayMode.LargeIconButton
            leftButton.mode: UIKit.NavigationBarButton.DisplayMode.LargeIconButton
            leftButtonHandler: function() {
              homeManager.back()
            }
            rightButtonHandler: function() {
              homeManager.abortCurrentFlow()
            }
          }
        },
        State {
          name: "edit"
          when: !homeManager.creatingObject
          PropertyChanges {
            target: toolbar
            rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
            rightButton.styledButton.text: Strings.done
            leftButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
            leftButton.styledButton.text: Strings.clearAll
            leftButton.styledButton.textItem.color: ThemeController.style.charadeColor
            leftButtonHandler: function() {
              // Clear selection
            }
            rightButtonHandler: function() {
              // Save the list and back
              homeManager.back()
            }
          }
        }
      ]

    }
  }

  UIKit.BaseFooter{
    id: footer
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    visible: homeManager.creatingObject

    UIKit.StyledButton {
      text: homeManager.creatingQuod ? Strings.saveQuod : Strings.saveArca
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors.right: parent.right
      anchors.left: parent.left
      anchors.rightMargin: ThemeController.style.margin.m16
      anchors.leftMargin: ThemeController.style.margin.m16
      anchors.verticalCenter: parent.verticalCenter
      onClicked: homeManager.saveCurrentFlow()
    }
  }

  ArcaUsersList{
    id: userListContent
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: footer.top
    }
  }
}
