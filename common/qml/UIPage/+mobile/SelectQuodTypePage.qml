import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  title: Strings.selectQuodType
  page: Pages.SelectQuodTypePage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: false
      rightButton.visible: true
      bottomBorderVisible: true
      leftButtonHandler: function() {
        homeManager.back()
      }
      rightButtonHandler: function() {
        homeManager.abortCurrentFlow()
      }

      UIKit.BaseText {
        text: Strings.selectQuodType
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  SelectQuodType {
    id: selectQuod
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
      bottom: parent.bottom
    }
  }
}
