import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.OneDevicePage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader

    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true
      leftButtonHandler: function() {
        homeManager.back()
      }

      rightButton.iconButton.icon.source: "qrc:/more-2.svg"
      rightButtonHandler: function() {
        actionMenu.open()
      }

      UIKit.BaseText {
        text: Strings.oneDevice
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  OneDevice {
    id: deviceContent
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    } 
  }

  ActionMenu {
    id: actionMenu
    model: singleDeviceActionModel.model
    currentItem: deviceEditor.object
  }

  DeviceActionModel {
    id: singleDeviceActionModel
    actionList: oneDeviceActions
  }
}
