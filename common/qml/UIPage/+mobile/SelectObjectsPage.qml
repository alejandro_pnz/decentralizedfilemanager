import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  page: Pages.SelectObjectsPage
  headerComponent: headerComponent
  footerVisible: false
  id: root

  property string headerTitle: Strings.linkedData + " \u2022 " + selectObjectsFragment.selectedCount
  property string headerSubtitle: arcaEditor.object.name

  Connections {
    target: homeManager
    function onSetupSelectObjectsPage(config) {
      if(config.title !== undefined){
        root.headerTitle = config.title
      }
      if(config.subtitle !== undefined){
        root.headerSubtitle = config.subtitle
      }
    }
  }

  Component {
    id: headerComponent
    UIKit.MainToolbar {
      height: ThemeController.style.pageHeaderHeight
      width: parent.width
      leftButton.visible: true
      rightButton.visible: selectObjectsFragment.multipleSelection
      bottomBorderVisible: true
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }

      leftButtonHandler: function () {
        if(selectObjectsFragment.multipleSelection){
        arcaEditor.linkedObjectsModel.clearSelection()
        } else {
          homeManager.back()
        }
      }
      rightButtonHandler: function (){
        //Save and back
        arcaEditor.save()
        homeManager.back()
      }

      ColumnLayout{
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          text: headerTitle
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          font.capitalization: Font.Capitalize
        }

        UIKit.BaseText {
          text: headerSubtitle
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
          visible: text !== ""
        }
      }

      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done
      leftButton.mode: selectObjectsFragment.multipleSelection ?
                       UIKit.NavigationBarButton.DisplayMode.StyledButton : UIKit.NavigationBarButton.DisplayMode.LargeIconButton
      leftButton.styledButton.text: Strings.clearAll
      leftButton.styledButton.textItem.color: ThemeController.style.charadeColor
    }
  }

  SelectObjects {
    id: selectObjectsFragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }
}
