import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.15

AbstractHomePage {
  id: root
  page: Pages.TagsFiltersPage
  headerComponent: pageHeader
  footerVisible: false
  property int selectedItems: fragment.selectionModel ? fragment.selectionModel.selectedIndexes.length : 0

  Connections {
    target: homeManager
    function onSetupFilterPage(config) {
      fragment.searchModel = config.searchModel
    }
  }

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done
      rightButtonHandler: function() {
        fragment.applyFilters()
        homeManager.back()
      }

      leftButtonHandler: function() {
        if (!fragment.accepted) {
          fragment.revertChanges()
        }
        fragment.cleanup()
        homeManager.back()
      }
      bottomBorderVisible: true

      UIKit.BaseText {
        anchors.centerIn: parent
        text: Strings.tags + (selectedItems > 0 ? " \u2022 " + selectedItems : "")
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
        Layout.alignment: Qt.AlignHCenter
      }
    }
  }

  TagsFilters {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }
}
