import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import com.testintl 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.OneQuodPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader

    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true
      rightButton.iconButton.icon.source: "qrc:/more-2.svg"
      rightButtonHandler: function() {
        actionMenu.open()
      }

      Avatars {
        model: UserListModel
        maxCount: 4
        bottomIcon: true
        anchors.centerIn: parent
      }
    }
  }

  OneQuod {
    id: quodContent
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }

  ActionMenu {
    id: actionMenu
    model: actionModel.model
    currentItem: actionModel.currentObject
  }


  ObjectActionModel {
    id: actionModel
    currentObject: quodEditor.object
    actionList: singleActions
  }
}
