import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.DeviceDashboardPage
  headerComponent: pageHeader
  footerVisible: false
  signal openPendingRequests()

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: false
      bottomBorderVisible: true

      UIKit.BaseText {
        text: Strings.myAccount
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  MyAccount {
    id: myAccount
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
      bottom: parent.bottom
    }
    onEnterDeleteAccount: deletePopup.open()
  }

  Item {
    anchors.fill: parent
    UIKit.DeleteAccountPopup {
      id: deletePopup
    }
  }
}
