import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.GlobalDevicesSettingsPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done
      leftButtonHandler: function() {
          homeManager.back()
      }
      bottomBorderVisible: true

      ColumnLayout {
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          text: Strings.devices
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          font.capitalization: Font.Capitalize
        }

        UIKit.BaseText {
          text: Strings.globalSettings
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }
    }
  }

  GlobalDevices {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      topMargin: 16
      bottom: parent.bottom
    }
  }
}
