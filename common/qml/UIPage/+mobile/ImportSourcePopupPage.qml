import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.StyledDrawer {
    id: popup
    visible: false
    contentItemHeight: content.height + ThemeController.style.margin.m24
    horizontalPadding: s(24)

    headerVisible: true
    headerSeparatorVisible: true
    interactive: true

    header: UIKit.BaseText {
      text: Strings.importPhotoFrom
      font.weight: Font.Bold
      Layout.fillWidth: true
    }

    ImportSource {
      id: content
      anchors {
          right: parent.right
          left: parent.left
      }
    }
  }
}
