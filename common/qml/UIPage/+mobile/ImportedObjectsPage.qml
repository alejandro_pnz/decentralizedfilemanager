import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12

AbstractHomePage {
  id: root
  page: Pages.ImportedObjectsPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader

    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: false
      bottomBorderVisible: true
      leftButtonHandler: function() { homeManager.back() }

      UIKit.BaseText {
        text: Strings.importedObjects
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
        anchors.centerIn: parent
      }
    }
  }

  ImportedObjects {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }
}
