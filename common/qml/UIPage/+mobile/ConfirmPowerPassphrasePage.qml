import QtQuick 2.15
import UIFragments 1.0

BasePage {
  id: root

  property bool busy: false

  ConfirmPassphrase {
    busy: root.busy
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
    }
  }
}
