import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.StyledDrawer {
    id: popup
    visible: false
    contentItemHeight: cntHeight
    verticalPadding: 24
    horizontalPadding: 24
    closePolicy: Popup.NoAutoClose
    headerVisible: true
    headerContent.height: 30
    headerSeparatorVisible: false
    interactive: true

    property int cntHeight: content.implicitHeight + footer.height + content.anchors.bottomMargin + 2 * verticalPadding

    ColumnLayout {
      anchors{
        right: parent.right
        left: parent.left
      }
      spacing: 24
      PopupInfoLayoutContent {
        id: content
        title: Strings.confirmResetGlobal
        content: Strings.actionWillRestore
        infoboxText: ""
        Layout.fillWidth: true
      }
      ColumnLayout {
        id: footer
        Layout.fillWidth: true
        spacing: 12

        UIKit.StyledButton {
          Layout.topMargin: 8
          text: Strings.yesReset
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          Layout.fillWidth: true
          onClicked: {
            page.close()
          }
        }

        UIKit.StyledButton {
          text: Strings.keepSettings
          type: UIKit.StyledButton.ButtonStyle.Secondary
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          Layout.fillWidth: true
          onClicked: {
            page.close()
          }
        }
      }
    }
  }
}
