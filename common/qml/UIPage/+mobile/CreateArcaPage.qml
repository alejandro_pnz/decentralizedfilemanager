import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0

AbstractHomePage {
  id: root
  page: Pages.CreateArcaPage
  headerComponent: pageHeader
  footerVisible: false
  signal displayNote()

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      height: ThemeController.style.pageHeaderHeight
      anchors {
        left: parent.left
        right: parent.right
        leftMargin: -horizontalMargins
        rightMargin: -horizontalMargins
      }

      leftButton.visible: fragment.noteContent === "" ? false : true
      rightButton.visible: true
      bottomBorderVisible: true

      leftButtonHandler: function() {
        homeManager.abortCurrentFlow()
      }

      rightButtonHandler: function() {
        homeManager.abortCurrentFlow()
      }

      UIKit.BaseText {
        text: Strings.newArca
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  UIKit.BaseFooter{
    id: footer
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    UIKit.StyledButton {
      text: Strings.saveArca
      type: UIKit.StyledButton.ButtonStyle.Secondary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors.left: parent.left
      anchors.leftMargin: ThemeController.style.margin.m16
      anchors.verticalCenter: parent.verticalCenter
      onClicked: {
        if (fragment.validate()) {
          fragment.save()
          homeManager.saveCurrentFlow()
        }
      }
    }

    UIKit.StyledButton {
      text: Strings.continueTxt
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors.right: parent.right
      anchors.rightMargin: ThemeController.style.margin.m16
      anchors.verticalCenter: parent.verticalCenter
      onClicked: {
        if (fragment.validate()) {
          fragment.save()
          homeManager.openPage(Pages.AddTagsPage)
        }
      }
    }
  }

  CreateArca{
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: footer.top
    }
  }
}
