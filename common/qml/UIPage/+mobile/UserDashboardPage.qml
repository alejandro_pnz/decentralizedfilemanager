import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.UserDashboardPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader

    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true
      rightButton.iconButton.icon.source: "qrc:/more-2.svg"
      rightButtonHandler: function() {
        if(userDashboard.selectionMode){
          selectionMenu.open()
        } else {
          actionMenu.open()
        }
      }

      UIKit.BaseText {
        text: Strings.userDashboard
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  UserDashboard {
    id: userDashboard
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
    givePowerPopup: givePowerPopup
  }

  ActionMenu {
    id: actionMenu
    model: actionModel.model
    headerText.text: Strings.allUsers
  }

  UserActionModel {
    id: actionModel
    actionList: userPageActions
    givePowerPopup: givePowerPopup
    onSelectCurrent: {
      userDashboard.selectCurrent()
    }
  }

  UserActionModel {
    id: selectionModel
    inSelectMode: userDashboard.ism.hasSelection
    actionList: singleUserActions
    selection: userDashboard.ism.selectedIndexes
  }

  SelectionMenu {
    id: selectionMenu
    model: selectionModel.model
    ism: userDashboard.ism
    objectTypeName: Strings.users
    allButton.visible: false
  }

  Item {
    anchors.fill: parent
    UIKit.GivePowerPopup {
      id: givePowerPopup
    }
  }
}
