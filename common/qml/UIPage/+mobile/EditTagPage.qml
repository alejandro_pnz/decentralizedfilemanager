import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.AddTagsPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: false
      rightButton.visible: true
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done
      rightButtonHandler: function() {
        if (fragment.save()) {
          homeManager.back()
        }
      }
      bottomBorderVisible: true

      UIKit.BaseText {
        text: Strings.editTag
        font.weight: Font.Bold
        anchors.centerIn: parent
      }
    }
  }
  EditTag {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }

  Connections {
    target: homeManager
    function onSetupEditTagPage(value) {
      fragment.object = value
    }
  }
}
