import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.SyncPage
  headerComponent: pageHeader
  footerVisible: false

  function save() {
    fragment.save()
    if (homeManager.creatingObject) {
      if (fragment.publicSync && !fragment.publicSyncAllUsers)
        homeManager.openPage(Pages.ArcaUsersListPage)
      else {
        homeManager.saveCurrentFlow()
      }
    } else {
      if (fragment.publicSync && !fragment.publicSyncAllUsers)
        homeManager.openPage(Pages.ArcaUsersListPage)
      else
        homeManager.saveCurrentFlow()
    }
  }

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      id: toolbar
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      width: parent.width
      bottomBorderVisible: true
      leftButtonHandler: function() {
        if (fragment.publicSync)
          fragment.mode = Enums.SyncStatus
        else
          homeManager.back()
      }
      rightButtonHandler: function() { root.save() }

      ColumnLayout{
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          text: {
            if (fragment.publicSync) {
              return Strings.configurePublicSync
            } else {
              if (homeManager.creatingObject) {
                return Strings.selectSyncStatus
              } else {
                return Strings.editSyncStatus
              }
            }
          }
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        UIKit.BaseText {
          text: {
            switch(homeManager.flow) {
            case HomeManager.CreateQuod:
              return Strings.createQuodHeader.arg(QuodTypeModel.currentType.name)
            case HomeManager.CreateArca:
              return Strings.newArca
            case HomeManager.CreateFile:
              return Strings.newFile
            case HomeManager.ManageArca:
              return arcaEditor.object.name
            case HomeManager.ManageQuod:
              return quodEditor.object.name
            case HomeManager.ManageFile:
              return fileEditor.object.name
            default: return ""
            }
          }
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }

      states: [
        State {
          name: "create"
          when: homeManager.creatingObject
          PropertyChanges {
            target: toolbar
            rightButton.mode: UIKit.NavigationBarButton.DisplayMode.LargeIconButton
          }
        },
        State {
          name: "edit"
          when: !homeManager.creatingObject
          PropertyChanges {
            target: toolbar
            rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
            rightButton.styledButton.text: Strings.done
          }
        }
      ]
    }
  }

  UIKit.BaseFooter{
    id: footer
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    visible: homeManager.creatingObject

    UIKit.StyledButton {
      text: {
       if(homeManager.creatingObject) {
        switch(homeManager.flow) {
        case HomeManager.CreateQuod:
          return Strings.saveQuod
        case HomeManager.CreateArca:
          return Strings.saveArca
        case HomeManager.CreateFile:
          return Strings.saveFile
        }
       } else {
         return Strings.done
       }
      }
      type: UIKit.StyledButton.ButtonStyle.Secondary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors.left: parent.left
      anchors.leftMargin: ThemeController.style.margin.m16
      anchors.verticalCenter: parent.verticalCenter
      onClicked: root.save()
    }

    UIKit.StyledButton {
      text: Strings.continueTxt
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors.right: parent.right
      anchors.rightMargin: ThemeController.style.margin.m16
      anchors.verticalCenter: parent.verticalCenter
      onClicked:{
        fragment.save()
        if (fragment.publicSync && !fragment.publicSyncAllUsers)
          homeManager.openPage(Pages.ArcaUsersListPage)
        else
          homeManager.saveCurrentFlow()
      }
    }
  }

  SelectSyncStatus{
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: footer.top
    }
  }
}
