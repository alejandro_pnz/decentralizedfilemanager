import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import UIModel 1.0 as UIModel

AbstractHomePage {
  id: root

  headerComponent: pageHeader
  page: Pages.DataDashboardPage

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: false
      bottomBorderVisible: true
      leftButtonHandler: function() {
        homeManager.back()
      }

      UIKit.LabelComboBox {
        anchors.centerIn: parent
      }
    }
  }

  DataDashboard {
    id: dataFragment
    anchors {
      fill: parent
    }
    horizontalMargins: homePage.horizontalMargins
  }

  UIKit.ChartExtendedPopup {
      id: chartExtended
      visible: false
  }
}
