import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.NameObjectPage
  headerComponent: pageHeader
  footerVisible: false

  property bool renameMode: false
  property int objectsToRenameCount: 1
  property int currentIndex: 1
  property string titleArg: objectsToRenameCount > 1 ? currentIndex + "/"
                            + objectsToRenameCount : ""

  Connections {
    target: homeManager
    function onSetupNameObjectPage(config) {
      page.renameMode = config.renameMode
      page.objectsToRenameCount = config.objectsToRenameCount
    }
  }

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      leftButton.iconButton.icon.source: "qrc:/close.svg"
      rightButton.visible: false
      leftButtonHandler: function() {
        homeManager.back()
      }
      bottomBorderVisible: true

      UIKit.BaseText {
        text: renameMode ? Strings.renameObject.arg(titleArg) : Strings.objectName
        font.weight: Font.Bold
        anchors.centerIn: parent
      }
    }
  }

  UIKit.BaseFooter {
    id: footer
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    UIKit.StyledButton {
      text: renameMode ? objectsToRenameCount === currentIndex ? Strings.done :
            Strings.next : Strings.save
      type: UIKit.StyledButton.ButtonStyle.Primary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors{
        verticalCenter: parent.verticalCenter
        left: parent.left
        right: parent.right
        leftMargin: horizontalMargins
        rightMargin: horizontalMargins
      }
      onClicked: {
      }
    }
  }

  ObjectName {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      topMargin: 16
    }
  }
}
