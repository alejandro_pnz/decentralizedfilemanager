import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.EditPermissionsPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: false
      rightButton.visible: true
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done
      leftButtonHandler: function() {
      }
      rightButtonHandler: function() {
        editPermissions.save()
        homeManager.back()
      }
      bottomBorderVisible: true

      UIKit.BaseText {
        text: Strings.editPermissions
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  EditPermissions {
    id: editPermissions
    anchors {
      fill: parent
      topMargin: ThemeController.style.margin.m12
    }
  }
}
