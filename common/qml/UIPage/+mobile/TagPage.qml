import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  page: Pages.TagPage
  headerComponent: pageHeader
  footerVisible: false

  TagPageFragment {
    id: fragment
  }

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      id: toolbar
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: false
      width: parent.width
      bottomBorderVisible: true
      leftButtonHandler: function() {
          homeManager.back()
      }

      UIKit.Tag {
        anchors.centerIn: parent
        text: fragment.tagName
        tagColor: fragment.tagColor
      }
    }
  }
}
