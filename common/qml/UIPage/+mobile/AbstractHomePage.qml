import QtQuick 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0

Item {
  property int page
  property string title: ""
  property bool searchButton: false
  property bool footerVisible: false
  property string rightBtnSrc: ""
  property bool leftButtonVisible: false
  property string subtitle: ""
  property var footerComponent:  null
  property var headerComponent:  null
  property bool isLandscapeMode: Screen.orientation === Qt.LandscapeOrientation
                                 || Screen.orientation === Qt.InvertedLandscapeOrientation
  property real horizontalMargins: ThemeController.style.pageHorizontalMargin
  property bool isPopup: false

  UIKit.NotificationDelegate {
    id: notification
    anchors {
      right: parent.right
      left: parent.left
      bottom: parent.bottom
      bottomMargin: ThemeController.style.margin.m24
    }
    visible: false
    contentText: Strings.copied
    icon.source: "qrc:/duplicate-white.svg"
    notificationStyle: ThemeController.style.notificationDelegateGreen
    z:99
    interval: 2000
    Connections {
      target: app.clipboard
      function onCopied() {
        if(homeManager.isPageOnTop(page))
          notification.show()
      }
    }
  }
}
