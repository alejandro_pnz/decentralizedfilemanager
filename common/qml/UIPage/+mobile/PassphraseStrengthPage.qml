import QtQuick 2.0
import UIFragments 1.0
import com.test 1.0
import Qt5Compat.GraphicalEffects

import UIKit 1.0 as UIKit
import AppStyles 1.0

BasePage {
  id: root
  PassphraseStrength {
    id: passphraseStrength
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
    }
    onShowHint: {
      drawer.open()
    }
  }
  Item {
    anchors.fill: parent
    UIKit.StyledDrawer {
      id: drawer
      contentItemHeight: 0.8 * parent.height

      Tip {
        id: tip
        anchors {
          top: parent.top
          left: parent.left
          right: parent.right
          bottom: parent.bottom
        }
        onHandleHideHint: {
          drawer.close()
        }
        headerText: Strings.quodArcaPowerPassphrase
        itemsModel: objectsModel

        Component.onCompleted: {
          objectsModel.append({ titleTxt: Strings.gainExclusiveAccess,
                                contentTxt: Strings.extraFeatures,
                               imageSrc: "qrc:/illustrations/r35.png"});
          objectsModel.append({ titleTxt: Strings.manageUsers,
                                contentTxt: Strings.usingThePowePassphrase,
                               imageSrc: "qrc:/illustrations/r36.png"});
          objectsModel.append({ titleTxt: Strings.loginReset,
                                contentTxt: Strings.powerPassphraseKnowledge,
                               imageSrc: "qrc:/illustrations/r37.png"});
        }

        ListModel{
          id: objectsModel
        }
      }
    }
  }
}
