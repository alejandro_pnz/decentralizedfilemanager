import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15

AbstractHomePage {
  id: root
  page: Pages.LogsPage
  headerComponent: pageHeader
  footerVisible: false
  title: "Logs"

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: false
      leftButtonHandler: function() {
        homeManager.back()
      }
      bottomBorderVisible: true
    }
  }

  ScrollView {
    id: scrollView
    anchors{
      fill: parent
      topMargin: ThemeController.style.margin.m16
    }
    TextArea {
      id: noteArea
      color: ThemeController.style.font.defaultColor
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      text: app.logs
      textFormat: Qt.RichText
      selectByMouse: true
      selectedTextColor: ThemeController.style.whiteColor
      persistentSelection: true

      font {
        pixelSize: ThemeController.style.font.bodySize
        family: ThemeController.style.font.primaryFontFamily
      }
      onTextChanged: noteArea.cursorPosition = noteArea.length - 1
    }
  }
}
