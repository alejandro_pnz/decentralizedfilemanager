import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  title: Strings.selectQuodType
  page: Pages.SelectQuodTypePage
  headerComponent: pageHeader
  footerVisible: false
  signal createAttribute()

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: false
      rightButton.visible: true
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done
      bottomBorderVisible: true
      rightButtonHandler: function() {
        homeManager.back()
      }

      ColumnLayout {
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          text: Strings.selectAttribute
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        UIKit.BaseText {
          text: Strings.customQuod
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }
    }
  }

  Connections {
    target: homeManager
    onCreateCustomAttribute: homeManager.enterCustomAttributePage(name)
  }

  QuodAttributes {
    id: quodAttributes
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
      bottom: parent.bottom
    }
    buttonGroup.onClicked: homeManager.back()
  }
}
