import QtQuick 2.0
import UIFragments 1.0
BasePage {
  id: root

  property bool busy: false

  DeviceStatus {
    busy: root.busy
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
    }
  }
}
