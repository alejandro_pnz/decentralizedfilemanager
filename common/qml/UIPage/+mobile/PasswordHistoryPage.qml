import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.PasswordHistoryPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      id: toolbar
      height: ThemeController.style.pageHeaderHeight
      anchors{
        left: parent.left
        right: parent.right
        leftMargin: -horizontalMargins
        rightMargin: -horizontalMargins
      }
      bottomBorderVisible: true
      leftButton.visible: true
      rightButton.visible: true
      leftButtonHandler: function() {
        fragment.close()
      }
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done
      rightButtonHandler: function() {
        fragment.save()
      }

      ColumnLayout {
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          text: Strings.passwordHistory
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        UIKit.BaseText {
          text: Strings.createQuodHeader.arg(quodEditor.object.name)
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }
    }
  }


  PasswordHistory {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }
}
