import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.OneFilePage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader

    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true
      rightButton.iconButton.icon.source: "qrc:/more-2.svg"
      rightButtonHandler: function () {
        actionMenu.open()
      }

      Avatars {
        model: UserListModel
        maxCount: 4
        bottomIcon: true
        anchors.centerIn: parent
      }
    }
  }

  OneFile {
    id: fileContent
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }

  ActionMenu {
    id: actionMenu
    model: actionModel.model
    currentItem: actionModel.currentObject
  }

  ObjectActionModel {
    id: actionModel
    currentObject: fileEditor.object
    actionList: singleActions
  }

  UIKit.BaseFooter {
    id: footer
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }
    visible: fileEditor.fileViewer.type === FileViewerClass.MarkdownType

    UIKit.StyledButton {
      anchors {
        right: parent.right
        left: parent.left
        leftMargin: horizontalMargins
        rightMargin: horizontalMargins
        verticalCenter: parent.verticalCenter
      }
      text: Strings.editInQuodArca
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      onClicked: {
        homeManager.enterEditMarkdownFile(fileEditor.object.id)
      }
    }
  }
}
