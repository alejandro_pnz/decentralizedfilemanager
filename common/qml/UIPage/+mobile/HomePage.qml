import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import com.testintl 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12
import Qt.labs.platform 1.1

BasePage {
  id: root

  property alias homeStackView: stackView.homeStackView

  Item {
    Connections {
      target: homeManager
      function onSetupFileDialog() {
        fileDialog.open()
      }
    }
  }

  header: ToolBar {
    height: ThemeController.style.pageHeaderHeight
    background: Item{}

    Item {
      id: content
      anchors {
        leftMargin: ThemeController.style.pageHorizontalMargin
        rightMargin: ThemeController.style.pageHorizontalMargin
        fill: parent
      }

      Loader {
        id: loader
        anchors.fill: parent
        sourceComponent: homeStackView.current.headerComponent
        visible: loader.sourceComponent !== null
      }
    }
  }

  footer: UIKit.BottomMenu {
    width: parent.width
    onAddButtonClicked: homeManager.openPage(Pages.AddMenuPopupPage)
    onBoardsButtonClicked: homeManager.openPage(Pages.DeviceDashboardPage)// homeManager.openPage(Pages.BoardsMenuPopupPage)
    visible: homeStackView.current.footerVisible
  }

  HomeStackView {
    id: stackView
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }

  Item {
    anchors.fill: parent
    FileDialog {
      id: fileDialog
      acceptLabel: Strings.import
      folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
      fileMode: FileDialog.OpenFiles
      onAccepted: {
        console.log("IMPORT FILES", files);
        ImportManager.importFiles(files);
      }
    }
  }
}
