import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import UIModel 1.0 as UIModel

AbstractHomePage {
  id: root
  title: Strings.library
  page: Pages.LibraryPage
  headerComponent: pageHeader
  footerVisible: true
  signal showSearchScreen()

  Library {
    id: libraryFragment
    anchors {
      fill: parent
      topMargin: ThemeController.style.margin.m16
    }
    horizontalMargins: root.horizontalMargins
    horizontalSpacing: s(8)
    verticalSpacing: ThemeController.style.margin.m12
  }

  UIModel.ObjectActionModel {
    id: actionModel
    objectType: libraryFragment.objectType
    actionList: selectMenuAction
    selection: libraryFragment.ism.selectedIndexes
    onOpenExportPopup: {
      homeManager.openPage(Pages.ExportQuodPage)
    }
  }

  SelectionMenu {
    id: actionMenu
    model: actionModel.model
    ism: libraryFragment.ism
    objectTypeName: libraryFragment.objectTypeName
  }

  Component {
    id: pageHeader
    LibraryHeader {
      id: header
      selectionMode: libraryFragment.selectionMode
      ism: libraryFragment.ism
      objectTypeName: libraryFragment.objectTypeName
      selectAllButton: false
      onSearchButtonClicked: showSearchScreen()
    }
  }
}
