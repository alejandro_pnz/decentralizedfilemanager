import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.LockStatusPage
  headerComponent: pageHeader
  footerVisible: false
  property int selectedItems: fragment.selectedCount

  Connections {
    target: homeManager
    function onSetupFilterPage(config) {
      fragment.searchModel = config.searchModel
    }
  }

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      leftButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      leftButton.styledButton.text: Strings.clearAll
      leftButton.styledButton.buttonStyle: ThemeController.style.clearTextButtonStyle
      rightButton.visible: true
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done

      rightButtonHandler: function() {
        fragment.applyFilters()
        homeManager.back()
      }

      leftButtonHandler: function() {
        fragment.clearAll()
      }
      bottomBorderVisible: true

      UIKit.BaseText {
        anchors.centerIn: parent
        text: Strings.lockStatus + (selectedItems > 0 ? " \u2022 " + selectedItems : "")
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
        Layout.alignment: Qt.AlignHCenter
      }
    }
  }

  LockStatus {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      topMargin: 12
      bottom: parent.bottom
    }
  }
}
