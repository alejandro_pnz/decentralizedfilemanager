import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.PdfViewerPage
  headerComponent: pageHeader
  footerVisible: false

  property bool searchMode: false

  Component {
    id: pageHeader
    PdfViewerHeader {
      id: header
      onMoreClicked: actionMenu.open()
      onBackClicked: homeManager.back()
      onDoneClicked: root.searchMode = false
      searchMode: root.searchMode
    }
  }

  DocumentViewer {
    id: fragment
    fileViewer: fileEditor.fileViewer
    anchors {
      left: parent.left
      right: parent.right
      bottom: parent.bottom
      top: parent.top
      leftMargin: -ThemeController.style.pageHorizontalMargin
      rightMargin: -ThemeController.style.pageHorizontalMargin
    }
  }

  UIKit.PageSwitch {
    anchors.bottom: fragment.bottom
    anchors.bottomMargin: ThemeController.style.margin.m24
    anchors.horizontalCenter: parent.horizontalCenter
    pagesCount: fragment.fileViewer.pageCount
    currentPage: fragment.fileViewer.currentPage + 1
    onForward: fragment.nextPage()
    onBack: fragment.previousPage()
    onLastPage: fragment.lastPage()
    onFirstPage: fragment.firstPage()
  }

  ActionMenu {
    id: actionMenu
    model: actionModel.model
  }

  PdfViewerActionModel {
    id: actionModel
    onSearchMode: root.searchMode = true
  }
}
