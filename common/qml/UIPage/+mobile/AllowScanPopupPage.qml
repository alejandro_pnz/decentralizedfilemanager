import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  id: root
  page: Pages.AllowScanPopupPage
  footerVisible: false
  headerComponent: pageHeader

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: false
      rightButton.visible: true
      width: parent.width
      bottomBorderVisible: true
      rightButtonHandler: function() {
        homeManager.back()
      }

      UIKit.BaseText {
        anchors.centerIn: parent
        text: Strings.accessToContent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
        Layout.alignment: Qt.AlignHCenter
      }
    }
  }

  PopupInfoLayoutContent {
    id: content
    anchors.top: parent.top
    anchors.topMargin: ThemeController.style.margin.m16
    anchors.left: parent.left
    anchors.right: parent.right
  }

  ColumnLayout {
    id: footer
    anchors.bottom: parent.bottom
    width: parent.width
    spacing: ThemeController.style.margin.m12

    RowLayout {
      spacing: ThemeController.style.margin.m8
      UIKit.BaseText {
        text: Strings.applyThisChoiceToFiles
        font.weight: Font.DemiBold
        Layout.fillWidth: true
      }
      UIKit.SwitchButton {
        id: switchButton
      }
    }

    UIKit.StyledButton {
      Layout.topMargin: ThemeController.style.margin.m20
      text: Strings.yesAllow
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      Layout.fillWidth: true
      onClicked: {
        homeManager.back()
      }
    }

    UIKit.StyledButton {
      text: Strings.noDontAllow
      type: UIKit.StyledButton.ButtonStyle.Secondary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      Layout.fillWidth: true
      onClicked: {
        homeManager.back()
      }
    }
  }
}
