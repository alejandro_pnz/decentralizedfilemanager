import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.StyledDrawer {
    id: popup
    visible: false
    contentItemHeight: cntHeight
    verticalPadding: 24
    horizontalPadding: 24
    closePolicy: Popup.NoAutoClose
    headerVisible: false
    headerSeparatorVisible: false
    interactive: false

    property int cntHeight: content.implicitHeight + footer.height + content.anchors.bottomMargin + 2 * verticalPadding

    ColumnLayout {
      anchors{
        right: parent.right
        left: parent.left
      }
      spacing: 16
      PopupInfoLayoutContent {
        id: content
        title: Strings.unlockAuthorization
        content: Strings.unlockAuthorizationExpires
        infoboxText: ""
        Layout.fillWidth: true
      }

      ColumnLayout {
        id: footer
        Layout.fillWidth: true
        spacing: 12

        Rectangle {
          Layout.fillWidth: true
          color: ThemeController.style.mercuryColor
          Layout.preferredHeight: 1
        }

        Row {
          spacing: 12
          Layout.alignment: Qt.AlignHCenter

          UIKit.SvgImage {
            width: 24
            height: 24
            source: "qrc:/clock.svg"
          }

          UIKit.BaseText {
            text: Strings.hourMinutes.arg("1").arg("20")
            font.weight: Font.Bold
          }
        }

        Rectangle {
          Layout.topMargin: -4
          Layout.fillWidth: true
          color: ThemeController.style.mercuryColor
          Layout.preferredHeight: 1
        }

        UIKit.StyledButton {
          Layout.topMargin: 16
          text: Strings.okGotIt
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          Layout.fillWidth: true
          onClicked: {
            page.close()
          }
        }

        UIKit.StyledButton {
          Layout.topMargin: -4
          text: Strings.cancelUnlock
          type: UIKit.StyledButton.ButtonStyle.Secondary
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          Layout.fillWidth: true
          onClicked: {
            page.close()
          }
        }
      }
    }
  }
}
