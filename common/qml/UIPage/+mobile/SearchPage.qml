import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import UIModel 1.0 as UIModel

AbstractHomePage {
  id: root
  title: Strings.search
  page: Pages.SearchPage
  headerComponent: pageHeader
  footerVisible: false

  Search {
    id: searchFragment
    anchors {
      fill: parent
    }
  }

  Component {
    id: pageHeader
    SearchHeader {
      id: header
      onFilterClicked: filters.open()
      searchModel: searchFragment.sModel
      Component.onCompleted: {
        searchFragment.searching = Qt.binding(function(){
          return header.searchInput.preeditText.length > 0 || header.searchInput.text.length > 0
        })
      }
    }
  }

  Item {
    anchors.fill: parent
    SearchFiltersMenu {
      id: filters
      searchModel: searchFragment.sModel
    }
  }
}
