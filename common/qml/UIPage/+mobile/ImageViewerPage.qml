import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.PdfViewerPage
  headerComponent: pageHeader
  footerVisible: false

  property bool searchMode: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      height: ThemeController.style.pageHeaderHeight
      leftButton.visible: true
      rightButton.visible: false
      bottomBorderVisible: true

      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        left: parent.left
        right: parent.right
      }

      rightButton.iconButton.icon.source: ""
      rightButtonHandler: function () {}

      leftButton.iconButton.icon.source: "qrc:/close-gray.svg"
      leftButtonHandler: function() {
        homeManager.back()
      }

      UIKit.BaseText {
        text: fileEditor.name
        font.weight: Font.Bold
        width: parent.width
        elide: Text.ElideRight
        anchors.verticalCenter: parent.verticalCenter
      }
    }
  }

  ImageViewer {
    id: fragment
    fileViewer: fileEditor.fileViewer
    anchors {
      left: parent.left
      right: parent.right
      bottom: parent.bottom
      top: parent.top
      leftMargin: -ThemeController.style.pageHorizontalMargin
      rightMargin: -ThemeController.style.pageHorizontalMargin
    }
  }
}
