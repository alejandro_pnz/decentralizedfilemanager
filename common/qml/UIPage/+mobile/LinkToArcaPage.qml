import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.LinkToArcaPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      leftButton.iconButton.icon.source: "qrc:/close-gray.svg"
      leftButton.iconButton.iconHeight: 32
      leftButton.iconButton.iconWidth: 32
      rightButton.visible: true
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done
      leftButtonHandler: function() {
        fragment.clearSelection()
        homeManager.back()
      }
      rightButtonHandler: function() {
        fragment.save()
        homeManager.saveCurrentFlow()
      }
      bottomBorderVisible: true

      UIKit.BaseText {
        text: Strings.linkToArca + " \u2022 " + fragment.selectedCount
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  LinkToArca {
    id: fragment
    anchors.fill: parent
  }
}
