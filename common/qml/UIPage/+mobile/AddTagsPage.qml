import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.AddTagsPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      id: toolbar
      height: ThemeController.style.pageHeaderHeight
      anchors {
        left: parent.left
        right: parent.right
        leftMargin: -horizontalMargins
        rightMargin: -horizontalMargins
      }

      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true      

      ColumnLayout{
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          id: title
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        UIKit.BaseText {
          id: subtitle
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }

      states: [
        State {
          name: "create"
          when: homeManager.creatingObject
          PropertyChanges {
            target: toolbar
            rightButton.mode: UIKit.NavigationBarButton.DisplayMode.LargeIconButton
            leftButton.mode: UIKit.NavigationBarButton.DisplayMode.LargeIconButton
            leftButtonHandler: function() {
              homeManager.back()
            }
            rightButtonHandler: function() {
              homeManager.abortCurrentFlow()
            }
          }
          PropertyChanges {
            target: title
            text: Strings.addTags
          }
          PropertyChanges {
            target: subtitle
            text: {
              switch(homeManager.flow) {
              case HomeManager.CreateArca:
                return Strings.newArca
              case HomeManager.CreateQuod:
                return Strings.createQuodHeader.arg(QuodTypeModel.currentType.name)
              case HomeManager.CreateFile:
                return Strings.newFile
              }
            }
          }
        },
        State {
          name: "edit"
          when: !homeManager.creatingObject
          PropertyChanges {
            target: toolbar
            rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
            rightButton.styledButton.text: Strings.done
            leftButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
            leftButton.styledButton.text: Strings.clearAll
            leftButton.styledButton.textItem.color: ThemeController.style.charadeColor
            leftButtonHandler: function() {
              // Clear selection
              homeManager.currentEditor.taglistModel.deleteAllTags()
            }
            rightButtonHandler: function() {
              // Save the list and back
              homeManager.saveCurrentFlow()
            }
          }
          PropertyChanges {
            target: title
            text: Strings.editTags
          }
          PropertyChanges {
            target: subtitle
            text: homeManager.currentEditor.name
          }
        }
      ]
    }
  }

  UIKit.BaseFooter{
    id: footer
    visible: homeManager.creatingObject
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    UIKit.StyledButton {
      text: {
        if(homeManager.creatingObject) {
         switch(homeManager.flow) {
         case HomeManager.CreateQuod:
           return Strings.saveQuod
         case HomeManager.CreateArca:
           return Strings.saveArca
         case HomeManager.CreateFile:
           return Strings.saveFile
         }
        } else {
          return Strings.done
        }
      }
      type: UIKit.StyledButton.ButtonStyle.Secondary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors.left: parent.left
      anchors.leftMargin: ThemeController.style.margin.m16
      anchors.verticalCenter: parent.verticalCenter
      onClicked:  {
        homeManager.saveCurrentFlow()
      }
    }

    UIKit.StyledButton {
      text: homeManager.creatingObject ? Strings.continueTxt : Strings.done
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors.right: parent.right
      anchors.rightMargin: ThemeController.style.margin.m16
      anchors.verticalCenter: parent.verticalCenter
      onClicked: {
        homeManager.openPage(Pages.SyncPage)
      }
    }
  }

  AddTags {
    id: arcaContent
    onEditTag: homeManager.openPage(Pages.EditTagPage)
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: footer.top
    }
  }
}
