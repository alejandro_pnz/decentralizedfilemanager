import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.StyledDrawer {
    id: popup
    visible: false
    contentItemHeight: 0.88 * parent.height
    verticalPadding: ThemeController.style.margin.m24
    horizontalPadding: ThemeController.style.margin.m24
    closePolicy: Popup.NoAutoClose
    headerVisible: false
    headerSeparatorVisible: false
    interactive: false

    ColumnLayout {
      id: content
      anchors{
        right: parent.right
        left: parent.left
      }
      height: parent.height
      spacing: ThemeController.style.margin.m24

      UIKit.LargeIconButton {
        id: closeButton
        icon.source: "qrc:/close.svg"
        Layout.preferredHeight: s(32)
        Layout.preferredWidth: s(32)
        Layout.alignment: Qt.AlignRight
        onClicked: popup.close()
      }

      CheckNotifications {
        id: fragment
        Layout.fillWidth: true
        Layout.fillHeight: true
      }
    }
  }
}
