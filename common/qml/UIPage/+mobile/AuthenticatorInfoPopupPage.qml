import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.StyledDrawer {
    id: popup
    visible: false
    contentItemHeight: 0.88 * parent.height
    verticalPadding: 0
    topPadding: 0
    horizontalPadding: 24
    closePolicy: Popup.NoAutoClose
    headerVisible: false
    headerSeparatorVisible: false
    interactive: false

    ColumnLayout {
      id: content
      anchors{
        right: parent.right
        left: parent.left
      }
      height: parent.height
      spacing: 24

      Item {
        Layout.preferredHeight: 64
        Layout.fillWidth: true

        UIKit.BaseText {
          text: Strings.authenticator
          font.weight: Font.Bold
          anchors.centerIn: parent
        }

        UIKit.LargeIconButton {
          id: headerCloseBtn
          anchors.right: parent.right
          anchors.verticalCenter: parent.verticalCenter

          icon.source: "qrc:/close.svg"
          onClicked: {
            popup.close()
          }
        }

        Rectangle {
          anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
          }
          color: ThemeController.style.mercuryColor
          height: 1
        }
      }

      UIKit.TipBox {
        Layout.fillWidth: true
        title: Strings.howDoesAuthenticatorWork
        content: Strings.configuringAuthenticator
        spacerVisible: false
        imageSource: "qrc:/illustrations/u8.png"

        UIKit.StyledButton {
          text: Strings.gotIt
          Layout.fillWidth: true
          Layout.topMargin: 20
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        }

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true
        }
      }
    }
  }
}
