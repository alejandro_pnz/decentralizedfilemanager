import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.EditGeneralDetailsPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      height: ThemeController.style.pageHeaderHeight      
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true

      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        left: parent.left
        right: parent.right
      }

      ColumnLayout{
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          text: Strings.generalDetails
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        UIKit.BaseText {
          text: Strings.arca
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }

      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done

      rightButtonHandler: function() {
        // Save details and back
        if (editDetailsFragment.validate()) {
          editDetailsFragment.save()
          homeManager.back()
        }
      }

      leftButtonHandler: function() {
        homeManager.back()
      }

    }
  }

  EditGeneralDetails {
    id: editDetailsFragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }
}
