import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  page: Pages.LinkObjectsPage
  headerComponent: headerComponent
  footerVisible: false
  id: root

  Component {
    id: headerComponent
    UIKit.MainToolbar {
      height: ThemeController.style.pageHeaderHeight
      width: parent.width
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }

      leftButtonHandler: function () {
        arcaEditor.linkedObjectsModel.clearSelection()
      }
      rightButtonHandler: function (){
        //Save and back
        arcaEditor.save()
        homeManager.back()
      }

      ColumnLayout{
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          text: Strings.linkedData + " \u2022 " + linkObjectsFragment.selectedCount
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          font.capitalization: Font.Capitalize
        }

        UIKit.BaseText {
          text: arcaEditor.object.name
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }

      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done
      leftButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      leftButton.styledButton.text: Strings.clearAll
      leftButton.styledButton.textItem.color: ThemeController.style.charadeColor
    }
  }



  LinkObjects {
    id: linkObjectsFragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }
}
