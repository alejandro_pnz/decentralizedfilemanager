import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

BasePage {
  id: root

  header: ShareHeader { }
  Share {
    id: changeOwnerFragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }

    onInfoButtonClicked: drawer.open()
  }

  Item {
    anchors.fill: parent
    UIKit.StyledDrawer {
      id: drawer
      contentItemHeight: shareInfo.implicitHeight + s(48)
      ShareInfoModal {
        id: shareInfo
        anchors {
          top: parent.top
          left: parent.left
          right: parent.right
          topMargin: ThemeController.style.margin.m24
          leftMargin: ThemeController.style.pageHorizontalMargin
          rightMargin: ThemeController.style.pageHorizontalMargin
        }
        onAccept: drawer.close()
      }
    }
  }
}
