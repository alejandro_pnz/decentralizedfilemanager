import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.DateFilterPage
  headerComponent: pageHeader
  footerVisible: false

  Connections {
    target: homeManager
    function onSetupFilterPage(config) {
      fragment.searchModel = config.searchModel
      fragment.dateFilterType = config.type
    }
  }

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done
      rightButtonHandler: function() {
        console.log("Apply filters")
        fragment.applyFilters()
        homeManager.back()
      }

      leftButtonHandler: function() {
        fragment.cleanup()
        homeManager.back()
      }
      bottomBorderVisible: true

      UIKit.BaseText {
        anchors.centerIn: parent
        text: Strings.modified
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
        Layout.alignment: Qt.AlignHCenter
      }
    }
  }

  DateFilter {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      topMargin: 12
      bottom: parent.bottom
    }
  }
}
