import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import Qt5Compat.GraphicalEffects


import QmlVlc 0.1
import QtMultimedia 5.12

AbstractHomePage {
  id: root
  page: Pages.AudioViewerPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      height: ThemeController.style.pageHeaderHeight
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true

      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        left: parent.left
        right: parent.right
      }

      rightButton.iconButton.icon.source: ""
      rightButtonHandler: function () {}

      leftButton.iconButton.icon.source: "qrc:/close-gray.svg"
      leftButtonHandler: function() {
        homeManager.back()
      }

      UIKit.BaseText {
        text: fileEditor.name
        font.weight: Font.Bold
        width: parent.width
        elide: Text.ElideRight
        anchors.verticalCenter: parent.verticalCenter
      }
    }
  }

  Item {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
    clip: true

    VlcPlayer {
      id: vlcPlayer
      onVolumeChanged: console.log("Video volume changed", vlcPlayer.volume)
      onPositionChanged: console.log("Position changed", vlcPlayer.position)
    }

    UIKit.CVideoOutput {
      //      source: fileViewer.player
      visible: false
      vlcPlayer: vlcPlayer
      objectId: fileEditor.fileViewer.id
      source: vlcPlayer
      anchors.fill: parent
    }

    Rectangle {
      anchors {
        fill: image
        topMargin: -ThemeController.style.margin.m48
        bottomMargin: -ThemeController.style.margin.m48
      }
      color: "#f6f7f9"
      border.width: s(1)
      border.color: ThemeController.style.whisperColor
      radius: s(4)
    }

    Image {
      id: image
      anchors.centerIn: parent
      width: parent.width
      height: sourceSize.height / sourceSize.width * parent.width
      // TODO remove temporary placeholder for mp3 when waves generator will be ready
      source: fileEditor.object.fileType == "mp3"
              ? "qrc:/audio_preview_waves.png"
              : "image://photo/" + fileEditor.object.id
      fillMode: Image.PreserveAspectFit
    }

    UIKit.VideoPlayerToolbar {
      anchors.bottom: parent.bottom
      anchors.bottomMargin: ThemeController.style.margin.m24
      anchors.horizontalCenter: parent.horizontalCenter
      componentStyle: ThemeController.style.audioPlayerLight
      width: Math.min(s(328), parent.width - 2 * ThemeController.style.margin.m24)
      playing: vlcPlayer.playing
      onPlay: {
        if (vlcPlayer.state === VlcPlayer.Paused || vlcPlayer.state === VlcPlayer.Playing)
          vlcPlayer.togglePause()
        else
          vlcPlayer.play(fileEditor.fileViewer.device)
      }
      slider.value: vlcPlayer.state === VlcPlayer.Ended ? 1.0 : vlcPlayer.position
      slider.onMoved: {
        console.log("Slider moved", slider.value)
        vlcPlayer.position = slider.value
      }
      startTxt.text: vlcPlayer.state === VlcPlayer.Ended
                     ? Utility.formatPlayerTime(vlcPlayer.length)
                     : Utility.formatPlayerTime(vlcPlayer.time)
      endTxt.text: Utility.formatPlayerTime(vlcPlayer.length)
      onBack: {
        vlcPlayer.position = Math.max((vlcPlayer.time - 15000.0), 0.0) / vlcPlayer.length
      }
      onForward: {
        vlcPlayer.position = Math.min((vlcPlayer.time + 15000.0), vlcPlayer.length) / vlcPlayer.length
      }
    }
  }
}
