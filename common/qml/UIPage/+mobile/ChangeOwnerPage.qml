import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import com.testintl 1.0

AbstractHomePage {
  id: root
  page: Pages.ChangeOwnerPage
  headerComponent: pageHeader
  footerVisible: false

  property string subtitle: objectType === SyncableObject.Arca ? Strings.arca :
                            objectType === SyncableObject.Quod ? Strings.quod : ""
  property int objectType: SyncableObject.Arca

  Connections {
    target: homeManager
    function onSetupChangeOwnerPage(type) {
      root.objectType = type
    }
  }

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: Strings.done
      bottomBorderVisible: true
      leftButtonHandler: function() {
        homeManager.back()
      }
      rightButtonHandler: function() {
        homeManager.back()
      }

      ColumnLayout{
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          text: Strings.changeOwner
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        UIKit.BaseText {
          text: subtitle
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }
    }
  }

  ChangeOwner {
    id: changeOwnerFragment
    anchors {
      fill: parent
    }
  }
}
