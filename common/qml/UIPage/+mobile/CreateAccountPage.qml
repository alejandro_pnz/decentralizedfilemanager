import QtQuick 2.0
import UIFragments 1.0
import com.test 1.0
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit

BasePage {
  id: root

  property bool busy: false

  NewAccount {
    title: Strings.createAccount
    buttonText: Strings.createAnAccount
    busy: root.busy
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
    }
    onShowHint: {
      drawer.open()
    }
  }
  Item {
    anchors.fill: parent
    UIKit.StyledDrawer {
      id: drawer
      contentItemHeight: 0.8 * parent.height
      Tip {
        id: tip
        anchors {
          top: parent.top
          left: parent.left
          right: parent.right
          bottom: parent.bottom
        }
        onHandleHideHint: {
          drawer.close()
        }
      }
    }
  }
}
