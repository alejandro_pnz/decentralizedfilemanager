import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.OverwriteObjectPage
  headerComponent: pageHeader
  footerVisible: false
  property string titleArg: fragment.conflictsNumber > 1 ?
                            "1/" + fragment.conflictsNumber : ""

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      leftButton.iconButton.icon.source: "qrc:/close.svg"
      rightButton.visible: false
      leftButtonHandler: function() {
          homeManager.back()
      }
      bottomBorderVisible: true

        UIKit.BaseText {
          text: Strings.nameConflict.arg(titleArg)
          font.weight: Font.Bold
          anchors.centerIn: parent
        }
    }
  }

  OverwriteObject {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }
}
