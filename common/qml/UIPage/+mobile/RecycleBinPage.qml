import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.RecycleBinPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader

    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true
      rightButton.iconButton.icon.source: "qrc:/more-2.svg"
      rightButtonHandler: function() {
        if(recycleBin.selectionMode){
          selectionMenu.open()
        } else {
          actionMenu.open()
        }
      }

      UIKit.BaseText {
        text: Strings.recycleBin
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  RecycleBin {
    id: recycleBin
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }

  ActionMenu {
    id: actionMenu
    model: actionModel.model
    headerText.text: Strings.allObjects
  }

  RecycleBinActionModel {
    id: actionModel
    actionList: recycleBinPageActions
    onSelectCurrent: {
      recycleBin.selectCurrent()
    }
  }

  RecycleBinActionModel {
    id: selectionModel
    inSelectMode: recycleBin.ism.hasSelection
    actionList: singleRecycledItemActions
    selection: recycleBin.ism.selectedIndexes
  }

  SelectionMenu {
    id: selectionMenu
    model: selectionModel.model
    ism: recycleBin.ism
    objectTypeName: Strings.objectsStr
    allButton.visible: false
  }
}
