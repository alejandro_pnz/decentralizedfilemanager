import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  page: Pages.InviteToAccountPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      leftButton.iconButton.icon.source: "qrc:/close.svg"
      rightButton.visible: false
      leftButtonHandler: function() {
        homeManager.back()
      }
      bottomBorderVisible: true

      UIKit.BaseText {
        text: Strings.inviteToAccount
        font.weight: Font.Bold
        anchors.centerIn: parent
      }
    }
  }

  UIKit.BaseFooter {
    id: footer
    height: 80
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    UIKit.StyledButton {
      text: Strings.sendRequest
      type: UIKit.StyledButton.ButtonStyle.Primary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors{
        verticalCenter: parent.verticalCenter
        left: app.isTablet ? undefined : parent.left
        right: parent.right
        leftMargin: horizontalMargins
        rightMargin: horizontalMargins
      }
      width: app.isTablet ? 146 : undefined
    }
  }

  InviteUserDetails {
    id: fragment
    anchors {
      top: parent.top
      topMargin: 16
      bottom: footer.top
      left: parent.left
      right: parent.right
      leftMargin: -ThemeController.style.pageHorizontalMargin
      rightMargin: -ThemeController.style.pageHorizontalMargin
    }
  }
}
