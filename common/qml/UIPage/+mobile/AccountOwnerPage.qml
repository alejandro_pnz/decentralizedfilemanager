import QtQuick 2.15
import UIFragments 1.0
BasePage {
  id: root
  AccountOwner {
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      topMargin: s(31)
    }
  }
}
