import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.NotePage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      height: ThemeController.style.pageHeaderHeight
      width: parent.width

      ColumnLayout{
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          text: homeManager.flow === HomeManager.CreateFile
                ? Strings.newTextFile
                : (noteFragment.noteMode ? Strings.addNotes : fileEditor.name)
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        UIKit.BaseText {
          text: {
            if (!noteFragment.noteMode) return ""
            switch(homeManager.flow) {
            case HomeManager.CreateQuod:
              return QuodTypeModel.currentType.name
            case HomeManager.CreateArca:
              return Strings.newArca
            case HomeManager.CreateFile:
              return Strings.newFile
            case HomeManager.ManageArca:
              return arcaEditor.object.name
            case HomeManager.ManageQuod:
              return quodEditor.object.name
            case HomeManager.ManageFile:
              return fileEditor.object.name
            }
          }
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }
      rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
      rightButton.styledButton.text: noteFragment.editMode ? Strings.done : Strings.edit
      leftButton.mode: noteFragment.editMode ? UIKit.NavigationBarButton.DisplayMode.StyledButton
                                         : UIKit.NavigationBarButton.DisplayMode.LargeIconButton
      leftButton.styledButton.text: Strings.discard
      leftButton.styledButton.textItem.color: ThemeController.style.charadeColor
      bottomBorderVisible: true

      leftButtonHandler: function() {
        homeManager.back()
      }
      rightButtonHandler: function() {
        if(!noteFragment.editMode){
          noteFragment.editMode = true
        }
        else{
          if (noteFragment.noteMode) {
            homeManager.saveNote(noteFragment.noteContent)
          } else {
            // Save file content
            fileEditor.saveMarkdownContent(noteFragment.noteContent)
            homeManager.back()
          }
        }
      }
    }
  }

  UIKit.BaseFooter {
    id: footer
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }
    visible: noteFragment.editMode

    RowLayout {
      height: s(32)
      anchors{
        bottom: parent.bottom
        left: parent.left
        right: parent.right
        leftMargin: ThemeController.style.margin.m16
        rightMargin: ThemeController.style.margin.m16
        verticalCenter: parent.verticalCenter
      }
      spacing: ThemeController.style.margin.m24

      UIKit.LargeIconButton {
        icon.source: "qrc:/h1-icon.svg"
        iconWidth: s(32)
        iconHeight: s(32)
        Layout.alignment: Qt.AlignVCenter
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/h2-icon.svg"
        iconWidth: s(32)
        iconHeight: s(32)
        Layout.alignment: Qt.AlignVCenter
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/b-icon.svg"
        iconWidth: s(32)
        iconHeight: s(32)
      }

      Rectangle {
        Layout.preferredHeight: s(32)
        Layout.preferredWidth: s(1)
        color: ThemeController.style.seashellColor
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/bullet-list.svg"
        iconWidth: s(32)
        iconHeight: s(32)
      }

      UIKit.LargeIconButton {
        icon.source: "qrc:/link.svg"
        iconWidth: s(32)
        iconHeight: s(32)
      }

      Rectangle {
        Layout.preferredHeight: s(32)
        Layout.preferredWidth: s(1)
        color: ThemeController.style.seashellColor
      }

      UIKit.StyledButton {
        id: addButton
        icon.source: "qrc:/chevron-white.svg"
        displayMode: UIKit.StyledButton.DisplayMode.IconOnly
        width: s(32)
        height: s(32)
        implicitHeight: s(32)
        implicitWidth: s(32)
      }
    }
  }

  Note {
    id: noteFragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: footer.top
    }
  }
}
