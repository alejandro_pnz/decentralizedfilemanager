import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.OneArcaPage
  headerComponent: pageHeader

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      height: ThemeController.style.pageHeaderHeight
      width: parent.width
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true
      rightButton.iconButton.icon.source: "qrc:/more-2.svg"
      rightButtonHandler: function() {
        actionMenu.open()
      }

      Avatars {
        model: UserListModel
        maxCount: 4
        bottomIcon: true
        anchors.centerIn: parent
      }
    }
  }

  OneArca {
    id: arcaContent
    anchors.fill: parent
  }

  ActionMenu {
    id: actionMenu
    model: actionModel.model
    currentItem: actionModel.currentObject
  }

  ObjectActionModel {
    id: actionModel
    currentObject: arcaEditor.object
    actionList: singleActions
  }
}
