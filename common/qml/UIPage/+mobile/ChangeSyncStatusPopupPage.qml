import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page

  Connections {
    target: homeManager
    function onSetupChangeSyncStatusPage(config) {
      page.acceptHandler = config.accept
      page.rejectHandler = config.reject
    }
  }

  function accept() {
    acceptHandler()
    close()
  }

  function reject() {
    rejectHandler()
    close()
  }

  property var acceptHandler
  property var rejectHandler
  popup: UIKit.StyledDrawer {
    id: popup
    visible: false
    contentItemHeight: cntHeight
    verticalPadding: ThemeController.style.margin.m24
    horizontalPadding: ThemeController.style.margin.m24
    closePolicy: Popup.NoAutoClose
    headerVisible: false
    headerSeparatorVisible: false
    interactive: false

    property int cntHeight: content.implicitHeight + footer.height + content.anchors.bottomMargin + 2 * verticalPadding

    ColumnLayout {
      anchors{
        right: parent.right
        left: parent.left
      }
      spacing: ThemeController.style.margin.m24
      PopupInfoLayoutContent {
        id: content
        title: Strings.changeSyncStatus
        content: Strings.changeSyncStatusContent
        infoboxText: ""
        Layout.fillWidth: true
        onCloseClicked: page.reject()
      }

      ColumnLayout {
        id: footer
        Layout.fillWidth: true
        spacing: ThemeController.style.margin.m12

        UIKit.StyledButton {
          Layout.topMargin: ThemeController.style.margin.m20
          text: Strings.yesChangeStatus
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          Layout.fillWidth: true
          onClicked: {
            page.accept()
          }
        }

        UIKit.StyledButton {
          text: Strings.noDontChangeStatus
          type: UIKit.StyledButton.ButtonStyle.Secondary
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          Layout.fillWidth: true
          onClicked: {
            page.reject()
          }
        }
      }
    }
  }
}
