import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page

  popup: UIKit.StyledDrawer {
    id: popup
    visible: false
    contentItemHeight: content.implicitHeight + 2 * verticalPadding
    drawerFullHeight: parent.height
    verticalPadding: ThemeController.style.margin.m24
    horizontalPadding: ThemeController.style.margin.m24
    closePolicy: Popup.NoAutoClose
    headerVisible: true
    headerSeparatorVisible: fullHeight
    interactive: false
    fullHeight: content.listView.count > 1
    headerContent.height: fullHeight ? s(90) : s(30)

    property int cntHeight: content.implicitHeight + content.anchors.bottomMargin + 2 * verticalPadding

    header: Item {
      visible: popup.fullHeight
      Layout.fillWidth: true
      Layout.fillHeight: true
      RowLayout {
        anchors.fill: parent

        UIKit.NavigationBarButton {
          id: leftButton
          Layout.alignment: Qt.AlignVCenter
          iconButton.icon.source: "qrc:/chevron-back.svg"
          onClicked: page.close()
        }

        UIKit.BaseText {
          id: text
          Layout.alignment: Qt.AlignVCenter
          Layout.fillWidth: true
          horizontalAlignment: Text.AlignHCenter
          font.weight: Font.Bold
          elide: Text.ElideRight
          text: Strings.checkedOut
        }

        UIKit.NavigationBarButton {
          id: rightBtn
          Layout.alignment: Qt.AlignVCenter
          iconButton.icon.source: "qrc:/close-gray.svg"
          onClicked: page.close()
        }
      }
    }

    QuodCheckedOut {
      id: content
      anchors.top: parent.top
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      anchors.centerIn: undefined
    }
  }
}
