import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import UIModel 1.0
import QtQml.Models 2.12

AbstractHomePage {
  id: root
  objectName: "deviceDashboardPage"
  page: Pages.DeviceDashboardPage
  headerComponent: pageHeader
  footerVisible: false

  signal openPendingRequests()

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      rightButton.iconButton.icon.source: "qrc:/more-2.svg"
      bottomBorderVisible: true
      rightButtonHandler: function() {
        if(deviceDashboard.selectionMode){
          selectionMenu.open()
        } else {
          actionMenu.open()
        }
      }

      UIKit.BaseText {
        text: Strings.deviceDashboard
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  DeviceDashboard {
    id: deviceDashboard
//    anchors.fill: parent
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: parent.bottom
    }
  }

  ActionMenu {
    id: actionMenu
    model: actionModel.model
    headerText.text: Strings.devices
  }

  DeviceActionModel {
    id: actionModel
    actionList: devicePageActions
    onSelectCurrent: {
      deviceDashboard.selectCurrent()
    }
  }

  DeviceActionModel {
    id: selectionModel
    inSelectMode: deviceDashboard.ism.hasSelection
    actionList: singleDeviceActions
    selection: deviceDashboard.ism.selectedIndexes
  }

  SelectionMenu {
    id: selectionMenu
    model: selectionModel.model
    ism: deviceDashboard.ism

    objectTypeName: Strings.devices
  }
}
