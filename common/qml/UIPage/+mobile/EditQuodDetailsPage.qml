import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.EditQuodDetailsPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      id: toolbar
      height: ThemeController.style.pageHeaderHeight
      anchors{
        left: parent.left
        right: parent.right
        leftMargin: -horizontalMargins
        rightMargin: -horizontalMargins
      }
      leftButton.visible: true
      rightButton.visible: true
      bottomBorderVisible: true
      leftButtonHandler: function() {
        if (!homeManager.creatingObject) {
          fragment.revertChanges()
        }

        homeManager.back()
      }

      ColumnLayout {
        anchors.centerIn: parent
        spacing: 0
        UIKit.BaseText {
          text: homeManager.creatingObject
                ? Strings.editDetails
                : (quodEditor.editOneSection ? quodEditor.editedSectionName : Strings.editDetails)
          font.weight: Font.Bold
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        UIKit.BaseText {
          text: Strings.createQuodHeader.arg(quodEditor.object.name)
          size: UIKit.BaseText.TextSize.Small
          horizontalAlignment: Qt.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
          color: ThemeController.style.shuttleColor
        }
      }

      states: [
        State {
          name: "create"
          when: homeManager.creatingObject
          PropertyChanges {
            target: toolbar
            rightButton.mode: UIKit.NavigationBarButton.DisplayMode.LargeIconButton
            leftButton.mode: UIKit.NavigationBarButton.DisplayMode.LargeIconButton
            rightButtonHandler: function() {
              homeManager.abortCurrentFlow()
            }
          }
        },
        State {
          name: "edit"
          when: !homeManager.creatingObject
          PropertyChanges {
            target: toolbar
            rightButton.mode: UIKit.NavigationBarButton.DisplayMode.StyledButton
            rightButton.styledButton.text: Strings.done
            rightButtonHandler: function() {
              // Save the list and back
              if (fragment.validate()) {
                fragment.save()
                homeManager.currentEditor.save()
                quodEditor.attributesSaved()
                homeManager.back()
              }
            }
          }
        }
      ]
    }
  }

  UIKit.BaseFooter {
    id: footer
    visible: homeManager.creatingObject
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    UIKit.StyledButton {
      text: Strings.saveQuod
      type: UIKit.StyledButton.ButtonStyle.Secondary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors.left: parent.left
      anchors.leftMargin: ThemeController.style.margin.m16
      anchors.verticalCenter: parent.verticalCenter
      onClicked:{
        if (fragment.validate()) {
          fragment.save()
          homeManager.saveCurrentFlow()
        }
      }
    }

    UIKit.StyledButton {
      text: Strings.continueTxt
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors.right: parent.right
      anchors.rightMargin: ThemeController.style.margin.m16
      anchors.verticalCenter: parent.verticalCenter
      onClicked: homeManager.openPage(Pages.AddTagsPage)
    }
  }

  EditQuodDetails {
    id: fragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      bottom: homeManager.creatingObject ? footer.top : parent.bottom
    }
  }
}
