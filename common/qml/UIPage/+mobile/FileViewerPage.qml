import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.FileViewerPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.PageHeader {
      rightButton.icon.source: "qrc:/more-2.svg"
      onRightButtonClicked: {
      }
      separatorVisible: true

      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }

      horizontalMargins: ThemeController.style.pageHorizontalMargin

      RowLayout {
        anchors.fill: parent
        spacing: 16

        UIKit.LargeIconButton {
          id: clearSelectionButton
          icon.source: "qrc:/close-gray.svg"
          Layout.alignment: Qt.AlignVCenter
          iconWidth: 32
          iconHeight: 32
        }

        UIKit.BaseText {
          id: text
          Layout.alignment: Qt.AlignVCenter
          font.weight: Font.Bold
          text: fragment.fileName
        }

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true
        }
      }
    }
  }

  UIKit.BaseFooter {
    id: footer
    height: 80
    visible: fragment.txtFileMode
    anchors {
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    UIKit.StyledButton {
      text: Strings.editInQuodArca
      type: UIKit.StyledButton.ButtonStyle.Primary
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors{
        verticalCenter: parent.verticalCenter
        left: app.isTablet ? undefined : parent.left
        right: parent.right
        leftMargin: app.isTablet ? undefined : horizontalMargins
        rightMargin: horizontalMargins
      }
      width: app.isTablet ? 166 : undefined
    }
  }

  FileViewer {
    id: fragment
    anchors {
      left: parent.left
      right: parent.right
      bottom: txtFileMode ? parent.bottom : footer.top
      top: parent.top
      topMargin: 16
    }
  }
}
