import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.StyledDrawer {
    id: popup
    visible: false
    contentItemHeight: cntHeight
    verticalPadding: ThemeController.style.margin.m24
    horizontalPadding: ThemeController.style.margin.m24
    closePolicy: Popup.NoAutoClose
    headerVisible: false
    headerSeparatorVisible: false
    interactive: false

    property int cntHeight: content.implicitHeight + footer.height + content.anchors.bottomMargin + 2 * verticalPadding

    ColumnLayout {
      anchors{
        right: parent.right
        left: parent.left
      }
      spacing: ThemeController.style.margin.m24
      PopupInfoLayoutContent {
        id: content
        title: Strings.deleteAfterUpload
        content: Strings.deleteAfterUploadQuestion
        infoboxText: ""
        Layout.fillWidth: true
      }
      ColumnLayout {
        id: footer
        Layout.fillWidth: true
        spacing: ThemeController.style.margin.m12
        RowLayout {
          spacing: ThemeController.style.margin.m8
          UIKit.BaseText {
            text: Strings.applyThisChoiceToFiles
            font.weight: Font.DemiBold
            Layout.fillWidth: true
          }
          UIKit.SwitchButton {
            id: switchButton
          }
        }

        UIKit.StyledButton {
          Layout.topMargin: ThemeController.style.margin.m20
          text: Strings.yesDeleteAfterUpload
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          Layout.fillWidth: true
          onClicked: {
            page.close()
          }
        }

        UIKit.StyledButton {
          text: Strings.noDeleteAfterUpload
          type: UIKit.StyledButton.ButtonStyle.Secondary
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          Layout.fillWidth: true
          onClicked: {
            page.close()
          }
        }
      }
    }
  }
}
