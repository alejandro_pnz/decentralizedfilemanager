import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import UIModel 1.0
import QtQml.Models 2.12

AbstractHomePage {
  id: root
  page: Pages.DeviceDashboardPage
  headerComponent: pageHeader
  footerVisible: false

  Component {
    id: pageHeader
    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: true
      rightButton.iconButton.icon.source: "qrc:/more-2.svg"
      bottomBorderVisible: true
      rightButtonHandler: function() {
        actionMenu.open()
      }

      UIKit.BaseText {
        text: Strings.pendingRequestsTxt
        anchors.centerIn: parent
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
      }
    }
  }

  PendingRequests {
    id: pendingRequests
    anchors {
      top: parent.top
      left: parent.left
      right: parent.right
      bottom: parent.bottom
    }
  }

  ActionMenu {
    id: actionMenu
    model: actionModel.model
    headerText.text: Strings.pendingRequestsTxt
  }

  RequestsActionModel {
    id: actionModel
  }
}
