import QtQuick 2.0
import UIFragments 1.0
BasePage {
  id: root
  hasLeftButton: false
  hasRightButton: true
  ResetPassword {
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
    }
  }
}
