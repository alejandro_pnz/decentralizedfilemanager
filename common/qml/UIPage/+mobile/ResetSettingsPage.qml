import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import QtQml.Models 2.12

AbstractHomePage {
  id: root
  page: Pages.SettingsPage
  headerComponent: pageHeader
  footerVisible: false

  property int pageMode: Enums.DeviceSettings

  Connections {
    target: homeManager
    function onSetupResetSettingsPage(config) {
      root.pageMode = config.pageMode
    }
  }

  Component {
    id: pageHeader

    UIKit.MainToolbar {
      anchors {
        leftMargin: -ThemeController.style.pageHorizontalMargin
        rightMargin: -ThemeController.style.pageHorizontalMargin
        fill: parent
      }
      leftButton.visible: true
      rightButton.visible: false
      bottomBorderVisible: true
      leftButtonHandler: function() { homeManager.back() }

      UIKit.BaseText {
        text: Strings.reset
        font.weight: Font.Bold
        horizontalAlignment: Qt.AlignHCenter
        anchors.centerIn: parent
      }
    }
  }

  ResetSettings {
    id: resetSettingsFragment
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
      topMargin: 16
      bottom: parent.bottom
    }
    pageMode: root.pageMode
  }
}
