import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  property int page: Pages.HomePage
  headerComponent: pageHeader
  footerVisible: true

  Home {
    id: homeFragment
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      rightButton.icon.source: "qrc:/bell-icon.svg"
      onRightButtonClicked: homeManager.openPage(Pages.NotificationPopupPage)
      horizontalMargins: 0
      UIKit.SvgImage {
        source: "qrc:/logo-horizontal.svg"
        height: s(40)
        width: s(137)
        anchors.verticalCenter: parent.verticalCenter
        MouseArea{
          anchors.fill: parent
          onClicked: Qt.openUrlExternally(app.companyWebsite())
        }
      }
    }
  }
}
