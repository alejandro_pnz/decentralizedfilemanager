import QtQuick 2.0
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0

BasePage {
  id: root
  hasLeftButton: false

  property bool busy: false

  Login {
    busy: root.busy
    anchors {
      right: parent.right
      left: parent.left
      top: parent.top
    }
  }

  UIKit.NotificationDelegate {
    id: notification
    anchors {
      right: parent.right
      left: parent.left
      bottom: parent.bottom
      bottomMargin: ThemeController.style.margin.m24
    }
    contentText: Strings.youHaveBeenLoggedOut
    visible: false
    Connections {
      target: pageManager
      function onSetupLoginPage() {
        notification.show()
      }
    }
  }
}
