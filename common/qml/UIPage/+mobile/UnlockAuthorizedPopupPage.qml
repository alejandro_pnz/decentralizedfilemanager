import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.StyledDrawer {
    id: popup
    visible: false
    contentItemHeight: 0.88 * parent.height
    verticalPadding: 24
    horizontalPadding: 24
    closePolicy: Popup.NoAutoClose
    headerVisible: false
    headerSeparatorVisible: false
    interactive: false

    ColumnLayout {
      id: content
      anchors{
        right: parent.right
        left: parent.left
      }
      spacing: 24

      UIKit.LargeIconButton {
        id: closeButton
        icon.source: "qrc:/close.svg"
        Layout.preferredHeight: 32
        Layout.preferredWidth: 32
        Layout.alignment: Qt.AlignRight
        onClicked: popup.close()
      }

      UnlockAuthorized {
        id: fragment
        Layout.fillWidth: true
      }
    }
  }
}
