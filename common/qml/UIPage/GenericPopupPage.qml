import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page

  property string title: ""
  property string content: ""
  property bool dontShowAgainSwitch: false
  property alias acceptButton: acceptButton
  property alias rejectButton: rejectButton
  property alias switchButton: switchButton

  signal accepted()
  signal rejected()
  Connections {
    target: homeManager
    function onSetupGenericPopupPage(config) {
      page.accepted.connect(config.accept)
      page.rejected.connect(config.reject)
      acceptButton.text = config.positiveText
      rejectButton.text = config.negativeText
      page.title = config.title
      page.content = config.content
    }
  }

  popup: UIKit.PopupScreen {
    id: popup
    visible: false
    width: s(384)
    height: Math.min(parent.height - 2 * s(80), cntHeight)
    showCloseButton: false
    showBackButton: false
    parent: Overlay.overlay
    verticalPadding: ThemeController.style.margin.m24
    horizontalPadding: ThemeController.style.margin.m24

    property int cntHeight: content.height + footer.height + content.anchors.bottomMargin + 2 * verticalPadding
    PopupInfoLayoutContent {
      id: content
      title: page.title
      content: page.content
      infoboxText: ""
      anchors{
        top: parent.top
        right: parent.right
        left: parent.left
        bottomMargin: ThemeController.style.margin.m24
      }
      onCloseClicked: page.rejected()
    }

    ColumnLayout {
      id: footer
      anchors.bottom: parent.bottom
      width: parent.width
      spacing: ThemeController.style.margin.m12

      RowLayout {
        Layout.topMargin: ThemeController.style.margin.m4
        Layout.fillWidth: true
        visible: dontShowAgainSwitch
        UIKit.BaseText {
          text: Strings.dontShowItAgain
          font.weight: Font.DemiBold
          Layout.fillWidth: true
        }

        UIKit.SwitchButton {
          id: switchButton
        }
      }

      UIKit.StyledButton {
        id: acceptButton
        Layout.topMargin: ThemeController.style.margin.m20
        text: Strings.yes
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        Layout.fillWidth: true
        onClicked: page.accepted()
      }

      UIKit.StyledButton {
        id: rejectButton
        text: Strings.no
        type: UIKit.StyledButton.ButtonStyle.Secondary
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        Layout.fillWidth: true
        onClicked: page.rejected()
      }
    }
  }
}
