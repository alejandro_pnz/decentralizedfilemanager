import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: p
  UIKit.PopupScreen {
    id: p
    visible: false
    width: aboutFragment.width + ThemeController.style.margin.m32
    height: aboutFragment.height + ThemeController.style.margin.m32
    parent: Overlay.overlay
    showCloseButton: false
    showBackButton: false
    modal: true
    closePolicy: Popup.CloseOnPressOutside

    Item {
      anchors{
        centerIn: parent
      }
      height: p.height
      width: p.width

      About {
        id: aboutFragment
        anchors {
          centerIn: parent
        }
      }
    }
  }
}
