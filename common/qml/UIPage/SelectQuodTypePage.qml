import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  page: Pages.SelectQuodTypePage
  objectName: "selectQuodTypePage"
  headerComponent: pageHeader
  footerVisible: true

  SelectQuodType {
    id: fragment
  }

  Keys.onTabPressed: {
    fragment.forceActiveFocus()
  }

  Keys.onReturnPressed: {
    if (fragment.hasSelection)
      homeManager.openPage(Pages.EditQuodDetailsPage)
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.selectQuodType

      rightButton.icon.source: "qrc:/close.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: false
      onRightButtonClicked: homeManager.abortCurrentFlow()
    }
  }
}
