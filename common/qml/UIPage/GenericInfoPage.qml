import QtQuick 2.0
import UIFragments 1.0

BasePage {
  id: root
  objectName: "genericInfoPage"
  hasLeftButton: genericInfo.showBackButton

  GenericInfo {
    id: genericInfo
    width: root.getWidth(defaultComponentColumnSpan)
  }
}
