import QtQuick 2.0
import QtQuick.Controls 2.15
import UIFragments 1.0

BasePage {
  id: root
  objectName: "choosePricePlanWebViewPage"
  hasLeftButton: false
  Component.onCompleted: console.log("ChoosePricePlanWebViewPage Completed")
  GenericWebView {
    parent: root.noScroll
    anchors.fill: parent
    url: "https://website-staging.test.com/webview/choose-price-plan.html"
    onWebViewLoadingStatusChanged: {}
    onSendConsoleMessage: function(message) {
      if (message.includes("Subscription active!") || message.includes("go back")) {
        RegistrationManager.loginPage()
      }
    }
  }
}
