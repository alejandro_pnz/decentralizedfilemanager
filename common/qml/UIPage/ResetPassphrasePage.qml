import QtQuick 2.0
import UIFragments 1.0
BasePage {
  id: root
  objectName: "resetPassphrasePage"
  hasLeftButton: false
  hasRightButton: true

  ResetPassword {
    id: resetPassword
    width: root.getWidth(defaultComponentColumnSpan)
  }
}
