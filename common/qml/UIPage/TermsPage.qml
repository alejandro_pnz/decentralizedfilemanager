import QtQuick 2.15
import UIFragments 1.0

BasePage {
  id: root

  objectName: "termsPage"
  hasLeftButton: false
  hasRightButton: true
  Terms {
    id: terms
    width: root.getWidth(defaultComponentColumnSpan)
  }
}
