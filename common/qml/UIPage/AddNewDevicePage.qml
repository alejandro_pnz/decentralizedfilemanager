import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
PopupPage {
  id: popup
  popup: UIKit.PopupScreen {
    visible: false
    width: s(384)
    height: Math.min(parent.height - 2 * s(80), s(621))
    showCloseButton: false
    showBackButton: false
    parent: Overlay.overlay
    verticalPadding: ThemeController.style.margin.m24
    horizontalPadding: ThemeController.style.margin.m24

    UIKit.PopupHeader {
      id: popupHeader
      Layout.fillWidth: true
      title: Strings.addNewDevice
      onCloseClicked: popup.close()
      anchors{
        top: parent.top
        right: parent.right
        left: parent.left
      }
    }

    AddNewDevice {
      id: content
      anchors{
        top: popupHeader.bottom
        topMargin: ThemeController.style.margin.m16
        bottom: parent.bottom
        right: parent.right
        left: parent.left
      }
    }
  }
}
