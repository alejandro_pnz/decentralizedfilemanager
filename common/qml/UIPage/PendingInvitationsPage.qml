import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  page: Pages.PendingInvitationsPage
  headerComponent: pageHeader
  footerVisible: false

  PendingInvitations {
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.pendingInvitationsTxt
      rightButton.icon.source: "qrc:/more-2.svg"
      rightButton.visible: false
      marginLeft: s(smallLayoutMode ? 80 : 160)
      onLeftButtonClicked: homeManager.back()
      leftButton.visible: true
    }
  }
}
