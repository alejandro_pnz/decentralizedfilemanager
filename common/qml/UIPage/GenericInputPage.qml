import QtQuick 2.15
import UIFragments 1.0

BasePage {
  id: root
  objectName: "genericInputPage"

  property bool busy: false

  GenericInput {
    id: genericInput
    width: root.getWidth(defaultComponentColumnSpan)
    busy: root.busy
  }
}
