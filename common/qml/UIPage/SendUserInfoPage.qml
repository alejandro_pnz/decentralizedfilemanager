import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  page: Pages.SendUserInfoPage
  footerComponent: bottomMenu
  headerComponent: pageHeader
  footerVisible: true

  SendUserInfo {
  }

  Component {
    id: bottomMenu
    CreateArcaBottomMenu {
      leftButton.text: Strings.clearAll
      rightButton.text: Strings.send

      onLeftButtonClicked: {
      }
      onRightButtonClicked: {
      }
    }
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.sendUserInfo
      subtitle: "katemills@example.com"
      rightButton.visible: false
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      onLeftButtonClicked: homeManager.back()
    }
  }
}
