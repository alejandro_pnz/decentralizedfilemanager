import QtQuick 2.0
import QtQuick.Controls 2.15
import UIFragments 1.0

BasePage {
  id: root
  objectName: "onboardingWebViewPage"
  hasLeftButton: false
  Component.onCompleted: console.log("OnboardingWebViewPage Completed")

  GenericWebView {
    parent: root.noScroll
    anchors.fill: parent
    url: "https://website-staging.test.com/webview/onboarding.html"
    onWebViewLoadingStatusChanged: {}
    onSendConsoleMessage: function(message) {
      if (message.includes("on signup")) {
        RegistrationManager.loginPage()
      }
    }
  }
}
