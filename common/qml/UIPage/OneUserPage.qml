import QtQuick 2.15
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.OneUserPage
  headerComponent: pageHeader
  footerVisible: false

  OneUser {
    id: oneUserFragment
    height: parent.height
    width: parent.width
    onOpenGivePowerPopup: givePowerPopup.open()
  }

  Component {
    id: pageHeader
    PageHeader {
      title: Strings.oneUser
      rightButton.icon.source: "qrc:/more-2.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      onLeftButtonClicked: homeManager.back()
      onRightButtonClicked: {
        actionPopup.open()
      }
    }
  }

  Item {
    anchors.fill: parent
    GivePowerPopup {
      id: givePowerPopup
      onAccepted: deviceEditor.switchPowerState()
    }
  }

  ActionMenu {
    id: actionPopup
    model: oneUserActionModel.model
    x: parent.width - actionPopup.width + horizontalMargin - ThemeController.style.margin.m24
    y: ThemeController.style.margin.m8
  }

  UserActionModel {
    id: oneUserActionModel
    currentObject: userEditor.object
    actionList: oneUserActions
  }
}
