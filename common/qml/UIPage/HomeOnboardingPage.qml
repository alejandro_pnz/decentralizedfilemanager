import QtQuick 2.0
import UIFragments 1.0
import QtQuick.Controls 2.15
import AppStyles 1.0
import com.test 1.0

AbstractHomePage {
  page: Pages.HomeOnboardingPage

  BasePage {
    id: root
    hasLeftButton: false
    hasRightButton: true
    parent: Overlay.overlay
    anchors.fill: parent
    headerItem.rightButtonHandler: function() {
      homeManager.back()
    }

    HomeOnboarding {
      width: root.getWidth(6)
    }
  }
}
