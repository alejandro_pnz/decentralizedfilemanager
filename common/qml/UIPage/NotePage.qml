import QtQuick 2.15
import QtQuick.Controls 2.15
import UIKit 1.0
import AppStyles 1.0
import UIFragments 1.0
import com.test 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  objectName: "notePage"
  page: Pages.NotePage
  footerComponent: bottomMenuArca
  headerComponent: pageHeader
  footerVisible: true

  Note {
    id: noteFragment
    horizontalMargins: homePage.horizontalMargins
    height: parent.height
    width: parent.width
  }

  Component {
    id: bottomMenuArca
    CreateArcaBottomMenu {
      id: createArcaBottomMenu
      leftButton.text: Strings.discard
      rightButton.text: homeManager.flow === HomeManager.CreateFile ? Strings.continueTxt : Strings.done
      onLeftButtonClicked: {
        homeManager.back()
      }
      onRightButtonClicked: {
        noteFragment.save();
      }
    }
  }

  Component {
    id: pageHeader

    RowLayout {
      anchors {
        right: parent.right
        left: parent.left
        verticalCenter: parent.verticalCenter
      }

      BaseText {
        id: titleComponent
        text:{
          homeManager.flow === HomeManager.CreateFile
              ? Strings.newTextFile
              : (noteFragment.noteMode ? Strings.notes : fileEditor.name)
        }
        font.weight: Font.DemiBold
        size: BaseText.TextSize.H3
        Layout.leftMargin: s(smallLayoutMode ? 56 : 146)
      }

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
      }

      RowLayout {
        visible: noteFragment.editMode
        spacing: ThemeController.style.margin.m16

        BaseText {
          text: Strings.markdownMode
          font.weight: Font.DemiBold
        }

        SwitchButton {
        }

        Rectangle {
          Layout.leftMargin: ThemeController.style.margin.m8
          Layout.preferredHeight: s(32)
          Layout.preferredWidth: s(1)
          color: ThemeController.style.seashellColor
        }

        LargeIconButton {
          Layout.leftMargin: ThemeController.style.margin.m8
          icon.source: "qrc:/undo.svg"
          height: s(32)
          width: s(32)
        }

        LargeIconButton {
          icon.source: "qrc:/redo.svg"
          height: s(32)
          width: s(32)
        }
      }

      StyledButton {
        visible: !noteFragment.editMode
        text: Strings.edit
        type: StyledButton.ButtonStyle.Secondary
        displayMode: StyledButton.DisplayMode.TextOnly
        onClicked: noteFragment.editMode = true
      }
    }
  }
}
