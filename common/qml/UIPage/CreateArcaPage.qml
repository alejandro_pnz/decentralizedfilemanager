import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  objectName: "createArcaPage"
  page: Pages.CreateArcaPage
  footerComponent: bottomMenuArca
  headerComponent: pageHeader
  footerVisible: true

  CreateArca{
    id: fragment
  }

  Component {
    id: bottomMenuArca
    CreateArcaBottomMenu {
      id: createArcaBottomMenu
      onLeftButtonClicked: {
        if (fragment.validate()) {
          fragment.save()
          homeManager.saveCurrentFlow()
        }
      }
      onRightButtonClicked: {
        if (fragment.validate()) {
          fragment.save()
          homeManager.openPage(Pages.AddTagsPage)
        }
      }
    }
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.newArca
      rightButton.icon.source: "qrc:/close.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      onRightButtonClicked: homeManager.abortCurrentFlow()
    }
  }
}
