import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

BasePage {
    id: root

    property bool busy: false
    AddDevice {
        Connections {
            target: pageManager
            function onShowGenericInfoPopupPage() {
                    popup.visible = true
            }
        }
        width: root.getWidth(defaultComponentColumnSpan)
        busy: root.busy
    }
    Item {
        parent: root.noScroll
        anchors.fill: parent
        UIKit.PopupScreen {
            id: popup
            visible: false
            width: root.getWidth(6)
            height: s(454)
            parent: Overlay.overlay
            showCloseButton: false
            showBackButton: false

            UIKit.LargeIconButton {
                icon.source: "qrc:/close.svg"
                height: s(24)
                width: s(24)
                anchors {
                    top: parent.top
                    right: parent.right
                    rightMargin: ThemeController.style.margin.m28 - popup.horizontalPadding
                    topMargin: ThemeController.style.margin.m28 - popup.verticalPadding
                }
                onClicked: {
                    popup.close()
                    pageManager.backToLogin()
                }
            }

            GenericInfo {
              anchors.centerIn: parent
              anchors.leftMargin: ThemeController.style.margin.m24
              anchors.rightMargin: ThemeController.style.margin.m24
            }
        }
    }
}
