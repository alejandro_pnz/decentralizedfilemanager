import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  objectName: "addTagsPage"
  page: Pages.AddTagsPage
  footerComponent: bottomMenuArca
  headerComponent: pageHeader
  footerVisible: true

  AddTags {
    id: addTags
    onEditTag: {
      editTagPopup.editTagComponent.object = object
      editTagPopup.open()
    }
  }

  Component {
    id: bottomMenuArca
    CreateArcaBottomMenu {
      id: bottomMenu
      states: [
        State {
          name: "createArca"
          when: homeManager.flow === HomeManager.CreateArca
          PropertyChanges {
            target: bottomMenu
            leftButton.text: Strings.saveArca
            rightButton.text: Strings.continueTxt
            onLeftButtonClicked: homeManager.saveCurrentFlow()
            onRightButtonClicked: homeManager.openPage(Pages.SyncPage)
          }
        },
        State {
          name: "createQuod"
          when: homeManager.flow === HomeManager.CreateQuod
          PropertyChanges {
            target: bottomMenu
            leftButton.text: Strings.saveQuod
            rightButton.text: Strings.continueTxt
            onLeftButtonClicked: homeManager.saveCurrentFlow()
            onRightButtonClicked: homeManager.openPage(Pages.SyncPage)
          }
        },
        State {
          name: "createFile"
          when: homeManager.flow === HomeManager.CreateFile
          PropertyChanges {
            target: bottomMenu
            leftButton.text: Strings.saveFile
            rightButton.text: Strings.continueTxt
            onLeftButtonClicked: homeManager.saveCurrentFlow()
            onRightButtonClicked: homeManager.openPage(Pages.SyncPage)
          }
        },
        State {
          name: "manageObject"
          when: homeManager.flow === HomeManager.ManageArca
                || homeManager.flow === HomeManager.ManageQuod
                || homeManager.flow === HomeManager.ManageFile
                || homeManager.flow === HomeManager.None
          PropertyChanges {
            target: bottomMenu
            leftButton.text: Strings.clearAll
            rightButton.text: Strings.done
            onLeftButtonClicked: { homeManager.currentEditor.taglistModel.deleteAllTags()}
            onRightButtonClicked: {
              homeManager.saveCurrentFlow()
            }
          }
        }
      ]
    }

  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      id: header
      rightButton.icon.source: "qrc:/close.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      onLeftButtonClicked: homeManager.back()

      states: [
        State {
          name: "createArca"
          when: homeManager.flow === HomeManager.CreateArca
          PropertyChanges {
            target: header
            title: Strings.addTags
            subtitle: Strings.newArca
            onRightButtonClicked: homeManager.abortCurrentFlow()
          }
        },
        State {
          name: "createQuod"
          when: homeManager.flow === HomeManager.CreateQuod
          PropertyChanges {
            target: header
            title: Strings.addTags
            subtitle: Strings.createQuodHeader.arg(QuodTypeModel.currentType.name)
            onRightButtonClicked: homeManager.abortCurrentFlow()
          }
        },
        State {
          name: "createFile"
          when: homeManager.flow === HomeManager.CreateFile
          PropertyChanges {
            target: header
            title: Strings.addTags
            subtitle: Strings.newFile
            onRightButtonClicked: homeManager.abortCurrentFlow()
          }
        },
        State {
          name: "manageObject"
          when: homeManager.flow === HomeManager.ManageArca
                || homeManager.flow === HomeManager.ManageQuod
                || homeManager.flow === HomeManager.ManageFile
                || homeManager.flow === HomeManager.None
          PropertyChanges {
            target: header
            title: Strings.editTags
            subtitle: homeManager.currentEditor.name
            onRightButtonClicked: homeManager.back()
          }
        }
      ]
    }
  }

  EditTagPage {
    id: editTagPopup
    anchors.centerIn: parent
    width: homePage.getWidth(6)
  }
}
