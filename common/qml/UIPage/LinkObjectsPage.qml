import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  page: Pages.LinkObjectsPage
  footerComponent: bottomMenuArca
  headerComponent: pageHeader
  footerVisible: true

  LinkObjects {
    id: linkObjectsFragment
  }

  Component {
    id: bottomMenuArca
    CreateArcaBottomMenu {
      leftButton.text: Strings.clearAll
      rightButton.text: Strings.done
      onLeftButtonClicked: {
        // TODO clear the selection
        arcaEditor.linkedObjectsModel.clearSelection()
      }
      onRightButtonClicked: {
        //Save and back
        arcaEditor.save()
        homeManager.back()
      }
    }
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.linkedData + " \u2022 " + linkObjectsFragment.selectedCount
      subtitle: arcaEditor.object.name
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      onLeftButtonClicked: homeManager.back()
      rightButton.visible: false
    }
  }
}
