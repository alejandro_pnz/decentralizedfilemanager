import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12

import Qt5Compat.GraphicalEffects


BasePage {
    id: root
    objectName: "addDeviceAdvancedPage"
    hasLeftButton: false
    hasRightButton: true
    property bool busy: false
    RowLayout {
        width: root.getWidth(defaultComponentColumnSpan)
        AddDeviceAdvanced {
            id: addDevice
            Connections {
                target: pageManager
                function onShowGenericInfoPopupPage() {
                    popup.visible = true
                }
            }
            width: parent.width
            busy: root.busy
            onShowHint: {
                tip.pointToGlobal =  globalPos
                tipItem.visible = true
                tip.show();
            }
        }
        Item {
            id: tipItem
            height: s(545)
            width: root.getWidth(defaultComponentColumnSpan)
            anchors {
                left: addDevice.right
                bottom: addDevice.bottom
                leftMargin: root.gutter
            }
            visible: false
            Tip {
                id: tip
                anchors.fill: parent
                onHandleHideHint: tipItem.visible = false
                headerText: Strings.powerPassphrase
                itemsModel: objectsModel

                Component.onCompleted: {
                    objectsModel.append({ titleTxt: Strings.powerPassphraseTipsSlide1Title,
                                            contentTxt: Strings.powerPassphraseTipsSlide1Content,
                                            imageSrc: "qrc:/illustrations/slide_2.png"});
                    objectsModel.append({ titleTxt: Strings.powerPassphraseTipsSlide2Title,
                                            contentTxt: Strings.powerPassphraseTipsSlide2Content,
                                            imageSrc: "qrc:/illustrations/r28.png"});
                    objectsModel.append({ titleTxt: Strings.powerPassphraseTipsSlide3Title,
                                            contentTxt: Strings.powerPassphraseTipsSlide3Content,
                                            imageSrc: "qrc:/illustrations/r37.png"});
                }
                ListModel{
                    id: objectsModel
                }
            }
            DropShadow {
                id: dropShadow
                anchors.fill: tip
                transparentBorder: true
                radius: s(32)
                spread: 0.1
                color: Qt.rgba(0, 0, 0, 0.08)
                source: tip
                visible: tip.visible
            }
        }
    }

    Item {
        parent: root.noScroll
        anchors.fill: parent
        UIKit.PopupScreen {
            id: popup
            visible: false
            width: root.getWidth(6)
            height: s(478)
            parent: Overlay.overlay
            showCloseButton: false
            showBackButton: false

            UIKit.LargeIconButton {
                icon.source: "qrc:/close.svg"
                height: s(24)
                width: s(24)
                anchors {
                    top: parent.top
                    right: parent.right
                    rightMargin: ThemeController.style.margin.m28 - popup.horizontalPadding
                    topMargin: ThemeController.style.margin.m28 - popup.verticalPadding
                }
                onClicked: {
                    popup.close()
                    pageManager.back()
                }
            }

            GenericInfo {
                id: genericInfo
                anchors.fill: parent
                anchors.leftMargin: ThemeController.style.margin.m24
                anchors.rightMargin: ThemeController.style.margin.m24
                anchors.topMargin: ThemeController.style.margin.m28
            }
        }
    }



    states: [
        State {
            name: "normal"
            when: !smallLayoutMode
            PropertyChanges {
                target: tipItem
                anchors.left: addDevice.right
                anchors.leftMargin: root.gutter
                anchors.right: undefined
                anchors.rightMargin: 0
            }
            PropertyChanges {
                target: tip
                pointing: UIKit.CanvasTriangle.Pointing.Left
            }
        },
        State {
            name: "smallLayout"
            when: smallLayoutMode
            PropertyChanges {
                target: tipItem
                anchors.right: addDevice.right
                anchors.rightMargin: root.gutter
                anchors.left: undefined
                anchors.leftMargin: 0
            }
            PropertyChanges {
                target: tip
                pointing: UIKit.CanvasTriangle.Pointing.Right
            }
        }
    ]

    UIKit.NotificationDelegate {
        id: notification
        parent: root.noScroll
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: ThemeController.style.margin.m24
        contentText: Strings.powerPassphraseConfiguredNotification
        icon.source: "qrc:/power-passphrase-success.svg"
        interval: 3000
        minWidth: s(347)
        iconContainer.color: ThemeController.style.whiteColor
        visible: false
        Connections {
            target: pageManager
            function onPowerPassphraseSuccessConfigured() {
                notification.show()
            }
        }
    }
}
