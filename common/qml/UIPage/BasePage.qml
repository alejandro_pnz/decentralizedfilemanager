import QtQuick 2.15
import QtQuick.Controls 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0

AbstractPage {
  id: root

  // The layout of application should adopt when app window width is smaller
  // than layoutSizeThreshold
  property int layoutSizeThreshold: s(1280)
  property bool smallLayoutMode: ApplicationWindow.window ? ApplicationWindow.window.width < layoutSizeThreshold : false
  // 4 column span is commonly used size for components. When app width is small
  // components should be streched to 6 columns. This property makes it
  // easier to adjust the component sizes when layout adopts to smaller width.
  // Note that some of the components has to be adjusted manually, because they
  // do not follow this rule
  property int defaultComponentColumnSpan: smallLayoutMode ? 6 :  4
  Component.onDestruction: print("Destroying page item")
}
