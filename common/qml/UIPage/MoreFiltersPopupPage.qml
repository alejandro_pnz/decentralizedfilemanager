import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.PopupScreen {
    id: popup
    visible: false
    width: s(588)
    height: Math.min(parent.height - 2 * s(80), s(574))
    showCloseButton: false
    showBackButton: false
    parent: Overlay.overlay

    onClosed: {
      if (!fragment.accepted) {
        fragment.revertChanges()
      }
    }

    onOpened: {
      fragment.init()
    }

    Item {
      anchors{
        centerIn: parent
      }
      height: popup.height
      width: popup.width

      Item {
        id: popupHeader
        anchors {
          top: parent.top
          right: parent.right
          left: parent.left
        }
        height: s(80)

        UIKit.BaseText {
          id: titleComponent
          text: Strings.moreFilters
          font.weight: Font.DemiBold
          size: UIKit.BaseText.TextSize.H3
          anchors.centerIn: parent
        }

        UIKit.LargeIconButton {
          icon.source: "qrc:/close.svg"
          height: s(32)
          width: s(32)
          anchors {
            right: parent.right
            rightMargin: ThemeController.style.margin.m24
            verticalCenter: parent.verticalCenter
          }

          onClicked: page.close()
        }
      }

      Rectangle {
        id: separator
        height: s(1)
        width: parent.width
        color: ThemeController.style.seashellColor
        anchors.top: popupHeader.bottom
      }

      MoreFilters {
        id: fragment

        anchors {
          top: popupHeader.bottom
          bottom: footer.top
          left: parent.left
          right: parent.right
          topMargin: ThemeController.style.margin.m24
          leftMargin: ThemeController.style.margin.m24
          rightMargin: ThemeController.style.margin.m24
        }
      }

      Rectangle{
        id: footer
        anchors.bottom: parent.bottom
        width: parent.width
        height: s(96)
        color: ThemeController.style.whiteSmokeColor
        radius: s(8)

        Rectangle {
          anchors.top: parent.top
          height: s(4)
          width: parent.width
          color: ThemeController.style.whiteSmokeColor
        }

        UIKit.StyledButton {
          text: Strings.apply
          displayMode: UIKit.StyledButton.DisplayMode.TextOnly
          anchors.right: parent.right
          anchors.left: parent.left
          anchors.rightMargin: ThemeController.style.margin.m24
          anchors.leftMargin: ThemeController.style.margin.m24
          anchors.verticalCenter: parent.verticalCenter
          onClicked: {
            fragment.applyFilters()
            page.close()
          }
        }
      }
    }
  }
}
