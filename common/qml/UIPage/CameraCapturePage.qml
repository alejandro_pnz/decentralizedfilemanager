import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import com.test 1.0
import com.testintl 1.0
import QtMultimedia 5.15
import UIKit 1.0 as UIKit
import UIFragments 1.0
import AppStyles 1.0
import QtQuick.Window 2.12

AbstractHomePage {
  id: root
  page: Pages.CameraCapturePage
  headerComponent: null
  footerVisible: false

  BasePage {
    hasLeftButton: false
    hasRightButton: false
    horizontalMargins: 0
    parent: Overlay.overlay
    anchors.fill: parent

    header: Item{}


    Rectangle {
      anchors.fill: parent
      color: ThemeController.style.blackColor
      state: "PhotoCapture"

      Camera {
        id: camera
        captureMode: Camera.CaptureStillImage
        focus.focusMode: Camera.FocusContinuous
        videoRecorder {
          resolution: "640x480"
          frameRate: 30
        }
        imageCapture {
          onImageCaptured: {
            console.log("Image captured")
          }
          onImageSaved: {
            console.log("Image saved", requestId, path, output.orientation)
            var p = captureArea.area.mapToGlobal(0,0)
            var o = output.mapFromGlobal(p)
            var capturedPoint = output.mapPointToSource(o)
            var capturedRectangle = output.mapRectToSourceNormalized((Qt.rect(o.x, o.y,
                                                                   captureArea.area.width, captureArea.area.height)))
            quodEditor.photoEditor.processImage(path, capturedRectangle, output.orientation
                                                                   /*capturedPoint, captureArea.area.width, captureArea.area.height*/)
            console.log("POINT", p, o, capturedPoint, capturedRectangle.width,
                        capturedRectangle.height, capturedRectangle)
            console.log("POINT v2", camera.imageCapture.resolution,
                        "SR:", output.sourceRect,
                        "WxH, x,y", output.width, output.height, output.x, output.y)

            // Enter edit photos page
            if (!homeManager.isPageOnStack(Pages.EditPhotosPage)){
              homeManager.openPage(Pages.EditPhotosPage, true)
            } else {
              homeManager.back()
            }
          }
        }
      }

      VideoOutput {
        id: output
        source: camera
        autoOrientation: true
        anchors.fill: parent
        fillMode: VideoOutput.PreserveAspectCrop
      }

//      Rectangle {
//        id: previewC
//        visible: false
//        anchors.fill: parent
//        anchors.margins: s(8)

//        PreviewImage {
//          id: preview
//          width: parent.width
//          height: width

//          anchors.centerIn: parent
//        }

//        MouseArea {
//          anchors.fill: parent
//          onClicked: {
//            parent.visible = false;
//          }
//        }
//      }

      UIKit.CameraCaptureArea {
        id: captureArea
        anchors {
          fill: parent
        }

        tooltip.tipContent: Strings.positionTheIdWithinTheRectangle
      }

      UIKit.CameraCloseButton {}

      // Menu with capture, flash, switch camera buttons
      CameraControlPanel { }

      states: [
        State {
          name: "PhotoCapture"
          StateChangeScript {
            script: {
              camera.captureMode = Camera.CaptureStillImage
              camera.start()
            }
          }
        }
      ]
    }
  }
}
