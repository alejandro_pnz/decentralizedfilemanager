import QtQuick 2.15
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  page: Pages.OneQuodPage
  headerComponent: pageHeader
  footerVisible: oneQuodFragment.showFooter
  footerComponent: bottomMenu
  footerBgColor: oneQuodFragment.showDataTabFooter ? ThemeController.style.whiteColor : ThemeController.style.whiteSmokeColor

  signal openPopup()

  OneQuod {
    id: oneQuodFragment
    height: parent.height
    width: parent.width
  }

  Component {
    id: pageHeader

    RowLayout {
      anchors {
        right: parent.right
        left: parent.left
        verticalCenter: parent.verticalCenter
      }

      LargeIconButton {
        icon.source: "qrc:/chevron-back.svg"
        height: s(32)
        width: s(32)
        onClicked: homeManager.back()
      }

      BaseText {
        id: titleComponent
        text: Strings.oneQuod
        font.weight: Font.DemiBold
        size: BaseText.TextSize.H3
        Layout.leftMargin: s(smallLayoutMode ? 24 : 104)
      }

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
      }

      RowLayout {
        spacing: ThemeController.style.margin.m16

        Avatars {
          id: avatars
          model: UserListModel
          maxCount: 4
          bottomIcon: true
          onShowTooltip: function(index, xPos) {
            tip.userEmail =  model.get(index).email
            tip.triangle.x = tip.width * 0.5 + xPos
            tip.roleTxt = model.get(index).guest ? Strings.guest : ""
            tip.visible = true
          }

          onHideTooltip: tip.visible = false
        }

        LargeIconButton {
          icon.source: "qrc:/more-2.svg"
          height: s(32)
          width: s(32)
          onClicked: actionPopup.open()

          ActionMenu {
            id: actionPopup
            model: actionModel.model
            x: parent.width - actionPopup.width
            y: parent.height + s(25)
          }
        }
      }
    }
  }

  ContactTooltip {
    id: tip
    visible: false
    y: parent.y + s(4)
    x: parent.x + parent.width + horizontalMargins - tip.width - s(40)
    triangle.pointing: CanvasTriangle.Pointing.Up
    triangle.anchors.top: undefined
    triangle.anchors.horizontalCenter: undefined
    triangle.anchors.bottom: triangle.parent.top
  }

  Component {
    id: bottomMenu

    Item {
      width: parent.width
      height: parent.height
      RowLayout {
        visible: !oneQuodFragment.showDataTabFooter
        anchors.fill: parent
        spacing: ThemeController.style.margin.m12

        BaseText {
          text: Strings.lastModifiedArg.arg("14 April 2021, 17:36")
          Layout.alignment: Qt.AlignHCenter
          horizontalAlignment: Qt.AlignHCenter

          MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            acceptedButtons: Qt.NoButton
            cursorShape: Qt.PointingHandCursor
            onHoveredChanged: {
              toolTip.visible = !toolTip.visible
            }
          }
        }

        BaseText {
          text: Strings.uniqueId.arg(Utility.uniqueId(homeManager.currentEditor.object.id))
          Layout.alignment: Qt.AlignHCenter
          horizontalAlignment: Qt.AlignHCenter
          color: ThemeController.style.manateeColor
        }

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true
        }
      }

      ContactTooltip {
        id: toolTip
        visible: false
        y: parent.y - s(70)
        x: parent.x - s(10)
        mode: ContactTooltip.Mode.Small
        userEmail: "james@workemail.com"
      }

      StyledButton {
        id: doneButton
        text: Strings.done
        displayMode: StyledButton.DisplayMode.TextOnly
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        onClicked: oneQuodFragment.saveQuodData()
        visible: oneQuodFragment.showDataTabFooter
      }
    }
  }

  ObjectActionModel {
    id: actionModel
    currentObject: quodEditor.object
    // TODO adjust
    actionList: singleActions
  }
}
