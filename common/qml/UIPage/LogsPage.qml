import QtQuick 2.15
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15

AbstractHomePage {
  id: root
  page: Pages.LogsPage
  headerComponent: pageHeader
  footerVisible: false

  ScrollView {
    id: scrollView
    anchors{
      fill: parent
      topMargin: ThemeController.style.margin.m16
    }
    TextArea {
      id: noteArea
      color: ThemeController.style.font.defaultColor
      wrapMode: Text.WrapAtWordBoundaryOrAnywhere
      text: app.logs
      readOnly: true
      textFormat: Qt.RichText
      selectByMouse: true
      selectedTextColor: ThemeController.style.whiteColor
      persistentSelection: true

      font {
        pixelSize: ThemeController.style.font.bodySize
        family: ThemeController.style.font.primaryFontFamily
      }
      onTextChanged: noteArea.cursorPosition = noteArea.length - 1
    }
  }

  Component {
    id: pageHeader
    PageHeader {
      title: "Logs"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      rightButton.visible: true
      rightButton.icon.source: "qrc:/duplicate-white.svg"
      onLeftButtonClicked: homeManager.back()
      onRightButtonClicked: app.clipboard.copy(noteArea.getText(0, noteArea.length))
    }
  }
}
