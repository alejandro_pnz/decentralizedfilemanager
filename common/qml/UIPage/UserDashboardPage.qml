import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  objectName: "userDashboardPage"
  page: Pages.UserDashboardPage
  headerComponent: pageHeader
  footerVisible: true
  footerComponent: selectionMenu

  UserDashboard {
    id: fragment
    height: parent.height
    width: parent.width
    horizontalMargins: root.horizontalMargin
    givePowerPopup: givePowerP
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.users
      rightButton.icon.source: "qrc:/more-2.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: false
      onRightButtonClicked: {
        actionPopup.open()
      }
    }
  }

  Component {
    id: selectionMenu
    BottomSelectMenu {
      id: bottomSelectionMenu
      ism: fragment.ism
      objectTypeName: fragment.objectTypeName
      objectType: fragment.objectType
      rootPage: root
      actionModel: selectionModel

      UserActionModel {
        id: selectionModel
        actionList: singleUserActions
        selection: bottomSelectionMenu.ism.selectedIndexes
        givePowerPopup: givePowerP

        onCleanSelection: {
          console.log("Clear selection")
          bottomSelectionMenu.ism.clearSelection()
        }

        onModelLoaded: {
          updateModel()
        }
      }
    }
  }

//  ContactSelectionModel {
//    id: contactSelectionModel
//  }

  ActionMenu {
    id: actionPopup
    model: actionModel.model
    x: parent.width - actionPopup.width + horizontalMargin - ThemeController.style.margin.m24
    y: ThemeController.style.margin.m8
  }

  UserActionModel {
    id: actionModel
    actionList: userPageActions
  }

  Item {
    anchors.fill: parent
    UIKit.GivePowerPopup {
      id: givePowerP
    }
  }
}
