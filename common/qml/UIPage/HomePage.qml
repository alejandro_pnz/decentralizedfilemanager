import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0
import UIModel 1.0

import Qt.labs.platform 1.1

import Qt5Compat.GraphicalEffects

Page {
  id: homePage
  // Common logic and properties should be placed here
  property bool isLandscapeMode: Screen.primaryOrientation === Qt.LandscapeOrientation

  property int layoutSizeThreshold: s(1280)
  property int tinyLayoutSizeThreshold: app.isTablet ? s(900) : s(1000)
  property bool smallLayoutMode: ApplicationWindow.window ? ApplicationWindow.window.width < layoutSizeThreshold : false
  property bool tinyLayoutMode: (ApplicationWindow.window ? ApplicationWindow.window.width < tinyLayoutSizeThreshold : false) || !isLandscapeMode

  property real horizontalMargins: smallLayoutMode ? s(80) : s(160)
  property alias page: homePage
  property alias homeHeader: homeHeader
  default property alias children: content.children

  //Columns properties for displaying popup
  property int columnCount: ThemeController.style.pageColumns
  property int gutter: ThemeController.style.pageHorizontalGutter
  property real columnWidth: s((homePage.width - 2 * ThemeController.style.pageHorizontalMargin
                                - (columnCount-1) * gutter) / columnCount)
  property real maximumColumnWidth: s((544 - 3 * gutter) / 4)

  property alias homeStackView: stackView.homeStackView

  Item {
    Connections {
      target: homeManager
      function onSetupFileDialog() {
        fileDialog.open()
      }
      function onSetupLibraryPage(tab, sorter) {
        sidebar.libraryButton.checked = true
      }
    }
    Connections {
      target: pageManager
      function onSetupHomePage(config) {
        if (config.showSuccessfullyCompletedPopupPage !== undefined) {
          if (config.showSuccessfullyCompletedPopupPage) {
              homeManager.enterSuccessfullyCompletedPopupPage(config)
          }
        }
      }
    }
  }

  UIKit.Sidebar {
    id: sidebar
    anchors.top: parent.top
    anchors.left: parent.left
    onAddButtonClicked: {
      // If user has no Quods of Files show Onboarding page
      if (QuodListModel.count > 0 || FileListModel.count > 0) {
        addList.open()
      } else {
        homeManager.openPage(Pages.HomeOnboardingPage)
      }
    }
    onEnterSwitchAccount: switchAccountPopup.open()
    smallMode: tinyLayoutMode
  }

  // Header
  Item {
    id: homeHeader
    anchors {
      top: parent.top
      left: parent.left
      leftMargin: sidebar.width
      right: parent.right
    }
    height: s(64)
    visible: headerLoader.sourceComponent !== null

    default property alias children: baseFooter.children
    UIKit.BaseHeader {
      id: baseHeader
      anchors.fill: parent

      Loader {
        id: headerLoader
        property alias sidebar: sidebar
        sourceComponent: homeStackView.current.headerComponent
        anchors.fill: parent
      }
    }
  }

  UIKit.NotificationPopup {
    id: notificationPopup
    maxHeight: stackView.height - s(24)
    x: parent.width - width - s(24)
    y: homeHeader.height + s(8)
  }

  UIKit.AddListPopup {
    id: addList
    x: sidebar.width + s(8)
    y: parent.height - addList.height - s(8)

    onOpenFileDialog: {
      fileDialog.open()
    }
  }

  // Footer
  Item {
    id: footer
    anchors {
      bottom: parent.bottom
      left: homeHeader.left
      right: parent.right
    }

    // Some screens contains footerComponent but it is not always visible
    // Then the visibility can be managed by AbstractHomePage::footerVisible property.
    height: homeStackView.current.footerVisible ? s(81) : 0
    visible: footerLoader.sourceComponent !== null

    default property alias children: baseFooter.children
    UIKit.BaseFooter {
      id: baseFooter
      anchors.fill: parent
      contentHorizontalMargins: horizontalMargins
      color: homeStackView.current.footerBgColor !== undefined ? homeStackView.current.footerBgColor : "transparent"

      Loader {
        id: footerLoader
        property alias sidebar: sidebar
        sourceComponent: homeStackView.current.footerComponent
        anchors.fill: parent
      }
    }

    DropShadow {
      id: dropShadow
      anchors.fill: baseFooter
      horizontalOffset: 0
      verticalOffset: s(8)
      radius: s(16)
      //samples: s(32)
      color: Qt.rgba(0, 0, 0, 0.05)
      source: baseFooter
    }
  }
//  ScrollView {
//    anchors {
//      top: header.bottom
//      bottom: footer.visible ? footer.top : parent.bottom
//      left: sidebar.right
//      right: parent.right
////      leftMargin: horizontalMargins
////      rightMargin: horizontalMargins
//    }
    HomeStackView {
      id: stackView
      anchors {
        top: homeHeader.bottom
        bottom: footer.visible ? footer.top : parent.bottom
//        left: sidebar.right
        left: homeHeader.left
        right: parent.right
        leftMargin: horizontalMargins
        rightMargin: horizontalMargins
      }
    }
//  }

  Item {
    id: content
    anchors {
      top: homeHeader.bottom
      bottom: parent.bottom
      left: sidebar.right
      right: parent.right
      leftMargin: horizontalMargins
      rightMargin: horizontalMargins
    }
  }

  UIKit.SwitchAccountPopup {
    id: switchAccountPopup
    anchors.centerIn: parent
  }

  OverwriteObjectPage {
    id: overwriteObjectPopup
    anchors.centerIn: parent
  }

  ImportArchivePage {
    id: importArchivePopup
    anchors.centerIn: parent
  }

  ImportFileDialog {
    id: fileDialog
  }

  UIKit.NotificationDelegate {
    Connections {
      target: app.clipboard
      function onCopied() {
        if(homeManager.isPageOnTop(Pages.GenerateNewPasswordPage))
          return
        notification.show()
      }
    }
    id: notification
    notificationStyle: ThemeController.style.notificationDelegateGreen
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.bottom: parent.bottom
    anchors.bottomMargin: ThemeController.style.margin.m24
    contentText: Strings.copied
    icon.source: "qrc:/duplicate-white.svg"
    visible: false
    z:99
    interval: 2000
  }

  // Calculates the maximum width of the component with given column span
  function getMaximumWidth(columnSpan=1) {
    return maximumColumnWidth * columnSpan + gutter * (columnSpan - 1)
  }

  function getWidth(columnSpan=1) {
    return Math.min(columnWidth * columnSpan + gutter * (columnSpan - 1), getMaximumWidth(columnSpan))
  }
}
