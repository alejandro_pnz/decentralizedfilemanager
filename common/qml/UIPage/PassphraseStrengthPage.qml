import QtQuick 2.0
import UIFragments 1.0
import com.test 1.0
import Qt5Compat.GraphicalEffects

import UIKit 1.0 as UIKit
import AppStyles 1.0

BasePage {
  id: root

  objectName: "passphraseStrengthPage"

  PassphraseStrength {
    id: passphraseStrength
    anchors.verticalCenter: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter
    width: root.getWidth(defaultComponentColumnSpan)
    onShowHint: {
      tip.pointToGlobal = globalPos
      tip.show()
    }
  }

  Tip {
    id: tip
    visible: false
    anchors {
      left: passphraseStrength.right
      leftMargin: root.gutter
      verticalCenter: parent.verticalCenter
    }

    width: root.getWidth(defaultComponentColumnSpan)
    onHandleHideHint: {
      tip.visible = false
    }
    headerText: Strings.quodArcaPowerPassphrase
    itemsModel: objectsModel

    Component.onCompleted: {
      objectsModel.append({ titleTxt: Strings.gainExclusiveAccess,
                            contentTxt: Strings.extraFeatures,
                            imageSrc: "qrc:/illustrations/r35.png"});
      objectsModel.append({ titleTxt: Strings.manageUsers,
                            contentTxt: Strings.usingThePowePassphrase,
                            imageSrc: "qrc:/illustrations/r36.png"});
      objectsModel.append({ titleTxt: Strings.loginReset,
                            contentTxt: Strings.powerPassphraseKnowledge,
                            imageSrc: "qrc:/illustrations/r37.png"});
    }

    ListModel{
      id: objectsModel
    }
  }

  DropShadow {
    id: dropShadow
    anchors.fill: tip
    horizontalOffset: 0
    verticalOffset: s(4)
    radius: s(40)
    //samples: s(81)
    color: Qt.rgba(0, 0, 0, 0.08)
    source: tip
    visible: tip.visible
  }

  states: [
    State {
      name: "normal"
      when: !smallLayoutMode
      PropertyChanges {
        target: tip
        anchors.left: passphraseStrength.right
        anchors.leftMargin: root.gutter
        anchors.right: undefined
        anchors.rightMargin: 0
        pointing: UIKit.CanvasTriangle.Pointing.Left
      }
    },
    State {
      name: "smallLayout"
      when: smallLayoutMode
      PropertyChanges {
        target: tip
        anchors.right: passphraseStrength.right
        anchors.rightMargin: passphraseStrength.tipButtonWidth + root.gutter
        anchors.left: undefined
        anchors.leftMargin: 0
        pointing: UIKit.CanvasTriangle.Pointing.Right
      }
    }
  ]
}
