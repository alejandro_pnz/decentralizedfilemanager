import QtQuick 2.15
import QtQuick.Controls 2.15
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
    id: page
    popup: p
    objectName: "successfullyCompletedPopupPage"
    UIKit.PopupScreen {
        id: p
        visible: false
        width: homePage.getWidth(6)
        height: s(454)
        parent: Overlay.overlay
        showCloseButton: false
        showBackButton: false

        UIKit.LargeIconButton {
            icon.source: "qrc:/close.svg"
            height: s(24)
            width: s(24)
            anchors {
                top: parent.top
                right: parent.right
                rightMargin: ThemeController.style.margin.m28 - p.horizontalPadding
                topMargin: ThemeController.style.margin.m28 - p.verticalPadding
            }
            onClicked: page.close()
        }

        SuccessfullyCompleted {
            id: successfullyCompletedFragment
            anchors.centerIn: parent
            anchors.leftMargin: ThemeController.style.margin.m24
            anchors.rightMargin: ThemeController.style.margin.m24
        }
    }
}
