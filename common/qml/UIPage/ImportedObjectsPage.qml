import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  page: Pages.ImportedObjectsPage
  headerComponent: pageHeader
  footerVisible: false

  ImportedObjects {
    id: fragment
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.importedObjects
      marginLeft: s(smallLayoutMode ? 80 : 160)
      rightButton.visible: false
      leftButton.visible: true
    }
  }
}
