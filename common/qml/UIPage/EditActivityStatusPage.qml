import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12

AbstractHomePage {
  id: root
  page: Pages.EditActivityStatusPage
  footerComponent: bottomMenu
  headerComponent: pageHeader
  footerVisible: true

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.editActivityStatus
      rightButton.icon.source: "qrc:/close.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      rightButton.visible: false
      onLeftButtonClicked: {
        homeManager.back()
      }
    }
  }

  UIKit.BaseFooter{
    id: footer
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: -horizontalMargins
      rightMargin: -horizontalMargins
    }

    visible: homeManager.creatingObject

    UIKit.StyledButton {
      text: Strings.done
      displayMode: UIKit.StyledButton.DisplayMode.TextOnly
      anchors.right: parent.right
      anchors.rightMargin: ThemeController.style.margin.m16
      anchors.verticalCenter: parent.verticalCenter
      onClicked: {
        fragment.save()
      }
    }
  }

  Component {
    id: bottomMenu
    CreateArcaBottomMenu {
      leftButton.visible: false
      rightButton.text: Strings.done
      onRightButtonClicked: {
        fragment.save()
        homeManager.back()
      }
    }
  }

  EditActivityStatus {
    id: fragment
  }
}
