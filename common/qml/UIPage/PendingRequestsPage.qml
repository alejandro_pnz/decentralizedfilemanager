import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import UIModel 1.0
import com.testintl 1.0

AbstractHomePage {
  id: root
  page: Pages.PendingRequestsPage
  headerComponent: pageHeader
  footerVisible: true
  footerComponent: selectionMenu

  signal enterEditQuodDetailsPage()

//  PendingDeviceRequests {
//    id: fragment
//  }

    PendingUserRequests {
      id: fragment
    }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.pendingRequestsTxt
      rightButton.icon.source: "qrc:/more-2.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      onLeftButtonClicked: homeManager.back()
      onRightButtonClicked: {
        actionPopup.open()
      }
    }
  }

  Component {
    id: selectionMenu
    BottomSelectMenu {
      id: bottomSelectionMenu
      ism: fragment.ism
      objectTypeName: fragment.objectTypeName
      objectType: fragment.objectType
      rootPage: root
      actionModel: selectionModel
    }
  }

  ActionMenu {
    id: actionPopup
    model: actionModel.model
    x: parent.width - actionPopup.width + horizontalMargin - ThemeController.style.margin.m24
    y: ThemeController.style.margin.m8
  }

  RequestsActionModel {
    id: actionModel
  }

  RequestsSelectionModel {
    id: selectionModel
  }
}
