import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.PopupScreen {
    id: popup
    visible: false
    width: s(384)
    height: Math.min(parent.height - 2 * s(80), cntHeight)
    showCloseButton: false
    showBackButton: false
    parent: Overlay.overlay
    verticalPadding: ThemeController.style.margin.m24
    horizontalPadding: ThemeController.style.margin.m24

    property int cntHeight: content.implicitHeight + footer.height + content.anchors.bottomMargin + 2 * verticalPadding
    PopupInfoLayoutContent {
      id: content
      title: Strings.confirmResetGlobal
      content: Strings.actionWillRestore
      infoboxText: ""
      onCloseClicked: page.close()
      anchors{
        top: parent.top
        right: parent.right
        left: parent.left
        bottom: footer.top
        bottomMargin: ThemeController.style.margin.m24
      }
    }

    ColumnLayout {
      id: footer
      anchors.bottom: parent.bottom
      width: parent.width
      spacing: ThemeController.style.margin.m12

      UIKit.StyledButton {
        text: Strings.yesReset
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        Layout.fillWidth: true
        onClicked: {
          page.close()
        }
      }

      UIKit.StyledButton {
        text: Strings.keepSettings
        type: UIKit.StyledButton.ButtonStyle.Secondary
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        Layout.fillWidth: true
        onClicked: {
          page.close()
        }
      }
    }
  }
}
