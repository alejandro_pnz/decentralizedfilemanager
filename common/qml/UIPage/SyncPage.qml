import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  objectName: "syncPage"
  page: Pages.SyncPage
  footerComponent: bottomMenuArca
  headerComponent: pageHeader
  footerVisible: true

  SelectSyncStatus {
    id: fragment
  }

  Component {
    id: bottomMenuArca
    CreateArcaBottomMenu {
      id: createArcaBottomMenu
      leftButton.visible: false
      rightButton.text: {
        if (homeManager.creatingObject) {
          if (!fragment.publicSync || fragment.publicSyncAllUsers) {
            switch(homeManager.flow) {
            case HomeManager.CreateQuod:
              return Strings.saveQuod
            case HomeManager.CreateArca:
              return Strings.saveArca
            case HomeManager.CreateFile:
              return Strings.saveFile
            }
          } else {
            Strings.continueTxt
          }
        } else {
            return Strings.done
        }
      }
      onLeftButtonClicked: {
        fragment.save()
        homeManager.saveCurrentFlow()
      }      
      onRightButtonClicked: {
        fragment.save()
        if (homeManager.creatingObject) {
          if (fragment.publicSync && !fragment.publicSyncAllUsers)
            homeManager.openPage(Pages.ArcaUsersListPage)
          else {
            homeManager.saveCurrentFlow()
          }
        } else {
          // Save edited object
          if (fragment.publicSync && !fragment.publicSyncAllUsers)
            homeManager.openPage(Pages.ArcaUsersListPage)
          else
            homeManager.saveCurrentFlow()
        }
      }
    }
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      id: header
      title: homeManager.creatingObject ? Strings.selectSyncStatus : Strings.editSyncStatus
      rightButton.icon.source: "qrc:/close.svg"
      subtitle:{
        switch(homeManager.flow) {
        case HomeManager.CreateQuod:
          return Strings.createQuodHeader.arg(QuodTypeModel.currentType.name)
        case HomeManager.CreateArca:
          return Strings.newArca
        case HomeManager.CreateFile:
          return Strings.newFile
        case HomeManager.ManageArca:
        case HomeManager.ManageQuod:
        case HomeManager.ManageFile:
        case HomeManager.None:
          return homeManager.currentEditor.name
        }
      }
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      onLeftButtonClicked: {
        homeManager.back()
      }
      onRightButtonClicked: {
        if (homeManager.creatingObject) {
          homeManager.abortCurrentFlow()
        } else {
          // Save the changes
          homeManager.back()
        }
      }
    }
  }
}
