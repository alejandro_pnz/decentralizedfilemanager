import QtQuick 2.0
import UIFragments 1.0
import com.test 1.0
import Qt5Compat.GraphicalEffects

import UIKit 1.0 as UIKit
import AppStyles 1.0

BasePage {
    id: root

    objectName: "setPassphrasePage"
    hasLeftButton: false
    hasRightButton: true
    SetPassphrase {
        id: setPassphrase
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        width: root.getWidth(defaultComponentColumnSpan)
        onShowHint: {
            tip.pointToGlobal = globalPos
            tip.show()
        }
    }

    Tip {
        id: tip
        visible: false
        anchors {
            left: setPassphrase.right
            leftMargin: root.gutter
            verticalCenter: parent.verticalCenter
        }
        height: s(545)
        width: root.getWidth(defaultComponentColumnSpan)
        onHandleHideHint: {
            tip.visible = false
        }
        headerText: setPassphrase.isLogin ? Strings.loginPassphrase : Strings.powerPassphrase
        itemsModel: setPassphrase.isLogin ? loginModel  : powerModel



        Component.onCompleted: {
            powerModel.append({ titleTxt: Strings.powerPassphraseTipsSlide1Title,
                                  contentTxt: Strings.powerPassphraseTipsSlide1Content,
                                  imageSrc: "qrc:/illustrations/slide_2.png"});
            powerModel.append({ titleTxt: Strings.powerPassphraseTipsSlide2Title,
                                  contentTxt: Strings.powerPassphraseTipsSlide2Content,
                                  imageSrc: "qrc:/illustrations/r28.png"});
            powerModel.append({ titleTxt: Strings.powerPassphraseTipsSlide3Title,
                                  contentTxt: Strings.powerPassphraseTipsSlide3Content,
                                  imageSrc: "qrc:/illustrations/r37.png"});

            ////////////////////////////////////////////////////////////////////////////

            loginModel.append({ titleTxt: Strings.whatIsPassphrase,
                                  contentTxt: Strings.loginPassphraseDefinition,
                                  imageSrc: "qrc:/illustrations/slide-login-passphrase_1.png"});
            loginModel.append({ titleTxt: Strings.whyUsePassphrase,
                                  contentTxt: Strings.humansCannotEasilyCreate,
                                  imageSrc: "qrc:/illustrations/r34.png"});
            loginModel.append({ titleTxt: Strings.theBestPassphrase,
                                  contentTxt: Strings.useLongFunnyPhrase,
                                  imageSrc: "qrc:/illustrations/slide-login-passphrase_3.png"});
        }

        ListModel{
            id: loginModel
        }
        ListModel{
            id: powerModel
        }
    }

    DropShadow {
        id: dropShadow
        anchors.fill: tip
        transparentBorder: true
        radius: s(32)
        spread: 0.1
        color: Qt.rgba(0, 0, 0, 0.08)
        source: tip
        visible: tip.visible
    }

    states: [
        State {
            name: "normal"
            when: !smallLayoutMode
            PropertyChanges {
                target: tip
                anchors.left: setPassphrase.right
                anchors.leftMargin: root.gutter
                anchors.right: undefined
                anchors.rightMargin: 0
                pointing: UIKit.CanvasTriangle.Pointing.Left
            }
        },
        State {
            name: "smallLayout"
            when: smallLayoutMode
            PropertyChanges {
                target: tip
                anchors.right: setPassphrase.right
                anchors.rightMargin: root.gutter
                anchors.left: undefined
                anchors.leftMargin: 0
                pointing: UIKit.CanvasTriangle.Pointing.Right
            }
        }
    ]
}
