import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.PopupScreen {
    id: popup
    visible: false
    width: s(384)
    height: Math.min(parent.height - 2 * s(80), cntHeight)
    showCloseButton: false
    showBackButton: false
    parent: Overlay.overlay
    verticalPadding: ThemeController.style.margin.m24
    horizontalPadding: ThemeController.style.margin.m24

    property int cntHeight: content.height + footer.height + content.anchors.bottomMargin + 2 * verticalPadding
    PopupInfoLayoutContent {
      id: content
      title: Strings.confirmDeletion
      content: Strings.confirmDeletionContent
      infoboxText: ""
      anchors{
        top: parent.top
        right: parent.right
        left: parent.left
        bottomMargin: ThemeController.style.margin.m24
      }
      onCloseClicked: {
        homeManager.currentEditor.abortDeletion()
        page.close()
      }
    }

    ColumnLayout {
      id: footer
      anchors.bottom: parent.bottom
      width: parent.width
      spacing: ThemeController.style.margin.m12

      UIKit.StyledButton {
        Layout.topMargin: ThemeController.style.margin.m20
        text: Strings.yes
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        Layout.fillWidth: true
        onClicked: {
          homeManager.currentEditor.proceedDeletion()
          page.close()
        }
      }

      UIKit.StyledButton {
        text: Strings.no
        type: UIKit.StyledButton.ButtonStyle.Secondary
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        Layout.fillWidth: true
        onClicked: {
          homeManager.currentEditor.abortDeletion()
          page.close()
        }
      }
    }
  }
}
