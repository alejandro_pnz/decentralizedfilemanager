import QtQuick 2.0

AbstractHomePage {
  isPopup: true
  property var popup
  Component.onCompleted: popup.open()
  function close() {
    popup.close()
    homeManager.back()
  }
}
