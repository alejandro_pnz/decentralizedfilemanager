import QtQuick 2.0
import UIFragments 1.0
BasePage {
  id: root

  property bool busy: false

  DeviceStatus {
    width: root.getWidth(defaultComponentColumnSpan)
    busy: root.busy
  }
}
