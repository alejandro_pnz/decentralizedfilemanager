import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page

  popup: UIKit.PopupScreen {
    id: popup
    visible: false
    width: parent.width - s(80)
    height: parent.height - s(80)
    showCloseButton: false
    showBackButton: false
    parent: Overlay.overlay
    verticalPadding: 0
    horizontalPadding: 0

    Item {
      id: header
      anchors{
        top: parent.top
        right: parent.right
        left: parent.left
      }
      height: s(64)

      RowLayout {
        anchors.centerIn: parent
        spacing: ThemeController.style.margin.m16
        width: childrenRect.width

        UIKit.BaseText {
          text: fragment.fileName
          horizontalAlignment: Text.AlignHCenter
          size: UIKit.BaseText.TextSize.H3
          Layout.alignment: Qt.AlignVCenter
          font.weight: Font.DemiBold
        }

        UIKit.BaseText {
          text: "1 page"
          horizontalAlignment: Text.AlignHCenter
          Layout.alignment: Qt.AlignVCenter
          color: ThemeController.style.shuttleColor
        }
      }

      RowLayout {
        spacing: 8
        anchors.right: parent.right
        anchors.rightMargin: ThemeController.style.margin.m24
        anchors.verticalCenter: parent.verticalCenter

        UIKit.LargeIconButton {
          icon.source: "qrc:/more-2.svg"
        }

        Rectangle {
          Layout.preferredHeight: ThemeController.style.margin.m24
          width: s(1)
          color: ThemeController.style.separatorColor
        }

        UIKit.LargeIconButton {
          icon.source: "qrc:/close.svg"
          onClicked: popup.close()
        }
      }

      Rectangle {
        height: s(1)
        width: parent.width
        color: ThemeController.style.mercuryColor
        anchors.bottom: parent.bottom
      }
    }

    Rectangle {
      anchors.top: header.bottom
      anchors.bottom: footer.visible ? footer.top : parent.bottom
      width: parent.width
      radius: s(8)

      FileViewer {
        id: fragment
        anchors.fill: parent
      }
    }

    Rectangle {
      id: footer
      height: s(80)
      width: parent.width
      radius: s(8)
      anchors.bottom: parent.bottom
      visible: fragment.txtFileMode

      UIKit.StyledButton {
        anchors {
          right: parent.right
          rightMargin: 24
          verticalCenter: parent.verticalCenter
        }
        text: Strings.editInQuodArca
        displayMode: UIKit.StyledButton.DisplayMode.TextOnly
        onClicked: {
        }
      }

      Rectangle {
        height: 1
        width: parent.width
        color: ThemeController.style.mercuryColor
        anchors.bottom: parent.top
      }
    }
  }
}
