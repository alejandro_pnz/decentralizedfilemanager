import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  page: Pages.TagPage
  headerComponent: pageHeader
  footerVisible: false

  TagPageFragment {
    id: fragment
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: ""
      rightButton.visible: false
      subtitle: ""
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      onLeftButtonClicked: {
        homeManager.back()
      }

      UIKit.Tag {
        text: fragment.tagName
        tagColor: fragment.tagColor
      }
    }
  }
}
