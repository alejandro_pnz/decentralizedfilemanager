import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import UIModel 1.0

import com.testintl 1.0

AbstractHomePage {
  id: root
  objectName: "recycleBinPage"

  page: Pages.RecycleBinPage
  footerComponent: selectionMenu
  headerComponent: pageHeader
  footerVisible: true

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.recycleBin
      rightButton.icon.source: "qrc:/more-2.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: false
      onLeftButtonClicked: homeManager.back()
      onRightButtonClicked: {
        actionPopup.open()
      }
    }
  }

  RecycleBin {
    id: recycleBinFragment
  }

  Component {
    id: selectionMenu
    BottomSelectMenu {
      id: bottomSelectionMenu
      ism: recycleBinFragment.ism
      objectTypeName: Strings.objectsStr
      objectType: SyncableObject.Generic
      rootPage: root
      actionModel: selectionModel

      RecycleBinActionModel {
        id: selectionModel
        actionList: singleRecycledItemActions
        selection: bottomSelectionMenu.ism.selectedIndexes

        onCleanSelection: {
          console.log("Clear selection")
          bottomSelectionMenu.ism.clearSelection()
        }

        onModelLoaded: {
          updateModel()
        }
      }
    }
  }

  ActionMenu {
    id: actionPopup
    model: actionModel.model
    x: parent.width - actionPopup.width + horizontalMargin - ThemeController.style.margin.m24
    y: ThemeController.style.margin.m8
  }

  RecycleBinActionModel {
    id: actionModel
    actionList: recycleBinPageActions
  }
}
