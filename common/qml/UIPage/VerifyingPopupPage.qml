import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import AppStyles 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import UIFragments 1.0

PopupPage {
  id: page
  popup: UIKit.PopupScreen {
    id: popup
    visible: false
    width: s(384)
    height: s(335)
    showCloseButton: false
    showBackButton: false
    parent: Overlay.overlay
    verticalPadding: ThemeController.style.margin.m24
    horizontalPadding: ThemeController.style.margin.m24

    UIKit.LargeIconButton {
      id: closeButton
      icon.source: "qrc:/close.svg"
      height: s(32)
      width: s(32)
      anchors {
        top: parent.top
        right: parent.right
      }
      onClicked: popup.close()
    }

    Verifying {
      id: fragment
      anchors {
        right: parent.right
        left: parent.left
        top: closeButton.bottom
        topMargin: ThemeController.style.margin.m8
        bottom: parent.bottom
      }
    }
  }
}
