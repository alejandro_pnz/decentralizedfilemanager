import QtQuick 2.15
import QtQuick.Layouts 1.12
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  id: dataPage
  objectName: "dataDashboardPage"

  page: Pages.DataDashboardPage
  headerComponent: pageHeader
  footerVisible: true

  property bool loadMockData: false

  DataDashboard {
    id: dataFragment
    horizontalMargins: s(smallLayoutMode ? 80 : 160)
    horizontalSpacing: ThemeController.style.margin.m32
    verticalSpacing: ThemeController.style.margin.m24
  }

  Component {
    id: pageHeader

    Item {
      UIKit.LabelComboBox {
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: s(smallLayoutMode ? 56 : 136)
      }
    }
  }

  UIKit.ChartExtendedPopup {
      id: chartExtended
      visible: false
  }
}
