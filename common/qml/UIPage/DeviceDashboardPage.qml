import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  objectName: "deviceDashboardPage"
  page: Pages.DeviceDashboardPage
  headerComponent: pageHeader
  footerVisible: true
  footerComponent: selectionMenu

  signal openPendingRequests()
  signal enterEditQuodDetailsPage()

  DeviceDashboard {
    id: fragment
    height: parent.height
    width: parent.width
//    horizontalMargins: root.horizontalMargin
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      id: header
      title: Strings.devices
      rightButton.icon.source: "qrc:/more-2.svg"
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: false

      onRightButtonClicked: {
        actionPopup.open()
      }
    }
  }

  Component {
    id: selectionMenu
    BottomSelectMenu {
      id: bottomSelectionMenu
      ism: fragment.ism
      objectTypeName: fragment.objectTypeName
      objectType: fragment.objectType
      rootPage: root
      actionModel: selectionModel
      DeviceActionModel {
        id: selectionModel
        actionList: singleDeviceActions
        selection: bottomSelectionMenu.ism.selectedIndexes

        onCleanSelection: {
          console.log("Clear selection")
          bottomSelectionMenu.ism.clearSelection()
        }

        onModelLoaded: {
          updateModel()
        }
      }
    }
  }

  ActionMenu {
    id: actionPopup
    model: actionModel.model
    x: parent.width - actionPopup.width + horizontalMargin - s(24)
  }

  DeviceActionModel {
    id: actionModel
    actionList: devicePageActions
  }
}
