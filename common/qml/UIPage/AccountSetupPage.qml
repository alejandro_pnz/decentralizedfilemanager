import QtQuick 2.15
import UIFragments 1.0

BasePage {
  id: root
  objectName: "accountSetupPage"
  hasLeftButton: false
  hasRightButton: true
  AccountSetup {
    width: root.getWidth(defaultComponentColumnSpan)
  }
}
