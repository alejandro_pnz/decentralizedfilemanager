import QtQuick 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import com.test 1.0
import AppStyles 1.0
import QtQuick.Layouts 1.12
import UIModel 1.0 as UIModel

AbstractHomePage {
  id: root
  page: Pages.LinkToArcaPage
  footerComponent: bottomMenu
  headerComponent: pageHeader
  footerVisible: true

  Component {
    id: bottomMenu
    CreateArcaBottomMenu {
      leftButton.visible: fragment.selectedCount > 0
      leftButton.text: Strings.clearAll
      rightButton.text: Strings.done
      onLeftButtonClicked: {
        fragment.clearSelection()
      }
      onRightButtonClicked: {
        fragment.save()
        homeManager.saveCurrentFlow()
      }
    }
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.linkToArca + " \u2022 " + fragment.selectedCount
      subtitle: {
        if (homeManager.creatingObject) {
          Strings.createQuodHeader.arg(UIModel.QuodTypeModel.currentType.name)
        } else {
          return homeManager.currentEditor.name
        }
      }
      rightButton.visible: false
      marginLeft: s(smallLayoutMode ? 80 : 160)
    }
  }

  LinkToArca {
    id: fragment
  }
}
