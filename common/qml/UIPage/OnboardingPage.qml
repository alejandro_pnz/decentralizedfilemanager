import QtQuick 2.0
import QtQuick.Controls 2.15
import UIFragments 1.0
import UIKit 1.0 as UIKit
import Qt5Compat.GraphicalEffects

import AppStyles 1.0
BasePage {
  id: root
  objectName: "onboardingPage"
  hasLeftButton: false

  StackView.onStatusChanged: function (){
    console.log("StackView status", StackView.status)
    if (StackView.status === StackView.Active
        || StackView.status === StackView.Activating) {
      popup.visible = true
    } else {
      popup.visible = false
    }
  }

  Item {
    id: item
    parent: root.noScroll
    anchors.fill: parent
    UIKit.PopupScreen {
      id: popup
      showBackButton: false
      parent: Overlay.overlay
      height: Math.min(parent.height - 2 * s(60), s(780))
      Onboarding {
        id: onboarding
        anchors {
          top: parent.top
          bottom: parent.bottom
          horizontalCenter: parent.horizontalCenter
        }

        width: root.getWidth(defaultComponentColumnSpan)
      }
    }
  }
}
