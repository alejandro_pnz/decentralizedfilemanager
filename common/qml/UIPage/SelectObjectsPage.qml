import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  id: root
  page: Pages.SelectObjectsPage
  footerComponent: bottomMenuArca
  headerComponent: pageHeader
  footerVisible: selectObjectsFragment.multipleSelection

  property string headerTitle: Strings.linkedData + " \u2022 " + selectObjectsFragment.selectedCount
  property string headerSubtitle: arcaEditor.object.name
  property bool closeVisible: !selectObjectsFragment.multipleSelection

  Connections {
    target: homeManager
    function onSetupSelectObjectsPage(config) {
      if(config.title !== undefined){
        root.headerTitle = config.title
      }
      if(config.subtitle !== undefined){
        root.headerSubtitle = config.subtitle
      }
    }
  }

  SelectObjects {
    id: selectObjectsFragment
  }

  Component {
    id: bottomMenuArca
    CreateArcaBottomMenu {
      leftButton.text: Strings.clearAll
      rightButton.text: Strings.done
      visible: selectObjectsFragment.multipleSelection
      onLeftButtonClicked: {
        // TODO clear the selection
        arcaEditor.linkedObjectsModel.clearSelection()
      }
      onRightButtonClicked: {
        //Save and back
        arcaEditor.save()
        homeManager.back()
      }
    }
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: headerTitle
      subtitle: headerSubtitle
      marginLeft: s(smallLayoutMode ? 80 : 160)
      rightButton.visible: closeVisible
      rightButton.icon.source: "qrc:/close.svg"
      onRightButtonClicked: {
        homeManager.back()
      }
    }
  }
}
