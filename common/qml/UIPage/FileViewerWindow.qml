import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import UIPage 1.0 as UIPage
import AppStyles 1.0
import UIKit 1.0 as UIKit
import UIFragments 1.0
import QtQuick.Layouts 1.12
import com.test 1.0
import UIModel 1.0

Window {
  id: fileViewerWindow
  title: "Files"
  width: 1440
  height: 900
  minimumHeight: 900
  minimumWidth: 900

  visible: false

  onClosing: {
    fileEditor.fileViewerManager.closeAllFiles()
  }

  function s(value) {
    return value * Utility.scaleFactor
  }

  function openViewer() {
    if (!fileViewerWindow.visible) {
      var mainWindowSize = app.mainWindowSize()
      fileViewerWindow.minimumHeight = smartScale.minHeight
      fileViewerWindow.minimumWidth = smartScale.minWidth
      fileViewerWindow.width = mainWindowSize.width
      fileViewerWindow.height = mainWindowSize.height
      fileViewerWindow.show()
    } else {
      fileViewerWindow.raise()
    }
  }

  function toggleFullScreen() {
    console.log("Toogle", visibility)
    if (visibility == Window.Windowed) {
      visibility = Window.FullScreen
    } else {
      visibility = Window.Windowed
    }
  }

  Connections {
    target: fileEditor.fileViewerManager
    function onSetupViewer(index) {
      console.log("Setup index", index)
      if (!bar.itemAt(index).checked){
        bar.itemAt(index).toggle()
      }
    }
    function onLastFileClosed() {
      console.log("Close")
      fileViewerWindow.close()
    }
  }

  Connections {
    target: homeManager
    function onOpenFileViewerWindow(){
      openViewer()
    }
  }

  Item {
    anchors.fill: parent
    focus: true
    Keys.onEscapePressed: {
      console.log("Escape!!!")
      if (fileViewerWindow.visibility == Window.FullScreen)
        fileViewerWindow.visibility = Window.Windowed
    }

    TabBar {
      id: bar
      currentIndex: 0
      visible: true
      anchors {
        left: parent.left
        right: parent.right
        top: parent.top
      }
      background: Rectangle {
        color: "#F6F7F9"
      }
      Repeater {
        id: repeater
        model: fileEditor.fileViewerManager
        UIKit.StyledTabButton {
          icon.source: Utility.fileIcon(model.fileViewer.fileType)
          text: fileName
          width: implicitWidth
          type: UIKit.StyledTabButton.TabStyle.Closable
          onCloseClicked: fileEditor.fileViewerManager.closeFile(model.fileViewer.id)
        }
      }
    }

    Item {
      anchors {
        fill: parent
        topMargin: bar.visible ? bar.height : 0
      }

      ListView {
        id: listView
        model: fileEditor.fileViewerManager
        anchors.fill: parent
        snapMode: ListView.SnapOneItem
        interactive: false
        boundsBehavior: ListView.StopAtBounds
        highlightFollowsCurrentItem: true
        highlightMoveDuration: 0
        orientation: ListView.Horizontal
        clip: true
        currentIndex: bar.currentIndex
        cacheBuffer: 0

        delegate: Item {
          width: listView.width
          height: listView.height
          property bool isCurrent: ListView.isCurrentItem
          Connections {
            target: listView
            function onCurrentIndexChanged() {
              if (loader.item)
                loader.item.handleIndexChanged(loader.idx != listView.currentIndex)
            }
          }
          Loader {
            id: loader
            anchors.fill: parent
            sourceComponent: {
              console.log("Source component", model.fileViewer.type)
              switch(model.fileViewer.type) {
              case FileViewerClass.VideoType: return videoPlayerComponent
              case FileViewerClass.DocumentType: return pdfViewerComponent
              case FileViewerClass.ImageType: return imageViewerComponent
              case FileViewerClass.MarkdownType: return markdownViewerComponent
              default: return videoPlayerComponent
              }
            }
            property int idx: model.index
            onLoaded: {
              console.log("Loaded", index, model.fileViewer.type)
              item.fileViewer = Qt.binding(function(){return model.fileViewer})
              if (model.fileViewer.type === FileViewerClass.VideoType) {
                item.onExpandButtonClicked.connect(function() {return toggleFullScreen()})
              }
            }
          }
        }
      }
    }
  }

  Component {
    id: pdfViewerComponent
    DocumentViewer {}
  }

  Component {
    id: videoPlayerComponent
    MediaViewer {
      id: mediaPlayer
    }
  }

  Component {
    id: imageViewerComponent
    ImageViewerFragment { }
  }

  Component {
    id: markdownViewerComponent
    MarkdownViewer {}
  }
}
