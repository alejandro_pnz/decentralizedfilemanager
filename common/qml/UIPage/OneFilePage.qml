import QtQuick 2.15
import UIKit 1.0
import AppStyles 1.0
import com.test 1.0
import com.testintl 1.0
import UIFragments 1.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import QtQml.Models 2.12
import UIModel 1.0

AbstractHomePage {
  id: root
  objectName: "oneFilePage"
  page: Pages.OneFilePage
  headerComponent: pageHeader
  footerVisible: oneFileFragment.showFooter
  footerComponent: bottomMenu
  footerBgColor: ThemeController.style.whiteSmokeColor

  OneFile {
    id: oneFileFragment
    height: parent.height
    width: parent.width
  }

  Component {
    id: pageHeader

    RowLayout {
      anchors {
        right: parent.right
        left: parent.left
        verticalCenter: parent.verticalCenter
      }

      LargeIconButton {
        icon.source: "qrc:/chevron-back.svg"
        height: s(32)
        width: s(32)
        onClicked: homeManager.back()
      }

      BaseText {
        id: titleComponent
        text: Strings.oneFile
        font.weight: Font.DemiBold
        size: BaseText.TextSize.H3
        Layout.leftMargin: s(smallLayoutMode ? 24 : 104)
      }

      Item {
        Layout.fillHeight: true
        Layout.fillWidth: true
      }

      RowLayout {
        spacing: ThemeController.style.margin.m16

        Avatars {
          id: avatars
          model: UserListModel
          maxCount: 4
          bottomIcon: true
          onShowTooltip: {
            tip.userEmail =  model.get(index).email
            tip.triangle.x = tip.width * 0.5 + xPos
            tip.roleTxt = model.get(index).guest ? Strings.guest : ""
            tip.visible = true
          }

          onHideTooltip: tip.visible = false
        }

        LargeIconButton {
          icon.source: "qrc:/more-2.svg"
          height: s(32)
          width: s(32)
          onClicked: actionPopup.open()
          ActionMenu {
            id: actionPopup
            model: actionModel.model
            x: parent.width - actionPopup.width
            y: parent.height + s(25)
          }
        }
      }
    }
  }

  ContactTooltip {
    id: tip
    visible: false
    y: parent.y + s(4)
    x: parent.x + parent.width + horizontalMargins - tip.width - s(40)
    triangle.pointing: CanvasTriangle.Pointing.Up
    triangle.anchors.top: undefined
    triangle.anchors.horizontalCenter: undefined
    triangle.anchors.bottom: triangle.parent.top
  }

  Component {
    id: bottomMenu

    Item {
      id: bottomMenuPanel
      width: parent.width
      height: parent.height
      property bool isMarkdownFile: fileEditor.fileViewer === null ?
                                      false :
                                      fileEditor.fileViewer.type === FileViewerClass.MarkdownType
      RowLayout {
        id: objectInfoPanel
        visible: !bottomMenuPanel.isMarkdownFile
        anchors.fill: parent
        spacing: ThemeController.style.margin.m12

        BaseText {
          text: Strings.lastModifiedArg.arg(Utility.formatDateTime(homeManager.currentEditor.object.modified))
          Layout.alignment: Qt.AlignHCenter
          horizontalAlignment: Qt.AlignHCenter

          MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            acceptedButtons: Qt.NoButton
            cursorShape: Qt.PointingHandCursor
            onHoveredChanged: {
              toolTip.visible = !toolTip.visible
            }
          }
        }

        BaseText {
          text: Strings.uniqueId.arg(Utility.uniqueId(homeManager.currentEditor.object.id))
          Layout.alignment: Qt.AlignHCenter
          horizontalAlignment: Qt.AlignHCenter
          color: ThemeController.style.manateeColor
        }

        Item {
          Layout.fillHeight: true
          Layout.fillWidth: true
        }
      }
      Item {
        visible: bottomMenuPanel.isMarkdownFile
        anchors.fill: parent
        StyledButton {
          anchors {
            right: parent.right
            rightMargin: ThemeController.style.margin.m24
            verticalCenter: parent.verticalCenter
          }
          text: Strings.editInQuodArca
          displayMode: StyledButton.DisplayMode.TextOnly
          onClicked: {
            homeManager.enterEditMarkdownFile(fileEditor.object.id)
          }
        }
      }
    }
  }

  ContactTooltip {
    id: toolTip
    visible: false
    y: parent.height - s(70)
    x: - s(10)
    mode: ContactTooltip.Mode.Small
    userEmail: "james@workemail.com"
  }

  ObjectActionModel {
    id: actionModel
    currentObject: fileEditor.object
    // TODO adjust
    actionList: singleActions
  }

}
