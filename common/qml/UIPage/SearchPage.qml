import QtQuick 2.15
import UIKit 1.0 as UIKit
import AppStyles 1.0
import com.test 1.0
import UIFragments 1.0

AbstractHomePage {
  page: Pages.SearchPage
  headerComponent: pageHeader
  footerVisible: false

  Search {
    id: searchFragment
  }

  Component {
    id: pageHeader
    UIKit.PageHeader {
      title: Strings.searchTxt
      marginLeft: s(smallLayoutMode ? 80 : 160)
      leftButton.visible: true
      rightButton.visible: false
    }
  }
}
