#ifndef __SINGLETON_H
#define __SINGLETON_H

#include <memory>
#ifndef QMUTEX_H
#include <QMutex>
#endif

#include <QPointer>

template<class T>
class TSingleton
{
public:
    static inline QPointer<T> instance()
    {
        if(m_qInstance.isNull())
        {
            // lock & double check
            QMutexLocker locker(&m_qMutex);

            if(m_qInstance.isNull())
            {
                m_qInstance = QPointer<T>(new T);
            }			
        }

        return m_qInstance;
    }

    static inline void destroy()
    {
        QMutexLocker locker(&m_qMutex);

        if(!m_qInstance.isNull())
        {
            m_qInstance->deleteLater();
        }
    }

protected:
    static QPointer<T> m_qInstance;
    static QMutex m_qMutex;

    TSingleton(const TSingleton&);
    TSingleton& operator=(const TSingleton&);
};

template <class T> QPointer<T> TSingleton<T>::m_qInstance(NULL);
template <class T> QMutex TSingleton<T>::m_qMutex;

#endif
