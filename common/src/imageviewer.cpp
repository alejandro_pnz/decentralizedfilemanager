#include "imageviewer.h"

ImageViewer::ImageViewer(QObject *parent) : FileViewer(parent)
{

}

ImageViewer::ImageViewer(const FileObject &file, QObject *parent)
    : FileViewer(file, parent)
{
    // TODO set source size based on file
    auto metadata = file.metadata();
    qDebug() << Q_FUNC_INFO << metadata<< metadata["width"] << metadata["height"] << file.name();
    setSourceSize({metadata["width"].toInt(),metadata["height"].toInt()});
    set_scaleFactor(1.0);
    set_fitToWidthScale(1.0); // TODO
    m_scales = {0.1, 0.25, 0.33, 0.5, 0.66, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 3.0, 4.0, 6.0, 8.0, 12.0, 32.0, 64.0};
}

const QSize &ImageViewer::sourceSize() const
{
    return m_sourceSize;
}

void ImageViewer::setSourceSize(const QSize &newSourceSize)
{
    if (m_sourceSize == newSourceSize)
        return;
    m_sourceSize = newSourceSize;
    emit sourceSizeChanged();
}

FileViewer::Type ImageViewer::type() const
{
    return ImageType;
}

void ImageViewer::zoomIn()
{
    auto it = std::upper_bound(m_scales.begin(), m_scales.end(), m_scaleFactor);
    if (it != m_scales.end()) {
        if (m_fitToWidthScale > *it) {
            // Fit to width scale it greater than next step from regular scale
            set_scaleFactor(*it);
        } else {
            if (m_fitToWidthScale > m_scaleFactor) {
                // fit to width scale is between the next step on regular scale
                // and current scale
                set_scaleFactor(m_fitToWidthScale);
            } else {
                set_scaleFactor(*it);
            }
        }
    } else {
        // No bigger scale available
        return;
    }
}

void ImageViewer::zoomOut()
{
    auto it = std::lower_bound(m_scales.begin(), m_scales.end(), m_scaleFactor);
    if (it != m_scales.end()) {
        if (it != m_scales.begin()) {
            it--;
            if (m_fitToWidthScale < *it) {
                // Fit to width scale it smaller than previous step from regular scale
                set_scaleFactor(*it);
            } else {
                if (m_fitToWidthScale < m_scaleFactor) {
                    set_scaleFactor(m_fitToWidthScale);
                } else {
                    set_scaleFactor(*it);
                }
            }
        } else {
            set_scaleFactor(*it);
        }
    } else {
        // No smaller scale available
        return;
    }
}
