#include "abstractpagemanager.h"
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QScreen>
#include <QGuiApplication>
#include <QQmlFileSelector>
#include <QFileSelector>
#include <QDebug>

AbstractPageManager::AbstractPageManager(QObject *parent) : QObject(parent)
{

}

void AbstractPageManager::init(const QString& contextProperty, QQmlApplicationEngine *engine, bool isTablet)
{
    qmlRegisterUncreatableType<Pages>("com.test", 1, 0, "Pages",
                                      "Attempt to spawn uncreatable type");
    engine->rootContext()->setContextProperty(contextProperty, this);

    m_fileSelector = new QQmlFileSelector(engine, this);
#if defined(Q_OS_ANDROID) || defined (Q_OS_IOS)
    if (isTablet) {
        m_fileSelector->setExtraSelectors({"tablet"});
    }
    else {
        m_fileSelector->setExtraSelectors({"mobile"});
    }
#else
    Q_UNUSED(isTablet)
#if defined(Q_OS_WINDOWS)
    m_fileSelector->setExtraSelectors({"windows"});
#endif
#endif
}

void AbstractPageManager::enterPage(Pages::Page page)
{
    const QString path = pagePath(page);
    if (path.isEmpty()) {
        return;
    }
    m_visiblePages.push(page);
    Q_EMIT pushPage(page, path);
}

void AbstractPageManager::enterEmptyPage(Pages::Page page)
{
    m_visiblePages.push(page);
    Q_EMIT pushPage(page, {});
}

void AbstractPageManager::back()
{
    auto page = m_visiblePages.pop();
    pageAboutToBeRemoved(page);
    Q_EMIT popPage(page);
}

void AbstractPageManager::backTo(Pages::Page page)
{
    if (isPageOnStack(page)) {
        while (m_visiblePages.top() != page) {
            back();
        }
    }
}

/*!
 * \brief Helper method to close all pages that has been opened above \a page including this page.
 * It is usefull when you need to close the page in the middle of the stack but you don't know what page is before \a page.
 */
void AbstractPageManager::backUntil(Pages::Page page)
{
    if (isPageOnStack(page)) {
        while (m_visiblePages.top() != page) {
            back();
        }
        back(); // close also current page
    }
}

void AbstractPageManager::pageAboutToBeRemoved(Pages::Page)
{
    // Nothing
}

Pages::Page AbstractPageManager::topPage() const
{
    return m_visiblePages.top();
}

bool AbstractPageManager::isPageOnStack(Pages::Page page) const
{
    return m_visiblePages.contains(page);
}

QString AbstractPageManager::pagePath(Pages::Page page)
{
    const QString pageString(Pages::toString(page));

    if (pageString.isEmpty()) return {};

    auto string = m_fileSelector->selector()->select(QStringLiteral("qrc:/UIPage/%1.qml").arg(pageString));
    qDebug() << "PAGE" << string;
    return string;
}

bool AbstractPageManager::isPageOnTop(Pages::Page page) const
{
    return m_visiblePages.top() == page;
}
