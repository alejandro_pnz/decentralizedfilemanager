#ifndef PHOTOEDITOR_H
#define PHOTOEDITOR_H

#include <QObject>
#include <QuodSectionObject.h>
#include <qqmlhelpers.h>
#include <QImage>
#include <QQuickPaintedItem>

class PreviewImage : public QQuickPaintedItem {
    // QQuickPaintedItem interface
    Q_OBJECT
public slots:
    Q_INVOKABLE void setImagess(const QImage& image);
public:
    void paint(QPainter *painter) override;
private:
    QImage m_image;
};

class PhotoEditor : public QObject
{
    Q_OBJECT
    QML_WRITABLE_PROPERTY(QVariantList, photosIds);
public:
    explicit PhotoEditor(QObject *parent = nullptr);
    void setSection(const QuodSectionObject &section);
    Q_INVOKABLE void setName(const QUuid& uuid, const QString& name);
    Q_INVOKABLE void save();
    Q_INVOKABLE void remove(const QUuid& uuid);
    Q_INVOKABLE void retake(const QUuid& uuid);
    Q_INVOKABLE QImage processImage(const QString &path, const QRectF&, const int);
    Q_INVOKABLE bool importFiles(const QList<QUrl> &files);
signals:
private:
    QuodSectionObject m_section;
    AttributeObject m_attribute;
    QUuid m_editedId;
    int m_photoCounter{0};

    QString getDefaultName();
    bool saveCroppedImage(QImage image, const QString& path);
    bool removeCapturedImage(const QString& path);
    QImage cropImage(QImage image, const QRectF&, const int);
    bool importFile(const QString& path);
};

#endif // PHOTOEDITOR_H
