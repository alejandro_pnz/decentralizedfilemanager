#include "homeeditor.h"
#include "arcalistmodel.h"
#include "syncableobjectlistmodel.h"
#include "associatedtaglistmodel.h"
#include "utility.h"
#include "DatabaseManager.h"

HomeEditor::HomeEditor(QObject *parent) : Editor(parent)
{

}


void HomeEditor::create()
{
    // Nothing, this editor does not create anything
}

bool HomeEditor::edit(const QUuid &id, SyncableObject::ObjectType type)
{
    m_currentFlow = Edit;

    cleanup();

    m_object = SyncableObjectListModel::get()->object(id, type);
    emit objectChanged();

    if (m_object) {
        setSyncProperty(m_object->syncProperty());
        setPermissions(m_object->permissions());

//        m_taglistModel->setTags(m_object->tags());
        return true;
    }

    qWarning("Cannot find object with given id");
    return false;
}

bool HomeEditor::editList(const QModelIndexList &indexes)
{
    qDebug() << Q_FUNC_INFO << indexes;
    if (indexes.isEmpty()) return false;

    m_currentFlow = Edit;

    cleanup();

    m_objects = SyncableObjectListModel::get()->objectPtrList(indexes);
    qDebug() << Q_FUNC_INFO << m_objects.size();
    if (m_objects.isEmpty()) {
        cleanup();
        return true;
    }

    // TODO decide what to do with single object list
    initTagsList();
    initSyncStatus();
    initPermissions();

    return true;
}

void HomeEditor::save()
{
    if (m_objects.isEmpty()) {
        m_object->setSyncProperty(syncProperty());
        m_object->setPermissions(permissions());
        setTags(m_object, m_taglistModel->tags());

        bool ret = m_object->save();
        qDebug() << "HomeEditor::save()" << ret;
    } else {
        // Multiple objects save
        saveTags();
        saveSyncStatus();
        savePermissions();
        // request db update for object list
        updateObjects();
    }
}

SyncableObject *HomeEditor::object() const
{
    return {};
}

void HomeEditor::cleanup()
{
    m_object.clear();
    emit objectChanged();

    m_objects.clear();

    Editor::cleanup();
}

void HomeEditor::saveLinkedArca(const QModelIndexList &indexes)
{
    setLinkedArcaIds(ArcaListModel::get()->idsFromModelIndexes(indexes));
}

SyncableObject::ObjectType HomeEditor::editorType()
{
    return SyncableObject::Generic;
}

void HomeEditor::updateObjects()
{
    QVariantList list;
    for (const auto& object : qAsConst(m_objects)) {
        list << QVariant::fromValue(SyncableObject(*object.get()));
    }
    DatabaseManager::get()->update(list);
}

void HomeEditor::initTagsList()
{
    // TODO Syncable object has no Tags
    QHash<TagObject, int> commonPart;

    // Initialize the hash table
    const auto firstTags = tagList(m_objects.first());
    qDebug() << Q_FUNC_INFO << "Tags in first object" << firstTags.size();
    commonPart.reserve(commonPart.size());
    for (const auto& tag: firstTags) {
        commonPart.insert(tag, 1);
    }

    // Iterate through other objects
    for (auto it = m_objects.cbegin() + 1; it != m_objects.cend(); it++) {
        const auto tags = tagList(*it);
        for (const auto& tag: tags) {
            auto iterator = commonPart.find(tag);
            if (iterator != commonPart.end()) {
                qDebug() << "OBJECT" << (*it)->name() << "has common tag" << iterator.value();
                iterator.value() = iterator.value() + 1;
            }
        }
    }

    // Get the tag list with frequency of m_objects.size()
    auto commonTags = commonPart.keys(m_objects.size());
    qDebug() << Q_FUNC_INFO << "Common tags" << commonTags.size();
    m_taglistModel->setTags(commonTags);
    m_tagsIntersection = QuodArca::asSet(commonTags);
}

void HomeEditor::initSyncStatus()
{
    auto syncProperty = m_objects.first()->syncProperty();
    auto allTheSame = std::all_of(m_objects.cbegin(), m_objects.cend(), [syncProperty](const QSharedPointer<SyncableObject>& ptr){
        return ptr->syncProperty() == syncProperty;
    });

    setSyncProperty(allTheSame ? syncProperty : defaultSyncProperty());
}

void HomeEditor::initPermissions()
{
    auto permissions = m_objects.first()->permissions();
    auto allTheSame = std::all_of(m_objects.cbegin(), m_objects.cend(), [permissions](const QSharedPointer<SyncableObject>& ptr){
        return ptr->permissions() == permissions;
    });

    setPermissions(allTheSame ? permissions : defaultPermission());
}

void HomeEditor::saveTags()
{
    // TODO Syncable object has no Tags
    auto tags = QuodArca::asSet(m_taglistModel->tags());
    // The set of removed tags
    auto removedTags = m_tagsIntersection.subtract(tags);
    for (auto& object : m_objects) {
        // Remove tags from object that has been removed during edition
        auto objectTags = QuodArca::asSet(tagList(object)).subtract(removedTags);
        // Add tags that have been added during edition
        objectTags = objectTags.unite(tags);
        auto tagsList = objectTags.values();
        setTags(object, tagsList);
    }
}

void HomeEditor::saveSyncStatus()
{
    for (auto& object : m_objects) {
        object->setSyncProperty(syncProperty());
    }
}

void HomeEditor::savePermissions()
{
    for (auto& object : m_objects) {
        object->setPermissions(permissions());
    }
}

void HomeEditor::initLinkedArcaList()
{
    auto linkedArcaListFromObject = [&](const QSharedPointer<SyncableObject>& object) {
        if (object->objectType() == SyncableObject::Quod) {
            return DatabaseManager::get()->linkedArca(qobject_cast<QuodObject*>(object.get()));
        } else if(object->objectType() == SyncableObject::File) {
            return DatabaseManager::get()->linkedArca(qobject_cast<FileObject*>(object.get()));
        }
        return ArcaObjectList();
    };

    if (m_objects.isEmpty()) {
        // Editing single object
        ArcaObjectList linkedArcaList = linkedArcaListFromObject(m_object);
        qDebug() << Q_FUNC_INFO << "Linked arcas" << linkedArcaList.size()
                 << m_object->name();
        for (const auto& arca : qAsConst(linkedArcaList)) {
            qDebug() << Q_FUNC_INFO << arca.name();
        }
        if (!linkedArcaList.isEmpty()) {
            QList<QUuid> idsList;
            idsList.reserve(linkedArcaList.size());
            for (const auto& arca : qAsConst(linkedArcaList)) {
                idsList.append(arca.id());
            }
            setLinkedArcaIds(idsList);
        }
    } else {
        // Editing multiple objects
        QHash<QUuid, int> commonPart;

        ArcaObjectList firstLinkedArca = linkedArcaListFromObject(m_objects.first());
        commonPart.reserve(firstLinkedArca.size());
        for (const auto& obj : firstLinkedArca) {
            commonPart.insert(obj.id(), 1);
        }

        // Iterate through other objects
        for (auto it = m_objects.cbegin() + 1; it != m_objects.cend(); it++) {
            ArcaObjectList linked = linkedArcaListFromObject((*it));
            for (const auto& obj: linked) {
                auto iterator = commonPart.find(obj.id());
                if (iterator != commonPart.end()) {
                    qDebug() << "OBJECT" << (*it)->name() << "has common linked Arca" << iterator.value();
                    iterator.value() = iterator.value() + 1;
                }
            }
        }

        // Get the tag list with frequency of m_objects.size()
        auto commonLink = commonPart.keys(m_objects.size());
        qDebug() << Q_FUNC_INFO << "Common linked" << commonLink.size();
        setLinkedArcaIds(commonLink);
    }
}

void HomeEditor::saveLinkedArcaList()
{
    // Get the ids of selected arcas to link to
    const auto arcas = ArcaListModel::get()->objectList(m_linkedArcaIds);

    if (m_object) {
        if (m_object->objectType() == SyncableObject::Quod) {
            linkQuods(QuodObjectList{qobject_cast<QuodObject*>(m_object.get())},
                      arcas);
        } else if (m_object->objectType() == SyncableObject::File) {
            linkFiles(FileObjectList{qobject_cast<FileObject*>(m_object.get())},
                      arcas);
        }
    } else {
        QuodObjectList quods;
        FileObjectList files;
        for (const auto& object : qAsConst(m_objects)) {
            if (object->objectType() == SyncableObject::Quod) {
                quods << qobject_cast<QuodObject*>(object.data());
            } else if (object->objectType() == SyncableObject::File) {
                files << qobject_cast<FileObject*>(object.data());
            }
        }

        linkQuods(quods, arcas);
        linkFiles(files, arcas);
    }
}

void HomeEditor::linkFiles(const FileObjectList &list, const ArcaObjectList& arcas)
{
    switch (m_linkArcaAction) {
    case Editor::AddToArca:
        // Link to arca
        for (const auto& arca : arcas) {
            qDebug() << Q_FUNC_INFO << "Linking to arca" << arca.name() << list.size() << "files";
            if (!DatabaseManager::get()->addArcaFiles(arca, list)) {
                qWarning() << Q_FUNC_INFO << "Error linking to" << arca.name();
            }
        }
        break;
    case Editor::MoveToArca:
        qDebug() << "NOT IMPLEMENTED";
        break;
    case Editor::RemoveFromArca:
        for (const auto& arca : arcas) {
            qDebug() << Q_FUNC_INFO << "Removing from arca" << arca.name() << list.size() << "files";
            if (!DatabaseManager::get()->removeArcaFiles(arca, list)) {
                qWarning() << Q_FUNC_INFO << "Error removing from" << arca.name();
            }
        }
        break;
    }
}

void HomeEditor::linkQuods(const QuodObjectList &list, const ArcaObjectList& arcas)
{
    switch (m_linkArcaAction) {
    case Editor::AddToArca:
        // Link to arca
        for (const auto& arca : arcas) {
            qDebug() << Q_FUNC_INFO << "Linking to arca" << arca.name() << list.size() << "quods";
            if (!DatabaseManager::get()->addArcaQuods(arca, list)) {
                qWarning() << Q_FUNC_INFO << "Error linking to" << arca.name();
            }
        }
        break;
    case Editor::MoveToArca:
        qDebug() << "NOT IMPLEMENTED";
        break;
    case Editor::RemoveFromArca:
        for (const auto& arca : arcas) {
            qDebug() << Q_FUNC_INFO << "Removing from arca" << arca.name() << list.size() << "quods";
            if (!DatabaseManager::get()->removeArcaQuods(arca, list)) {
                qWarning() << Q_FUNC_INFO << "Error removing from" << arca.name();
            }
        }
        break;
    }
}

TagObjectList HomeEditor::tagList(QSharedPointer<SyncableObject> object)
{
    if (object) {
        auto property = (object)->property("tags");
        if (property.isValid()) {
            return property.value<TagObjectList>();
        }
    }
    return {};
}

void HomeEditor::setTags(QSharedPointer<SyncableObject> object, TagObjectList list)
{
    if (object) {
        auto property = (object)->property("tags");
        if (property.isValid()) {
            property.setValue(list);
        }
    }
}
