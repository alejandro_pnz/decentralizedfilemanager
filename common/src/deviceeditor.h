#ifndef DEVICEEDITOR_H
#define DEVICEEDITOR_H

#include "editor.h"
#include <QModelIndexList>
#include <DeviceObject.h>
class DeviceEditor : public Editor
{
    Q_OBJECT
public:
    explicit DeviceEditor(QObject *parent = nullptr);

    // Editor interface
    void create() override;
    bool edit(const QUuid &id, SyncableObject::ObjectType type) override;
    bool editList(const QModelIndexList &indexes) override;
    void save() override;
    void cleanup() override;
    DeviceObject *object() const override;

    Q_INVOKABLE void switchPowerState();
    Q_INVOKABLE void activate();
    Q_INVOKABLE void deactivate();
    Q_INVOKABLE void block();

protected:
    SyncableObject::ObjectType editorType() override;

private:
    void refreshObject();
    void setObject(const QUuid& id);

    DeviceObjectPtr m_object;
};



#endif // DEVICEEDITOR_H
