#include "editor.h"
#include <QDebug>
#include <QFlag>
#include "associatedtaglistmodel.h"
#include "arcalistmodel.h"
#include "quodlistmodel.h"
#include "filelistmodel.h"
#include "syncableobjectlistmodel.h"
#include "DatabaseManager.h"

Editor::Editor(QObject *parent) : QObject(parent)
{
    m_taglistModel = new AssociatedTagListModel(this);
}

void Editor::cleanup()
{
    setLinkedArcaIds({});
    m_taglistModel->setTags({});
    m_tagsIntersection.clear();

    setName({});
    setNotes({});

    // Back to default sync property
    setSyncProperty(defaultSyncProperty());

    // Default permission
    setPermissions(defaultPermission());
}

void Editor::preprareToDelete(const QUuid &id, SyncableObject::ObjectType type)
{
    m_deleteInfo.reset(new DeleteInfo);
    m_deleteInfo->id = id;
    m_deleteInfo->type = type;
}

void Editor::preprareToDelete(const QModelIndexList &indexes)
{
    m_deleteInfo.reset(new DeleteInfo);
    m_deleteInfo->indexes = indexes;
}

void Editor::proceedDeletion()
{
    if (m_deleteInfo) {
        if (m_deleteInfo->indexes.isEmpty()) {
            removeOne(m_deleteInfo->id, m_deleteInfo->type);
        } else {
            removeList(m_deleteInfo->indexes);
            emit cleanSelection();
        }
        m_deleteInfo.reset();
    }
}

void Editor::abortDeletion()
{
    m_deleteInfo.reset();
}

void Editor::removeOne(const QUuid &id, SyncableObject::ObjectType type)
{
    // If editor type is generic, then decide how to remove item using type arg
    auto t = editorType() == SyncableObject::Generic ? type : editorType();
    switch (t) {
    case SyncableObject::Arca: {
        auto object = ArcaListModel::get()->arca(id);
        if (object) DatabaseManager::get()->remove(object.get());
        break;
    }
    case SyncableObject::File:{
        auto object = FileListModel::get()->file(id);
        if (object) DatabaseManager::get()->remove(object.get());
        break;
    }
    case SyncableObject::Quod:{
        auto object = QuodListModel::get()->quod(id);
        qDebug() << Q_FUNC_INFO << "Hello" << id << object->isValid();
        if (object) DatabaseManager::get()->remove(object.get());
        break;
    }
    default:
        qWarning() << Q_FUNC_INFO << "NOT IMPLEMENTED";
    }
}

void Editor::removeList(const QModelIndexList &indexes)
{
    switch (editorType()) {
    case SyncableObject::Arca: {
        auto list = ArcaListModel::get()->objectList(indexes);
        DatabaseManager::get()->remove(list);
        break;
    }
    case SyncableObject::File:{
        auto list = FileListModel::get()->objectList(indexes);
        DatabaseManager::get()->remove(list);
        break;
    }
    case SyncableObject::Quod:{
        auto list = QuodListModel::get()->objectList(indexes);
        DatabaseManager::get()->remove(list);
        break;
    }
    case SyncableObject::Generic: {
        auto list = SyncableObjectListModel::get()->objectList(indexes);
        DatabaseManager::get()->remove(list);
    }
    default:
        qWarning() << Q_FUNC_INFO << "NOT IMPLEMENTED";
    }
}

void Editor::duplicateList(const QModelIndexList &indexes)
{
    switch (editorType()) {
    case SyncableObject::Arca: {
        auto list = ArcaListModel::get()->objectList(indexes);
        DatabaseManager::get()->duplicate(list);
        break;
    }
    case SyncableObject::File:{
        auto list = FileListModel::get()->objectList(indexes);
        DatabaseManager::get()->duplicate(list);
        break;
    }
    case SyncableObject::Quod:{
        auto list = QuodListModel::get()->objectList(indexes);
        DatabaseManager::get()->duplicate(list);
        break;
    }
    default:
        qWarning() << Q_FUNC_INFO << "NOT IMPLEMENTED";
    }
}

void Editor::duplicateOne(const QUuid &id)
{
    switch (editorType()) {
    case SyncableObject::Arca: {
        auto object = ArcaListModel::get()->arca(id);
        if (object) DatabaseManager::get()->duplicate(object.get());
        break;
    }
    case SyncableObject::File:{
        auto object = FileListModel::get()->file(id);
        if (object) DatabaseManager::get()->duplicate(object.get());
        break;
    }
    case SyncableObject::Quod:{
        auto object = QuodListModel::get()->quod(id);
        if (object) DatabaseManager::get()->duplicate(object.get());
        break;
    }
    default:
        qWarning() << Q_FUNC_INFO << "NOT IMPLEMENTED";
    }
}

QString Editor::name() const
{
    return m_name;
}

void Editor::setName(const QString &name)
{
    if (name == m_name) return;
    m_name = name;
    emit nameChanged();
}

QString Editor::notes() const
{
    return m_notes;
}

void Editor::setNotes(const QString &notes)
{
    if (notes == m_notes) return;
    m_notes = notes;
    emit notesChanged();
}

void Editor::setLinkArcaAction(LinkArcaAction action)
{
    m_linkArcaAction = action;
    emit invertLinkedListChanged();
}

SyncableObject::SyncProperty Editor::syncProperty() const
{
    return m_syncProperty;
}

void Editor::setSyncProperty(SyncableObject::SyncProperty syncProperty)
{
    qDebug() << Q_FUNC_INFO << syncProperty << m_syncProperty;
    if (syncProperty == m_syncProperty) return;

    m_syncProperty = syncProperty;
    emit syncPropertyChanged();
}

/*!
 * \brief Returns the default sync status
 */
SyncableObject::SyncProperty Editor::defaultSyncProperty() const
{
    return SyncableObject::Private;
}

SyncableObject::Permissions Editor::defaultPermission() const
{
    return SyncableObject::AllowAll;
}

const SyncableObject::Permissions &Editor::permissions() const
{
    return m_permissions;
}

void Editor::setPermission(SyncableObject::Permission permission, bool on)
{
    qDebug() << Q_FUNC_INFO << permission << on;
    m_permissions.setFlag(permission, on);
    emit permissionsChanged();
}

void Editor::setPermissions(const SyncableObject::Permissions &newPermissions)
{
    if (m_permissions == newPermissions)
        return;
    m_permissions = newPermissions;
    emit permissionsChanged();
}

AssociatedTagListModel *Editor::taglistModel() const
{
    return m_taglistModel;
}

void Editor::setTags(const TagObjectList &tags)
{
    m_taglistModel->setTags(tags);
}

bool Editor::invertLinkedList() const
{
    return m_linkArcaAction == Editor::AddToArca;
}

void Editor::setLinkedArcaIds(const QList<QUuid> &newLinkedArcaIds)
{
    if (m_linkedArcaIds == newLinkedArcaIds)
        return;
    m_linkedArcaIds = newLinkedArcaIds;
    m_linkedArcaIdsVL = QVariant::fromValue(m_linkedArcaIds).value<QVariantList>();
    emit linkedArcaIdsChanged();
}

QVariantList Editor::linkedArcaIds() const
{
    return m_linkedArcaIdsVL;
}
