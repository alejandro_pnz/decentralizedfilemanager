#ifndef UTILITY_H
#define UTILITY_H

#include <QDirIterator>
#include <QFontDatabase>
#include <QGuiApplication>
#include <QScreen>
#include <QDebug>
#include "singleton.h"
#include <TagObject.h>
#include <FileObject.h>
#include <QColor>
#include <QtMath>
#include "DatabaseManager.h"
#include "UserManager.h"
#include <qqmlhelpers.h>
#include <QCoreApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QQuickItem>
#include <QStorageInfo>
#include <Utils.h>
#include <AuthenticationManager.h>

namespace QuodArca {

const auto asSet = [](const auto &container) {
    return QSet(container.begin(), container.end());
};

class Utility : public QObject {
    Q_OBJECT
    QML_WRITABLE_PROPERTY(qreal, scaleFactor)

public:
    static Utility *get()
    {
        return TSingleton<Utility>::instance().data();
    }
    ~Utility() {}

    Q_INVOKABLE QString tagsToString(const TagObjectList &tags) {
        QStringList list;
        for (const auto& tag : tags) {
            list.append(tag.name());
        }
        return list.join(QStringLiteral(", "));
    }

    Q_INVOKABLE QString getTags(SyncableObject *object) {
        if (!object) {
            return "";
        }

        if (object->objectType() == SyncableObject::Arca) {
            ArcaObject *arca = reinterpret_cast<ArcaObject *>(object);
            return tagsToString(arca->tags());
        }
        else if (object->objectType() == SyncableObject::Quod) {
            QuodObject *quod = reinterpret_cast<QuodObject *>(object);
            return tagsToString(quod->tags());
        }
        else if (object->objectType() == SyncableObject::File) {
            FileObject *file = reinterpret_cast<FileObject *>(object);
            return tagsToString(file->tags());
        }

        return "";
    }

    Q_INVOKABLE QString arcaIcon(const QString &colorString) {
        return m_arcaIcons.value(colorString.toUpper(), QStringLiteral("qrc:/arca/neutral.svg"));
    }

    Q_INVOKABLE QString quodIcon(QUuid id) {
        const QuodObject quod = DatabaseManager::get()->quod(id);
        if (quod.isValid()) {
            return quod.quodType().iconPath();
        }
        return QStringLiteral("qrc:/custom.svg");
    }

    Q_INVOKABLE QString fileIcon(const QString &extension) {
        // TODO:
        if (m_fileText.contains(extension, Qt::CaseInsensitive)) {
            return QStringLiteral("qrc:/files/txt.svg");
        }
        if (m_fileImages.contains(extension, Qt::CaseInsensitive)) {
            return QStringLiteral("qrc:/files/image.svg");
        }
        if (m_fileOfficeWord.contains(extension, Qt::CaseInsensitive)) {
            return QStringLiteral("qrc:/files/doc.svg");
        }
        if (m_fileOfficeExcel.contains(extension, Qt::CaseInsensitive)) {
            return QStringLiteral("qrc:/files/xls.svg");
        }
        if (m_filePdf.contains(extension, Qt::CaseInsensitive)) {
            return QStringLiteral("qrc:/files/pdf.svg");
        }
        if (m_fileVideo.contains(extension, Qt::CaseInsensitive)) {
            return QStringLiteral("qrc:/files/video.svg");
        }
        if (m_fileAudio.contains(extension, Qt::CaseInsensitive)) {
            return QStringLiteral("qrc:/files/audio.svg");
        }

        return QStringLiteral("qrc:/files/other-files.svg");
    }

    Q_INVOKABLE QString bytesToSize(const qulonglong bytes) {
        QStringList sizes = {tr("Bytes"), "KB", "MB", "GB", "TB"};
        if (bytes == 0) return tr("0 Byte");
        uint i = qFloor(qLn(bytes) / qLn(1024));
        return QStringLiteral("%1 %2").arg(QString::number(qRound(bytes / qPow(1024, i)), 'f', 0), sizes.at(i));
    }

    Q_INVOKABLE quint64 totalSize(const FileObjectList &files) {
        return std::accumulate(files.begin(), files.end(), 0);
    }

    Q_INVOKABLE QString formatDateTime(const QDateTime& datetime) {
        // TODO use QLocale::toString to match application locale
        return datetime.toString("d MMMM yyyy, h:mm");
    }

    Q_INVOKABLE QString formatPlayerTime(double time) {
        QTime tm = QTime::fromMSecsSinceStartOfDay(time);
        if (tm.hour() > 0) {
            return tm.toString("H:mm:ss");
        }
        return tm.toString("mm:ss");
    }

    Q_INVOKABLE QString uniqueId(const QUuid &id) {
        return QString::number(qHash(id));
    }

    Q_INVOKABLE bool syncStatusLessThan(int l, int r) {
        return m_syncPropertyOrder.value(static_cast<SyncableObject::SyncProperty>(l))
                < m_syncPropertyOrder.value(static_cast<SyncableObject::SyncProperty>(r));
    }

    QList<QUuid> variantListToUuid(const QVariantList& list) {
        QList<QUuid> l;
        l.reserve(list.size());
        for (const auto& v : qAsConst(list)) {
            l << v.toUuid();
        }
        return l;
    }

    Q_INVOKABLE QString formatArcaContent(int quods, int files) {
        QString content;
        if (quods > 0) {
            content.append(tr("%n Quod", "", quods));
        }
        if (files > 0) {
            if (!content.isEmpty()) {
                content.append(QStringLiteral(" %1 ").arg(QChar(0x2022)));
            }
            content.append(tr("%n File", "", files));
        }
        return content;
    }

    Q_INVOKABLE QString formatFileContent(const QUuid &id) {
        const FileObject file = DatabaseManager::get()->file(id);
        const auto arcaList = DatabaseManager::get()->linkedArca(file);
        if (arcaList.isEmpty()) {
            return QString("%1 %3 %2").arg(file.fileType().toUpper(), bytesToSize(file.size())).arg(QChar(0x2022));
        } else {
            QString arcas;
            if (arcaList.size() > 2) {
                arcas = QStringLiteral("<a href = %3 style='text-decoration: none'><font color='#5C5AE8'><b>%1</font></b></a> and %2 more").arg(arcaList.first().name()).arg(arcaList.size()).arg(arcaList.first().id().toString());
            } else {
                QStringList names;
                for (const auto& arca : qAsConst(arcaList)) {
                    names.append(QStringLiteral("<a href = %2 style='text-decoration: none'><font color='#5C5AE8'><b>%1</font></b></a>").arg(arca.name(), arca.id().toString()));
                }
                arcas.append(names.join(" and "));
            }
            return QString("%1 %3 %4 %3 in %2").arg(file.fileType().toUpper(), arcas).arg(QChar(0x2022)).arg(bytesToSize(file.size()));
        }
    }

    Q_INVOKABLE QString formatQuodContent(QUuid id) {
        const QuodObject quod = DatabaseManager::get()->quod(id);
        const auto arcaList = DatabaseManager::get()->linkedArca(quod);
        if (arcaList.isEmpty()) {
            return quod.quodType().name();
        } else {
            QString arcas;
            if (arcaList.size() > 2) {
                arcas = QStringLiteral("<a href = %2 style='text-decoration: none'><font color='#5C5AE8'><b>%1</font></b></a> and more").arg(arcaList.first().name(), arcaList.first().id().toString());
            } else {
                QStringList names;
                for (const auto& arca : qAsConst(arcaList)) {
                    names.append(QStringLiteral("<a href = %2 style='text-decoration: none'><font color='#5C5AE8'><b>%1</font></b></a>").arg(arca.name(), arca.id().toString()));
                }
                arcas.append(names.join(" and "));
            }
            return QString("%1 %3 in %2").arg(quod.quodType().name(), arcas).arg(QChar(0x2022));
        }
    }

    Q_INVOKABLE QString formatInvitationContent(quint64 userId, const QDateTime &created) {
        const auto user = UserManager::get()->user(userId);

        return tr("Invited by %1\n%2").arg(user.email(), formatDateTime(created));
    }

    Q_INVOKABLE QString formatLastSeen(QDateTime dateTime){
        auto now = QDateTime::currentDateTimeUtc();
        auto days = dateTime.daysTo(now);
        if (days <= 0) {
            return tr("Last seen recently");
        } else if (days < 2) {
            return tr("Last seen yesterday");
        } else {
            return tr("Last seen %1 days ago").arg(days);
        }
    }

    Q_INVOKABLE QString formatUserName(quint64 userId) {
        const auto user = UserManager::get()->user(userId);
        // TODO: check name also
        return user.email();
    }

    Q_INVOKABLE qint64 getUsedSpace() {
        auto account = AuthenticationManager::get()->currentAccount();
        if (!account) {
            return {};
        }

        const auto path = Utils::get()->databasePath() + account->id().toString(QUuid::WithoutBraces) + QDir::separator();

        QDirIterator it(path, QDirIterator::Subdirectories);
        qint64 total{0};
        while (it.hasNext()) {
            total += it.fileInfo().size();
            it.next();
        }

        return total;
    }

    Q_INVOKABLE qint64 getFreeSpace() {
        QStorageInfo storageInfo(Utils::get()->databasePath());
        return storageInfo.bytesFree();
    }

    Q_INVOKABLE bool anyValue(AttributeObjectList list) {
        return std::any_of(list.begin(), list.end(), [](const AttributeObject& obj){
            return !obj.value().isEmpty();
        });
    }

    Q_INVOKABLE qreal s(qreal value) {
        return qMax(1.0, value * m_scaleFactor);
    }

/*!
     * \brief Loads fonts to application's font database from resource file
     */
    static inline void loadFonts() {
        QDirIterator it(":/font", {"*.otf", "*.ttf"}, QDir::Files, QDirIterator::Subdirectories);
        while (it.hasNext()) {
            auto fileName = it.next();
            auto id = QFontDatabase::addApplicationFont(fileName);
            if ( id == -1) {
                qWarning() << "Failed to load font" << fileName;
            } else {
                qDebug() << Q_FUNC_INFO << QFontDatabase::applicationFontFamilies(id);

            }
        }
    }

    Q_INVOKABLE bool isPreviewSupported(const QString &fileType) const
    {
        return m_supportedImageFormats.contains(fileType, Qt::CaseInsensitive) ||
               m_supportedDocumentFormats.contains(fileType, Qt::CaseInsensitive);
    }

    Q_INVOKABLE inline const QStringList &supportedAudioFormats() const
    {
        return m_supportedAudioFormats;
    }

    Q_INVOKABLE inline const QStringList &supportedVideoFormats() const
    {
        return m_supportedVideoFormats;
    }

    Q_INVOKABLE inline const QStringList &supportedDocumentFormats() const
    {
        return m_supportedDocumentFormats;
    }

    Q_INVOKABLE inline const QStringList &supportedImageFormats() const
    {
        return m_supportedImageFormats;
    }

    Q_INVOKABLE inline const QStringList &supportedMarkdownFormats() const
    {
        return m_supportedMarkdownFormats;
    }

protected:
Utility() : m_scaleFactor(1.0){};
friend class TSingleton<Utility>;

private:
QHash<QString, QString> m_arcaIcons {
    {QStringLiteral("#5C5AE8"), QStringLiteral("qrc:/arca/indigo.svg")},
    {QStringLiteral("#E042FB"), QStringLiteral("qrc:/arca/pink.svg")},
    {QStringLiteral("#4CBDFF"), QStringLiteral("qrc:/arca/blue.svg")},
    {QStringLiteral("#22D396"), QStringLiteral("qrc:/arca/green.svg")},
    {QStringLiteral("#FFB904"), QStringLiteral("qrc:/arca/yellow.svg")}
};

QHash<SyncableObject::SyncProperty, int> m_syncPropertyOrder {
    {SyncableObject::SyncProperty::Public, 0},
    {SyncableObject::SyncProperty::Private, 1},
    {SyncableObject::SyncProperty::NoSync, 2},
};

// File types

QStringList m_fileText {
    QStringLiteral("txt")
};

QStringList m_fileImages {
    QStringLiteral("jpg"), QStringLiteral("png"),
    QStringLiteral("gif"), QStringLiteral("svg")
};

QStringList m_fileOfficeWord {
    QStringLiteral("doc"), QStringLiteral("docx")
};

QStringList m_fileOfficeExcel {
    QStringLiteral("xls"), QStringLiteral("xlsx"),
    QStringLiteral("csv")
};

QStringList m_filePdf {
    QStringLiteral("pdf")
};

QStringList m_fileVideo {
    QStringLiteral("avi"), QStringLiteral("flv"),
    QStringLiteral("mp4"), QStringLiteral("mpg"),
    QStringLiteral("mkv"),

};

QStringList m_fileAudio {
    QStringLiteral("mp3"), QStringLiteral("wav"),
    QStringLiteral("ogg"), QStringLiteral("flac")
};

QStringList m_supportedAudioFormats {
    QStringLiteral("mp3"), QStringLiteral("wav"),
    QStringLiteral("ogg"), QStringLiteral("flac")
};

QStringList m_supportedVideoFormats {
    QStringLiteral("avi"), QStringLiteral("flv"),
    QStringLiteral("mp4"), QStringLiteral("mpg"),
    QStringLiteral("mkv")
};

QStringList m_supportedImageFormats {
    QStringLiteral("jpg"), QStringLiteral("png"),
    QStringLiteral("jpeg"), QStringLiteral("gif"),
            QStringLiteral("svg")
};

QStringList m_supportedDocumentFormats {
    QStringLiteral("pdf"), QStringLiteral("doc"),
    QStringLiteral("docx"), QStringLiteral("xls"),
    QStringLiteral("xlsx")
};

QStringList m_supportedMarkdownFormats {
    QStringLiteral("txt"), QStringLiteral("md")
};

};
}

#endif // UTILITY_H
