#ifndef PHOTOIMAGEPROVIDER_H
#define PHOTOIMAGEPROVIDER_H

#include <QQuickAsyncImageProvider>
#include <qqmlextensionplugin.h>

#include <qqmlengine.h>
#include <qquickimageprovider.h>
#include <QDebug>
#include <QImage>
#include <QThreadPool>
#include <DatabaseManager.h>

class AsyncImageResponseRunnable : public QObject, public QRunnable
{
    Q_OBJECT

signals:
    void done(QImage image);

public:
    AsyncImageResponseRunnable(const QString &id, const QSize &requestedSize)
        : m_id(id), m_requestedSize(requestedSize) {}

    void run() override
    {
        auto file = DatabaseManager::get()->file(QUuid(m_id));
        if (file.isValid()) {
            qDebug() << Q_FUNC_INFO << m_requestedSize << m_id;
            auto image = file.preview();
            if (m_requestedSize.isValid())
                image = image.scaled(m_requestedSize);

            emit done(image);
        }
        else {
            emit done({});
        }
    }

private:
    QString m_id;
    QSize m_requestedSize;
};

class AsyncImageResponse : public QQuickImageResponse
{
    public:
        AsyncImageResponse(const QString &id, const QSize &requestedSize, QThreadPool *pool)
        {
            auto runnable = new AsyncImageResponseRunnable(QByteArray::fromPercentEncoding(id.toUtf8()), requestedSize);
            connect(runnable, &AsyncImageResponseRunnable::done, this, &AsyncImageResponse::handleDone);
            pool->start(runnable);
        }

        void handleDone(QImage image) {
            m_image = image;
            emit finished();
        }

        QQuickTextureFactory *textureFactory() const override
        {
            return QQuickTextureFactory::textureFactoryForImage(m_image);
        }

        QImage m_image;
};

class PhotoImageProvider : public QQuickAsyncImageProvider
{
public:
    QQuickImageResponse *requestImageResponse(const QString &id, const QSize &requestedSize) override
    {
        AsyncImageResponse *response = new AsyncImageResponse(id, requestedSize, &pool);
        return response;
    }

private:
    QThreadPool pool;
};

#endif // PHOTOIMAGEPROVIDER_H
