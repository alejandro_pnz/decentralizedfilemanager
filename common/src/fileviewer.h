/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef FILEVIEWER_H
#define FILEVIEWER_H

#include <QObject>
#include <QUuid>
#include "FileObject.h"

class FileViewer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString fileName READ fileName NOTIFY fileNameChanged)
    Q_PROPERTY(QString fileType READ fileType NOTIFY fileTypeChanged)
    Q_PROPERTY(int pageCount READ pageCount NOTIFY pageCountChanged)
    Q_PROPERTY(QUuid id READ id NOTIFY idChanged)
    Q_PROPERTY(Type type READ type CONSTANT)

public:
    enum Type {
        GenericType,
        DocumentType,
        VideoType,
        AudioType,
        ImageType,
        MarkdownType
    };
    Q_ENUM(Type)

    FileViewer(QObject *parent = nullptr);
    FileViewer(const FileObject &file, QObject *parent = nullptr);
    ~FileViewer();

    virtual Q_INVOKABLE QUuid id() const;

    virtual QString fileName() const;

    virtual QString fileType() const;

    virtual int pageCount() const;

    virtual Q_INVOKABLE qreal pageWidth(int page);

    virtual Q_INVOKABLE qreal pageHeight(int page);

    virtual QImage render(int page);

    virtual Type type() const;

    virtual QString content() const;

    virtual void update();

protected:
    FileObject m_file;

Q_SIGNALS:
    void fileNameChanged();
    void fileTypeChanged();
    void pageCountChanged();
    void idChanged();
};

#endif // FILEVIEWER_H
