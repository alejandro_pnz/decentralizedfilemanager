#ifndef THEMESETTINGS_H
#define THEMESETTINGS_H

#include <QObject>
#include <QSettings>

class ThemeSettings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString activeThemeId READ activeThemeId WRITE setActiveThemeId NOTIFY activeThemeIdChanged)

public:
    explicit ThemeSettings(QObject* parent = nullptr);
    void setActiveThemeId(const QString &themeId);
    QString activeThemeId() const noexcept;

signals:
    void activeThemeIdChanged();

private:
    QSettings m_themeSettings {QStringLiteral("QuodArca"), QSettings::IniFormat};
};

#endif // THEMESETTINGS_H
