/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "fileviewer.h"

#include <QImage>
#include <QtQml>
#include <QGuiApplication>

FileViewer::FileViewer(QObject *parent) : QObject(parent)
{

}

FileViewer::FileViewer(const FileObject &file, QObject *parent)
    : QObject(parent), m_file(file)
{

}

FileViewer::~FileViewer()
{

}

QUuid FileViewer::id() const
{
    return m_file.id();
}

QString FileViewer::fileName() const
{
    return m_file.name();
}

QString FileViewer::fileType() const
{
    return m_file.fileType();
}

int FileViewer::pageCount() const
{
    return 0;
}

qreal FileViewer::pageWidth(int page)
{
    return 500;
}

qreal FileViewer::pageHeight(int page)
{
    return 500;
}

QImage FileViewer::render(int page)
{
    return {};
}

FileViewer::Type FileViewer::type() const
{
    return GenericType;
}

QString FileViewer::content() const
{
    return {};
}

void FileViewer::update()
{
    // Nothing
}

void _q_FileViewer_registerTypes() {
    qmlRegisterType<FileViewer>("com.test", 1, 0, "FileViewerClass");
}

Q_COREAPP_STARTUP_FUNCTION(_q_FileViewer_registerTypes)
