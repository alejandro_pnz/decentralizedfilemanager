#include "pagemanager.h"
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QScreen>
#include <QGuiApplication>
#include <QQmlFileSelector>
#include <QFileSelector>
#include <QDebug>


PageManager::PageManager(QObject *parent) : AbstractPageManager(parent)
{

}

void PageManager::backToLogin()
{
    backTo(Pages::LoginPage);
}

void PageManager::backToCreateAccount()
{
    backTo(Pages::CreateAccountPage);
}

void PageManager::enterCheckEmailPage(const QJSValue &config)
{
    enterPage(Pages::Page::CheckEmailPage);
    emit setupCheckEmailPage(config);
}

void PageManager::enterDeviceStatusPage(const QJSValue &config)
{
    enterPage(Pages::Page::DeviceStatusPage);
    emit setupDeviceStatusPage(config);
}

void PageManager::enterGenericInfoPage(const QJSValue &config)
{
    enterPage(Pages::Page::GenericInfoPage);
    emit setupGenericInfoPage(config);
}

void PageManager::enterCreateAccountPage(const QJSValue &config)
{
    enterPage(Pages::Page::CreateAccountPage);
    emit setupCreateAccountPage(config);
}

void PageManager::enterSetPassphrasePage(const QJSValue &config)
{
    enterPage(Pages::Page::SetPassphrasePage);
    emit setupSetPassphrasePage(config);
}

void PageManager::enterGenericInputPage(const QJSValue &config)
{
    enterPage(Pages::Page::GenericInputPage);
    emit setupGenericInputPage(config);
}

void PageManager::enterAddDevicePage(const QJSValue &config)
{
    enterPage(Pages::Page::AddDevicePage);
    emit setupAddDevicePage(config);
}

//TODO remove this function(added only for test)
void PageManager::enterHomePage(const QJSValue &config)
{
    enterPage(Pages::Page::HomePage);
    emit setupHomePage(config);
}

void PageManager::enterOnboardingPage(const QJSValue &config)
{
    enterPage(Pages::Page::OnboardingPage);
    emit setupOnboardingPage(config);
}

void PageManager::enterTermsPage(const QJSValue &config)
{
    enterPage(Pages::TermsPage);
    emit setupTermsPage(config);
}

void PageManager::enterAccountSetupPage(const QJSValue &config)
{
    enterPage(Pages::AccountSetupPage);
    emit setupAccountSetupPage(config);
}

void PageManager::enterWebViewLinkPage(const QJSValue &config)
{
    enterPage(Pages::Page::WebViewLinkPage);
    emit setupWebViewLinkPage(config);
}

void PageManager::enterOnboardingWebViewPage()
{
    enterPage(Pages::Page::OnboardingWebViewPage);
}

void PageManager::enterChoosePricePlanWebViewPage()
{
    enterPage(Pages::Page::ChoosePricePlanWebViewPage);
}

void PageManager::enterPowerPasswordPage(const QJSValue &config)
{
    enterPage(Pages::Page::PassphraseStrengthPage);
    emit setupPowerPasswordPage(config);
}

void PageManager::enterGenericInfoPopupPage(const QJSValue &config)
{
    emit showGenericInfoPopupPage();
    emit setupGenericInfoPage(config);
}
