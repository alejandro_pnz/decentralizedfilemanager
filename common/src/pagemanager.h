﻿#ifndef PAGEMANAGER_H
#define PAGEMANAGER_H

#include "abstractpagemanager.h"

class PageManager : public AbstractPageManager
{
    Q_OBJECT
public:
    explicit PageManager(QObject *parent = nullptr);

    Q_INVOKABLE void backToLogin();
    Q_INVOKABLE void backToCreateAccount();

    Q_INVOKABLE void enterCheckEmailPage(const QJSValue& config);
    Q_INVOKABLE void enterTermsPage(const QJSValue& config);
    Q_INVOKABLE void enterAccountSetupPage(const QJSValue& config);
    Q_INVOKABLE void enterDeviceStatusPage(const QJSValue& config);
    Q_INVOKABLE void enterGenericInfoPage(const QJSValue& config);
    Q_INVOKABLE void enterCreateAccountPage(const QJSValue& config);
    Q_INVOKABLE void enterSetPassphrasePage(const QJSValue &config);
    Q_INVOKABLE void enterGenericInputPage(const QJSValue& config);
    Q_INVOKABLE void enterAddDevicePage(const QJSValue& config);
    Q_INVOKABLE void enterHomePage(const QJSValue& config);    
    Q_INVOKABLE void enterOnboardingPage(const QJSValue& config);
    Q_INVOKABLE void enterWebViewLinkPage(const QJSValue& config);
    Q_INVOKABLE void enterOnboardingWebViewPage();
    Q_INVOKABLE void enterChoosePricePlanWebViewPage();
    Q_INVOKABLE void enterPowerPasswordPage(const QJSValue& config);
    Q_INVOKABLE void enterGenericInfoPopupPage(const QJSValue& config);

signals:
    void setupLoginPage(const QString& email);
    void setupCheckEmailPage(const QJSValue& config);
    void setupDeviceStatusPage(const QJSValue& config);
    void setupGenericInfoPage(const QJSValue& config);
    void setupErrorMessagePopupPage(const QJSValue& config);
    void setupCreateAccountPage(const QJSValue& config);
    void setupGenericInputPage(const QJSValue& config);
    void setupAddDevicePage(const QJSValue& config);
    void setupHomePage(const QJSValue& config);
    void setupNotesPage(const QJSValue& config);
    void setupOnboardingPage(const QJSValue& config);
    void setupTermsPage(const QJSValue& config);
    void setupAccountSetupPage(const QJSValue& config);
    void setupPowerPasswordPage(const QJSValue& config);
    void setupWebViewLinkPage(const QJSValue& config);
    void setupSetPassphrasePage(const QJSValue& config);
    void setupAddDeviceAdvancedPage(const QJSValue& config);
    void powerPassphraseSuccessConfigured();
    void showGenericInfoPopupPage();
};

#endif // PAGEMANAGER_H
