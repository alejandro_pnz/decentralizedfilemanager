#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include "fileviewer.h"
#include <qqmlhelpers.h>

class ImageViewer : public FileViewer
{
    Q_OBJECT
    Q_PROPERTY(QSize sourceSize READ sourceSize WRITE setSourceSize NOTIFY sourceSizeChanged)
    QML_WRITABLE_PROPERTY(qreal, scaleFactor)
    QML_WRITABLE_PROPERTY(qreal, fitToWidthScale)

public:
    explicit ImageViewer(QObject *parent = nullptr);
    ImageViewer(const FileObject &file, QObject *parent = nullptr);

    const QSize &sourceSize() const;
    void setSourceSize(const QSize &newSourceSize);
    virtual Type type() const;

    Q_INVOKABLE void zoomIn();
    Q_INVOKABLE void zoomOut();
signals:

    void sourceSizeChanged();
private:
    QList<qreal> m_scales;
    QSize m_sourceSize;
};

#endif // IMAGEVIEWER_H
