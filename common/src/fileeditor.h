#ifndef FILEEDITOR_H
#define FILEEDITOR_H

#include "editor.h"

#include "FileObject.h"
#include "associatedtaglistmodel.h"

#include "fileviewer.h"
#include "fileviewermanager.h"

class FileEditor : public Editor
{
    Q_OBJECT
    Q_PROPERTY(FileViewer* fileViewer READ fileViewer NOTIFY fileViewerChanged)
    Q_PROPERTY(FileViewerManager* fileViewerManager READ fileViewerManager NOTIFY fileViewerManagerChanged)
//    Q_PROPERTY(QIODevice* device READ device NOTIFY objectChanged)

public:
    explicit FileEditor(QObject *parent = nullptr);
    Q_INVOKABLE void create() override;
    Q_INVOKABLE bool edit(const QUuid &id, SyncableObject::ObjectType) override;
    Q_INVOKABLE bool editList(const QModelIndexList& indexes) override;
    Q_INVOKABLE void save() override;
    Q_INVOKABLE void saveLinkedArca(const QModelIndexList& indexes);
    Q_INVOKABLE bool openFile();
    Q_INVOKABLE void saveMarkdownContent(const QString& content);
    void cleanup() override;
    FileViewer* getViewer(const QUuid& fileId) const;

    // Properties
    FileObject *object() const override;

    FileViewer *fileViewer() const;

//    QIODevice *device() const;

    FileViewerManager *fileViewerManager() const;

Q_SIGNALS:
    void fileViewerChanged();    
    void fileViewerManagerChanged();

protected:
    SyncableObject::ObjectType editorType() override;

private:
    void refreshObject();
    void setObject(const QUuid& id);
    void updateObjects();
    void initTagsList();
    void initSyncStatus();
    void initPermissions();
    void saveTags();
    void saveSyncStatus();
    void savePermissions();

    void initLinkedArcaList();
    void saveLinkedArcaList();

    FileObjectPtr m_object;
    FileObjectPtrList m_objects;

    QScopedPointer<FileViewer> m_fileViewer;
    FileViewerManager* m_fileViewerManager{};

//    QSharedPointer<QIODevice> m_device;
};

#endif // FILEEDITOR_H
