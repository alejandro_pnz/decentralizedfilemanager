#include "fileeditor.h"
#include "filelistmodel.h"

#include <QDebug>
#include <arcalistmodel.h>
#include "DatabaseManager.h"
#include <QUuid>

#include "documentviewer.h"
#include "fileviewermanager.h"
#include "utility.h"

FileEditor::FileEditor(QObject *parent)
    : Editor(parent),
      m_fileViewerManager(new FileViewerManager(this))
{
    emit fileViewerChanged();
    qRegisterMetaType<FileViewerManager*>("FileViewerManager*");
    connect(DatabaseManager::get(), &DatabaseManager::filesUpdated, this, &FileEditor::refreshObject);
}

void FileEditor::create()
{
    m_currentFlow = Add;

    cleanup();

    m_object.reset(new FileObject);
    m_object->setFileType("TXT"); // TODO: not sure it set here
    emit objectChanged();

    m_fileViewer.reset(FileViewerManager::createViewer(m_object.get()));
    Q_EMIT this->fileViewerChanged();
}

bool FileEditor::edit(const QUuid &id, SyncableObject::ObjectType)
{
    if (m_object) {
        // File already edited
        if (m_object->id() == id) return true;
    }

    m_currentFlow = Edit;

    cleanup();

    m_object = FileListModel::get()->file(id);
    emit objectChanged();

    if (m_object) {
        setName(m_object->name());
        setNotes(m_object->notes());
        setSyncProperty(m_object->syncProperty());
        setPermissions(m_object->permissions());
        initLinkedArcaList();
        m_taglistModel->setTags(m_object->tags());

        m_fileViewer.reset(FileViewerManager::createViewer(m_object.get()));
        Q_EMIT this->fileViewerChanged();

        return true;
    }

    qWarning("Cannot find file with given id");
    return false;
}

bool FileEditor::editList(const QModelIndexList &indexes)
{
    qDebug() << Q_FUNC_INFO;
    if (indexes.isEmpty()) return false;

    m_currentFlow = Edit;

    cleanup();

    const auto ids = FileListModel::get()->idsFromModelIndexes(indexes);
    for (const auto& id : ids) {
        auto object = FileListModel::get()->file(id);
        if (object) {
            m_objects.append(object);
        } else {
            qWarning("Cannot find file with given id");
            return false;
        }
    }

    if (m_objects.isEmpty()) {
        cleanup();
        return true;
    }

    // TODO decide what to do with single object list
    initTagsList();
    initSyncStatus();
    initPermissions();
    initLinkedArcaList();

    return true;
}

void FileEditor::save()
{
    if (m_objects.isEmpty()) {
        const auto ownerId = UserManager::get()->currentUser() ? UserManager::get()->currentUser()->userId() : 0;
        m_object->setName(name());
        m_object->setOwnerId(ownerId);
        m_object->setNotes(notes());
        m_object->setSyncProperty(syncProperty());
        m_object->setPermissions(permissions());
        m_object->setTags(m_taglistModel->tags());

        bool ret = m_object->save();
        saveLinkedArcaList();
        qDebug() << "FileEditor::save()" << ret;
    }
    else {
        // Multiple objects save
        saveTags();
        saveSyncStatus();
        savePermissions();
        saveLinkedArcaList();
        // request db update for object list
        updateObjects();
    }
}

void FileEditor::saveLinkedArca(const QModelIndexList &indexes)
{
    setLinkedArcaIds(ArcaListModel::get()->idsFromModelIndexes(indexes));
}

bool FileEditor::openFile()
{
   if (m_object) {
       return m_fileViewerManager->prepareFile(m_object.get());
   }
   else {
       qWarning() << Q_FUNC_INFO << "Trying to open non existing file";
       return false;
   }
}

void FileEditor::saveMarkdownContent(const QString &content)
{
    if (m_object && m_fileViewer) {
        const auto device = DatabaseManager::get()->fileData(m_object.get());

        if (device) {
            device->close();
            device->open(QIODevice::WriteOnly | QIODevice::Truncate);
            device->write(content.toUtf8());
            device->close();
            qDebug() << Q_FUNC_INFO << "setcontent Save file content" << m_object->name() << content.toUtf8();

            m_fileViewer->update();
        }
    }
}

void FileEditor::cleanup()
{
    m_object.clear();
//    m_device.clear();
    emit objectChanged();

    m_objects.clear();

    Editor::cleanup();
}

FileViewer *FileEditor::getViewer(const QUuid& fileId) const
{
    auto mV = fileViewerManager()->getViewer(fileId);
    if (mV)
        return mV;
    if (m_object) {
        if (m_object->id() == fileId) {
            return fileViewer();
        }
    }
    return {};
}

FileObject *FileEditor::object() const
{
    return m_object.get();
}

FileViewer *FileEditor::fileViewer() const
{
    return m_fileViewer.get();
}

//QIODevice *FileEditor::device() const
//{
//    return m_device.get();
//}

void FileEditor::refreshObject()
{
    qDebug() << Q_FUNC_INFO << "REFRESH OBJECT" << m_object;
    if (m_object) setObject(m_object->id());
}

void FileEditor::setObject(const QUuid &id)
{
    m_object = FileListModel::get()->file(id);
    emit objectChanged();
}

void FileEditor::updateObjects()
{
    FileObjectList list;
    for (const auto& object : qAsConst(m_objects)) {
        list << *object.get();
    }
    DatabaseManager::get()->update(list);
}

void FileEditor::initLinkedArcaList()
{
    if (m_objects.isEmpty()) {
        // Editing single object
        const auto linkedArcaList = DatabaseManager::get()->linkedArca(m_object.get());
        qDebug() << Q_FUNC_INFO << "Linked arcas" << linkedArcaList.size()
                 << m_object->name();
        for (const auto& arca : linkedArcaList) {
            qDebug() << Q_FUNC_INFO << arca.name();
        }
        if (!linkedArcaList.isEmpty()) {
            QList<QUuid> idsList;
            idsList.reserve(linkedArcaList.size());
            for (const auto& arca : linkedArcaList) {
                idsList.append(arca.id());
            }
            setLinkedArcaIds(idsList);
        }
    } else {
        // Editing multiple objects
        QHash<QUuid, int> commonPart;
        const auto firstLinkedArca = DatabaseManager::get()->linkedArca(m_objects.first().get());
        commonPart.reserve(firstLinkedArca.size());
        for (const auto& obj : firstLinkedArca) {
            commonPart.insert(obj.id(), 1);
        }

        // Iterate through other objects
        for (auto it = m_objects.cbegin() + 1; it != m_objects.cend(); it++) {
            const auto linked = DatabaseManager::get()->linkedArca(it->get());
            for (const auto& obj: linked) {
                auto iterator = commonPart.find(obj.id());
                if (iterator != commonPart.end()) {
                    qDebug() << "OBJECT" << (*it)->name() << "has common linked Arca" << iterator.value();
                    iterator.value() = iterator.value() + 1;
                }
            }
        }

        // Get the tag list with frequency of m_objects.size()
        auto commonLink = commonPart.keys(m_objects.size());
        qDebug() << Q_FUNC_INFO << "Common linked" << commonLink.size();
        setLinkedArcaIds(commonLink);
    }
}

void FileEditor::saveLinkedArcaList()
{
    // Get the ids of selected arcas to link to
    const auto arcas = ArcaListModel::get()->objectList(m_linkedArcaIds);    
    FileObjectList files;
    if (m_object) {
        files.append(m_object.get());
    } else {
        files.reserve(m_objects.size());
        for (const auto& object : qAsConst(m_objects)) {
            files << *object.get();
        }
    }

    qDebug() << Q_FUNC_INFO
             << "Selected arcas" << arcas.size()
             << "Files" << files.size()
             << "Action" << m_linkArcaAction;

    switch (m_linkArcaAction) {
    case FileEditor::AddToArca:
        // Link to arca
        for (const auto& arca : arcas) {
            qDebug() << Q_FUNC_INFO << "Linking to arca" << arca.name() << files.size() << "files";
            if (!DatabaseManager::get()->addArcaFiles(arca, files)) {
                qWarning() << Q_FUNC_INFO << "Error linking to" << arca.name();
            }
        }
        break;
    case FileEditor::MoveToArca:
        qDebug() << "NOT IMPLEMENTED";
        break;
    case FileEditor::RemoveFromArca:
        for (const auto& arca : arcas) {
            qDebug() << Q_FUNC_INFO << "Removing from arca" << arca.isValid() << arca.name() << files.size() << "files";
            for (const auto& file : arca.files()) {
                qDebug() << Q_FUNC_INFO << file.name();
            }
            for (const auto& file : qAsConst(files)) {
//                DatabaseManager::get()->
                qDebug() << Q_FUNC_INFO << "Is file valid?" << file.isValid();
                qDebug() << file.name() << "index of" << arca.files().indexOf(file);
            }
            if (!DatabaseManager::get()->removeArcaFiles(arca, files)) {
                qWarning() << Q_FUNC_INFO << "Error removing from" << arca.name();
            }
        }
        break;
    }
}

void FileEditor::initTagsList()
{
    QHash<TagObject, int> commonPart;

    // Initialize the hash table
    const auto firstTags = m_objects.first()->tags();
    qDebug() << Q_FUNC_INFO << "Tags in first object" << firstTags.size();
    commonPart.reserve(commonPart.size());
    for (const auto& tag: firstTags) {
        commonPart.insert(tag, 1);
    }

    // Iterate through other objects
    for (auto it = m_objects.cbegin() + 1; it != m_objects.cend(); it++) {
        const auto tags = (*it)->tags();
        for (const auto& tag: tags) {
            auto iterator = commonPart.find(tag);
            if (iterator != commonPart.end()) {
                qDebug() << "OBJECT" << (*it)->name() << "has common tag" << iterator.value();
                iterator.value() = iterator.value() + 1;
            }
        }
    }

    // Get the tag list with frequency of m_objects.size()
    auto commonTags = commonPart.keys(m_objects.size());
    qDebug() << Q_FUNC_INFO << "Common tags" << commonTags.size();
    m_taglistModel->setTags(commonTags);
    m_tagsIntersection = QuodArca::asSet(commonTags);
}

void FileEditor::initSyncStatus()
{
    auto syncProperty = m_objects.first()->syncProperty();
    auto allTheSame = std::all_of(m_objects.cbegin(), m_objects.cend(), [syncProperty](const FileObjectPtr& ptr){
        return ptr->syncProperty() == syncProperty;
    });

    setSyncProperty(allTheSame ? syncProperty : defaultSyncProperty());
}

void FileEditor::initPermissions()
{
    auto permissions = m_objects.first()->permissions();
    auto allTheSame = std::all_of(m_objects.cbegin(), m_objects.cend(), [permissions](const FileObjectPtr& ptr){
        return ptr->permissions() == permissions;
    });

    setPermissions(allTheSame ? permissions : defaultPermission());
}

void FileEditor::saveTags()
{
    auto tags = QuodArca::asSet(m_taglistModel->tags());
    // The set of removed tags
    auto removedTags = m_tagsIntersection.subtract(tags);
    for (auto& object : m_objects) {
        // Remove tags from object that has been removed during edition
        auto objectTags = QuodArca::asSet(object->tags()).subtract(removedTags);
        // Add tags that have been added during edition
        objectTags = objectTags.unite(tags);
        auto tagsList = objectTags.values();
        object->setTags(tagsList);
    }
}

void FileEditor::saveSyncStatus()
{
    for (auto& object : m_objects) {
        object->setSyncProperty(syncProperty());
    }
}

void FileEditor::savePermissions()
{
    for (auto& object : m_objects) {
        object->setPermissions(permissions());
    }
}


SyncableObject::ObjectType FileEditor::editorType()
{
    return SyncableObject::File;
}

FileViewerManager *FileEditor::fileViewerManager() const
{
    qDebug() << "GETTER" << m_fileViewerManager;
    return m_fileViewerManager;
}
