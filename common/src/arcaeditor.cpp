#include "arcaeditor.h"
#include <QDebug>
#include "arcalistmodel.h"
#include "filelistmodel.h"
#include "quodlistmodel.h"
#include "quodfilelistmodel.h"
#include "utility.h"
#include "DatabaseManager.h"

ArcaEditor::ArcaEditor(QObject *parent)
    : Editor(parent),
      m_linkedObjectsModel(new QItemSelectionModel)
{
    m_linkedObjectsModel->setModel(QuodFileListModel::get());
    connect(m_linkedObjectsModel, &QItemSelectionModel::selectionChanged, this, &ArcaEditor::updateSelectedLinkedObjects);
    connect(DatabaseManager::get(), &DatabaseManager::arcaUpdated, this, &ArcaEditor::refreshObject);
    connect(this, &ArcaEditor::objectChanged, this, &ArcaEditor::initLinkedObjectsList, Qt::QueuedConnection);
}

void ArcaEditor::create()
{
    m_currentFlow = Add;

    cleanup();

    m_object.reset(new ArcaObject);
    emit objectChanged();
}

bool ArcaEditor::edit(const QUuid &id, SyncableObject::ObjectType)
{
    m_currentFlow = Edit;

    cleanup();

    setObject(id);

    if (m_object) {
        setName(m_object->name());
        setColor(m_object->color());
        setNotes(m_object->notes());
        setSyncProperty(m_object->syncProperty());
        setPermissions(m_object->permissions());
        m_taglistModel->setTags(m_object->tags());

        qDebug() << Q_FUNC_INFO << m_object->quods().size();

        return true;
    }

    qWarning("Cannot find arca with given id");
    return false;
}

void ArcaEditor::initTagsList()
{
    QHash<TagObject, int> commonPart;

    // Initialize the hash table
    const auto firstTags = m_objects.first()->tags();
    qDebug() << Q_FUNC_INFO << "Tags in first object" << firstTags.size();
    commonPart.reserve(commonPart.size());
    for (const auto& tag: firstTags) {
        commonPart.insert(tag, 1);
    }

    // Iterate through other objects
    for (auto it = m_objects.cbegin() + 1; it != m_objects.cend(); it++) {
        const auto tags = (*it)->tags();
        for (const auto& tag: tags) {
            auto iterator = commonPart.find(tag);
            if (iterator != commonPart.end()) {
                qDebug() << "OBJECT" << (*it)->name() << "has common tag" << iterator.value();
                iterator.value() = iterator.value() + 1;
            }
        }
    }

    // Get the tag list with frequency of m_objects.size()
    auto commonTags = commonPart.keys(m_objects.size());
    qDebug() << Q_FUNC_INFO << "Common tags" << commonTags.size();
    m_taglistModel->setTags(commonTags);
    m_tagsIntersection = QuodArca::asSet(commonTags);
}

void ArcaEditor::initSyncStatus()
{
    auto syncProperty = m_objects.first()->syncProperty();
    auto allTheSame = std::all_of(m_objects.cbegin(), m_objects.cend(), [syncProperty](const ArcaObjectPtr& ptr){
        return ptr->syncProperty() == syncProperty;
    });

    setSyncProperty(allTheSame ? syncProperty : defaultSyncProperty());
}

void ArcaEditor::initPermissions()
{
    auto permissions = m_objects.first()->permissions();
    auto allTheSame = std::all_of(m_objects.cbegin(), m_objects.cend(), [permissions](const ArcaObjectPtr& ptr){
        return ptr->permissions() == permissions;
    });

    setPermissions(allTheSame ? permissions : defaultPermission());
}

void ArcaEditor::initLinkedObjectsList()
{
    if (m_object) {
        m_linkedObjectsIdList.clear();
        m_linkedObjectsModel->clearSelection();
        m_linkedFiles.clear();
        m_linkedQuods.clear();

        const auto quods = m_object->quods();
        for (const auto& quod : quods) {
            auto index = QuodFileListModel::get()->indexOf(quod.id());
            qDebug() << Q_FUNC_INFO << "Index" << index;
            if (index.isValid()) {
                m_linkedObjectsModel->select(index, QItemSelectionModel::Select);
            }
            m_linkedObjectsIdList << quod.id();
            m_linkedQuods << quod.id();
        }
        const auto files = m_object->files();
        for (const auto& file : files) {
            auto index = QuodFileListModel::get()->indexOf(file.id());
            if (index.isValid()) m_linkedObjectsModel->select(index, QItemSelectionModel::Select);
            m_linkedObjectsIdList << file.id();
            m_linkedFiles << file.id();
        }
        qDebug() << Q_FUNC_INFO << quods.size() << files.size();
        emit linkedObjectsIdListChanged();
    }
}

bool ArcaEditor::editList(const QModelIndexList &indexes)
{
    qDebug() << Q_FUNC_INFO;
    if (indexes.isEmpty()) return false;

    m_currentFlow = Edit;

    cleanup();

    const auto ids = ArcaListModel::get()->idsFromModelIndexes(indexes);
    for (const auto& id : ids) {
        auto object = ArcaListModel::get()->arca(id);
        if (object) {
            m_objects.append(object);
        } else {
            qWarning("Cannot find arca with given id");
            return false;
        }
    }

    if (m_objects.isEmpty()) {
        cleanup();
        return true;
    }

    // TODO decide what to do with single object list
    initTagsList();
    initSyncStatus();
    initPermissions();

    return true;
}

void ArcaEditor::saveTags()
{
    auto tags = QuodArca::asSet(m_taglistModel->tags());
    // The set of removed tags
    auto removedTags = m_tagsIntersection.subtract(tags);
    for (auto& object : m_objects) {
        // Remove tags from object that has been removed during edition
        auto objectTags = QuodArca::asSet(object->tags()).subtract(removedTags);
        // Add tags that have been added during edition
        objectTags = objectTags.unite(tags);
        auto tagsList = objectTags.values();
        object->setTags(tagsList);
    }
}

void ArcaEditor::saveSyncStatus()
{
    for (auto& object : m_objects) {
        object->setSyncProperty(syncProperty());
    }
}

void ArcaEditor::savePermissions()
{
    for (auto& object : m_objects) {
        object->setPermissions(permissions());
    }
}

void ArcaEditor::saveLinkedObjectList()
{
    if (m_object) {
        // Get selected objects
        const auto indexes = m_linkedObjectsModel->selectedIndexes();
        const auto idRole = QuodFileListModel::get()->roleNames().key("id");
        const auto objectTypeRole = QuodFileListModel::get()->roleNames().key("objectType");
        QList<QUuid> quodIds;
        QList<QUuid> fileIds;
        for (const auto& index : indexes) {
            auto id = index.data(idRole).toUuid();
            auto type = index.data(objectTypeRole);
            if (type == SyncableObject::Quod) {
                quodIds << id;
            } else if (type == SyncableObject::File) {
                fileIds << id;
            }
        }

        auto removedFiles = QuodArca::asSet(m_linkedFiles).subtract(QuodArca::asSet(fileIds));
        auto removedQuods = QuodArca::asSet(m_linkedQuods).subtract(QuodArca::asSet(quodIds));

        auto newFiles = QuodArca::asSet(fileIds).subtract(QuodArca::asSet(m_linkedFiles));
        auto newQuods = QuodArca::asSet(quodIds).subtract(QuodArca::asSet(m_linkedQuods));

        qDebug() << Q_FUNC_INFO << QStringLiteral("Removing %1 files, %2 quods, link %3 files, %4 quods")
                    .arg(removedFiles.size()).arg(removedQuods.size())
                    .arg(newFiles.size()).arg(newQuods.size());

        bool ret{false};

        DatabaseManager::get()->blockSignals(true);
        FileObjectList filesToRemove = FileListModel::get()->objectList(removedFiles.values());
        ret = DatabaseManager::get()->removeArcaFiles(m_object.get(), filesToRemove);

        if (!ret) qWarning() << Q_FUNC_INFO << "Failed to remove files";

        QuodObjectList quodsToRemove = QuodListModel::get()->objectList(removedQuods.values());
        ret = DatabaseManager::get()->removeArcaQuods(m_object.get(), quodsToRemove);

        if (!ret) qWarning() << Q_FUNC_INFO << "Failed to remove quods";

        FileObjectList filesToLink = FileListModel::get()->objectList(newFiles.values());
        ret = DatabaseManager::get()->addArcaFiles(m_object.get(), filesToLink);

        if (!ret) qWarning() << Q_FUNC_INFO << "Failed to link files";

        QuodObjectList quodsToLink = QuodListModel::get()->objectList(newQuods.values());
        for (const auto& o : quodsToLink) {
            qDebug() << Q_FUNC_INFO << "LINK" << o.name() << o.isValid() << m_object->name();
        }
        ret = DatabaseManager::get()->addArcaQuods(m_object.get(), quodsToLink);
        if (!ret) qWarning() << Q_FUNC_INFO << "Failed to link quods";
        DatabaseManager::get()->blockSignals(false);

        Q_EMIT DatabaseManager::get()->arcaUpdated({m_object->id()});
        if (!removedFiles.isEmpty() || !newFiles.isEmpty())
            Q_EMIT DatabaseManager::get()->filesUpdated(removedFiles.unite(newFiles).values());
        if (!removedQuods.isEmpty() || !newQuods.isEmpty()) {
            Q_EMIT DatabaseManager::get()->quodUpdated(removedQuods.unite(newQuods).values());
        }
    }
}

void ArcaEditor::save()
{
    if (m_objects.isEmpty()) {
        // Single object save
        const auto ownerId = UserManager::get()->currentUser() ? UserManager::get()->currentUser()->userId() : 0;
        m_object->setName(name());
        m_object->setOwnerId(ownerId);
        m_object->setColor(color());
        m_object->setNotes(notes());
        m_object->setTags(m_taglistModel->tags());
        m_object->setSyncProperty(syncProperty());
        m_object->setPermissions(permissions());

        // TODO: spawn a thread
        bool ret = m_object->save();
        saveLinkedObjectList();

        qDebug() << "ArcaEditor::save()" << ret;
    } else {
        // Multiple objects save
        saveTags();
        saveSyncStatus();
        savePermissions();
        // request db update for object list
        updateObjects();
    }
}

ArcaObject *ArcaEditor::object() const
{
    return m_object.get();
}

QString ArcaEditor::color() const
{
    return m_color;
}

void ArcaEditor::setColor(const QString &color)
{
    if (color == m_color) {
        return;
    }
    m_color = color;
    emit colorChanged();
}

SyncableObject::ObjectType ArcaEditor::editorType()
{
    return SyncableObject::Arca;
}

void ArcaEditor::updateSelectedLinkedObjects()
{
    m_selectedObjectsIds.clear();
    auto indexes = m_linkedObjectsModel->selectedIndexes();
    for (auto idx : indexes) {
        auto uuid = idx.data(QuodFileListModel::get()->idRole()).toUuid();
        qDebug() << "UUID" << uuid;
        m_selectedObjectsIds.append(uuid);
    }
    emit selectedObjectsIdsChanged();
}

void ArcaEditor::refreshObject()
{
    qDebug() << Q_FUNC_INFO << "REFRESH OBJECT" << m_object;
    if (m_object) setObject(m_object->id());
}

void ArcaEditor::setObject(const QUuid &id)
{
    m_object = ArcaListModel::get()->arca(id);
    emit objectChanged();
}

void ArcaEditor::updateObjects()
{
    // TODO add possibility to pass QSharedPtrList to DatabaseManager::update
    QList<ArcaObject> list;
    for (const auto& object : qAsConst(m_objects)) {
        list << *object.get();
    }
    DatabaseManager::get()->update(list);
}

void ArcaEditor::cleanup()
{
    qDebug() << Q_FUNC_INFO;
    m_linkedObjectsIdList.clear();
    m_linkedFiles.clear();
    m_linkedQuods.clear();
    m_linkedObjectsModel->clearSelection();

    m_color.clear();
    emit colorChanged();

    m_object.clear();
    emit objectChanged();

    m_objects.clear();

    Editor::cleanup();
}

const QVariantList &ArcaEditor::linkedObjectsIdList() const
{
    return m_linkedObjectsIdList;
}

const QVariantList &ArcaEditor::selectedObjectsIds() const
{
    return m_selectedObjectsIds;
}
