#include "themesettings.h"
#include <QCoreApplication>
#include <QQmlEngine>

ThemeSettings::ThemeSettings(QObject* parent) : QObject(parent)
{
}

void ThemeSettings::setActiveThemeId(const QString & themeId)
{
    m_themeSettings.setValue(QStringLiteral("activeTheme"), themeId);
    emit activeThemeIdChanged();
}

QString ThemeSettings::activeThemeId() const noexcept
{
    return m_themeSettings.value(QStringLiteral("activeTheme"), QStringLiteral("c0b90da2-a71e-11eb-bcbc-0242ac130002")).toString();
}

static QObject *themeSettingsFunction(QQmlEngine *, QJSEngine *)
{
    ThemeSettings *singletonInstance = new ThemeSettings();
    return singletonInstance;
}

namespace
{
void _q_ThemeSettings_registerTypes()
{
    qmlRegisterSingletonType<ThemeSettings>("QuodArca.ThemeSettings", 1, 0, "ThemeSettings", themeSettingsFunction);
}
}
Q_COREAPP_STARTUP_FUNCTION(_q_ThemeSettings_registerTypes)

