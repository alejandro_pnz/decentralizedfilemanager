#ifndef FILEVIEWERMANAGER_H
#define FILEVIEWERMANAGER_H

#include <QObject>
#include <FileObject.h>
#include <QAbstractListModel>

class FileViewer;
class FileViewerManager : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit FileViewerManager(QObject *parent = nullptr);
    ~FileViewerManager();

    Q_INVOKABLE bool prepareFile(const FileObject& file);
    Q_INVOKABLE void closeFile(const QUuid& id);
    Q_INVOKABLE void closeAllFiles();
    FileViewer* getViewer(const QUuid& fileId);

    static FileViewer* createViewer(const FileObject& file);

signals:
    void setupViewer(const int index);
    void lastFileClosed();
private:
    QList<FileViewer*> m_viewers;
    int indexOfViewer(const QUuid& fileId) const;
    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
private:
    QHash<int, QByteArray> m_roleNames {{Qt::UserRole, "fileName"},
                                       {Qt::UserRole+1, "fileViewer"}};
};



#endif // FILEVIEWERMANAGER_H
