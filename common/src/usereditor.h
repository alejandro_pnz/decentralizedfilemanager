/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef USEREDITOR_H
#define USEREDITOR_H

#include "editor.h"

#include <QModelIndexList>
#include <UserObject.h>

class UserEditor : public Editor
{
    Q_OBJECT

public:
    explicit UserEditor(QObject *parent = nullptr);

    // Editor interface
    void create() override;
    bool edit(const QUuid &id, SyncableObject::ObjectType type) override;
    bool editList(const QModelIndexList &indexes) override;
    void save() override;
    void cleanup() override;
    UserObject *object() const override;

protected:
    SyncableObject::ObjectType editorType() override;

private:
    void refreshObject();
    void setObject(const QUuid& id);

    UserObjectPtr m_object;
};

#endif // USEREDITOR_H
