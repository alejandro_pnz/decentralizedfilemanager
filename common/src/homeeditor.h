#ifndef HOMEEDITOR_H
#define HOMEEDITOR_H

#include "editor.h"
#include "FileObject.h"
#include "QuodObject.h"
#include "ArcaObject.h"

class HomeEditor : public Editor
{
    Q_OBJECT
public:
    explicit HomeEditor(QObject *parent = nullptr);

    void create() override;
    bool edit(const QUuid &id, SyncableObject::ObjectType) override;
    bool editList(const QModelIndexList &indexes) override;
    void save() override;
    SyncableObject *object() const override;
    void cleanup() override;
    Q_INVOKABLE void saveLinkedArca(const QModelIndexList& indexes);

protected:
    SyncableObject::ObjectType editorType() override;

private:
    void updateObjects();
    void initTagsList();
    void initSyncStatus();
    void initPermissions();
    void saveTags();
    void saveSyncStatus();
    void savePermissions();

    void initLinkedArcaList();
    void saveLinkedArcaList();

    void linkFiles(const FileObjectList& list, const ArcaObjectList& arcas);
    void linkQuods(const QuodObjectList& list, const ArcaObjectList& arcas);

    TagObjectList tagList(QSharedPointer<SyncableObject> object);
    void setTags(QSharedPointer<SyncableObject> object, TagObjectList list);
    QSharedPointer<SyncableObject> m_object;
    QList<QSharedPointer<SyncableObject>> m_objects;
};

#endif // HOMEEDITOR_H
