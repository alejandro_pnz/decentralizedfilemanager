/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "usereditor.h"

#include <UserManager.h>

UserEditor::UserEditor(QObject *parent) : Editor(parent)
{

}

void UserEditor::create()
{

}

bool UserEditor::edit(const QUuid &id, SyncableObject::ObjectType type)
{
    cleanup();
    setObject(id);
    return !m_object.isNull();
}

bool UserEditor::editList(const QModelIndexList &indexes)
{
    Q_UNUSED(indexes);
    return false;
}

void UserEditor::save()
{

}

void UserEditor::cleanup()
{
    m_object.clear();
    emit objectChanged();
}

UserObject *UserEditor::object() const
{
    return m_object.get();
}

SyncableObject::ObjectType UserEditor::editorType()
{
    return SyncableObject::User;
}

void UserEditor::refreshObject()
{
    if (m_object) setObject(m_object->id());
}

void UserEditor::setObject(const QUuid &id)
{
    auto user = UserManager::get()->user(id);
    if (user.isValid()) {
        m_object = UserObjectPtr(new UserObject(user));
    }
}
