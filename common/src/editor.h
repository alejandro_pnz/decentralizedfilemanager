#ifndef EDITOR_H
#define EDITOR_H

#include <QObject>
#include <QModelIndexList>
#include <qqmlhelpers.h>
#include <SyncableObject.h>
#include <TagObject.h>
#include <QSet>

#include "associatedtaglistmodel.h"

class SyncableObject;

class Editor : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString notes READ notes WRITE setNotes NOTIFY notesChanged)
    Q_PROPERTY(SyncableObject::SyncProperty syncProperty READ syncProperty WRITE setSyncProperty NOTIFY syncPropertyChanged)
    Q_PROPERTY(SyncableObject::Permissions permissions READ permissions WRITE setPermissions NOTIFY permissionsChanged)
    Q_PROPERTY(AssociatedTagListModel* taglistModel READ taglistModel CONSTANT)
    Q_PROPERTY(SyncableObject* object READ object NOTIFY objectChanged)

    Q_PROPERTY(bool invertLinkedList READ invertLinkedList NOTIFY invertLinkedListChanged)
    Q_PROPERTY(QVariantList linkedArcaIds READ linkedArcaIds NOTIFY linkedArcaIdsChanged)
public:
    enum Flow {
        Add,
        Edit
    };

    enum LinkArcaAction {
        AddToArca,
        MoveToArca,
        RemoveFromArca
    };
    Q_ENUM(LinkArcaAction)

    explicit Editor(QObject *parent = nullptr);

    virtual void create() = 0;
    virtual bool edit(const QUuid &id, SyncableObject::ObjectType type = SyncableObject::Generic) = 0;
    virtual bool editList(const QModelIndexList& indexes) = 0;
    virtual void save() = 0;
    virtual void cleanup();
    virtual SyncableObject *object() const = 0;

    Q_INVOKABLE void preprareToDelete(const QUuid &id, SyncableObject::ObjectType type);
    Q_INVOKABLE void preprareToDelete(const QModelIndexList& indexes);
    Q_INVOKABLE void proceedDeletion();
    Q_INVOKABLE void abortDeletion();

    Q_INVOKABLE void removeOne(const QUuid &id, SyncableObject::ObjectType type);
    Q_INVOKABLE void removeList(const QModelIndexList& indexes);
    Q_INVOKABLE void duplicateList(const QModelIndexList& indexes);
    Q_INVOKABLE void duplicateOne(const QUuid &id);

    QString name() const;
    void setName(const QString &name);

    QString notes() const;
    void setNotes(const QString &notes);

    void setLinkArcaAction(Editor::LinkArcaAction action);

    SyncableObject::SyncProperty syncProperty() const;
    void setSyncProperty(SyncableObject::SyncProperty syncProperty);

    SyncableObject::SyncProperty defaultSyncProperty() const;
    SyncableObject::Permissions defaultPermission() const;
    const SyncableObject::Permissions &permissions() const;
    void setPermissions(const SyncableObject::Permissions &newPermissions);

    Q_INVOKABLE void setPermission(SyncableObject::Permission permission, bool on);

    AssociatedTagListModel *taglistModel() const;
    Q_INVOKABLE void setTags(const TagObjectList &tags);

    bool invertLinkedList() const;

    QVariantList linkedArcaIds() const;

signals:
    void nameChanged();
    void notesChanged();
    void objectChanged();
    void syncPropertyChanged();
    void permissionsChanged();
    void linkedArcaIdsChanged();
    void invertLinkedListChanged();
    void cleanSelection();

protected:
    virtual SyncableObject::ObjectType editorType() = 0;
    void setLinkedArcaIds(const QList<QUuid> &newLinkedArcaIds);

    QString m_name;
    QString m_notes;

    Flow m_currentFlow;
    SyncableObject::SyncProperty m_syncProperty;
    SyncableObject::Permissions m_permissions;

    QSet<TagObject> m_tagsIntersection;
    AssociatedTagListModel *m_taglistModel{nullptr};

    QVariantList m_linkedArcaIdsVL;
    QList<QUuid> m_linkedArcaIds;
    LinkArcaAction m_linkArcaAction{AddToArca};

    struct DeleteInfo {
        QUuid id;
        SyncableObject::ObjectType type;
        QModelIndexList indexes;
    };

    std::unique_ptr<DeleteInfo> m_deleteInfo;
};

#endif // EDITOR_H
