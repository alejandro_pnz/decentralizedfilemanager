/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef DOCUMENTVIEWER_H
#define DOCUMENTVIEWER_H

#include "fileviewer.h"
#include <QObject>
#include <qqmlhelpers.h>

class PdfDocument;

class DocumentViewer : public FileViewer
{
    Q_OBJECT
    Q_PROPERTY(qreal maxWidth READ maxWidth NOTIFY maxWidthChanged)
    Q_PROPERTY(int currentPage READ currentPage WRITE setCurrentPage NOTIFY currentPageChanged)
    QML_WRITABLE_PROPERTY(bool, canDrag)
    QML_WRITABLE_PROPERTY(qreal, scaleFactor)
    QML_WRITABLE_PROPERTY(int, contentY)
    QML_WRITABLE_PROPERTY(int, contentX)
    QML_WRITABLE_PROPERTY(qreal, fitToWidthScale)

public:
    explicit DocumentViewer(const FileObject &file, QObject *parent = nullptr);
    ~DocumentViewer();

    int pageCount() const override;

    virtual Q_INVOKABLE qreal pageWidth(int page) override;

    virtual Q_INVOKABLE qreal pageHeight(int page) override;

    virtual QImage render(int page) override;

    virtual Type type() const override;

    qreal maxWidth() const;

    int currentPage() const;
    void setCurrentPage(int newCurrentPage);

    Q_INVOKABLE void zoomIn();
    Q_INVOKABLE void zoomOut();

signals:
    void maxWidthChanged();

    void currentPageChanged();

private:
    QList<qreal> m_scales;
    void calculateMaxWidth();
    PdfDocument *m_document{nullptr};

    QSharedPointer<QIODevice> m_device;

    qreal m_maxWidth;
    int m_currentPage;
};

#endif // DOCUMENTVIEWER_H
