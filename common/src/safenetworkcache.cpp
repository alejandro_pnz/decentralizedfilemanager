/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "safenetworkcache.h"

SafeNetworkCache::SafeNetworkCache(const QString &path, QObject *parent)
    : QNetworkDiskCache(parent)
{
    setCacheDirectory(path);
}

QIODevice *SafeNetworkCache::data(const QUrl &url)
{
    QMutexLocker lock(&m_mutex);
    return QNetworkDiskCache::data(url);
}

void SafeNetworkCache::insert(QIODevice *device)
{
    QMutexLocker lock(&m_mutex);
    QNetworkDiskCache::insert(device);
}

QNetworkCacheMetaData SafeNetworkCache::metaData(const QUrl &url)
{
    QMutexLocker lock(&m_mutex);
    return QNetworkDiskCache::metaData(url);
}

QIODevice *SafeNetworkCache::prepare(const QNetworkCacheMetaData &metaData)
{
    QMutexLocker lock(&m_mutex);
    return QNetworkDiskCache::prepare(metaData);
}

bool SafeNetworkCache::remove(const QUrl &url)
{
    QMutexLocker lock(&m_mutex);
    return QNetworkDiskCache::remove(url);
}

void SafeNetworkCache::updateMetaData(const QNetworkCacheMetaData &metaData)
{
    QMutexLocker lock(&m_mutex);
    QNetworkDiskCache::updateMetaData(metaData);
}
