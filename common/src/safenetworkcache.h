/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef SAFENETWORKCACHE_H
#define SAFENETWORKCACHE_H

#include <QNetworkDiskCache>
#include <QMutex>

class SafeNetworkCache : public QNetworkDiskCache
{
    Q_OBJECT

public:
    explicit SafeNetworkCache(const QString &path, QObject *parent = nullptr);

    QIODevice *data(const QUrl &url) override;

    void insert(QIODevice *device) override;

    QNetworkCacheMetaData metaData(const QUrl &url) override;

    QIODevice *prepare(const QNetworkCacheMetaData &metaData) override;

    bool remove(const QUrl &url) override;

    void updateMetaData(const QNetworkCacheMetaData &metaData) override;

private:
    mutable QRecursiveMutex m_mutex;
};

#endif // SAFENETWORKCACHE_H
