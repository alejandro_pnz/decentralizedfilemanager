#ifndef QUODEDITOR_H
#define QUODEDITOR_H

#include "editor.h"

#include "QuodObject.h"
#include "associatedtaglistmodel.h"

#include "photoeditor.h"

class QuodEditor : public Editor
{
    Q_OBJECT
    Q_PROPERTY(SyncableObject* object READ object WRITE setObject NOTIFY objectChanged)
    Q_PROPERTY(bool editOneSection READ editOneSection NOTIFY editOneSectionChanged)
    Q_PROPERTY(QUuid editedSectionId READ editedSectionId WRITE setEditedSectionId NOTIFY editedSectionIdChanged)
    Q_PROPERTY(QString editedSectionName READ editedSectionName WRITE setEditedSectionName NOTIFY editedSectionNameChanged)
    Q_PROPERTY(QuodTypeObjectList subTypes READ subTypes WRITE setSubtypes NOTIFY subTypesChanged)
    Q_PROPERTY(PhotoEditor* photoEditor READ photoEditor CONSTANT)
    Q_PROPERTY(bool hasPhotos READ hasPhotos WRITE setHasPhotos NOTIFY hasPhotosChanged)
    Q_PROPERTY(QVariantList deletedPasswords READ deletedPasswords WRITE setDeletedPasswords NOTIFY deletedPasswordsChanged)
    Q_PROPERTY(AttributeHistoryList passwordList READ passwordList NOTIFY passwordListChanged)
public:
    struct AttributeLocation {
        bool valid{false};
        QUuid sectionId;
        QUuid attributeId;
        int sectionIndex{-1};

        void invalidate() {
            valid = false;
            sectionId = QUuid();
            attributeId = QUuid();
            sectionIndex = {-1};
        }

        void setLocation(const QUuid &sectionId, const QUuid &attributeId, int sectionIndex) {
            valid = true;
            this->sectionId = sectionId;
            this->attributeId = attributeId;
            this->sectionIndex = sectionIndex;
        }
    };

    explicit QuodEditor(QObject *parent = nullptr);
    Q_INVOKABLE void create() override;
    Q_INVOKABLE bool edit(const QUuid &id, SyncableObject::ObjectType) override;
    Q_INVOKABLE bool editList(const QModelIndexList& indexes) override;
    Q_INVOKABLE void save() override;
    Q_INVOKABLE void saveLinkedArca(const QModelIndexList& indexes);
    Q_INVOKABLE void cleanEditSection();
    void cleanup() override;

    Q_INVOKABLE void setAttributeValue(const QUuid &sectionId, const QUuid &attributeId, const QVariant& value, const int sectionIndex);

    // Password Generator feature
    void setPasswordAttributeForPasswordGenerator(const QUuid &sectionId, const QUuid &attributeId, int sectionIndex);
    Q_INVOKABLE void applyGeneratedPasswordToAttribute(const QString& password);


    // Password history
    Q_INVOKABLE void appendPasswordToDelete(const int passwordId);
    Q_INVOKABLE void removePasswords();
    void setPasswords(const AttributeHistoryList& list);


    // Properties

    QuodObject* object() const override;
    void setObject(SyncableObject *object);

    QUuid editedSectionId() const;
    void setEditedSectionId(QUuid newEditedSectionId);

    QuodTypeObjectList subTypes() const;
    void setSubtypes(const QuodTypeObjectList &newSubTypes);

    bool editOneSection() const;
    void setEditOneSection(bool newEditOneSection);

    const QString &editedSectionName() const;
    void setEditedSectionName(const QString &newEditedSectionName);

    PhotoEditor *photoEditor();
    bool hasPhotos() const;
    void setHasPhotos(bool newHasPhotos);

    const QVariantList &deletedPasswords() const;
    void setDeletedPasswords(const QVariantList &newDeletedPasswords);

    const AttributeHistoryList &passwordList() const;

signals:
    void attributesSaved();

    void globalEdit();

    void editedSectionIdChanged();

    void subTypesChanged();

    void editOneSectionChanged();

    void editedSectionNameChanged();

    void hasPhotosChanged();
    void passwordGenerated(const QUuid &sectionId, const QUuid &attributeId, const QString& password);

    void deletedPasswordsChanged();

    void passwordListChanged();

protected:
    SyncableObject::ObjectType editorType() override;

private:
    void refreshObject();
    void setObject(const QUuid& id);
    void updateObjects();
    void initTagsList();
    void initSyncStatus();
    void initPermissions();
    void saveTags();
    void saveSyncStatus();
    void savePermissions();

    void checkSections();
    void initLinkedArcaList();
    void saveLinkedArcaList();

    void setAddressAttributeValue(QuodSectionObject sectionWithAddress, const QString& value);
    QString getCombinedAddress(QuodSectionObject addressSection);
    QuodObjectPtr m_object;
    QuodObjectPtrList m_objects;
    QUuid m_editedSectionId;
    QuodTypeObjectList m_subTypes;
    bool m_editOneSection{false};
    QString m_editedSectionName;

    PhotoEditor* m_photoEditor{};
    bool m_hasPhotos{false};

    AttributeLocation m_passwordAttributeForPG;
    QVariantList m_deletedPasswords;
    AttributeHistoryList m_passwordList;
};

#endif // QUODEDITOR_H
