/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef VIEWERIMAGEPROVIDER_H
#define VIEWERIMAGEPROVIDER_H

#include <QQuickAsyncImageProvider>

#include <QDebug>
#include <QImage>
#include <QThreadPool>
#include <DatabaseManager.h>
#include <QtQml>

#include "homepagemanager.h"
#include "fileeditor.h"
#include "fileviewer.h"
#include "fileviewermanager.h"

class AsyncViewerImageResponseRunnable : public QObject, public QRunnable
{
    Q_OBJECT

signals:
    void done(QImage image);

public:
    AsyncViewerImageResponseRunnable(const QString &id, const QSize &requestedSize)
        : m_id(id), m_requestedSize(requestedSize) {}

    void run() override
    {
        QImage image;

        HomePageManager *homePageManager = HomePageManager::get();

        // The id structure is: <file_uuid>/<page>
        auto idList = m_id.split("/");
        Q_ASSERT(idList.size() == 2);
        auto fileId = idList.first();
        auto page = idList.last().toInt();
        auto viewer = homePageManager->get_fileEditor()->getViewer(QUuid(fileId));
        qDebug() << Q_FUNC_INFO << viewer << page << fileId;
        if (viewer) {
            image = viewer->render(page);

            if (!image.isNull() && m_requestedSize.isValid()) {
                image = image.scaled(m_requestedSize);
            }
        }

        Q_EMIT done(image);
    }

private:
    QString m_id;
    QSize m_requestedSize;
};

class AsyncViewerImageResponse : public QQuickImageResponse
{
    public:
        AsyncViewerImageResponse(const QString &id, const QSize &requestedSize, QThreadPool *pool)
        {
            auto runnable = new AsyncViewerImageResponseRunnable(QByteArray::fromPercentEncoding(id.toUtf8()), requestedSize);
            connect(runnable, &AsyncViewerImageResponseRunnable::done, this, &AsyncViewerImageResponse::handleDone);
            pool->start(runnable);
        }

        void handleDone(QImage image) {
            m_image = image;
            emit finished();
        }

        QQuickTextureFactory *textureFactory() const override
        {
            return QQuickTextureFactory::textureFactoryForImage(m_image);
        }

        QImage m_image;
};

class ViewerImageProvider : public QQuickAsyncImageProvider
{
public:
    ViewerImageProvider()
    {
        m_threadPool.setMaxThreadCount(3);
    }

    QQuickImageResponse *requestImageResponse(const QString &id, const QSize &requestedSize) override
    {
        AsyncViewerImageResponse *response = new AsyncViewerImageResponse(id, requestedSize, &m_threadPool);
        return response;
    }

private:
    QThreadPool m_threadPool;
};

#endif // VIEWERIMAGEPROVIDER_H
