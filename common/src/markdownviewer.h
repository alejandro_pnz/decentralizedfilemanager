#ifndef MARKDOWNVIEWER_H
#define MARKDOWNVIEWER_H

#include "fileviewer.h"
#include <qqmlhelpers.h>

class MarkdownViewer : public FileViewer
{
    Q_OBJECT
    QML_WRITABLE_PROPERTY(QString, content);

public:
    explicit MarkdownViewer(QObject *parent = nullptr);
    MarkdownViewer(const FileObject &file, QObject *parent = nullptr);

    virtual Type type() const override;
    virtual QString content() const override;
    virtual void update() override;

private:
    void setContent();
};

#endif // MARKDOWNVIEWER_H
