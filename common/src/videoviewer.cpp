#include "videoviewer.h"
#include <QmlVlcPlayer.h>
#include "DatabaseManager.h"

VideoViewer::VideoViewer(const FileObject &file, const bool hasVideo, QObject *parent)
    : FileViewer(file, parent), /*m_player(new QmlVlcPlayer(this)),*/ m_hasVideo(hasVideo)
{
    if (file.isValid()) {
        m_device = DatabaseManager::get()->fileData(file);
    }
}

VideoViewer::~VideoViewer()
{
    if (m_player && m_hasVideo) {
        m_player->stop();
    }
}

FileViewer::Type VideoViewer::type() const
{
    return VideoType;
}

QmlVlcPlayer *VideoViewer::player() const
{
    return m_player;
}

QIODevice *VideoViewer::device() const
{
    return m_device.get();
}

bool VideoViewer::hasVideo() const
{
    return m_hasVideo;
}
