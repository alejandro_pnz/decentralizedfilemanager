#include "quodeditor.h"
#include "quodlistmodel.h"
#include "arcalistmodel.h"
#include "utility.h"
#include "DatabaseManager.h"
#include "photoeditor.h"

QuodEditor::QuodEditor(QObject *parent)
    : Editor(parent),
      m_photoEditor(new PhotoEditor(this))
{
    connect(DatabaseManager::get(), &DatabaseManager::quodUpdated, this, &QuodEditor::refreshObject);
}

void QuodEditor::create()
{
    m_currentFlow = Add;

    cleanup();
    m_subTypes.clear();
    emit subTypesChanged();

    m_object.reset(new QuodObject);
    emit objectChanged();
}

bool QuodEditor::edit(const QUuid &id, SyncableObject::ObjectType)
{
    m_currentFlow = Edit;

    cleanup();

    m_object = QuodListModel::get()->quod(id);
    emit objectChanged();

    if (m_object) {
        setName(m_object->name());
        setNotes(m_object->notes());
        setSyncProperty(m_object->syncProperty());
        setPermissions(m_object->permissions());
        initLinkedArcaList();
        m_taglistModel->setTags(m_object->tags());
        checkSections();

        return true;
    }

    qWarning("Cannot find quod with given id");
    return false;
}

bool QuodEditor::editList(const QModelIndexList &indexes)
{
    qDebug() << Q_FUNC_INFO;
    if (indexes.isEmpty()) return false;

    m_currentFlow = Edit;

    cleanup();

    const auto ids = QuodListModel::get()->idsFromModelIndexes(indexes);
    for (const auto& id : ids) {
        auto object = QuodListModel::get()->quod(id);
        if (object) {
            m_objects.append(object);
        } else {
            qWarning("Cannot find quod with given id");
            return false;
        }
    }

    if (m_objects.isEmpty()) {
        cleanup();
        return true;
    }

    // TODO decide what to do with single object list
    initTagsList();
    initSyncStatus();
    initPermissions();
    initLinkedArcaList();

    return true;
}

void QuodEditor::save()
{
    if (m_objects.isEmpty()) {
        const auto ownerId = UserManager::get()->currentUser() ? UserManager::get()->currentUser()->userId() : 0;
        m_object->setName(name());
        m_object->setOwnerId(ownerId);
        m_object->setNotes(notes());
        m_object->setSyncProperty(syncProperty());
        m_object->setPermissions(permissions());
        m_object->setTags(m_taglistModel->tags());
        bool ret = m_object->save();
        saveLinkedArcaList();
        m_photoEditor->save();
        qDebug() << "QuodEditor::save()" << ret;
    } else {
        // Multiple objects save
        saveTags();
        saveSyncStatus();
        savePermissions();
        saveLinkedArcaList();
        // request db update for object list
        updateObjects();
    }
}

void QuodEditor::saveLinkedArca(const QModelIndexList &indexes)
{
    setLinkedArcaIds(ArcaListModel::get()->idsFromModelIndexes(indexes));
}

void QuodEditor::cleanEditSection()
{
    setEditedSectionId(QUuid());
    emit globalEdit();
}

void QuodEditor::cleanup()
{
    m_object.clear();
    emit objectChanged();

    m_objects.clear();

    setEditOneSection(false);
    setEditedSectionName(QString());

    // set null QUuid
    setEditedSectionId(QUuid());

    setHasPhotos(false);

    Editor::cleanup();
}

//void QuodEditor::setAttributeValue(const int sectionId, const int attributeId, const QVariant &value)
//{
//    if (m_object) {
//        qDebug() << Q_FUNC_INFO << m_object->name() << m_object->sections().size() << value;

//        if (sectionId < m_object->sections().size()) {
//            auto section = m_object->sections().at(sectionId);
//            if (attributeId < section.attributes().size()) {
//                auto attribute = section.attributes().at(attributeId);
//                attribute.setPlainValue(value.toByteArray());
//            } else {
//                qWarning() << Q_FUNC_INFO << "No such attribute";
//            }
//        } else {
//            qWarning() << Q_FUNC_INFO << "No such section";
//        }
//    }
//}

void QuodEditor::setAttributeValue(const QUuid &sectionId, const QUuid &attributeId, const QVariant &value, const int sectionIndex)
{
    if (m_object) {
        qDebug() << Q_FUNC_INFO << m_object->name() << m_object->sections().size() << value << sectionId << attributeId << sectionIndex << sectionId.isNull();

        QuodSectionObject section;
        QuodSectionObject sectionWithAddressAttribute;
        bool sectionIsValid{false};
        bool sectionIsAddress{false};

        if (sectionId.isNull()) {
            qDebug() << "Null section" << m_object->sections().size();
            return;
        }

        qDebug() << "section not null";
        auto sections = m_object->sections();
        auto sectionIt = std::find_if(sections.begin(), sections.end(), [sectionId](const QuodSectionObject& section){
            qDebug() << "Section is valid" << section.isValid() << section.id() << sectionId;
            return section.id() == sectionId;
        });
        if (sectionIt != sections.end()) {
            section = (*sectionIt);
            sectionIsValid = true;
            if (section.type() == QuodSectionObject::Type::Address) {
                qDebug() << Q_FUNC_INFO << "Address section spoted";
                sectionIsAddress = true;
                sectionWithAddressAttribute = (*--sectionIt);
                qDebug() << Q_FUNC_INFO << "Destination section" << sectionWithAddressAttribute.name();
            }
        }

        if (sectionIsValid) {
            auto attributes = section.attributes();
            auto attributeIt = std::find_if(attributes.begin(), attributes.end(), [attributeId](const AttributeObject& attribute){
                return attribute.id() == attributeId;
            });
            if (attributeIt != attributes.end()) {
                auto attribute = (*attributeIt);
                qDebug() << "ATTRIBUTE IS VALID" << attribute.isValid();
                if (!attribute.isValid()) {
                    qWarning() << Q_FUNC_INFO << "ERROR";
                } else {
                    qDebug() << "setting attribute" << attribute.name() << "to" << value;
                    attribute.setPlainValue(value.toByteArray());

                    if (sectionIsAddress) {
                        setAddressAttributeValue(sectionWithAddressAttribute, getCombinedAddress(section));
                    }
                }
            } else {
                qWarning() << Q_FUNC_INFO << "No such attribute";
            }
        } else {
            qWarning() << Q_FUNC_INFO << "No such section";
        }
    }
}

void QuodEditor::setPasswordAttributeForPasswordGenerator(const QUuid &sectionId, const QUuid &attributeId, int sectionIndex)
{
    m_passwordAttributeForPG.setLocation(sectionId, attributeId, sectionIndex);
}

void QuodEditor::applyGeneratedPasswordToAttribute(const QString &password)
{
    if (m_passwordAttributeForPG.valid) {
        emit passwordGenerated(m_passwordAttributeForPG.sectionId, m_passwordAttributeForPG.attributeId, password);
        m_passwordAttributeForPG.invalidate();
    }
}

void QuodEditor::appendPasswordToDelete(const int passwordId)
{
    m_deletedPasswords.append(passwordId);
    emit deletedPasswordsChanged();
}

void QuodEditor::removePasswords()
{
    AttributeHistoryList toDelete;
    for (const auto& a : qAsConst(m_passwordList)) {
        if (deletedPasswords().contains(a.id()))
            toDelete.append(a);
    }
    DatabaseManager::get()->removeAttributeHistory(toDelete);
}

void QuodEditor::setPasswords(const AttributeHistoryList &list)
{
    m_deletedPasswords.clear();
    emit deletedPasswordsChanged();
    m_passwordList = list;
    emit passwordListChanged();
}

QuodObject *QuodEditor::object() const
{
    return m_object.get();
}

void QuodEditor::setObject(SyncableObject *object)
{
    if (m_object == object) {
        return;
    }

    cleanup();

    if (object) {
        if (object->objectType() == SyncableObject::QuodType) {
            auto qtObject = qobject_cast<QuodTypeObject *>(object);
            QScopedPointer<QuodTypeObject> quodTypeObject(new QuodTypeObject(qtObject));
            quodTypeObject->detach();
            m_object.reset(new QuodObject(quodTypeObject.get()));
            emit objectChanged();
            checkSections();
        }
        else if (object->objectType() == SyncableObject::Quod) {
            m_object.reset(new QuodObject(reinterpret_cast<QuodObject *>(object)));
            emit objectChanged();
            checkSections();
        }
        else {
            qWarning() << "Cannot convert object type:" << object->objectType() << "to quod";
        }
    }
}

void QuodEditor::updateObjects()
{
    QuodObjectList list;
    for (const auto& object : qAsConst(m_objects)) {
        list << *object.get();
    }
    DatabaseManager::get()->update(list);
}

void QuodEditor::initTagsList()
{
    QHash<TagObject, int> commonPart;

    // Initialize the hash table
    const auto firstTags = m_objects.first()->tags();
    qDebug() << Q_FUNC_INFO << "Tags in first object" << firstTags.size();
    commonPart.reserve(commonPart.size());
    for (const auto& tag: firstTags) {
        commonPart.insert(tag, 1);
    }

    // Iterate through other objects
    for (auto it = m_objects.cbegin() + 1; it != m_objects.cend(); it++) {
        const auto tags = (*it)->tags();
        for (const auto& tag: tags) {
            auto iterator = commonPart.find(tag);
            if (iterator != commonPart.end()) {
                qDebug() << "OBJECT" << (*it)->name() << "has common tag" << iterator.value();
                iterator.value() = iterator.value() + 1;
            }
        }
    }

    // Get the tag list with frequency of m_objects.size()
    auto commonTags = commonPart.keys(m_objects.size());
    qDebug() << Q_FUNC_INFO << "Common tags" << commonTags.size();
    m_taglistModel->setTags(commonTags);
    m_tagsIntersection = QuodArca::asSet(commonTags);
}

void QuodEditor::initSyncStatus()
{
    auto syncProperty = m_objects.first()->syncProperty();
    auto allTheSame = std::all_of(m_objects.cbegin(), m_objects.cend(), [syncProperty](const QuodObjectPtr& ptr){
        return ptr->syncProperty() == syncProperty;
    });

    setSyncProperty(allTheSame ? syncProperty : defaultSyncProperty());
}

void QuodEditor::initPermissions()
{
    auto permissions = m_objects.first()->permissions();
    auto allTheSame = std::all_of(m_objects.cbegin(), m_objects.cend(), [permissions](const QuodObjectPtr& ptr){
        return ptr->permissions() == permissions;
    });

    setPermissions(allTheSame ? permissions : defaultPermission());
}

void QuodEditor::saveTags()
{
    auto tags = QuodArca::asSet(m_taglistModel->tags());
    // The set of removed tags
    auto removedTags = m_tagsIntersection.subtract(tags);
    for (auto& object : m_objects) {
        // Remove tags from object that has been removed during edition
        auto objectTags = QuodArca::asSet(object->tags()).subtract(removedTags);
        // Add tags that have been added during edition
        objectTags = objectTags.unite(tags);
        auto tagsList = objectTags.values();
        object->setTags(tagsList);
    }
}

void QuodEditor::saveSyncStatus()
{
    for (auto& object : m_objects) {
        object->setSyncProperty(syncProperty());
    }
}

void QuodEditor::savePermissions()
{
    for (auto& object : m_objects) {
        object->setPermissions(permissions());
    }
}

void QuodEditor::checkSections()
{
    qDebug() << Q_FUNC_INFO;
    // Detect if quod has photos section
    setHasPhotos(false);
    if (m_object) {
        const auto sections = m_object->sections();
        qDebug() << Q_FUNC_INFO << "Sections:" << sections.size();
        for (const auto& section : sections) {
            const auto attributes = section.attributes();
            qDebug() << Q_FUNC_INFO << "Attributes:" << attributes.size();
            for (const auto& attribute: attributes) {
                qDebug() << Q_FUNC_INFO << "Type" << attribute.type() << attribute.name();
                if (attribute.type() == AttributeObject::Type::Icon) {
                    setHasPhotos(true);
                    m_photoEditor->setSection(section);
                    return;
                }
            }
        }
    }
}

void QuodEditor::initLinkedArcaList()
{
    if (m_objects.isEmpty()) {
        // Editing single object
        const auto linkedArcaList = DatabaseManager::get()->linkedArca(m_object.get());
        qDebug() << Q_FUNC_INFO << "Linked arcas" << linkedArcaList.size()
                 << m_object->name();
        for (const auto& arca : linkedArcaList) {
            qDebug() << Q_FUNC_INFO << arca.name();
        }
        if (!linkedArcaList.isEmpty()) {
            QList<QUuid> idsList;
            idsList.reserve(linkedArcaList.size());
            for (const auto& arca : linkedArcaList) {
                idsList.append(arca.id());
            }
            setLinkedArcaIds(idsList);
        }
    } else {
        // Editing multiple objects
        QHash<QUuid, int> commonPart;
        const auto firstLinkedArca = DatabaseManager::get()->linkedArca(m_objects.first().get());
        commonPart.reserve(firstLinkedArca.size());
        for (const auto& obj : firstLinkedArca) {
            commonPart.insert(obj.id(), 1);
        }

        // Iterate through other objects
        for (auto it = m_objects.cbegin() + 1; it != m_objects.cend(); it++) {
            const auto linked = DatabaseManager::get()->linkedArca(it->get());
            for (const auto& obj: linked) {
                auto iterator = commonPart.find(obj.id());
                if (iterator != commonPart.end()) {
                    qDebug() << "OBJECT" << (*it)->name() << "has common linked Arca" << iterator.value();
                    iterator.value() = iterator.value() + 1;
                }
            }
        }

        // Get the tag list with frequency of m_objects.size()
        auto commonLink = commonPart.keys(m_objects.size());
        qDebug() << Q_FUNC_INFO << "Common linked" << commonLink.size();
        setLinkedArcaIds(commonLink);
    }
}

void QuodEditor::saveLinkedArcaList()
{
    // Get the ids of selected arcas to link to
    const auto arcas = ArcaListModel::get()->objectList(m_linkedArcaIds);
    QuodObjectList quods;
    if (m_object) {
        quods.append(m_object.get());
    } else {
        quods.reserve(m_objects.size());
        for (const auto& object : qAsConst(m_objects)) {
            quods << *object.get();
        }
    }

    qDebug() << Q_FUNC_INFO
             << "Selected arcas" << arcas.size()
             << "quods" << quods.size()
             << "Action" << m_linkArcaAction;

    switch (m_linkArcaAction) {
    case Editor::AddToArca:
        // Link to arca
        for (const auto& arca : arcas) {
            qDebug() << Q_FUNC_INFO << "Linking to arca" << arca.name() << quods.size() << "quods";
            if (!DatabaseManager::get()->addArcaQuods(arca, quods)) {
                qWarning() << Q_FUNC_INFO << "Error linking to" << arca.name();
            }
        }
        break;
    case Editor::MoveToArca:
        qDebug() << "NOT IMPLEMENTED";
        break;
    case Editor::RemoveFromArca:
        for (const auto& arca : arcas) {
            qDebug() << Q_FUNC_INFO << "Removing from arca" << arca.name() << quods.size() << "quods";
            if (!DatabaseManager::get()->addArcaQuods(arca, quods)) {
                qWarning() << Q_FUNC_INFO << "Error removing from" << arca.name();
            }
        }
        break;
    }
}

void QuodEditor::setAddressAttributeValue(QuodSectionObject sectionWithAddress, const QString& value)
{
    // Find the address attribute
    auto attributes = sectionWithAddress.attributes();
    auto addressAttributeIt = std::find_if(attributes.begin(),
                                          attributes.end(),
                                          [](const AttributeObject& attribute){
        return attribute.type() == AttributeObject::Address;
    });

    if (addressAttributeIt != attributes.end()) {
        auto addressAttribute = (*addressAttributeIt);
        qDebug() << Q_FUNC_INFO << "Address attribute found :)" << value;
        if (addressAttribute.isValid()) {
            addressAttribute.setPlainValue(value.toUtf8());
        } else {
            qWarning() << Q_FUNC_INFO << "Invalid attribute";
        }
    }
}

QString QuodEditor::getCombinedAddress(QuodSectionObject addressSection)
{
    auto attributes = addressSection.attributes();
    QStringList fields;
    for (auto& attribute : attributes) {
        if (!attribute.value().isEmpty()) {
            fields.append(attribute.value());
        }
    }

    return fields.join(", ");
}

SyncableObject::ObjectType QuodEditor::editorType()
{
    return SyncableObject::Quod;
}

void QuodEditor::refreshObject()
{
    qDebug() << Q_FUNC_INFO << "REFRESH OBJECT" << m_object;
    if (m_object) setObject(m_object->id());
}

void QuodEditor::setObject(const QUuid &id)
{
    m_object = QuodListModel::get()->quod(id);
    Q_EMIT this->objectChanged();
}

QUuid QuodEditor::editedSectionId() const
{
    return m_editedSectionId;
}

void QuodEditor::setEditedSectionId(QUuid newEditedSectionId)
{
    if (m_editedSectionId == newEditedSectionId)
        return;
    m_editedSectionId = newEditedSectionId;
    emit editedSectionIdChanged();
    setEditOneSection(!m_editedSectionId.isNull());
}

QuodTypeObjectList QuodEditor::subTypes() const
{
    return m_subTypes;
}

void QuodEditor::setSubtypes(const QuodTypeObjectList &newSubTypes)
{
    if (m_subTypes == newSubTypes)
        return;
    m_subTypes = newSubTypes;
    emit subTypesChanged();
}

bool QuodEditor::editOneSection() const
{
    return m_editOneSection;
}

void QuodEditor::setEditOneSection(bool newEditOneSection)
{
    if (m_editOneSection == newEditOneSection)
        return;
    m_editOneSection = newEditOneSection;
    emit editOneSectionChanged();
}

const QString &QuodEditor::editedSectionName() const
{
    return m_editedSectionName;
}

void QuodEditor::setEditedSectionName(const QString &newEditedSectionName)
{
    if (m_editedSectionName == newEditedSectionName)
        return;
    m_editedSectionName = newEditedSectionName;
    emit editedSectionNameChanged();
}

PhotoEditor *QuodEditor::photoEditor()
{
    return m_photoEditor;
}

bool QuodEditor::hasPhotos() const
{
    return m_hasPhotos;
}

void QuodEditor::setHasPhotos(bool newHasPhotos)
{
    if (m_hasPhotos == newHasPhotos)
        return;
    m_hasPhotos = newHasPhotos;
    emit hasPhotosChanged();
}

const QVariantList &QuodEditor::deletedPasswords() const
{
    return m_deletedPasswords;
}

void QuodEditor::setDeletedPasswords(const QVariantList &newDeletedPasswords)
{
    if (m_deletedPasswords == newDeletedPasswords)
        return;
    m_deletedPasswords = newDeletedPasswords;
    emit deletedPasswordsChanged();
}

const AttributeHistoryList &QuodEditor::passwordList() const
{
    return m_passwordList;
}
