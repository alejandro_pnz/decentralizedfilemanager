#include "fileviewermanager.h"
#include "fileviewer.h"
#include "imageviewer.h"
#include "documentviewer.h"
#include "videoviewer.h"
#include "utility.h"
#include "markdownviewer.h"

FileViewerManager::FileViewerManager(QObject *parent)
    : QAbstractListModel(parent)
{
}

FileViewerManager::~FileViewerManager()
{
    // Nothing
}

bool FileViewerManager::prepareFile(const FileObject &file)
{
    qDebug() << Q_FUNC_INFO << file.name();

    auto index = indexOfViewer(file.id());
    if (index > -1) {
        // File already exist
        emit setupViewer(index);
        return false;
    }

    auto viewer = createViewer(file);
    if (viewer) {
        beginInsertRows(QModelIndex(), rowCount(QModelIndex()), rowCount(QModelIndex()));
        viewer->setParent(this);
        m_viewers.append(viewer);
        endInsertRows();
        emit setupViewer(rowCount(QModelIndex()) - 1);
    }

//    auto format = file.fileType();
//    if (format == "pdf" || format == "docx" || format == "xlsx") {
//        // Create viewer for pdf, docx and xlsx files
//        beginInsertRows(QModelIndex(), rowCount(QModelIndex()), rowCount(QModelIndex()));
//        m_viewers.append(new DocumentViewer(file, this));
//        endInsertRows();
//        emit setupViewer(rowCount(QModelIndex()) - 1);
//    } else if (QuodArca::Utility::) {
//        beginInsertRows(QModelIndex(), rowCount(QModelIndex()), rowCount(QModelIndex()));
//        m_viewers.append(new VideoViewer(file, this));
//        endInsertRows();
//        emit setupViewer(rowCount(QModelIndex()) - 1);
//    }

    return true;
}

int FileViewerManager::indexOfViewer(const QUuid &fileId) const
{
    auto size = m_viewers.size();
    for (int i = 0; i < size; i++) {
        if (m_viewers.at(i)->id() == fileId) {
            return i;
        }
    }
    return -1;
}

void FileViewerManager::closeFile(const QUuid &id)
{
    auto index = indexOfViewer(id);

    if (index > -1) {
        beginRemoveRows(QModelIndex(), index, index);
        auto viewer = m_viewers.takeAt(index);
        viewer->deleteLater();
        endRemoveRows();
        if (m_viewers.isEmpty())
            emit lastFileClosed();
    } else {
        qWarning() << Q_FUNC_INFO << "Attempt to close invalid file";
    }
}

void FileViewerManager::closeAllFiles()
{
    beginResetModel();
    for (auto& v : m_viewers) {
        v->deleteLater();
    }
    m_viewers.clear();
    endResetModel();
}

FileViewer *FileViewerManager::getViewer(const QUuid &fileId)
{
    if (auto index = indexOfViewer(fileId); index > -1) {
        return m_viewers.at(index);
    } else {
    }
    return {};
}

FileViewer *FileViewerManager::createViewer(const FileObject &file)
{
    const auto format = file.fileType();

    qDebug() << "FileViewerManager::createViewer(" << format << ")";

    if (QuodArca::Utility::get()->supportedDocumentFormats().contains(format, Qt::CaseInsensitive)) {
        // Create viewer for pdf, docx and xlsx files
        return new DocumentViewer(file);
    }
    else if (QuodArca::Utility::get()->supportedAudioFormats().contains(format, Qt::CaseInsensitive)) {
        return new VideoViewer(file, false);
    }
    else if (QuodArca::Utility::get()->supportedVideoFormats().contains(format, Qt::CaseInsensitive)) {
        return new VideoViewer(file, true);
    }
    else if (QuodArca::Utility::get()->supportedImageFormats().contains(format, Qt::CaseInsensitive)) {
        return new ImageViewer(file);
    }
    else if (QuodArca::Utility::get()->supportedMarkdownFormats().contains(format, Qt::CaseInsensitive)){
        return new MarkdownViewer(file);
    }
    return {};
}

int FileViewerManager::rowCount(const QModelIndex &parent) const
{
    return m_viewers.size();
}

QVariant FileViewerManager::data(const QModelIndex &index, int role) const
{
    if (checkIndex(index)) {
        if (role == Qt::UserRole) {
            return m_viewers.at(index.row())->fileName();
        } else if (role == Qt::UserRole + 1){
            return QVariant::fromValue(m_viewers.at(index.row()));
        }
    }
    return {};
}

QHash<int, QByteArray> FileViewerManager::roleNames() const
{
    return m_roleNames;
}
