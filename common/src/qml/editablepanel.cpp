#include "editablepanel.h"
#include <QGuiApplication>

EditablePanel::EditablePanel() : m_canEdit{true}, m_editMode{false},
    m_hasSpacer{false}, m_hasSave{true}, m_hasEdit{true}, m_hasAdd{false}, m_enable{true}
{
}

void EditablePanel::edit()
{
    set_editMode(true);
}

void EditablePanel::finishEditing()
{
    set_editMode(false);
}

EditGroup::EditGroup(QObject *parent) : QObject(parent)
{
}

EditGroup::~EditGroup()
{
    clear();
}

EditGroupAttached *EditGroup::qmlAttachedProperties(QObject *object)
{
    return new EditGroupAttached(object);
}

void EditGroup::addItem(EditablePanel *item)
{
    if (!item || m_items.contains(item)) return;

    connect(item, &EditablePanel::edit, this, &EditGroup::handleEdition);
    connect(item, &EditablePanel::finishEditing, this, &EditGroup::handleEditFinished);
    m_items.append(item);
}

void EditGroup::removeItem(EditablePanel *item)
{
    if (!item || !m_items.contains(item)) return;

    disconnect(item, &EditablePanel::edit, this, &EditGroup::handleEdition);
    disconnect(item, &EditablePanel::finishEditing, this, &EditGroup::handleEditFinished);

    m_items.removeOne(item);
}

void EditGroup::handleEdition()
{
    auto *panel = qobject_cast<EditablePanel*>(sender());
    if (panel) {
        if (!panel->get_enable()) return;
        qDebug() << Q_FUNC_INFO << panel->get_title();
        panel->set_editMode(true);
        for (auto item : qAsConst(m_items)) {
            if (item == panel) continue;
            if (item) {
                qDebug() << item->isVisible() << item->get_title();
                item->set_canEdit(false);
            } else {
                qDebug() << "wrong item";
            }
        }
    }
}

void EditGroup::handleEditFinished()
{
    auto *panel = qobject_cast<EditablePanel*>(sender());
    if (panel) {
        if (!panel->get_enable()) return;
        qDebug() << Q_FUNC_INFO << panel->get_title();
        panel->set_editMode(false);
        for (auto item : qAsConst(m_items)) {
            item->set_canEdit(true);
        }
    }
}

void EditGroup::clear()
{
    for (auto item : qAsConst(m_items)) {
        disconnect(item, &EditablePanel::edit, this, &EditGroup::handleEdition);
        disconnect(item, &EditablePanel::finishEditing, this, &EditGroup::handleEditFinished);
    }
    m_items.clear();
}

EditGroupAttached::EditGroupAttached(QObject *parent) : QObject(parent)
{
}

EditGroup *EditGroupAttached::group() const
{
    return m_group;
}

void EditGroupAttached::setGroup(EditGroup *newGroup)
{
    if (m_group == newGroup)
        return;

    if (m_group)
        m_group->removeItem(qobject_cast<EditablePanel*>(parent()));

    m_group = newGroup;

    if (newGroup)
        newGroup->addItem(qobject_cast<EditablePanel*>(parent()));

    emit groupChanged();
}

void registerTypes() {
    qmlRegisterType<EditablePanel>("com.test", 1, 0, "EditablePanel");
    qmlRegisterType<EditGroup>("com.test", 1, 0, "EditGroup");
    qmlRegisterType<EditGroupAttached>("com.test", 1, 0, "EditGroupAttached");
}

Q_COREAPP_STARTUP_FUNCTION(registerTypes)
