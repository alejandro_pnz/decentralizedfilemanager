#ifndef EDITABLEPANEL_H
#define EDITABLEPANEL_H

#include <QtQml/qqml.h>
#include <QtQml/QQmlEngine>
#include <QQuickItem>
#include <qqmlhelpers.h>

class EditablePanel : public QQuickItem
{
    Q_OBJECT
    QML_WRITABLE_PROPERTY(QString, title)
    QML_WRITABLE_PROPERTY(bool, canEdit)
    QML_WRITABLE_PROPERTY(bool, editMode)
    QML_WRITABLE_PROPERTY(bool, hasSpacer)
    QML_WRITABLE_PROPERTY(bool, hasSave)
    QML_WRITABLE_PROPERTY(bool, hasEdit)
    QML_WRITABLE_PROPERTY(bool, hasAdd)
    QML_WRITABLE_PROPERTY(bool, enable)
public:
    EditablePanel();
    Q_INVOKABLE void edit();
    Q_INVOKABLE void finishEditing();
signals:
    void editButtonClicked();
    void saveButtonClicked();
    void cancelButtonClicked();
    void addButtonClicked();
};
QML_DECLARE_TYPE(EditablePanel)

class EditGroupAttached;
class EditGroup : public QObject
{
    Q_OBJECT
public:
    explicit EditGroup(QObject *parent = nullptr);
    ~EditGroup();
    static EditGroupAttached *qmlAttachedProperties(QObject *object);

    void addItem(EditablePanel *item);
    void removeItem(EditablePanel *item);
private slots:
    void handleEdition();
    void handleEditFinished();
signals:

private:
    void clear();
    QVector<EditablePanel*> m_items;
};

class EditGroupAttached : public QObject
{
    Q_OBJECT
    Q_PROPERTY(EditGroup* group READ group WRITE setGroup NOTIFY groupChanged)
public:
    explicit EditGroupAttached(QObject *parent = nullptr);
    EditGroup *group() const;
    void setGroup(EditGroup *newGroup);

    bool edited() const;
    void setEdited(bool newEdited);

signals:
    void groupChanged();
    void editedChanged();

private:
    EditGroup *m_group{nullptr};
    Q_DISABLE_COPY(EditGroupAttached)
};
QML_DECLARE_TYPE(EditGroup)
QML_DECLARE_TYPEINFO(EditGroup, QML_HAS_ATTACHED_PROPERTIES)

#endif // EDITABLEPANEL_H
