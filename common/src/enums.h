#ifndef ENUMS_H
#define ENUMS_H
#include <QObject>

namespace QuodArca {

class Enums : public QObject {
    Q_OBJECT
public:
    enum DeviceType {
        Phone,
        Tablet,
        Laptop
    };
    Q_ENUM(DeviceType);

    enum SyncPageMode {
        SyncStatus,
        PublicSync
    };
    Q_ENUM(SyncPageMode);

    enum ObjectTypeFilter {
        Arca = 1,
        Quod = 2,
        File = 4,
        Device = 5,
        Request = 6,
        User = 7,
        Contacts = 8
    };
    Q_ENUM(ObjectTypeFilter);

    enum ObjectAction {
           EditAction = 0,
           RenameAction,
           ShareAction,
           UnshareAction,
           ExportAction,
           DuplicateAction,
           DeleteAction,
           PinAction,
           UnPinAction,
           AddToArcaAction,
           MoveToArcaAction,
           RemoveFromArcaAction,
           LockAction,
           UnLockAction,
           SyncNowAction,
           SendAction,
           SetSyncPropertyAction,
           ViewAndEditAction,
           OpenWithAction,
           SetPermissionsAction,
           PropertiesAction,
           KeepRemoteAction,
           ShowInfoAction
       };
    Q_ENUM(ObjectAction);

    enum SettingsType {
        GlobalSettings,
        UserSettings,
        DeviceSettings
    };
    Q_ENUM(SettingsType);

    enum SettingsFragments {
        GlobalFragment,
        GlobalLogin,
        GlobalDevices,
        GlobalData,
        UserFragment,
        UserLogin,
        UserData,
        DeviceFragment,
        DeviceLogin,
        DeviceData,
        DeviceMiscellaneous
    };
    Q_ENUM(SettingsFragments);

    enum PasswordType {
        LettersAndNumbers = 0,
        NumbersOnly,
        Passphrase,
        Random
    };
    Q_ENUM(PasswordType)

    enum SortMenuSorters {
        LastUsed,
        MostUsed,
        Locked,
        Sync,
        NameAsc,
        NameDesc,
        Type,
        Size,
        Created,
        Modified
    };
    Q_ENUM(SortMenuSorters)
};
}
#endif // ENUMS_H
