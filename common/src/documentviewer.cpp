/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "documentviewer.h"

#include "DatabaseManager.h"

#include "pdfdocument.h"

DocumentViewer::DocumentViewer(const FileObject &file, QObject *parent)
    : FileViewer(file, parent)
{    
    m_device = DatabaseManager::get()->fileData(file);

    m_document = new PdfDocument(this);

    m_document->load(m_device.get(), file.fileType().toLower());

    calculateMaxWidth();
    setCurrentPage(0);
    set_canDrag(false);
    set_scaleFactor(1.0);
    set_contentX(0);
    set_contentY(0);
    m_scales = {0.1, 0.25, 0.33, 0.5, 0.66, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 3.0, 4.0, 6.0, 8.0, 12.0, 32.0, 64.0};
    Q_EMIT this->pageCountChanged();
}

DocumentViewer::~DocumentViewer()
{

}

int DocumentViewer::pageCount() const
{
    return m_document->pageCount();
}

qreal DocumentViewer::pageWidth(int page)
{
    return m_document->pageSize(page + 1).width();
}

qreal DocumentViewer::pageHeight(int page)
{
    return m_document->pageSize(page + 1).height();
}

QImage DocumentViewer::render(int page)
{
    qDebug() << Q_FUNC_INFO << page;
    return m_document->render(page + 1, {});
}

FileViewer::Type DocumentViewer::type() const
{
    return DocumentType;
}

void DocumentViewer::calculateMaxWidth()
{
    auto pageCount = m_document->pageCount();
    qreal maxWidth = 0.0;
    for (int i = 0; i < pageCount; ++i) {
        maxWidth = qMax(maxWidth, pageWidth(i));
    }
    qDebug() << Q_FUNC_INFO << maxWidth;
    m_maxWidth = maxWidth;
    emit maxWidthChanged();
}

qreal DocumentViewer::maxWidth() const
{
    return m_maxWidth;
}

int DocumentViewer::currentPage() const
{
    return m_currentPage;
}

void DocumentViewer::setCurrentPage(int newCurrentPage)
{
    if (m_currentPage == newCurrentPage)
        return;
    m_currentPage = newCurrentPage;
    emit currentPageChanged();
}

void DocumentViewer::zoomIn()
{
    auto it = std::upper_bound(m_scales.begin(), m_scales.end(), m_scaleFactor);
    if (it != m_scales.end()) {
        if (m_fitToWidthScale > *it) {
            // Fit to width scale it greater than next step from regular scale
            set_scaleFactor(*it);
        } else {
            if (m_fitToWidthScale > m_scaleFactor) {
                // fit to width scale is between the next step on regular scale
                // and current scale
                set_scaleFactor(m_fitToWidthScale);
            } else {
                set_scaleFactor(*it);
            }
        }
    } else {
        // No bigger scale available
        return;
    }
}

void DocumentViewer::zoomOut()
{
    auto it = std::lower_bound(m_scales.begin(), m_scales.end(), m_scaleFactor);
    if (it != m_scales.end()) {
        if (it != m_scales.begin()) {
            it--;
            if (m_fitToWidthScale < *it) {
                // Fit to width scale it smaller than previous step from regular scale
                set_scaleFactor(*it);
            } else {
                if (m_fitToWidthScale < m_scaleFactor) {
                    set_scaleFactor(m_fitToWidthScale);
                } else {
                    set_scaleFactor(*it);
                }
            }
        } else {
            set_scaleFactor(*it);
        }
    } else {
        // No smaller scale available
        return;
    }
}
