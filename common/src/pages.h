#ifndef PAGES_H
#define PAGES_H

#include <QObject>
#include <QMetaObject>
#include <QMetaEnum>

class Pages
{
    Q_GADGET

public:
    enum Page {
        LoginPage,
        OnboardingPage,
        OnboardingWebViewPage,
        ChoosePricePlanWebViewPage,
        CheckEmailPage,
        GenericInfoPage,
        InviteUsersPage,
        DeviceStatusPage,
        ConfirmPowerPassphrasePage,
        ResetPassphrasePage,
        CreateAccountPage,
        AccountOwnerPage,
        AccountOwnerEmailPage,
        AccountSetupPage,
        TermsPage,
        GenericInputPage,
        PaymentPage,
        AddDevicePage,
        AddDeviceAdvancedPage,
        HomePage,
        HomeOnboardingPage,
        LibraryPage,
        CreateArcaPage,
        AddTagsPage,
        SyncPage,
        ArcaUsersListPage,
        NotePage,
        OneArcaPage,
        LinkObjectsPage,
        EditGeneralDetailsPage,
        ChangeOwnerPage,
        SuccessfullyCompletedPopupPage,
        ShowInfoPage,
        SharePage,
        SearchPage,
        SelectQuodTypePage,
        SelectQuodSubTypePage,
        PassphraseStrengthPage,
        EditQuodDetailsPage,
        LinkToArcaPage,
        ArcaPromptPage,
        OneQuodPage,
        RecycleBinPage,
        UserDashboardPage,
        DeviceDashboardPage,
        AddNewDevicePage,
        DataDashboardPage,
        PasswordGeneratorPage,
        HelpPage,
        SupportPage,
        AboutPage,
        EditTagPage,
        OneFilePage,
        MorePage,
        PendingRequestsPage,
        OneDevicePage,
        QuodAttributesPage,
        CustomAttributePage,
        LoginPassphrasePage,
        PowerPassphrasePage,
        CreateFilePage,
        AccountInformationPage,
        SettingsPage,
        GenerateNewPasswordPage,
        AllowScanPopupPage,
        DeleteAfterUploadPopupPage,
        QuodCheckedOutPopupPage,
        ChangeSyncStatusPopupPage,
        PowerDevicesPage,
        EditActivityStatusPage,
        OverwriteObjectPage,
        ImportArchivePage,
        TagPage,
        DeviceDataPage,
        UnlockObjectsPopupPage,
        EditPermissionsPage,
        AddLinkPage,
        FileViewerPage,
        EditAuthenticatorPage,
        LockedQuodInfoPage,
        AuthenticatorInfoPopupPage,
        DateFilterPage,
        TagsFiltersPage,
        LockStatusPage,
        SyncStatusPage,
        MoreFiltersPopupPage,
        FiltersTypePage,
        UnlockUnsuccessfulPopupPage,
        UnlockAuthorizedPopupPage,
        VerifyingPopupPage,
        CheckNotificationsPopupPage,
        CheckEmailPopupPage,
        ShowDeviceInfoPage,
        GlobalDevicesSettingsPage,
        LoginAndInactivityPage,
        InviteUserDetailsPage,
        InviteToAccountPage,
        InviteUserPage,
        EditNicknamePage,
        SendUserInfoPage,
        OneUserPage,
        SyncStatusInfoPage,
        ShowUserInfoPage,
        PendingInvitationsPage,
        LinkCloudPage,
        GlobalDataPage,
        ResetSettingsPage,
        UserSettingsPage,
        DataSettingsPage,
        DeviceSettingsPage,
        NotificationsOffPopupPage,
        AppPermissionsPage,
        ResetConfirmationPopupPage,
        GdprInformationPage,
        GlobalAccountSettingsPage,
        HistoryAndStatusPage,
        MyAccountPage,
        SubscriptionPage,
        MyLoginPage,
        ProfileDetailsPage,
        LoginInformationPage,
        NameObjectPage,
        ExportQuodPage,
        ImportedObjectsPage,
        AddMenuPopupPage,
        BoardsMenuPopupPage,
        NotificationPopupPage,
        CreatePassphrasePopupPage,
        ImportSourcePopupPage,
        EditPhotosPage,
        CameraCapturePage,
        SelectObjectsPage,
        AboutPopupPage,
        PdfViewerPage,
        ConfirmTagDeletionPopupPage,
        ConfirmObjectDeletionPopupPage,
        PasswordHistoryPage,
        VideoViewerPage,
        AudioViewerPage,
        GenericPopupPage,
        ImageViewerPage,
        MarkdownViewerPage,
        LogsPage,
        WebViewLinkPage,
        SetPassphrasePage
    }; // enum Page
    Q_ENUM(Page)

    enum GenericInfoType {
        Single,
        Double,
        Progress,
        None
    };
    Q_ENUM(GenericInfoType);

    Q_INVOKABLE static QString toString(Pages::Page page)
    {
        const QMetaObject smo = Pages::staticMetaObject;
        const int indexOfPage = smo.indexOfEnumerator("Page");
        const QMetaEnum enumerator = smo.enumerator(indexOfPage);
        return enumerator.valueToKey(page);
    }

}; // class Pages

Q_DECLARE_METATYPE(Pages::Page)

#endif // PAGES_H
