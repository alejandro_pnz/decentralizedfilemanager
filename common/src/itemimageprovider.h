/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef ITEMIMAGEPROVIDER_H
#define ITEMIMAGEPROVIDER_H

#include <QQuickAsyncImageProvider>

#include <QDebug>
#include <QImage>
#include <QThreadPool>
#include <QtQml>

#include <QSvgRenderer>
#include <QPainter>

#include <QNetworkAccessManager>

#include <DatabaseManager.h>
#include <Utils.h>

#include <QtPromise>

#include "safenetworkcache.h"

#include "utility.h"

class AsyncItemImageResponseRunnable : public QObject, public QRunnable
{
    Q_OBJECT

public:
    AsyncItemImageResponseRunnable(const QString &id, const QSize &requestedSize)
        : m_id(id), m_requestedSize(requestedSize)
    {}

    void run() override
    {
        qDebug() << m_id;

        QImage image;

        QUrlQuery query(m_id);

        bool ok{false};
        SyncableObject::ObjectType objType = static_cast<SyncableObject::ObjectType>(query.queryItemValue("type").toInt(&ok));

        if (objType == SyncableObject::Arca) {
            const QUuid objId(query.queryItemValue("id"));

            const ArcaObject arca = DatabaseManager::get()->arca(objId);

            QString iconPath = QuodArca::Utility::get()->arcaIcon(arca.color());

            iconPath.remove(0, 3);

            image = QImage(m_requestedSize, QImage::Format_ARGB32);
            image.fill(Qt::transparent);

            m_renderer.load(iconPath);

            QPainter painter(&image);
            m_renderer.render(&painter);
        }
        else if (objType == SyncableObject::File) {
            const QUuid objId(query.queryItemValue("id"));

            const FileObject file = DatabaseManager::get()->file(objId);

            QString iconPath = QuodArca::Utility::get()->fileIcon(file.fileType());

            iconPath.remove(0, 3);

            image = QImage(m_requestedSize, QImage::Format_ARGB32);
            image.fill(Qt::transparent);

            m_renderer.load(iconPath);

            QPainter painter(&image);
            m_renderer.render(&painter);
        }
        else if (objType == SyncableObject::Quod) {
            const QUuid objId(query.queryItemValue("id"));

            QuodObject quod = DatabaseManager::get()->quod(objId);

            // TODO: get all non-empty non-sensitive urls for quod from sdk
            QString url;
            for (const auto &section: quod.sections()) {
                for (const auto &attribute: section.attributes()) {
                    if (attribute.type() == AttributeObject::Url) {
                        url = attribute.value();
                        break;
                    }
                }
            }

            if (!url.isEmpty()) {
                QNetworkAccessManager networkManager;

                const auto iconCacheDir = Utils::get()->iconCachePath();
                if (iconCacheDir.isEmpty()) {
                    qWarning() << "Icon cache path is empty";
                }
                else {
                    SafeNetworkCache *cache = new SafeNetworkCache(iconCacheDir);
                    cache->setMaximumCacheSize(200 * 1024 * 1024);

                    networkManager.setCache(cache);
                }

                QUrl u = QUrl::fromUserInput(url);

                if (u.isValid()) {
                    QStringList idn = QUrl::idnWhitelist();
                    auto host = u.host();
                    if (host.startsWith("www.")) {
                        host.remove(0, 4);
                    }
                    const auto queryUrl = QString("https://icons.test.com/icon?size=80..120..200&url=%1").arg(host);
                    QNetworkRequest req(queryUrl);
                    req.setHeader(QNetworkRequest::UserAgentHeader, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246");
                    req.setAttribute(QNetworkRequest::RedirectPolicyAttribute, QNetworkRequest::NoLessSafeRedirectPolicy);
                    req.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);
                    QNetworkReply *reply = networkManager.get(req);
                    connect(reply, &QNetworkReply::sslErrors, this, [=](const QList<QSslError> &errors) {
                        qDebug() << errors;
                    });

                    if (reply->isFinished()) {
                        image.loadFromData(reply->readAll());
                    }
                    else {
                        auto promise = QtPromise::connect(reply, &QNetworkReply::finished, &QNetworkReply::errorOccurred).timeout(3000);
                        promise.then([&image, reply]() {

                            image.loadFromData(reply->readAll());

                        })
                        .fail([](QNetworkReply::NetworkError error) {
                            qDebug() << "Failed to retrieve icon with" << error;
                        })
                        .fail([](QtPromise::QPromiseTimeoutException const &) {
                            qDebug() << "Failed to retrieve icon with timeout";
                        })
                        .finally([reply]() {
                            reply->deleteLater();
                        })
                        .wait();
                    }
                }

                // TODO:
                if (image.isNull()) {
                    auto filename = quod.isValid() ? quod.quodType().iconPath() : QStringLiteral("qrc:/custom.svg");
                    filename.remove(0, 3);

                    image = QImage(m_requestedSize, QImage::Format_ARGB32);
                    image.fill(Qt::transparent);

                    m_renderer.load(filename);

                    QPainter painter(&image);
                    m_renderer.render(&painter);
                }
            }
            else {
                auto filename = quod.isValid() ? quod.quodType().iconPath() : QStringLiteral("qrc:/custom.svg");
                filename.remove(0, 3);

                image = QImage(m_requestedSize, QImage::Format_ARGB32);
                image.fill(Qt::transparent);

                m_renderer.load(filename);

                QPainter painter(&image);
                m_renderer.render(&painter);
            }
        }

        Q_EMIT done(image);
    }

Q_SIGNALS:
    void done(QImage image);

private:
    QString m_id;
    QSize m_requestedSize;
    QSvgRenderer m_renderer;
};

class AsyncItemImageResponse : public QQuickImageResponse
{
public:
    AsyncItemImageResponse(const QString &id, const QSize &requestedSize, QThreadPool *pool)
    {
        auto runnable = new AsyncItemImageResponseRunnable(QByteArray::fromPercentEncoding(id.toUtf8()), requestedSize);
        connect(runnable, &AsyncItemImageResponseRunnable::done, this, &AsyncItemImageResponse::handleDone);
        pool->start(runnable);
    }

    void handleDone(QImage image) {
        m_image = image;
        Q_EMIT finished();
    }

    QQuickTextureFactory *textureFactory() const override
    {
        return QQuickTextureFactory::textureFactoryForImage(m_image);
    }

    QImage m_image;
};

class ItemImageProvider : public QQuickAsyncImageProvider
{
public:
    ItemImageProvider()
    {
        m_threadPool.setMaxThreadCount(3);
    }

    QQuickImageResponse *requestImageResponse(const QString &id, const QSize &requestedSize) override
    {
        return new AsyncItemImageResponse(id, requestedSize, &m_threadPool);
    }

private:
    QThreadPool m_threadPool;
};

#endif // ITEMIMAGEPROVIDER_H
