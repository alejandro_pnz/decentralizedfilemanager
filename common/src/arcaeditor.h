#ifndef ARCAEDITOR_H
#define ARCAEDITOR_H

#include "editor.h"

#include "ArcaObject.h"
#include "associatedtaglistmodel.h"

class ArcaEditor : public Editor
{
    Q_OBJECT
    Q_PROPERTY(QString color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(QVariantList linkedObjectsIdList READ linkedObjectsIdList NOTIFY linkedObjectsIdListChanged)
    Q_PROPERTY(QVariantList selectedObjectsIds READ selectedObjectsIds NOTIFY selectedObjectsIdsChanged)
    QML_CONSTANT_PROPERTY(QItemSelectionModel*, linkedObjectsModel);

public:
    explicit ArcaEditor(QObject *parent = nullptr);

    Q_INVOKABLE void create() override;
    Q_INVOKABLE bool edit(const QUuid &id, SyncableObject::ObjectType) override;
    Q_INVOKABLE void save() override;
    Q_INVOKABLE bool editList(const QModelIndexList& indexes) override;

    void cleanup() override;

    ArcaObject *object() const override;

    QString color() const;
    void setColor(const QString &color);    

    const QVariantList &linkedObjectsIdList() const;

    const QVariantList &selectedObjectsIds() const;

signals:
    void colorChanged();
    void linkedObjectsIdListChanged();

    void selectedObjectsIdsChanged();

protected:
    SyncableObject::ObjectType editorType() override;

private slots:
    void updateSelectedLinkedObjects();
private:
    void refreshObject();
    void setObject(const QUuid& id);
    void updateObjects();
    void initTagsList();
    void initSyncStatus();
    void initPermissions();
    void initLinkedObjectsList();
    void saveTags();
    void saveSyncStatus();
    void savePermissions();
    void saveLinkedObjectList();


    QString m_color;

    ArcaObjectPtr m_object;
    ArcaObjectPtrList m_objects;
    QList<QUuid> m_linkedQuods;
    QList<QUuid> m_linkedFiles;
    QVariantList m_linkedObjectsIdList;
    QVariantList m_selectedObjectsIds;
};

#endif // ARCAEDITOR_H
