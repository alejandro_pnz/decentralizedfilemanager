#include "homepagemanager.h"
#include "arcaeditor.h"
#include "quodeditor.h"
#include "fileeditor.h"
#include "homeeditor.h"
#include "deviceeditor.h"
#include "usereditor.h"
#include "taglistmodel.h"
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QDesktopServices>
#include <QtQml>

#include <QDebug>

#include "singleton.h"

#include <SettingsManager.h>

HomePageManager *HomePageManager::get()
{
    return TSingleton<HomePageManager>::instance();
}

HomePageManager::HomePageManager(QObject *parent)
    : AbstractPageManager(parent),
      m_fileEditor(new FileEditor(this)),
      m_arcaEditor(new ArcaEditor(this)),
      m_quodEditor(new QuodEditor(this)),
      m_homeEditor(new HomeEditor(this)),
      m_deviceEditor(new DeviceEditor(this)),
      m_userEditor(new UserEditor(this))
{
    // Home page is always on stack
    m_visiblePages.push(Pages::Page::HomePage);
}

void HomePageManager::openPage(Pages::Page page, bool closeCurrent)
{
    if (closeCurrent) back();

    switch (page) {
    case Pages::Page::HomePage:
        backTo(Pages::Page::HomePage);
        break;
    case Pages::Page::LibraryPage:
    case Pages::Page::RecycleBinPage:
    case Pages::Page::UserDashboardPage:
    case Pages::Page::DeviceDashboardPage:
    case Pages::Page::DataDashboardPage:
    case Pages::Page::PasswordGeneratorPage:
        backToHomeAndOpenPage(page);
        break;
    case Pages::Page::SelectQuodTypePage:
        enterCreateQuod();
        break;
    // Not Implemented pages
    case Pages::Page::HelpPage:
        QDesktopServices::openUrl(QUrl("https://qubibox.com/help"));
        break;
    case Pages::Page::SupportPage:
        QDesktopServices::openUrl(QUrl("https://qubibox.com/support"));
        break;
    case Pages::Page::AboutPage:
        enterEmptyPage(page);
        break;
    default:
       enterPage(page);
       break;
    }
}

void HomePageManager::abortCurrentFlow()
{
    switch (m_currentFlow) {
    case HomePageManager::None:
        break;
    case HomePageManager::CreateQuod:
        abortQuodCreation();
        break;
    case HomePageManager::CreateArca:
        abortArcaCreation();
        break;
    default:
        break;
    }

    backToFirstMainPage();
    setCurrentFlow(Flows::None);
}

void HomePageManager::saveCurrentFlow()
{
    switch (m_currentFlow) {
    case HomePageManager::CreateQuod:
        saveNewQuod();
        break;
    case HomePageManager::CreateArca:
        saveNewArca();
        break;
    case HomePageManager::CreateFile:
        saveNewFile();
        break;
    default:
        saveEditedObject();
        break;
    }
}

void HomePageManager::enterCreateArca()
{
    backToFirstMainPage();
    setCurrentFlow(Flows::CreateArca);
    enterPage(Pages::Page::CreateArcaPage);
}

void HomePageManager::abortArcaCreation()
{
    // Nothing for now. Either add specific code here or remove
}

void HomePageManager::saveNewArca()
{
    m_arcaEditor->save();
    backToFirstMainPage();
    setCurrentFlow(Flows::None);
    enterLibraryPage(LibraryTabs::ArcaTab, QuodArca::Enums::Created);
}

void HomePageManager::enterCreateQuod()
{
    backToFirstMainPage();
    setCurrentFlow(Flows::CreateQuod);
    enterPage(Pages::Page::SelectQuodTypePage);
}

void HomePageManager::abortQuodCreation()
{
    // Nothing for now. Either add specific code here or remove
}

void HomePageManager::saveNewQuod()
{
    m_quodEditor->save();
    // TODO: this is workaround to avoid crash during link to arca, need to fix logic
    //setCurrentFlow(Flows::None);

    // Back to Home
    backTo(Pages::HomePage);
    // Open library with Quod Tab
    enterLibraryPage(QuodTab, QuodArca::Enums::Created);

    // TODO: temporary settings
    QSettings settings;
    bool dontShowAgain = settings.value("dontShowArcaPromt", false).toBool();
    if (dontShowAgain) return;

    openPage(Pages::ArcaPromptPage);
}

void HomePageManager::enterCreateFile()
{
    m_fileEditor->create();

    setCurrentFlow(CreateFile);
    backToFirstMainPage();
    openPage(Pages::NotePage);
    emit setupNotePage({}, true, false);
}

void HomePageManager::abortFileCreation()
{

}

void HomePageManager::saveNewFile()
{
    m_fileEditor->save();
    setCurrentFlow(Flows::None);
    backToFirstMainPage();
    enterLibraryPage(FileTab, QuodArca::Enums::Created);
}

void HomePageManager::saveEditedObject()
{
    currentEditor()->save();
    if (currentFlow() == None) {
        backTo(Pages::HomePage);
    } else {
        back();
        if (isPageOnStack(Pages::LibraryPage)) return;

        setCurrentFlow(None);
    }
}

void HomePageManager::openOneQuodPage(const QUuid &id)
{
    qDebug() << Q_FUNC_INFO << id;
    if (m_quodEditor->edit(id, SyncableObject::Quod)) {
        setCurrentFlow(ManageQuod);
        enterPage(Pages::OneQuodPage);
    }
}

void HomePageManager::openOneFilePage(const QUuid &id)
{
    if (m_fileEditor->edit(id, SyncableObject::File)) {
        setCurrentFlow(ManageFile);
        enterPage(Pages::OneFilePage);
    }
}

void HomePageManager::openOneDevicePage(const QUuid &id)
{
    if (m_deviceEditor->edit(id, SyncableObject::Device)) {
        // If enter nested device, close current one
        openPage(Pages::OneDevicePage, isPageOnStack(Pages::OneDevicePage));
    }
}

void HomePageManager::openAddNewDevicePage(bool newDeviceIsAllowed)
{
    if(newDeviceIsAllowed)
        openPage(Pages::AddNewDevicePage);
    else
        emit cannotAddMoreDevices();
}


void HomePageManager::openOneUserPage(const QUuid &id)
{
    if (m_userEditor->edit(id, SyncableObject::User)) {
        // If enter nested user, close current one
        openPage(Pages::OneUserPage, isPageOnStack(Pages::OneUserPage));
    }
}

void HomePageManager::openQuodFromTag(const QUuid &id)
{
    abortCurrentFlow();
    openOneQuodPage(id);
}

HomePageManager::Flows HomePageManager::setFlowForObjectType(SyncableObject::ObjectType type)
{
    qDebug() << Q_FUNC_INFO << type;
    auto prevFlow = currentFlow();
    switch (type) {
    case SyncableObject::Quod:
        setCurrentFlow(ManageQuod);
        break;
    case SyncableObject::File:
        setCurrentFlow(ManageFile);
        break;
    case SyncableObject::Arca:
        setCurrentFlow(ManageArca);
        break;
    default:
        setCurrentFlow(None);
    }
    return prevFlow;
}

void HomePageManager::enterEditListPage(const QModelIndexList &indexes, Pages::Page page, int type)
{
    auto prevFlow = setFlowForObjectType(static_cast<SyncableObject::ObjectType>(type));

    if (currentEditor()->editList(indexes)) {
        enterPage(page);
        emit clearLibrarySelection();
    } else {
        qWarning() << Q_FUNC_INFO << "Cannot edit object list";
        setCurrentFlow(prevFlow);
    }
}

void HomePageManager::enterEditPage(const QUuid &id, Pages::Page page, int type)
{
    //TODO change int type to SyncableObject::type when it will be registered in qml
    auto t = static_cast<SyncableObject::ObjectType>(type);
    auto prevFlow = setFlowForObjectType(t);

    if (currentEditor()->edit(id, t)) {
        qDebug() << Q_FUNC_INFO << currentFlow();
        enterPage(page);
        emit clearLibrarySelection();
    } else {
        qWarning() << Q_FUNC_INFO << "Cannot edit object with id" << id.toString();
        setCurrentFlow(prevFlow);
    }
}

void HomePageManager::enterLinkToArcaPage(const QUuid &id, int type, int action)
{
    auto prevFlow = setFlowForObjectType(static_cast<SyncableObject::ObjectType>(type));
    if (currentEditor()->edit(id)) {
        qDebug() << Q_FUNC_INFO << currentFlow();
        currentEditor()->setLinkArcaAction(static_cast<Editor::LinkArcaAction>(action));
        enterPage(Pages::LinkToArcaPage);
        emit clearLibrarySelection();
    } else {
        qWarning() << Q_FUNC_INFO << "Cannot edit object with id" << id.toString();
        setCurrentFlow(prevFlow);
    }
}

void HomePageManager::enterLinkListToArcaPage(const QModelIndexList &indexes, int type, int action)
{
    auto prevFlow = setFlowForObjectType(static_cast<SyncableObject::ObjectType>(type));
    if (currentEditor()->editList(indexes)) {
        currentEditor()->setLinkArcaAction(static_cast<Editor::LinkArcaAction>(action));
        enterPage(Pages::LinkToArcaPage);
        emit clearLibrarySelection();
    } else {
        qWarning() << Q_FUNC_INFO << "Cannot edit object list";
        setCurrentFlow(prevFlow);
    }
}

void HomePageManager::setCurrentLibraryTab(const int tab)
{
    switch(tab) {
    case AllTab:
        setCurrentFlow(None);
        break;
    case ArcaTab:
        setCurrentFlow(ManageArca);
        break;
    case QuodTab:
        setCurrentFlow(ManageQuod);
        break;
    case FileTab:
        setCurrentFlow(ManageFile);
        break;
    }
}

void HomePageManager::openOneArcaPage(const QUuid &id)
{
    if (m_arcaEditor->edit(id, SyncableObject::Arca)) {
        setCurrentFlow(ManageArca);
        enterPage(Pages::OneArcaPage);
    }
}

void HomePageManager::enterEditTagPage(const QJSValue &value)
{
    openPage(Pages::Page::EditTagPage);
    emit setupEditTagPage(value);
}

void HomePageManager::enterLibraryPage(const int tab, const int sorter)
{
    setCurrentLibraryTab(tab);
    openPage(Pages::Page::LibraryPage);
    emit setupLibraryPage(tab, sorter);
}

void HomePageManager::enterCustomAttributePage(const QString &name)
{
    openPage(Pages::Page::CustomAttributePage);
    emit setupCustomAttributePage(name);
}

void HomePageManager::enterQuodCheckedOutPopupPage(const bool edit)
{
    openPage(Pages::Page::QuodCheckedOutPopupPage);
    emit setupQuodCheckedOut(edit);
}

bool HomePageManager::creatingQuod() const
{
    return m_currentFlow == Flows::CreateQuod;
}

bool HomePageManager::creatingObject() const
{
    return m_currentFlow == Flows::CreateArca
            || m_currentFlow == Flows::CreateQuod
            || m_currentFlow == Flows::CreateFile;
}

void HomePageManager::setDontShowArca(const bool value)
{
    SettingsManager::get()->setValue(SettingsManager::Custom, "", "dontShowArcaPromt", value);
}

void HomePageManager::saveNote(const QString &note)
{
    qDebug() << "note" <<  note << m_currentFlow;
    switch(m_currentFlow) {
    case HomePageManager::None: return;
    case HomePageManager::CreateQuod:
    case HomePageManager::ManageQuod:
        m_quodEditor->setNotes(note);
        if (m_currentFlow == HomePageManager::ManageQuod) {
            m_quodEditor->save();
        }
        back();
        break;
    case HomePageManager::ManageArca:
    case HomePageManager::CreateArca:
        m_arcaEditor->setNotes(note);
        if (m_currentFlow == HomePageManager::ManageArca) {
            m_arcaEditor->save();
        }
        back();
        break;
    case HomePageManager::ManageFile:
    case HomePageManager::CreateFile:
        m_fileEditor->setNotes(note);
        if (m_currentFlow == HomePageManager::ManageFile) {
            m_fileEditor->save();
            back();
            break;
        }
        back();
        break;
    default:
        break;
    }

}

void HomePageManager::editNote()
{
    // TODO maybe Editor base class could have Note property, then we can get rid
    // of switch and use current editor (depending on flow)
    openPage(Pages::NotePage);
    switch(m_currentFlow) {
    case HomePageManager::None:
        break;
    case HomePageManager::ManageArca:
    case HomePageManager::CreateArca:
         emit setupNotePage(m_arcaEditor->notes());
        break;
    case HomePageManager::ManageQuod:
    case HomePageManager::CreateQuod:
         emit setupNotePage(m_quodEditor->notes());
        break;
    case HomePageManager::ManageFile:
    case HomePageManager::CreateFile:
         emit setupNotePage(m_fileEditor->notes());
        break;
    }
}

void HomePageManager::enterEditGeneralDetailsPage(const QJSValue &config)
{
    openPage(Pages::Page::EditGeneralDetailsPage);
    emit setupEditGeneralDetailsPage(config);
}

void HomePageManager::enterChangeSyncStatusPage(const QJSValue &config)
{
    openPage(Pages::Page::ChangeSyncStatusPopupPage);
    emit setupChangeSyncStatusPage(config);
}

void HomePageManager::enterLoginInactivitySettingsPage(const QJSValue &config)
{
    openPage(Pages::Page::LoginAndInactivityPage);
    emit setupLoginInactivitySettingsPage(config);
}

void HomePageManager::enterResetSettingsPage(const QJSValue &config)
{
    openPage(Pages::Page::ResetSettingsPage);
    emit setupResetSettingsPage(config);
}

void HomePageManager::enterDataSettingsPage(const QJSValue &config)
{
    openPage(Pages::Page::DataSettingsPage);
    emit setupDataSettingsPage(config);
}

void HomePageManager::enterShowInfoPage(const QUuid &id, const int type)
{
    if (type == SyncableObject::Device) {
        openPage(Pages::Page::ShowDeviceInfoPage);
    }
    else if (type == SyncableObject::User) {
        openPage(Pages::Page::ShowUserInfoPage);
    }
    else {
        openPage(Pages::Page::ShowInfoPage);
    }
    emit setupInfoPage(id, type);
}

void HomePageManager::enterNameObjectPage(const QJSValue &config)
{
    openPage(Pages::Page::NameObjectPage);
    emit setupNameObjectPage(config);
}

void HomePageManager::enterSettingsPage(const int id)
{
    openPage(Pages::Page::SettingsPage);
    emit setupSettingsPage(id);
}

void HomePageManager::enterFileDialog()
{
    backToFirstMainPage();
    emit setupFileDialog();
}

void HomePageManager::enterChangeOwnerPage(SyncableObject::ObjectType type)
{
    openPage(Pages::Page::ChangeOwnerPage);
    emit setupChangeOwnerPage(type);
}

void HomePageManager::enterChangeOwnerPage()
{
    SyncableObject::ObjectType type;
    switch (currentFlow()) {
    case HomePageManager::None:
        return;
    case HomePageManager::ManageArca:
    case HomePageManager::CreateArca:
        type = SyncableObject::Arca;
        break;
    case HomePageManager::CreateQuod:
    case HomePageManager::ManageQuod:
        type = SyncableObject::Quod;
        break;
    case HomePageManager::CreateFile:
    case HomePageManager::ManageFile:
        type = SyncableObject::File;
        break;

    }
    openPage(Pages::Page::ChangeOwnerPage);
    emit setupChangeOwnerPage(type);
}

void HomePageManager::enterSuccessfullyCompletedPopupPage(const QJSValue& config)
{
    openPage(Pages::Page::SuccessfullyCompletedPopupPage);
    emit setupSuccessfullyCompletedPopupPage(config);
}

void HomePageManager::enterCheckEmailPopupPage()
{
    openPage(Pages::Page::CheckEmailPopupPage, true);
}

void HomePageManager::enterSelectObjectPage(const QJSValue &config)
{
    openPage(Pages::Page::SelectObjectsPage);
    emit setupSelectObjectsPage(config);
}

void HomePageManager::enterGenericPopupPage(const QJSValue &config)
{
    openPage(Pages::Page::GenericPopupPage);
    emit setupGenericPopupPage(config);
}

void HomePageManager::enterPasswordGeneratorPage(const QUuid &sectionId, const QUuid &attributeId, int sectionIndex)
{
    m_quodEditor->setPasswordAttributeForPasswordGenerator(sectionId, attributeId, sectionIndex);
    openPage(Pages::GenerateNewPasswordPage);
}

void HomePageManager::enterPasswordHistoryPage(const AttributeHistoryList &list)
{
    m_quodEditor->setPasswords(list);
    openPage(Pages::PasswordHistoryPage);
}

void HomePageManager::enterConfirmObjectDeletionPopupPage()
{
    openPage(Pages::ConfirmObjectDeletionPopupPage);
}

void HomePageManager::enterConfirmTagDeletionPopupPage(const QUuid &id)
{
    TagListModel::get()->requestTagDeletion(id);
    openPage(Pages::ConfirmTagDeletionPopupPage);
}

void HomePageManager::closeEditTag()
{
    emit closeEditTagRequested();
    back();
}

void HomePageManager::enterFileViewerWindow()
{
    static bool loaded = false;
    if (!loaded) {
        qDebug() << Q_FUNC_INFO << "Load window";
        m_engine->load(QStringLiteral("qrc:/UIPage/FileViewerWindow.qml")); // Load file viewer window
        loaded = true;
    }
    emit openFileViewerWindow();
}

void HomePageManager::enterMoreFiltersPage(const QJSValue &config)
{
    openPage(Pages::MoreFiltersPopupPage);
    emit setupMoreFiltersPage(config);
}

void HomePageManager::enterFilterPage(Pages::Page page, const QJSValue &config)
{
    openPage(page);
    emit setupFilterPage(config);
}

void HomePageManager::enterEditMarkdownFile(const QUuid &fileId)
{
    if (m_fileEditor->edit(fileId, SyncableObject::File)) {
        setCurrentFlow(ManageFile);
        enterPage(Pages::NotePage);
        qDebug() << Q_FUNC_INFO << "setContent" <<m_fileEditor->fileViewer()->content();
        emit setupNotePage(m_fileEditor->fileViewer()->content(), true, false);
    }
}

void HomePageManager::setCurrentFlow(HomePageManager::Flows flow)
{
    qDebug() << "Set current flow to" << flow;
    if (m_currentFlow == flow) return;

    // Actions before changing flow
    switch(m_currentFlow) {
    case CreateArca:
        m_arcaEditor->cleanup();
        break;
    case CreateQuod:
        m_quodEditor->cleanup();
    default: break;
    }

    m_currentFlow = flow;
    emit currentFlowChanged();
}

void HomePageManager::backToFirstMainPage()
{
    if (m_mainPages.contains(topPage())) return;
    back();
    backToFirstMainPage();
}

void HomePageManager::backToHomeAndOpenPage(Pages::Page page)
{
    if (isPageOnStack(page)) {
        backTo(page);
    } else {
        backTo(Pages::Page::HomePage);
        enterPage(page);
    }
}

void HomePageManager::setBoardsSectionExpanded(const bool expanded)
{
    m_settings.setValue("boardsSectionExpanded", expanded);
    emit boardsSectionExpandedChanged();
}

void HomePageManager::setToolsSectionExpanded(const bool expanded)
{
    m_settings.setValue("toolsSectionExpanded", expanded);
    emit toolsSectionExpandedChanged();
}

void HomePageManager::setSupportSectionExpanded(const bool expanded)
{
    m_settings.setValue("supportSectionExpanded", expanded);
    emit supportSectionExpandedChanged();
}

bool HomePageManager::getBoardsSectionExpanded() const
{
    return m_settings.value("boardsSectionExpanded", true).toBool();
}

bool HomePageManager::getToolsSectionExpanded() const
{
    return m_settings.value("toolsSectionExpanded", true).toBool();
}

bool HomePageManager::getSupportSectionExpanded() const
{
    return m_settings.value("supportSectionExpanded", true).toBool();
}

HomePageManager::Flows HomePageManager::currentFlow() const
{
    return m_currentFlow;
}

Editor *HomePageManager::currentEditor() const
{
    switch(m_currentFlow) {
    case HomePageManager::None:
        return m_homeEditor;
    case HomePageManager::ManageQuod:
    case HomePageManager::CreateQuod:
        return m_quodEditor;
    case HomePageManager::ManageArca:
    case HomePageManager::CreateArca:
        return m_arcaEditor;
    case HomePageManager::ManageFile:
    case HomePageManager::CreateFile:
        return m_fileEditor;
    }

    qWarning() << "HomePageManager::currentEditor()" << "Unhandled flow" << m_currentFlow;
    return nullptr;
}

Editor *HomePageManager::editorForType(SyncableObject::ObjectType type)
{
    switch (type) {
    case SyncableObject::Arca:
        return m_arcaEditor;
    case SyncableObject::File:
        return m_fileEditor;
    case SyncableObject::Quod:
        return m_quodEditor;
    default:
        return nullptr;
    }
}

void HomePageManager::init(const QString &contextProperty, QQmlApplicationEngine *engine, bool isTablet)
{
    AbstractPageManager::init(contextProperty, engine, isTablet);
    m_engine = engine;
    engine->rootContext()->setContextProperty("arcaEditor", m_arcaEditor);
    engine->rootContext()->setContextProperty("quodEditor", m_quodEditor);
    engine->rootContext()->setContextProperty("fileEditor", m_fileEditor);
    engine->rootContext()->setContextProperty("deviceEditor", m_deviceEditor);
    engine->rootContext()->setContextProperty("userEditor", m_userEditor);
}

void HomePageManager::pageAboutToBeRemoved(Pages::Page page)
{
    switch (page) {
      case Pages::LibraryPage:
        // Library tabs manages the flows, so when Library page is popped out the flow should be cleaned
        setCurrentFlow(None);
        break;
    default:
        break;
    }
}
