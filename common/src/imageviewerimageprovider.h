#ifndef IMAGEVIEWERIMAGEPROVIDER_H
#define IMAGEVIEWERIMAGEPROVIDER_H

#include <QQuickAsyncImageProvider>
#include <qqmlextensionplugin.h>

#include <qqmlengine.h>
#include <qquickimageprovider.h>
#include <QDebug>
#include <QImage>
#include <QThreadPool>
#include <DatabaseManager.h>
#include <QSvgRenderer>
#include <QPainter>

class AsyncImageViewerResponseRunnable : public QObject, public QRunnable
{
    Q_OBJECT

signals:
    void done(QImage image);

public:
    AsyncImageViewerResponseRunnable(const QString &id, const QSize &requestedSize)
        : m_id(id), m_requestedSize(requestedSize) {}

    void run() override
    {
        auto file = DatabaseManager::get()->file(QUuid(m_id));
        if (file.isValid()) {
            auto device = DatabaseManager::get()->fileData(file);
            QImage image;// = file.preview();
            if (file.fileType() == "svg") {
                if (!m_requestedSize.isValid()) {
                    m_requestedSize = {400,400}; // TODO use vieport size when this info will be available
                }
                image = QImage(m_requestedSize, QImage::Format_ARGB32);
                image.fill(Qt::transparent);

                auto success = m_renderer.load(device->readAll());

                QPainter painter(&image);
                m_renderer.render(&painter);
                qDebug() << Q_FUNC_INFO << "Render svg" << success << m_requestedSize;
            } else if (image.load(device.get(), file.fileType().toStdString().c_str())) {
                qDebug() << Q_FUNC_INFO << "Image loaded" << image.size();
            } else {
                qWarning() << Q_FUNC_INFO << "Cannot load image" << file.fileType();
                emit done({});
                return;
            }
            if (m_requestedSize.isValid())
                image = image.scaled(m_requestedSize);

            emit done(image);
        }
        else {
            emit done({});
        }
    }

private:
    QString m_id;
    QSize m_requestedSize;
    QSvgRenderer m_renderer;
};

class AsyncImageViewerResponse : public QQuickImageResponse
{
public:
    AsyncImageViewerResponse(const QString &id, const QSize &requestedSize, QThreadPool *pool)
    {
        auto runnable = new AsyncImageViewerResponseRunnable(QByteArray::fromPercentEncoding(id.toUtf8()), requestedSize);
        connect(runnable, &AsyncImageViewerResponseRunnable::done, this, &AsyncImageViewerResponse::handleDone);
        pool->start(runnable);
    }

    void handleDone(QImage image) {
        m_image = image;
        emit finished();
    }

    QQuickTextureFactory *textureFactory() const override
    {
        return QQuickTextureFactory::textureFactoryForImage(m_image);
    }

    QImage m_image;
};

class ImageViewerImageProvider : public QQuickAsyncImageProvider
{
public:
    QQuickImageResponse *requestImageResponse(const QString &id, const QSize &requestedSize) override
    {
        AsyncImageViewerResponse *response = new AsyncImageViewerResponse(id, requestedSize, &pool);
        return response;
    }

private:
    QThreadPool pool;
};

#endif // IMAGEVIEWERIMAGEPROVIDER_H
