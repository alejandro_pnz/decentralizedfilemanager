#include "photoeditor.h"
#include <filelistmodel.h>
#include <DatabaseManager.h>
#include <QtQml>
#include <QImage>
#include <QPainter>
#include <QGuiApplication>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QDirIterator>

PhotoEditor::PhotoEditor(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<PhotoEditor*>("PhotoEditor*");
}

void PhotoEditor::setSection(const QuodSectionObject &section)
{
    m_section = section;

    m_photosIds.clear();
    m_photoCounter = 0;

    const auto attributes = m_section.attributes();
    Q_ASSERT(attributes.size() == 1);
    m_attribute = m_section.attributes().at(0);
    qDebug() << Q_FUNC_INFO << m_attribute.value() << QString(m_attribute.value()).split(",") << m_section.name();
    const QStringList ids = QString(m_attribute.value()).split(",");
    for (const auto &id: ids) {
        // Initialize photos ids
        qDebug() << Q_FUNC_INFO << id;
        m_photosIds.append(QUuid(id));
    }
    qDebug() << Q_FUNC_INFO << m_photosIds.size();
    emit photosIdsChanged(m_photosIds);
}

void PhotoEditor::setName(const QUuid &uuid, const QString &name)
{
    qDebug() << Q_FUNC_INFO << uuid << name;
    if (name.isEmpty()) {
        qDebug() << Q_FUNC_INFO << "empty name";
        return;
    }
    auto file = FileListModel::get()->file(uuid);

    if (file && file->isValid()) {
        file->setName(name);
    } else {
        qWarning() << Q_FUNC_INFO << "Invalid file" << uuid;
    }
}

void PhotoEditor::save()
{
    qDebug() << Q_FUNC_INFO << m_photosIds.size();
    QList<QByteArray> stringids;
    for (const auto& id : qAsConst(m_photosIds)) {
        auto file = FileListModel::get()->file(id.toUuid());
        if (file && file->isValid()) {

            stringids.append(file->id().toByteArray());
            file->save();
        }
    }
    qDebug() << Q_FUNC_INFO << stringids;
    m_attribute.setPlainValue(stringids.join(","));
}

void PhotoEditor::remove(const QUuid &uuid)
{
    auto file = FileListModel::get()->file(uuid);
    if (file) {
        DatabaseManager::get()->remove(file.get());
    } else {
        qWarning() << Q_FUNC_INFO << "attempt to remove invalid file" << uuid;
    }
}

void PhotoEditor::retake(const QUuid &uuid)
{
    m_editedId = uuid;
}

QImage PhotoEditor::processImage(const QString &path, const QRectF& rectF, const int rotation)
{
    QImage image(path);

    if (image.isNull()) {
        qDebug() << Q_FUNC_INFO << "NULL IMAGE";
    } else {
        QImage cropped = cropImage(image, rectF, rotation);
        QFileInfo info(path);
        saveCroppedImage(cropped, info.absoluteDir().absolutePath());
        if (!m_editedId.isNull()) {
            remove(m_editedId);
            m_editedId = QUuid();
        }
        removeCapturedImage(path);
        return cropped;
    }
    return QImage();
}

bool PhotoEditor::importFiles(const QList<QUrl> &files)
{
    for (const auto &url: files) {
        importFile(url.toString());
    }
    return true;
}

QString PhotoEditor::getDefaultName()
{
    m_photoCounter++;
    return QString("photo-%1").arg(m_photoCounter);
}

bool PhotoEditor::saveCroppedImage(QImage image, const QString &path)
{
    QDir dir(path);

    QString fileName = dir.absoluteFilePath(QString("%1.JPG").arg(getDefaultName()));
    if (image.save(fileName)) {
        qDebug() << "Saving Image" << fileName;
        return importFile(fileName);
    } else {
        qWarning() << Q_FUNC_INFO << "Cannot save image" << fileName;
        return false;
    }
}

bool PhotoEditor::removeCapturedImage(const QString &path)
{
    qDebug() << Q_FUNC_INFO << path;
    if (QFile::remove(path)) {
        qDebug() << "Old image removed";
        return true;
    } else {
        qWarning() << "Can't remove the file";
        return false;
    }
}

QImage PhotoEditor::cropImage(QImage image, const QRectF &rectF, const int rotation)
{
    image = image.transformed(QTransform().rotate(-rotation));
    QPointF topLeft;
    QPointF bottomRight;
    if (rotation == 270) {
        topLeft.setX(image.width() * rectF.topLeft().y());
        topLeft.setY(image.height() * rectF.topLeft().x());
        bottomRight.setX(image.width() * rectF.bottomRight().y());
        bottomRight.setY(image.height() * rectF.bottomRight().x());
    } else if (rotation == 0 || rotation == 180) {
        topLeft.setX(image.width() * rectF.topLeft().x());
        topLeft.setY(image.height() * rectF.topLeft().y());
        bottomRight.setX(image.width() * rectF.bottomRight().x());
        bottomRight.setY(image.height() * rectF.bottomRight().y());
    }

    QRect rect(topLeft.toPoint(), bottomRight.toPoint());
    qDebug() << Q_FUNC_INFO << rectF.topLeft().x() << rectF.topLeft().y()
                << rectF.bottomLeft().x() << rectF.bottomRight().y()
             << "AF" << topLeft << bottomRight
             << "F" << rect << rectF
             << "ROTATION" << rotation;

    QImage cropped = image.copy(rect);
    qDebug() << Q_FUNC_INFO << image.size() << image.devicePixelRatio() <<"rect" << rectF.size() << rectF.topLeft() <<  rectF.bottomRight() << rect.size() << rect << "Cropped" << cropped.size() << cropped.devicePixelRatio();
    return cropped;
}

bool PhotoEditor::importFile(const QString &path)
{
    qDebug() << Q_FUNC_INFO << "Importing file" << path << QUrl::fromLocalFile(path);
    FileObject file;
    file.setName(getDefaultName());
    if (DatabaseManager::get()->add(file)) {
        Error error;
        auto result = DatabaseManager::get()->addFileData(file, QUrl::fromLocalFile(path), error);
        qDebug() << "Importing file:" << result;
        qDebug() << Q_FUNC_INFO << "New file id" << file.id() << file.name();
        m_photosIds.append(file.id());
        emit photosIdsChanged(m_photosIds);
        return true;
    } else {
        m_photoCounter--;
        qWarning() << "Unable to import file";
        return false;
    }
}


void PreviewImage::setImagess(const QImage &image)
{
    m_image = image;
    update();
}

void PreviewImage::paint(QPainter *painter)
{
    if (m_image.isNull()) return;
    painter->drawImage(QPointF(0.0f,0.0f), m_image.scaled(300, 200, Qt::KeepAspectRatio));//, this->height(), Qt::KeepAspectRatio));
}

void _q_PreviewImage_registerTypes() {
    qmlRegisterType<PreviewImage>("com.test", 1, 0, "PreviewImage");
}

Q_COREAPP_STARTUP_FUNCTION(_q_PreviewImage_registerTypes)
