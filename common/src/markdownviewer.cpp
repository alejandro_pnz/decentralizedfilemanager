#include "markdownviewer.h"
#include <DatabaseManager.h>

MarkdownViewer::MarkdownViewer(QObject *parent)
    : FileViewer(parent)
{

}

MarkdownViewer::MarkdownViewer(const FileObject &file, QObject *parent)
    : FileViewer(file, parent)
{
    setContent();
}

FileViewer::Type MarkdownViewer::type() const
{
    return MarkdownType;
}

QString MarkdownViewer::content() const
{
    return get_content();
}

void MarkdownViewer::update()
{
    setContent();
}

void MarkdownViewer::setContent()
{
    const auto device = DatabaseManager::get()->fileData(m_file);
    if (device) {
        const auto content = device->readAll();
        qDebug() << "MarkdownViewer::setContent" << device->isOpen() << device->openMode() << content;
        set_content(content);
    }
    else {
        set_content({});
    }
}
