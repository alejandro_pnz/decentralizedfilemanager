#include "deviceeditor.h"
#include <DeviceManager.h>
#include <DeviceObject.h>
#include <devicelistmodel.h>

DeviceEditor::DeviceEditor(QObject *parent) : Editor(parent)
{
    connect(DeviceManager::get(), &DeviceManager::devicesUpdated, this,
            &DeviceEditor::refreshObject);
}

void DeviceEditor::create()
{
}

bool DeviceEditor::edit(const QUuid &id, SyncableObject::ObjectType type)
{
    cleanup();
    setObject(id);
    return !m_object.isNull();
}

bool DeviceEditor::editList(const QModelIndexList &indexes)
{
    Q_UNUSED(indexes);
    return false;
}

void DeviceEditor::save()
{
}

void DeviceEditor::cleanup()
{
    m_object.clear();
    emit objectChanged();
}

DeviceObject *DeviceEditor::object() const
{
    return m_object.get();
}

void DeviceEditor::switchPowerState()
{
//    if (m_object) {
//        if (m_object->powerDevice()) {
//            DeviceManager::get()->revokePower(m_object.get());
//        } else {
//            DeviceManager::get()->givePower(m_object.get());
//        }
//    }
}

void DeviceEditor::activate()
{
    if (m_object) {
        if (!m_object->active()) {
            DeviceManager::get()->activate(m_object.get());
        }
    }
}

void DeviceEditor::deactivate()
{
    if (m_object) {
        if (m_object->active()) {
            DeviceManager::get()->deactivate(m_object.get());
        }
    }
}

void DeviceEditor::block()
{
    if (m_object) {
        if (m_object->active()) {
            DeviceManager::get()->block(m_object.get());
        }
    }
}

SyncableObject::ObjectType DeviceEditor::editorType()
{
    return SyncableObject::Device;
}

void DeviceEditor::refreshObject()
{
    if (m_object) setObject(m_object->id());
}

void DeviceEditor::setObject(const QUuid &id)
{
    auto device = DeviceManager::get()->device(id);
    if (device.isValid()) {
        m_object = DeviceObjectPtr(new DeviceObject(device));
    }
}
