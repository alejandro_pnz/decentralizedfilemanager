#ifndef HOMEPAGEMANAGER_H
#define HOMEPAGEMANAGER_H

#include "abstractpagemanager.h"
#include "qqmlhelpers.h"
#include <QModelIndexList>
#include "enums.h"
#include <SyncableObject.h>
#include <AttributeObject.h>

#include "arcaeditor.h"
#include "quodeditor.h"
#include "fileeditor.h"
#include "homeeditor.h"
#include "deviceeditor.h"
#include "usereditor.h"


class HomePageManager : public AbstractPageManager
{
    Q_OBJECT
    Q_PROPERTY(Flows flow READ currentFlow NOTIFY currentFlowChanged);
    Q_PROPERTY(bool creatingQuod READ creatingQuod NOTIFY currentFlowChanged);
    Q_PROPERTY(bool creatingObject READ creatingObject NOTIFY currentFlowChanged)
    QML_CONSTANT_PROPERTY(FileEditor*, fileEditor)
    QML_CONSTANT_PROPERTY(ArcaEditor*, arcaEditor)
    QML_CONSTANT_PROPERTY(QuodEditor*, quodEditor)
    QML_CONSTANT_PROPERTY(HomeEditor*, homeEditor)
    QML_CONSTANT_PROPERTY(DeviceEditor*, deviceEditor)
    QML_CONSTANT_PROPERTY(UserEditor*, userEditor)
    Q_PROPERTY(Editor* currentEditor READ currentEditor NOTIFY currentFlowChanged);

    Q_PROPERTY(bool boardsSectionExpanded READ getBoardsSectionExpanded WRITE setBoardsSectionExpanded NOTIFY boardsSectionExpandedChanged)
    Q_PROPERTY(bool toolsSectionExpanded READ getToolsSectionExpanded WRITE setToolsSectionExpanded NOTIFY toolsSectionExpandedChanged)
    Q_PROPERTY(bool supportSectionExpanded READ getSupportSectionExpanded WRITE setSupportSectionExpanded NOTIFY supportSectionExpandedChanged)

public:
    enum Flows {
        None,
        CreateQuod,
        CreateArca,
        CreateFile,
        ManageArca,
        ManageQuod,
        ManageFile,        
    };
    Q_ENUM(Flows);

    enum LibraryTabs {
        AllTab  = 0,
        ArcaTab = 1,
        QuodTab = 2,
        FileTab = 3
    };
    Q_ENUM(LibraryTabs)

    static HomePageManager *get();

    explicit HomePageManager(QObject *parent = nullptr);
    Q_INVOKABLE void openPage(Pages::Page page, bool closeCurrent = false);

    Q_INVOKABLE void abortCurrentFlow();
    Q_INVOKABLE void saveCurrentFlow();

    // Create Arca flow
    Q_INVOKABLE void enterCreateArca();

    // Create Quod flow
    Q_INVOKABLE void enterCreateQuod();

    // Create File flow
    Q_INVOKABLE void enterCreateFile();

    Q_INVOKABLE void openOneArcaPage(const QUuid &id);
    Q_INVOKABLE void openOneQuodPage(const QUuid &id);
    Q_INVOKABLE void openOneFilePage(const QUuid &id);
    Q_INVOKABLE void openOneDevicePage(const QUuid &id);
    Q_INVOKABLE void openAddNewDevicePage(bool newDeviceIsAllowed);
    Q_INVOKABLE void openOneUserPage(const QUuid &id);

    Q_INVOKABLE void openQuodFromTag(const QUuid &id);

    Q_INVOKABLE void enterEditListPage(const QModelIndexList& indexes, Pages::Page page, int type);
    Q_INVOKABLE void enterEditPage(const QUuid &id, Pages::Page page, int type);
    Q_INVOKABLE void enterLinkToArcaPage(const QUuid &id, int type, int action);
    Q_INVOKABLE void enterLinkListToArcaPage(const QModelIndexList& indexes, int type, int action);

    Q_INVOKABLE void setCurrentLibraryTab(const int tab);

    Q_INVOKABLE void enterEditTagPage(const QJSValue& value);
    Q_INVOKABLE void enterLibraryPage(const int tab = AllTab, const int sorter = 0);
    Q_INVOKABLE void enterCustomAttributePage(const QString& name);
    Q_INVOKABLE void enterQuodCheckedOutPopupPage(const bool edit = false);
    Q_INVOKABLE void saveNote(const QString&);
    Q_INVOKABLE void editNote();
    Q_INVOKABLE void enterEditGeneralDetailsPage(const QJSValue &config);
    Q_INVOKABLE void enterChangeSyncStatusPage(const QJSValue& config);

    Q_INVOKABLE void enterShowInfoPage(const QUuid &id, const int type);
    Q_INVOKABLE void enterLoginInactivitySettingsPage(const QJSValue& config);
    Q_INVOKABLE void enterResetSettingsPage(const QJSValue& config);
    Q_INVOKABLE void enterDataSettingsPage(const QJSValue& config);
    Q_INVOKABLE void enterNameObjectPage(const QJSValue& config);
    Q_INVOKABLE void enterSettingsPage(const int id);
    Q_INVOKABLE void enterFileDialog();
    Q_INVOKABLE void enterChangeOwnerPage(SyncableObject::ObjectType type);
    Q_INVOKABLE void enterChangeOwnerPage();
    Q_INVOKABLE void enterSuccessfullyCompletedPopupPage(const QJSValue& config);
    Q_INVOKABLE void enterCheckEmailPopupPage();
    Q_INVOKABLE void enterSelectObjectPage(const QJSValue& config);
    Q_INVOKABLE void enterGenericPopupPage(const QJSValue& config);

    Q_INVOKABLE void enterPasswordGeneratorPage(const QUuid& sectionId, const QUuid& attributeId, int sectionIndex);
    Q_INVOKABLE void enterPasswordHistoryPage(const AttributeHistoryList &list);

    Q_INVOKABLE void enterConfirmObjectDeletionPopupPage();

    Q_INVOKABLE void enterConfirmTagDeletionPopupPage(const QUuid& id);
    Q_INVOKABLE void closeEditTag();

    Q_INVOKABLE void enterFileViewerWindow();

    Q_INVOKABLE void enterMoreFiltersPage(const QJSValue& config);
    Q_INVOKABLE void enterFilterPage(Pages::Page page, const QJSValue& config);

    Q_INVOKABLE void enterEditMarkdownFile(const QUuid& fileId);

    Flows currentFlow() const;

    Editor *currentEditor() const;
    Q_INVOKABLE Editor* editorForType(SyncableObject::ObjectType type);

    bool creatingQuod() const; // TODO this method should be removed and all invokation should be replaced by checking current flow flag or creatingObject() helper method
    bool creatingObject() const;

    Q_INVOKABLE void setDontShowArca(const bool);

signals:
    void currentFlowChanged();
    void cannotAddMoreDevices();
    void setupEditTagPage(const QJSValue& value);
    void setupLibraryPage(const int tab, const int sorter);
    void createCustomAttribute(const QString& name);
    void setupCustomAttributePage(const QString& name);
    void setupQuodCheckedOut(const bool edit);
    void setupNotePage(const QString& note, const bool edit = true, const bool noteMode = true);
    void setupAddTagsPage();
    void setupEditGeneralDetailsPage(const QJSValue& config);
    void setupChangeSyncStatusPage(const QJSValue& config);
    void clearLibrarySelection();
    void setupLoginInactivitySettingsPage(const QJSValue& config);
    void setupResetSettingsPage(const QJSValue& config);
    void setupDataSettingsPage(const QJSValue& config);
    void setupNameObjectPage(const QJSValue& config);
    void setupSettingsPage(const int id);
    void setupFileDialog();
    void setupChangeOwnerPage(SyncableObject::ObjectType type);
    void setupSelectObjectsPage(const QJSValue& config);
    void fileChosen(const QString& name, const QUuid& id);
    void closeEditTagRequested();
    void openFileViewerWindow();
    void setupPasswordHistoryPage(const AttributeHistoryList &list);
    void setupGenericPopupPage(const QJSValue& config);
    void setupMoreFiltersPage(const QJSValue& config);
    void setupFilterPage(const QJSValue& config);
    void setupInfoPage(const QUuid &id, const int type);
    void setupSuccessfullyCompletedPopupPage(const QJSValue& config);

    void boardsSectionExpandedChanged();
    void toolsSectionExpandedChanged();
    void supportSectionExpandedChanged();

private:
    void saveNewArca();
    void saveNewQuod();
    void saveNewFile();

    void saveEditedObject();

    void abortArcaCreation();
    void abortQuodCreation();
    void abortFileCreation();

    void setCurrentFlow(Flows flow);
    void backToFirstMainPage();
    void backToHomeAndOpenPage(Pages::Page page);

    void setBoardsSectionExpanded(const bool);
    void setToolsSectionExpanded(const bool);
    void setSupportSectionExpanded(const bool);

    bool getBoardsSectionExpanded()const;
    bool getToolsSectionExpanded()const;
    bool getSupportSectionExpanded()const;

    //Vanik: this is temporary solution, I think that we need include these settings into SettingsManager
    QSettings m_settings;

    QVector<Pages::Page> m_mainPages{
        Pages::Page::HomePage, Pages::Page::LibraryPage
    };
    Flows m_currentFlow{None};
    // AbstractPageManager interface
    Flows setFlowForObjectType(SyncableObject::ObjectType type);

    QQmlApplicationEngine *m_engine{};

public:
    void init(const QString &contextProperty, QQmlApplicationEngine *engine, bool isTablet = false) override;
protected:
    void pageAboutToBeRemoved(Pages::Page page) override final;
};





#endif // HOMEPAGEMANAGER_H
