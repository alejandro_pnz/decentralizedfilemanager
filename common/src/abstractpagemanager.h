#ifndef ABSTRACTPAGEMANAGER_H
#define ABSTRACTPAGEMANAGER_H

#include <QObject>
#include <QJSValue>
#include "pages.h"
#include <QStack>

class QQmlApplicationEngine;
class QQmlFileSelector;

class AbstractPageManager : public QObject
{
    Q_OBJECT
public:
    explicit AbstractPageManager(QObject *parent = nullptr);
    virtual void init(const QString& contextProperty, QQmlApplicationEngine* engine = nullptr, bool isTablet = false);
    Q_INVOKABLE void enterPage(Pages::Page page);
    Q_INVOKABLE void enterEmptyPage(Pages::Page page);
    Q_INVOKABLE void back();
    Q_INVOKABLE void backTo(Pages::Page page);
    Q_INVOKABLE void backUntil(Pages::Page page);

    Q_INVOKABLE bool isPageOnStack(Pages::Page page) const;
    Q_INVOKABLE bool isPageOnTop(Pages::Page page) const;
signals:
    void popPage(Pages::Page page);
    void pushPage(Pages::Page page, const QString& path) const;
protected:
    virtual void pageAboutToBeRemoved(Pages::Page page);
    Pages::Page topPage() const;
    QString pagePath(Pages::Page page);
    QQmlFileSelector *m_fileSelector{};
    QStack<Pages::Page> m_visiblePages;
};

#endif // ABSTRACTPAGEMANAGER_H
