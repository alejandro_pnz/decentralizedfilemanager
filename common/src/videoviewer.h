#ifndef VIDEOVIEWER_H
#define VIDEOVIEWER_H

#include <QObject>
#include "fileviewer.h"
#include <QObject>

#include "QmlVlcPlayer.h"

class VideoViewer : public FileViewer
{
    Q_OBJECT
    Q_PROPERTY(QmlVlcPlayer* player READ player NOTIFY playerChanged)
    Q_PROPERTY(QIODevice* device READ device NOTIFY deviceChanged)
    Q_PROPERTY(bool hasVideo READ hasVideo NOTIFY hasVideoChanged)
public:
    explicit VideoViewer(const FileObject &file, const bool hasVideo = true, QObject *parent = nullptr);
    ~VideoViewer();

    virtual Type type() const override;
    QmlVlcPlayer *player() const;
    QIODevice *device() const;
    bool hasVideo() const;

signals:
    void playerChanged();
    void deviceChanged();

    void hasVideoChanged();

private:
    QmlVlcPlayer* m_player{};
    QSharedPointer<QIODevice> m_device;
    bool m_hasVideo;
};

#endif // VIDEOVIEWER_H
