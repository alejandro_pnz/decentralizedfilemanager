
QT += concurrent

CONFIG += resources_big

include($$PWD/../application/src/models/models.pri)

win32 { # Windows specific configuration
    # Add proper font for this platform
    RESOURCES += $$PWD/fonts/roboto.qrc
} macx { # MacOSX specific configuration
    RESOURCES += $$PWD/fonts/sfpro.qrc
} ios { # iOS specific configuration
    RESOURCES += $$PWD/fonts/sfpro.qrc
} android { # Android specific configuration
    RESOURCES += $$PWD/fonts/roboto.qrc
}

tests {
    mobile {
        RESOURCES += $$PWD/qml/tests_mobile.qrc
    }

    desktop {
        RESOURCES += $$PWD/qml/qml.qrc
    }
}
else {
    RESOURCES += $$PWD/qml/qml.qrc
}

RESOURCES += \
        $$PWD/fonts/fonts.qrc \
        $$PWD/resources/resources.qrc

include($$PWD/illustrations/illustrations.pri)

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH += $$PWD/qml

HEADERS += \
        $$PWD/src/deviceeditor.h \
        $$PWD/src/documentviewer.h \
        $$PWD/src/fileviewer.h \
        $$PWD/src/fileviewermanager.h \
        $$PWD/src/homeeditor.h \
        $$PWD/src/imageviewer.h \
        $$PWD/src/imageviewerimageprovider.h \
        $$PWD/src/itemimageprovider.h \
        $$PWD/src/markdownviewer.h \
        $$PWD/src/photoeditor.h \
        $$PWD/src/qml/editablepanel.h \
        $$PWD/src/safenetworkcache.h \
        $$PWD/src/singleton.h \
        $$PWD/src/qqmlhelpers.h \
        $$PWD/src/editor.h \
        $$PWD/src/quodeditor.h \
        $$PWD/src/enums.h \
        $$PWD/src/homepagemanager.h \
        $$PWD/src/pagemanager.h \
        $$PWD/src/pages.h \
        $$PWD/src/themesettings.h \
        $$PWD/src/usereditor.h \
        $$PWD/src/utility.h \
        $$PWD/src/abstractpagemanager.h \
        $$PWD/src/arcaeditor.h \
        $$PWD/src/fileeditor.h \
        $$PWD/src/photoimageprovider.h \
        $$PWD/src/videoviewer.h \
        $$PWD/src/viewerimageprovider.h

INCLUDEPATH += \
        $$PWD/../application/src/models \
        $$PWD/src

DEPENDPATH += $$PWD/../application/src/models

SOURCES += \
        $$PWD/src/deviceeditor.cpp \
        $$PWD/src/documentviewer.cpp \
        $$PWD/src/editor.cpp \
        $$PWD/src/fileviewer.cpp \
        $$PWD/src/fileviewermanager.cpp \
        $$PWD/src/homeeditor.cpp \
        $$PWD/src/imageviewer.cpp \
        $$PWD/src/markdownviewer.cpp \
        $$PWD/src/photoeditor.cpp \
        $$PWD/src/qml/editablepanel.cpp \
        $$PWD/src/quodeditor.cpp \
        $$PWD/src/homepagemanager.cpp \
        $$PWD/src/pagemanager.cpp \
        $$PWD/src/safenetworkcache.cpp \
        $$PWD/src/themesettings.cpp \
        $$PWD/src/abstractpagemanager.cpp \
        $$PWD/src/arcaeditor.cpp \
        $$PWD/src/fileeditor.cpp \
        $$PWD/src/usereditor.cpp \
        $$PWD/src/videoviewer.cpp



# Media
include($$PWD/media/media.pri)

# OpenSSL
include($$PWD/openssl/openssl.pri)

# PDF
include($$PWD/pdf/pdf.pri)

# QPM
include($$PWD/vendor/vendor.pri)

# SDK
include($$PWD/sdk/0.9.1/qbsdk.pri)
