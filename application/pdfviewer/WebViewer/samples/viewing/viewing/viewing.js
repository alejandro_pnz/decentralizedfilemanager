// @link WebViewerInstance: https://www.pdftron.com/api/web/WebViewerInstance.html
// @link UI.loadDocument: https://www.pdftron.com/api/web/UI.html#loadDocument__anchor

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

WebViewer(
  {
    path: '../../../lib'
  },
  document.getElementById('viewer')
).then(instance => {
  samplesSetup(instance);

  var fileId = getParameterByName('fileid');
  instance.UI.loadDocument("http://localhost:44387/files/" + fileId);
});
