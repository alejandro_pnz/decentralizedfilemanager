
# must use a variable as input
pdfviewer_DEPS = $$PWD/pdfviewer.qrc
PreBuildPdfviewer.input = pdfviewer_DEPS
# use non-existing file here to execute every time
PreBuildPdfviewer.output = $$OUT_PWD/pdfviewer.rcc
# the system call to the batch file
PreBuildPdfviewer.commands = $$[QT_HOST_LIBEXECS]/rcc -binary -no-compress ${QMAKE_FILE_IN} -o ${QMAKE_FILE_OUT}
# some name that displays during execution
PreBuildPdfviewer.name = Preparing pdfviewer resource...
# "no_link" tells qmake we don’t need to add the output to the object files for linking,
# "target_predeps" tells qmake that the output of this needs to exist before we can do the rest of our compilation.
PreBuildPdfviewer.CONFIG += no_link target_predeps
# Add the compiler to the list of 'extra compilers'.
QMAKE_EXTRA_COMPILERS += PreBuildPdfviewer

android {
    pdfviewer_assets.files = $$OUT_PWD/pdfviewer.rcc
    pdfviewer_assets.path = /assets/pdfviewer
    pdfviewer_assets.CONFIG += no_check_exist
    INSTALLS += pdfviewer_assets
}

mac {
    pdfviewer.files = $$OUT_PWD/pdfviewer.rcc
    macx: pdfviewer.path = Contents/Resources
    ios: pdfviewer.path = .
    QMAKE_BUNDLE_DATA += pdfviewer
}
