#include "RegistrationManager.h"
#include "quodarcaapplication.h"
#include <QQmlEngine>
#include <QQmlContext>
#include "pagemanager.h"
#include "enums.h"
#include "Utils.h"
#include <QDebug>
#include <QQmlExpression>
#include <QJSValue>

#include <AccountManager.h>
#include <AuthenticationManager.h>
#include <ErrorException.h>
#include <Errors.h>

#include <SyncManager.h>

#include <QtPromise>

RegistrationManager::RegistrationManager(PageManager *pageManager, QQmlEngine *engine, QObject *)
    : m_engine(engine),
      m_pageManager(pageManager)
{
    engine->rootContext()->setContextProperty("RegistrationManager", this);
    connect(AccountManager::get(), &AccountManager::accountsChanged, this, &RegistrationManager::registeredChanged);
    connect(AuthenticationManager::get(), &AuthenticationManager::registrationTokenReceived, this, &RegistrationManager::onRegistrationTokenReceived);
}

/*!
 * \brief RegistrationManager::init
 * Decides which page to open first.
 */
void RegistrationManager::init()
{
    cleanup();
    m_isAccountOwner = true;
    m_powerPassphraseIsConfigured = true;

    enterHome();

//    if (!registered()) {
//        m_pageManager->enterPage(Pages::DeviceDashboardPage);
////        m_pageManager->enterOnboardingWebViewPage();
//    }
//    else {
//        m_pageManager->enterPage(Pages::LoginPage);
//        if(m_addingNewDeviceReceived){
//            termsPage();
//        }
//        else if (m_addDeviceBlocked) {
//            deviceActivationBlocked();
//        }
//        else if (m_addDeviceRejected) {
//            deviceActivationRejected();
//        }
//    }
}

void RegistrationManager::createAccount()
{
    cleanup();
    m_currentFlow = RegistrationType::NewAccount;
    m_pageManager->enterChoosePricePlanWebViewPage();
}

void RegistrationManager::loginPage()
{
    m_pageManager->enterPage(Pages::LoginPage);
}

void RegistrationManager::joinExistingAccount()
{
    cleanup();

    m_currentFlow = RegistrationType::JoinExistingAccount;
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.termsPage() })");

    QJSValue config = m_engine->newObject();
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));

    m_pageManager->enterOnboardingPage(config);
}

void RegistrationManager::termsPage()
{
    QJSValue config = m_engine->newObject();

    if (m_currentFlow == RegistrationType::NewAccount) {
        QQmlExpression handler(m_engine->rootContext(), nullptr,
                               "(function() { RegistrationManager.accountSetupPage() })");
        config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
        m_pageManager->enterTermsPage(config);
    }
    else if (m_currentFlow == RegistrationType::JoinExistingAccount) { // JoinExistingAccount
        enterAccountOwnerEmail();
    }
    else if(m_currentFlow == RegistrationType::AddNewDevice){
        QQmlExpression handler(m_engine->rootContext(), nullptr,
                               "(function() { RegistrationManager.addNewDeviceMultipleAccountOwner() })");
        config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
        m_pageManager->enterTermsPage(config);
    }
}

void RegistrationManager::openLink(const QString &link)
{
    QJSValue config = m_engine->newObject();
    config.setProperty("link", link);
    m_pageManager->enterWebViewLinkPage(config);
}

void RegistrationManager::createAccountPage()
{
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function(email, passphrase) { RegistrationManager.checkEmail(email, passphrase) })");

    QJSValue config = m_engine->newObject();
    config.setProperty("title", m_strings.property("createAnAccount"));
    config.setProperty("buttonText", m_strings.property("createAccount"));
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));

    m_pageManager->enterCreateAccountPage(config);
}

void RegistrationManager::joinAccountPage(const QString &accountOwnerEmail)
{
    auto promise = QtPromise::resolve(AuthenticationManager::get()->accountCount(accountOwnerEmail));

    promise.then([=](int accountCount) {
        if (accountCount == 0) {
            QJSValue config = m_engine->newObject();
            config.setProperty("emailError", m_strings.property("emailIsIncorrect"));
            emailDoesNotExist(config);
        }
        else {
            m_accountOwnerEmail = accountOwnerEmail;

            QQmlExpression handler(m_engine->rootContext(), nullptr,
                                   "(function(email, passphrase) { RegistrationManager.checkEmail(email, passphrase) })");

            QJSValue config = m_engine->newObject();
            config.setProperty("title", m_strings.property("joinTitle"));
            config.setProperty("buttonText", m_strings.property("joinExistingAccount"));
            config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));

            m_pageManager->enterCreateAccountPage(config);
        }
    });
}

void RegistrationManager::checkEmailPage()
{
//    m_pageManager->enterPage(Pages::CheckEmailPage);
}

void RegistrationManager::accountSetupPage()
{
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function(name) { RegistrationManager.setPassphrasePage(name) })");
    QJSValue config = m_engine->newObject();
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("email", m_accountOwnerEmail);
    m_pageManager->enterAccountSetupPage(config);
}

void RegistrationManager::choosePowerPassphrasePage()
{
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function(passphrase) { RegistrationManager.confirmPowerPassphrasePage(passphrase) })");

    QJSValue config = getConfig(false);
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));

    m_pageManager->enterPowerPasswordPage(config);
}

void RegistrationManager::confirmPowerPassphrasePage(const QString& passphrase)
{
    m_powerPassphrase = passphrase;
    m_pageManager->enterPage(Pages::ConfirmPowerPassphrasePage);
}

void RegistrationManager::inviteUsersPage()
{
    m_pageManager->enterPage(Pages::InviteUsersPage);
}

void RegistrationManager::emailValidated()
{
    if (m_currentFlow == RegistrationType::NewAccount) {
        choosePowerPassphrasePage();
    } else if (m_currentFlow == RegistrationType::JoinExistingAccount) {
        requestToJoinSubmitted();
    }
}

void RegistrationManager::accountOwnerOrNot()
{
    QQmlExpression primaryHandler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.meAccountOwner() })");
    QQmlExpression secondaryHandler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.enterAccountOwnerEmail() })");
    QJSValue config = getConfig(false);
    config.setProperty("title", m_strings.property("areYouOwner"));
    config.setProperty("content", "");
    config.setProperty("type", Pages::Double);
    config.setProperty("primaryButtonText", m_strings.property("yes"));
    config.setProperty("secondaryButtonText", m_strings.property("no"));
    config.setProperty("secondaryButtonLabel", "");
    config.setProperty("imageSrc", "qrc:/illustrations/r1-account-owner.png");
    config.setProperty("primaryButtonHandler", m_engine->toScriptValue(primaryHandler.evaluate()));
    config.setProperty("secondaryButtonHandler", m_engine->toScriptValue(secondaryHandler.evaluate()));

    m_pageManager->enterGenericInfoPage(config);
}

void RegistrationManager::enterAccountOwnerEmail()
{
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function(email) { RegistrationManager.enteredAccountEmail(email) })");

    QJSValue config = m_engine->newObject();
    config.setProperty("title", m_strings.property("whoIsOwner"));
    config.setProperty("contentText", "");
    config.setProperty("buttonText", m_strings.property("continueTxt"));
    config.setProperty("labelText", m_strings.property("accountOwnerEmail"));
    config.setProperty("placeholderText", m_strings.property("typeAccountOwnerEmail"));
    config.setProperty("isPassPhrase", false);
    config.setProperty("eyeIconVisible", false);
    config.setProperty("clearIconVisible", false);
    config.setProperty("emailValidator", true);
    config.setProperty("imageSrc", "qrc:/illustrations/r2-who-is-account-owner.png");
    config.setProperty("validatorText", m_strings.property("emailIsNotValid"));
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));

    m_pageManager->enterGenericInputPage(config);
}

void RegistrationManager::enterAddDeviceAdvancedPage()
{
    QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(qApp);
    Q_ASSERT(app);
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.firstSyncInProgress() })");
    QJSValue config = m_engine->newObject();
    config.setProperty("deviceType", app->deviceType());
    config.setProperty("deviceName", app->deviceName());
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    m_pageManager->enterPage(Pages::Page::AddDeviceAdvancedPage);
    emit m_pageManager->setupAddDeviceAdvancedPage(config);
}

void RegistrationManager::requestToJoinSubmitted()
{
    QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(qApp);
    Q_ASSERT(app);

    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.registration() })");

    QJSValue config = getConfig(false);
    config.setProperty("title", m_strings.property("requestToJoinSubmitted"));
    config.setProperty("content", m_strings.property("weWillSendEmailAfterReview"));
    config.setProperty("deviceType", app->deviceType());
    config.setProperty("deviceName", app->deviceName());
    config.setProperty("imageSrc", "qrc:/illustrations/r4-request-to-join-submitted.png");
    config.setProperty("buttonText", m_strings.property("gotIt"));
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));

    m_pageManager->enterDeviceStatusPage(config);
}

void RegistrationManager::showEmailConfirmation()
{
    QJSValue genericInfoConfig = getConfig(false);
    genericInfoConfig.setProperty("title", m_strings.property("checkYourEmail"));
    genericInfoConfig.setProperty("content", m_strings.property("emailWithLinkAddNewDevice"));
    genericInfoConfig.setProperty("imageSrc", "qrc:/illustrations/r20_26.png");
    genericInfoConfig.setProperty("type", Pages::None);
    m_pageManager->enterGenericInfoPopupPage(genericInfoConfig);
}

void RegistrationManager::newDeviceRequestAccepted()
{
    QJSValue genericInfoConfig = getConfig(false);
    genericInfoConfig.setProperty("title", m_strings.property("newDeviceRequestAcceptedTitle"));
    genericInfoConfig.setProperty("content", m_strings.property("newDeviceRequestAcceptedContent"));
    genericInfoConfig.setProperty("imageSrc", "qrc:/illustrations/successfully-completed.png");
    genericInfoConfig.setProperty("type", Pages::None);
    m_pageManager->enterGenericInfoPopupPage(genericInfoConfig);
}

void RegistrationManager::inviteUsers()
{
    QQmlExpression primaryHandler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.enterHome() })");
    QQmlExpression secondaryHandler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.choosePlan() })");
    QJSValue config = getConfig(false);
    config.setProperty("title", m_strings.property("invitationsToJoinSent"));
    config.setProperty("content", m_strings.property("youAreAllDoneAndReadyToGo"));
    config.setProperty("type", Pages::Double);
    config.setProperty("primaryButtonText", m_strings.property("startFree14DayTrial"));
    config.setProperty("secondaryButtonText", m_strings.property("subscribeToQuodArcaNow"));
    config.setProperty("secondaryButtonLabel", m_strings.property("or"));
    config.setProperty("primaryButtonHandler", m_engine->toScriptValue(primaryHandler.evaluate()));
    config.setProperty("secondaryButtonHandler", m_engine->toScriptValue(secondaryHandler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r30.png");

    m_pageManager->enterGenericInfoPage(config);
}

void RegistrationManager::skipInvitingUsers()
{
    QQmlExpression primaryHandler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.enterHome() })");
    QQmlExpression secondaryHandler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.choosePlan() })");
    QJSValue config = getConfig(false);
    config.setProperty("title", m_strings.property("thatsIt"));
    config.setProperty("content", m_strings.property("youAreAllDoneAndReadyToGo"));
    config.setProperty("type", Pages::Double);
    config.setProperty("primaryButtonText", m_strings.property("startFree14DayTrial"));
    config.setProperty("secondaryButtonText", m_strings.property("subscribeToQuodArcaNow"));
    config.setProperty("secondaryButtonLabel", m_strings.property("or"));
    config.setProperty("primaryButtonHandler", m_engine->toScriptValue(primaryHandler.evaluate()));
    config.setProperty("secondaryButtonHandler", m_engine->toScriptValue(secondaryHandler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r29-Thats-it.png");

    m_pageManager->enterGenericInfoPage(config);
}

void RegistrationManager::enterHome(const QJSValue &config)
{
    m_pageManager->backToLogin();
    m_pageManager->enterHomePage(config);
}

void RegistrationManager::settingUpYourAccount(const QString& passphrase)
{
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.enterHome() })");
    QJSValue config = getConfig(false);
    config.setProperty("title", m_strings.property("settingUpYourAccount"));
    config.setProperty("content", m_strings.property("pleaseBePatient"));
    config.setProperty("type", Pages::Progress);
    config.setProperty("primaryButtonText", m_strings.property("next"));
    config.setProperty("primaryButtonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("finishedHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r8-first-sync-in-progress.png");

    m_pageManager->enterGenericInfoPage(config);

    m_loginPassphrase = passphrase;
    m_powerPassphrase = passphrase;

    auto promise = QtPromise::resolve();

    promise.then([this]() {
        if (currentFlow() == RegistrationType::NewAccount) {
            return QtPromise::resolve(AuthenticationManager::get()->accountRegistration(m_registrationToken, m_accountName, m_accountOwnerEmail, m_loginPassphrase, m_powerPassphrase));
        }
        else {
            qWarning() << "This flow is not supported:" << currentFlow();
        }
        throw QtPromise::QPromiseCanceledException();
    }).then([this]() {
        if (currentFlow() == RegistrationType::NewAccount) {
            QJSValue config = m_engine->newObject();
            config.setProperty("showSuccessfullyCompletedPopupPage", true);
            enterHome(config);
        }
        else {
            qWarning() << "This flow is not supported:" << currentFlow();
        }
    }).fail([this](const ErrorException &e) {
        handleError(e.error());
    });
}

void RegistrationManager::setPassphrasePage(const QString &accountName, bool isLogin)
{
    // TODO:
    if (isLogin) {
        m_accountName = accountName;
    }

    QQmlExpression handlerLogin(m_engine->rootContext(), nullptr,
                           "(function(passphrase) { RegistrationManager.settingUpYourAccount(passphrase) })");
    QQmlExpression handlerPower(m_engine->rootContext(), nullptr,
                           "(function(passphrase) { RegistrationManager.configurePowerPassphrase(passphrase) })");
    QJSValue config = m_engine->newObject();
    config.setProperty("buttonHandler", m_engine->toScriptValue(isLogin ? handlerLogin.evaluate() : handlerPower.evaluate()));
    config.setProperty("email", m_accountOwnerEmail);
    config.setProperty("isLogin", isLogin);
    m_pageManager->enterSetPassphrasePage(config);
}

void RegistrationManager::configurePowerPassphrase(const QString& powerPassphrase)
{
    //for SDK
    Q_UNUSED(powerPassphrase)

    //need to ask sdk to configure power passphrase

    m_pageManager->back();
    emit m_pageManager->powerPassphraseSuccessConfigured();
}

void RegistrationManager::onboardingInvitation()
{
    m_currentFlow = RegistrationType::InvitedUser;
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.temporaryPassphrase() })");

    QJSValue config = m_engine->newObject();
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));

    m_pageManager->enterOnboardingPage(config);
}

void RegistrationManager::temporaryPassphrase()
{
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.configureLoginPassphrase() })");

    QJSValue config = m_engine->newObject();
    config.setProperty("title", m_strings.property("welcome"));
    config.setProperty("contentText", m_strings.property("enterTemporaryPassphraseFromEmail"));
    config.setProperty("buttonText", m_strings.property("next"));
    config.setProperty("deviceDelegate", false);
    config.setProperty("icon", "qrc:/login-logo.svg");
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));

    m_pageManager->enterAddDevicePage(config);
}

void RegistrationManager::configureLoginPassphrase()
{
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.requestToJoinSubmitted() })");

    QJSValue config = m_engine->newObject();
    config.setProperty("title", m_strings.property("configureLoginPassphrase"));
    config.setProperty("contentText", m_strings.property("youWillNeedToEnterEmailAndPassphrase"));
    config.setProperty("buttonText", m_strings.property("next"));
    config.setProperty("noEmail", true);
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));

    m_pageManager->enterCreateAccountPage(config);
}

void RegistrationManager::login(const QString &email, const QString &passphrase)
{
    cleanup();

    m_currentFlow = RegistrationType::Login;
    setBusy(true);
    m_loginPassphrase = passphrase;

    auto promise = QtPromise::resolve(AuthenticationManager::get()->accountCount(email));

    promise.then([=](int accountsCount) {
        if (accountsCount == 0) {
            QJSValue config = m_engine->newObject();
            config.setProperty("emailError", m_strings.property("emailIsIncorrect"));
            wrongCredentials(config);
            setBusy(false);
        }
        else {
            // Search for local account
            AccountList allAccounts = AccountManager::get()->allAccounts();

            AccountList accounts;
            std::copy_if(allAccounts.begin(), allAccounts.end(), std::back_inserter(accounts), [&](const Account &account) {
                return account.email() == email;
            });

            if (accounts.isEmpty() && accountsCount > 0) {

                m_currentFlow = RegistrationType::AddNewDevice;

                auto promise = QtPromise::connect(this, &RegistrationManager::meAccountOwner, &RegistrationManager::loginWithAccountOwnerEmail);

                promise.then([=]() {
                    checkEmail(email, passphrase);
                }).fail([=](const QString &accountOwnerEmail) {
                    m_accountOwnerEmail = accountOwnerEmail;
                    newDeviceRegisteredUser();
                });

                if (accountsCount == 1) {
                    Q_EMIT this->meAccountOwner();
                }
                else {
                    // TODO:
                    m_deviceEmail = email;
                    accountOwnerOrNot();
                }
            }
            else if (accounts.count() == 1 && accountsCount == 1) {

                m_account.reset(new Account(accounts.first()));

                return QtPromise::resolve(AuthenticationManager::get()->login(m_account.get(), m_loginPassphrase));
            }
            else {
                /// TODO: ask only if multiple slave accounts
                accountOwnerOrNot();

                auto promise = QtPromise::connect(this, &RegistrationManager::meAccountOwner, &RegistrationManager::loginWithAccountOwnerEmail);

                return promise.then([=]() {
                    m_pageManager->backToLogin();
                    setBusy(true);

                    auto account = std::find_if(accounts.begin(), accounts.end(), [](const Account &account) { return account.accountOwner(); });
                    m_account.reset(new Account(*account));
                    return QtPromise::resolve(AuthenticationManager::get()->login(m_account.get(), m_loginPassphrase));
                }).fail([=](const QString &accountOwnerEmail) {
                    m_pageManager->backToLogin();
                    setBusy(true);

                    auto account = std::find_if(accounts.begin(), accounts.end(), [&accountOwnerEmail](const Account &account) { return account.accountEmail() == accountOwnerEmail; });
                    if (account == accounts.end()) {
                        setBusy(false);
                        QJSValue config = m_engine->newObject();
                        config.setProperty("emailError", m_strings.property("emailIsIncorrect"));
                        wrongCredentials(config);
                        throw QtPromise::QPromiseCanceledException();
                    }
                    m_account.reset(new Account(*account));
                    return QtPromise::resolve(AuthenticationManager::get()->login(m_account.get(), m_loginPassphrase));
                });
            }
        }

        throw QtPromise::QPromiseCanceledException();
    }).then([=]() {
        enterHome();

        cleanup();
    }).fail([=](const ErrorException &e) {
        if (e.error() == Errors::LoginFailed) {
            const QVariantMap data = e.error().data().toMap();

            int remainingAttempts = data.value("remaining_attempts").toInt();

            QJSValue config = m_engine->newObject();
            config.setProperty("passphraseError", m_strings.property("passphraseIsIncorrect").toString().arg(remainingAttempts));
            wrongCredentials(config);

            setBusy(false);
        }
        else if (e.error() == Errors::LoginBlocked) {
            if (m_account->accountOwner()) {
                deviceBlockedAccountOwner();
            }
            else {
                loginBlocked();
            }
        }
        else if (e.error() == Errors::AccountBlocked) {
            if (m_account->accountOwner()) {
                deviceBlockedAccountOwner();
            }
            else {
                accountBlocked();
            }
        }
        else if (e.error() == Errors::NeedFirstSync) {
            firstSyncPending();
        }
        else {
            handleError(e.error());
        }
    }).finally([=]() {
        //setBusy(false);
    });

    // TODO REMOVE just to show the idea
    //if (passphrase == "bad") {
    //    QJSValue config = m_engine->newObject();
    //    config.setProperty("emailError", m_strings.property("emailIsIncorrect"));
    //    config.setProperty("passphraseError", m_strings.property("passphraseIsIncorrect").toString().arg(4));
    //    wrongCredentials(config);
    //    return;
    //}
    // TODO call SDK login methods and in callbacks call below methods:
    // if new device registered user (not multiple)
    //newDeviceRegisteredUser();
    // if part of multiple account
//    newDeviceMultipleAccount();
    // if new device account owner
//    newDeviceAccountOwner();
    // if user is blocked
//    loginBlocked();
//    loginUnblocked();
    // if trial will expire in 3 days - account owner
//      trialWillExpireAccountOwner();
    // if trial will expire in 3 days - invited user
//    trialWillExpireInvitedUser();
    // if trial expired -- account owner
//      trialExpiredAccountOwner();
    // if trial expired - invited user
//    trialExpiredInvitedUser();
    // invited user - device/ account blocked
//    deviceBlocked();
//    accountBlocked();
    // account owner - device blocked
//    deviceBlockedAccountOwner();
}

/*!
 * \brief RegistrationManager::newDeviceRegisteredUser
 * Starts the Flow 1. Registered user that is not an account owner
 * add a new device
 */
void RegistrationManager::newDeviceRegisteredUser()
{
    QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(qApp);
    Q_ASSERT(app);

    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.requestToJoinSubmitted() })");

    QJSValue config = m_engine->newObject();
    config.setProperty("title", m_strings.property("welcomeBack"));
    config.setProperty("contentText", m_strings.property("logInDifferentDevice"));
    config.setProperty("buttonText", m_strings.property("addDevice"));
    config.setProperty("deviceType", app->deviceType());
    config.setProperty("deviceName", app->deviceName());
    config.setProperty("input", false);
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r13.png");

    m_pageManager->enterAddDevicePage(config);
}

void RegistrationManager::addNewDeviceMultipleAccountOwner()
{
    QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(qApp);
    Q_ASSERT(app);

    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.firstSyncPending() })");

    QJSValue config = m_engine->newObject();
    config.setProperty("title", m_strings.property("addingNewDevice"));
    config.setProperty("contentText", m_strings.property("requestedAddingFollowingDevice"));
    config.setProperty("buttonText", m_strings.property("next"));
    config.setProperty("deviceType", app->deviceType());
    config.setProperty("deviceName", app->deviceName());
    config.setProperty("imageSrc", "qrc:/illustrations/r5-add-new-device.png");
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("inputLabel", m_strings.property(m_selectedConfirmViaEmaiOption ? "loginPassphrase" : "powerPassphrase"));
    config.setProperty("inputPlaceholder", m_strings.property(m_selectedConfirmViaEmaiOption ? "typeLoginPassphrase" : "typePowerPassphrase"));
    config.setProperty("deviceUUID", Utils::get()->uuid().toString(QUuid::WithoutBraces));

    m_pageManager->enterAddDevicePage(config);
}

void RegistrationManager::newDeviceAdded()
{
    QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(qApp);
    Q_ASSERT(app);

    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.firstSyncPending() })");

    QJSValue config = getConfig(false);
    config.setProperty("title", m_strings.property("congratsYouAddedDevice"));
    config.setProperty("deviceType", app->deviceType());
    config.setProperty("deviceName", app->deviceName());
    config.setProperty("buttonText", m_strings.property("continueTxt"));
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r6.png");

    m_pageManager->enterDeviceStatusPage(config);
}

void RegistrationManager::firstSyncPending()
{
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.firstSyncInProgress() })");
    QJSValue config = getConfig(false);
    m_wrongDeviceType = true;
    if(m_wrongDeviceType){
        config.setProperty("title", m_strings.property("wrongDeviceTypeTitle"));
        config.setProperty("content", m_strings.property("wrongDeviceTypeDescription"));
        config.setProperty("imageSrc", "qrc:/illustrations/r14.png");
        config.setProperty("type", Pages::None);
        m_pageManager->enterGenericInfoPopupPage(config);
        return;
    }

    config.setProperty("title", m_strings.property("firstSyncPending"));
    config.setProperty("content", m_strings.property("firstSyncPendingContent"));
    // TODO remove the button when backend will be integrated
    config.setProperty("type", Pages::Progress);
    config.setProperty("primaryButtonText", m_strings.property("cancel"));
    config.setProperty("primaryButtonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("finishedHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/u4.png");
    m_pageManager->enterGenericInfoPage(config);

    //this is for test
    QTimer::singleShot(5000, this, [this](){firstSyncInProgress();});

    // TODO: refactor this part ---->

    auto promise = QtPromise::connect(SyncManager::get(), &SyncManager::syncStarted);

    promise.then([=]() {
        if (SyncManager::get()->syncing()) {
            firstSyncInProgress();
        }
    });

    Error error;

    SyncManager::get()->sync(Account(m_account.get()), m_loginPassphrase, error);

    auto syncPromise = QtPromise::connect(SyncManager::get(), &SyncManager::syncFinished, SyncManager::get(), &SyncManager::error);

    syncPromise.then([=]() {
        enterHome();
    }).fail([=](const Error &e) {
        handleError(e);
    });
}

void RegistrationManager::firstSyncInProgress()
{
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.enterHome() })");
    QJSValue config = getConfig(false);
    config.setProperty("title", m_strings.property("firstSyncProgress"));
    config.setProperty("content", m_strings.property("firstSyncProgressContent"));
    config.setProperty("type", Pages::Progress);
    config.setProperty("primaryButtonText", m_strings.property("gotIt"));
    config.setProperty("primaryButtonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r8-first-sync-in-progress.png");

    m_pageManager->enterGenericInfoPage(config);
    QJSValue configTest = m_engine->newObject();
    configTest.setProperty("showSuccessfullyCompletedPopupPage", true);
    configTest.setProperty("title", "New device added!");
    configTest.setProperty("contentText", "First sync successfully completed, you device is now fully functional!");
    configTest.setProperty("imageSrc", "qrc:/illustrations/successfully-completed.png");
    QTimer::singleShot(5000, this, [=](){enterHome(configTest);});
}

/*!
 * \brief RegistrationManager::newDeviceAccountOwner
 * Starts Flow 3
 */
void RegistrationManager::newDeviceAccountOwner()
{
    QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(qApp);
    Q_ASSERT(app);

    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function(passphrase) { RegistrationManager.addNewAccountOwnerDevice(passphrase) })");

    QJSValue config = m_engine->newObject();
    config.setProperty("title", m_strings.property("welcomeBack"));
    config.setProperty("contentText", m_strings.property("logInDifferentDevice"));
    config.setProperty("buttonText", m_strings.property("addDevice"));
    config.setProperty("deviceType", app->deviceType());
    config.setProperty("deviceName", app->deviceName());
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("inputLabel", m_strings.property("powerPassphrase"));
    config.setProperty("inputPlaceholder", m_strings.property("typePowerPassphrase"));
    config.setProperty("imageSrc", "qrc:/illustrations/r13.png");

    m_pageManager->enterAddDevicePage(config);
}
/*!
 * \brief RegistrationManager::loginBlocked
 * Starts Flow 4
 */
void RegistrationManager::loginBlocked()
{
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { pageManager.back() })");
    QJSValue config = getConfig(false);
    config.setProperty("title", m_strings.property("loginBlocked"));
    config.setProperty("content", m_strings.property("loginBlockedContent"));
    config.setProperty("type", Pages::Single);
    config.setProperty("primaryButtonText", m_strings.property("gotIt"));
    config.setProperty("primaryButtonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r9.png");

    m_pageManager->enterGenericInfoPage(config);
}

void RegistrationManager::loginUnblocked(const QString& passphrase)
{
    m_powerPassphrase = passphrase;
    m_pageManager->enterPage(Pages::ResetPassphrasePage);
}

/*!
 * \brief RegistrationManager::deleteData
 * Method invoked when user decides to wipe the data
 */
void RegistrationManager::deleteData()
{

}

void RegistrationManager::resetPassphrase(const QString& passphrase)
{
    m_unblocking = true;
    Q_EMIT this->unblockingChanged();

    auto promise = QtPromise::resolve(AuthenticationManager::get()->unblock(m_account.get(), m_powerPassphrase, passphrase));

    promise.then([=]() {
        QQmlExpression handler(m_engine->rootContext(), nullptr,
                               "(function() { RegistrationManager.enterHome() })");

        QJSValue config = getConfig(false, true);
        config.setProperty("title", m_strings.property("passhraseHasBeenReset"));
        config.setProperty("content", m_strings.property("passhraseHasBeenResetContent"));
        config.setProperty("type", Pages::Single);
        config.setProperty("primaryButtonText", m_strings.property("goToLogIn"));
        config.setProperty("primaryButtonHandler", m_engine->toScriptValue(handler.evaluate()));
        config.setProperty("imageSrc", "qrc:/illustrations/r10.png");

        m_pageManager->enterGenericInfoPage(config);

        cleanup();
    }).fail([=](const ErrorException &e) {
        handleError(e.error());
    }).finally([=]() {
        m_unblocking = false;
        Q_EMIT this->unblockingChanged();
    });
}

void RegistrationManager::deviceBlockedAccountOwner()
{
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function(passphrase) { RegistrationManager.loginUnblocked(passphrase) })");

    QJSValue config = getConfig(false);
    config.setProperty("title", m_strings.property("deviceInactive"));
    config.setProperty("contentText", m_strings.property("enterPowerPassphrase"));
    config.setProperty("buttonText", m_strings.property("goToLogIn"));
    config.setProperty("labelText", m_strings.property("powerPassphrase"));
    config.setProperty("placeholderText", m_strings.property("typePowerPassphrase"));
    config.setProperty("isPassPhrase", true);
    config.setProperty("eyeIconVisible", true);
    config.setProperty("clearIconVisible", false);
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r16_r18.png");

    m_pageManager->enterGenericInputPage(config);
}

void RegistrationManager::deviceBlocked()
{
    QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(qApp);
    Q_ASSERT(app);

    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { pageManager.backToLogin() })");

    QJSValue config = getConfig(false);
    config.setProperty("title", m_strings.property("deviceInactiveInvitedUser"));

    // TODO find a gently way to apply text formatting like font color etc from C++
//    config.setProperty("content", m_strings.property("deviceInactiveContent"));
    config.setProperty("content", m_strings.property("deviceInactiveContent").toString().arg("helenamorrison@email.com"));
    config.setProperty("deviceType", app->deviceType());
    config.setProperty("deviceName", app->deviceName());
    config.setProperty("buttonText", m_strings.property("okGotIt"));
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r16_r18.png");

    m_pageManager->enterDeviceStatusPage(config);
}

void RegistrationManager::accountBlocked()
{
    QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(qApp);
    Q_ASSERT(app);

    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { pageManager.backToLogin() })");

    QJSValue config = getConfig(false);
    config.setProperty("title", m_strings.property("accountBlocked"));

    // TODO find a gently way to apply text formatting like font color etc from C++
//    config.setProperty("content", m_strings.property("deviceInactiveContent"));
    config.setProperty("content", m_strings.property("deviceInactiveContent").toString().arg("helenamorrison@email.com"));
    config.setProperty("deviceType", app->deviceType());
    config.setProperty("deviceName", app->deviceName());
    config.setProperty("buttonText", m_strings.property("okGotIt"));
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r17.jpg");

    m_pageManager->enterDeviceStatusPage(config);
}

void RegistrationManager::deviceActivationRejected()
{
    QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(qApp);
    Q_ASSERT(app);

    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { pageManager.backToLogin() })");

    QJSValue config = m_engine->newObject();
    config.setProperty("title", m_strings.property("addDeviceRejected"));
    config.setProperty("content", m_strings.property("addDeviceRejectedContent"));
    config.setProperty("deviceType", app->deviceType());
    config.setProperty("deviceName", app->deviceName());
    config.setProperty("buttonText", m_strings.property("okGotIt"));
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r14.png");

    m_pageManager->enterDeviceStatusPage(config);
}

void RegistrationManager::deviceActivationApproved()
{
    QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(qApp);
    Q_ASSERT(app);

    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { RegistrationManager.enterHome() })");

    QJSValue config = getConfig(false);
    config.setProperty("title", m_strings.property("requestApproved"));
    config.setProperty("deviceType", app->deviceType());
    config.setProperty("deviceName", app->deviceName());
    config.setProperty("buttonText", m_strings.property("continueTxt"));
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r13.png");

    m_pageManager->enterDeviceStatusPage(config);
}

void RegistrationManager::deviceActivationBlocked()
{
    QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(qApp);
    Q_ASSERT(app);

    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function() { pageManager.backToLogin() })");

    QJSValue config = m_engine->newObject();
    config.setProperty("title", m_strings.property("addDeviceBlocked"));
    config.setProperty("content", m_strings.property("addDeviceBlockedContent"));
    config.setProperty("deviceType", app->deviceType());
    config.setProperty("deviceName", app->deviceName());
    config.setProperty("buttonText", m_strings.property("okGotIt"));
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r15.png");

    m_pageManager->enterDeviceStatusPage(config);
}

void RegistrationManager::unblockDevice()
{
    // TODO SDK processing
    m_pageManager->backToLogin();
}

void RegistrationManager::switchAccount(const QString &email)
{
    qDebug() << "Switch account to" << email;
    m_pageManager->backToLogin();
    emit m_pageManager->setupLoginPage(email);
}

void RegistrationManager::enterReviewerEmail()
{
    QQmlExpression handler(m_engine->rootContext(), nullptr,
                           "(function(email) { RegistrationManager.enteredAccountEmail(email) })");

    QJSValue config = m_engine->newObject();
    config.setProperty("title", m_strings.property("emailValidated"));
    config.setProperty("contentText", m_strings.property("enterReviewer"));
    config.setProperty("buttonText", m_strings.property("sendRequest"));
    config.setProperty("labelText", m_strings.property("accountOwnerEmail"));
    config.setProperty("placeholderText", m_strings.property("typeAccountOwnerEmail"));
    config.setProperty("isPassPhrase", false);
    config.setProperty("eyeIconVisible", false);
    config.setProperty("clearIconVisible", false);
    config.setProperty("emailValidator", true);
    config.setProperty("validatorText", m_strings.property("emailIsNotValid"));
    config.setProperty("buttonHandler", m_engine->toScriptValue(handler.evaluate()));
    config.setProperty("imageSrc", "qrc:/illustrations/r21-email-validated.png");

    m_pageManager->enterGenericInputPage(config);
}

RegistrationManager::RegistrationType RegistrationManager::currentFlow() const
{
    return m_currentFlow;
}

const QString &RegistrationManager::loginPassphrase() const
{
    return m_loginPassphrase;
}

const QString &RegistrationManager::powerPassphrase() const
{
    return m_powerPassphrase;
}

QQuickItem *RegistrationManager::currentItem() const
{
    return m_currentItem;
}

void RegistrationManager::setCurrentItem(QQuickItem *item)
{
    if (m_currentItem == item) {
        return;
    }

    if (m_currentItem && (m_currentItem->metaObject()->indexOfProperty("busy") > -1)) {
        m_currentItem->setProperty("busy", false);
    }

    m_currentItem = item;
    Q_EMIT this->currentItemChanged();
}

bool RegistrationManager::registered() const
{
    return AccountManager::get()->allAccounts().count() > 0;
}

void RegistrationManager::registerStrings(int id)
{
    m_strings = m_engine->singletonInstance<QJSValue>(id);
    Q_ASSERT(!m_strings.isNull() && !m_strings.isUndefined());
}

void RegistrationManager::onRegistrationTokenReceived(const QString &token, const QString &email)
{
    setBusy(true);

    qDebug() << "token: " << token << "email:" << email;

    m_currentFlow = RegistrationType::NewAccount;

    m_registrationToken = token;
    m_accountOwnerEmail = email;

    termsPage();
}

void RegistrationManager::onAddingNewDeviceReceived()
{
    m_addingNewDeviceReceived = true;
    m_currentFlow = RegistrationType::AddNewDevice;
}

void RegistrationManager::setBusy(bool busy)
{
    Q_ASSERT(m_currentItem);

    if (m_currentItem->metaObject()->indexOfProperty("busy") < 0) {
        qWarning() << "No busy property exists";
        return;
    }

    m_currentItem->setProperty("busy", busy);
}

QJSValue RegistrationManager::getConfig(bool leftButton, bool rightButton)
{
    QJSValue config = m_engine->newObject();
    config.setProperty("leftButton", leftButton);
    config.setProperty("rightButton", rightButton);
    return config;
}

void RegistrationManager::checkEmail(const QString &email, const QString &loginPassphrase)
{
    qWarning() << "Obsolete";
}

void RegistrationManager::verifyEmail(const QString &token)
{
    qWarning() << "Obsolete";
}

void RegistrationManager::registration()
{
    setBusy(true);

    auto promise = QtPromise::resolve();

    promise.then([=]() {
        if (currentFlow() == RegistrationType::NewAccount) {
            return QtPromise::resolve(AuthenticationManager::get()->accountRegistration(m_registrationToken, m_accountName, m_accountOwnerEmail, m_loginPassphrase, m_powerPassphrase));
        }
        else if (currentFlow() == RegistrationType::JoinExistingAccount || currentFlow() == RegistrationType::AddNewDevice) {
            return QtPromise::resolve(AuthenticationManager::get()->deviceRegistration(m_account.get(), m_loginPassphrase, m_powerPassphrase));
        }
        else {
            qWarning() << currentFlow();
        }

        throw QtPromise::QPromiseCanceledException();
    }).then([=]() {
        if (currentFlow() == RegistrationType::NewAccount) {
            cleanup();

            inviteUsersPage();
        }
        else if (currentFlow() == RegistrationType::JoinExistingAccount || currentFlow() == RegistrationType::AddNewDevice) {
            firstSyncPending();

            cleanup();
        }
    }).fail([=](const ErrorException &e) {
        handleError(e.error());
    }).finally([=]() {

    });
}

void RegistrationManager::enteredAccountEmail(const QString &email)
{
    setBusy(true);

    if (currentFlow() == RegistrationType::JoinExistingAccount) {
        joinAccountPage(email);
    }
    else if (currentFlow() == RegistrationType::Login) {
        auto promise = QtPromise::resolve(AuthenticationManager::get()->accountCount(email));

        promise.then([=](int accountCount) {
            if (accountCount == 0) {
                QJSValue config = m_engine->newObject();
                config.setProperty("emailError", m_strings.property("emailIsIncorrect"));
                emailDoesNotExist(config);
            }
            else {
                Q_EMIT this->loginWithAccountOwnerEmail(email);
            }
        }).finally([=]() {

        });
    }
    else if (currentFlow() == RegistrationType::AddNewDevice) {
        m_accountOwnerEmail = email;
        checkEmail(m_deviceEmail, m_loginPassphrase);
    }
    else {
        qWarning() << currentFlow();
    }
}

void RegistrationManager::addNewAccountOwnerDevice(const QString &passphrase)
{
    Q_ASSERT(currentFlow() == RegistrationType::AddNewDevice);

    m_powerPassphrase = passphrase;
    registration();
}

void RegistrationManager::handleError(const Error &error)
{
    QQmlExpression primaryHandler(m_engine->rootContext(), nullptr,
                           "(function() { pageManager.backToLogin() })");
    QJSValue config = m_engine->newObject();
    config.setProperty("type", Pages::Single);
    config.setProperty("primaryButtonText", m_strings.property("gotIt"));
    config.setProperty("primaryButtonHandler", m_engine->toScriptValue(primaryHandler.evaluate()));

    if (error == Errors::TokenExpired) {
        config.setProperty("title", m_strings.property("linkExpired"));
        config.setProperty("content", m_strings.property("makeSureToValidateEmail"));
        config.setProperty("imageSrc", "qrc:/illustrations/r23.png");
    }
    else {
        config.setProperty("title", QString("Error: #%1").arg(error.code()));
        config.setProperty("content", error.message());
        config.setProperty("imageSrc", "qrc:/illustrations/u6.png");
    }

    m_pageManager->enterGenericInfoPage(config);
}

void RegistrationManager::cleanup()
{
    m_registrationToken.clear();
    m_accountName.clear();
    m_account.clear();
    m_accountOwnerEmail.clear();
    m_deviceEmail.clear();
    m_loginPassphrase.clear();
    m_powerPassphrase.clear();
}
