/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021 Test Ltd.                    *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QQmlContext>
#include <QTimer>
#include <QImage>

#include <QBSDK.h>

#include "quodarcaapplication.h"
#include "utility.h"
#include "pagemanager.h"
#include "homepagemanager.h"
#include "RegistrationManager.h"
#include "baselistmodel.h"
#include "arcalistmodel.h"
#include "quodlistmodel.h"
#include "filelistmodel.h"
#include "quodtypelistmodel.h"
#include "quodsectionsmodel.h"
#include "attributesectionmodel.h"
#include "quodfilelistmodel.h"
#include "associatedtaglistmodel.h"
#include "taglistmodel.h"
#include "syncableobjectlistmodel.h"
#include "devicelistmodel.h"
#include "arcaeditor.h"
#include "deviceeditor.h"
#include "usereditor.h"
#include <QQuickWindow>
#include "smartscale.h"
#include "photoimageprovider.h"
#include "viewerimageprovider.h"
#include "itemimageprovider.h"
#include "imageviewerimageprovider.h"
#include <QTextDocument>

#if defined(Q_OS_WINDOWS)
#include <QtWebView>
#endif
#include <QQuickStyle>

#include "ImportFileJob.h"
#include "SyncFileJob.h"

#include "pdfdocument.h"

#include "analytics.h"

#include <QmlVlc.h>
#include <QmlVlcMediaThumbnailer.h>
#include <QmlVlcWaveFormGenerator.h>

#include <QtPromise>


#include "httpServer/httpServer.h"

#include "requesthandler.h"

#include "logger.h"

void registerAppTypes() {
    qmlRegisterSingletonType<QuodArca::Utility>("com.test", 1, 0, "Utility", [](QQmlEngine *, QJSEngine *) -> QObject * {
        return QuodArca::Utility::get();
    });

    qmlRegisterSingletonType<ArcaListModel>("com.test", 1, 0, "ArcaListModel", [](QQmlEngine *, QJSEngine *) -> QObject * {
        return ArcaListModel::get();
    });

    qmlRegisterSingletonType<QuodListModel>("com.test", 1, 0, "QuodListModel", [](QQmlEngine *, QJSEngine *) -> QObject * {
        return QuodListModel::get();
    });

    qmlRegisterSingletonType<TagListModel>("com.test", 1, 0, "TagListModel", [](QQmlEngine *, QJSEngine *) -> QObject * {
        return TagListModel::get();
    });

    qmlRegisterSingletonType<QuodFileListModel>("com.test", 1, 0, "QuodFileListModel", [](QQmlEngine *, QJSEngine *) -> QObject * {
        return QuodFileListModel::get();
    });

    qmlRegisterSingletonType<SyncableObjectListModel>("com.test", 1, 0, "SyncableObjectListModel", [](QQmlEngine *, QJSEngine *) -> QObject * {
        return SyncableObjectListModel::get();
    });

    qmlRegisterType<BaseListModel>("com.test", 1, 0, "BaseListModel");

    qmlRegisterType<QuodTypeListModel>("com.test", 1, 0, "QuodTypeListModel");
    qmlRegisterType<QuodSectionsModel>("com.test", 1, 0, "QuodSectionsModel");
    qmlRegisterType<AttributeSectionModel>("com.test", 1, 0, "AttributeSectionModel");

    qmlRegisterType<AssociatedTagListModel>("com.test", 1, 0, "AssociatedTagListModel");

    qmlRegisterUncreatableType<Editor>("com.test", 1, 0, "Editor", "Attempt to create uncreatable object!");
    qmlRegisterType<HomePageManager>("com.test", 1, 0, "HomeManager");

    qmlRegisterType<ArcaEditor>("com.test", 1, 0, "ArcaEditor");

    qmlRegisterType<DeviceEditor>("com.test", 1, 0, "DeviceEditor");

    qmlRegisterType<UserEditor>("com.test", 1, 0, "UserEditor");

    qmlRegisterUncreatableType<ImportFileJob>("com.test", 1, 0, "ImportFileJob", "Attempt to create uncreatable object!");
    qmlRegisterUncreatableType<SyncFileJob>("com.test", 1, 0, "SyncFileJob", "Attempt to create uncreatable object!");
    qRegisterMetaType<QList<QUuid> >();
    qRegisterMetaType<QMultiHash<QString, QUuid> >();
}

int main(int argc, char *argv[])
{
    Logger::init();

#ifdef Q_OS_WINDOWS
    QGuiApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::PassThrough);
#endif

    QCoreApplication::setApplicationName(QStringLiteral("QuodArca"));
    QCoreApplication::setOrganizationName(QStringLiteral("Test Ltd."));
    QCoreApplication::setOrganizationDomain(QStringLiteral("test.com"));

#if defined(Q_OS_WINDOWS)
    QCoreApplication::addLibraryPath("./");
#elif defined(Q_OS_MACOS)
    QCoreApplication::addLibraryPath("./../Plugins/");
#endif

#if defined(Q_OS_WINDOWS)
    QtWebView::initialize();
#endif

    QuodArcaApplication app(argc, argv);

    QQuickStyle::setStyle("Basic");

#if defined(Q_OS_ANDROID)
    bool resourceLoaded = QResource::registerResource("assets:/illustrations/illustrations.rcc");
    qDebug() << "Loading illustrations..." << resourceLoaded;
    resourceLoaded = QResource::registerResource("assets:/pdfviewer/pdfviewer.rcc");
    qDebug() << "Loading pdfviewer..." << resourceLoaded;
    resourceLoaded = QResource::registerResource("assets:/mdeditor/mdeditor.rcc");
    qDebug() << "Loading markdown editor..." << resourceLoaded;
#elif defined(Q_OS_MACOS)
    bool resourceLoaded = QResource::registerResource(QDir(app.applicationDirPath() + "/../Resources/illustrations.rcc").absolutePath());
    qDebug() << "Loading illustrations..." << resourceLoaded;
    resourceLoaded = QResource::registerResource(QDir(app.applicationDirPath() + "/../Resources/pdfviewer.rcc").absolutePath());
    qDebug() << "Loading pdfviewer..." << resourceLoaded;
    resourceLoaded = QResource::registerResource(QDir(app.applicationDirPath() + "/../Resources/mdeditor.rcc").absolutePath());
    qDebug() << "Loading markdown editor..." << resourceLoaded;
#elif defined(Q_OS_WINDOWS) || defined(Q_OS_IOS)
    bool resourceLoaded = QResource::registerResource(QDir(app.applicationDirPath() + "/illustrations.rcc").absolutePath());
    qDebug() << "Loading illustrations..." << resourceLoaded;
    resourceLoaded = QResource::registerResource(QDir(app.applicationDirPath() + "/pdfviewer.rcc").absolutePath());
    qDebug() << "Loading pdfviewer..." << resourceLoaded;
    resourceLoaded = QResource::registerResource(QDir(app.applicationDirPath() + "/mdeditor.rcc").absolutePath());
    qDebug() << "Loading markdown editor..." << resourceLoaded;
#endif

    {
        HttpServerConfig config;
        config.port = 44387;
        config.requestTimeout = 20;
        config.responseTimeout = 20;
        config.verbosity = HttpServerConfig::Verbose::All;
        config.maxMultipartSize = 512 * 1024 * 1024;

        RequestHandler *handler = new RequestHandler();
        HttpServer *server = new HttpServer(config, handler);
        server->listen();
    }

#if defined (Q_OS_MACOS) || defined (Q_OS_WINDOWS)
    const bool portable = true; // Basically for debug purposes. All files will be in the same folder with exe file
#else
    const bool portable = false;
#endif

    const QString key = "bf57f435-02af-45a6-8e1c-86dc084434c4"; //"77d01ac7-1c15-44ab-ac77-b6a36636018b";

    if (!QBSDK::initialize(key, portable)) {
        return -1;
    }

    QBSDK::registerPreviewProvider(QuodArca::Utility::get()->supportedImageFormats(), [](auto device, auto format) {
        QImage img;
        if (img.load(device, format.toUtf8().constData())) {
            //img = img.scaled(200, 200, Qt::KeepAspectRatio, Qt::SmoothTransformation);
        }
        return img;
    });

    QBSDK::registerPreviewProvider(QuodArca::Utility::get()->supportedDocumentFormats(), [](auto device, auto format) {
        PdfDocument doc;
        doc.load(device, format);
        QImage img = doc.render(1, doc.pageSize(1)); // render first page only
        return img;
    });

    QBSDK::registerPreviewProvider(QuodArca::Utility::get()->supportedVideoFormats(), [](auto device, auto format) {
        QmlVlcMediaThumbnailer thumbnailer(device);

        auto p = QtPromise::connect(&thumbnailer, &QmlVlcMediaThumbnailer::ready);

        p.wait();

        return thumbnailer.thumbnail();
    });

    QBSDK::registerPreviewProvider(QuodArca::Utility::get()->supportedMarkdownFormats(), [](auto device, auto format) {
        QTextDocument document;
        document.setMarkdown(device->readAll());

        QImage image(QSize(600, 900), QImage::Format_ARGB32);
        image.fill(Qt::transparent);

        QPainter painter(&image);

        document.drawContents(&painter, QRectF(QPointF(), image.size()));

        return image;
    });

    QBSDK::registerPreviewProvider(QuodArca::Utility::get()->supportedAudioFormats(), [](auto device, auto format) {
        QmlVlcWaveFormGenerator waveFormGenerator(device);
        waveFormGenerator.setBackgroundColor("#F1F3F8");
        waveFormGenerator.setWaveBackgroundColor("#5C5AE8");
        waveFormGenerator.setWaveBorderColor("#5C5AE8");

//        auto p = QtPromise::connect(&waveFormGenerator, &QmlVlcWaveFormGenerator::ready);

//        waveFormGenerator.fetch();

//        p.wait();

//        return waveFormGenerator.image();
        return QImage();
    });

    QBSDK::registerMetadataProvider(QuodArca::Utility::get()->supportedImageFormats(), [](auto device, auto format) {
        QVariantMap metadata;
        QImage img;
        qDebug() << Q_FUNC_INFO << "METADATA PROVIDER" << format;
        if (!img.load(device, format.toUtf8().constData())) {
            qDebug() << Q_FUNC_INFO << "Unable to load" << format;
            return metadata;
        }
        qDebug() << Q_FUNC_INFO << "Metadata" << img.width() << img.height() << img.size();
        metadata["width"] = img.width();
        metadata["height"] = img.height();
        qDebug() << Q_FUNC_INFO << metadata;
        // etc...
        return metadata;
    });

    QBSDK::registerContentProvider(QuodArca::Utility::get()->supportedMarkdownFormats(), [](auto device, auto format) {
        QTextStream stream(device);
        return stream.readAll();
    });

    QObject::connect(&app, &QCoreApplication::aboutToQuit, &app, &QBSDK::finalize);

    Analytics::get()->init();

    QuodArca::Utility::loadFonts();

    registerAppTypes();

    RegisterQmlVlc();

    QmlVlcConfig& config = QmlVlcConfig::instance();
    config.enableAdjustFilter( true );
    config.enableMarqueeFilter( true );
    config.enableLogoFilter( true );
    config.enableDebug( true );
    config.enableHardwareAcceleration(true);
    config.setTrustedEnvironment(true);
#if defined (Q_OS_WINDOWS)
    config.setPluginPath(QDir(qApp->applicationDirPath() + "/plugins").absolutePath());
#elif defined (Q_OS_MACOS)
    config.setPluginPath(QDir(qApp->applicationDirPath() + "/../Frameworks/plugins").absolutePath());
#endif

    RebuildVlcPluginsCache();

    QQmlApplicationEngine engine;

    engine.addImageProvider(QStringLiteral("photo"), new PhotoImageProvider);
    engine.addImageProvider(QStringLiteral("viewer"), new ViewerImageProvider);
    engine.addImageProvider(QStringLiteral("item"), new ItemImageProvider);
    engine.addImageProvider(QStringLiteral("imageViewer"), new ImageViewerImageProvider);

    PageManager pageManager;
    pageManager.init(QStringLiteral("pageManager"), &engine, app.isTablet());

    RegistrationManager manager(&pageManager, &engine);
    QObject::connect(&app, &QuodArcaApplication::stringsRegistered, &manager, &RegistrationManager::registerStrings);

    HomePageManager *homePageManager = HomePageManager::get();
    homePageManager->init(QStringLiteral("homeManager"), &engine, app.isTablet());

    SmartScale smartScale(&engine);

#if defined(Q_OS_MACOS) || defined(Q_OS_WINDOWS)
    app.initQML(&engine, QStringLiteral("qrc:/SplashScreen.qml"));

    QuodArcaApplication::connect(&app, &QuodArcaApplication::loaded, [&app]() {
        app.engine()->rootObjects().first()->deleteLater();
        app.engine()->load(QStringLiteral("qrc:/main.qml"));
        emit app.closeSplash();
    });
#else
    app.initQML(&engine, "qrc:/main.qml");
#endif
    app.handleCommandLine(app.arguments());

    QTimer::singleShot(1000, &app, &QuodArcaApplication::loaded);

    return app.exec();
}
