#ifndef DOCUMENTEDITOR_H
#define DOCUMENTEDITOR_H

#include <QObject>
#include <QQuickTextDocument>
#include <QTextCursor>
#include <QUrl>

class QTextDocument;

class DocumentEditor : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQuickTextDocument *document READ document WRITE setDocument NOTIFY documentChanged)
    Q_PROPERTY(int cursorPosition READ cursorPosition WRITE setCursorPosition NOTIFY cursorPositionChanged)
    Q_PROPERTY(int selectionStart READ selectionStart WRITE setSelectionStart NOTIFY selectionStartChanged)
    Q_PROPERTY(int selectionEnd READ selectionEnd WRITE setSelectionEnd NOTIFY selectionEndChanged)

    Q_PROPERTY(QString defaultFontFamily READ defaultFontFamily WRITE setDefaultFontFamily NOTIFY defaultFontFamilyChanged)
    Q_PROPERTY(int defaultFontSize READ defaultFontSize WRITE setDefaultFontSize NOTIFY defaultFontSizeChanged)

    Q_PROPERTY(QColor textColor READ textColor WRITE setTextColor NOTIFY textColorChanged)
    Q_PROPERTY(QString fontFamily READ fontFamily WRITE setFontFamily NOTIFY fontFamilyChanged)
    Q_PROPERTY(int fontSize READ fontSize WRITE setFontSize NOTIFY fontSizeChanged)
    Q_PROPERTY(Qt::Alignment alignment READ alignment WRITE setAlignment NOTIFY alignmentChanged)

    Q_PROPERTY(bool bold READ bold WRITE setBold NOTIFY boldChanged)
    Q_PROPERTY(bool italic READ italic WRITE setItalic NOTIFY italicChanged)
    Q_PROPERTY(bool underline READ underline WRITE setUnderline NOTIFY underlineChanged)
    Q_PROPERTY(bool strike READ strike WRITE setStrike NOTIFY strikeChanged)
    Q_PROPERTY(Heading heading READ heading WRITE setHeading NOTIFY headingChanged)
    Q_PROPERTY(bool bulletList READ bulletList WRITE setBulletList NOTIFY listChanged)
    Q_PROPERTY(bool numberedList READ numberedList WRITE setNumberedList NOTIFY listChanged)
    Q_PROPERTY(bool anchor READ anchor WRITE setAnchor NOTIFY anchorChanged)
    Q_PROPERTY(bool codeBlock READ codeBlock WRITE setCodeBlock NOTIFY codeBlockChanged)

    Q_PROPERTY(bool horizontalLine READ horizontalLine WRITE setHorizontalLine NOTIFY horizontalLineChanged)


    Q_PROPERTY(QString fileName READ fileName NOTIFY fileUrlChanged)
    Q_PROPERTY(QString fileType READ fileType NOTIFY fileUrlChanged)
    Q_PROPERTY(QUrl fileUrl READ fileUrl NOTIFY fileUrlChanged)

    Q_PROPERTY(bool modified READ modified WRITE setModified NOTIFY modifiedChanged)

public:
    struct HeadingStyle{
        qreal pointSize;
    };
    enum Heading {
        H1, H2, H3, H4, H5, H6, None
    };
    Q_ENUM(Heading);

    explicit DocumentEditor(QObject *parent = nullptr);

    // Properties
    QQuickTextDocument *document() const;
    void setDocument(QQuickTextDocument *document);

    int cursorPosition() const;
    void setCursorPosition(int position);

    int selectionStart() const;
    void setSelectionStart(int position);

    int selectionEnd() const;
    void setSelectionEnd(int position);

    QString fontFamily() const;
    void setFontFamily(const QString &family);

    QString defaultFontFamily() const;
    void setDefaultFontFamily(const QString &family);

    int defaultFontSize() const;
    void setDefaultFontSize(int size);

    QColor textColor() const;
    void setTextColor(const QColor &color);

    Qt::Alignment alignment() const;
    void setAlignment(Qt::Alignment alignment);

    bool bold() const;
    void setBold(bool bold);

    bool italic() const;
    void setItalic(bool italic);

    bool underline() const;
    void setUnderline(bool underline);

    bool strike() const;
    void setStrike(bool strike);

    int fontSize() const;
    void setFontSize(int size);

    QString fileName() const;
    QString fileType() const;
    QUrl fileUrl() const;

    bool modified() const;
    void setModified(bool m);

    Heading heading() const;
    void setHeading(Heading heading);

    bool bulletList() const;
    void setBulletList(bool bulletList);

    bool numberedList() const;
    void setNumberedList(bool numberedList);

    bool anchor() const;
    void setAnchor(bool anchor);

    bool horizontalLine() const;
    void setHorizontalLine(bool line);

    Q_INVOKABLE void addAnchor(const QString& href);
    Q_INVOKABLE void preformatAnchor(bool clear = false);

    Q_INVOKABLE void toHTML();

    bool codeBlock() const;
    void setCodeBlock(bool codeBlock);

signals:
    // Properties signals
    void documentChanged();
    void cursorPositionChanged();
    void selectionStartChanged();
    void selectionEndChanged();

    void fontFamilyChanged();
    void textColorChanged();
    void alignmentChanged();

    void boldChanged();
    void italicChanged();
    void underlineChanged();
    void strikeChanged();

    void fontSizeChanged();

    void textChanged();
    void fileUrlChanged();

    void loaded(const QString &text);
    void error(const QString &message);

    void modifiedChanged();

    void headingChanged();
    void listChanged();
    void anchorChanged();
    void horizontalLineChanged();
    void codeBlockChanged();
    void defaultFontFamilyChanged();
    void defaultFontSizeChanged();
private:
    void reset();
    QTextCursor textCursor() const;
    QTextDocument *textDocument() const;
    void mergeFormatOnWordOrSelection(const QTextCharFormat &format);
    Heading headingFromPointSize(qreal pointSize) const;
    bool isListBlock() const;
    bool isListBlock(QTextListFormat::Style style) const;
    void setListBlock(bool insert, QTextListFormat::Style style);
    void clearPreformatting();

    QQuickTextDocument *m_document;

    QPair<int, int> m_preformattedAnchor;

    int m_cursorPosition;
    int m_selectionStart;
    int m_selectionEnd;

    QFont m_font;
    QUrl m_fileUrl;
};

#endif // DOCUMENTEDITOR_H
