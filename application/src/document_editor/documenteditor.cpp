#include "documenteditor.h"
#include <QtQml>
#include <QFile>
#include <QFileInfo>
#include <QFileSelector>
#include <QQmlFile>
#include <QQmlFileSelector>
#include <QQuickTextDocument>
#include <QTextCharFormat>
#include <QTextDocument>
#include <QTextBlock>
#include <QTextList>



static QString linkStyle = "style=\"background:#1A5C5AE8; color:#030303;font-weight: normal;\"";
static QString codeBlockStyle = "style=\"font-size:14px;font-weight:600;padding:12px;color:#5C5AE8;background: #F6F7F9;border:1px solid #ECEEF3;border-radius: 4px;\"";

static QMap<DocumentEditor::Heading, DocumentEditor::HeadingStyle> headings {
    {DocumentEditor::Heading::H1, DocumentEditor::HeadingStyle{40}},
    {DocumentEditor::Heading::H2, DocumentEditor::HeadingStyle{21}},
    {DocumentEditor::Heading::H3, DocumentEditor::HeadingStyle{18}},
    {DocumentEditor::Heading::H4, DocumentEditor::HeadingStyle{16}},
    {DocumentEditor::Heading::H5, DocumentEditor::HeadingStyle{12}},
    {DocumentEditor::Heading::H6, DocumentEditor::HeadingStyle{9}}
};

DocumentEditor::DocumentEditor(QObject *parent)
    : QObject(parent)
    , m_document(nullptr)
    , m_cursorPosition(-1)
    , m_selectionStart(0)
    , m_selectionEnd(0)
{
}

QQuickTextDocument *DocumentEditor::document() const
{
    return m_document;
}

void DocumentEditor::setDocument(QQuickTextDocument *document)
{
    if (document == m_document)
        return;

    if (m_document)
        m_document->textDocument()->disconnect(this);
    m_document = document;
    if (m_document)
        connect(m_document->textDocument(), &QTextDocument::modificationChanged, this, &DocumentEditor::modifiedChanged);
    emit documentChanged();
}

int DocumentEditor::cursorPosition() const
{
    return m_cursorPosition;
}

void DocumentEditor::setCursorPosition(int position)
{
    if (position == m_cursorPosition)
        return;

    m_cursorPosition = position;
    reset();
    emit cursorPositionChanged();
}

int DocumentEditor::selectionStart() const
{
    return m_selectionStart;
}

void DocumentEditor::setSelectionStart(int position)
{
    if (position == m_selectionStart)
        return;

    m_selectionStart = position;
    emit selectionStartChanged();
}

int DocumentEditor::selectionEnd() const
{
    return m_selectionEnd;
}

void DocumentEditor::setSelectionEnd(int position)
{
    if (position == m_selectionEnd)
        return;

    m_selectionEnd = position;
    emit selectionEndChanged();
}

QString DocumentEditor::fontFamily() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return QString();
    QTextCharFormat format = cursor.charFormat();
    return format.font().family();
}

void DocumentEditor::setFontFamily(const QString &family)
{
    QTextCharFormat format;
    format.setFontFamily(family);
    mergeFormatOnWordOrSelection(format);
    emit fontFamilyChanged();
}

QString DocumentEditor::defaultFontFamily() const
{
    if (m_document) {
        return m_document->textDocument()->defaultFont().family();
    }
    return {};
}

void DocumentEditor::setDefaultFontFamily(const QString &family)
{
    if (m_document) {
        auto font = m_document->textDocument()->defaultFont();
        font.setFamily(family);
        m_document->textDocument()->setDefaultFont(font);
    }
    emit defaultFontFamilyChanged();
}

int DocumentEditor::defaultFontSize() const
{
    if (m_document) {
        return m_document->textDocument()->defaultFont().pixelSize();
    }
    return {};
}

void DocumentEditor::setDefaultFontSize(int size)
{
    qDebug() << "Default point size" << size;
    if (m_document) {
        auto font = m_document->textDocument()->defaultFont();
        font.setPixelSize(size);
        m_document->textDocument()->setDefaultFont(font);
    }
    emit defaultFontSizeChanged();
}

QColor DocumentEditor::textColor() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return QColor(Qt::black);
    QTextCharFormat format = cursor.charFormat();
    return format.foreground().color();
}

void DocumentEditor::setTextColor(const QColor &color)
{
    QTextCharFormat format;
    format.setForeground(QBrush(color));
    mergeFormatOnWordOrSelection(format);
    emit textColorChanged();
}

Qt::Alignment DocumentEditor::alignment() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return Qt::AlignLeft;
    return textCursor().blockFormat().alignment();
}

void DocumentEditor::setAlignment(Qt::Alignment alignment)
{
    QTextBlockFormat format;
    format.setAlignment(alignment);
    QTextCursor cursor = textCursor();
    cursor.mergeBlockFormat(format);
    emit alignmentChanged();
}

bool DocumentEditor::bold() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontWeight() == QFont::Bold;
}

void DocumentEditor::setBold(bool bold)
{
    QTextCharFormat format;
    format.setFontWeight(bold ? QFont::Bold : QFont::Normal);
    mergeFormatOnWordOrSelection(format);
    emit boldChanged();
}

bool DocumentEditor::italic() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontItalic();
}

void DocumentEditor::setItalic(bool italic)
{
    QTextCharFormat format;
    format.setFontItalic(italic);
    mergeFormatOnWordOrSelection(format);
    emit italicChanged();
}

bool DocumentEditor::underline() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontUnderline();
}

void DocumentEditor::setUnderline(bool underline)
{
    QTextCharFormat format;
    format.setFontUnderline(underline);
    mergeFormatOnWordOrSelection(format);
    emit underlineChanged();
}

int DocumentEditor::fontSize() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return 0;
    QTextCharFormat format = cursor.charFormat();
    return format.font().pointSize();
}

void DocumentEditor::setFontSize(int size)
{
    if (size <= 0)
        return;

    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;

    if (!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);

    if (cursor.charFormat().property(QTextFormat::FontPointSize).toInt() == size)
        return;

    QTextCharFormat format;
    format.setFontPointSize(size);
    mergeFormatOnWordOrSelection(format);
    emit fontSizeChanged();
}

QString DocumentEditor::fileName() const
{
    const QString filePath = QQmlFile::urlToLocalFileOrQrc(m_fileUrl);
    const QString fileName = QFileInfo(filePath).fileName();
    if (fileName.isEmpty())
        return QStringLiteral("untitled.txt");
    return fileName;
}

QString DocumentEditor::fileType() const
{
    return QFileInfo(fileName()).suffix();
}

QUrl DocumentEditor::fileUrl() const
{
    return m_fileUrl;
}

void DocumentEditor::reset()
{
    emit fontFamilyChanged();
    emit alignmentChanged();
    emit boldChanged();
    emit italicChanged();
    emit underlineChanged();
    emit fontSizeChanged();
    emit textColorChanged();
    emit strikeChanged();
    emit headingChanged();
    emit listChanged();
    emit anchorChanged();
    emit horizontalLineChanged();
    emit codeBlockChanged();
}

QTextCursor DocumentEditor::textCursor() const
{
    QTextDocument *doc = textDocument();
    if (!doc)
        return QTextCursor();

    QTextCursor cursor = QTextCursor(doc);
    if (m_selectionStart != m_selectionEnd) {
        cursor.setPosition(m_selectionStart);
        cursor.setPosition(m_selectionEnd, QTextCursor::KeepAnchor);
    } else {
        cursor.setPosition(m_cursorPosition);
    }
    return cursor;
}

QTextDocument *DocumentEditor::textDocument() const
{
    if (!m_document)
        return nullptr;

    return m_document->textDocument();
}

void DocumentEditor::mergeFormatOnWordOrSelection(const QTextCharFormat &format)
{
    QTextCursor cursor = textCursor();
    if (!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);
    cursor.mergeCharFormat(format);
}

bool DocumentEditor::modified() const
{
    return m_document && m_document->textDocument()->isModified();
}

void DocumentEditor::setModified(bool m)
{
    if (m_document)
        m_document->textDocument()->setModified(m);
}

DocumentEditor::Heading DocumentEditor::heading() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return None;
    return headingFromPointSize(textCursor().charFormat().fontPointSize());
}

void DocumentEditor::setHeading(Heading heading)
{
    QTextCharFormat format;
    if (heading == Heading::None)
        format.setFontPointSize(defaultFontSize());
    else
        format.setFontPointSize(headings[heading].pointSize);
    mergeFormatOnWordOrSelection(format);
    emit headingChanged();
}

bool DocumentEditor::bulletList() const
{
    return isListBlock(QTextListFormat::ListDisc);
}

void DocumentEditor::setBulletList(bool bulletList)
{
    setListBlock(bulletList, QTextListFormat::ListDisc);
    emit listChanged();
}

bool DocumentEditor::numberedList() const
{
    return isListBlock(QTextListFormat::ListDecimal);
}

void DocumentEditor::setNumberedList(bool numberedList)
{
    setListBlock(numberedList, QTextListFormat::Style::ListDecimal);
    emit listChanged();
}

bool DocumentEditor::anchor() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().isAnchor();
}

void DocumentEditor::setAnchor(bool anchor)
{
    QTextCharFormat format;
    format.setAnchor(anchor);
    format.setAnchorHref("http://google.com");
    //format.setAnchorName("Google!");
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;
    cursor.insertHtml("<a href=\"www.google.com\" >Google</a>");
    mergeFormatOnWordOrSelection(format);
    emit anchorChanged();
}

void DocumentEditor::addAnchor(const QString &href)
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;

    if(!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);

    QString anchorName = cursor.selectedText();
    cursor.removeSelectedText();
    cursor.insertHtml(QStringLiteral("<a %3 href=\"%1\">%2</a>")
                      .arg(href, anchorName, linkStyle));
    emit anchorChanged();
}

void DocumentEditor::preformatAnchor(bool clear)
{
    if (clear) {
        clearPreformatting();
        return;
    }

    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;

    if(!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);

    m_preformattedAnchor = {cursor.selectionStart(), cursor.selectionEnd()};
    QTextCharFormat format;
    format.setBackground(QBrush(QColor("#1A5C5AE8")));
    mergeFormatOnWordOrSelection(format);
}

void DocumentEditor::toHTML()
{
    if (m_document) {
        qDebug("%s", qPrintable(m_document->textDocument()->toHtml()));
    }
}

bool DocumentEditor::codeBlock() const
{
    return false;
}

// TODO fix this. The formatting is wrong (padding and border propertied does not work correctly
void DocumentEditor::setCodeBlock(bool codeBlock)
{
    Q_UNUSED(codeBlock)
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;
    if (cursor.hasSelection()) {
        //TODO check if its codeblock already
        QString selectedText = cursor.selectedText();
        cursor.removeSelectedText();
        cursor.insertBlock();
        cursor.insertHtml(QStringLiteral("<div %2>%1</div><br>").arg(selectedText, codeBlockStyle));
    } else {
        cursor.insertHtml(QStringLiteral("<div %2></div><br>").arg(codeBlockStyle));
    }
    emit codeBlockChanged();
}

bool DocumentEditor::horizontalLine() const
{
    return false;
}


// TODO seems that <hr> is not working correctly.
void DocumentEditor::setHorizontalLine(bool line)
{
    Q_UNUSED(line)
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;
    cursor.insertHtml("<hr width=\"100%\">");
    emit horizontalLineChanged();
}


bool DocumentEditor::strike() const {
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontStrikeOut();
}

void DocumentEditor::setStrike(bool strike)
{
    QTextCharFormat format;
    format.setFontStrikeOut(strike);
    mergeFormatOnWordOrSelection(format);
    emit strikeChanged();
}

DocumentEditor::Heading  DocumentEditor::headingFromPointSize(qreal pointSize) const {
    auto it = std::find_if(headings.begin(), headings.end(), [&](const HeadingStyle& style) {
        return style.pointSize == pointSize;
    });

    if (it != headings.end()) {
        return it.key();
    }
    return DocumentEditor::Heading::None;
}

bool DocumentEditor::isListBlock() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    if (cursor.block().isValid()) {
        if (auto textList = cursor.block().textList()) {
            return true;
        }
    }
    return false;
}

bool DocumentEditor::isListBlock(QTextListFormat::Style style) const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    if (cursor.block().isValid()) {
        if (auto textList = cursor.block().textList()) {
            return textList->format().style() == style;
        }
    }
    return false;
}

void DocumentEditor::setListBlock(bool insert, QTextListFormat::Style style)
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;

    if (insert) {
        if (isListBlock()) {
            auto format = cursor.block().textList()->format();
            format.setStyle(style);
            cursor.block().textList()->setFormat(format);
        } else {
            QTextListFormat listFormat;
            listFormat.setStyle(style);
            cursor.insertList(listFormat);
        }
    } else {
        QTextList* currentList = cursor.currentList();
        if (currentList) {
            QTextBlock currentBlock = cursor.block();
            if (currentBlock.isValid()) {
                currentList->remove(currentBlock);
                QTextBlockFormat blockFormat = cursor.blockFormat();
                blockFormat.setIndent(0);
                cursor.setBlockFormat(blockFormat);
            }
        }
    }
}

void DocumentEditor::clearPreformatting()
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;

    const int lastPosition = cursor.position();

    cursor.setPosition(m_preformattedAnchor.first);
    cursor.setPosition(m_preformattedAnchor.second, QTextCursor::MoveMode::KeepAnchor);

    QTextCharFormat format = cursor.charFormat();
    format.clearBackground();
    cursor.setCharFormat(format);
    cursor.setPosition(lastPosition);
}

void registerDocumentEditorTypes() {
    qmlRegisterType<DocumentEditor>("DocumentEditor", 1, 0, "DocumentEditor");
}

Q_COREAPP_STARTUP_FUNCTION(registerDocumentEditorTypes)
