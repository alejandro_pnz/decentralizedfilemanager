#include "smartscale.h"
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QQuickItem>
#include <QQmlContext>
#include "utility.h"

#ifdef Q_OS_WINDOWS
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QtPlatformHeaders/QWindowsWindowFunctions>
#endif
#include <windows.h>
#include <winuser.h>
#endif
#include <QElapsedTimer>

SmartScale::SmartScale(QQmlApplicationEngine *engine, QObject *parent) : QObject(parent),
    m_engine(engine), m_minHeight(600), m_minWidth(900)
{
    m_engine->rootContext()->setContextProperty("smartScale", this);
    calculateSplashScreenSizeAndScale();
}

void SmartScale::init(QQuickWindow* window)
{
#ifdef Q_OS_WINDOWS
    if( window ) {
        if (m_window) {
            disconnect(m_window, &QQuickWindow::screenChanged, this,
                       &SmartScale::handleScreenChange);
        }
        m_window = window;
        m_currentScreen = window->screen();
        connect(m_window, &QQuickWindow::screenChanged, this,
                &SmartScale::handleScreenChange, Qt::QueuedConnection);
        #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        QWindowsWindowFunctions::setWindowActivationBehavior(QWindowsWindowFunctions::AlwaysActivateWindow);
        #endif
        // Initialy update the scale of the window to be sure that we can see the preffered size
        updateScale(true);
    }
#else
    Q_UNUSED(window)
#endif
}

void SmartScale::zoomIn()
{
    auto currentScaleFactor = QuodArca::Utility::get()->get_scaleFactor();
    auto scaleCandidate = static_cast<qreal>(qFloor(currentScaleFactor * 100) + s_step) / 100.0;
    QuodArca::Utility::get()->set_scaleFactor(qMin(scaleCandidate, s_maxScale));
}

void SmartScale::zoomOut()
{
    auto currentScaleFactor = QuodArca::Utility::get()->get_scaleFactor();
    auto scaleCandidate = static_cast<qreal>(qFloor(currentScaleFactor * 100) - s_step) / 100.0;
    QuodArca::Utility::get()->set_scaleFactor(qMax(scaleCandidate, s_minScale));
}

void SmartScale::calculateSplashScreenSizeAndScale()
{
    QSize windowSize{1440, 900}; // Reference size for splash screen
#ifdef Q_OS_WINDOWS
    auto s = qApp->screenAt(QCursor::pos());
    if (!s) {
        s = qApp->primaryScreen();
    }
    auto availableSize = s->availableSize();

    qreal scaleFactor = 1.0; // Default zoom factor
    int availableHeightForContent = availableSize.height();
    if ( availableHeightForContent < windowSize.height()) {
        // The available size on this screen is less than the reference
        // So to be able to swo the reference design we have to calculate the screen factor
        // for this screen

        // Calculate window size
        windowSize.setHeight(availableHeightForContent);
        windowSize.setWidth(availableHeightForContent * 1.6);
        qDebug() << Q_FUNC_INFO << "set sf to" << availableHeightForContent / 900.0;
        scaleFactor = availableHeightForContent / 900.0;

    } else {
        qDebug() << Q_FUNC_INFO << "back sf to 1.0";
    }
    QuodArca::Utility::get()->set_scaleFactor(scaleFactor);
#endif
    set_width(windowSize.width());
    set_height(windowSize.height());
}

void SmartScale::updateScale(bool centerWindow)
{
    auto s = m_currentScreen;
    auto top_border_with_title_bar = titleBarSize();

    qDebug() << Q_FUNC_INFO << "screen size" << s->availableSize()
             << "DPR" << s->devicePixelRatio() << "windId" << m_window->winId()
             << "title bar size" << top_border_with_title_bar;
    auto availableSize = s->availableSize();

    qreal scaleFactor = 1.0; // Default zoom factor
    QSize windowSize{1440, 900}; // Reference size
    int availableHeightForContent = availableSize.height() - top_border_with_title_bar;
    if ( availableHeightForContent < windowSize.height()) {
        // The available size on this screen is less than the reference
        // So to be able to swo the reference design we have to calculate the screen factor
        // for this screen

        // Calculate window size
        windowSize.setHeight(availableHeightForContent);
        windowSize.setWidth(availableHeightForContent * 1.6);
        qDebug() << Q_FUNC_INFO << "set sf to" << availableHeightForContent / 900.0;
        scaleFactor = availableHeightForContent / 900.0;

        // In some cases the calculated size might be less than initial minimum height.
        // So this property needs update.
        if (windowSize.height() < m_window->minimumHeight()) {
            m_window->setMinimumHeight(windowSize.height());
            set_minHeight(windowSize.height());
        }
    } else {
        qDebug() << Q_FUNC_INFO << "back sf to 1.0";
    }

    qDebug() << Q_FUNC_INFO << "Final window size" << windowSize << top_border_with_title_bar;

    // Center the window
    if (centerWindow) {
        // Qt6 TODO: QStyle
        auto alignedRect = [](Qt::LayoutDirection direction, Qt::Alignment alignment, const QSize &size, const QRect &rectangle) -> QRect {
            if (!(alignment & Qt::AlignHorizontal_Mask))
                alignment |= Qt::AlignLeft;
            if (!(alignment & Qt::AlignAbsolute) && (alignment & (Qt::AlignLeft | Qt::AlignRight))) {
                if (direction == Qt::RightToLeft)
                    alignment ^= (Qt::AlignLeft | Qt::AlignRight);
                alignment |= Qt::AlignAbsolute;
            }

            int x = rectangle.x();
            int y = rectangle.y();
            int w = size.width();
            int h = size.height();
            if ((alignment & Qt::AlignVCenter) == Qt::AlignVCenter)
                y += rectangle.size().height()/2 - h/2;
            else if ((alignment & Qt::AlignBottom) == Qt::AlignBottom)
                y += rectangle.size().height() - h;
            if ((alignment & Qt::AlignRight) == Qt::AlignRight)
                x += rectangle.size().width() - w;
            else if ((alignment & Qt::AlignHCenter) == Qt::AlignHCenter)
                x += rectangle.size().width()/2 - w/2;
            return QRect(x, y, w, h);
        };

        auto centeredRect = alignedRect(Qt::LeftToRight, Qt::AlignCenter, windowSize, s->availableGeometry());
        qDebug() << Q_FUNC_INFO << centeredRect << windowSize << s->availableGeometry();
        m_window->setGeometry(centeredRect.x(), centeredRect.y() + top_border_with_title_bar * 0.5,
                              centeredRect.width(), centeredRect.height());
    } else {
        m_window->setWidth(windowSize.width());
        m_window->setHeight(windowSize.height());
    }

    QuodArca::Utility::get()->set_scaleFactor(scaleFactor);
    m_window->requestUpdate();
    m_window->requestActivate();

    qDebug() << Q_FUNC_INFO << m_window->width() << m_window->height() << scaleFactor;

    set_desc(QString("%1x%2 available size %3 dpr, %4 title. AppSize %5x%6")
             .arg(s->availableSize().width()).arg(s->availableSize().height())
             .arg(s->devicePixelRatio()).arg(top_border_with_title_bar)
             .arg(m_window->width()).arg(m_window->height()));
}

int SmartScale::titleBarSize()
{
#ifdef Q_OS_WINDOWS
    if (!m_window || !m_currentScreen) return 0;
    auto s = m_currentScreen;
    HWND hwnd = (HWND)m_window->winId();
    RECT wrect;
    GetWindowRect( hwnd, &wrect );
    RECT crect;
    GetClientRect( hwnd, &crect );
    POINT lefttop = { crect.left, crect.top }; // Practicaly both are 0
    ClientToScreen( hwnd, &lefttop );
    POINT rightbottom = { crect.right, crect.bottom };
    ClientToScreen( hwnd, &rightbottom );

    int top_border_with_title_bar = (lefttop.y - wrect.top) / s->devicePixelRatio() ; // There is no transparent part
    return top_border_with_title_bar;
#endif
    return 0;
}

void SmartScale::handleScreenChange(QScreen*)
{
    qDebug() << Q_FUNC_INFO << m_window->screen()->availableSize();

    auto newScreen = m_window->screen();

    if (m_currentScreen) {
        disconnect(m_currentScreen, &QScreen::availableGeometryChanged, this, &SmartScale::handleAvailableGeometryChanged);
    }

    m_currentScreen = newScreen;
    connect(m_currentScreen, &QScreen::availableGeometryChanged, this, &SmartScale::handleAvailableGeometryChanged);
}

void SmartScale::handleAvailableGeometryChanged(QRect)
{
    updateScale(false);
}
