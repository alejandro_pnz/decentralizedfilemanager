/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef QUODARCAAPPLICATION_H
#define QUODARCAAPPLICATION_H

#include <QGuiApplication>
#ifdef Q_OS_ANDROID
#include <QJniEnvironment>
#endif

#if (defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)) || defined(Q_OS_MACOS) || defined(Q_OS_WINDOWS)
    #define USE_SINGLE_APPLICATION
    #include <SingleApplication>
#endif

#include "enums.h"
#include "clipboard.h"
#include <QPointer>

class QQuickWindow;
class SafeMargins;

class QQmlApplicationEngine;

#if defined(USE_SINGLE_APPLICATION)
using AppClassTemplate = SingleApplication;
#else
using AppClassTemplate = QGuiApplication;
#endif

class CapsLockDetector : public QObject
{
    Q_OBJECT

public:
    CapsLockDetector(QObject *parent = nullptr);
    ~CapsLockDetector();

signals:
    void capsLockStatusChanged();

protected:
    bool eventFilter(QObject *watched, QEvent *event) override;

    bool isCapsLockOn() const;

private:
    bool m_capsLockOn{false};

    friend class QuodArcaApplication;
};

class QuodArcaApplication : public AppClassTemplate
{
    Q_OBJECT
    Q_PROPERTY(bool isMobile READ isMobile CONSTANT)
    Q_PROPERTY(bool isTablet READ isTablet CONSTANT)
    Q_PROPERTY(bool isPhone READ isPhone CONSTANT)
    Q_PROPERTY(bool capsLockOn READ capsLockOn NOTIFY capsLockStatusChanged)
    Q_PROPERTY(Clipboard* clipboard READ clipboard CONSTANT)
    Q_PROPERTY(QString logs READ logs NOTIFY logsChanged)

public:
    QuodArcaApplication(int &argc, char **argv);
    ~QuodArcaApplication();

    void initQML(QQmlApplicationEngine *engine, const QString& mainQmlFile);

    void handleCommandLine(const QStringList &arguments);

    bool isMobile() const;

    bool isTablet() const;

    bool isPhone() const;

    bool capsLockOn() const;

    QuodArca::Enums::DeviceType deviceType() const;

    QString deviceName() const;

    Q_INVOKABLE QString companyWebsite() const;

    Q_INVOKABLE QString generatePassword(int type, quint16 length, bool specialChars);

    Q_INVOKABLE QSize mainWindowSize() const;
    Q_INVOKABLE void activateMainWindow() const;

    QQmlApplicationEngine *engine() const;
    Clipboard *clipboard() const;

    QString logs() const;
    void appendLogs(const QString &logs);

signals:
    void loaded();
    void closeSplash();
    void stringsRegistered(int id);
    void capsLockStatusChanged();
    void logsChanged();

protected:
    bool event(QEvent *e) override;

    void initLocalizationSupport();

#if defined(Q_OS_WINDOWS)
    void registerUrlHandler();
#endif

private slots:
    void receiveMessage(quint32 instanceId, const QByteArray &message);

private:    
    QPointer<QQmlApplicationEngine> m_engine;

    CapsLockDetector *m_detector{};
    SafeMargins *m_safeMargins{};
    Clipboard *m_clipboard{};

    QStringList m_logs;

    QQuickWindow *getMainWindow() const;

// PLATFORM SPECIFIC CODE
#ifdef Q_OS_ANDROID
    // ANDROID CODE
public:
    static void handleUniversalLinks(JNIEnv *env, jclass clazz, jstring jurl);
    static JNINativeMethod main_activity_methods[];
#endif
};

#endif // QUODARCAAPPLICATION_H
