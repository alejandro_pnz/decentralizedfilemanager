
#import "UIKit/UIKit.h"

#include <QUrl>
#include <QCoreApplication>
#include <QFileOpenEvent>
#include <QUuid>

#include <Utils.h>

@interface QIOSApplicationDelegate
@end

@interface QIOSApplicationDelegate (QuodArcaApplicationDelegate)
@end

@implementation QIOSApplicationDelegate (QuodArcaApplicationDelegate)

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    NSLog(@"Liftoff!");

    if (@available(iOS 13.0, *)) {
        // TODO: Set in according to selected theme
        application.statusBarStyle = UIStatusBarStyleDarkContent;
    }

    // TODO:
    application.idleTimerDisabled = YES;

    // TODO: setup background fetch setup

    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(nonnull NSUserActivity *)userActivity restorationHandler:(nonnull void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler {
    
    NSLog(@"Continue User Activity called");
    
    // TODO:
    application.idleTimerDisabled = YES;

    if ([userActivity.activityType isEqualToString:NSUserActivityTypeBrowsingWeb]) {
        NSURL *nsurl = userActivity.webpageURL;
        if (!nsurl) {
            return NO;
        }

        NSLog(@"%@", nsurl);

        QCoreApplication::postEvent(qApp, new QFileOpenEvent(QUrl::fromNSURL(nsurl)));
    }
    
    return YES;
}

bool isiOSAppOnMac() {
    if (@available(iOS 14.0, *)) {
        return NSProcessInfo.processInfo.isiOSAppOnMac;
    }

    return NO;
}

bool isIpad() {
    return [[[[UIDevice currentDevice] model] lowercaseString] containsString:@"ipad"];
}

QString deviceModel() {
    return QString::fromNSString([[UIDevice currentDevice] name]);
}

QMargins safeIntents() {
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.windows.firstObject;
        CGFloat topPadding = window.safeAreaInsets.top;
        CGFloat bottomPadding = window.safeAreaInsets.bottom;
        CGFloat leftPadding = window.safeAreaInsets.left;
        CGFloat rightPadding = window.safeAreaInsets.right;

        return QMargins(leftPadding,topPadding,rightPadding,bottomPadding);
    }
    return QMargins();
}

@end
