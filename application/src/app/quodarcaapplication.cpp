/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "quodarcaapplication.h"
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QTimer>
#include <QFileOpenEvent>
#include <QDesktopServices>
#include <QCommandLineParser>
#include <QSettings>
#include <QDir>
#include <QDirIterator>
#include <QTranslator>
#include <QDebug>
#include <QWindow>
#include <QQuickWindow>

#include "safemargins.h"

#include "enums.h"

#if defined(Q_OS_ANDROID)
#include <QJniObject>
#include "androidwebview.h"
#elif defined(Q_OS_IOS)
extern bool isiOSAppOnMac();
extern bool isIpad();
extern QString deviceModel();
#elif defined(Q_OS_MACOS)
#include "macx/utils.h"
#elif defined(Q_OS_WINDOWS)
#include <Windows.h>
#endif

QuodArcaApplication::QuodArcaApplication(int &argc, char **argv)
#if defined(USE_SINGLE_APPLICATION)
    : SingleApplication(argc, argv, true, SingleApplication::ExcludeAppVersion | SingleApplication::ExcludeAppPath)
#else
    : QGuiApplication(argc, argv)
#endif
{

#if defined(USE_SINGLE_APPLICATION)
    if (isSecondary() ) {
#if defined(Q_OS_WINDOWS)
        AllowSetForegroundWindow(DWORD(primaryPid()));
#endif
        sendMessage(arguments().join(' ').toUtf8());
        ::exit(EXIT_SUCCESS);
    }
    connect(this, &SingleApplication::receivedMessage, this, &QuodArcaApplication::receiveMessage);
#endif

#if defined (Q_OS_WINDOWS)
    registerUrlHandler();
#endif

    m_safeMargins = new SafeMargins(this);
    m_safeMargins->init();
    m_detector = new CapsLockDetector(this);
    m_clipboard = new Clipboard(this);

#if defined(USE_SINGLE_APPLICATION)
    connect(m_detector, &CapsLockDetector::capsLockStatusChanged, this, &QuodArcaApplication::capsLockStatusChanged);

    this->installEventFilter(m_detector);
#endif
}

QuodArcaApplication::~QuodArcaApplication()
{

}

void QuodArcaApplication::initQML(QQmlApplicationEngine *engine, const QString &mainQmlFile)
{
    Q_ASSERT(engine);
    m_engine = engine;

    initLocalizationSupport();

    engine->addImportPath(QStringLiteral("qrc:/"));
    int typeId = qmlRegisterSingletonType(QUrl(QStringLiteral("qrc:/Strings.qml")),
                              "com.test", 1, 0, "Strings");
    emit stringsRegistered(typeId);
    qmlRegisterUncreatableType<QuodArca::Enums>("com.test", 1, 0, "Enums",
        "Attempt to spawn uncreatable type");
    engine->rootContext()->setContextProperty("app", this);
    engine->rootContext()->setContextProperty("safeMargins", m_safeMargins);

    const QUrl url(mainQmlFile);
    QObject::connect(engine, &QQmlApplicationEngine::objectCreated,
                     this, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    m_engine->load(url);
}

void QuodArcaApplication::handleCommandLine(const QStringList &arguments)
{
    QCommandLineParser parser;

    QCommandLineOption urlOption({"u", "url"}, "Url to handle", "url");
    parser.addOption(urlOption);

    if (!parser.parse(arguments)) {
        qWarning() << parser.errorText();
        return;
    }

    parser.process(arguments);

    qDebug() << parser.values(urlOption);

    QString sUrl = parser.value(urlOption);
    if (!sUrl.isEmpty()) {
        AppClassTemplate::postEvent(this, new QFileOpenEvent(QUrl(sUrl)));
    }
}

bool QuodArcaApplication::isMobile() const
{
#if defined(Q_OS_ANDROID)
    return true;
#elif defined(Q_OS_IOS)
    return !isiOSAppOnMac();
#else
    return false;
#endif
}

bool QuodArcaApplication::isTablet() const
{
#if defined(Q_OS_ANDROID)
    const QJniObject activity = QNativeInterface::QAndroidApplication::context();
    return static_cast<bool>(activity.callMethod<jboolean>("isTablet"));
#elif defined(Q_OS_IOS)
    return isIpad();
#else
    return false;
#endif
}

bool QuodArcaApplication::isPhone() const
{
    return isMobile() && !isTablet();
}

bool QuodArcaApplication::capsLockOn() const
{
    return m_detector->isCapsLockOn();
}

QuodArca::Enums::DeviceType QuodArcaApplication::deviceType() const
{
    if (isTablet()) {
        return QuodArca::Enums::Tablet;
    }
    else if (isMobile()) {
        return QuodArca::Enums::Phone;
    }

    return QuodArca::Enums::Laptop;
}

QString QuodArcaApplication::deviceName() const
{
#if defined(Q_OS_ANDROID)
    const QString manufacturer = QJniObject::getStaticObjectField("android/os/Build", "MANUFACTURER", "Ljava/lang/String;").toString();
    const QString model = QJniObject::getStaticObjectField("android/os/Build", "MODEL", "Ljava/lang/String;").toString();
    return manufacturer + QLatin1Char(' ') + model;
#elif defined(Q_OS_IOS)
    return deviceModel();
#elif defined(Q_OS_WINDOWS)
    // TODO: get pretty name
    return QStringLiteral("Windows PC");
#else
    // TODO: get pretty name
    return QStringLiteral("MacBook Pro");
#endif
}

QString QuodArcaApplication::companyWebsite() const
{
    return QStringLiteral("https://qubibox.com/");
}

#include <PasswordGenerator.h>
#include <QFlag>
QString QuodArcaApplication::generatePassword(int type, quint16 length, bool specialChars)
{
    qDebug() << Q_FUNC_INFO << type << length << specialChars;
    QFlags<PasswordGenerator::PasswordPattern> pattern;
    switch (type) {
    case QuodArca::Enums::LettersAndNumbers:
        pattern = PasswordGenerator::Letters | PasswordGenerator::Numbers;
        break;
    case QuodArca::Enums::NumbersOnly:
        pattern = PasswordGenerator::Numbers;
        break;
    case QuodArca::Enums::Passphrase: // TODO adjust when specification will be delivered
        pattern = PasswordGenerator::Letters | PasswordGenerator::Numbers | PasswordGenerator::Symbols;
        break;
    case QuodArca::Enums::Random: // TODO adjust when specification will be delivered
        pattern = PasswordGenerator::Letters | PasswordGenerator::Numbers
                | PasswordGenerator::Symbols | PasswordGenerator::NoSimilar;
        break;
    default:
        return {};
    }

    auto passwords = PasswordGenerator::generatePasswords(pattern, 1, length);
    Q_ASSERT(passwords.size() == 1);
    return passwords.first();
}

QSize QuodArcaApplication::mainWindowSize() const
{
    auto window = getMainWindow();
    if (window) {
        return window->size();
    }
    return {};
}

void QuodArcaApplication::activateMainWindow() const
{
    auto window = getMainWindow();
    if (window) {
        window->requestActivate();
    }
}

QQmlApplicationEngine *QuodArcaApplication::engine() const
{
    return m_engine;
}

Clipboard *QuodArcaApplication::clipboard() const
{
    return m_clipboard;
}

QString QuodArcaApplication::logs() const
{
    return m_logs.join("<br>");
}

void QuodArcaApplication::appendLogs(const QString &logs)
{
    m_logs.append(logs);
    Q_EMIT this->logsChanged();
}

bool QuodArcaApplication::event(QEvent *e)
{
    if (e->type() == QEvent::FileOpen) {
        QFileOpenEvent *fileOpenEvent = reinterpret_cast<QFileOpenEvent *>(e);
        return QDesktopServices::openUrl(fileOpenEvent->url());
    }
    else if (e->type() == QEvent::LanguageChange) {
        if (m_engine) {
            m_engine->retranslate();
        }
    }

    return AppClassTemplate::event(e);
}

void QuodArcaApplication::initLocalizationSupport()
{
    // TODO: This code just prints available languages

    QDirIterator it(":/i18n/", { "*.qm" }, QDir::NoDotAndDotDot | QDir::Files);
    while (it.hasNext()) {
        it.next();
        QTranslator translator;
        if (!translator.load(it.filePath())) {
            qWarning() << "Failed to load" << it.filePath();
            continue;
        }

        QString filename = it.fileName(); // QuodArca-en.qm
        filename.truncate(filename.lastIndexOf('.')); // QuodArca-en
        filename.remove(0, filename.lastIndexOf('-') + 1); // en

        QLocale locale(filename);

        qDebug() << locale.name() << QLocale::languageToString(locale.language()) << locale.nativeLanguageName();
    }
}

#if defined(Q_OS_WINDOWS)
void QuodArcaApplication::registerUrlHandler()
{    
    QSettings registry("HKEY_CURRENT_USER\\Software\\Classes\\qb", QSettings::NativeFormat);

    registry.setValue("URL protocol", "");

    registry.setValue("shell/open/command/.", QString("\"%1\" --url ").arg(QDir::toNativeSeparators(applicationFilePath())) + "\"%1\"");
}
#endif

void QuodArcaApplication::receiveMessage(quint32 instanceId, const QByteArray &message)
{
    qDebug() << instanceId << message;

    QStringList arguments = QString::fromUtf8(message).split(' ');

    if (arguments.isEmpty()) {
        return;
    }

    for (QWindow* appWindow : qApp->allWindows()) {
        appWindow->show(); //bring window to top on OSX
        appWindow->raise(); //bring window from minimized state on OSX

        appWindow->requestActivate(); //bring window to front/unminimize on windows
    }

    handleCommandLine(arguments);
}

QQuickWindow *QuodArcaApplication::getMainWindow() const
{
    // Find main window, which is always last in the list
    const auto rootObjects = m_engine->rootObjects();
    auto it = std::find_if(rootObjects.begin(), rootObjects.end(), [](const QObject* obj){
        return obj->objectName() == QStringLiteral("appMainWindow");
    });
    if (it != rootObjects.end()) {
        auto window = qobject_cast<QQuickWindow*>(*it);
        if (window) {
            return window;
        }
    }
    return {};
}

CapsLockDetector::CapsLockDetector(QObject *parent)
    : QObject(parent)
{
    m_capsLockOn = isCapsLockOn();
}

CapsLockDetector::~CapsLockDetector()
{

}

bool CapsLockDetector::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::KeyPress || event->type() == QEvent::KeyRelease || event->type() == QEvent::FocusIn) {
        bool capsLockOn = isCapsLockOn();
        if (m_capsLockOn != capsLockOn) {
            m_capsLockOn = capsLockOn;
            Q_EMIT this->capsLockStatusChanged();
        }
    }

    return QObject::eventFilter(watched, event);
}

bool CapsLockDetector::isCapsLockOn() const
{
#if defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)
    qWarning() << "Caps Lock status check not supported";
    return false;
#elif defined(Q_OS_MACOS)
    return MacIsCapsLocked();
#elif defined(Q_OS_WINDOWS)
    return (GetKeyState(VK_CAPITAL) == 1);
#else
    // Not applicable
    return false;
#endif
}

#ifdef Q_OS_ANDROID

JNINativeMethod QuodArcaApplication::main_activity_methods[] = {
    { "handleUniversalLinks", "(Ljava/lang/String;)V", reinterpret_cast<void*>(&QuodArcaApplication::handleUniversalLinks) }
};

void QuodArcaApplication::handleUniversalLinks(JNIEnv */*env*/, jclass /*clazz*/, jstring jurl)
{
    QJniObject jniuri(jurl);
    const QUrl url(jniuri.toString());
    if (url.isValid()) {
        AppClassTemplate::postEvent(qApp, new QFileOpenEvent(url));
    }
}

jint JNI_OnLoad(JavaVM *vm, void *reserved)
{
    Q_UNUSED(reserved)

    JNIEnv* env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return JNI_ERR;
    }

    // Find class. JNI_OnLoad is called from the correct class loader context for this to work.
    jclass c = env->FindClass("org/qtproject/qt/android/bindings/QtFragmentActivity");
    if (c == nullptr) return JNI_ERR;

    // Register class' native methods.
    static const JNINativeMethod methods[] = {
        QuodArcaApplication::main_activity_methods[0],
        SafeMargins::main_activity_methods[0]
    };
    int rc = env->RegisterNatives(c, methods, sizeof(methods)/sizeof(JNINativeMethod));
    if (rc != JNI_OK) return rc;

    if (!AndroidWebView::registerNatives(env)) {
        return JNI_ERR;
    }

    return JNI_VERSION_1_6;
}
#endif
