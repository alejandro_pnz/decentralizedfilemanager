
ios {
    QT += gui-private
}

INCLUDEPATH += \
    $$PWD

HEADERS += \
    $$PWD/clipboard.h \
    $$PWD/safemargins.h \
    $$PWD/quodarcaapplication.h \
    $$PWD/smartscale.h

SOURCES += \
    $$PWD/clipboard.cpp \
    $$PWD/safemargins.cpp \
    $$PWD/quodarcaapplication.cpp \
    $$PWD/smartscale.cpp

android {
    include($$PWD/../../functions.pri)

    JAVA_SOURCES = \
        $$PWD/android/java/QtFragmentActivity.java \
        $$PWD/android/java/QtFragmentActivityLoader.java

    QMAKE_EXTRA_TARGETS += $$copyAndroidSources("JavaSources", "src/org/qtproject/qt/android/bindings", $$JAVA_SOURCES)

    ANDROID_UTILS_SOURCES = \
        $$PWD/android/java/QPathResolver.java

    QMAKE_EXTRA_TARGETS += $$copyAndroidSources("AndroidUtilsSources", "src/com/eisst/quodarca", $$ANDROID_UTILS_SOURCES)
}

ios {
    OBJECTIVE_SOURCES += \
        $$PWD/ios/AppDelegate.mm
}

macx {
    HEADERS += \
        $$PWD/macx/utils.h

    OBJECTIVE_SOURCES += \
        $$PWD/macx/utils.mm
}

macx|win32 {
    include($$PWD/singleapplication/singleapplication.pri)

    DEFINES += QAPPLICATION_CLASS=QGuiApplication

    win32:LIBS += User32.lib
}
