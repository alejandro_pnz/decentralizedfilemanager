#ifndef SAFEMARGINS_H
#define SAFEMARGINS_H

#include <QObject>
#include <QMargins>

#ifdef Q_OS_ANDROID
#include <QJniEnvironment>
#endif

class QQuickWindow;
class SafeMargins : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int left READ left NOTIFY marginsChanged);
    Q_PROPERTY(int bottom READ bottom NOTIFY marginsChanged);
    Q_PROPERTY(int right READ right NOTIFY marginsChanged);
    Q_PROPERTY(int top READ top NOTIFY marginsChanged);
    Q_PROPERTY(int statusBarHeight READ statusBarHeight NOTIFY statusBarHeightChanged);
public:
    explicit SafeMargins(QObject *parent = nullptr);
    void init();

    int left() const;
    int right() const;
    int top() const;
    int bottom() const;
    int statusBarHeight() const;
    void setOrientation(int orientation);
    void setStatusBarHeight(int height);
    Q_INVOKABLE void setQmlWindow(QQuickWindow* window);
signals:
    void marginsChanged();
    void updateSafeMarginRequest();
    void statusBarHeightChanged();
private:
    void updateSafeMargins(Qt::ScreenOrientation orientation = Qt::PrimaryOrientation);
    QMargins m_safeMargins;
    Qt::ScreenOrientation m_orientation{Qt::PrimaryOrientation};

    QQuickWindow *m_window{};

    int m_statusBarHeight{0};
// PLATFORM SPECIFIC CODE
#ifdef Q_OS_ANDROID
    // ANDROID CODE
public:
    bool hasNotch();
    static void handleOrientationChanged(JNIEnv *env, jobject, jint orientation);
    static JNINativeMethod main_activity_methods[];
#elif defined (Q_OS_IOS)
    // IOS CODE
public:

#endif

};

#endif // SAFEMARGINS_H
