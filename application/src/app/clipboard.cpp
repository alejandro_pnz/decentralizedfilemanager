#include "clipboard.h"
#include <QGuiApplication>
#include <QClipboard>
#include <QDebug>
Clipboard::Clipboard(QObject *parent) : QObject(parent)
{

}

void Clipboard::copy(const QString &text)
{
    //qDebug() << "SET text" << text;
    qGuiApp->clipboard()->setText(text);
    //qDebug() << "text" << qGuiApp->clipboard()->text();
    qDebug() << "Copied to clipboard";
    emit copied();
}

void Clipboard::clear()
{
    qGuiApp->clipboard()->clear();
}
