#ifndef CLIPBOARD_H
#define CLIPBOARD_H

#include <QObject>

class Clipboard : public QObject
{
    Q_OBJECT
public:
    explicit Clipboard(QObject *parent = nullptr);
    Q_INVOKABLE void copy(const QString& text);
    Q_INVOKABLE void clear();

signals:
    void copied();

};

#endif // CLIPBOARD_H
