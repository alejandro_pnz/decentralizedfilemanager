#ifndef SMARTSCALE_H
#define SMARTSCALE_H

#include <QObject>
#include <qqmlhelpers.h>
#include <QElapsedTimer>

class QQmlApplicationEngine;
class QQuickWindow;
class QScreen;
class SmartScale : public QObject
{
    Q_OBJECT
    QML_WRITABLE_PROPERTY(QString, desc);
    QML_WRITABLE_PROPERTY(int, width);
    QML_WRITABLE_PROPERTY(int, height);
    QML_WRITABLE_PROPERTY(int, minWidth);
    QML_WRITABLE_PROPERTY(int, minHeight);
public:
    explicit SmartScale(QQmlApplicationEngine *engine, QObject *parent = nullptr);
    Q_INVOKABLE void init(QQuickWindow* window);
    Q_INVOKABLE void zoomIn();
    Q_INVOKABLE void zoomOut();
signals:
    void updateMainWindow(int width, int height);
private:
    void calculateSplashScreenSizeAndScale();
    void updateScale(bool centerWindow = false);
    int titleBarSize();
    void handleScreenChange(QScreen*);
    void handleAvailableGeometryChanged(QRect);
    QScreen *m_currentScreen{};
    QQuickWindow *m_window{};
    QQmlApplicationEngine* m_engine{};

    static constexpr int s_step{5};
    static constexpr qreal s_minScale{0.5};
    static constexpr qreal s_maxScale{1.4};
    static constexpr int s_minWidowHeight{600};
};

#endif // SMARTSCALE_H
