#include "safemargins.h"
#include <QDebug>
#include <QGuiApplication>
#include <QScreen>
#include <QQuickWindow>

#ifdef Q_OS_ANDROID
#include <QJniObject>
SafeMargins * s_ptr = nullptr;
#elif defined(Q_OS_IOS)
#include <qpa/qplatformwindow.h>
extern QMargins safeIntents();
#endif

SafeMargins::SafeMargins(QObject *parent) : QObject(parent)
{
#ifdef Q_OS_ANDROID
    s_ptr = this;
#endif
}

void SafeMargins::init()
{
#ifdef Q_OS_IOS
    connect(this, &SafeMargins::updateSafeMarginRequest, this, [this](){
        updateSafeMargins();
    });
    connect(QGuiApplication::primaryScreen(), &QScreen::orientationChanged, this, &SafeMargins::updateSafeMarginRequest, Qt::QueuedConnection);
#elif defined(Q_OS_ANDROID)
    const QJniObject activity = QNativeInterface::QAndroidApplication::context();

    // Init status bar height
    setStatusBarHeight(static_cast<int>(activity.callMethod<jint>("getStatusBarHeight"))
            / QGuiApplication::primaryScreen()->devicePixelRatio());

    // Init orientation
    setOrientation(static_cast<int>(activity.callMethod<jint>("getCurrentOrientation")));
#endif
}

int SafeMargins::left() const
{
    return m_safeMargins.left();
}

int SafeMargins::right() const
{
    return m_safeMargins.right();
}

int SafeMargins::top() const
{
    return m_safeMargins.top();
}

int SafeMargins::bottom() const
{
    return m_safeMargins.bottom();
}

void SafeMargins::setOrientation(int orientation)
{
    Qt::ScreenOrientation newOrientation = Qt::PrimaryOrientation;
    if (orientation == 0) newOrientation = Qt::PortraitOrientation;
    else if (orientation == 180) newOrientation = Qt::InvertedPortraitOrientation;
    else if (orientation == 90) newOrientation = Qt::LandscapeOrientation;
    else if (orientation == 270) newOrientation = Qt::InvertedLandscapeOrientation;
    else return;

    if (newOrientation == m_orientation) {
        return;
    }

    m_orientation = newOrientation;
    updateSafeMargins(m_orientation);
}

void SafeMargins::setStatusBarHeight(int height)
{
    if (height == m_statusBarHeight)
        return;
    m_statusBarHeight = height;
    emit statusBarHeightChanged();
}

void SafeMargins::setQmlWindow(QQuickWindow *window)
{
    m_window = window;
    emit updateSafeMarginRequest();
}

/*!
 * \brief Obtain the safe margins from native API and process them.
 * \param orientation
 *
 */
void SafeMargins::updateSafeMargins(Qt::ScreenOrientation orientation)
{
    Q_UNUSED(orientation)
#ifdef Q_OS_ANDROID
    const qreal dpr = QGuiApplication::primaryScreen()->devicePixelRatio();

    const QJniObject activity = QNativeInterface::QAndroidApplication::context();
    QJniObject object = activity.callObjectMethod("getSafeArea", "()Landroid/graphics/Rect;");
    const int left = static_cast<int>(object.getField<jint>("left")) / dpr;
    const int right = static_cast<int>(object.getField<jint>("right")) / dpr;
    const int top = static_cast<int>(object.getField<jint>("top")) / dpr;
    const int bottom = static_cast<int>(object.getField<jint>("bottom")) / dpr;

    switch (orientation) {
    case Qt::PortraitOrientation:
        m_safeMargins = QMargins(left, qMax(top, statusBarHeight()), right, bottom);
        break;
    case Qt::LandscapeOrientation:
        m_safeMargins = QMargins(bottom, qMax(right, statusBarHeight()), top, left);
        break;
    case Qt::InvertedPortraitOrientation:
        m_safeMargins = QMargins(bottom, qMax(left, statusBarHeight()), top, right);
        break;
    case Qt::InvertedLandscapeOrientation:
        m_safeMargins = QMargins(top, qMax(right, statusBarHeight()), bottom, left);
        break;
    default: break;
    }

    qDebug() << "New margins" << m_safeMargins;
    marginsChanged();
#elif defined(Q_OS_IOS)
    m_safeMargins = safeIntents();
    qDebug() << "New margins" << m_safeMargins;
    marginsChanged();
#endif
}




#if defined(Q_OS_ANDROID)
bool SafeMargins::hasNotch()
{
    const QJniObject activity = QNativeInterface::QAndroidApplication::context();
    return static_cast<bool>(activity.callMethod<jboolean>("hasNotch"));
}

void SafeMargins::handleOrientationChanged(JNIEnv *, jobject, jint orientation)
{
    if (s_ptr) s_ptr->setOrientation(static_cast<int>(orientation));
}

JNINativeMethod SafeMargins::main_activity_methods[] = {
       { "handleOrientationChanged", "(I)V", (void*)&SafeMargins::handleOrientationChanged }
   };
#endif

int SafeMargins::statusBarHeight() const
{
    return m_statusBarHeight;
}
