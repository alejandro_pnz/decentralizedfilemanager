#import <AppKit/AppKit.h>

bool MacIsCapsLocked()
{
    return [NSEvent modifierFlags] & NSEventModifierFlagCapsLock;
}
