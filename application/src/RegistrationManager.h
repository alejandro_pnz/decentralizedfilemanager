#ifndef REGISTRATIONMANAGER_H
#define REGISTRATIONMANAGER_H

#include <QObject>
#include <QJSValue>
#include <QSharedPointer>
#include <QQuickItem>

class PageManager;
class QQmlEngine;
class Account;
class Error;

class RegistrationManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(RegistrationType currentFlow READ currentFlow MEMBER m_currentFlow)
    Q_PROPERTY(QString loginPassphrase READ loginPassphrase CONSTANT)
    Q_PROPERTY(QString powerPassphrase READ powerPassphrase CONSTANT)
    Q_PROPERTY(bool unblocking MEMBER m_unblocking NOTIFY unblockingChanged)
    Q_PROPERTY(bool registered READ registered NOTIFY registeredChanged)
    Q_PROPERTY(bool accountOwner MEMBER m_isAccountOwner NOTIFY accountOwnerChanged)
    Q_PROPERTY(bool powerPassphraseConfigured MEMBER m_powerPassphraseIsConfigured NOTIFY powerPassphraseConfigurationChanged)

    Q_PROPERTY(QQuickItem* currentItem READ currentItem WRITE setCurrentItem NOTIFY currentItemChanged)
    QML_UNAVAILABLE

public:
    enum RegistrationType {
        NewAccount,
        JoinExistingAccount,
        Login,
        AddNewDevice,
        InvitedUser
    };
    Q_ENUM(RegistrationType);
    explicit RegistrationManager(PageManager *pageManager, QQmlEngine* engine, QObject *parent = nullptr);

    Q_INVOKABLE void init();
    Q_INVOKABLE void createAccount();
    Q_INVOKABLE void loginPage();
    Q_INVOKABLE void joinExistingAccount();
    Q_INVOKABLE void termsPage();
    Q_INVOKABLE void openLink(const QString &link);
    Q_INVOKABLE void createAccountPage();
    Q_INVOKABLE void joinAccountPage(const QString &accountOwnerEmail);
    Q_INVOKABLE void checkEmailPage();
    Q_INVOKABLE void accountSetupPage();
    Q_INVOKABLE void choosePowerPassphrasePage();
    Q_INVOKABLE void confirmPowerPassphrasePage(const QString& passphrase);
    Q_INVOKABLE void inviteUsersPage();
    Q_INVOKABLE void emailValidated();
    Q_INVOKABLE void accountOwnerOrNot();
    Q_INVOKABLE void enterAccountOwnerEmail();
    Q_INVOKABLE void enterAddDeviceAdvancedPage();
    Q_INVOKABLE void requestToJoinSubmitted();
    Q_INVOKABLE void showEmailConfirmation();
    Q_INVOKABLE void newDeviceRequestAccepted();
    Q_INVOKABLE void inviteUsers();
    Q_INVOKABLE void skipInvitingUsers();
    Q_INVOKABLE void enterHome(const QJSValue &config = QJSValue());
    Q_INVOKABLE void setPassphrasePage(const QString &accountName = {}, bool isLogin = true);
    Q_INVOKABLE void configurePowerPassphrase(const QString& powerPassphrase);

    Q_INVOKABLE void onboardingInvitation();
    Q_INVOKABLE void temporaryPassphrase();
    Q_INVOKABLE void configureLoginPassphrase();

    Q_INVOKABLE void login(const QString& email, const QString& passphrase);
    Q_INVOKABLE void newDeviceRegisteredUser();
    Q_INVOKABLE void addNewDeviceMultipleAccountOwner();
    Q_INVOKABLE void newDeviceAdded();
    Q_INVOKABLE void firstSyncPending();
    Q_INVOKABLE void firstSyncInProgress();
    Q_INVOKABLE void settingUpYourAccount(const QString &passphrase);
    Q_INVOKABLE void newDeviceAccountOwner();
    Q_INVOKABLE void enterReviewerEmail();
    void loginBlocked();
    Q_INVOKABLE void loginUnblocked(const QString& passphrase);
    void deleteData();
    Q_INVOKABLE void resetPassphrase(const QString& passphrase);
    Q_INVOKABLE void deviceBlockedAccountOwner();
    Q_INVOKABLE void deviceBlocked();
    Q_INVOKABLE void accountBlocked();
    Q_INVOKABLE void deviceActivationRejected();
    Q_INVOKABLE void deviceActivationBlocked();
    Q_INVOKABLE void unblockDevice();
    Q_INVOKABLE void deviceActivationApproved();

    Q_INVOKABLE void switchAccount(const QString& email);

    RegistrationType currentFlow() const;

    const QString &loginPassphrase() const;
    const QString &powerPassphrase() const;

    QQuickItem *currentItem() const;
    void setCurrentItem(QQuickItem *item);

    bool registered() const;

    Q_INVOKABLE void checkEmail(const QString &email, const QString &loginPassphrase);
    Q_INVOKABLE void verifyEmail(const QString &token);
    Q_INVOKABLE void registration();

    Q_INVOKABLE void enteredAccountEmail(const QString &email);
    Q_INVOKABLE void addNewAccountOwnerDevice(const QString &passphrase);

    void handleError(const Error &error);

    void cleanup();

signals:
    void wrongCredentials(const QJSValue& config);
    void emailAlreadyUsed(const QJSValue& config);
    void emailDoesNotExist(const QJSValue& config);
    void meAccountOwner();
    void loginWithAccountOwnerEmail(const QString &email);

    void currentItemChanged();

    void unblockingChanged();
    void registeredChanged();
    void accountOwnerChanged();
    void powerPassphraseConfigurationChanged();

public slots:
    void registerStrings(int id);

private slots:
    void onRegistrationTokenReceived(const QString &token, const QString &email);
    void onAddingNewDeviceReceived();

protected:
    void setBusy(bool busy);

private:
    QJSValue getConfig(bool leftButton = true, bool rightButton = false);

    QQmlEngine *m_engine{};
    PageManager *m_pageManager{};
    RegistrationType m_currentFlow;
    QJSValue m_strings;

    QQuickItem *m_currentItem{nullptr};

    // Note: set it to true to test email invitation flow
    bool m_emailInvitation{false};
    bool m_addDeviceRejected{false};
    bool m_addDeviceBlocked{false};
    bool m_addingNewDeviceReceived{false};
    // Note: set it to true to test wrong device popup
    bool m_wrongDeviceType{false};
    // Note: set it to true to test Account Owner adds a new device
    bool m_isAccountOwner{false};
    // Note: set it to true to test Power Passphrase configured
    bool m_powerPassphraseIsConfigured{false};
    // Note: set it to true to test selected the Power Passphrase authorization option
    bool m_selectedConfirmViaEmaiOption{true};

    QSharedPointer<Account> m_account;
    QString m_registrationToken;
    QString m_accountName;
    QString m_accountOwnerEmail;
    QString m_deviceEmail;
    QString m_loginPassphrase;
    QString m_powerPassphrase;

    bool m_unblocking{false};
};

#endif // REGISTRATIONMANAGER_H
