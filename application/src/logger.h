/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef LOGGER_H
#define LOGGER_H


class Logger
{
public:
    static void init();

protected:
    Logger();
};

#endif // LOGGER_H
