/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "logger.h"

#include <QString>
#include <QDateTime>

#include "quodarcaapplication.h"

#if defined(Q_OS_ANDROID)
#include <android/log.h>
#elif defined(Q_OS_WINDOWS)
#include <Windows.h>
#endif


namespace QuodArca {

bool shouldLogToStderr()
{
    static bool forceStderrLogging = qEnvironmentVariableIntValue("QT_FORCE_STDERR_LOGGING");
    return forceStderrLogging;
}

#if defined(Q_OS_ANDROID) && !defined(Q_OS_ANDROID_EMBEDDED)
static bool android_message_handler(QtMsgType type,
                                    const QString &message)
{
    if (shouldLogToStderr()) {
        return false; // Leave logging up to stderr handler
    }

    android_LogPriority priority = ANDROID_LOG_DEBUG;
    switch (type) {
    case QtDebugMsg: priority = ANDROID_LOG_DEBUG; break;
    case QtInfoMsg: priority = ANDROID_LOG_INFO; break;
    case QtWarningMsg: priority = ANDROID_LOG_WARN; break;
    case QtCriticalMsg: priority = ANDROID_LOG_ERROR; break;
    case QtFatalMsg: priority = ANDROID_LOG_FATAL; break;
    };

    __android_log_print(priority, qPrintable(QCoreApplication::applicationName()), "%s", qPrintable(message));
    return true; // Prevent further output to stderr
}
#endif //Q_OS_ANDROID

#ifdef Q_OS_WIN
static bool windows_message_handler(const QString &message)
{
    if (shouldLogToStderr()) {
        return false; // Leave logging up to stderr handler
    }

    OutputDebugString(reinterpret_cast<const wchar_t *>(message.utf16()));
    return true; // Prevent further output to stderr
}
#endif

static void stderr_message_handler(const QString &message)
{
    fprintf(stderr, "%s", message.toLocal8Bit().constData());
    fflush(stderr);
}

// The implementation of the handler
void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &message)
{
    // Open stream file writes
    QString out;
    // Write the date of recording
    out += QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");
    // By type determine to what level belongs message
    switch (type)
    {
    case QtInfoMsg:     out += "INF "; break;
    case QtDebugMsg:    out += "DBG "; break;
    case QtWarningMsg:  out += "WRN "; break;
    case QtCriticalMsg: out += "CRT "; break;
    case QtFatalMsg:    out += "FTL "; break;
    }
    // Write to the output category of the message and the message itself
    out += context.category;
    out += ": ";
    out += message;
    out += "\n";

    bool handledStderr = false;

# if defined(Q_OS_WIN)
    handledStderr = windows_message_handler(out);
# elif defined(Q_OS_ANDROID) && !defined(Q_OS_ANDROID_EMBEDDED)
    handledStderr = android_message_handler(type, out);
# endif

    if (!handledStderr) {
        stderr_message_handler(out);
    }

    QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(qApp);
    if (app) {
        app->appendLogs(out);
    }
}

}

Logger::Logger()
{

}

void Logger::init()
{
    qInstallMessageHandler(QuodArca::messageHandler);
}
