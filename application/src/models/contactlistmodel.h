/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef CONTACTLISTMODEL_H
#define CONTACTLISTMODEL_H

#include "singleton.h"

#include "ContactObject.h"

#include "basemodel.h"


class ContactListModel : public BaseListModel
{
    Q_OBJECT

public:

    static ContactListModel *get();

    ~ContactListModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE void addContact(const QString &email);

protected:
    ContactListModel();

    QVariantList source() const override;

    friend class TSingleton<ContactListModel>;

private:

};

#endif // CONTACTLISTMODEL_H
