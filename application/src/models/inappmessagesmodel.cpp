/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "inappmessagesmodel.h"

#include <QCoreApplication>

#include "AuthenticationManager.h"
#include "InAppMessagesManager.h"

InAppMessagesModel::InAppMessagesModel(QObject *parent)
    : BaseListModel(parent)
{
    setKeyField("id");
    setRoleNames({"id", "text", "received"});

    connect(AuthenticationManager::get(), &AuthenticationManager::loginStateChanged, this, &InAppMessagesModel::sync);

    connect(InAppMessagesManager::get(), &InAppMessagesManager::newMessage, this, &InAppMessagesModel::sync);
}

InAppMessagesModel::~InAppMessagesModel()
{

}

QVariantList InAppMessagesModel::source() const
{
    const auto messages = InAppMessagesManager::get()->messages();

    return QVariant::fromValue(messages).toList();
}

void _q_InAppMessagesModel_registerTypes() {
    qmlRegisterType<InAppMessagesModel>("com.test", 1, 0, "InAppMessagesModel");
}

Q_COREAPP_STARTUP_FUNCTION(_q_InAppMessagesModel_registerTypes)
