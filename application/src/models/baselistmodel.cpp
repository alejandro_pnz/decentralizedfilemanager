#include "baselistmodel.h"

#include "QSDiffRunner"

BaseListModel::BaseListModel(QObject *parent)
    : QSListModel(parent)
{
    m_threadPool.setMaxThreadCount(1);

    connect(this, &BaseListModel::keyFieldChanged, this, &BaseListModel::sync);
    connect(this, &BaseListModel::sourceChanged, this, &BaseListModel::sync);
}

QString BaseListModel::keyField() const
{
    return m_keyField;
}

void BaseListModel::setKeyField(const QString &keyField)
{
    if (m_keyField == keyField) {
        return;
    }

    m_keyField = keyField;
    Q_EMIT this->keyFieldChanged();
}

QStringList BaseListModel::fields() const
{
    return m_fields;
}

void BaseListModel::setFields(const QStringList &roleNames)
{
    m_fields = roleNames;
    setRoleNames(roleNames);
    Q_EMIT this->fieldsChanged();
}

QVariantList BaseListModel::source() const
{
    return m_source;
}

void BaseListModel::setSource(const QVariantList &source)
{
    if (m_source == source) {
        return;
    }

    qDebug().nospace() << this->metaObject()->className() << "::setSource("  << source << ")";
    m_source = source;
    Q_EMIT this->sourceChanged();
}

void BaseListModel::sync()
{
    qDebug().nospace() << this->metaObject()->className() << "::sync()";

    m_threadPool.start([=]() {
        QSDiffRunner runner;
        runner.setKeyField(m_keyField);

        const QList<QSPatch> patches = runner.compare(storage(), source());

        if (!patches.isEmpty()) {
            QMetaObject::invokeMethod(this, "patch", Qt::BlockingQueuedConnection, Q_ARG(QList<QSPatch>, patches));
        }
    });
}

void BaseListModel::patch(const QList<QSPatch> &patches)
{
    Q_ASSERT(QThread::currentThread() == qApp->thread());

    QSDiffRunner runner;
    runner.patch(this, patches);
}

void _q_BaseListModel_registerTypes() {
    qRegisterMetaType<QList<QSPatch> >();
}

Q_COREAPP_STARTUP_FUNCTION(_q_BaseListModel_registerTypes)
