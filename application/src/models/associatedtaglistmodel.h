/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef ASSOCIATEDTAGLISTMODEL_H
#define ASSOCIATEDTAGLISTMODEL_H

#include "QSListModel"
#include "QSDiffRunner"

#include "TagObject.h"

class AssociatedTagListModel : public QSListModel
{
    Q_OBJECT
    Q_PROPERTY(QVariantList idList READ idList NOTIFY idListChanged)

public:
    AssociatedTagListModel(QObject *parent = nullptr);

    QVariantList idList() const;

    TagObjectList tags() const;

    void setTags(const TagObjectList &tags);

    Q_INVOKABLE void addTag(TagObject *obj);

    Q_INVOKABLE void deleteTag(const QUuid &id);

    Q_INVOKABLE void deleteAllTags();

signals:
    void idListChanged();

protected:
    void sync();

private:
    QHash<QUuid, TagObject> m_tags;
};

#endif // ASSOCIATEDTAGLISTMODEL_H
