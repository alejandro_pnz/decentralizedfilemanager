#ifndef BASELISTMODEL_H
#define BASELISTMODEL_H

#include <QThreadPool>

#include "QSListModel"
#include "qspatch.h"

class BaseListModel : public QSListModel
{
    Q_OBJECT
    Q_PROPERTY(QString keyField READ keyField WRITE setKeyField NOTIFY keyFieldChanged)
    Q_PROPERTY(QStringList fields READ fields WRITE setFields NOTIFY fieldsChanged)
    Q_PROPERTY(QVariantList source READ source WRITE setSource NOTIFY sourceChanged)

public:
    explicit BaseListModel(QObject *parent = nullptr);

    virtual QString keyField() const;
    virtual void setKeyField(const QString &keyField);

    QStringList fields() const;
    void setFields(const QStringList &roleNames);

    virtual QVariantList source() const;
    virtual void setSource(const QVariantList &source);

Q_SIGNALS:
    void keyFieldChanged();
    void fieldsChanged();
    void sourceChanged();

protected Q_SLOTS:
    void sync();

    void patch(const QList<QSPatch> &patches);

private:
    QString m_keyField{QStringLiteral("id")};
    QStringList m_fields;
    QVariantList m_source;

    QThreadPool m_threadPool;
};

#endif // BASELISTMODEL_H
