/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "quodsectionsmodel.h"

#include <QQmlEngine>

#include <QDebug>

#include "AuthenticationManager.h"
#include "DatabaseManager.h"

#include "QSDiffRunner"

QuodSectionsModel::QuodSectionsModel()
{
    setKeyField("id");
    setRoleNames({"id", "name", "attributes", "type", "object", "specialSection"});
}

QuodSectionsModel::~QuodSectionsModel()
{

}

QVariant QuodSectionsModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    const QVariant d = QSListModel::data(index, role);
    const auto roleName = roleNames().value(role);

    if (d.isValid()) {
        if (roleName == "type") {
            // TODO: workaround, comparison QVariant(0) vs. QVariant(QuodSectionObject::Generic) doesn't work
            return d.toInt();
        }
        return d;
    }

    if (roleName == "object" && m_quod) {
        auto sections = m_quod->sections();
        const auto sectionId = QSListModel::data(index, roleNames().key("id")).toUuid();
        const auto it = std::find_if(sections.begin(), sections.end(), [&sectionId](const QuodSectionObject &section) {
            return section.id() == sectionId;
        });

        if (it == sections.end()) {
            return {};
        }

        if (it->isValid()) {
            if (roleName == "object") {
                QuodSectionObject *qmlSection = new QuodSectionObject(*it);
                qmlSection->detach();
                QQmlEngine::setObjectOwnership(qmlSection, QQmlEngine::JavaScriptOwnership);
                return QVariant::fromValue(qmlSection);
            }
        }
    } else if (roleName == "specialSection" && m_quod) {
        return handleSpecialSectionRole(index);
    }
    return {};
}

QuodSectionObjectPtr QuodSectionsModel::quodSection(const QUuid &id) const
{
    return nullptr;
}

QVariantList QuodSectionsModel::source() const
{
    return m_source;
}

void QuodSectionsModel::setSource(const QVariantList &source)
{
    qDebug().nospace() << this->metaObject()->className() << "::setSource(" << source << ")";

    m_source = source;
    Q_EMIT this->sourceChanged();

    sync();
}

void QuodSectionsModel::setKeyField(const QString &keyField)
{
    m_keyField = keyField;
}

void QuodSectionsModel::sync()
{
    qDebug() << "QuodSectionsModel::sync()";

    QSDiffRunner runner;
    runner.setKeyField(m_keyField);

    QList<QSPatch> patches = runner.compare(storage(), m_source);

    if (!patches.isEmpty()) {
        runner.patch(this, patches);
    }
    else {
        qDebug() << "QuodSectionsModel::sync(): Nothing to patch";
    }
}


bool QuodSectionsModel::handleSpecialSectionRole(const QModelIndex& index) const
{
    qDebug() << Q_FUNC_INFO;
    auto sections = m_quod->sections();
    const auto sectionId = QSListModel::data(index, roleNames().key("id")).toUuid();
    const auto it = std::find_if(sections.begin(), sections.end(), [&sectionId](const QuodSectionObject &section) {
        return section.id() == sectionId;
    });

    if (it == sections.end()) {
        qDebug() << Q_FUNC_INFO << "section not found";
        return false;
    }
    // Photo section is special section
    if (it->attributes().size() > 0) {
        return it->attributes().at(0).type() == AttributeObject::Type::Icon;
    } else {
        qDebug() << Q_FUNC_INFO <<"Not a special section";
        return false;
    }
    return false;
}

QuodObject *QuodSectionsModel::quod() const
{
    return m_quod;
}

void QuodSectionsModel::setQuod(QuodObject *newQuod)
{
    if (m_quod == newQuod) {
        return;
    }

    m_quod = newQuod;
    Q_EMIT this->quodChanged();

    if (m_quod) {
        Q_EMIT this->dataChanged(index(0), index(rowCount({}) - 1), { roleNames().key("object") });

        const auto sections = m_quod->sections();
        setSource(QVariant::fromValue(sections).toList());
    }
}
