#ifndef SFILTER_H
#define SFILTER_H

#include <QObject>
#include <qqmlhelpers.h>
#include <QVariant>
#include <QUuid>

class SFilter : public QObject
{
    Q_OBJECT
public:
    enum Type {
        OwnedByFilter = 0,
        SentByFilter = 1,
        SharedByFilter = 2,
        ModifiedFilter = 3,
        OpenedFilter = 4,
        TagFilter = 5,
        TypeFilter = 6,
        SyncStatusFilter = 7,
        LockStatusFilter = 8,
        MoreFilter = 9,
        QuodTypeFilter = 10,
        FileTypeFilter = 11,
        QuickFilters = 12
    };
    Q_ENUM(Type)

    struct FilterDesc {
        SFilter::Type type;
        QString name; // Display name
        QVariant id;
    };

    explicit SFilter(QObject *parent = nullptr);

    Q_INVOKABLE virtual void init();
    Q_INVOKABLE virtual void clearAll();
    Q_INVOKABLE virtual void revertChanges();
    virtual void remove(const QVariant&);

    virtual SFilter::Type type() = 0;
    virtual QString& apply(QString& query) = 0;
    QVector<SFilter::FilterDesc> desc();

    void setDetails(const QVariant &newDetails);


protected:
    QVector<SFilter::FilterDesc> m_desc;
private:
    QML_CONSTANT_PROPERTY(SFilter::Type, type)
    QML_WRITABLE_PROPERTY(QString, details)
};


#endif // SFILTER_H
