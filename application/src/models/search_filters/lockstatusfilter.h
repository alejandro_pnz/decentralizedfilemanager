#ifndef LOCKSTATUSFILTER_H
#define LOCKSTATUSFILTER_H

#include "sfilter.h"
#include <QItemSelection>

#include "baselistmodel.h"

class QItemSelectionModel;

class LockStatusFilter : public SFilter
{
    Q_OBJECT
    QML_CONSTANT_PROPERTY(BaseListModel*, model)
    QML_CONSTANT_PROPERTY(QItemSelectionModel*, selectionModel)
public:
    explicit LockStatusFilter(QObject *parent = nullptr);

    virtual void init() override;
    virtual void clearAll() override;
    virtual void revertChanges() override;
    virtual void remove(const QVariant& id) override;
    virtual SFilter::Type type() override;
    virtual QString &apply(QString &query) override;

    Q_INVOKABLE void toggle(const int index);
private:
    QItemSelection m_selection;
private slots:
    void populateModel();
};

#endif // LOCKSTATUSFILTER_H
