#ifndef DATEFILTER_H
#define DATEFILTER_H

#include "sfilter.h"
#include <QDate>
class DateFilter : public SFilter
{
    Q_OBJECT
    QML_WRITABLE_PROPERTY(QDate, startDate);
    QML_WRITABLE_PROPERTY(QDate, endDate);
public:
    explicit DateFilter(SFilter::Type type, const QString& luceneTag, QObject *parent = nullptr);
    virtual void init() override;
    virtual void clearAll() override;
    virtual void revertChanges() override;
    virtual void remove(const QVariant& id) override;
    virtual SFilter::Type type() override;
    virtual QString &apply(QString &query) override;

private:
    SFilter::Type m_type;
    QString m_luceneTag;
};

#endif // DATEFILTER_H
