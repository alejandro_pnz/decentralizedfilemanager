#include "lockstatusfilter.h"
#include "baselistmodel.h"
#include <QItemSelection>
#include <QItemSelectionModel>
#include <QVariantMap>
#include <QVariant>
#include <QDebug>
#include "utility.h"

LockStatusFilter::LockStatusFilter(QObject *parent) : SFilter(parent),
    m_model(new BaseListModel(this)),
    m_selectionModel(new QItemSelectionModel(m_model, this))
{
    populateModel();
}


void LockStatusFilter::init()
{
    m_selection = m_selectionModel->selection();
}

void LockStatusFilter::clearAll()
{
}

void LockStatusFilter::revertChanges()
{
    m_selectionModel->select(m_selection, QItemSelectionModel::Select);
}

void LockStatusFilter::remove(const QVariant& id)
{
    qDebug () << Q_FUNC_INFO << id;
    if (id.isValid()) {
        auto roleName = m_model->roleNames().key("name");
        for (const auto& idx: m_selectionModel->selectedIndexes()) {
            if (idx.data(roleName) == id) {
                m_selectionModel->select(idx, QItemSelectionModel::Deselect);
            }
        }
        m_selection.clear();
    } else {
        m_selection.clear();
        m_selectionModel->clearSelection();
    }
}

SFilter::Type LockStatusFilter::type()
{
    return SFilter::LockStatusFilter;
}

QString &LockStatusFilter::apply(QString &query)
{
    m_selection.clear();
    auto indexes = m_selectionModel->selectedIndexes();

    auto role = m_model->roleNames().key("lucene");
    auto roleName = m_model->roleNames().key("name");
    QStringList list;
    m_desc.clear();
    for (const auto& index : indexes) {
        list.append(m_model->data(index, role).toString());
        FilterDesc d;
        d.type = type();
        d.name = m_model->data(index, roleName).toString();
        d.id = d.name;
        m_desc.append(d);
    }

    qDebug() << Q_FUNC_INFO << "APPLY" << list.length() << m_desc.length();

    set_details(list.length() > 0 ? QString::number(list.length()) : QString());

    if (list.isEmpty()) return query;

    query.append(" ");
    query.append(list.join(" AND "));
    return query;
}

void LockStatusFilter::toggle(const int index)
{
    m_selectionModel->select(m_model->index(index), QItemSelectionModel::Toggle);
}

void LockStatusFilter::populateModel()
{
    QVariantList list;
    // TODO use proper lucene tags when it will be ready
    list.append(QVariant(QVariantMap({{"name",tr("Locked")},
                                      {"iconSource", "qrc:/lock.svg"},
                                      {"lucene", QString()}})));
    list.append(QVariant(QVariantMap({{"name",tr("Unlocked")},
                                      {"iconSource", "qrc:/unlock.svg"},
                                      {"lucene",QString()}})));


    m_model->setKeyField("name");
    m_model->setFields({"name","lucene", "iconSource"});
    m_model->setSource(list);
}
