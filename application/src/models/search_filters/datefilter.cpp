#include "datefilter.h"
#include "utility.h"
#include <QDateTime>
DateFilter::DateFilter(Type type, const QString &luceneTag, QObject *parent)
    : SFilter(parent),
    m_type(type), m_luceneTag(luceneTag)
{

}

void DateFilter::init()
{
}

void DateFilter::clearAll()
{
}

void DateFilter::revertChanges()
{
}

void DateFilter::remove(const QVariant&)
{
    set_startDate(QDate());
    set_endDate(QDate());
}

SFilter::Type DateFilter::type()
{
    return m_type;
}

QString &DateFilter::apply(QString &query)
{
    if (!m_startDate.isValid()) {
        set_details("");
        m_desc = {};
        return query;
    }
    set_details(m_startDate.toString("yyyy/MM/dd"));
    query.append(QStringLiteral(" %1:\"%2\"").arg(m_luceneTag,
                                                  QDateTime(m_startDate, QTime()).toString("yyyyMMddHHmm")));
    qDebug() << Q_FUNC_INFO << query << get_details();
    FilterDesc desc;
    desc.type = type();
    desc.name = (type() == SFilter::Type::ModifiedFilter
                 ? tr("Modified %1").arg(get_details())
                 : tr("Opened %1").arg(get_details()));
    m_desc = {desc};
    qDebug() << Q_FUNC_INFO << m_desc.size();
    return query;
}
