#ifndef QUODTYPEFILTER_H
#define QUODTYPEFILTER_H

#include "sfilter.h"
#include <QItemSelection>

#include "quodtypelistmodel.h"

class QItemSelectionModel;

class QuodTypeFilter : public SFilter
{
    Q_OBJECT
    QML_CONSTANT_PROPERTY(QuodTypeListModel*, model)
    QML_CONSTANT_PROPERTY(QItemSelectionModel*, selectionModel)
public:
    explicit QuodTypeFilter(QObject *parent = nullptr);

    virtual void init() override;
    virtual void clearAll() override;
    virtual void revertChanges() override;
    virtual void remove(const QVariant& id) override;
    virtual SFilter::Type type() override;
    virtual QString &apply(QString &query) override;

    Q_INVOKABLE void toggle(const int index);
private:
    QItemSelection m_selection;
};

#endif // QUODTYPEFILTER_H
