#include "filetypefilter.h"
#include "baselistmodel.h"
#include <QItemSelection>
#include <QItemSelectionModel>
#include <QVariantMap>
#include <QVariant>
#include <QDebug>
#include "utility.h"
FileTypeFilter::FileTypeFilter(QObject *parent) : SFilter(parent),
    m_model(new BaseListModel(this)),
    m_selectionModel(new QItemSelectionModel(m_model, this))
{
    QVariantList list;
    list.append(QVariant(QVariantMap({{"name",tr("Images")}, {"type","jpg gif svg"}})));
    list.append(QVariant(QVariantMap({{"name",tr("Office")}, {"type","docx pdf"}})));
    list.append(QVariant(QVariantMap({{"name",tr("Media")}, {"type","mp4"}})));
    list.append(QVariant(QVariantMap({{"name",tr("Text")}, {"type","txt md"}})));
    list.append(QVariant(QVariantMap({{"name",tr("Other")}, {"type","docx"}})));

    m_model->setKeyField("name");
    m_model->setFields({"name","type"});
    m_model->setSource(list);
}


void FileTypeFilter::init()
{
    m_selection = m_selectionModel->selection();
}

void FileTypeFilter::clearAll()
{
}

void FileTypeFilter::revertChanges()
{
    m_selectionModel->select(m_selection, QItemSelectionModel::Select);
}

void FileTypeFilter::remove(const QVariant& id)
{
    qDebug () << Q_FUNC_INFO << id;
    if (id.isValid()) {
        auto roleName = m_model->roleNames().key("name");
        for (const auto& idx: m_selectionModel->selectedIndexes()) {
            if (idx.data(roleName) == id) {
                m_selectionModel->select(idx, QItemSelectionModel::Deselect);
            }
        }
        m_selection.clear();
    } else {
        m_selection.clear();
        m_selectionModel->clearSelection();
    }
}

SFilter::Type FileTypeFilter::type()
{
    return SFilter::FileTypeFilter;
}

QString &FileTypeFilter::apply(QString &query)
{
    m_selection.clear();
    auto indexes = m_selectionModel->selectedIndexes();

    auto role = m_model->roleNames().key("type");
    auto roleName = m_model->roleNames().key("name");
    QStringList list;
    m_desc.clear();
    for (const auto& index : indexes) {
        list.append(m_model->data(index, role).toString());
        FilterDesc d;
        d.type = type();
        d.name = m_model->data(index, roleName).toString();
        d.id = d.name;
        m_desc.append(d);
    }
    set_details(list.length() > 0 ? QString::number(list.length()) : QString());

    if (list.isEmpty()) return query;

    query.append(QStringLiteral(" fileType:\"%1\"").arg(list.join(" ")));
    return query;
}


void FileTypeFilter::toggle(const int index)
{
    m_selectionModel->select(m_model->index(index), QItemSelectionModel::Toggle);
}
