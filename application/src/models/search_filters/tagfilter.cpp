#include "tagfilter.h"
#include <QDebug>
#include "taglistmodel.h"

TagFilter::TagFilter(QObject *parent) : SFilter(parent),
    m_selectedTagsModel(new QItemSelectionModel(TagListModel::get(), this))
{
}


void TagFilter::init()
{
    m_tagsSelection = m_selectedTagsModel->selection();
}

void TagFilter::clearAll()
{
}

void TagFilter::revertChanges()
{
    qDebug() << Q_FUNC_INFO << m_tagsSelection.length();
    m_selectedTagsModel->select(m_tagsSelection, QItemSelectionModel::Select);
}

void TagFilter::remove(const QVariant& id)
{
    qDebug () << Q_FUNC_INFO << id;
    if (id.isValid()) {
        auto roleName = TagListModel::get()->roleNames().key("id");
        for (const auto& idx: m_selectedTagsModel->selectedIndexes()) {
            if (idx.data(roleName).toString() == id.toString()) {
                m_selectedTagsModel->select(idx, QItemSelectionModel::Deselect);
            }
        }
        m_tagsSelection.clear();
    } else {
        m_tagsSelection.clear();
        m_selectedTagsModel->clearSelection();
    }
}

SFilter::Type TagFilter::type()
{
    return SFilter::TagFilter;
}

QString &TagFilter::apply(QString &query)
{
    m_tagsSelection.clear();
    auto indexes = m_selectedTagsModel->selectedIndexes();

    auto role = TagListModel::get()->roleNames().key("id");
    auto roleName = TagListModel::get()->roleNames().key("name");
    QStringList list;
    m_desc.clear();
    for (const auto& index : indexes) {
        QString id = TagListModel::get()->data(index, role).toString();
        list.append(TagListModel::get()->data(index, roleName).toString());
        FilterDesc d;
        d.type = type();
        d.name = TagListModel::get()->data(index, roleName).toString();
        d.id = id;
        m_desc.append(d);
    }

    set_details(list.length() > 0 ? QString::number(list.length()) : QString());

    if (list.empty()) return query;

    query.append(QStringLiteral(" tags:\"%1\"").arg(list.join(" ")));
    qDebug() << Q_FUNC_INFO << query << get_details();
    return query;
}
