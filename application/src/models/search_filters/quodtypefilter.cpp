#include "quodtypefilter.h"
#include "quodtypelistmodel.h"
#include <QItemSelection>
#include <QItemSelectionModel>
#include "DatabaseManager.h"

QuodTypeFilter::QuodTypeFilter(QObject *parent) : SFilter(parent),
    m_model(new QuodTypeListModel(this)),
    m_selectionModel(new QItemSelectionModel(m_model, this))
{
    auto quodTypes = DatabaseManager::get()->quodTypes();
    QVariantList source;
    for (const auto& type : quodTypes) {
        source.append(QVariant::fromValue(type));
    }
    m_model->setSource(source);
}


void QuodTypeFilter::init()
{
    m_selection = m_selectionModel->selection();
}

void QuodTypeFilter::clearAll()
{
}

void QuodTypeFilter::revertChanges()
{
    m_selectionModel->select(m_selection, QItemSelectionModel::Select);
}

void QuodTypeFilter::remove(const QVariant& id)
{
    qDebug () << Q_FUNC_INFO << id;
    if (id.isValid()) {
        auto role = m_model->roleNames().key("id");
        for (const auto& idx: m_selectionModel->selectedIndexes()) {
            if (idx.data(role).toString() == id.toString()) {
                m_selectionModel->select(idx, QItemSelectionModel::Deselect);
            }
        }
        m_selection.clear();
    } else {
        m_selection.clear();
        m_selectionModel->clearSelection();
    }
}

SFilter::Type QuodTypeFilter::type()
{
    return SFilter::QuodTypeFilter;
}

QString &QuodTypeFilter::apply(QString &query)
{
    m_selection.clear();
    auto indexes = m_selectionModel->selectedIndexes();

    auto role = m_model->roleNames().key("id");
    auto roleName = m_model->roleNames().key("name");
    QStringList idsList;
    m_desc.clear();
    for (const auto& index : indexes) {
        QString id = m_model->data(index, role).toString();
        idsList.append(id);

        FilterDesc d;
        d.type = type();
        d.name = m_model->data(index, roleName).toString();
        d.id = id;
        m_desc.append(d);
    }
    set_details(idsList.length() > 0 ? QString::number(idsList.length()) : QString());

    if (idsList.isEmpty()) return query;

    query.append(QStringLiteral(" quodType:\"%1\"").arg(idsList.join(" ")));
    return query;
}

void QuodTypeFilter::toggle(const int index)
{
    m_selectionModel->select(m_model->index(index), QItemSelectionModel::Toggle);
}
