#ifndef FILTERS_H
#define FILTERS_H

#include "tagfilter.h"
#include "datefilter.h"
#include "quodtypefilter.h"
#include "filetypefilter.h"
#include "quickfilters.h"
#include "lockstatusfilter.h"
#include "syncstatusfilter.h"

#endif // FILTERS_H
