#ifndef TAGFILTER_H
#define TAGFILTER_H

#include "sfilter.h"
#include <QItemSelection>

class QItemSelectionModel;
class TagFilter : public SFilter
{
    Q_OBJECT
    QML_CONSTANT_PROPERTY(QItemSelectionModel*, selectedTagsModel);
public:
    explicit TagFilter(QObject *parent = nullptr);
    virtual void init() override;
    virtual void clearAll() override;
    virtual void revertChanges() override;
    virtual void remove(const QVariant& id) override;
    virtual SFilter::Type type() override;
    virtual QString &apply(QString &query) override;

private:
    QItemSelection m_tagsSelection;
};

#endif // TAGFILTER_H
