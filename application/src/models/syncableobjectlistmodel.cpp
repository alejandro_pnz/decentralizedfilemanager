/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "syncableobjectlistmodel.h"

#include <QQmlEngine>

#include <QDebug>

#include <AuthenticationManager.h>
#include <DatabaseManager.h>
#include <ImportManager.h>
#include <SyncManager.h>
#include <SyncFileJob.h>

#include "utility.h"

SyncableObjectListModel *SyncableObjectListModel::get()
{
    return TSingleton<SyncableObjectListModel>::instance();
}

SyncableObjectListModel::~SyncableObjectListModel()
{

}

QVariant SyncableObjectListModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    QVariant d = QSListModel::data(index, role);

    if (d.isValid()) {
        if (roleNames().value(role) == "objectType") {
            return d.value<SyncableObject::ObjectType>();
        }
        return d;
    }

    const auto roleName = roleNames().value(role);

    if (roleName == "object") {
        const int objectTypeRole = roleNames().key("objectType");
        auto objectType = QSListModel::data(index, objectTypeRole).value<SyncableObject::ObjectType>();

        switch(objectType) {
        case SyncableObject::Arca: {
            const ArcaObject arca = DatabaseManager::get()->arca(QSListModel::data(index, roleNames().key("id")).toUuid());

            if (arca.isValid()) {
                ArcaObject *qmlArca = new ArcaObject(arca);
                qmlArca->detach();
                QQmlEngine::setObjectOwnership(qmlArca, QQmlEngine::JavaScriptOwnership);
                return QVariant::fromValue(qmlArca);
            }
            break;
        }
        case SyncableObject::Quod: {
            const QuodObject quod = DatabaseManager::get()->quod(QSListModel::data(index, roleNames().key("id")).toUuid());

            if (quod.isValid()) {
                QuodObject *qmlQuod = new QuodObject(quod);
                qmlQuod->detach();
                QQmlEngine::setObjectOwnership(qmlQuod, QQmlEngine::JavaScriptOwnership);
                return QVariant::fromValue(qmlQuod);
            }
            break;
        }
        case SyncableObject::File: {
            const FileObject file = DatabaseManager::get()->file(QSListModel::data(index, roleNames().key("id")).toUuid());

            if (file.isValid()) {
                FileObject *qmlFile = new FileObject(file);
                qmlFile->detach();
                QQmlEngine::setObjectOwnership(qmlFile, QQmlEngine::JavaScriptOwnership);
                return QVariant::fromValue(qmlFile);
            }
            break;
        }
        default: break;
        }
    }
    else if (roleName == "content") {
        const auto id = QSListModel::data(index, roleNames().key("id")).toUuid();
        if (m_queuedResult.exists(id, roleName)) {
            if (!m_queuedResult.pending(id, roleName)) {
                return m_queuedResult.result(id, roleName);
            }
        }
        else {
            m_queuedResult.enqueue(id, roleName, QSListModel::data(index, roleNames().key("objectType")));
        }

        // Return empty string to avoid qml warning
        return QString{""};
    }
    else if (roleName == "progress") {
        auto job = m_jobs.value(QSListModel::data(index, roleNames().key("id")).toUuid(), nullptr);
        return QVariant::fromValue(job);
    }
    else if (roleName == "available") {
        const auto objectType = QSListModel::data(index, roleNames().key("objectType")).value<SyncableObject::ObjectType>();
        if (objectType == SyncableObject::File) {
            const FileObject file = DatabaseManager::get()->file(QSListModel::data(index, roleNames().key("id")).toUuid());
            return file.available();
        }
        return true;
    }

    return {};
}

bool SyncableObjectListModel::anyIsQuodType(const QModelIndexList &indexes) const
{
    const int role = roleNames().key("objectType", -1);
    if (role == -1) {
        return false;
    }
    return std::any_of(indexes.constBegin(), indexes.constEnd(), [&role](const auto &index) {
        return index.data(role).template value<SyncableObject::ObjectType>() == SyncableObject::Quod;
    });
}

bool SyncableObjectListModel::anyIsFileType(const QModelIndexList &indexes) const
{
    const int role = roleNames().key("objectType", -1);
    if (role == -1) {
        return false;
    }
    return std::any_of(indexes.constBegin(), indexes.constEnd(), [&role](const auto &index) {
        return index.data(role).template value<SyncableObject::ObjectType>() == SyncableObject::File;
    });
}

bool SyncableObjectListModel::anyIsArcaType(const QModelIndexList &indexes) const
{
    const int role = roleNames().key("objectType", -1);
    if (role == -1) {
        return false;
    }
    return std::any_of(indexes.constBegin(), indexes.constEnd(), [&role](const auto &index) {
        return index.data(role).template value<SyncableObject::ObjectType>() == SyncableObject::Arca;
    });
}

void SyncableObjectListModel::pinList(const QModelIndexList &indexes, const bool pin)
{
    qDebug() << Q_FUNC_INFO << "PIN" << pin;
    const auto objects = objectList(indexes);
    for (const auto& object : objects) {
        auto syncable = fromVariant(object);
        qDebug() << Q_FUNC_INFO << "IS VALID" << syncable.isValid();
        if (syncable.isValid()) {
            qDebug() << syncable.name() << syncable.objectType() << syncable.pinned() << "Change to" << pin;
            syncable.setPinned(pin);
        }
    }

    for (const auto& object : objects) {
        auto syncable = fromVariant(object);
        qDebug() << syncable.name() << syncable.pinned();
    }
    bool ret = DatabaseManager::get()->update(objects);
    qDebug() << Q_FUNC_INFO << "UPDATED" << ret << objects.size();
}

void SyncableObjectListModel::pinOne(const QUuid &id, SyncableObject::ObjectType type)
{
    SyncableObject object;
    switch (type) {
    case SyncableObject::Arca:
        object = DatabaseManager::get()->arca(id);
        break;
    case SyncableObject::Quod:
        object = DatabaseManager::get()->quod(id);
        break;
    case SyncableObject::File:
        object = DatabaseManager::get()->file(id);
        break;
    default:
        break;
    }

    BaseModel::pinOne(object);
}

void SyncableObjectListModel::lockList(const QModelIndexList &indexes, const bool lock)
{
    const auto objects = objectList(indexes);
    for (const auto& object : objects) {
       if (object.canConvert<QuodObject>()) {
           auto quod = object.value<QuodObject>();
           quod.setLocked(lock);
       } else if (object.canConvert<FileObject>()) {
           auto file = object.value<FileObject>();
           file.setLocked(lock);
       }
    }
    DatabaseManager::get()->update(objects);
}

void SyncableObjectListModel::lockOne(const QUuid &id, SyncableObject::ObjectType type)
{
    BaseModel::lockOne(object(id, type));
}

bool SyncableObjectListModel::anyLocked(const QModelIndexList &indexes) const
{
    const auto objects = objectList(indexes);
    for (const auto& object : objects) {
       auto quod = object.value<QuodObject>();
       if (quod.isValid()) {
           if (quod.locked()) return true;
       } else {
           auto file = object.value<FileObject>();
           if (file.isValid()){
               if (file.locked()) return true;
           }
       }
    }
    return false;
}

bool SyncableObjectListModel::addedInArca(const QUuid &id, SyncableObject::ObjectType type)
{
    ArcaObjectList arcaList;
    switch (type) {
    case SyncableObject::Quod:
        arcaList = DatabaseManager::get()->linkedArca(DatabaseManager::get()->quod(id));
        break;
    case SyncableObject::File:
        arcaList = DatabaseManager::get()->linkedArca(DatabaseManager::get()->file(id));
        break;
    default:
        break;
    }
    return !arcaList.isEmpty();
}

bool SyncableObjectListModel::anyAddedInArca(const QModelIndexList &indexes)
{
    const auto objects = objectList(indexes);
    for (const auto& object : objects) {
       auto quod = object.value<QuodObject>();
       if (quod.isValid()) {
           if(!DatabaseManager::get()->linkedArca(quod).isEmpty()) return true;
       } else {
           auto file = object.value<FileObject>();
           if (file.isValid()){
               if (!DatabaseManager::get()->linkedArca(file).isEmpty()) return true;
           }
       }
    }
    return false;
}

QVariantList SyncableObjectListModel::objectList(const QModelIndexList &indexes) const
{
    ArcaObjectList arcaList;
    QuodObjectList quodList;
    FileObjectList fileList;

    const int role = roleNames().key("objectType");
    const int idRole = roleNames().key("id");

    for (const auto& idx: indexes) {
        const auto id = QSListModel::data(idx, idRole).toUuid();
        switch(idx.data(role).value<SyncableObject::ObjectType>()) {
        case SyncableObject::Arca:
            arcaList << DatabaseManager::get()->arca(id);
            break;
        case SyncableObject::Quod:
            quodList << DatabaseManager::get()->quod(id);
            break;
        case SyncableObject::File:
            fileList << DatabaseManager::get()->file(id);
            break;
        }
    }

    return QVariant::fromValue(arcaList).value<QVariantList>() + QVariant::fromValue(quodList).value<QVariantList>() + QVariant::fromValue(fileList).value<QVariantList>();
}

QList<QSharedPointer<SyncableObject> > SyncableObjectListModel::objectPtrList(const QModelIndexList &indexes) const
{
    const int role = roleNames().key("objectType");
    const int idRole = roleNames().key("id");
    QList<QSharedPointer<SyncableObject>> list;
    for (const auto& idx: indexes) {
        const auto id = QSListModel::data(idx, idRole).toUuid();
        const auto type = idx.data(role).value<SyncableObject::ObjectType>();
        list << object(id, type);
    }
    return list;
}

QSharedPointer<SyncableObject> SyncableObjectListModel::object(const QUuid &id, SyncableObject::ObjectType type) const
{
    QSharedPointer<SyncableObject> object;
    switch(type) {
    case SyncableObject::Arca:
        object.reset(new ArcaObject(DatabaseManager::get()->arca(id)));
        break;
    case SyncableObject::Quod:
        object.reset(new QuodObject(DatabaseManager::get()->quod(id)));
        break;
    case SyncableObject::File:
        object.reset(new FileObject(DatabaseManager::get()->file(id)));
    default:
        break;
    }
    return object;
}

SyncableObjectListModel::SyncableObjectListModel()
{
    m_queuedResult.setGroupResults(true);

    m_queuedResult.setDelegate([](const QUuid &id, const QString &role, const QVariant &data) -> QVariant {
        QVariant ret;
        if (role == "content") {
            const SyncableObject::ObjectType objectType = static_cast<SyncableObject::ObjectType>(data.toInt());

            switch(objectType) {
            case SyncableObject::Arca: {
                ArcaObject arca = DatabaseManager::get()->arca(id);
                ret = QVariant(QuodArca::Utility::get()->formatArcaContent(arca.quodCount(), arca.filesCount()));
            }
            break;
            case SyncableObject::File:
                ret = QVariant(QuodArca::Utility::get()->formatFileContent(id));
            break;
            case SyncableObject::Quod:
                ret = QVariant(QuodArca::Utility::get()->formatQuodContent(id));
            break;
            default:
                break;
            }
        }

        return ret;
    });

    setRoleNames({"id", "name", "objectType", "pinned", "locked", "object", "color", "fileType", "size", "syncProperty", "created", "modified", "content", "progress", "available"});
    m_cachedRoleNames = {roleNames().key("content"), roleNames().key("object"), roleNames().key("available")};

    connect(AuthenticationManager::get(), &AuthenticationManager::loginStateChanged, this, &SyncableObjectListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::arcaAdded, this, &SyncableObjectListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::arcaUpdated, this, &SyncableObjectListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::arcaRemoved, this, &SyncableObjectListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::quodAdded, this, &SyncableObjectListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::quodUpdated, this, &SyncableObjectListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::quodRemoved, this, &SyncableObjectListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::filesAdded, this, &SyncableObjectListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::filesUpdated, this, &SyncableObjectListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::filesRemoved, this, &SyncableObjectListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::arcaUpdated,
            this, QOverload<QList<QUuid>>::of(&SyncableObjectListModel::invalidateCache));
    connect(DatabaseManager::get(), &DatabaseManager::filesUpdated,
            this, QOverload<QList<QUuid>>::of(&SyncableObjectListModel::invalidateCache));
    connect(DatabaseManager::get(), &DatabaseManager::quodUpdated,
            this, QOverload<QList<QUuid>>::of(&SyncableObjectListModel::invalidateCache));

    connect(SyncManager::get(), &SyncManager::fileReceived, this, &SyncableObjectListModel::addJob, Qt::QueuedConnection);

    connect(ImportManager::get(), &ImportManager::jobsAdded, this, [this](const QList<ImportFileJob *> &jobs) {
        for (auto *job: jobs) {
            addJob(job);
        }
    }, Qt::QueuedConnection);

    connect(&m_queuedResult, &QueuedResult::resultsReady, this, [=](const QMultiHash<QString, QUuid> &results) {
        const auto roles = results.uniqueKeys();
        for (const auto &role: roles) {
            QVector<int> indexes;
            const auto ids = results.values(role);
            std::transform(ids.begin(), ids.end(), std::back_inserter(indexes), [this](const auto &id) { return indexOf("id", id); });
            std::sort(indexes.begin(), indexes.end());
            qDebug() << "results ready" << indexes.size();
            Q_EMIT dataChanged(index(indexes.first(), 0), index(indexes.last(), 0), { roleNames().key(role.toUtf8()) });
        }
    }, Qt::QueuedConnection);
}

SyncableObject SyncableObjectListModel::object(const QUuid &id)
{
    return {};
}

SyncableObject SyncableObjectListModel::fromVariant(const QVariant &variant)
{
    if (variant.canConvert<ArcaObject>()) {
        return variant.value<ArcaObject>();
    } else if (variant.canConvert<QuodObject>()) {
        return variant.value<QuodObject>();
    } else if (variant.canConvert<FileObject>()) {
        return variant.value<FileObject>();
    }

    return {};
}

QSharedPointer<SyncableObject> SyncableObjectListModel::objectPtr(const QUuid &id)
{
    return {};
}

void SyncableObjectListModel::addJob(AbstractJob *job)
{
    if (!job) {
        return;
    }

    auto addNewJob = [=]() {
        qDebug() << job->id();
        m_jobs[job->id()] = job;
        const auto idx = indexOf("id", job->id());
        Q_EMIT dataChanged(index(idx, 0), index(idx, 0), { roleNames().key("progress") });
    };

    if (job->id().isNull()) {
        connect(job, &AbstractJob::idChanged, this, addNewJob, Qt::QueuedConnection);
    }
    else {
        addNewJob();
    }

    connect(job, &AbstractJob::finished, this, [=]() {
        m_jobs.remove(job->id());
        const auto idx = indexOf("id", job->id());
        Q_EMIT dataChanged(index(idx, 0), index(idx, 0), { roleNames().key("progress"), roleNames().key("available") });
    }, Qt::QueuedConnection);
}

QVariantList SyncableObjectListModel::source() const
{
    ArcaObjectList arcaList = DatabaseManager::get()->allArcas();
    QuodObjectList quodList = DatabaseManager::get()->allQuods();
    FileObjectList fileList = DatabaseManager::get()->allFiles();

    return { QVariant::fromValue(arcaList).toList() + QVariant::fromValue(quodList).toList() + QVariant::fromValue(fileList).toList() };
}
