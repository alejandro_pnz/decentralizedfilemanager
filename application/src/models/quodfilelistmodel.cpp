/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "quodfilelistmodel.h"

#include <QQmlEngine>

#include <QDebug>

#include "AuthenticationManager.h"
#include "DatabaseManager.h"
#include "utility.h"
#include "QSDiffRunner"

QuodFileListModel *QuodFileListModel::get()
{
    return TSingleton<QuodFileListModel>::instance();
}

QuodFileListModel::~QuodFileListModel()
{

}

QVariant QuodFileListModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    QVariant d = QSListModel::data(index, role);

    if (d.isValid()) {
        if (roleNames().value(role) == "objectType") {
            return d.value<SyncableObject::ObjectType>();
        }
        return d;
    }

    const auto roleName = roleNames().value(role);

    if (roleName == "object") {
        SyncableObject::ObjectType objectType = static_cast<SyncableObject::ObjectType>(QSListModel::data(index, roleNames().key("objectType")).toInt());

        if (objectType == SyncableObject::Quod) {
            QuodObject quod = DatabaseManager::get()->quod(QSListModel::data(index, roleNames().key("id")).toUuid());

            if (quod.isValid()) {
                QuodObject *qmlQuod = new QuodObject(quod);
                qmlQuod->detach();
                QQmlEngine::setObjectOwnership(qmlQuod, QQmlEngine::JavaScriptOwnership);
                return QVariant::fromValue(qmlQuod);
            }
        }
        else if (objectType == SyncableObject::File) {
            FileObject file = DatabaseManager::get()->file(QSListModel::data(index, roleNames().key("id")).toUuid());

            if (file.isValid()) {
                FileObject *qmlFile = new FileObject(file);
                qmlFile->detach();
                QQmlEngine::setObjectOwnership(qmlFile, QQmlEngine::JavaScriptOwnership);
                return QVariant::fromValue(qmlFile);
            }
        }
    } else if (roleName == "content") {
        const auto id = QSListModel::data(index, roleNames().key("id")).toUuid();
        if (m_queuedResult.exists(id, roleName)) {
            if (!m_queuedResult.pending(id, roleName)) {
                return m_queuedResult.result(id, roleName);
            }
        }
        else {
            m_queuedResult.enqueue(id, roleName, QSListModel::data(index, roleNames().key("objectType")));
        }
    }

    return {};
}

ArcaObjectPtr QuodFileListModel::arca(const QUuid &id) const
{
    ArcaObject arca = DatabaseManager::get()->arca(id);

    if (arca.isValid()) {
        return ArcaObjectPtr(new ArcaObject(arca));
    }

    return nullptr;
}

QModelIndex QuodFileListModel::indexOf(const QUuid &id) const
{
    return index(QSListModel::indexOf("id", id), 0);
}

int QuodFileListModel::idRole() const
{
    return roleNames().key("id");
}

QuodFileListModel::QuodFileListModel()
{
    m_queuedResult.setGroupResults(true);

    m_queuedResult.setDelegate([](const QUuid &id, const QString &role, const QVariant &data) -> QVariant {
        QVariant ret;
        if (role == "content") {
            const SyncableObject::ObjectType objectType = static_cast<SyncableObject::ObjectType>(data.toInt());
            if (objectType == SyncableObject::Quod) {
                ret = QVariant(QuodArca::Utility::get()->formatQuodContent(id));
            }
            else if (objectType == SyncableObject::File) {
                ret = QVariant(QuodArca::Utility::get()->formatFileContent(id));
            }
        }

        return ret;
    });

    setRoleNames({"id", "name", "objectType", "color", "fileType", "locked", "pinned", "syncProperty", "content", "object"});
    m_cachedRoleNames = {roleNames().key("content"), roleNames().key("object")};

    connect(AuthenticationManager::get(), &AuthenticationManager::loginStateChanged, this, &QuodFileListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::quodAdded, this, &QuodFileListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::quodUpdated, this, &QuodFileListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::quodRemoved, this, &QuodFileListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::filesAdded, this, &QuodFileListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::filesUpdated, this, &QuodFileListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::filesRemoved, this, &QuodFileListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::filesUpdated, this, QOverload<QList<QUuid>>::of(&QuodFileListModel::invalidateCache));
    connect(DatabaseManager::get(), &DatabaseManager::quodUpdated, this, QOverload<QList<QUuid>>::of(&QuodFileListModel::invalidateCache));

    connect(&m_queuedResult, &QueuedResult::resultsReady, this, [=](const QMultiHash<QString, QUuid> &results) {
        const auto roles = results.uniqueKeys();
        for (const auto &role: roles) {
            QVector<int> indexes;
            const auto ids = results.values(role);
            std::transform(ids.begin(), ids.end(), std::back_inserter(indexes), [this](const auto &id) { return QSListModel::indexOf("id", id); });
            std::sort(indexes.begin(), indexes.end());
            qDebug() << "results ready" << indexes.size();
            Q_EMIT dataChanged(index(indexes.first(), 0), index(indexes.last(), 0), { roleNames().key(role.toUtf8()) });
        }
    }, Qt::QueuedConnection);
}

QVariantList QuodFileListModel::source() const
{
    QuodObjectList quods = DatabaseManager::get()->allQuods();
    FileObjectList files = DatabaseManager::get()->allFiles();

    return { QVariant::fromValue(quods).toList() + QVariant::fromValue(files).toList() };
}

SyncableObject QuodFileListModel::object(const QUuid &)
{
    return {};
}

QSharedPointer<SyncableObject> QuodFileListModel::objectPtr(const QUuid &)
{
    return {};
}
