/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef FILELISTMODEL_H
#define FILELISTMODEL_H

#include "singleton.h"

#include "FileObject.h"

#include "basemodel.h"

#include <QUrl>

class AbstractJob;

class FileListModel : public BaseModel
{
    Q_OBJECT

public:
    static FileListModel *get();

    ~FileListModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    FileObjectPtr file(const QUuid &id) const;

    Q_INVOKABLE void pinList(const QModelIndexList& indexes, const bool pin = false) override;
    Q_INVOKABLE void lockList(const QModelIndexList& indexes, const bool lock = false) override;

    FileObjectList objectList(const QModelIndexList& indexes);
    FileObjectList objectList(const QList<QUuid>& ids);

    Q_INVOKABLE bool importFiles(const QList<QUrl> &files);
    Q_INVOKABLE QString importFile(const QString &filePath);

protected:
    FileListModel();

    QVariantList source() const override;

    friend class TSingleton<FileListModel>;

    QVariant insertToCache(QUuid id) override;

    void addJob(AbstractJob *job);

    // BaseModel interface
protected:
    SyncableObject object(const QUuid &id) override;
    QSharedPointer<SyncableObject> objectPtr(const QUuid &id) override;    

private:
    QHash<QUuid, AbstractJob *> m_jobs;

};

#endif // FILELISTMODEL_H
