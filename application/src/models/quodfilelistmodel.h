/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef QUODFILEPROXYMODEL_H
#define QUODFILEPROXYMODEL_H

#include "singleton.h"

#include "ArcaObject.h"
#include "basemodel.h"
#include "QSListModel"

#include "queuedresult.h"

class QuodFileListModel : public BaseModel
{
    Q_OBJECT

public:

    static QuodFileListModel *get();
    ~QuodFileListModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    ArcaObjectPtr arca(const QUuid &id) const;
    QModelIndex indexOf(const QUuid &id) const;
    int idRole() const;

protected:
    QuodFileListModel();

    QVariantList source() const override;

    friend class TSingleton<QuodFileListModel>;

    // BaseModel interface
protected:
    SyncableObject object(const QUuid &id) override;
    QSharedPointer<SyncableObject> objectPtr(const QUuid &id) override;

};

#endif // QUODFILEPROXYMODEL_H
