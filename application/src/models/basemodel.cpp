#include "basemodel.h"
#include <QDebug>
#include <DatabaseManager.h>

BaseModel::BaseModel(QObject *parent)
    : BaseListModel(parent)
{

}

void BaseModel::pinList(const QModelIndexList &indexes, const bool pin)
{

}

void BaseModel::pinOne(const QUuid &id, SyncableObject::ObjectType)
{
    qDebug() << Q_FUNC_INFO << id;
    pinOne(object(id));
}


void BaseModel::lockList(const QModelIndexList &indexes, const bool pin)
{

}

/*!
 * \brief Tries to toggle the lock property of object with given \a id.
 * If object has no locked property nothing happen
 */
void BaseModel::lockOne(const QUuid &id)
{
    lockOne(objectPtr(id));
}

bool BaseModel::allPinned(const QModelIndexList &indexes) const
{
    return allTrue(indexes, "pinned");
}

bool BaseModel::anyPinned(const QModelIndexList &indexes) const
{
    return anyTrue(indexes, "pinned");
}

bool BaseModel::anyLocked(const QModelIndexList &indexes) const
{
    return anyTrue(indexes, "locked");
}

bool BaseModel::allLocked(const QModelIndexList &indexes) const
{
    return allTrue(indexes, "locked");
}

QList<QUuid> BaseModel::idsFromModelIndexes(const QModelIndexList &indexes) const
{
    const int idRole = roleNames().key("id", -1);
    if (idRole == -1) {
        qWarning() << Q_FUNC_INFO << "No id role name defined!";
        return {};
    }
    QList<QUuid> idList;
    for (const auto& index : indexes) {
        idList << QSListModel::data(index, idRole).toUuid();
    }
    return idList;
}

void BaseModel::pinOne(SyncableObject o)
{
    if (o.isValid()) {
        o.setPinned(!o.pinned());
        o.save();
    }
}

void BaseModel::lockOne(QSharedPointer<SyncableObject> o)
{
    if (o) {
        auto property = (o)->property("locked");
        if (property.isValid()) {
            o->setProperty("locked", !property.toBool());
            o->save();
        }
    }
}

void BaseModel::invalidateCache(QList<QUuid> list)
{
    for (const auto& id : list) {
        invalidateCache(id);
    }
}

void BaseModel::invalidateCache(const QUuid &id)
{
    for (const auto role: qAsConst(m_cachedRoleNames)) {
        m_queuedResult.dequeue(id, roleNames().value(role));
    }

    m_cache.remove(id);
    insertToCache(id);
    const auto idx = indexOf("id", id);
    Q_EMIT dataChanged(index(idx, 0), index(idx,0), m_cachedRoleNames);
}

QVariant BaseModel::insertToCache(QUuid)
{
    // Nothing
    return {};
}

QVariant BaseModel::cachedValue(QUuid id)
{
    if (m_cache.contains(id)) return m_cache.value(id);

    return insertToCache(id);
}

bool BaseModel::anyTrue(const QModelIndexList &indexes, const QByteArray &roleName) const
{
    const int role = roleNames().key(roleName, -1);
    if (role == -1) return false;
    return std::any_of(indexes.constBegin(), indexes.constEnd(), [&role](const auto& index) { return index.data(role).toBool(); });
}

bool BaseModel::allTrue(const QModelIndexList &indexes, const QByteArray &roleName) const
{
    const int role = roleNames().key(roleName, -1);
    if (role == -1) return false;
    return std::all_of(indexes.constBegin(), indexes.constEnd(), [&role](const auto& index) { return index.data(role).toBool(); });
}
