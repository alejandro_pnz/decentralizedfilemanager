#include "attributesectionmodel.h"

#include <QQmlEngine>

#include "DatabaseManager.h"

#include "QSDiffRunner"

AttributeSectionModel::AttributeSectionModel(QObject *parent)
    : QSListModel(parent)
{
    setKeyField("id");
    setRoleNames({"id", "name", "type", "object", "value", "values", "history", "sensitive", "placeholder", "maximumLength", "isEmpty"});

    connect(this, &AttributeSectionModel::sectionChanged, this, [=]() {
        qDebug() << "Section changed";
        if (m_section) {
            setSource(QVariant::fromValue(m_section->attributes()).toList());
        }
    });
}

AttributeSectionModel::~AttributeSectionModel()
{

}

QVariant AttributeSectionModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    QVariant d = QSListModel::data(index, role);

    if (d.isValid()) {
        return d;
    }

    const auto roleName = roleNames().value(role);
    if (roleName == "object" && m_section) {
        auto attributes = m_section->attributes();
        const auto attributeId = QSListModel::data(index, roleNames().key("id")).toUuid();
        const auto it = std::find_if(attributes.begin(), attributes.end(), [&attributeId](const AttributeObject &attribute) {
            return attribute.id() == attributeId;
        });

        if (it == attributes.end()) {
            return {};
        }

        if (it->isValid()) {
            AttributeObject *qmlAttribute = new AttributeObject(*it);
            qmlAttribute->detach();
            QQmlEngine::setObjectOwnership(qmlAttribute, QQmlEngine::JavaScriptOwnership);
            qDebug() << "Attribute Object" << qmlAttribute->name() << qmlAttribute->value();
            return QVariant::fromValue(qmlAttribute);
        }
    } else if (roleName == "isEmpty") {
        const auto valueRole = roleNames().key("value");
        const auto nameRole = roleNames().key("name");
        const QVariant v = QSListModel::data(index, valueRole);
        qDebug() << "IS EMPTY" << QSListModel::data(index, nameRole).toString() << v.toString().isEmpty() << v;
        return v.toString().isEmpty();
    }

    return {};
}

QVariantList AttributeSectionModel::source() const
{
    return m_source;
}

void AttributeSectionModel::setSource(const QVariantList &source)
{
    qDebug().nospace() << this->metaObject()->className() << "::setSource(" << source << ")";

    m_source = source;
    Q_EMIT this->sourceChanged();

    sync();
}

void AttributeSectionModel::revertChanges()
{
    if (!m_section) return;
    auto attributes = m_section->attributes();
    for (auto& attr : attributes) {
        attr.revert();
    }
}

void AttributeSectionModel::setKeyField(const QString &keyField)
{
    m_keyField = keyField;
}

void AttributeSectionModel::sync()
{
    qDebug() << "AttributeSectionModel::sync()";

    QSDiffRunner runner;
    runner.setKeyField(m_keyField);

    QList<QSPatch> patches = runner.compare(storage(), m_source);


    if (!patches.isEmpty()) {
        runner.patch(this, patches);
    } else {
        qDebug () << "AttributeSectionModel::sync(): Nothing to patch";
    }
}

