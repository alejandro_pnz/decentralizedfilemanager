/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef QUODSECTIONSMODEL_H
#define QUODSECTIONSMODEL_H

#include <QuodObject.h>
#include <QuodSectionObject.h>

#include "QSListModel"

class QuodSectionsModel : public QSListModel
{
    Q_OBJECT
    Q_PROPERTY(QuodObject* quod READ quod WRITE setQuod NOTIFY quodChanged)

public:
    QuodSectionsModel();

    ~QuodSectionsModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QuodSectionObjectPtr quodSection(const QUuid &id) const;

    QVariantList source() const;
    void setSource(const QVariantList &source);

    QuodObject *quod() const;
    void setQuod(QuodObject *newQuod);

Q_SIGNALS:
    void sourceChanged();
    void quodChanged();

protected:
    void setKeyField(const QString &keyField);

    void sync();

private:
    QString m_keyField;

    QVariantList m_source;
    QuodObject *m_quod{nullptr};

    void quodUpdated(const QList<QUuid>& list);
    bool handleSpecialSectionRole(const QModelIndex& index) const;
};

#endif // QUODSECTIONSMODEL_H
