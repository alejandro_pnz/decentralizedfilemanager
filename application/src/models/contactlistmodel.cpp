/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "contactlistmodel.h"

#include <QQmlEngine>
#include <QCoreApplication>

#include "AuthenticationManager.h"
#include "ContactManager.h"

ContactListModel *ContactListModel::get()
{
    return TSingleton<ContactListModel>::instance();
}

ContactListModel::~ContactListModel()
{

}

QVariant ContactListModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    QVariant d = QSListModel::data(index, role);

    if (d.isValid()) {
        return d;
    }

    auto roleName = roleNames().value(role);

    if (roleName == "object") {
        ContactObject contact = ContactManager::get()->contact(QSListModel::data(index, roleNames().key("id")).toUuid());

        if (contact.isValid()) {
            ContactObject *qmlContact = new ContactObject(contact);
            qmlContact->detach();
            QQmlEngine::setObjectOwnership(qmlContact, QQmlEngine::JavaScriptOwnership);
            return QVariant::fromValue(qmlContact);
        }
    }

    return {};
}

void ContactListModel::addContact(const QString &email)
{
    ContactManager::get()->addContact(email);
}

ContactListModel::ContactListModel()
    : BaseListModel()
{
    setKeyField("id");
    setRoleNames({"id", "name", "email", "pinned", "online", "pending", "object"});

    connect(AuthenticationManager::get(), &AuthenticationManager::loginStateChanged, this, &ContactListModel::sync);

    connect(ContactManager::get(), &ContactManager::contactsAdded, this, &ContactListModel::sync);
    connect(ContactManager::get(), &ContactManager::contactsUpdated, this, &ContactListModel::sync);
    connect(ContactManager::get(), &ContactManager::contactsRemoved, this, &ContactListModel::sync);
}

QVariantList ContactListModel::source() const
{
    auto contacts = ContactManager::get()->allContacts();

    return QVariant::fromValue(contacts).toList();
}

void _q_ContactListModel_registerTypes() {
    qmlRegisterSingletonType<ContactListModel>("com.test", 1, 0, "ContactListModel", [](QQmlEngine *, QJSEngine *) -> QObject * {
        return ContactListModel::get();
    });
}

Q_COREAPP_STARTUP_FUNCTION(_q_ContactListModel_registerTypes)
