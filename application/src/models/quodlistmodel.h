/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef QUODLISTMODEL_H
#define QUODLISTMODEL_H

#include "singleton.h"

#include "QuodObject.h"

#include "basemodel.h"

#include "queuedresult.h"

class QuodListModel : public BaseModel
{
    Q_OBJECT

public:
    static QuodListModel *get();

    ~QuodListModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QuodObjectPtr quod(const QUuid &id);

    Q_INVOKABLE void pinList(const QModelIndexList& indexes, const bool pin = false) override;
    Q_INVOKABLE void lockList(const QModelIndexList& indexes, const bool lock = false) override;

    QuodObjectList objectList(const QModelIndexList& indexes);
    QuodObjectList objectList(const QList<QUuid>& ids);

protected:
    QuodListModel();

    QVariantList source() const override;

    friend class TSingleton<QuodListModel>;

    // BaseModel interface
protected:
    SyncableObject object(const QUuid &id) override;
    QSharedPointer<SyncableObject> objectPtr(const QUuid &id) override;

};

#endif // QUODLISTMODEL_H
