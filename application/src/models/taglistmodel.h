#ifndef TAGLISTMODEL_H
#define TAGLISTMODEL_H

#include "singleton.h"

#include "QSListModel"
#include <QUuid>

#include <TagObject.h>

class TagListModel : public QSListModel
{
    Q_OBJECT

public:
    static TagListModel *get();

    ~TagListModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE QVariant addTag(const QString &name, const QString &color);
    Q_INVOKABLE void updateTag(TagObject *obj);
    Q_INVOKABLE bool tagNameExists(const QString &name) const;
    Q_INVOKABLE bool proceedTagDeletion();
    Q_INVOKABLE void requestTagDeletion(const QUuid& id);
    Q_INVOKABLE void abortTagDeletion();
    QStringList getTagNames(const QModelIndexList& indexes);
signals:
    void tagDeleted(const QUuid& id);
protected:
    TagListModel();

    void setKeyField(const QString &keyField);

    void sync();

    friend class TSingleton<TagListModel>;

private:
    QString m_keyField;
    QUuid m_tagToDelete;

    TagObjectList m_predefinedTags;
};

#endif // TAGLISTMODEL_H
