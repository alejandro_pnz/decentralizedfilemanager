/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "arcalistmodel.h"

#include <QQmlEngine>

#include <QDebug>

#include "utility.h"
#include "AuthenticationManager.h"
#include "DatabaseManager.h"

#include "QSDiffRunner"

ArcaListModel *ArcaListModel::get()
{
    return TSingleton<ArcaListModel>::instance();
}

ArcaListModel::~ArcaListModel()
{

}

QVariant ArcaListModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    QVariant d = QSListModel::data(index, role);

    if (d.isValid()) {
        return d;
    }

    auto roleName = roleNames().value(role);

    if (roleName == "object") {
        ArcaObject arca = DatabaseManager::get()->arca(QSListModel::data(index, roleNames().key("id")).toUuid());

        if (arca.isValid()) {
            ArcaObject *qmlArca = new ArcaObject(arca);
            qmlArca->detach();
            QQmlEngine::setObjectOwnership(qmlArca, QQmlEngine::JavaScriptOwnership);
            return QVariant::fromValue(qmlArca);
        }
    } else if (roleName == "content") {
        const auto id = QSListModel::data(index, roleNames().key("id")).toUuid();
        if (m_queuedResult.exists(id, roleName)) {
            if (!m_queuedResult.pending(id, roleName)) {
                return m_queuedResult.result(id, roleName);
            }
        }
        else {
            m_queuedResult.enqueue(id, roleName);
        }
    }

    return {};
}

ArcaObjectPtr ArcaListModel::arca(const QUuid &id) const
{
    ArcaObject arca = DatabaseManager::get()->arca(id);

    if (arca.isValid()) {
        return ArcaObjectPtr(new ArcaObject(arca));
    }

    return nullptr;
}


ArcaListModel::ArcaListModel()
{
    m_queuedResult.setDelegate([](const QUuid &id, const QString &role, const QVariant &) -> QVariant {
        if (role == "content") {
            const ArcaObject arca = DatabaseManager::get()->arca(id);
            return QVariant(QuodArca::Utility::get()->formatArcaContent(arca.quodCount(), arca.filesCount()));
        }

        return {};
    });

    setKeyField("id");
    setRoleNames({"id", "name", "color", "pinned", "object", "syncProperty", "created", "modified", "content"});
    m_cachedRoleNames = {roleNames().key("content"), roleNames().key("object")};
    connect(AuthenticationManager::get(), &AuthenticationManager::loginStateChanged, this, &ArcaListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::arcaAdded, this, &ArcaListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::arcaUpdated, this, &ArcaListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::arcaRemoved, this, &ArcaListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::arcaUpdated, this, QOverload<QList<QUuid>>::of(&ArcaListModel::invalidateCache));

    connect(&m_queuedResult, &QueuedResult::resultReady, this, [=](const QUuid &id, const QString &roleName) {
        const auto idx = indexOf("id", id);
        Q_EMIT dataChanged(index(idx, 0), index(idx, 0), { roleNames().key(roleName.toUtf8()) });
    }, Qt::QueuedConnection);
}

QVariantList ArcaListModel::source() const
{
    ArcaObjectList source = DatabaseManager::get()->allArcas();

    return QVariant::fromValue(source).toList();
}

void ArcaListModel::pinList(const QModelIndexList& indexes, const bool pin)
{
    const int idRole = roleNames().key("id");
    ArcaObjectList list;
    for (const auto& index : indexes) {
        ArcaObject object = DatabaseManager::get()->arca(QSListModel::data(index, idRole).toUuid());
        if (object.isValid()) {
            object.setPinned(pin);
            list << object;
        }
    }
    DatabaseManager::get()->update(list);
}

ArcaObjectList ArcaListModel::objectList(const QModelIndexList &indexes)
{
    const int idRole = roleNames().key("id");
    ArcaObjectList list;
    for (const auto& index : indexes) {
       list << DatabaseManager::get()->arca(QSListModel::data(index, idRole).toUuid());
    }
    return list;
}

ArcaObjectList ArcaListModel::objectList(const QList<QUuid> &ids)
{
    ArcaObjectList list;
    for (const auto& id : ids) {
       list << DatabaseManager::get()->arca(id);
    }
    return list;
}

SyncableObject ArcaListModel::object(const QUuid &id)
{
    return DatabaseManager::get()->arca(id);
}

QSharedPointer<SyncableObject> ArcaListModel::objectPtr(const QUuid &id)
{
    return arca(id);
}
