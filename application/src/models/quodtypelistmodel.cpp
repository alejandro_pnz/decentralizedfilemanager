#include "quodtypelistmodel.h"

#include <QQmlEngine>

#include <QDebug>

#include "AuthenticationManager.h"
#include "DatabaseManager.h"

#include "QSDiffRunner"

QuodTypeListModel::~QuodTypeListModel()
{

}

QVariant QuodTypeListModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    QVariant d = QSListModel::data(index, role);

    if (d.isValid()) {
        return d;
    }

    if (roleNames().value(role) == "object") {
        QuodTypeObject quodType = DatabaseManager::get()->quodType(QSListModel::data(index, roleNames().key("id")).toUuid());

        if (quodType.isValid()) {
            QuodTypeObject *qmlQuodType = new QuodTypeObject(quodType);
            qmlQuodType->detach();
            QQmlEngine::setObjectOwnership(qmlQuodType, QQmlEngine::JavaScriptOwnership);
            return QVariant::fromValue(qmlQuodType);
        }
    }

    return {};
}

QuodTypeObjectPtr QuodTypeListModel::quodType(const QUuid &id) const
{
    QuodTypeObject quodType = DatabaseManager::get()->quodType(id);

    if (quodType.isValid()) {
        return QuodTypeObjectPtr(new QuodTypeObject(quodType));
    }

    return nullptr;
}

QStringList QuodTypeListModel::idsStringList(const QModelIndexList &indexes) const
{
    auto idRole = roleNames().key("id");
    QStringList list;
    for (const auto& index : indexes) {
        list.append(data(index, idRole).toUuid().toString());
    }
    return list;
}

QuodTypeListModel::QuodTypeListModel(QObject* parent) : BaseListModel(parent)
{
    setKeyField("id");
    setRoleNames({"id", "name", "iconPath", "subTypes", "object"});

    connect(AuthenticationManager::get(), &AuthenticationManager::loginStateChanged, this, &QuodTypeListModel::sync);
}
