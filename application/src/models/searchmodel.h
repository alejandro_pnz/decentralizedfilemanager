/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef SEARCHMODEL_H
#define SEARCHMODEL_H

#include "SearchManager.h"

#include "syncableobjectlistmodel.h"
#include <qqmlhelpers.h>
#include "searchfilters.h"


class SearchModel : public BaseModel
{
    Q_OBJECT    
    QML_CONSTANT_PROPERTY(SearchFilters*, filters);
public:
    SearchModel();
    ~SearchModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE void search(const QString &query);
    Q_INVOKABLE void updateFilters();
    Q_INVOKABLE void removeFilter(SFilter::Type filter, const QVariant& id = {});

protected:
    SyncableObject object(const QUuid& id) override;
    QSharedPointer<SyncableObject> objectPtr(const QUuid &id) override;
    QVariantList source() const override;

private:
    SearchManager m_manager;
    QString m_searchedText;
};

#endif // SEARCHMODEL_H
