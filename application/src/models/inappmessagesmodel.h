/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef INAPPMESSAGESMODEL_H
#define INAPPMESSAGESMODEL_H

#include "baselistmodel.h"

class InAppMessagesModel : public BaseListModel
{
public:
    InAppMessagesModel(QObject *parent = nullptr);

    ~InAppMessagesModel();

protected:
    QVariantList source() const override;
};

#endif // INAPPMESSAGESMODEL_H
