
INCLUDEPATH += \
    $$PWD \
    $$PWD/../../common/src

HEADERS += \
    $$PWD/arcalistmodel.h \
    $$PWD/associatedtaglistmodel.h \
    $$PWD/attributesectionmodel.h \
    $$PWD/basemodel.h \
    $$PWD/contactlistmodel.h \
    $$PWD/defaultattributesmodel.h \
    $$PWD/devicelistmodel.h \
    $$PWD/filelistmodel.h \
    $$PWD/inappmessagesmodel.h \
    $$PWD/queuedresult.h \
    $$PWD/quodfilelistmodel.h \
    $$PWD/quodlistmodel.h \
    $$PWD/quodsectionsmodel.h \
    $$PWD/quodtypelistmodel.h \
    $$PWD/recyclebinmodel.h \
    $$PWD/search_filters/datefilter.h \
    $$PWD/search_filters/filetypefilter.h \
    $$PWD/search_filters/filters.h \
    $$PWD/search_filters/lockstatusfilter.h \
    $$PWD/search_filters/quickfilters.h \
    $$PWD/search_filters/quodtypefilter.h \
    $$PWD/search_filters/sfilter.h \
    $$PWD/search_filters/syncstatusfilter.h \
    $$PWD/search_filters/tagfilter.h \
    $$PWD/searchfilters.h \
    $$PWD/searchmodel.h \
    $$PWD/searchproxymodel.h \
    $$PWD/syncableobjectlistmodel.h \
    $$PWD/taglistmodel.h \
    $$PWD/userlistmodel.h \
    $$PWD/whiteblacklistuserproxymodel.h \
    $$PWD/baselistmodel.h

SOURCES += \
    $$PWD/arcalistmodel.cpp \
    $$PWD/associatedtaglistmodel.cpp \
    $$PWD/attributesectionmodel.cpp \
    $$PWD/basemodel.cpp \
    $$PWD/contactlistmodel.cpp \
    $$PWD/defaultattributesmodel.cpp \
    $$PWD/devicelistmodel.cpp \
    $$PWD/filelistmodel.cpp \
    $$PWD/inappmessagesmodel.cpp \
    $$PWD/queuedresult.cpp \
    $$PWD/quodfilelistmodel.cpp \
    $$PWD/quodlistmodel.cpp \
    $$PWD/quodsectionsmodel.cpp \
    $$PWD/quodtypelistmodel.cpp \
    $$PWD/recyclebinmodel.cpp \
    $$PWD/search_filters/datefilter.cpp \
    $$PWD/search_filters/filetypefilter.cpp \
    $$PWD/search_filters/lockstatusfilter.cpp \
    $$PWD/search_filters/quickfilters.cpp \
    $$PWD/search_filters/quodtypefilter.cpp \
    $$PWD/search_filters/sfilter.cpp \
    $$PWD/search_filters/syncstatusfilter.cpp \
    $$PWD/search_filters/tagfilter.cpp \
    $$PWD/searchfilters.cpp \
    $$PWD/searchmodel.cpp \
    $$PWD/searchproxymodel.cpp \
    $$PWD/syncableobjectlistmodel.cpp \
    $$PWD/taglistmodel.cpp \
    $$PWD/userlistmodel.cpp \
    $$PWD/whiteblacklistuserproxymodel.cpp \
    $$PWD/baselistmodel.cpp

