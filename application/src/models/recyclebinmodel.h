/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef RECYCLEBINMODEL_H
#define RECYCLEBINMODEL_H

#include "singleton.h"

#include "syncableobjectlistmodel.h"

class RecycleBinModel : public SyncableObjectListModel
{
    Q_OBJECT

public:
    static RecycleBinModel *get();

    ~RecycleBinModel();

    Q_INVOKABLE void restoreItems(const QModelIndexList& indexes);

    Q_INVOKABLE void deleteItems(const QModelIndexList& indexes);

    Q_INVOKABLE void emptyRecycleBin();

protected:
    RecycleBinModel();

    QVariantList source() const override;

    friend class TSingleton<RecycleBinModel>;
};

#endif // RECYCLEBINMODEL_H
