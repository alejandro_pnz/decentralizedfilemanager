/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef SYNCABLEOBJECTLISTMODEL_H
#define SYNCABLEOBJECTLISTMODEL_H

#include "singleton.h"

#include "SyncableObject.h"

#include "basemodel.h"

#include "queuedresult.h"

class AbstractJob;

class SyncableObjectListModel : public BaseModel
{
    Q_OBJECT

public:

    static SyncableObjectListModel *get();
    ~SyncableObjectListModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Q_INVOKABLE bool anyIsArcaType(const QModelIndexList& indexes) const;
    Q_INVOKABLE bool anyIsFileType(const QModelIndexList& indexes) const;
    Q_INVOKABLE bool anyIsQuodType(const QModelIndexList& indexes) const;

    Q_INVOKABLE void pinList(const QModelIndexList& indexes, const bool pin = false) override;
    Q_INVOKABLE void pinOne(const QUuid &id, SyncableObject::ObjectType type);
    Q_INVOKABLE void lockList(const QModelIndexList& indexes, const bool lock = false) override;
    Q_INVOKABLE void lockOne(const QUuid &id, SyncableObject::ObjectType type);
    Q_INVOKABLE bool anyLocked(const QModelIndexList& indexes) const override;

    Q_INVOKABLE bool addedInArca(const QUuid &id, SyncableObject::ObjectType type);
    Q_INVOKABLE bool anyAddedInArca(const QModelIndexList& indexes);

    QVariantList objectList(const QModelIndexList& indexes) const;
    QList<QSharedPointer<SyncableObject>> objectPtrList(const QModelIndexList& indexes) const;
    QSharedPointer<SyncableObject> object(const QUuid& id, SyncableObject::ObjectType type) const;
    static SyncableObject fromVariant(const QVariant& variant);

protected:
    SyncableObjectListModel();

    SyncableObject object(const QUuid& id) override;
    QSharedPointer<SyncableObject> objectPtr(const QUuid &id) override;

    void addJob(AbstractJob *job);

    QVariantList source() const override;

    friend class TSingleton<SyncableObjectListModel>;

private:
    QHash<QUuid, AbstractJob *> m_jobs;
};

#endif // SYNCABLEOBJECTLISTMODEL_H
