#ifndef USERLISTMODEL_H
#define USERLISTMODEL_H

#include "singleton.h"

#include "UserObject.h"

#include "basemodel.h"


class UserListModel : public BaseModel
{
    Q_OBJECT

public:

    static UserListModel *get();

    ~UserListModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE void inviteUser(const QString &email);

    Q_INVOKABLE bool anyPowerUser(const QModelIndexList& indexes) const;

    Q_INVOKABLE void pinList(const QModelIndexList& indexes, const bool pin = false) override;

    Q_INVOKABLE UserObject *qmlObject(const QUuid &id) const;

protected:
    UserListModel();

    QVariantList source() const override;

    friend class TSingleton<UserListModel>;

protected:
    SyncableObject object(const QUuid &id) override;
    QSharedPointer<SyncableObject> objectPtr(const QUuid &id) override;
};

#endif // USERLISTMODEL_H
