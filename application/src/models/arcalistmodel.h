/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef ARCALISTMODEL_H
#define ARCALISTMODEL_H

#include "singleton.h"

#include "ArcaObject.h"

#include "basemodel.h"

#include "queuedresult.h"

class ArcaListModel : public BaseModel
{
    Q_OBJECT

public:

    static ArcaListModel *get();
    ~ArcaListModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    ArcaObjectPtr arca(const QUuid &id) const;

    Q_INVOKABLE void pinList(const QModelIndexList& indexes, const bool pin = false) override;

    ArcaObjectList objectList(const QModelIndexList& indexes);
    ArcaObjectList objectList(const QList<QUuid>& ids);

protected:
    ArcaListModel();

    QVariantList source() const override;

    friend class TSingleton<ArcaListModel>;

    // BaseModel interface
protected:
    SyncableObject object(const QUuid &id) override;
    QSharedPointer<SyncableObject> objectPtr(const QUuid &id) override;

};

#endif // ARCALISTMODEL_H
