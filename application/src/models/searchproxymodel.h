#ifndef SEARCHPROXYMODEL_H
#define SEARCHPROXYMODEL_H

#include <QSortFilterProxyModel>
#include "baselistmodel.h"

class SearchProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QString pattern READ pattern WRITE setPattern NOTIFY patternChanged)
public:
    explicit SearchProxyModel(QObject *parent = nullptr);
    QVariant data(const QModelIndex &index, int role) const override;
    QString pattern() const;

public slots:
    void setPattern(QString pattern);

signals:
    void patternChanged(QString pattern);

private:
    QRegularExpression m_regExp;
    QString m_pattern;
};



#endif // SEARCHPROXYMODEL_H
