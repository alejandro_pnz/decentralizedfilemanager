/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "quodlistmodel.h"

#include <QQmlEngine>

#include <QDebug>

#include "AuthenticationManager.h"
#include "DatabaseManager.h"
#include "utility.h"
#include "QSDiffRunner"

QuodListModel *QuodListModel::get()
{
    return TSingleton<QuodListModel>::instance();
}

QuodListModel::~QuodListModel()
{

}

QVariant QuodListModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    QVariant d = QSListModel::data(index, role);

    if (d.isValid()) {
        return d;
    }

    auto roleName = roleNames().value(role);

    if (roleName == "object") {
        QuodObject quod = DatabaseManager::get()->quod(QSListModel::data(index, roleNames().key("id")).toUuid());

        if (quod.isValid()) {
            QuodObject *qmlQuod = new QuodObject(quod);
            qmlQuod->detach();
            QQmlEngine::setObjectOwnership(qmlQuod, QQmlEngine::JavaScriptOwnership);
            return QVariant::fromValue(qmlQuod);
        }
    } else if (roleName == "content") {
        const auto id = QSListModel::data(index, roleNames().key("id")).toUuid();
        if (m_queuedResult.exists(id, roleName)) {
            if (!m_queuedResult.pending(id, roleName)) {
                return m_queuedResult.result(id, roleName);
            }
        }
        else {
            m_queuedResult.enqueue(id, roleName);
        }
    }

    return {};
}

QuodObjectPtr QuodListModel::quod(const QUuid &id)
{
    QuodObject quod = DatabaseManager::get()->quod(id);

    if (quod.isValid()) {
        return QuodObjectPtr(new QuodObject(quod));
    }

    return nullptr;
}

void QuodListModel::pinList(const QModelIndexList &indexes, const bool pin)
{
    const int idRole = roleNames().key("id");
    QuodObjectList list;
    for (const auto& index : indexes) {
        QuodObject object = DatabaseManager::get()->quod(QSListModel::data(index, idRole).toUuid());
        if (object.isValid()) {
            object.setPinned(pin);
            list << object;
        }
    }
    DatabaseManager::get()->update(list);
}

void QuodListModel::lockList(const QModelIndexList &indexes, const bool lock)
{
    const int idRole = roleNames().key("id");
    QuodObjectList list;
    for (const auto& index : indexes) {
        QuodObject object = DatabaseManager::get()->quod(QSListModel::data(index, idRole).toUuid());
        if (object.isValid()) {
            object.setLocked(lock);
            list << object;
        }
    }
    DatabaseManager::get()->update(list);
}

QuodObjectList QuodListModel::objectList(const QModelIndexList &indexes)
{
    const int idRole = roleNames().key("id");
    QuodObjectList list;
    for (const auto& index : indexes) {
       list << DatabaseManager::get()->quod(QSListModel::data(index, idRole).toUuid());
    }
    return list;
}

QuodObjectList QuodListModel::objectList(const QList<QUuid> &ids)
{
    QuodObjectList list;
    for (const auto& id : ids) {
       list << DatabaseManager::get()->quod(id);
    }
    return list;
}

QuodListModel::QuodListModel()
{
    m_queuedResult.setDelegate([](const QUuid &id, const QString &role, const QVariant &) -> QVariant {
        if (role == "content") {
            return QVariant(QuodArca::Utility::get()->formatQuodContent(id));
        }

        return {};
    });

    setKeyField("id");
    setRoleNames({"id", "name", "pinned", "locked", "object", "syncProperty", "created", "modified", "content"});
    m_cachedRoleNames = {roleNames().key("content"), roleNames().key("object")};

    connect(AuthenticationManager::get(), &AuthenticationManager::loginStateChanged, this, &QuodListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::quodAdded, this, &QuodListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::quodUpdated, this, &QuodListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::quodRemoved, this, &QuodListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::quodUpdated, this, QOverload<QList<QUuid>>::of(&QuodListModel::invalidateCache));

    connect(&m_queuedResult, &QueuedResult::resultReady, this, [=](const QUuid &id, const QString &roleName) {
        const auto idx = indexOf("id", id);
        Q_EMIT dataChanged(index(idx, 0), index(idx, 0), { roleNames().key(roleName.toUtf8()) });
    }, Qt::QueuedConnection);
}

QVariantList QuodListModel::source() const
{
    QuodObjectList source = DatabaseManager::get()->allQuods();

    return QVariant::fromValue(source).toList();
}

SyncableObject QuodListModel::object(const QUuid &id)
{
    return DatabaseManager::get()->quod(id);
}

QSharedPointer<SyncableObject> QuodListModel::objectPtr(const QUuid &id)
{
    return quod(id);
}
