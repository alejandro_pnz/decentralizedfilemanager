#ifndef QUODTYPELISTMODEL_H
#define QUODTYPELISTMODEL_H

#include "QuodTypeObject.h"

#include "baselistmodel.h"

class QuodTypeListModel : public BaseListModel
{
    Q_OBJECT

public:
    explicit QuodTypeListModel(QObject* parent = nullptr);
    ~QuodTypeListModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QuodTypeObjectPtr quodType(const QUuid &id) const;

    QStringList idsStringList(const QModelIndexList& indexes) const;

};

#endif // QUODTYPELISTMODEL_H
