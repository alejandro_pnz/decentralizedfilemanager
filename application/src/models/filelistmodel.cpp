/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "filelistmodel.h"

#include <QQmlEngine>

#include <AuthenticationManager.h>
#include <DatabaseManager.h>
#include <ImportManager.h>
#include <SyncFileJob.h>
#include <SyncManager.h>

#include "utility.h"

FileListModel *FileListModel::get()
{
    return TSingleton<FileListModel>::instance();
}

FileListModel::~FileListModel()
{
}

QVariant FileListModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    QVariant d = QSListModel::data(index, role);

    if (d.isValid()) {
        return d;
    }

    auto roleName = roleNames().value(role);

    if (roleName == "object") {
        const FileObject file = DatabaseManager::get()->file(QSListModel::data(index, roleNames().key("id")).toUuid());

        if (file.isValid()) {
            FileObject *qmlFile = new FileObject(file);
            qmlFile->detach();
            QQmlEngine::setObjectOwnership(qmlFile, QQmlEngine::JavaScriptOwnership);
            return QVariant::fromValue(qmlFile);
        }
    }
    else if (roleName == "content") {
        const auto id = QSListModel::data(index, roleNames().key("id")).toUuid();
        if (m_queuedResult.exists(id, roleName)) {
            if (!m_queuedResult.pending(id, roleName)) {
                return m_queuedResult.result(id, roleName);
            }
        }
        else {
            m_queuedResult.enqueue(id, roleName);
        }
    }
    else if (roleName == "progress") {
        auto job = m_jobs.value(QSListModel::data(index, roleNames().key("id")).toUuid(), nullptr);
        return QVariant::fromValue(job);
    }
    else if (roleName == "available") {
        const FileObject file = DatabaseManager::get()->file(QSListModel::data(index, roleNames().key("id")).toUuid());
        return file.available();
    }

    return {};
}

FileObjectPtr FileListModel::file(const QUuid &id) const
{
    FileObject file = DatabaseManager::get()->file(id);

    if (file.isValid()) {
        return FileObjectPtr(new FileObject(file));
    }

    return nullptr;
}

void FileListModel::pinList(const QModelIndexList &indexes, const bool pin)
{
    const int idRole = roleNames().key("id");
    FileObjectList list;
    for (const auto& index : indexes) {
        const auto object = DatabaseManager::get()->file(QSListModel::data(index, idRole).toUuid());
        if (object.isValid()) {
            list.append(object);
        }
    }
    DatabaseManager::get()->pin(list, pin);
}

void FileListModel::lockList(const QModelIndexList &indexes, const bool lock)
{
    const int idRole = roleNames().key("id");
    FileObjectList list;
    for (const auto& index : indexes) {
        FileObject object = DatabaseManager::get()->file(QSListModel::data(index, idRole).toUuid());
        if (object.isValid()) {
            object.setLocked(lock);
            list << object;
        }
    }
    DatabaseManager::get()->update(list);
}

FileObjectList FileListModel::objectList(const QModelIndexList &indexes)
{
    const int idRole = roleNames().key("id");
    FileObjectList list;
    for (const auto& index : indexes) {
       list << DatabaseManager::get()->file(QSListModel::data(index, idRole).toUuid());
    }
    return list;
}

FileObjectList FileListModel::objectList(const QList<QUuid> &ids)
{
    FileObjectList list;
    for (const auto& id : ids) {
       list << DatabaseManager::get()->file(id);
    }
    return list;
}

bool FileListModel::importFiles(const QList<QUrl> &files)
{
    return ImportManager::get()->importFiles(files);
}

QString FileListModel::importFile(const QString &filePath)
{
    FileObject file;
    if (DatabaseManager::get()->add(file)) {
        qDebug() << "Importing file:";
        Error error;
        DatabaseManager::get()->addFileData(file, QUrl(filePath), error);

        if(file.isValid()){
            return file.id().toString();
        }
    }
    return QString();
}

FileListModel::FileListModel()
{
    m_queuedResult.setGroupResults(true);

    m_queuedResult.setDelegate([](const QUuid &id, const QString &role, const QVariant &) -> QVariant {
        if (role == "content") {
            return QVariant(QuodArca::Utility::get()->formatFileContent(id));
        }

        return {};
    });

    setRoleNames({"id", "name", "pinned", "locked", "object", "fileType", "size", "syncProperty", "created", "modified", "content", "progress", "available"});
    m_cachedRoleNames = {roleNames().key("content"), roleNames().key("object"), roleNames().key("available")};

    connect(AuthenticationManager::get(), &AuthenticationManager::loginStateChanged, this, &FileListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::filesAdded, this, &FileListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::filesUpdated, this, &FileListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::filesRemoved, this, &FileListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::filesUpdated, this, QOverload<QList<QUuid>>::of(&FileListModel::invalidateCache));

    connect(SyncManager::get(), &SyncManager::fileReceived, this, &FileListModel::addJob, Qt::QueuedConnection);

    connect(ImportManager::get(), &ImportManager::jobsAdded, this, [this](const QList<ImportFileJob *> &jobs) {
        for (auto *job: jobs) {
            addJob(job);
        }
    }, Qt::QueuedConnection);

    connect(&m_queuedResult, &QueuedResult::resultsReady, this, [=](const QMultiHash<QString, QUuid> &results) {
        const auto roles = results.uniqueKeys();
        for (const auto &role: roles) {
            QVector<int> indexes;
            const auto ids = results.values(role);
            std::transform(ids.begin(), ids.end(), std::back_inserter(indexes), [this](const auto &id) { return indexOf("id", id); });
            std::sort(indexes.begin(), indexes.end());
            qDebug() << "results ready" << indexes.size();
            Q_EMIT dataChanged(index(indexes.first(), 0), index(indexes.last(), 0), { roleNames().key(role.toUtf8()) });
        }
    }, Qt::QueuedConnection);
}

QVariantList FileListModel::source() const
{
    auto source = DatabaseManager::get()->allFiles();

    return QVariant::fromValue(source).toList();
}

QVariant FileListModel::insertToCache(QUuid id)
{
    return {};
}

void FileListModel::addJob(AbstractJob *job)
{
    if (!job) {
        return;
    }

    auto addNewJob = [=]() {
        qDebug() << job->id();
        m_jobs[job->id()] = job;
        const auto idx = indexOf("id", job->id());
        Q_EMIT dataChanged(index(idx, 0), index(idx, 0), { roleNames().key("progress") });
    };

    if (job->id().isNull()) {
        connect(job, &AbstractJob::idChanged, this, addNewJob, Qt::QueuedConnection);
    }
    else {
        addNewJob();
    }

    connect(job, &AbstractJob::finished, this, [=]() {
        m_jobs.remove(job->id());
        const auto idx = indexOf("id", job->id());
        Q_EMIT dataChanged(index(idx, 0), index(idx, 0), { roleNames().key("progress"), roleNames().key("available") });
    }, Qt::QueuedConnection);
}

SyncableObject FileListModel::object(const QUuid &id)
{
    return DatabaseManager::get()->file(id);
}

QSharedPointer<SyncableObject> FileListModel::objectPtr(const QUuid &id)
{
    return file(id);
}
