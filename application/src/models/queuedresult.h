/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef QUEUEDRESULT_H
#define QUEUEDRESULT_H

#include <QHash>
#include <QObject>
#include <QThreadPool>
#include <QTimer>
#include <QUuid>
#include <QVariant>

class Merger : public QObject
{
    Q_OBJECT
public:
    Merger()
    {
        timer.setInterval(500);
        timer.setSingleShot(true);
        connect(&timer, &QTimer::timeout, this, [=]() {
            Q_EMIT this->ready(results);
            results.clear();
        });
    }
    ~Merger() = default;

    void add(const QUuid &id, const QString &role)
    {
        results.insert(role, id);
        timer.start();
    }

    Q_SIGNAL void ready(const QMultiHash<QString, QUuid> &results);

private:
    QMultiHash<QString, QUuid> results;
    QTimer timer;
};

class QueuedResult : public QObject
{
    Q_OBJECT

public:
    enum ResultState {
        Pending = 1,
        Ready
    };

    QueuedResult(QObject *parent = nullptr);
    ~QueuedResult();

    void setDelegate(const std::function<QVariant (const QUuid &, const QString &, const QVariant &)> &delegate);

    void enqueue(const QUuid &id, const QString &role, const QVariant &data = {}) const;

    QVariant dequeue(const QUuid &id, const QString &role) const;

    bool exists(const QUuid &id, const QString &role) const;

    bool pending(const QUuid &id, const QString &role) const;

    QVariant result(const QUuid &id, const QString &role) const;

    void setGroupResults(bool groupResults);

Q_SIGNALS:
    void resultReady(const QUuid &id, const QString &role);
    void resultsReady(const QMultiHash<QString, QUuid> &results);

private:
    struct Container {
        ResultState state{Pending};
        QVariant data;
        QVariant result;
    };
    mutable QHash<std::pair<QUuid, QString>, Container> m_results;

    std::function<QVariant (const QUuid &, const QString &, const QVariant &)> m_delegate;

    QThreadPool m_threadPool;

    Merger m_merger;
};

using t = QMultiHash<QString, QUuid>;
Q_DECLARE_METATYPE(t);

#endif // QUEUEDRESULT_H
