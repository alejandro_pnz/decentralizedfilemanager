#include "whiteblacklistuserproxymodel.h"
#include <QtQml>

WhiteBlackListUserProxyModel::WhiteBlackListUserProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{

}

int WhiteBlackListUserProxyModel::checkStateRoleValue() const
{
    return int(Qt::ItemDataRole::CheckStateRole);
}

int WhiteBlackListUserProxyModel::whiteListMode() const
{
    return m_whiteListMode;
}

void WhiteBlackListUserProxyModel::setWhiteListMode(int whiteListMode)
{
    if (m_whiteListMode == whiteListMode)
        return;

    m_whiteListMode = whiteListMode;
    emit whiteListModeChanged(m_whiteListMode);
    emit selectionCountChanged();
    beginResetModel();
    endResetModel();
}

QVariant WhiteBlackListUserProxyModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::ItemDataRole::CheckStateRole) {
        return getCheckState(whiteListMode() ? m_whiteList : m_blackList, index);
    } else if (role == Qt::UserRole + 10) {
        return getCheckState(m_blackList, index);
    } else if (role == Qt::UserRole + 11) {
        return getCheckState(m_whiteList, index);
    } else {
        return sourceModel()->data(mapToSource(index), role);
    }
}

bool WhiteBlackListUserProxyModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::ItemDataRole::CheckStateRole) {
        setCheckState(whiteListMode() ? m_whiteList : m_blackList, index);
        emit selectionCountChanged();
        emit dataChanged(index, index, {Qt::ItemDataRole::CheckStateRole});
        return true;
    } else {
        return sourceModel()->setData(mapToSource(index), value, role);
    }
}

QHash<int, QByteArray> WhiteBlackListUserProxyModel::roleNames() const
{
    return m_roleNames;
}

void WhiteBlackListUserProxyModel::setSourceModel(QAbstractItemModel *sourceModel)
{
    QSortFilterProxyModel::setSourceModel(sourceModel);
    m_roleNames = sourceModel->roleNames();
    m_roleNames.insert(Qt::ItemDataRole::CheckStateRole, "isChecked");
    m_roleNames.insert(Qt::UserRole + 10, "blacklisted");
    m_roleNames.insert(Qt::UserRole + 11, "whitelisted");
}

int WhiteBlackListUserProxyModel::selectionCount() const
{
    return whiteListMode() ? m_whiteList.size() : m_blackList.size();
}

void WhiteBlackListUserProxyModel::setCheckState(QModelIndexList &list, const QModelIndex& index)
{
    auto sourceIndex = mapToSource(index);
    auto idx = list.indexOf(sourceIndex);
    if (idx > -1){
        list.removeAt(idx);
    } else {
        list.append(sourceIndex);
    }

}

bool WhiteBlackListUserProxyModel::getCheckState(const QModelIndexList &list, const QModelIndex &index) const
{
    return list.contains(mapToSource(index));
}

void registerType() {
    qmlRegisterType<WhiteBlackListUserProxyModel>("com.test", 1, 0, "WhiteListProxy");
}

Q_COREAPP_STARTUP_FUNCTION(registerType)
