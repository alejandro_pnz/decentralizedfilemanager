/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "queuedresult.h"

#include "utility.h"

#include <QtConcurrent>

#include <QtPromise>

QueuedResult::QueuedResult(QObject *parent)
    : QObject(parent)
{
    m_threadPool.setMaxThreadCount(5);
}

QueuedResult::~QueuedResult()
{

}

void QueuedResult::setDelegate(const std::function<QVariant (const QUuid &, const QString &, const QVariant &)> &delegate)
{
    m_delegate = delegate;
}

void QueuedResult::enqueue(const QUuid &id, const QString &role, const QVariant &data) const
{
    if (!m_delegate) {
        return;
    }

    QueuedResult *self = const_cast<QueuedResult *>(this);

    m_results.insert(std::make_pair(id, role), {});
    auto promise = QtPromise::resolve(QtConcurrent::run(&self->m_threadPool, [=](const QUuid &id, const QString &role, const QVariant &data) {
        return m_delegate(id, role, data);
    }, id, role, data));

    promise.then([=](const QVariant &res) {
        const auto pair = std::make_pair(id, role);
        if (m_results.contains(pair)) {
            m_results[pair].result = res;
            m_results[pair].state = Ready;
            Q_EMIT self->resultReady(id, role);
        }
    });
}

QVariant QueuedResult::dequeue(const QUuid &id, const QString &role) const
{
    return m_results.take(std::make_pair(id, role)).result;
}

bool QueuedResult::exists(const QUuid &id, const QString &role) const
{
    return m_results.contains(std::make_pair(id, role));
}

bool QueuedResult::pending(const QUuid &id, const QString &role) const
{
    return m_results.value(std::make_pair(id, role)).state == Pending;
}

QVariant QueuedResult::result(const QUuid &id, const QString &role) const
{
    return m_results.value(std::make_pair(id, role)).result;
}

void QueuedResult::setGroupResults(bool groupResults)
{
    if (groupResults) {
        connect(this, &QueuedResult::resultReady, &m_merger, &Merger::add, Qt::UniqueConnection);
        connect(&m_merger, &Merger::ready, this, &QueuedResult::resultsReady, Qt::UniqueConnection);
    }
    else {
        disconnect(&m_merger, &Merger::ready, this, &QueuedResult::resultsReady);
        disconnect(this, &QueuedResult::resultReady, &m_merger, &Merger::add);
    }
}
