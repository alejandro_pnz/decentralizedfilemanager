#ifndef WHITEBLACKLISTUSERPROXYMODEL_H
#define WHITEBLACKLISTUSERPROXYMODEL_H

#include <QSortFilterProxyModel>

class WhiteBlackListUserProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(int selectionCount READ selectionCount NOTIFY selectionCountChanged)
    Q_PROPERTY(int whiteListMode READ whiteListMode WRITE setWhiteListMode NOTIFY whiteListModeChanged)

public:
    explicit WhiteBlackListUserProxyModel(QObject *parent = nullptr);
    Q_INVOKABLE int checkStateRoleValue() const;
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    void setSourceModel(QAbstractItemModel *sourceModel) override;
    QHash<int, QByteArray> roleNames() const override;

    int selectionCount() const;
    int whiteListMode() const;

public slots:
    void setWhiteListMode(int whiteListMode);

signals:
    void selectionCountChanged();
    void whiteListModeChanged(int whiteListMode);

private:
    QModelIndexList m_whiteList;
    QModelIndexList m_blackList;
    QHash<int, QByteArray> m_roleNames;
    int m_selectionCount;
    int m_whiteListMode;
    void setCheckState(QModelIndexList &list, const QModelIndex& index);
    bool getCheckState(const QModelIndexList& list, const QModelIndex& index) const;
};





#endif // WHITEBLACKLISTUSERPROXYMODEL_H
