/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "searchmodel.h"

#include <QCoreApplication>

#include "DatabaseManager.h"

#include "utility.h"
#include "searchfilters.h"

SearchModel::SearchModel() : m_filters(new SearchFilters(this))
{
    setKeyField("id");
    setRoleNames({"id", "name", "locked", "objectType", "pinned", "syncProperty", "color", "fileType", "object", "content", "score"});

    connect(&m_manager, &SearchManager::resultsChanged, this, &SearchModel::sync);
}

SearchModel::~SearchModel()
{

}

QVariant SearchModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    QVariant d = QSListModel::data(index, role);

    if (d.isValid()) {
        if (roleNames().value(role) == "objectType") {
            return d.value<SyncableObject::ObjectType>();
        }
        return d;
    }

    auto roleName = roleNames().value(role);

    if (roleName == "object") {
        const int objectTypeRole = roleNames().key("objectType");
        auto objectType = QSListModel::data(index, objectTypeRole).value<SyncableObject::ObjectType>();

        switch(objectType) {
        case SyncableObject::Arca: {
            ArcaObject arca = DatabaseManager::get()->arca(QSListModel::data(index, roleNames().key("id")).toUuid());

            if (arca.isValid()) {
                ArcaObject *qmlArca = new ArcaObject(arca);
                qmlArca->detach();
                QQmlEngine::setObjectOwnership(qmlArca, QQmlEngine::JavaScriptOwnership);
                return QVariant::fromValue(qmlArca);
            }
            break;
        }
        case SyncableObject::Quod: {
            QuodObject quod = DatabaseManager::get()->quod(QSListModel::data(index, roleNames().key("id")).toUuid());

            if (quod.isValid()) {
                QuodObject *qmlQuod = new QuodObject(quod);
                qmlQuod->detach();
                QQmlEngine::setObjectOwnership(qmlQuod, QQmlEngine::JavaScriptOwnership);
                return QVariant::fromValue(qmlQuod);
            }
            break;
        }
        case SyncableObject::File: {
            FileObject file = DatabaseManager::get()->file(QSListModel::data(index, roleNames().key("id")).toUuid());

            if (file.isValid()) {
                FileObject *qmlFile = new FileObject(file);
                qmlFile->detach();
                QQmlEngine::setObjectOwnership(qmlFile, QQmlEngine::JavaScriptOwnership);
                return QVariant::fromValue(qmlFile);
            }
            break;
        }
        default: break;
        }
    } else if (roleName == "content") {
        const auto id = QSListModel::data(index, roleNames().key("id")).toUuid();
        const int objectTypeRole = roleNames().key("objectType");
        SyncableObject::ObjectType objectType = QSListModel::data(index, objectTypeRole).value<SyncableObject::ObjectType>();
        switch(objectType) {
        case SyncableObject::Arca: {
            ArcaObject arca = DatabaseManager::get()->arca(QSListModel::data(index, roleNames().key("id")).toUuid());
            return QuodArca::Utility::get()->formatArcaContent(arca.quodCount(), arca.filesCount());
        }
        case SyncableObject::File:
            return QuodArca::Utility::get()->formatFileContent(id);
        case SyncableObject::Quod:
            return QuodArca::Utility::get()->formatQuodContent(id);
        default:
            break;

        }
    } else if (roleName == "syncProperty") {
        return -1;
    }

    return {};
}

void SearchModel::search(const QString &query)
{    
    m_searchedText = query;
    m_manager.search(get_filters()->formatQuery(m_searchedText));
}

void SearchModel::updateFilters()
{
    qDebug() << Q_FUNC_INFO;
    search(m_searchedText);
}

void SearchModel::removeFilter(SFilter::Type filter, const QVariant& id)
{
    qDebug() << Q_FUNC_INFO << filter;
    m_filters->removeFilter(filter, id);
    search(m_searchedText);
}

SyncableObject SearchModel::object(const QUuid &id)
{
    return {};
}

QSharedPointer<SyncableObject> SearchModel::objectPtr(const QUuid &id)
{
    return {};
}

QVariantList SearchModel::source() const
{
    return m_manager.results();
}

void _q_SearchModel_registerTypes() {
    qmlRegisterType<SearchModel>("com.test", 1, 0, "SearchModel");
    qmlRegisterType<SearchFilters>("com.test", 1, 0, "SearchFilters");
    qmlRegisterUncreatableType<SFilter>("com.test", 1, 0, "SFilter", "Only enum exposed");
    qRegisterMetaType<SearchFilters*>();
    qRegisterMetaType<SFilter*>();
    qRegisterMetaType<SFilter::Type>();
}

Q_COREAPP_STARTUP_FUNCTION(_q_SearchModel_registerTypes)
