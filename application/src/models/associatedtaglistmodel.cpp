/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "associatedtaglistmodel.h"

AssociatedTagListModel::AssociatedTagListModel(QObject *parent)
    : QSListModel(parent)
{
    setRoleNames({"id", "name", "color"});
}

QVariantList AssociatedTagListModel::idList() const
{
    return QVariant::fromValue(m_tags.keys()).value<QVariantList>();
}

TagObjectList AssociatedTagListModel::tags() const
{
    return m_tags.values();
}

void AssociatedTagListModel::setTags(const TagObjectList &tags)
{
    m_tags.clear();

    for (const auto &tag: tags) {
        m_tags.insert(tag.id(), tag);
    }

    emit idListChanged();

    sync();
}

void AssociatedTagListModel::addTag(TagObject *obj)
{
    if (!obj || !obj->isValid()) {
        qWarning() << "Invalid tag object";
        return;
    }

    m_tags.insert(obj->id(), TagObject(obj));

    emit idListChanged();

    sync();
}

void AssociatedTagListModel::deleteTag(const QUuid &id)
{
    if (id.isNull()) {
        qWarning() << "Invalid tag id";
        return;
    }

    if (m_tags.remove(id) > 0) {
        emit idListChanged();

        sync();
    }
}

void AssociatedTagListModel::deleteAllTags()
{
    m_tags.clear();
    emit idListChanged();
    sync();
}

void AssociatedTagListModel::sync()
{
    qDebug() << "AssociatedTagListModel::sync()";

    QSDiffRunner runner;
    runner.setKeyField("id");

    QList<QSPatch> patches = runner.compare(storage(), QVariant::fromValue(m_tags.values()).value<QVariantList>());

    if (!patches.isEmpty()) {
        runner.patch(this, patches);
    }
}
