#ifndef BASEMODEL_H
#define BASEMODEL_H

#include "baselistmodel.h"
#include <SyncableObject.h>

#include "queuedresult.h"

class BaseModel : public BaseListModel
{
    Q_OBJECT

public:
    explicit BaseModel(QObject *parent = nullptr);

    Q_INVOKABLE virtual void pinList(const QModelIndexList& indexes, const bool pin = false);
    Q_INVOKABLE void pinOne(const QUuid &id, SyncableObject::ObjectType);
    Q_INVOKABLE virtual void lockList(const QModelIndexList& indexes, const bool pin = false);
    Q_INVOKABLE void lockOne(const QUuid &id);
    Q_INVOKABLE virtual bool anyPinned(const QModelIndexList& indexes) const;
    Q_INVOKABLE virtual bool allPinned(const QModelIndexList& indexes) const;
    Q_INVOKABLE virtual bool anyLocked(const QModelIndexList& indexes) const;
    Q_INVOKABLE virtual bool allLocked(const QModelIndexList& indexes) const;
    Q_INVOKABLE QList<QUuid> idsFromModelIndexes(const QModelIndexList& indexes) const;

protected Q_SLOTS:
    void invalidateCache(QList<QUuid> list);

protected:
    virtual SyncableObject object(const QUuid& id) = 0;
    virtual QSharedPointer<SyncableObject> objectPtr(const QUuid &id) = 0;
    void pinOne(SyncableObject object);
    void lockOne(QSharedPointer<SyncableObject> object);
    void removeOne(SyncableObject object);
    bool anyTrue(const QModelIndexList& indexes, const QByteArray &roleName) const;
    bool allTrue(const QModelIndexList& indexes, const QByteArray &roleName) const;

    virtual void invalidateCache(const QUuid &id);
    virtual QVariant insertToCache(QUuid id);
    virtual QVariant cachedValue(QUuid id);
    QHash<QUuid, QString> m_cache;
    QVector<int> m_cachedRoleNames;

    QueuedResult m_queuedResult;
};

#endif // BASEMODEL_H
