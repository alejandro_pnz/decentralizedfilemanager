/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "devicelistmodel.h"

#include "QSDiffRunner"

#include "AuthenticationManager.h"
#include "DatabaseManager.h"
#include "DeviceManager.h"
#include "SyncManager.h"
#include <QQmlEngine>

DeviceListModel *DeviceListModel::get()
{
    return TSingleton<DeviceListModel>::instance();
}

DeviceListModel::~DeviceListModel()
{

}

QVariant DeviceListModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    QVariant d = QSListModel::data(index, role);

    if (d.isValid()) {
        if (roleNames().value(role) == "deviceType") {
            return d.value<DeviceObject::DeviceType>();
        }
        return d;
    }

    if (roleNames().value(role) == "object") {
        DeviceObject device = DeviceManager::get()->device(QSListModel::data(index, roleNames().key("id")).toUuid());

        if (device.isValid()) {
            DeviceObject *qmlDevice = new DeviceObject(device);
            qmlDevice->detach();
            QQmlEngine::setObjectOwnership(qmlDevice, QQmlEngine::JavaScriptOwnership);
            return QVariant::fromValue(qmlDevice);
        }
    } else if (roleNames().value(role) == "outOfSync") {
        // TODO this should be just property of DeviceObject, but it's added as custom role now
        return false;
    }

    return {};
}

void DeviceListModel::activateList(const QModelIndexList &indexes, const bool activate)
{
    auto devices = devicesFromIndexes(indexes);
    if (devices.isEmpty()) return;
    if (activate) {
        DeviceManager::get()->activate(devices);
    } else {
        DeviceManager::get()->deactivate(devices);
    }
}

void DeviceListModel::activateOne(const QUuid &id)
{
    auto device = DeviceManager::get()->device(id);
    if (device.isValid()) {
        if (device.active()) {
            DeviceManager::get()->deactivate(device);
        } else {
            DeviceManager::get()->activate(device);
        }
    } else {
        qWarning() << Q_FUNC_INFO << "Invalid device";
    }
}

void DeviceListModel::blockList(const QModelIndexList &indexes, const bool block)
{
    auto devices = devicesFromIndexes(indexes);
    if (devices.isEmpty()) return;
    if (block) {
        DeviceManager::get()->block(devices);
    } else {
        DeviceManager::get()->unblock(devices);
    }
}

void DeviceListModel::blockOne(const QUuid &id)
{
    qDebug() << "ID" << id;
    auto device = DeviceManager::get()->device(id);
    if (device.isValid()) {
        if (device.blocked()) {
            DeviceManager::get()->unblock(device);
        } else {
            DeviceManager::get()->block(device);
        }
    } else {
        qWarning() << Q_FUNC_INFO << "Invalid device";
    }
}

void DeviceListModel::givePowerList(const QModelIndexList &indexes, const bool givePower)
{
//    auto devices = devicesFromIndexes(indexes);
//    if (devices.isEmpty()) return;
//    if (givePower) {
//        DeviceManager::get()->givePower(devices);
//    } else {
//        DeviceManager::get()->revokePower(devices);
//    }
}

void DeviceListModel::givePowerOne(const QUuid &id)
{
//    auto device = DeviceManager::get()->device(id);
//    if (device.isValid()) {
//        if (device.powerDevice()) {
//            DeviceManager::get()->revokePower(device);
//        } else {
//            DeviceManager::get()->givePower(device);
//        }
//    } else {
//        qWarning() << Q_FUNC_INFO << "Invalid device";
//    }
}

void DeviceListModel::logoutList(const QModelIndexList &indexes)
{
    auto devices = devicesFromIndexes(indexes);
    if (devices.isEmpty()) return;
    DeviceManager::get()->logout(devices);
}

void DeviceListModel::logoutOne(const QUuid &id)
{
    auto device = DeviceManager::get()->device(id);
    if (device.isValid()) {
        DeviceManager::get()->logout(device);
    } else {
        qWarning() << Q_FUNC_INFO << "Invalid device";
    }
}

void DeviceListModel::removeList(const QModelIndexList &indexes)
{
    const auto devices = devicesFromIndexes(indexes);
    if (devices.isEmpty()) return;
    for (const auto& device : devices) {
        DeviceManager::get()->remove(device);
    }
}

void DeviceListModel::removeOne(const QUuid &id)
{
    auto device = DeviceManager::get()->device(id);
    if (device.isValid()) {
        DeviceManager::get()->remove(device);
    } else {
        qWarning() << Q_FUNC_INFO << "Invalid device";
    }
}

bool DeviceListModel::canAddNewDevice() const
{
   return  true;//DeviceManager::allowedDevicesCount() > (DeviceManager::myDevicesCount() + DeviceManager::otherDevicesCount());
}

bool DeviceListModel::anyActive(const QModelIndexList &indexes) const
{
    return anyTrue(indexes, "active");
}

bool DeviceListModel::anyBlocked(const QModelIndexList &indexes) const
{
    return anyTrue(indexes, "blocked");
}

bool DeviceListModel::anyPowerDevice(const QModelIndexList &indexes) const
{
    return anyTrue(indexes, "powerDevice");
}

void DeviceListModel::pinList(const QModelIndexList &indexes, const bool pin)
{
    const int idRole = roleNames().key("id");
    DeviceObjectList list;
    for (const auto& index : indexes) {
        auto object = DeviceManager::get()->device(QSListModel::data(index, idRole).toUuid());
        if (object.isValid()) {
            list.append(object);
        }
    }
    DatabaseManager::get()->pin(list, pin);
}

DeviceObject *DeviceListModel::qmlObject(const QUuid &id) const
{
    DeviceObject device = DeviceManager::get()->device(id);

    if (device.isValid()) {
        DeviceObject *qmlDevice = new DeviceObject(device);
        qmlDevice->detach();
        QQmlEngine::setObjectOwnership(qmlDevice, QQmlEngine::JavaScriptOwnership);
        return qmlDevice;
    }

    return nullptr;
}

DeviceListModel::DeviceListModel()
    : BaseModel()
{
    setRoleNames({"id", "name", "email", "pinned", "deviceModel", "deviceOS", "deviceType",
                  "active", "created", "blocked", "pending", "online", "syncing",
                  "outOfSync", "masterDevice", "powerDevice", "object", "lastSeen", "deviceOSVesion", "deleted"});

    connect(AuthenticationManager::get(), &AuthenticationManager::loginStateChanged, this, &DeviceListModel::sync);

    connect(DeviceManager::get(), &DeviceManager::devicesAdded, this, &DeviceListModel::sync);
    connect(DeviceManager::get(), &DeviceManager::devicesUpdated, this, &DeviceListModel::sync);
    connect(DeviceManager::get(), &DeviceManager::devicesRemoved, this, &DeviceListModel::sync);
    connect(DeviceManager::get(), &DeviceManager::deviceOnlineStatusChanged, this, &DeviceListModel::sync);

    sync();
}

QVariantList DeviceListModel::source() const
{
    auto devices = DeviceManager::get()->allDevices();

    return QVariant::fromValue(devices).toList();
}

DeviceObjectList DeviceListModel::devicesFromIndexes(const QModelIndexList &indexes)
{
    const auto ids = idsFromModelIndexes(indexes);
    DeviceObjectList devices;
    for (const auto& id: ids) {
        auto device = DeviceManager::get()->device(id);
        if (device.isValid()) {
            devices.append(device);
        } else {
            qWarning() << Q_FUNC_INFO << "Invalid device";
        }
    }
    return devices;
}

SyncableObject DeviceListModel::object(const QUuid &id)
{
    return DeviceManager::get()->device(id);
}

QSharedPointer<SyncableObject> DeviceListModel::objectPtr(const QUuid &id)
{
    DeviceObject device = DeviceManager::get()->device(id);

    if (device.isValid()) {
        return DeviceObjectPtr(new DeviceObject(device));
    }

    return nullptr;
}

int DeviceListModel::getAllowedDevicesCount() const
{
    return 10;//DeviceManager::allowedDevicesCount();
}

int DeviceListModel::getMyDevicesCount() const
{
    return 6;//DeviceManager::myDevicesCount();
}

int DeviceListModel::getOtherDevicesCount() const
{
    return 3;//DeviceManager::otherDevicesCount();
}

void _q_DeviceListModel_registerTypes() {
    qmlRegisterSingletonType<DeviceListModel>("com.test", 1, 0, "DeviceListModel", [](QQmlEngine *, QJSEngine *) -> QObject * {
        return DeviceListModel::get();
    });
}

Q_COREAPP_STARTUP_FUNCTION(_q_DeviceListModel_registerTypes)
