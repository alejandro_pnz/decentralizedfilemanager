#include "defaultattributesmodel.h"
#include <QtQml>

//DefaultAttributesModel::DefaultAttributesModel(QObject *parent) : AttributeModel(parent)
//{
//    QStringList names {tr("Account Holder"), tr("Account Number"), tr("Bank Name"),
//                tr("BIC/SWIFT"), tr("Billing Address"), tr("Certificate"), tr("Channel"), tr("Citizenship"),
//                tr("Company"), tr("CVV"), tr("Duplex Mode"), tr("EIGRP"), tr("Expiration Date"), tr("First Name"),
//                tr("Flow Control"), tr("Frequency"), tr("Full Name"), tr("Gateway IP"), tr("Gender"), tr("Generation Date"),
//                tr("Generator"), tr("Height"), tr("Hostname"), tr("IBAN"), tr("IM"),
//                tr("Incoming Server"), tr("IP address"), tr("Issue Date"), tr("Issued by"),
//                tr("Length (bits)"), tr("License Key"), tr("Limit"), tr("Number"),
//                tr("Order Number"), tr("Outgoing Server"), tr("Phone"), tr("PIN"),
//                tr("Place of Birth"), tr("Place of Issue"), tr("Policy"), tr("Port"),
//                tr("Private Key"), tr("Product"), tr("Public Key"), tr("Purchase Date"),
//                tr("Purchase Price"), tr("RIP Version"), tr("Security"), tr("Speed Mode"),
//                tr("SSID"), tr("Support Phone"), tr("Surname"), tr("Website (URL)") };

//    beginResetModel();
//    for (int i = 0; i < names.size(); i++) {
//        auto attribute = QuodAttributeUPtr(new QuodAttribute(i, 0, names.at(i), "Type value...", QuodAttribute::InputType::Text));
//        attribute->setRemovable(true);
//        m_list.push_back(std::move(attribute));
//    }
//    endResetModel();

//    m_roleNames.insert(Roles::Name + 1, "recentlyUsed");
//}

//void registerDefaultAttributesTypes() {
//    qmlRegisterType<DefaultAttributesModel>("com.test", 1, 0, "DefaultAttributesModel");
//}

//Q_COREAPP_STARTUP_FUNCTION(registerDefaultAttributesTypes)

//QVariant DefaultAttributesModel::data(const QModelIndex &index, int role) const
//{
//    if (role == Roles::Name + 1) {
//        // TODO just to test, normally it should be a method to check if attribute is recently used
//        return index.row() % 5 == 0;
//    } else {
//        return AttributeModel::data(index, role);
//    }
//}
