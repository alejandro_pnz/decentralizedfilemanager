#include "searchproxymodel.h"
#include <QtQml>

SearchProxyModel::SearchProxyModel(QObject *parent) : QSortFilterProxyModel(parent)
{
    m_regExp.setPatternOptions(QRegularExpression::PatternOption::CaseInsensitiveOption);
}

QString SearchProxyModel::pattern() const
{
    return m_pattern;
}

void SearchProxyModel::setPattern(QString pattern)
{
    if (m_pattern == pattern)
        return;

    m_pattern = pattern;
    m_regExp.setPattern(pattern);
    emit patternChanged(m_pattern);
    beginResetModel();
    endResetModel();
}

QVariant SearchProxyModel::data(const QModelIndex &index, int role) const
{
    return sourceModel()->data(mapToSource(index), role);
}

void registerSearchProxyModelTypes() {
    qmlRegisterType<SearchProxyModel>("com.test", 1, 0, "SearchProxyModel");
}

Q_COREAPP_STARTUP_FUNCTION(registerSearchProxyModelTypes)

