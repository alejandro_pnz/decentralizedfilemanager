#ifndef SEARCHFILTERS_H
#define SEARCHFILTERS_H

#include <QObject>
#include <qqmlhelpers.h>
#include <QItemSelection>
#include "search_filters/sfilter.h"
#include "baselistmodel.h"
class QItemSelectionModel;
class AppliedSearchFiltersListModel : public BaseListModel {
    Q_OBJECT
public:
    AppliedSearchFiltersListModel(QObject* parent = nullptr);
};

class SearchFilters : public QObject
{
    Q_OBJECT    
    QML_WRITABLE_PROPERTY(bool, librarySearch)
    QML_WRITABLE_PROPERTY(AppliedSearchFiltersListModel*, appliedFiltersModel);
public:
    explicit SearchFilters(QObject *parent = nullptr);

    Q_INVOKABLE void removeFilter(SFilter::Type filter, const QVariant& id = {});
    Q_INVOKABLE SFilter* filter(SFilter::Type filter);
    Q_INVOKABLE QVariant filtersDetails();
    Q_INVOKABLE QString moreFiltersDetails();
    Q_INVOKABLE void removeAll();

    void updateFilters();
    QString formatQuery(const QString& query);
    static QVariantList filtersDescToVariantList(const QVector<SFilter::FilterDesc>& desc);
signals:
    void filtersUpdated();
private:
    void createFilters();
    QVector<SFilter::FilterDesc> filtersDesc();
    QMap<SFilter::Type, SFilter*> m_filters;
    QVector<SFilter::Type> m_moreFilters;
};


#endif // SEARCHFILTERS_H
