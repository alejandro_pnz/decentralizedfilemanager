#include "searchfilters.h"
#include "taglistmodel.h"
#include "search_filters/filters.h"
#include <QItemSelectionModel>
#include <QDebug>

SearchFilters::SearchFilters(QObject *parent) : QObject(parent),
    m_librarySearch(false),
    m_appliedFiltersModel(new AppliedSearchFiltersListModel(this))
{
    m_moreFilters.append(SFilter::QuickFilters);
    m_moreFilters.append(SFilter::FileTypeFilter);
    m_moreFilters.append(SFilter::QuodTypeFilter);
    createFilters();
}

void SearchFilters::removeFilter(SFilter::Type filter, const QVariant& id)
{
    qDebug() << Q_FUNC_INFO << filter;
    if (filter == SFilter::MoreFilter) {
        //Special case, this is grouped filter
        for (const auto type: m_moreFilters) {
            m_filters.value(type)->remove(id);
        }
    } else {
        m_filters.value(filter)->remove(id);
    }
}

SFilter *SearchFilters::filter(SFilter::Type filter)
{
    return m_filters.value(filter, nullptr);
}

QVariant SearchFilters::filtersDetails()
{
    QVariantMap result;
    auto end = m_filters.cend();
    for (auto it = m_filters.cbegin(); it != end; ++it){
        result.insert(QString::number(it.key()), it.value()->get_details());
    }
    return QVariant::fromValue(result);
}

QString SearchFilters::moreFiltersDetails()
{
    int applied{0};
    for (const auto& filter: qAsConst(m_moreFilters)) {
        auto details = m_filters.value(filter)->get_details();
        applied += details.toInt();
    }

    return applied > 0 ? QString::number(applied) : QString();
}

void SearchFilters::removeAll()
{
    for (const auto& filter: m_filters) {
        filter->remove({});
    }
}

QString SearchFilters::formatQuery(const QString &query)
{
    QString q = query;

    if (m_librarySearch) {
        q.append(" type:arca quod file");
    }

    qDebug() << Q_FUNC_INFO << "before filters" << q << "original" << query;
    for (const auto filter: qAsConst(m_filters)){
        filter->apply(q);
    }

    get_appliedFiltersModel()->setSource(filtersDescToVariantList(filtersDesc()));

    qDebug() << Q_FUNC_INFO << q << "Aplied filters model count" << get_appliedFiltersModel()->count();
    emit filtersUpdated();
    return q;
}

QVariantList SearchFilters::filtersDescToVariantList(const QVector<SFilter::FilterDesc> &desc)
{
    QVariantList list;
    auto i = 0;
    for (auto& filter : desc) {
        list.append(QVariant(QVariantMap({{"id", i},{"name",filter.name}, {"type",filter.type}, {"filterId", filter.id}})));
        i++;
    }
    qDebug() << Q_FUNC_INFO << list;
    return list;
}

void SearchFilters::createFilters()
{
    m_filters.insert(SFilter::TagFilter, new TagFilter(this));
    m_filters.insert(SFilter::ModifiedFilter, new DateFilter(SFilter::ModifiedFilter, "modified", this));
    m_filters.insert(SFilter::OpenedFilter, new DateFilter(SFilter::OpenedFilter, "opened", this));
    m_filters.insert(SFilter::QuodTypeFilter, new QuodTypeFilter(this));
    m_filters.insert(SFilter::FileTypeFilter, new FileTypeFilter(this));
    m_filters.insert(SFilter::QuickFilters, new QuickFilters(this));
    m_filters.insert(SFilter::LockStatusFilter, new LockStatusFilter(this));
    m_filters.insert(SFilter::SyncStatusFilter, new SyncStatusFilter(this));
}

QVector<SFilter::FilterDesc> SearchFilters::filtersDesc()
{
    QVector<SFilter::FilterDesc> desc;
    auto end = m_filters.cend();
    for (auto it = m_filters.cbegin(); it != end; ++it){
        desc.append(it.value()->desc());
    }
    return desc;
}

AppliedSearchFiltersListModel::AppliedSearchFiltersListModel(QObject *parent)
    : BaseListModel(parent)
{
    setKeyField("id");
    setFields({"id", "name", "type", "filterId"});
}
