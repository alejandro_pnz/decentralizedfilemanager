/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "recyclebinmodel.h"

#include <QCoreApplication>

#include "DatabaseManager.h"
#include "AuthenticationManager.h"

RecycleBinModel *RecycleBinModel::get()
{
    return TSingleton<RecycleBinModel>::instance();
}

RecycleBinModel::~RecycleBinModel()
{

}

void RecycleBinModel::restoreItems(const QModelIndexList& indexes)
{
    if (indexes.isEmpty()) {
        return;
    }

    const auto objects = objectList(indexes);

    DatabaseManager::get()->restore(objects);
}

void RecycleBinModel::deleteItems(const QModelIndexList& indexes)
{
    if (indexes.isEmpty()) {
        return;
    }

    const auto objects = objectList(indexes);

    DatabaseManager::get()->remove(objects);
}

void RecycleBinModel::emptyRecycleBin()
{
    if (count() == 0) {
        return;
    }

    DatabaseManager::get()->remove(source());
}

RecycleBinModel::RecycleBinModel()
    : SyncableObjectListModel()
{

}

QVariantList RecycleBinModel::source() const
{
    ArcaObjectList arcaList = DatabaseManager::get()->deletedArcas();
    QuodObjectList quodList = DatabaseManager::get()->deletedQuods();
    FileObjectList fileList = DatabaseManager::get()->deletedFiles();

    return { QVariant::fromValue(arcaList).toList() + QVariant::fromValue(quodList).toList() + QVariant::fromValue(fileList).toList() };
}

void _q_RecycleBinModel_registerTypes() {
    qmlRegisterSingletonType<RecycleBinModel>("com.test", 1, 0, "RecycleBinModel", [](QQmlEngine *, QJSEngine *) -> QObject * {
        return RecycleBinModel::get();
    });
}

Q_COREAPP_STARTUP_FUNCTION(_q_RecycleBinModel_registerTypes)
