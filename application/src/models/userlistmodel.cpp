
#include "userlistmodel.h"

#include <QQmlEngine>
#include <QCoreApplication>

#include "AuthenticationManager.h"
#include "DatabaseManager.h"
#include "UserManager.h"

UserListModel *UserListModel::get()
{
    return TSingleton<UserListModel>::instance();
}

UserListModel::~UserListModel()
{

}

QVariant UserListModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    QVariant d = QSListModel::data(index, role);

    if (d.isValid()) {
        return d;
    }

    auto roleName = roleNames().value(role);

    if (roleName == "object") {
        UserObject user = UserManager::get()->user(QSListModel::data(index, roleNames().key("id")).toUuid());

        if (user.isValid()) {
            UserObject *qmlUser = new UserObject(user);
            qmlUser->detach();
            QQmlEngine::setObjectOwnership(qmlUser, QQmlEngine::JavaScriptOwnership);
            return QVariant::fromValue(qmlUser);
        }
    }

    return {};
}

void UserListModel::inviteUser(const QString &email)
{
    // TODO: invite user
    UserManager::get()->inviteUser(email);
}

bool UserListModel::anyPowerUser(const QModelIndexList &indexes) const
{
    return anyTrue(indexes, "hasPower");
}

void UserListModel::pinList(const QModelIndexList &indexes, const bool pin)
{
    const int idRole = roleNames().key("id");
    UserObjectList list;
    for (const auto& index : indexes) {
        auto object = UserManager::get()->user(QSListModel::data(index, idRole).toUuid());
        if (object.isValid()) {
            list.append(object);
        }
    }
    DatabaseManager::get()->pin(list, pin);
}

UserObject *UserListModel::qmlObject(const QUuid &id) const
{
    UserObject user = UserManager::get()->user(id);

    if (user.isValid()) {
        UserObject *qmlUser = new UserObject(user);
        qmlUser->detach();
        QQmlEngine::setObjectOwnership(qmlUser, QQmlEngine::JavaScriptOwnership);
        return qmlUser;
    }

    return nullptr;
}

UserListModel::UserListModel()
{
    setRoleNames({"id", "userId", "name", "nickname", "email", "hasPower", "guest", "pinned", "online", "blocked", "pending", "invited", "invitedBy", "created", "object"});

    connect(AuthenticationManager::get(), &AuthenticationManager::loginStateChanged, this, &UserListModel::sync);

    connect(UserManager::get(), &UserManager::usersAdded, this, &UserListModel::sync);
    connect(UserManager::get(), &UserManager::usersUpdated, this, &UserListModel::sync);
    connect(UserManager::get(), &UserManager::usersRemoved, this, &UserListModel::sync);

    sync();
}

QVariantList UserListModel::source() const
{
    auto users = UserManager::get()->allUsers();

    return QVariant::fromValue(users).toList();
}

SyncableObject UserListModel::object(const QUuid &id)
{
    return UserManager::get()->user(id);
}

QSharedPointer<SyncableObject> UserListModel::objectPtr(const QUuid &id)
{
    UserObject user = UserManager::get()->user(id);

    if (user.isValid()) {
        return UserObjectPtr(new UserObject(user));
    }

    return nullptr;
}

void _q_UserListModel_registerTypes() {
    qmlRegisterSingletonType<UserListModel>("com.test", 1, 0, "UserListModel", [](QQmlEngine *, QJSEngine *) -> QObject * {
        return UserListModel::get();
    });
}

Q_COREAPP_STARTUP_FUNCTION(_q_UserListModel_registerTypes)
