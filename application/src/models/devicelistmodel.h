/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef DEVICELISTMODEL_H
#define DEVICELISTMODEL_H

#include "basemodel.h"
#include "singleton.h"
#include <DeviceObject.h>

class DeviceListModel : public BaseModel
{
    Q_OBJECT
    Q_PROPERTY(int allowedDevicesCount READ getAllowedDevicesCount CONSTANT)
    Q_PROPERTY(int myDevicesCount READ getMyDevicesCount NOTIFY myDevicesCountChanged)
    Q_PROPERTY(int otherDevicesCount READ getOtherDevicesCount NOTIFY otherDevicesCountChanged)

public:
    static DeviceListModel *get();

    ~DeviceListModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE void activateList(const QModelIndexList& indexes, const bool activate);
    Q_INVOKABLE void activateOne(const QUuid& id);
    Q_INVOKABLE void blockList(const QModelIndexList& indexes, const bool block);
    Q_INVOKABLE void blockOne(const QUuid& id);
    Q_INVOKABLE void givePowerList(const QModelIndexList& indexes, const bool block);
    Q_INVOKABLE void givePowerOne(const QUuid& id);
    Q_INVOKABLE void logoutList(const QModelIndexList& indexes);
    Q_INVOKABLE void logoutOne(const QUuid& id);
    Q_INVOKABLE void removeList(const QModelIndexList& indexes);
    Q_INVOKABLE void removeOne(const QUuid& id);

    Q_INVOKABLE bool anyActive(const QModelIndexList& indexes) const;
    Q_INVOKABLE bool anyBlocked(const QModelIndexList& indexes) const;
    Q_INVOKABLE bool anyPowerDevice(const QModelIndexList& indexes) const;

    Q_INVOKABLE void pinList(const QModelIndexList& indexes, const bool pin = false) override;

    Q_INVOKABLE DeviceObject *qmlObject(const QUuid &id) const;

    Q_INVOKABLE bool canAddNewDevice() const;

protected:
    DeviceListModel();

    DeviceObjectList devicesFromIndexes(const QModelIndexList& indexes);

    QVariantList source() const override;

    friend class TSingleton<DeviceListModel>;

protected:
    SyncableObject object(const QUuid &id) override;
    QSharedPointer<SyncableObject> objectPtr(const QUuid &id) override;

private:
    int  getAllowedDevicesCount() const;
    int  getMyDevicesCount() const;
    int  getOtherDevicesCount() const;

signals:
    void otherDevicesCountChanged();
    void myDevicesCountChanged();

};
#endif // DEVICELISTMODEL_H
