#ifndef ATTRIBUTEMODEL_H
#define ATTRIBUTEMODEL_H

#include "singleton.h"

#include "ArcaObject.h"

#include "QSListModel"

class AttributeSectionModel : public QSListModel
{
    Q_OBJECT
    Q_PROPERTY(QVariantList source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(QuodSectionObject* section MEMBER m_section NOTIFY sectionChanged)

public:
    AttributeSectionModel(QObject *parent = nullptr);

    ~AttributeSectionModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QVariantList source() const;
    void setSource(const QVariantList &source);

    Q_INVOKABLE void revertChanges();

Q_SIGNALS:
    void sourceChanged();
    void sectionChanged();

protected:
    void setKeyField(const QString &keyField);

    void sync();

private:
    QString m_keyField;

    QVariantList m_source;
    QuodSectionObject *m_section{nullptr};
};

#endif // ATTRIBUTEMODEL_H
