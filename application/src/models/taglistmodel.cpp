#include "taglistmodel.h"

#include <QQmlEngine>

#include "AuthenticationManager.h"
#include "DatabaseManager.h"

#include "qsdiffrunner.h"

TagListModel *TagListModel::get()
{
    return TSingleton<TagListModel>::instance();
}

TagListModel::~TagListModel()
{

}

QVariant TagListModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    QVariant d = QSListModel::data(index, role);

    if (d.isValid()) {
        return d;
    }

    if (roleNames().value(role) == "object") {
        const auto id = QSListModel::data(index, roleNames().key("id")).toUuid();
        auto it = std::find_if(m_predefinedTags.constBegin(), m_predefinedTags.constEnd(), [&id](const auto &tag) {
            return tag.id() == id;
        });

        const TagObject tag = (it == m_predefinedTags.end()) ? DatabaseManager::get()->tag(id) : *it;

        if (tag.isValid()) {
            TagObject *qmlTag = new TagObject(tag);
            qmlTag->detach();
            QQmlEngine::setObjectOwnership(qmlTag, QQmlEngine::JavaScriptOwnership);
            return QVariant::fromValue(qmlTag);
        }
    }

    return {};
}

QVariant TagListModel::addTag(const QString &name, const QString &color)
{
    TagObject tag;
    tag.setName(name);
    tag.setColor(color);

    bool ret = DatabaseManager::get()->add(tag);

    qDebug() << "Add tag(" << name << ")" << ret;
    if (ret) {
        TagObject *qmlTag = new TagObject(tag);
        qmlTag->detach();
        QQmlEngine::setObjectOwnership(qmlTag, QQmlEngine::JavaScriptOwnership);
        return QVariant::fromValue(qmlTag);
    } else {
        return {};
    }

}

void TagListModel::updateTag(TagObject *obj)
{
    if (!obj || !obj->isValid()) {
        qWarning() << "Invalid tag object";
        return;
    }

    bool ret = DatabaseManager::get()->update(TagObject(obj));

    qDebug() << "Update tag(" << obj->name() << ")" << ret;
}

bool TagListModel::tagNameExists(const QString &name) const
{
    return !match(index(0, 0), roleNames().key("name"), name, 1, Qt::MatchExactly | Qt::MatchRecursive).isEmpty();
}

bool TagListModel::proceedTagDeletion()
{
    auto tag = DatabaseManager::get()->tag(m_tagToDelete);
    if (tag.isValid()) {
        if (DatabaseManager::get()->remove(tag)) {
            emit tagDeleted(m_tagToDelete);
            m_tagToDelete = QUuid();
            return true;
        } else {
            qWarning() <<Q_FUNC_INFO << "Cannot remove tag: db error";
            return false;
        }
    }
    qWarning() <<Q_FUNC_INFO << "Cannot remove tag: invalid tag";
    return false;
}

void TagListModel::requestTagDeletion(const QUuid &id)
{
    m_tagToDelete = id;
}

void TagListModel::abortTagDeletion()
{
    m_tagToDelete = QUuid();
}

QStringList TagListModel::getTagNames(const QModelIndexList &indexes)
{
    auto nameRole = roleNames().key("name");
    QStringList names;
    for (const auto& index : indexes) {
        auto name = data(index, nameRole);
        if (name.isValid()) {
            names.append(name.toString());
        }
    }
    return names;
}

TagListModel::TagListModel()
{
    setKeyField("id");
    setRoleNames({"id", "name", "color", "object"});

    connect(AuthenticationManager::get(), &AuthenticationManager::loginStateChanged, this, &TagListModel::sync);

    connect(DatabaseManager::get(), &DatabaseManager::tagsAdded, this, &TagListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::tagsUpdated, this, &TagListModel::sync);
    connect(DatabaseManager::get(), &DatabaseManager::tagsRemoved, this, &TagListModel::sync);

    QFile jsonFile(":/tags.json");
    if (jsonFile.open(QIODevice::ReadOnly)) {
        QJsonDocument doc = QJsonDocument::fromJson(jsonFile.readAll());
        if (!doc.array().isEmpty()) {
            const auto values = doc.array().toVariantList();
            for(const auto &val: values) {
                const auto obj = val.value<TagObject>();
                m_predefinedTags.append(obj);
            }
        }
    }
}

void TagListModel::setKeyField(const QString &keyField)
{
    m_keyField = keyField;
}

void TagListModel::sync()
{
    qDebug() << "TagListModel::sync()";

    QSDiffRunner runner;
    runner.setKeyField(m_keyField);


    TagObjectList source = DatabaseManager::get()->allTags();

    source.append(m_predefinedTags);

    QList<QSPatch> patches = runner.compare(storage(), QVariant::fromValue(source).value<QVariantList>());

    if (!patches.isEmpty()) {
        runner.patch(this, patches);
    }
}
