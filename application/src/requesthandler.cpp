/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "requesthandler.h"

#include <QtConcurrent>

#include <DatabaseManager.h>

RequestHandler::RequestHandler()
{
    router.addRoute("GET", "^/pdfviewer/(.*)", this, &RequestHandler::handleWebViewer);

    router.addRoute("GET", "^/mdeditor/(.*)", this, &RequestHandler::handleTUIEditor);

    router.addRoute("GET", "^/files/(.*)", this, &RequestHandler::handleFiles);
}

HttpPromise RequestHandler::handle(HttpDataPtr data)
{
    bool foundRoute;
    HttpPromise promise = router.route(data, &foundRoute);
    if (foundRoute)
        return promise;

    if (data->request->mimeType().compare("application/json", Qt::CaseInsensitive) != 0)
        throw HttpException(HttpStatus::BadRequest, "Request body content type must be application/json");

    QJsonDocument jsonDocument = data->request->parseJsonBody();
    if (jsonDocument.isNull())
        throw HttpException(HttpStatus::BadRequest, "Invalid JSON body");

    QJsonObject object;
    object["test"] = 5;
    object["another test"] = "OK";

    data->response->setStatus(HttpStatus::Ok, QJsonDocument(object));
    return HttpPromise::resolve(data);
}

HttpPromise RequestHandler::handleWebViewer(HttpDataPtr data)
{
    auto match = data->state["match"].value<QRegularExpressionMatch>();
    const QString file = match.captured(0);

//    QString mimeType{"text/html"};

//    if (file.endsWith(".js")) {
//        mimeType = "text/javascript";
//    }
//    else if (file.endsWith(".css")) {
//        mimeType = "text/css";
//    }
//    else if (file.endsWith(".wasm")) {
//        mimeType = "application/wasm";
//    }
//    else if (file.endsWith(".mem")) {
//        mimeType = "application/octet-stream";
//    }
//    else if (file.endsWith(".gz")) {
//        mimeType = "application/octet-stream";
//    }

    data->response->sendFile(":" + file, "", "utf-8", -1, -2);

    data->response->setStatus(HttpStatus::Ok);
    return HttpPromise::resolve(data);
}

HttpPromise RequestHandler::handleTUIEditor(HttpDataPtr data)
{
    auto match = data->state["match"].value<QRegularExpressionMatch>();
    const QString file = match.captured(0);

    QString mimeType{"text/html"};

    if (file.endsWith(".js")) {
        mimeType = "text/javascript";
    }
    else if (file.endsWith(".css")) {
        mimeType = "text/css";
    }
    else if (file.endsWith(".wasm")) {
        mimeType = "application/wasm";
    }
    else if (file.endsWith(".mem")) {
        mimeType = "application/octet-stream";
    }

    data->response->sendFile(":" + file, mimeType, "utf-8", -1, -2);

    data->response->setStatus(HttpStatus::Ok);
    return HttpPromise::resolve(data);
}

HttpPromise RequestHandler::handleFiles(HttpDataPtr data)
{
    auto match = data->state["match"].value<QRegularExpressionMatch>();
    return HttpPromise::resolve(data).then([match](HttpDataPtr data) {

        return QtPromise::resolve(QtConcurrent::run([](const QRegularExpressionMatch &match, HttpDataPtr data) {
            int first = -1;
            int last = -1;

            QString range;
            data->request->header("Range", &range);

            if (!range.isEmpty()) {
                QRegularExpression re("((?<byteRangeSpec>(?<firstBytePos>\\d+)-(?<lastBytePos>\\d+)?))");
                QRegularExpressionMatch m = re.match(range);

                if (m.hasMatch()) {
                    bool ok{false};
                    int firstByte = m.captured("firstBytePos").toInt(&ok);
                    if (ok) {
                        first = firstByte;
                        int lastByte = m.captured("lastBytePos").toInt(&ok);
                        if (ok) {
                            last = lastByte;
                        }
                    }
                }
            }

            const QString fileId = match.captured(1);

            auto file = DatabaseManager::get()->file(QUuid(fileId));

            auto fileData = DatabaseManager::get()->fileData(file);

            if (!fileData) {
                data->response->setStatus(HttpStatus::Unauthorized);
                return data;
            }

            int compression = range.isEmpty() ? -2 : -2;

            data->response->sendFile(fileData.get(), "", "utf-8", first, last, compression);

            if (!range.isEmpty()) {
                data->response->setStatus(HttpStatus::PartialContent);
            }
            else {
                data->response->setStatus(HttpStatus::Ok);
            }

            return data;
        }, match, data));
    });

//    int len = -1;

//    QString range;
//    data->request->header("Range", &range);

//    if (!range.isEmpty()) {
//        len = 200;
//    }

//    QString fileId = match.captured(0);

//    fileId.remove("/files/");

//    auto file = DatabaseManager::get()->file(fileId);

//    auto fileData = DatabaseManager::get()->fileData(file);

//    data->response->sendFile(fileData.get(), "", "utf-8", len, -2 /*Z_DEFAULT_COMPRESSION*/);

//    if (!range.isEmpty()) {
//        data->response->setStatus(HttpStatus::PartialContent);
//    }
//    else {
//        data->response->setStatus(HttpStatus::Ok);
//    }

//    return HttpPromise::resolve(data);
}
