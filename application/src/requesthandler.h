/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef REQUESTHANDLER_H
#define REQUESTHANDLER_H

#include <QtPromise>
#include <QTimer>

#include "httpServer/httpData.h"
#include "httpServer/httpRequestHandler.h"
#include "httpServer/httpRequestRouter.h"


class RequestHandler : public HttpRequestHandler
{
private:
    HttpRequestRouter router;

public:
    RequestHandler();

    HttpPromise handle(HttpDataPtr data);

    HttpPromise handleWebViewer(HttpDataPtr data);

    HttpPromise handleTUIEditor(HttpDataPtr data);

    HttpPromise handleFiles(HttpDataPtr data);
};

#endif // REQUESTHANDLER_H
