
# must use a variable as input
mdeditor_DEPS = $$PWD/mdeditor.qrc
PreBuildMdeditor.input = mdeditor_DEPS
# use non-existing file here to execute every time
PreBuildMdeditor.output = $$OUT_PWD/mdeditor.rcc
# the system call to the batch file
PreBuildMdeditor.commands = $$[QT_HOST_LIBEXECS]/rcc -binary -no-compress ${QMAKE_FILE_IN} -o ${QMAKE_FILE_OUT}
# some name that displays during execution
PreBuildMdeditor.name = Preparing markdown editor resource...
# "no_link" tells qmake we don’t need to add the output to the object files for linking,
# "target_predeps" tells qmake that the output of this needs to exist before we can do the rest of our compilation.
PreBuildMdeditor.CONFIG += no_link target_predeps
# Add the compiler to the list of 'extra compilers'.
QMAKE_EXTRA_COMPILERS += PreBuildMdeditor

android {
    mdeditor_assets.files = $$OUT_PWD/mdeditor.rcc
    mdeditor_assets.path = /assets/mdeditor
    mdeditor_assets.CONFIG += no_check_exist
    INSTALLS += mdeditor_assets
}

mac {
    mdeditor.files = $$OUT_PWD/mdeditor.rcc
    macx: mdeditor.path = Contents/Resources
    ios: mdeditor.path = .
    QMAKE_BUNDLE_DATA += mdeditor
}
