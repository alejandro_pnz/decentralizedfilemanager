/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "analytics.h"

#include "analytics_p.h"

#include "Utils.h"

#include <QUuid>
#include <QQmlEngine>
#include <QCoreApplication>

Analytics *Analytics::get()
{
    return TSingleton<Analytics>::instance();
}

Analytics::Analytics(QObject *parent)
    : QObject(parent),
      d_ptr(new AnalyticsPrivate)
{
    setHost("https://countly.test.com");

    setDeviceId(Utils::get()->uuid().toString(QUuid::WithoutBraces).toLower());

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
    setAppKey("fb864b424fd05c44881d664d6041c33b2d2249fb");
#elif defined(Q_OS_OSX) || defined(Q_OS_WINDOWS)
    setAppKey("ec19431acbd8500522698e7dbb795445a5144164");
#else
    qWarning() << "Analytics is not supported for this platform";
#endif
}

Analytics::~Analytics()
{

}

void Analytics::init()
{
    d_ptr->init();
}

const QString &Analytics::host() const
{
    return d_ptr->host;
}

const QString &Analytics::appKey() const
{
    return d_ptr->appKey;
}

const QString &Analytics::deviceId() const
{
    return d_ptr->deviceId;
}

void Analytics::setHost(const QString &host)
{
    if (d_ptr->host == host) {
        return;
    }

    d_ptr->host = host;
    Q_EMIT this->hostChanged();
}

void Analytics::setAppKey(const QString &appKey)
{
    if (d_ptr->appKey == appKey) {
      return;
    }

    d_ptr->appKey = appKey;
    Q_EMIT this->appKeyChanged();
}

void Analytics::setDeviceId(const QString &deviceId)
{
    if (d_ptr->deviceId == deviceId) {
      return;
    }

    d_ptr->deviceId = deviceId;
    Q_EMIT this->deviceIdChanged();
}

void Analytics::sendEvent(const QString &name)
{
    d_ptr->sendEvent(name);
}

void Analytics::sendScreen(const QString &name)
{
    d_ptr->sendScreen(name);
}

static void _q_Analytics_registerTypes() {
    qmlRegisterSingletonType<Analytics>("com.test", 1, 0, "Analytics", [](QQmlEngine *, QJSEngine *) -> QObject * {
        return Analytics::get();
    });
}

Q_COREAPP_STARTUP_FUNCTION(_q_Analytics_registerTypes)
