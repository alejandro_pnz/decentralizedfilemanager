
QT += concurrent

CONFIG(release, debug|release): PATH = release
else:CONFIG(debug, debug|release): PATH = debug

# iOS and OSX common settings
mac {
    QMAKE_OBJECTIVE_CFLAGS += -fobjc-arc
    QMAKE_OBJECTIVE_CXXFLAGS += -fobjc-arc
}

android {
    SOURCES += \
        $$PWD/analytics_p_android.cpp
}

ios {
    CONFIG(iphonesimulator, iphoneos|iphonesimulator) {
        SOURCES += \
            $$PWD/analytics_dummy.cpp
    }

    CONFIG(iphoneos, iphoneos|iphonesimulator) {
        OBJECTIVE_SOURCES += \
            $$PWD/analytics_p.mm

        INCLUDEPATH += $$PWD/countly/ios/$$PATH/Countly.framework/Headers

        QMAKE_LFLAGS += -F$$PWD/countly/ios/$$PATH

        LIBS += -framework Countly

        QMAKE_LFLAGS += -rpath @executable_path/Frameworks

        embedFramework.files = $$files($$PWD/countly/ios/$$PATH/Countly.framework)
        embedFramework.path = Frameworks    # This is the essential keyword for qmake to add the desired Framework as "embedded Framework"
        QMAKE_BUNDLE_DATA += embedFramework
    }
}

macx {
    OBJECTIVE_SOURCES += \
        $$PWD/analytics_p.mm

    INCLUDEPATH += $$PWD/countly/macx/$$PATH/Countly.framework/Headers

    QMAKE_LFLAGS += -F$$PWD/countly/macx/$$PATH

    LIBS += -framework Countly

    embedFramework.files = $$files($$PWD/countly/macx/$$PATH/Countly.framework)
    embedFramework.path = Contents/Frameworks   # This is the essential keyword for qmake to add the desired Framework as "embedded Framework"
    QMAKE_BUNDLE_DATA += embedFramework
}

win32 {
    INCLUDEPATH += \
        $$PWD/countly/win

    HEADERS += \
        $$PWD/countly/win/Countly.h \
        $$PWD/countly/win/CountlyCommon.h \
        $$PWD/countly/win/CountlyConfig.h \
        $$PWD/countly/win/CountlyConnectionManager.h \
        $$PWD/countly/win/CountlyConsentManager.h \
        $$PWD/countly/win/CountlyDeviceInfo.h \
        $$PWD/countly/win/CountlyEvent.h \
        $$PWD/countly/win/CountlyLocationManager.h \
        $$PWD/countly/win/CountlyPersistency.h

    SOURCES += \
        $$PWD/analytics_p_windows.cpp \
        $$PWD/countly/win/Countly.cpp \
        $$PWD/countly/win/CountlyCommon.cpp \
        $$PWD/countly/win/CountlyConfig.cpp \
        $$PWD/countly/win/CountlyConnectionManager.cpp \
        $$PWD/countly/win/CountlyConsentManager.cpp \
        $$PWD/countly/win/CountlyDeviceInfo.cpp \
        $$PWD/countly/win/CountlyEvent.cpp \
        $$PWD/countly/win/CountlyLocationManager.cpp \
        $$PWD/countly/win/CountlyPersistency.cpp
}

HEADERS += \
    $$PWD/analytics.h \
    $$PWD/analytics_p.h

SOURCES += \
    $$PWD/analytics.cpp

INCLUDEPATH += \
    $$PWD
