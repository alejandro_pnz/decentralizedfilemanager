
#include "analytics_p.h"

#include "Countly.h"

#include <QUuid>

#include <Utils.h>

AnalyticsPrivate::AnalyticsPrivate()
{

}

AnalyticsPrivate::~AnalyticsPrivate()
{

}

void AnalyticsPrivate::init()
{
    CountlyConfig *config = [[CountlyConfig alloc] init];
    config.deviceID = deviceId.toNSString();
    config.resetStoredDeviceID = YES;
    config.appKey = appKey.toNSString();
    config.host = host.toNSString();

    config.alwaysUsePOST = YES;

    config.enableDebug = NO;

    config.enablePerformanceMonitoring = YES;

    config.features = @[CLYPushNotifications];

    config.pushTestMode = CLYPushTestModeDevelopment;

    config.requiresConsent = YES;

    config.consents = @[CLYConsentPushNotifications, CLYConsentSessions, CLYConsentEvents, CLYConsentViewTracking, CLYConsentLocation, CLYConsentPerformanceMonitoring];

    [Countly.sharedInstance startWithConfig:config];

    [Countly.sharedInstance askForNotificationPermission];
}

void AnalyticsPrivate::sendEvent(const QString &name)
{
    [Countly.sharedInstance recordEvent:name.toNSString()];
}

void AnalyticsPrivate::sendScreen(const QString &name)
{
    [Countly.sharedInstance recordView:name.toNSString()];
}
