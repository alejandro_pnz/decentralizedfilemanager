/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/

#include "analytics_p.h"

#include <QDebug>

AnalyticsPrivate::AnalyticsPrivate()
{

}

AnalyticsPrivate::~AnalyticsPrivate()
{

}

void AnalyticsPrivate::init()
{
    qWarning() << "Dummy Analytics init";
}

void AnalyticsPrivate::sendEvent(const QString &name)
{
    qWarning() << "Dummy Analytics send event" << name;
}

void AnalyticsPrivate::sendScreen(const QString &name)
{
    qWarning() << "Dummy Analytics send screen" << name;
}
