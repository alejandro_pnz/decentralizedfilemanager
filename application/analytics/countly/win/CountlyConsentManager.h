/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef COUNTLYCONSENTMANAGER_H
#define COUNTLYCONSENTMANAGER_H

#include <QObject>

#include "CountlyCommon.h"

class CountlyConsentManager : public QObject
{
    Q_OBJECT

public:
    explicit CountlyConsentManager(QObject *parent = nullptr);

    static CountlyConsentManager *sharedInstance();

    bool consentForSessions() const;
    void setConsentForSessions(bool consent);

    bool consentForEvents() const;
    void setConsentForEvents(bool consent);

    bool consentForUserDetails() const;
    bool consentForCrashReporting() const;
    bool consentForPushNotifications() const;

    bool consentForLocation() const;
    void setConsentForLocation(bool consent);

    bool consentForViewTracking() const;
    bool consentForAttribution() const;
    bool consentForAppleWatch() const;
    bool consentForPerformanceMonitoring() const;
    bool consentForFeedback() const;
    bool consentForRemoteConfig() const;

    void giveConsentForFeatures(const QList<CLYConsent> &features);

protected:
    void sendConsentChanges();

private:
    static QPointer<CountlyConsentManager> m_instance;
    static QMutex m_mutex;

    bool m_requiresConsent{true};

    bool m_consentForSessions{false};
    bool m_consentForEvents{false};
    bool m_consentForUserDetails{false};
    bool m_consentForCrashReporting{false};
    bool m_consentForPushNotifications{false};
    bool m_consentForLocation{false};
    bool m_consentForViewTracking{false};
    bool m_consentForAttribution{false};
    bool m_consentForAppleWatch{false};
    bool m_consentForPerformanceMonitoring{false};
    bool m_consentForFeedback{false};
    bool m_consentForRemoteConfig{false};

    QVariantMap m_consentChanges;

    friend class Countly;
};

#endif // COUNTLYCONSENTMANAGER_H
