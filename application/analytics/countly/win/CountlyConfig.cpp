/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "CountlyConfig.h"

//NOTE: Device ID options
const char* const CLYDefaultDeviceID = ""; //NOTE: It will be overridden to default device ID mechanism, depending on platform.
const char* const CLYTemporaryDeviceID = "CLYTemporaryDeviceID";

//NOTE: Device ID Types
CLYDeviceIDType const CLYDeviceIDTypeCustom     = "CLYDeviceIDTypeCustom";
CLYDeviceIDType const CLYDeviceIDTypeTemporary  = "CLYDeviceIDTypeTemporary";
CLYDeviceIDType const CLYDeviceIDTypeIDFV       = "CLYDeviceIDTypeIDFV";
CLYDeviceIDType const CLYDeviceIDTypeNSUUID     = "CLYDeviceIDTypeNSUUID";

CountlyConfig::CountlyConfig(QObject *parent)
    : QObject(parent)
{

}
