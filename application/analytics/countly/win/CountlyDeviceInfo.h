/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef COUNTLYDEVICEINFO_H
#define COUNTLYDEVICEINFO_H

#include <QObject>
#include <QVariantMap>
#include <QMutex>

class CountlyDeviceInfo : public QObject
{
    Q_OBJECT

public:
    explicit CountlyDeviceInfo(QObject *parent = nullptr);

    static CountlyDeviceInfo *sharedInstance();

    QVariantMap customMetrics() const;
    QString deviceID() const;

    void initializeDeviceID(const QString &deviceID);
    QString ensafeDeviceID(const QString &deviceID);
    bool isDeviceIDTemporary() const;

    static QString device();
    static QString architecture();
    static QString osName();
    static QString osVersion();
    static QString carrier();
    static QString resolution();
    static QString density();
    static QString locale();
    static QString appVersion();
    static QString appBuild();
    #if (TARGET_OS_IOS)
    int hasWatch();
    int installedWatchApp();
    #endif

    static QString metrics();

    quint16 connectionType() const;
    static quint64 freeRAM();
    static quint64 totalRAM();
    static quint64 freeDisk();
    static quint64 totalDisk();
    static int batteryLevel();
    static QString orientation();
    static bool isJailbroken();
    static bool isInBackground();

protected:
    static QString deviceType();

private:
    static QPointer<CountlyDeviceInfo> m_instance;
    static QMutex m_mutex;

    QString m_deviceID;
    QVariantMap m_customMetrics;

    friend class Countly;
};

#endif // COUNTLYDEVICEINFO_H
