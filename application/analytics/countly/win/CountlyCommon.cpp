/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "CountlyCommon.h"

#include <QPointer>

#include <QTimeZone>

QPointer<CountlyCommon> CountlyCommon::m_instance{nullptr};
QMutex CountlyCommon::m_mutex;

const char* const kCountlySDKVersion = "20.11.2";
const char* const kCountlySDKName = "qt-native-";

const char* const kCountlyInternalLogPrefix = "[Countly] ";

CountlyCommon::CountlyCommon(QObject *parent)
    : QObject(parent)
{

}

CountlyCommon *CountlyCommon::sharedInstance()
{
    if (!m_instance) {
        // lock & double check
        QMutexLocker locker(&m_mutex);

        if (!m_instance) {
            m_instance = QPointer<CountlyCommon>(new CountlyCommon);
        }
    }

    return m_instance;
}

QString CountlyCommon::SDKVersion() const
{
    return kCountlySDKVersion;
}

QString CountlyCommon::SDKName() const
{
    return QString(kCountlySDKName) +
#if defined(Q_OS_ANDROID)
            "android"
#elif defined(Q_OS_IOS)
            "ios"
#elif defined(Q_OS_MACOS)
            "macos"
#elif defined(Q_OS_WINDOWS)
            "windows"
#else
            "unknown"
#endif
            ;
}

bool CountlyCommon::hasStarted() const
{
    return m_hasStarted;
}

bool CountlyCommon::enableDebug() const
{
    return m_enableDebug;
}

void *CountlyCommon::loggerDelegate() const
{
    return m_loggerDelegate;
}

CLYInternalLogLevel CountlyCommon::internalLogLevel() const
{
    return m_internalLogLevel;
}

QString CountlyCommon::attributionID() const
{
    return m_attributionID;
}

bool CountlyCommon::manualSessionHandling()
{
    return m_manualSessionHandling;
}

quint8 CountlyCommon::hourOfDay() const
{
    return QDateTime::currentDateTime().time().hour();
}

quint8 CountlyCommon::dayOfWeek() const
{
    return QDateTime::currentDateTime().date().dayOfWeek();
}

quint32 CountlyCommon::timeZone() const
{
    QDateTime localDateTime = QDateTime::currentDateTime();
    return localDateTime.timeZone().offsetFromUtc(localDateTime) / 60;
}

quint64 CountlyCommon::uniqueTimestamp() const
{
    quint64 now = QDateTime::currentDateTime().toMSecsSinceEpoch();

    if (now <= m_lastTimestamp) {
        m_lastTimestamp++;
    }
    else {
        m_lastTimestamp = now;
    }

    return m_lastTimestamp / 1000;
}

void CountlyInternalLog(CLYInternalLogLevel level, const char *format, ...)
{
    if (!CountlyCommon::sharedInstance()->enableDebug() && !CountlyCommon::sharedInstance()->loggerDelegate()) {
        return;
    }

    if (level > CountlyCommon::sharedInstance()->internalLogLevel()) {
        return;
    }


    va_list args;
    va_start(args, format);

    QString logString = QString::vasprintf(format, args);

    QStringList logLevelPrefixes
    {
        "None",
        "Error",
        "Warning",
        "Info",
        "Debug",
        "Verbose",
    };

    logString = QString("[%1] %2").arg(logLevelPrefixes[level], logString);

    const QString logStringWithPrefix = QString(kCountlyInternalLogPrefix) + logString;

    switch (level) {
        case CLYInternalLogLevelNone: break;
        case CLYInternalLogLevelError: qCritical() << logStringWithPrefix; break;
        case CLYInternalLogLevelWarning: qWarning() << logStringWithPrefix; break;
        case CLYInternalLogLevelInfo: qInfo() << logStringWithPrefix; break;
        case CLYInternalLogLevelDebug:
        case CLYInternalLogLevelVerbose: qDebug() << logStringWithPrefix;
    }

    va_end(args);
}
