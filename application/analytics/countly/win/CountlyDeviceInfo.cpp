/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "CountlyDeviceInfo.h"

#include <QPointer>

#include <QJsonDocument>
#include <QJsonObject>

#include <QGuiApplication>
#include <QSysInfo>
#include <QWindow>
#include <QScreen>
#include <QLocale>

#include "CountlyCommon.h"

CLYMetricKey const CLYMetricKeyDevice             = "_device";
CLYMetricKey const CLYMetricKeyDeviceType         = "_device_type";
CLYMetricKey const CLYMetricKeyOS                 = "_os";
CLYMetricKey const CLYMetricKeyOSVersion          = "_os_version";
CLYMetricKey const CLYMetricKeyAppVersion         = "_app_version";
CLYMetricKey const CLYMetricKeyCarrier            = "_carrier";
CLYMetricKey const CLYMetricKeyResolution         = "_resolution";
CLYMetricKey const CLYMetricKeyDensity            = "_density";
CLYMetricKey const CLYMetricKeyLocale             = "_locale";
CLYMetricKey const CLYMetricKeyHasWatch           = "_has_watch";
CLYMetricKey const CLYMetricKeyInstalledWatchApp  = "_installed_watch_app";

QPointer<CountlyDeviceInfo> CountlyDeviceInfo::m_instance{nullptr};
QMutex CountlyDeviceInfo::m_mutex;

CountlyDeviceInfo::CountlyDeviceInfo(QObject *parent)
    : QObject(parent),
      m_deviceID(CountlyPersistency::sharedInstance()->retrieveDeviceID())
{

}

CountlyDeviceInfo *CountlyDeviceInfo::sharedInstance()
{
    if (!m_instance) {
        // lock & double check
        QMutexLocker locker(&m_mutex);

        if (!m_instance) {
            m_instance = QPointer<CountlyDeviceInfo>(new CountlyDeviceInfo);
        }
    }

    return m_instance;
}

QVariantMap CountlyDeviceInfo::customMetrics() const
{
    return m_customMetrics;
}

QString CountlyDeviceInfo::deviceID() const
{
    return m_deviceID;
}

void CountlyDeviceInfo::initializeDeviceID(const QString &deviceID)
{
    m_deviceID = deviceID;

    CountlyPersistency::sharedInstance()->storeDeviceID(deviceID);
}

QString CountlyDeviceInfo::device()
{
    qWarning() << "CountlyDeviceInfo::device() NIY";
    return {};
}

QString CountlyDeviceInfo::osName()
{
    QString productName = QSysInfo::prettyProductName();
    return productName.left(productName.indexOf(" "));
}

QString CountlyDeviceInfo::osVersion()
{
    return QSysInfo::productVersion();
}

QString CountlyDeviceInfo::carrier()
{
    qWarning() << "CountlyDeviceInfo::carrier() NIY";
    return {};
}

QString CountlyDeviceInfo::resolution()
{
    QScreen *screen = QGuiApplication::primaryScreen();

    if (screen) {
        return QString("%1x%2").arg(screen->devicePixelRatio() * screen->size().width()).arg(screen->devicePixelRatio() * screen->size().height());
    }

    return {};
}

QString CountlyDeviceInfo::density()
{
    QScreen *screen = QGuiApplication::primaryScreen();

    if (screen) {
        return QString("@%1x").arg(screen->devicePixelRatio());
    }

    return {};
}

QString CountlyDeviceInfo::locale()
{
    QLocale locale;

    return locale.name();
}

QString CountlyDeviceInfo::appVersion()
{
    return qApp->applicationVersion();
}

QString CountlyDeviceInfo::metrics()
{
    QJsonObject metricsDictionary;
    metricsDictionary[CLYMetricKeyDevice] = CountlyDeviceInfo::device();
    metricsDictionary[CLYMetricKeyDeviceType] = CountlyDeviceInfo::deviceType();
    metricsDictionary[CLYMetricKeyOS] = CountlyDeviceInfo::osName();
    metricsDictionary[CLYMetricKeyOSVersion] = CountlyDeviceInfo::osVersion();
    metricsDictionary[CLYMetricKeyAppVersion] = CountlyDeviceInfo::appVersion();

    QString carrier = CountlyDeviceInfo::carrier();
    if (!carrier.isEmpty()) {
        metricsDictionary[CLYMetricKeyCarrier] = carrier;
    }

    metricsDictionary[CLYMetricKeyResolution] = CountlyDeviceInfo::resolution();
    metricsDictionary[CLYMetricKeyDensity] = CountlyDeviceInfo::density();
    metricsDictionary[CLYMetricKeyLocale] = CountlyDeviceInfo::locale();

#if (TARGET_OS_IOS)
    if (CountlyCommon.sharedInstance.enableAppleWatch) {
        if (CountlyConsentManager.sharedInstance.consentForAppleWatch) {
            metricsDictionary[CLYMetricKeyHasWatch] = @(CountlyDeviceInfo.hasWatch);
            metricsDictionary[CLYMetricKeyInstalledWatchApp] = @(CountlyDeviceInfo.installedWatchApp);
        }
    }
#endif

    if (!CountlyDeviceInfo::sharedInstance()->customMetrics().isEmpty()) {
        for (QVariantMap::const_key_value_iterator it = CountlyDeviceInfo::sharedInstance()->customMetrics().constKeyValueBegin();
             it != CountlyDeviceInfo::sharedInstance()->customMetrics().constKeyValueEnd();
             ++it) {
            if ((*it).second.canConvert<QString>())  {
                metricsDictionary[(*it).first] = (*it).second.toString();
            }
        }
    }

    return QJsonDocument(metricsDictionary).toJson(QJsonDocument::Compact);
}

QString CountlyDeviceInfo::deviceType()
{
#if defined(Q_OS_OSX) || defined(Q_OS_WIN)
    return "desktop";
#else
    qWarning() << "CountlyDeviceInfo::deviceType() NIY";
    return {};
#endif
}
