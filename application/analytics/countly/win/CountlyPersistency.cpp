/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "CountlyPersistency.h"

#include <QPointer>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include <QSettings>

#include "CountlyCommon.h"

const char* const kCountlyQueuedRequestsPersistencyKey = "kCountlyQueuedRequestsPersistencyKey";
const char* const kCountlyStartedEventsPersistencyKey = "kCountlyStartedEventsPersistencyKey";
const char* const kCountlyStoredDeviceIDKey = "kCountlyStoredDeviceIDKey";
const char* const kCountlyStoredNSUUIDKey = "kCountlyStoredNSUUIDKey";
const char* const kCountlyWatchParentDeviceIDKey = "kCountlyWatchParentDeviceIDKey";
const char* const kCountlyStarRatingStatusKey = "kCountlyStarRatingStatusKey";
const char* const kCountlyNotificationPermissionKey = "kCountlyNotificationPermissionKey";
const char* const kCountlyIsCustomDeviceIDKey = "kCountlyIsCustomDeviceIDKey";
const char* const kCountlyRemoteConfigPersistencyKey = "kCountlyRemoteConfigPersistencyKey";

const char* const kCountlyCustomCrashLogFileName = "CountlyCustomCrash.log";

QPointer<CountlyPersistency> CountlyPersistency::m_instance{nullptr};
QMutex CountlyPersistency::m_mutex;

CountlyPersistency::CountlyPersistency(QObject *parent)
    : QObject(parent)
{
    QSettings settings;
    m_queuedRequests = settings.value(kCountlyQueuedRequestsPersistencyKey).toStringList();
}

CountlyPersistency::~CountlyPersistency()
{

}

CountlyPersistency *CountlyPersistency::sharedInstance()
{
    if (!m_instance) {
        // lock & double check
        QMutexLocker locker(&m_mutex);

        if (!m_instance) {
            m_instance = QPointer<CountlyPersistency>(new CountlyPersistency);
        }
    }

    return m_instance;
}

void CountlyPersistency::addToQueue(const QString &queryString)
{
    if (queryString.isEmpty()) {
        return;
    }

    QMutexLocker guard(&m_ioMutex);

    m_queuedRequests.append(queryString);

    if (m_queuedRequests.count() > m_storedRequestsLimit && !CountlyConnectionManager::sharedInstance()->connection()) {
        m_queuedRequests.removeAt(0);
    }
}

void CountlyPersistency::removeFromQueue(const QString &queryString)
{
    QMutexLocker guard(&m_ioMutex);

    if (!m_queuedRequests.isEmpty()) {
        m_queuedRequests.removeOne(queryString);
    }
}

QString CountlyPersistency::firstItemInQueue()
{
    QMutexLocker guard(&m_ioMutex);

    if (!m_queuedRequests.isEmpty()) {
        return m_queuedRequests.first();
    }

    return {};
}

void CountlyPersistency::flushQueue()
{
    QMutexLocker guard(&m_ioMutex);

    m_queuedRequests.clear();
}

void CountlyPersistency::recordEvent(const QSharedPointer<CountlyEvent> &event)
{
    QMutexLocker guard(&m_ioMutex);

    m_recordedEvents.append(event);

    if (m_recordedEvents.count() >= m_eventSendThreshold) {
        CountlyConnectionManager::sharedInstance()->sendEvents();
    }
}

QString CountlyPersistency::serializedRecordedEvents()
{
    QJsonArray events;

    QMutexLocker guard(&m_ioMutex);

    if (m_recordedEvents.isEmpty()) {
        return {};
    }

    for (const auto &event: qAsConst(m_recordedEvents)) {
        events.append(QJsonObject::fromVariantMap(event->dictionaryRepresentation()));
    }

    m_recordedEvents.clear();

    return QJsonDocument(events).toJson(QJsonDocument::Compact);
}

void CountlyPersistency::clearAllTimedEvents()
{
    QMutexLocker guard(&m_ioMutex);

    m_startedEvents.clear();
}

void CountlyPersistency::saveToFile()
{
    QSettings settings;
    settings.setValue(kCountlyQueuedRequestsPersistencyKey, m_queuedRequests);
    settings.sync();

    CLY_LOG_D("Requests queue successfully saved");
}

QString CountlyPersistency::retrieveDeviceID()
{
    QSettings settings;
    QString retrievedDeviceID = settings.value(kCountlyStoredDeviceIDKey).toString();

    if (!retrievedDeviceID.isEmpty()) {
        CLY_LOG_D("Device ID successfully retrieved from UserDefaults: %s", retrievedDeviceID.toUtf8().constData());
        return retrievedDeviceID;
    }

    CLY_LOG_D("There is no stored Device ID!");

    return {};
}

void CountlyPersistency::storeDeviceID(const QString &deviceID)
{
    QSettings settings;
    settings.setValue(kCountlyStoredDeviceIDKey, deviceID);
    settings.sync();

    CLY_LOG_D("Device ID successfully stored: %s", deviceID.toUtf8().constData());
}

void CountlyPersistency::storeIsCustomDeviceID(bool isCustomDeviceID)
{
    QSettings settings;
    settings.setValue(kCountlyIsCustomDeviceIDKey, isCustomDeviceID);
    settings.sync();
}

quint32 CountlyPersistency::eventSendThreshold() const
{
    return m_eventSendThreshold;
}

quint32 CountlyPersistency::storedRequestsLimit() const
{
    return m_storedRequestsLimit;
}

bool CountlyPersistency::isQueueBeingModified() const
{
    return m_isQueueBeingModified;
}
