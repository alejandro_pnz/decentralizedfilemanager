/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef COUNTLYCOMMON_H
#define COUNTLYCOMMON_H

#include <QObject>

#include "CountlyConfig.h"
#include "CountlyConnectionManager.h"
#include "CountlyConsentManager.h"
#include "CountlyDeviceInfo.h"
#include "CountlyEvent.h"
#include "CountlyPersistency.h"
#include "CountlyLocationManager.h"

#define CLY_LOG_E(fmt, ...) CountlyInternalLog(CLYInternalLogLevelError, fmt, ##__VA_ARGS__)
#define CLY_LOG_W(fmt, ...) CountlyInternalLog(CLYInternalLogLevelWarning, fmt, ##__VA_ARGS__)
#define CLY_LOG_I(fmt, ...) CountlyInternalLog(CLYInternalLogLevelInfo, fmt, ##__VA_ARGS__)
#define CLY_LOG_D(fmt, ...) CountlyInternalLog(CLYInternalLogLevelDebug, fmt, ##__VA_ARGS__)
#define CLY_LOG_V(fmt, ...) CountlyInternalLog(CLYInternalLogLevelVerbose, fmt, ##__VA_ARGS__)

void CountlyInternalLog(CLYInternalLogLevel level, const char *format, ...);

class CountlyCommon : public QObject
{
    Q_OBJECT

public:
    explicit CountlyCommon(QObject *parent = nullptr);

    static CountlyCommon *sharedInstance();

    QString SDKVersion() const;
    QString SDKName() const;

    bool hasStarted() const;
    bool enableDebug() const;
    void *loggerDelegate() const;
    CLYInternalLogLevel internalLogLevel() const;
    bool enableAppleWatch() const;
    QString attributionID() const;
    bool manualSessionHandling();

    quint8 hourOfDay() const;
    quint8 dayOfWeek() const;
    quint32 timeZone() const;
    quint32 timeSinceLaunch() const;
    quint64 uniqueTimestamp() const;

private:
    static QPointer<CountlyCommon> m_instance;
    static QMutex m_mutex;

    bool m_hasStarted{false};
    bool m_enableDebug{false};
    void *m_loggerDelegate{nullptr};
    CLYInternalLogLevel m_internalLogLevel{CLYInternalLogLevelNone};
    QString m_attributionID;
    bool m_manualSessionHandling{false};

    mutable quint64 m_lastTimestamp{0};

    friend class Countly;
};

#endif // COUNTLYCOMMON_H
