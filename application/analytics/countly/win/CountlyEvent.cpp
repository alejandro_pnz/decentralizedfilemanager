/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "CountlyEvent.h"

#include <QVariantMap>

const char* const kCountlyEventKeyKey           = "key";
const char* const kCountlyEventKeySegmentation  = "segmentation";
const char* const kCountlyEventKeyCount         = "count";
const char* const kCountlyEventKeySum           = "sum";
const char* const kCountlyEventKeyTimestamp     = "timestamp";
const char* const kCountlyEventKeyHourOfDay     = "hour";
const char* const kCountlyEventKeyDayOfWeek     = "dow";
const char* const kCountlyEventKeyDuration      = "dur";

CountlyEvent::CountlyEvent(QObject *parent)
    : QObject(parent)
{

}

CountlyEvent::~CountlyEvent()
{

}

QString CountlyEvent::key() const
{
    return m_key;
}

QVariantMap CountlyEvent::segmentation() const
{
    return m_segmentation;
}

quint32 CountlyEvent::count() const
{
    return m_count;
}

double CountlyEvent::sum() const
{
    return m_sum;
}

quint64 CountlyEvent::timestamp() const
{
    return m_timestamp;
}

quint8 CountlyEvent::hourOfDay() const
{
    return m_hourOfDay;
}

quint8 CountlyEvent::dayOfWeek() const
{
    return m_dayOfWeek;
}

quint64 CountlyEvent::duration() const
{
    return m_duration;
}

QVariantMap CountlyEvent::dictionaryRepresentation()
{
    QVariantMap eventData;
    eventData[kCountlyEventKeyKey] = m_key;
    if (!m_segmentation.isEmpty()) {
        eventData[kCountlyEventKeySegmentation] = m_segmentation;
    }
    eventData[kCountlyEventKeyCount] = QString::number(m_count);
    eventData[kCountlyEventKeySum] = QString::number(m_sum);
    eventData[kCountlyEventKeyTimestamp] = QString::number(m_timestamp * 1000);
    eventData[kCountlyEventKeyHourOfDay] = QString::number(m_hourOfDay);
    eventData[kCountlyEventKeyDayOfWeek] = QString::number(m_dayOfWeek);
    eventData[kCountlyEventKeyDuration] = QString::number(m_duration);

    return eventData;
}
