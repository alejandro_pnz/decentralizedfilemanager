/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "Countly.h"

#include "CountlyCommon.h"

#include <QTimer>

QPointer<Countly> Countly::m_instance{nullptr};
QMutex Countly::m_mutex;

Countly::Countly(QObject *parent) : QObject(parent)
{

}

Countly::~Countly()
{

}

Countly *Countly::sharedInstance()
{
    if (!m_instance) {
        // lock & double check
        QMutexLocker locker(&m_mutex);

        if (!m_instance) {
            m_instance = QPointer<Countly>(new Countly);
        }
    }

    return m_instance;
}

void Countly::startWithConfig(const QSharedPointer<CountlyConfig> &config)
{
    if (CountlyCommon::sharedInstance()->hasStarted()) {
        return;
    }

    if (!config) {
        return;
    }

    CountlyCommon::sharedInstance()->m_hasStarted = true;
    CountlyCommon::sharedInstance()->m_enableDebug = config->enableDebug;
    CountlyCommon::sharedInstance()->m_loggerDelegate = config->loggerDelegate;
    CountlyCommon::sharedInstance()->m_internalLogLevel = config->internalLogLevel;

    CountlyConsentManager::sharedInstance()->m_requiresConsent = config->requiresConsent;

    if (config->appKey.isEmpty() || config->appKey == "YOUR_APP_KEY") {
        CLY_LOG_E("appKey property on CountlyConfig object is not set");
        return;
    }

    if (config->host.isEmpty() || config->host == "https://YOUR_COUNTLY_SERVER") {
        CLY_LOG_E("host property on CountlyConfig object is not set");
        return;
    }

    CLY_LOG_I("Initializing with %s SDK v%s", CountlyCommon::sharedInstance()->SDKName().toUtf8().constData(), CountlyCommon::sharedInstance()->SDKVersion().toUtf8().constData());

    if (CountlyDeviceInfo::sharedInstance()->deviceID().isEmpty() || config->resetStoredDeviceID) {
        storeCustomDeviceIDState(config->deviceID);

        CountlyDeviceInfo::sharedInstance()->initializeDeviceID(config->deviceID);
    }

    CountlyConnectionManager::sharedInstance()->m_appKey = config->appKey;
    CountlyConnectionManager::sharedInstance()->m_host = config->host.endsWith("/") ? config->host.remove(config->host.lastIndexOf("/"), 1) : config->host;
    CountlyConnectionManager::sharedInstance()->m_alwaysUsePOST = config->alwaysUsePOST;
    //CountlyConnectionManager::sharedInstance()->pinnedCertificates = config->pinnedCertificates;
    CountlyConnectionManager::sharedInstance()->m_customHeaderFieldName = config->customHeaderFieldName;
    CountlyConnectionManager::sharedInstance()->m_customHeaderFieldValue = config->customHeaderFieldValue;
    CountlyConnectionManager::sharedInstance()->m_secretSalt = config->secretSalt;
    //CountlyConnectionManager::sharedInstance()->URLSessionConfiguration = config.URLSessionConfiguration;

    CountlyPersistency::sharedInstance()->m_eventSendThreshold = config->eventSendThreshold;
    CountlyPersistency::sharedInstance()->m_storedRequestsLimit = qMax((quint32)1, config->storedRequestsLimit);

    CountlyCommon::sharedInstance()->m_manualSessionHandling = config->manualSessionHandling;

    //CountlyCommon::sharedInstance()->enableAppleWatch = config.enableAppleWatch;

    CountlyCommon::sharedInstance()->m_attributionID = config->attributionID;

    CountlyDeviceInfo::sharedInstance()->m_customMetrics = config->customMetrics;

#if (TARGET_OS_IOS)
    CountlyFeedbacks.sharedInstance.message = config.starRatingMessage;
    CountlyFeedbacks.sharedInstance.sessionCount = config.starRatingSessionCount;
    CountlyFeedbacks.sharedInstance.disableAskingForEachAppVersion = config.starRatingDisableAskingForEachAppVersion;
    CountlyFeedbacks.sharedInstance.ratingCompletionForAutoAsk = config.starRatingCompletion;
    [CountlyFeedbacks.sharedInstance checkForStarRatingAutoAsk];

    [CountlyLocationManager.sharedInstance updateLocation:config.location city:config.city ISOCountryCode:config.ISOCountryCode IP:config.IP];
#endif

    if (!CountlyCommon::sharedInstance()->manualSessionHandling()) {
        CountlyConnectionManager::sharedInstance()->beginSession();
    }

    //NOTE: If there is no consent for sessions, location info and attribution should be sent separately, as they cannot be sent with begin_session request.
    if (!CountlyConsentManager::sharedInstance()->consentForSessions()) {
        CountlyLocationManager::sharedInstance()->sendLocationInfo();
        CountlyConnectionManager::sharedInstance()->sendAttribution();
    }

//#if (TARGET_OS_IOS || TARGET_OS_OSX)
//#ifndef COUNTLY_EXCLUDE_PUSHNOTIFICATIONS
//    if ([config.features containsObject:CLYPushNotifications])
//    {
//        CountlyPushNotifications.sharedInstance.isEnabledOnInitialConfig = YES;
//        CountlyPushNotifications.sharedInstance.pushTestMode = config.pushTestMode;
//        CountlyPushNotifications.sharedInstance.sendPushTokenAlways = config.sendPushTokenAlways;
//        CountlyPushNotifications.sharedInstance.doNotShowAlertForNotifications = config.doNotShowAlertForNotifications;
//        CountlyPushNotifications.sharedInstance.launchNotification = config.launchNotification;
//        [CountlyPushNotifications.sharedInstance startPushNotifications];
//    }
//#endif
//#endif

    //CountlyCrashReporter.sharedInstance.crashSegmentation = config.crashSegmentation;
    //CountlyCrashReporter.sharedInstance.crashLogLimit = MAX(1, config.crashLogLimit);
    //CountlyCrashReporter.sharedInstance.crashFilter = config.crashFilter;
    //CountlyCrashReporter.sharedInstance.shouldUsePLCrashReporter = config.shouldUsePLCrashReporter;
    //CountlyCrashReporter.sharedInstance.shouldUseMachSignalHandler = config.shouldUseMachSignalHandler;
    //CountlyCrashReporter.sharedInstance.crashOccuredOnPreviousSessionCallback = config.crashOccuredOnPreviousSessionCallback;
    //CountlyCrashReporter.sharedInstance.shouldSendCrashReportCallback = config.shouldSendCrashReportCallback;
    //if ([config.features containsObject:CLYCrashReporting])
    //{
    //    CountlyCrashReporter.sharedInstance.isEnabledOnInitialConfig = YES;
    //    [CountlyCrashReporter.sharedInstance startCrashReporting];
    //}

//#if (TARGET_OS_IOS || TARGET_OS_TV)
//    if ([config.features containsObject:CLYAutoViewTracking])
//    {
//        CountlyViewTracking.sharedInstance.isEnabledOnInitialConfig = YES;
//        [CountlyViewTracking.sharedInstance startAutoViewTracking];
//    }
//#endif

    QTimer *timer = new QTimer(this);
    timer->setInterval(config->updateSessionPeriod * 1000);

    connect(timer, &QTimer::timeout, this, &Countly::onTimer);

    timer->start();

//    CountlyCommon::sharedInstance()->startAppleWatchMatching();

//    CountlyRemoteConfig.sharedInstance.isEnabledOnInitialConfig = config.enableRemoteConfig;
//    CountlyRemoteConfig.sharedInstance.remoteConfigCompletionHandler = config.remoteConfigCompletionHandler;
//    [CountlyRemoteConfig.sharedInstance startRemoteConfig];

//    CountlyPerformanceMonitoring.sharedInstance.isEnabledOnInitialConfig = config.enablePerformanceMonitoring;
//    [CountlyPerformanceMonitoring.sharedInstance startPerformanceMonitoring];

//    [CountlyCommon.sharedInstance observeDeviceOrientationChanges];

    CountlyConnectionManager::sharedInstance()->proceedOnQueue();

    if (!config->consents.isEmpty()) {
        giveConsentForFeatures(config->consents);
    }
}

void Countly::giveConsentForFeatures(const QList<CLYConsent> &features)
{
    CLY_LOG_I("%s", __FUNCTION__);

    CountlyConsentManager::sharedInstance()->giveConsentForFeatures(features);
}

void Countly::storeCustomDeviceIDState(const QString &deviceID)
{
    bool isCustomDeviceID = !deviceID.isEmpty() && (deviceID != CLYTemporaryDeviceID);
    CountlyPersistency::sharedInstance()->storeIsCustomDeviceID(isCustomDeviceID);
}

void Countly::onTimer()
{
    if (m_isSuspended) {
        return;
    }

    if (!CountlyCommon::sharedInstance()->manualSessionHandling()) {
        CountlyConnectionManager::sharedInstance()->updateSession();
    }

    CountlyConnectionManager::sharedInstance()->sendEvents();
}
