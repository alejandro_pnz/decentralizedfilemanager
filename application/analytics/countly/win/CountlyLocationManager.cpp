/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "CountlyLocationManager.h"

#include "CountlyCommon.h"

#include <QPointer>

QPointer<CountlyLocationManager> CountlyLocationManager::m_instance{nullptr};
QMutex CountlyLocationManager::m_mutex;

CountlyLocationManager::CountlyLocationManager(QObject *parent)
    : QObject(parent)
{

}

CountlyLocationManager *CountlyLocationManager::sharedInstance()
{
    if (!m_instance) {
        // lock & double check
        QMutexLocker locker(&m_mutex);

        if (!m_instance) {
            m_instance = QPointer<CountlyLocationManager>(new CountlyLocationManager);
        }
    }

    return m_instance;
}

QString CountlyLocationManager::location() const
{
    return m_location;
}

QString CountlyLocationManager::city() const
{
    return m_city;
}

QString CountlyLocationManager::ISOCountryCode() const
{
    return m_ISOCountryCode;
}

QString CountlyLocationManager::IP() const
{
    return m_IP;
}

bool CountlyLocationManager::isLocationInfoDisabled() const
{
    return m_isLocationInfoDisabled;
}

void CountlyLocationManager::sendLocationInfo()
{
    if (!CountlyConsentManager::sharedInstance()->consentForLocation()) {
        return;
    }

    CountlyConnectionManager::sharedInstance()->sendLocationInfo();
}

void CountlyLocationManager::disableLocationInfo()
{
    if (!CountlyConsentManager::sharedInstance()->consentForLocation()) {
        return;
    }

    m_isLocationInfoDisabled = true;

    m_location.clear();
    m_city.clear();
    m_ISOCountryCode.clear();
    m_IP.clear();

    CountlyConnectionManager::sharedInstance()->sendLocationInfo();
}
