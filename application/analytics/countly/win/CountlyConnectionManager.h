/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef COUNTLYCONNECTIONMANAGER_H
#define COUNTLYCONNECTIONMANAGER_H

#include <QObject>
#include <QPointer>
#include <QUrlQuery>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QMutex>

class CountlyConnectionManager : public QObject
{
    Q_OBJECT

public:
    explicit CountlyConnectionManager(QObject *parent = nullptr);

    ~CountlyConnectionManager();

    static CountlyConnectionManager *sharedInstance();

    QPointer<QNetworkReply> connection() const;

    void beginSession();
    void updateSession();
    void endSession();

    void sendEvents();
    void sendPushToken(const QString &token);
    void sendLocationInfo();
    void sendUserDetails(const QString &userDetails);
    void sendCrashReport(const QString &report, bool immediately);
    void sendOldDeviceID(const QString &oldDeviceID);
    void sendParentDeviceID(const QString &parentDeviceID);
    void sendAttribution();
    void sendConsentChanges(const QString &consentChanges);
    void sendPerformanceMonitoringTrace(const QString &trace);

    void proceedOnQueue();

    QUrlQuery queryEssentials();
    QString appendChecksum(const QString &queryString);

protected:
    QString locationRelatedInfoQueryString() const;

    QString attributionQueryString() const;

    int sessionLengthInSeconds();

    bool isRequestSuccessful(QNetworkReply *reply) const;

private:
    static QPointer<CountlyConnectionManager> m_instance;
    static QMutex m_mutex;

    QString m_appKey;
    QString m_host;
    QPointer<QNetworkReply> m_connection;
    QList<QString> m_pinnedCertificates;
    QString m_customHeaderFieldName;
    QString m_customHeaderFieldValue;
    QString m_secretSalt;
    bool m_alwaysUsePOST{false};
    qint64 m_lastSessionStartTime{0};
    qint64 m_unsentSessionLength{0};
    bool m_isCrashing{false};
    bool m_isTerminating{false};

    QNetworkAccessManager m_manager;

    friend class Countly;
};

#endif // COUNTLYCONNECTIONMANAGER_H
