/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "CountlyConnectionManager.h"

#include <QDateTime>

#include <QJsonDocument>
#include <QJsonObject>

#include <QCoreApplication>

#include "CountlyCommon.h"

static const char* kCountlyQSKeyAppKey           = "app_key";

static const char* kCountlyQSKeyDeviceID         = "device_id";
static const char* kCountlyQSKeyDeviceIDOld      = "old_device_id";
static const char* kCountlyQSKeyDeviceIDParent   = "parent_device_id";

static const char* kCountlyQSKeyTimestamp        = "timestamp";
static const char* kCountlyQSKeyTimeZone         = "tz";
static const char* kCountlyQSKeyTimeHourOfDay    = "hour";
static const char* kCountlyQSKeyTimeDayOfWeek    = "dow";

static const char* kCountlyQSKeySDKVersion       = "sdk_version";
static const char* kCountlyQSKeySDKName          = "sdk_name";

static const char* kCountlyQSKeySessionBegin     = "begin_session";
static const char* kCountlyQSKeySessionDuration  = "session_duration";
static const char* kCountlyQSKeySessionEnd       = "end_session";

static const char* kCountlyQSKeyPushTokenSession = "token_session";
static const char* kCountlyQSKeyPushTokeniOS     = "ios_token";
static const char* kCountlyQSKeyPushTestMode     = "test_mode";

static const char* kCountlyQSKeyLocation         = "location";
static const char* kCountlyQSKeyLocationCity     = "city";
static const char* kCountlyQSKeyLocationCountry  = "country_code";
static const char* kCountlyQSKeyLocationIP       = "ip_address";

static const char* kCountlyQSKeyMetrics          = "metrics";
static const char* kCountlyQSKeyEvents           = "events";
static const char* kCountlyQSKeyUserDetails      = "user_details";
static const char* kCountlyQSKeyCrash            = "crash";
static const char* kCountlyQSKeyChecksum256      = "checksum256";
static const char* kCountlyQSKeyAttributionID    = "aid";
static const char* kCountlyQSKeyIDFA             = "idfa";
static const char* kCountlyQSKeyConsent          = "consent";
static const char* kCountlyQSKeyAPM              = "apm";

static const char* kCountlyQSKeyMethod           = "method";

static const char* kCountlyUploadBoundary = "0cae04a8b698d63ff6ea55d168993f21";

static const char* kCountlyEndpointI = "/i"; //NOTE: input endpoint
static const char* kCountlyEndpointO = "/o"; //NOTE: output endpoint
static const char* kCountlyEndpointSDK = "/sdk";
static const char* kCountlyEndpointFeedback = "/feedback";
static const char* kCountlyEndpointWidget = "/widget";

static const int kCountlyGETRequestMaxLength = 2048;

QPointer<CountlyConnectionManager> CountlyConnectionManager::m_instance{nullptr};
QMutex CountlyConnectionManager::m_mutex;

CountlyConnectionManager::CountlyConnectionManager(QObject *parent)
    : QObject(parent)
{
    connect(&m_manager, &QNetworkAccessManager::finished, this, [=](QNetworkReply *reply) {

        if (m_connection == reply) {
            m_connection = nullptr;
        }

        CLY_LOG_V("Approximate received data size for request <%p> is %ld bytes.", reply, reply->size());

        if (reply->error() == QNetworkReply::NoError) {
            if (isRequestSuccessful(reply)) {
                CLY_LOG_D("Request <%p> successfully completed.", reply);

                QString queryString = reply->request().attribute(QNetworkRequest::User).toString();

                CountlyPersistency::sharedInstance()->removeFromQueue(queryString);

                CountlyPersistency::sharedInstance()->saveToFile();

                proceedOnQueue();
            }
            else {
                CLY_LOG_D("Request <%p> failed!\nServer reply: %s", reply, reply->readAll().constData());
            }
        }
        else {
            CLY_LOG_D("Request <%p> failed!\nError: %s", reply, reply->errorString().constData());
#if (TARGET_OS_WATCH)
            CountlyPersistency::sharedInstance()->saveToFile();
#endif
        }
        reply->deleteLater();
    });

    connect(qApp, &QCoreApplication::aboutToQuit, this, [=](){
        m_isTerminating = true;
    });
}

CountlyConnectionManager::~CountlyConnectionManager()
{

}

CountlyConnectionManager *CountlyConnectionManager::sharedInstance()
{
    if(!m_instance) {
        // lock & double check
        QMutexLocker locker(&m_mutex);

        if(!m_instance) {
            m_instance = QPointer<CountlyConnectionManager>(new CountlyConnectionManager);
        }
    }

    return m_instance;
}

QPointer<QNetworkReply> CountlyConnectionManager::connection() const
{
    return m_connection;
}

void CountlyConnectionManager::beginSession()
{
    if (!CountlyConsentManager::sharedInstance()->consentForSessions()) {
        return;
    }

    m_lastSessionStartTime = QDateTime::currentDateTime().toSecsSinceEpoch();
    m_unsentSessionLength = 0.0;

    QUrlQuery urlQuery = queryEssentials();

    urlQuery.addQueryItem(kCountlyQSKeySessionBegin, "1");
    urlQuery.addQueryItem(kCountlyQSKeyMetrics, CountlyDeviceInfo::metrics());

    QString query = urlQuery.toString(QUrl::FullyEncoded);

    QString locationRelatedInfoQuery = locationRelatedInfoQueryString();
    if (!locationRelatedInfoQuery.isEmpty()) {
        query.append("&").append(locationRelatedInfoQuery);
    }

    QString attributionQuery = attributionQueryString();
    if (!attributionQuery.isEmpty()) {
        query.append("&").append(attributionQuery);
    }

    CountlyPersistency::sharedInstance()->addToQueue(query);

    proceedOnQueue();
}

void CountlyConnectionManager::updateSession()
{
    if (!CountlyConsentManager::sharedInstance()->consentForSessions()) {
        return;
    }

    QUrlQuery query = queryEssentials();

    query.addQueryItem(kCountlyQSKeySessionDuration, QString::number(sessionLengthInSeconds()));

    CountlyPersistency::sharedInstance()->addToQueue(query.toString(QUrl::FullyEncoded));

    proceedOnQueue();
}

void CountlyConnectionManager::endSession()
{
    if (!CountlyConsentManager::sharedInstance()->consentForSessions()) {
        return;
    }

    QUrlQuery query = queryEssentials();

    query.addQueryItem(kCountlyQSKeySessionEnd, "1");
    query.addQueryItem(kCountlyQSKeySessionDuration, QString::number(sessionLengthInSeconds()));

    CountlyPersistency::sharedInstance()->addToQueue(query.toString(QUrl::FullyEncoded));

    proceedOnQueue();
}

void CountlyConnectionManager::sendEvents()
{
    QString events = CountlyPersistency::sharedInstance()->serializedRecordedEvents();

    if (events.isEmpty()) {
        return;
    }

    QUrlQuery query = queryEssentials();

    query.addQueryItem(kCountlyQSKeySessionDuration, QString::number(sessionLengthInSeconds()));
    query.addQueryItem(kCountlyQSKeyEvents, events);

    CountlyPersistency::sharedInstance()->addToQueue(query.toString(QUrl::FullyEncoded));

    proceedOnQueue();
}

void CountlyConnectionManager::sendLocationInfo()
{
    QString locationInfoQueryString = locationRelatedInfoQueryString();

    if (locationInfoQueryString.isEmpty()) {
        return;
    }

    QUrlQuery urlQuery = queryEssentials();

    QString query = urlQuery.toString(QUrl::FullyEncoded);

    query.append("&").append(locationInfoQueryString);

    CountlyPersistency::sharedInstance()->addToQueue(query);

    proceedOnQueue();
}

void CountlyConnectionManager::sendAttribution()
{
    QString attributionString = attributionQueryString();
    if (attributionString.isEmpty()) {
        return;
    }

    QUrlQuery urlQuery = queryEssentials();

    QString query = urlQuery.toString(QUrl::FullyEncoded);

    query.append("&").append(attributionString);

    CountlyPersistency::sharedInstance()->addToQueue(query);

    proceedOnQueue();
}

void CountlyConnectionManager::sendConsentChanges(const QString &consentChanges)
{
    QUrlQuery urlQuery = queryEssentials();

    urlQuery.addQueryItem(kCountlyQSKeyConsent, consentChanges);

    CountlyPersistency::sharedInstance()->addToQueue(urlQuery.toString(QUrl::FullyEncoded));

    proceedOnQueue();
}

void CountlyConnectionManager::proceedOnQueue()
{
    CLY_LOG_D("Proceeding on queue...");

    if (m_connection) {
        CLY_LOG_D("Proceeding on queue is aborted: Already has a request in process!");
        return;
    }

    if (m_isCrashing) {
        CLY_LOG_D("Proceeding on queue is aborted: Application is crashing!");
        return;
    }

    if (m_isTerminating) {
        CLY_LOG_D("Proceeding on queue is aborted: Application is terminating!");
        return;
    }

    if (!m_customHeaderFieldName.isEmpty() && m_customHeaderFieldValue.isEmpty()) {
        CLY_LOG_D("Proceeding on queue is aborted: customHeaderFieldName specified on config, but customHeaderFieldValue not set yet!");
        return;
    }

    if (CountlyPersistency::sharedInstance()->isQueueBeingModified()) {
        CLY_LOG_D("Proceeding on queue is aborted: Queue is being modified!");
        return;
    }

    const QString firstItemInQueue = CountlyPersistency::sharedInstance()->firstItemInQueue();
    if (firstItemInQueue.isEmpty()) {
        CLY_LOG_D("Queue is empty. All requests are processed.");
        return;
    }

    QString queryString = firstItemInQueue;

    queryString = appendChecksum(queryString);

    QString serverInputEndpoint = m_host + kCountlyEndpointI;

    QUrl fullRequestURL(serverInputEndpoint);
    fullRequestURL.setQuery(queryString);

    QNetworkRequest request(fullRequestURL);

    QByteArray verb{"GET"};

    QByteArray data;

    if (queryString.length() > kCountlyGETRequestMaxLength || m_alwaysUsePOST)  {
        request.setUrl(serverInputEndpoint);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        verb = "POST";
        data = queryString.toUtf8();
    }

    if (!m_customHeaderFieldName.isEmpty() && !m_customHeaderFieldValue.isEmpty()) {
        request.setRawHeader(m_customHeaderFieldName.toUtf8(), m_customHeaderFieldValue.toUtf8());
    }

    request.setAttribute(QNetworkRequest::User, firstItemInQueue);

    m_connection = m_manager.sendCustomRequest(request, verb, data);
}

QUrlQuery CountlyConnectionManager::queryEssentials()
{
    QUrlQuery query;

    query.addQueryItem(kCountlyQSKeyAppKey, m_appKey);
    query.addQueryItem(kCountlyQSKeyDeviceID, CountlyDeviceInfo::sharedInstance()->deviceID());
    query.addQueryItem(kCountlyQSKeyTimestamp, QString::number(CountlyCommon::sharedInstance()->uniqueTimestamp() * 1000));
    query.addQueryItem(kCountlyQSKeyTimeHourOfDay, QString::number(CountlyCommon::sharedInstance()->hourOfDay()));
    query.addQueryItem(kCountlyQSKeyTimeDayOfWeek, QString::number(CountlyCommon::sharedInstance()->dayOfWeek()));
    query.addQueryItem(kCountlyQSKeyTimeZone, QString::number(CountlyCommon::sharedInstance()->timeZone()));
    query.addQueryItem(kCountlyQSKeySDKVersion, CountlyCommon::sharedInstance()->SDKVersion());
    query.addQueryItem(kCountlyQSKeySDKName, CountlyCommon::sharedInstance()->SDKName());

    return query;
}

QString CountlyConnectionManager::appendChecksum(const QString &queryString)
{
    if (!m_secretSalt.isEmpty()) {
        QCryptographicHash hash(QCryptographicHash::Sha256);
        hash.addData(queryString.toUtf8() + m_secretSalt.toUtf8());
        return QString(queryString) + QString("&%1=%2").arg(kCountlyQSKeyChecksum256, hash.result().toHex().constData());
    }
    return queryString;
}

QString CountlyConnectionManager::locationRelatedInfoQueryString() const
{
    QUrlQuery query;

    if (!CountlyConsentManager::sharedInstance()->consentForLocation() || CountlyLocationManager::sharedInstance()->isLocationInfoDisabled()) {
        //NOTE: Return empty string for location. This is a server requirement to disable IP based location inferring.
        query.addQueryItem(kCountlyQSKeyLocation, "");
        return query.toString(QUrl::FullyEncoded);
    }

    QString location = CountlyLocationManager::sharedInstance()->location();
    QString city = CountlyLocationManager::sharedInstance()->city();
    QString ISOCountryCode = CountlyLocationManager::sharedInstance()->ISOCountryCode();
    QString IP = CountlyLocationManager::sharedInstance()->IP();

    if (!location.isEmpty()) {
        query.addQueryItem(kCountlyQSKeyLocation, location);
    }

    if (!city.isEmpty()) {
        query.addQueryItem(kCountlyQSKeyLocationCity, city);
    }

    if (!ISOCountryCode.isEmpty()) {
        query.addQueryItem(kCountlyQSKeyLocationCountry, ISOCountryCode);
    }

    if (!IP.isEmpty()) {
        query.addQueryItem(kCountlyQSKeyLocationIP, IP);
    }

    return query.toString(QUrl::FullyEncoded);
}

QString CountlyConnectionManager::attributionQueryString() const
{
    if (!CountlyConsentManager::sharedInstance()->consentForAttribution()) {
        return {};
    }

    if (CountlyCommon::sharedInstance()->attributionID().isEmpty()) {
        return {};
    }

    QJsonObject attribution
    {
        {kCountlyQSKeyIDFA, CountlyCommon::sharedInstance()->attributionID()}
    };

    QUrlQuery query;

    query.addQueryItem(kCountlyQSKeyAttributionID, QJsonDocument(attribution).toJson(QJsonDocument::Compact));

    return query.toString(QUrl::FullyEncoded);
}

int CountlyConnectionManager::sessionLengthInSeconds()
{
    qint64 currentTime = QDateTime::currentDateTime().toSecsSinceEpoch();
    m_unsentSessionLength += (currentTime - m_lastSessionStartTime);
    m_lastSessionStartTime = currentTime;
    int sessionLengthInSeconds = m_unsentSessionLength;
    m_unsentSessionLength -= sessionLengthInSeconds;
    return sessionLengthInSeconds;
}

bool CountlyConnectionManager::isRequestSuccessful(QNetworkReply *reply) const
{
    if (!reply) {
        return false;
    }

    int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    if (code >= 200 && code < 300) {
        QJsonParseError error;
        QJsonObject serverReply = QJsonDocument::fromJson(reply->readAll(), &error).object();

        if (serverReply.isEmpty()) {
            CLY_LOG_W("Server reply is not a valid JSON!");
            return false;
        }

        if (serverReply.value("result") == "Success") {
            CLY_LOG_V("Value for `result` key in server reply is `Success`.");
            return true;
        }
        else {
            CLY_LOG_V("Value for `result` key in server reply is not `Success`.");
            return false;
        }
    }

    CLY_LOG_V("HTTP status code is not 2XX series.");
    return false;
}
