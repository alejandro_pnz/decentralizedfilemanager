/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef COUNTLYEVENT_H
#define COUNTLYEVENT_H

#include <QObject>
#include <QVariantMap>

class CountlyEvent : public QObject
{
    Q_OBJECT
public:
    explicit CountlyEvent(QObject *parent = nullptr);

    ~CountlyEvent();

    QString key() const;
    QVariantMap segmentation() const;
    quint32 count() const;
    double sum() const;
    quint64 timestamp() const;
    quint8 hourOfDay() const;
    quint8 dayOfWeek() const;
    quint64 duration() const;

    QVariantMap dictionaryRepresentation();

private:
    QString m_key;
    QVariantMap m_segmentation;
    quint32 m_count{0};
    double m_sum{0};
    quint64 m_timestamp{0};
    quint8 m_hourOfDay{0};
    quint8 m_dayOfWeek{0};
    quint64 m_duration{0};
};

#endif // COUNTLYEVENT_H
