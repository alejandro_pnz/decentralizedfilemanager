/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "CountlyConsentManager.h"

#include "CountlyCommon.h"

#include <QJsonDocument>
#include <QJsonObject>

CLYConsent const CLYConsentSessions                 = "sessions";
CLYConsent const CLYConsentEvents                   = "events";
CLYConsent const CLYConsentUserDetails              = "users";
CLYConsent const CLYConsentCrashReporting           = "crashes";
CLYConsent const CLYConsentPushNotifications        = "push";
CLYConsent const CLYConsentLocation                 = "location";
CLYConsent const CLYConsentViewTracking             = "views";
CLYConsent const CLYConsentAttribution              = "attribution";
CLYConsent const CLYConsentStarRating               = "star-rating";
CLYConsent const CLYConsentAppleWatch               = "accessory-devices";
CLYConsent const CLYConsentPerformanceMonitoring    = "apm";
CLYConsent const CLYConsentFeedback                 = "feedback";
CLYConsent const CLYConsentRemoteConfig             = "remote-config";

QPointer<CountlyConsentManager> CountlyConsentManager::m_instance{nullptr};
QMutex CountlyConsentManager::m_mutex;

CountlyConsentManager::CountlyConsentManager(QObject *parent)
    : QObject(parent)
{

}

CountlyConsentManager *CountlyConsentManager::sharedInstance()
{
    if (!m_instance) {
        // lock & double check
        QMutexLocker locker(&m_mutex);

        if (!m_instance) {
            m_instance = QPointer<CountlyConsentManager>(new CountlyConsentManager);
        }
    }

    return m_instance;
}

bool CountlyConsentManager::consentForSessions() const
{
    if (!m_requiresConsent) {
        return true;
    }

    return m_consentForSessions;
}

void CountlyConsentManager::setConsentForSessions(bool consent)
{
    m_consentForSessions = consent;

    if (m_consentForSessions) {
        CLY_LOG_D("Consent for Session is given.");

        if (!CountlyCommon::sharedInstance()->manualSessionHandling()) {
            CountlyConnectionManager::sharedInstance()->beginSession();
        }
    }
    else {
        CLY_LOG_D("Consent for Session is cancelled.");
    }

    m_consentChanges[CLYConsentSessions] = QVariant(m_consentForSessions).toString();
}

bool CountlyConsentManager::consentForEvents() const
{
    if (!m_requiresConsent) {
        return true;
    }

    return m_consentForEvents;
}

void CountlyConsentManager::setConsentForEvents(bool consent)
{
    m_consentForEvents = consent;

    if (m_consentForEvents) {
        CLY_LOG_D("Consent for Events is given.");
    }
    else {
        CLY_LOG_D("Consent for Events is cancelled.");

        CountlyConnectionManager::sharedInstance()->sendEvents();
        CountlyPersistency::sharedInstance()->clearAllTimedEvents();
    }

    m_consentChanges[CLYConsentEvents] = QVariant(m_consentForEvents).toString();
}

bool CountlyConsentManager::consentForUserDetails() const
{
    if (!m_requiresConsent) {
        return true;
    }

    return m_consentForUserDetails;
}

bool CountlyConsentManager::consentForCrashReporting() const
{
    if (!m_requiresConsent) {
        return true;
    }

    return m_consentForCrashReporting;
}

bool CountlyConsentManager::consentForPushNotifications() const
{
    if (!m_requiresConsent) {
        return true;
    }

    return m_consentForPushNotifications;
}

bool CountlyConsentManager::consentForLocation() const
{
    if (!m_requiresConsent) {
        return true;
    }

    return m_consentForLocation;
}

void CountlyConsentManager::setConsentForLocation(bool consent)
{
    m_consentForLocation = consent;

    if (m_consentForLocation) {
        CLY_LOG_D("Consent for Location is given.");

        CountlyLocationManager::sharedInstance()->sendLocationInfo();
    }
    else {
        CLY_LOG_D("Consent for Location is cancelled.");
    }

    m_consentChanges[CLYConsentLocation] = QVariant(m_consentForLocation).toString();
}

bool CountlyConsentManager::consentForViewTracking() const
{
    if (!m_requiresConsent) {
        return true;
    }

    return m_consentForViewTracking;
}

bool CountlyConsentManager::consentForAttribution() const
{
    if (!m_requiresConsent) {
        return true;
    }

    return m_consentForAttribution;
}

bool CountlyConsentManager::consentForAppleWatch() const
{
    if (!m_requiresConsent) {
        return true;
    }

    return m_consentForAppleWatch;
}

bool CountlyConsentManager::consentForPerformanceMonitoring() const
{
    if (!m_requiresConsent) {
        return true;
    }

    return m_consentForPerformanceMonitoring;
}

bool CountlyConsentManager::consentForFeedback() const
{
    if (!m_requiresConsent) {
        return true;
    }

    return m_consentForFeedback;
}

bool CountlyConsentManager::consentForRemoteConfig() const
{
    if (!m_requiresConsent) {
        return true;
    }

    return m_consentForRemoteConfig;
}

void CountlyConsentManager::giveConsentForFeatures(const QList<CLYConsent> &features)
{
    if (!m_requiresConsent) {
        return;
    }

    if (features.isEmpty()) {
        return;
    }

    //NOTE: Due to some legacy Countly Server location info problems, giving consent for location should be the first.
    //NOTE: Otherwise, if location consent is given after sessions consent, begin_session request will be sent with an empty string as location.
    if (features.contains(CLYConsentLocation) && !m_consentForLocation) {
        setConsentForLocation(true);
    }

    if (features.contains(CLYConsentSessions) && !m_consentForSessions) {
        setConsentForSessions(true);
    }

    if (features.contains(CLYConsentEvents) && !m_consentForEvents) {
        setConsentForEvents(true);
    }

    if (features.contains(CLYConsentUserDetails) && !m_consentForUserDetails) {
        m_consentForUserDetails = true;
    }

    if (features.contains(CLYConsentCrashReporting) && !m_consentForCrashReporting) {
        m_consentForCrashReporting = true;
    }

    if (features.contains(CLYConsentPushNotifications) && !m_consentForPushNotifications) {
        m_consentForPushNotifications = true;
    }

    if (features.contains(CLYConsentViewTracking) && !m_consentForViewTracking) {
        m_consentForViewTracking = true;
    }

    if (features.contains(CLYConsentAttribution) && !m_consentForAttribution) {
        m_consentForAttribution = true;
    }

    if (features.contains(CLYConsentAppleWatch) && !m_consentForAppleWatch) {
        m_consentForAppleWatch = true;
    }

    if (features.contains(CLYConsentPerformanceMonitoring) && !m_consentForPerformanceMonitoring) {
        m_consentForPerformanceMonitoring = true;
    }

    if (features.contains(CLYConsentFeedback) && !m_consentForFeedback) {
        m_consentForFeedback = true;
    }

    if (features.contains(CLYConsentRemoteConfig) && !m_consentForRemoteConfig) {
        m_consentForRemoteConfig = true;
    }

    sendConsentChanges();
}

void CountlyConsentManager::sendConsentChanges()
{
    if (!m_consentChanges.isEmpty()) {
        CountlyConnectionManager::sharedInstance()->sendConsentChanges(QJsonDocument(QJsonObject::fromVariantMap(m_consentChanges)).toJson(QJsonDocument::Compact));
        m_consentChanges.clear();
    }
}
