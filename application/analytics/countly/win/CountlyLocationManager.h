/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef COUNTLYLOCATIONMANAGER_H
#define COUNTLYLOCATIONMANAGER_H

#include <QObject>
#include <QMutex>

class CountlyLocationManager : public QObject
{
    Q_OBJECT

public:
    explicit CountlyLocationManager(QObject *parent = nullptr);

    static CountlyLocationManager *sharedInstance();

    QString location() const;
    QString city() const;
    QString ISOCountryCode() const;
    QString IP() const;
    bool isLocationInfoDisabled() const;

    void sendLocationInfo();
    //void updateLocation:(CLLocationCoordinate2D)location city:(NSString *)city ISOCountryCode:(NSString *)ISOCountryCode IP:(NSString *)IP;
    //- (void)recordLocation:(CLLocationCoordinate2D)location city:(NSString *)city ISOCountryCode:(NSString *)ISOCountryCode IP:(NSString *)IP;
    void disableLocationInfo();

private:
    static QPointer<CountlyLocationManager> m_instance;
    static QMutex m_mutex;

    QString m_location;
    QString m_city;
    QString m_ISOCountryCode;
    QString m_IP;
    bool m_isLocationInfoDisabled{false};
};

#endif // COUNTLYLOCATIONMANAGER_H
