/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef COUNTLYPERSISTENCY_H
#define COUNTLYPERSISTENCY_H

#include <QObject>
#include <QVariantMap>
#include <QMutex>

#include "CountlyEvent.h"

class CountlyPersistency : public QObject
{
    Q_OBJECT

public:
    explicit CountlyPersistency(QObject *parent = nullptr);

    ~CountlyPersistency();

    static CountlyPersistency *sharedInstance();

    void addToQueue(const QString &queryString);
    void removeFromQueue(const QString &queryString);
    QString firstItemInQueue();
    void flushQueue();
    void replaceAllTemporaryDeviceIDsInQueueWithDeviceID(const QString &deviceID);
    void replaceAllAppKeysInQueueWithCurrentAppKey();
    void removeDifferentAppKeysFromQueue();

    void recordEvent(const QSharedPointer<CountlyEvent> &event);
    QString serializedRecordedEvents();
    void flushEvents();

    void recordTimedEvent(CountlyEvent *event);
    CountlyEvent *timedEventForKey(const QString &key);
    void clearAllTimedEvents();

    void writeCustomCrashLogToFile(const QString &log);
    QString customCrashLogsFromFile();
    void deleteCustomCrashLogFile();

    void saveToFile();
    void saveToFileSync();

    QString retrieveDeviceID();
    void storeDeviceID(const QString &deviceID);

    QString retrieveNSUUID();
    void storeNSUUID(const QString &UUID);

    QString retrieveWatchParentDeviceID();
    void storeWatchParentDeviceID(const QString &deviceID);

    QVariantMap retrieveStarRatingStatus();
    void storeStarRatingStatus(QVariantMap status);

    bool retrieveNotificationPermission();
    void storeNotificationPermission(bool allowed);

    bool retrieveIsCustomDeviceID();
    void storeIsCustomDeviceID(bool isCustomDeviceID);

    QVariantMap retrieveRemoteConfig();
    void storeRemoteConfig(QVariantMap remoteConfig);

    quint32 eventSendThreshold() const;
    quint32 storedRequestsLimit() const;
    bool isQueueBeingModified() const;

private:
    static QPointer<CountlyPersistency> m_instance;
    static QMutex m_mutex;

    QMutex m_ioMutex;

    int m_eventSendThreshold{0};
    int m_storedRequestsLimit{0};

    QStringList m_queuedRequests;
    QList<QSharedPointer<CountlyEvent> > m_recordedEvents;
    QVariantMap m_startedEvents;
    bool m_isQueueBeingModified{false};

    friend class Countly;
};

#endif // COUNTLYPERSISTENCY_H
