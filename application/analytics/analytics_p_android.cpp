/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/

#include "analytics_p.h"

#include <QJniObject>
#include <QCoreApplication>

AnalyticsPrivate::AnalyticsPrivate()
{

}

AnalyticsPrivate::~AnalyticsPrivate()
{

}

void AnalyticsPrivate::init()
{
    wrapper.reset(new QJniObject("com.test/CountlyHelper", "()V"));

    QJniObject jHost = QJniObject::fromString(host);
    QJniObject jAppKey = QJniObject::fromString(appKey);
    QJniObject jDeviceId = QJniObject::fromString(deviceId);

    wrapper->setField<jstring>("host", jHost.object<jstring>());
    wrapper->setField<jstring>("appKey", jAppKey.object<jstring>());
    wrapper->setField<jstring>("deviceId", jDeviceId.object<jstring>());

    wrapper->callMethod<void>("init", "(Landroid/content/Context;)V", QNativeInterface::QAndroidApplication::context());
}

void AnalyticsPrivate::sendEvent(const QString &name)
{

}

void AnalyticsPrivate::sendScreen(const QString &name)
{

}
