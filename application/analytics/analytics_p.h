/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef ANALYTICS_P_H
#define ANALYTICS_P_H

#include <QObject>

#if defined(Q_OS_ANDROID)
#include <QSharedPointer>
class QJniObject;
#endif

class AnalyticsPrivate : public QObject
{
public:
    AnalyticsPrivate();

    ~AnalyticsPrivate();

    void init();

    void sendEvent(const QString &name);

    void sendScreen(const QString &name);

    QString host;

    QString deviceId;

    QString appKey;

#if defined(Q_OS_ANDROID)
    QSharedPointer<QJniObject> wrapper;
#endif
};

#endif // ANALYTICS_P_H
