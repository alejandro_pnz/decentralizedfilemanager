/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef ANALYTICS_H
#define ANALYTICS_H

#include <QObject>
#include <QSharedPointer>

#include "singleton.h"

class AnalyticsPrivate;

class Analytics : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString host READ host NOTIFY hostChanged)
    Q_PROPERTY(QString appKey READ appKey NOTIFY appKeyChanged)
    Q_PROPERTY(QString deviceId READ deviceId NOTIFY deviceIdChanged)

public:
    static Analytics *get();

    explicit Analytics(QObject *parent = nullptr);

    ~Analytics();

    void init();

    const QString &host() const;

    const QString &appKey() const;

    const QString &deviceId() const;

public Q_SLOTS:
     void sendEvent(const QString &name);

     void sendScreen(const QString &name);

Q_SIGNALS:
    void hostChanged();
    void appKeyChanged();
    void deviceIdChanged();

protected:
    void setHost(const QString &host);

    void setAppKey(const QString &appKey);

    void setDeviceId(const QString &deviceId);

    friend class TSingleton<Analytics>;

private:
    QSharedPointer<AnalyticsPrivate> d_ptr;
};

#endif // ANALYTICS_H
