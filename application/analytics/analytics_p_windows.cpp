/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2021  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/

#include "analytics_p.h"

#include <Utils.h>

#include "Countly.h"

AnalyticsPrivate::AnalyticsPrivate()
{

}

AnalyticsPrivate::~AnalyticsPrivate()
{

}

void AnalyticsPrivate::init()
{
    QSharedPointer<CountlyConfig> config(new CountlyConfig);

    config->deviceID = deviceId;
    config->resetStoredDeviceID = true;
    config->appKey = appKey;
    config->host = host;

    config->alwaysUsePOST = true;

    config->enableDebug = false;

    config->enablePerformanceMonitoring = true;

    config->requiresConsent = true;

    config->consents = {CLYConsentPushNotifications, CLYConsentSessions, CLYConsentEvents, CLYConsentViewTracking, CLYConsentLocation, CLYConsentPerformanceMonitoring};

    Countly::sharedInstance()->startWithConfig(config);
}

void AnalyticsPrivate::sendEvent(const QString &name)
{

}

void AnalyticsPrivate::sendScreen(const QString &name)
{
    qWarning() << "NIY";
}
