import QtQuick 2.15
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import UIPage 1.0 as UIPage
import AppStyles 1.0
import UIKit 1.0 as UIKit
import UIFragments 1.0 as UIFragments
import QtQuick.Layouts 1.12
import com.test 1.0

ApplicationWindow {
  id: applicationMainWindow
  objectName: "appMainWindow"
  visible: true
  width: 1440
  height: 900
  minimumWidth: 900
  minimumHeight: 600

  title: Strings.quodArca
  color: ThemeController.style.whiteColor
  flags: Qt.Window | (Qt.platform.os === "ios" ? Qt.MaximizeUsingFullscreenGeometryHint : 0)
  onClosing: Qt.quit()
  property bool isMobile: app.isMobile

  Component.onCompleted: {
    smartScale.init(applicationMainWindow)
    safeMargins.setQmlWindow(applicationMainWindow)
  }

  StackView {
    id: mainStackView
    anchors {
      fill: parent
      leftMargin: safeMargins.left
      rightMargin: safeMargins.right
      topMargin: safeMargins.top
      bottomMargin: safeMargins.bottom
    }

    onCurrentItemChanged: RegistrationManager.currentItem = mainStackView.currentItem

    Component.onCompleted: RegistrationManager.init()
  }

  Connections {
    target: pageManager

    function onPushPage(page, pagePath) {
      switch (page) {
      case Pages.HomePage:
        if (homeScreen.status === Loader.Ready) {
          mainStackView.push(homeScreen.item)
          homeScreen.item.visible = true
        } else {
          // Item has not been loaded yet, set loader flag to show item ASAP
          homeScreen.requested = true
        }
        break
      default:
        mainStackView.push(Qt.createComponent(pagePath))
      }

    }

    function onPopPage(page) {
      switch (page) {
      case Pages.HomePage:
        homeScreen.requested = false
      }
      back()
    }
  }

  Loader {
    id: homeScreen
    property bool requested: false
    source: "qrc:/UIPage/HomePage.qml"
    asynchronous: true
    visible: false
    anchors.fill: mainStackView

    onLoaded: {
        if(homeScreen.requested) {
            mainStackView.push(homeScreen.item)
            homeScreen.item.visible = true
        } else {
            homeScreen.item.visible = false
        }        
    }
  }

  Shortcut {
    sequence: StandardKey.ZoomIn
    context: Qt.ApplicationShortcut
    onActivated: smartScale.zoomIn()
    enabled: !app.isMobile
  }
  Shortcut {
    sequence: StandardKey.ZoomOut
    context: Qt.ApplicationShortcut
    onActivated: smartScale.zoomOut()
    enabled: !app.isMobile
  }

  // JavaScript
  function back() {
    console.log("Back request. Items on stack:", mainStackView.depth)
    if (mainStackView.depth > 1) {
      mainStackView.pop()
    }
  }

  /* Checks if input is not empty. Otherwise shows the error message.
   * Returns true on no error
   */
  function validateMandatoryField(input, text = undefined) {
    if (input.textField.text === "" && input.visible){
      input.errorMessage = text !== undefined ? [text] : [Strings.mandatoryField]
      input.state = 'error'
      return false
    }
    return true
  }

  /* Validates the input for the emptiness and acceptableInput. */
  function validateInput(input, text) {
    var ok = validateMandatoryField(input)
    if (ok && !input.textField.acceptableInput) {
      input.errorMessage = [text]
      input.state = 'error'
      return false
    } else {
      return ok
    }
  }

  /* helper method with error text for email input defined */
  function validateEmailInput(input) {
    return validateInput(input, Strings.emailIsNotValid)
  }

  /* Validates two corresponding passphrase inputs. Checks the emptiness and if
   * texts in both fields are the same
   */
  function validatePassphraseInput(input, repeatInput = null) {
    var inputOK = validateMandatoryField(input)
    var repeatOK = true
    if (repeatInput !== null) {
      repeatOK = validateMandatoryField(repeatInput)
      if (repeatOK) {
        if (repeatInput.textField.text !== input.textField.text) {
          repeatInput.errorMessage = [Strings.passhrasesAreNotTheSame]
          repeatInput.state = 'error'
          repeatOK = false
        }
      }
    }
    return inputOK && repeatOK
  }

  /* Validates passphrase inputs. Checks the emptiness and if
   * text in input is equal to provided text argument
   */
  function validatePassphraseInputText(input, text) {
    var inputOK = validateMandatoryField(input)
    if (text !== input.textField.text) {
      input.errorMessage = [Strings.passhrasesAreNotTheSame]
      input.state = 'error'
      inputOK = false
    }
    return inputOK
  }

  function s(value) {
    return value * Utility.scaleFactor
  }
}

