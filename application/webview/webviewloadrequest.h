/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef WEBVIEWLOADREQUEST_H
#define WEBVIEWLOADREQUEST_H

#include <QUrl>

#include "webview.h"

class QWebViewLoadRequestPrivate
{
public:
    QWebViewLoadRequestPrivate() {}
    QWebViewLoadRequestPrivate(const QUrl &url,
                               QuodArcaWebView::LoadStatus status,
                               const QString &errorString)
        : m_url(url), m_status(status), m_errorString(errorString)
    {}

    ~QWebViewLoadRequestPrivate() {}

    QUrl m_url;
    QuodArcaWebView::LoadStatus m_status;
    QString m_errorString;
};

#endif // WEBVIEWLOADREQUEST_H
