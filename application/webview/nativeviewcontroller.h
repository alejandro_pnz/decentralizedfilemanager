/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef NATIVEVIEWCONTROLLER_H
#define NATIVEVIEWCONTROLLER_H

#include <QWindow>

class NativeViewController
{
public:
    virtual ~NativeViewController() {}
    virtual void setParentView(QObject *view) = 0;
    virtual QObject *parentView() const = 0;
    virtual void setGeometry(const QRect &geometry) = 0;
    virtual void setVisibility(QWindow::Visibility visibility) = 0;
    virtual void setVisible(bool visible) = 0;
    virtual void init() { }
    virtual void setFocus(bool focus) { Q_UNUSED(focus); }
};

#endif // NATIVEVIEWCONTROLLER_H
