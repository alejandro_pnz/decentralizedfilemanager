/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "quickviewcontroller.h"

#include "nativeviewcontroller.h"

#include <QQuickWindow>
#include <QQuickRenderControl>

#include <QtQuick/private/qquickitem_p.h>
#include <QtQuick/private/qquickitemchangelistener_p.h>

static const QQuickItemPrivate::ChangeTypes changeMask =
        QQuickItemPrivate::Geometry | QQuickItemPrivate::Children | QQuickItemPrivate::Parent;

class QuickViewChangeListener : public QQuickItemChangeListener
{
public:
    explicit QuickViewChangeListener(QuickViewController *item);
    ~QuickViewChangeListener();

    inline void itemGeometryChanged(QQuickItem *, QQuickGeometryChange, const QRectF &) override;
    void itemChildRemoved(QQuickItem *item, QQuickItem *child) override;
    void itemParentChanged(QQuickItem *item, QQuickItem *parent) override;

private:
    Q_DISABLE_COPY(QuickViewChangeListener)
    QuickViewController *m_item;
    void addAncestorListeners(QQuickItem *item, QQuickItemPrivate::ChangeTypes changeType);
    void removeAncestorListeners(QQuickItem *item, QQuickItemPrivate::ChangeTypes changeType);
    bool isAncestor(QQuickItem *item);
};

QuickViewChangeListener::QuickViewChangeListener(QuickViewController *item) : m_item(item)
{
    // Only listen for parent changes on the view controller item.
    QQuickItemPrivate::get(item)->addItemChangeListener(this, QQuickItemPrivate::Parent);
    // Listen to all changes, that are relevant, on all ancestors.
    addAncestorListeners(item->parentItem(), changeMask);
}

QuickViewChangeListener::~QuickViewChangeListener()
{
    if (m_item == 0)
        return;

    QQuickItemPrivate::get(m_item)->removeItemChangeListener(this, QQuickItemPrivate::Parent);
    removeAncestorListeners(m_item->parentItem(), changeMask);
}

void QuickViewChangeListener::itemGeometryChanged(QQuickItem *, QQuickGeometryChange,
                                                   const QRectF &)
{
    m_item->polish();
}

void QuickViewChangeListener::itemChildRemoved(QQuickItem *item, QQuickItem *child)
{
    Q_UNUSED(item);
    Q_ASSERT(item != m_item);

    const bool remove = (child == m_item) || isAncestor(child);

    // if the child isn't the view item or its ancestor, then we don't care.
    if (!remove)
        return;

    // Remove any listener we attached to the child and its ancestors.
    removeAncestorListeners(item, changeMask);
}

void QuickViewChangeListener::itemParentChanged(QQuickItem *item, QQuickItem *newParent)
{
    removeAncestorListeners(item->parentItem(), changeMask);
    // Adds this as a listener for newParent and its ancestors.
    addAncestorListeners(newParent, changeMask);
}

void QuickViewChangeListener::addAncestorListeners(QQuickItem *item,
                                                    QQuickItemPrivate::ChangeTypes changeType)
{
    QQuickItem *p = item;
    while (p != 0) {
        QQuickItemPrivate::get(p)->addItemChangeListener(this, changeType);
        p = p->parentItem();
    }
}

void QuickViewChangeListener::removeAncestorListeners(QQuickItem *item,
                                                       QQuickItemPrivate::ChangeTypes changeType)
{
    QQuickItem *p = item;
    while (p != 0) {
        QQuickItemPrivate::get(p)->removeItemChangeListener(this, changeType);
        p = p->parentItem();
    }
}

bool QuickViewChangeListener::isAncestor(QQuickItem *item)
{
    Q_ASSERT(m_item != 0);

    if (item == 0)
        return false;

    QQuickItem *p = m_item->parentItem();
    while (p != 0) {
        if (p == item)
            return true;
        p = p->parentItem();
    }

    return false;
}

///
/// \brief QQuickViewController::QQuickViewController
/// \param parent
///

QuickViewController::QuickViewController(QQuickItem *parent)
    : QQuickItem(parent), m_view(0), m_changeListener(new QuickViewChangeListener(this))
{
    connect(this, &QuickViewController::windowChanged, this,
            &QuickViewController::onWindowChanged);
    connect(this, &QuickViewController::visibleChanged, this,
            &QuickViewController::onVisibleChanged);
}

QuickViewController::~QuickViewController()
{
    disconnect(this);
    onWindowChanged(nullptr);
}

void QuickViewController::componentComplete()
{
    QQuickItem::componentComplete();
    m_view->init();
    m_view->setVisibility(QWindow::Windowed);
}

void QuickViewController::updatePolish()
{
    if (m_view == nullptr)
        return;

    QSize itemSize = QSize(width(), height());
    if (!itemSize.isValid())
        return;

    QQuickWindow *w = window();
    if (w == nullptr)
        return;

    // Find this item's geometry in the scene.
    QRect itemGeometry = mapRectToScene(QRect(QPoint(0, 0), itemSize)).toRect();
    // Check if we should be clipped to our parent's shape
    // Note: This is crude but it should give an acceptable result on all platforms.
    QQuickItem *p = parentItem();
    const bool clip = p != 0 ? p->clip() : false;
    if (clip) {
        const QSize &parentSize = QSize(p->width(), p->height());
        const QRect &parentGeometry = p->mapRectToScene(QRect(QPoint(0, 0), parentSize)).toRect();
        itemGeometry &= parentGeometry;
        itemSize = itemGeometry.size();
    }

    // Find the top left position of this item, in global coordinates.
    const QPoint &tl = w->mapToGlobal(itemGeometry.topLeft());
    // Get the actual render window, in case we're rendering into a off-screen window.
    QWindow *rw = QQuickRenderControl::renderWindowFor(w);

    m_view->setGeometry(rw ? QRect(rw->mapFromGlobal(tl), itemSize) : itemGeometry);
    m_view->setVisible(isVisible());
}

void QuickViewController::setView(NativeViewController *view)
{
    Q_ASSERT(m_view == 0);
    m_view = view;
}

void QuickViewController::scheduleUpdatePolish()
{
    polish();
}

void QuickViewController::geometryChange(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickItem::geometryChange(newGeometry, oldGeometry);
    if (newGeometry.isValid())
        polish();
}

void QuickViewController::onWindowChanged(QQuickWindow *window)
{
    QQuickWindow *oldParent = qobject_cast<QQuickWindow *>(m_view->parentView());
    if (oldParent)
        oldParent->disconnect(this);

    if (!window) {
        m_view->setParentView(nullptr);
        return;
    }

    // Check if there's an actual native window available.
    QWindow *rw = QQuickRenderControl::renderWindowFor(window);

    if (rw) {
        connect(rw, &QWindow::widthChanged, this, &QuickViewController::scheduleUpdatePolish);
        connect(rw, &QWindow::heightChanged, this, &QuickViewController::scheduleUpdatePolish);
        connect(rw, &QWindow::xChanged, this, &QuickViewController::scheduleUpdatePolish);
        connect(rw, &QWindow::yChanged, this, &QuickViewController::scheduleUpdatePolish);
        connect(rw, &QWindow::visibleChanged, this,
                [this](bool visible) { m_view->setVisible(visible); });
        connect(window, &QQuickWindow::sceneGraphInitialized, this,
                &QuickViewController::scheduleUpdatePolish);
        connect(window, &QQuickWindow::sceneGraphInvalidated, this,
                &QuickViewController::onSceneGraphInvalidated);
        m_view->setParentView(rw);
    } else {
        connect(window, &QWindow::widthChanged, this, &QuickViewController::scheduleUpdatePolish);
        connect(window, &QWindow::heightChanged, this, &QuickViewController::scheduleUpdatePolish);
        connect(window, &QWindow::xChanged, this, &QuickViewController::scheduleUpdatePolish);
        connect(window, &QWindow::yChanged, this, &QuickViewController::scheduleUpdatePolish);
        connect(window, &QQuickWindow::sceneGraphInitialized, this,
                &QuickViewController::scheduleUpdatePolish);
        connect(window, &QQuickWindow::sceneGraphInvalidated, this,
                &QuickViewController::onSceneGraphInvalidated);
        connect(window, &QWindow::visibilityChanged, this, [this](QWindow::Visibility visibility) {
            m_view->setVisible(visibility != QWindow::Hidden);
        });
        m_view->setVisible(window->visibility() != QWindow::Hidden);
        m_view->setParentView(window);
    }
}

void QuickViewController::onVisibleChanged()
{
    m_view->setVisible(isVisible());
}

void QuickViewController::onSceneGraphInvalidated()
{
    if (m_view == 0)
        return;

    m_view->setVisible(false);
}
