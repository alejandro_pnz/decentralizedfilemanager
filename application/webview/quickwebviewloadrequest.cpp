/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "quickwebviewloadrequest.h"

#include "webviewloadrequest.h"

QuickWebViewLoadRequest::QuickWebViewLoadRequest(const QWebViewLoadRequestPrivate &d)
    : d_ptr(new QWebViewLoadRequestPrivate(d))
{
}

QuickWebViewLoadRequest::~QuickWebViewLoadRequest()
{
}

QUrl QuickWebViewLoadRequest::url() const
{
    Q_D(const QWebViewLoadRequest);
    return d->m_url;
}

QuickWebView::LoadStatus QuickWebViewLoadRequest::status() const
{
    Q_D(const QWebViewLoadRequest);
    return QuickWebView::LoadStatus(d->m_status);
}

QString QuickWebViewLoadRequest::errorString() const
{
    Q_D(const QWebViewLoadRequest);
    return d->m_errorString;
}
