/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef WEBVIEWFACTORY_H
#define WEBVIEWFACTORY_H

#include "abstractwebview.h"

#if defined(Q_OS_ANDROID)
#include "androidwebview.h"
#elif defined(Q_OS_DARWIN)
#include "darwinwebview.h"
#endif

namespace WebViewFactory
{
    AbstractWebView *createWebView()
    {
#if defined(Q_OS_ANDROID)
        return new AndroidWebView;
#elif defined(Q_OS_DARWIN)
        return new DarwinWebView;
#else
        return nullptr;
#endif
    }
};

#endif // WEBVIEWFACTORY_H
