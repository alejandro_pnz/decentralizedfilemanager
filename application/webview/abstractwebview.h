/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef ABSTRACTWEBVIEW_H
#define ABSTRACTWEBVIEW_H

#include "webviewinterface.h"
#include "nativeviewcontroller.h"
#include "webviewloadrequest.h"

class AbstractWebView : public QObject, public WebViewInterface, public NativeViewController
{
    Q_OBJECT

Q_SIGNALS:
    void titleChanged(const QString &title);
    void urlChanged(const QUrl &url);
    void loadingChanged(const QWebViewLoadRequestPrivate &loadRequest);
    void loadProgressChanged(int progress);
    void javaScriptResult(int id, const QVariant &result);
    void requestFocus(bool focus);
    void httpUserAgentChanged(const QString &httpUserAgent);
    void javaScriptConsoleMessage(const QString &message);

protected:
    explicit AbstractWebView(QObject *p = 0) : QObject(p) { }
};

#endif // ABSTRACTWEBVIEW_H
