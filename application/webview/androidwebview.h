/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef ANDROIDWEBVIEW_H
#define ANDROIDWEBVIEW_H

#include "abstractwebview.h"

#include <QJniObject>
#include <QUrl>
#include <QWindow>

class AndroidWebView : public AbstractWebView
{
    Q_OBJECT

public:
    explicit AndroidWebView(QObject *p = 0);
    ~AndroidWebView() override;

    QString httpUserAgent() const override;
    void setHttpUserAgent(const QString &httpUserAgent) override;

    QUrl url() const override;
    void setUrl(const QUrl &url) override;

    bool canGoBack() const override;
    bool canGoForward() const override;

    QString title() const override;

    int loadProgress() const override;

    bool isLoading() const override;

    void setParentView(QObject *view) override;

    QObject *parentView() const override;

    void setGeometry(const QRect &geometry) override;

    void setVisibility(QWindow::Visibility visibility) override;
    void setVisible(bool visible) override;

    static bool registerNatives(JNIEnv *env);

public Q_SLOTS:
    void goBack() override;
    void goForward() override;
    void reload() override;
    void stop() override;
    void loadHtml(const QString &html, const QUrl &baseUrl = QUrl()) override;

protected:
    void runJavaScriptPrivate(const QString& script,
                              int callbackId) override;
private Q_SLOTS:
    void onApplicationStateChanged(Qt::ApplicationState state);

private:
    quintptr m_id;
    quint64 m_callbackId;
    QWindow *m_window;
    QJniObject m_viewController;
    QJniObject m_webView;
};

#endif // ANDROIDWEBVIEW_H
