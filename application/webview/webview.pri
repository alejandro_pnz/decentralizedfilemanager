
QT += quick-private

android {
    include($$PWD/../functions.pri)

    ANDROID_WEBVIEW_SOURCES = \
        $$PWD/android/java/QuodArcaAndroidWebViewController.java

    QMAKE_EXTRA_TARGETS += $$copyAndroidSources("AndroidWebViewSources", "src/com/eisst/quodarca", $$ANDROID_WEBVIEW_SOURCES)

    INCLUDEPATH += $$PWD

    HEADERS += \
        $$PWD/androidwebview.h

    SOURCES += \
        $$PWD/androidwebview.cpp
}
mac {
    HEADERS += \
        $$PWD/darwinwebview.h

    SOURCES += \
        $$PWD/darwinwebview.mm

    LIBS += -framework WebKit
}

HEADERS += \
    $$PWD/abstractwebview.h \
    $$PWD/nativeviewcontroller.h \
    $$PWD/quickviewcontroller.h \
    $$PWD/quickwebview.h \
    $$PWD/quickwebviewloadrequest.h \
    $$PWD/webview.h \
    $$PWD/webviewfactory.h \
    $$PWD/webviewinterface.h \
    $$PWD/webviewloadrequest.h

SOURCES += \
    $$PWD/quickviewcontroller.cpp \
    $$PWD/quickwebview.cpp \
    $$PWD/quickwebviewloadrequest.cpp \
    $$PWD/webview.cpp
