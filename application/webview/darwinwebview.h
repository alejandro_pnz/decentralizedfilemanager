/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef DARWINWEBVIEW_H
#define DARWINWEBVIEW_H

#include "abstractwebview.h"

#include <QPointer>

#if defined(Q_OS_IOS) && defined(__OBJC__)
#include <UIKit/UIGestureRecognizer.h>

@interface IOSNativeViewSelectedRecognizer : UIGestureRecognizer <UIGestureRecognizerDelegate>
{
@public
    NativeViewController *m_item;
}
- (id)initWithQWindowControllerItem:(NativeViewController *)item;
@end
#endif

Q_FORWARD_DECLARE_OBJC_CLASS(WKWebView);
Q_FORWARD_DECLARE_OBJC_CLASS(WKNavigation);
Q_FORWARD_DECLARE_OBJC_CLASS(QtWKWebViewNavigationDelegate);

#ifdef Q_OS_IOS
Q_FORWARD_DECLARE_OBJC_CLASS(UIGestureRecognizer);
#endif

class DarwinWebView : public AbstractWebView
{
    Q_OBJECT

public:
    explicit DarwinWebView(QObject *p = nullptr);
    ~DarwinWebView() override;

    QString httpUserAgent() const override;
    void setHttpUserAgent(const QString &httpUserAgent) override;
    QUrl url() const override;
    void setUrl(const QUrl &url) override;
    bool canGoBack() const override;
    bool canGoForward() const override;
    QString title() const override;
    int loadProgress() const override;
    bool isLoading() const override;

    void setParentView(QObject *view) override;
    QObject *parentView() const override;
    void setGeometry(const QRect &geometry) override;
    void setVisibility(QWindow::Visibility visibility) override;
    void setVisible(bool visible) override;
    void setFocus(bool focus) override;

public Q_SLOTS:
    void goBack() override;
    void goForward() override;
    void reload() override;
    void stop() override;
    void loadHtml(const QString &html, const QUrl &baseUrl = QUrl()) override;

protected:
    void runJavaScriptPrivate(const QString& script,
                              int callbackId) override;

public:
    WKWebView *wkWebView;
    WKNavigation *wkNavigation;
    QtWKWebViewNavigationDelegate *delegate;
#ifdef Q_OS_IOS
    UIGestureRecognizer *m_recognizer;
#endif
    QPointer<QObject> m_parentView;
};

#endif // DARWINWEBVIEW_H
