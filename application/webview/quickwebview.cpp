/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "quickwebview.h"

#include <QQmlEngine>
#include <QMutexLocker>

#include "quickwebviewloadrequest.h"

namespace {

class CallbackStorage
{
public:
    int insertCallback(const QJSValue &callback)
    {
        QMutexLocker locker(&m_mtx);
        const int nextId = qMax(++m_counter, 0);
        if (nextId == 0)
            m_counter = 1;

        m_callbacks.insert(nextId, callback);
        return nextId;
    }

    QJSValue takeCallback(int callbackId)
    {
        QMutexLocker lock(&m_mtx);
        return m_callbacks.take(callbackId);
    }

private:
    QMutex m_mtx;
    int m_counter;
    QHash<int, QJSValue> m_callbacks;
};

} // namespace

Q_GLOBAL_STATIC(CallbackStorage, callbacks)

/*!
    \qmltype WebView
    \inqmlmodule QtWebView
    \ingroup qtwebview
    \brief A component for displaying web content.

    WebView is a component for displaying web content which is implemented using native
    APIs on the platforms where this is available, thus it does not necessarily require
    including a full web browser stack as part of the application.

    To make the Qt WebView module function correctly across all platforms, it is necessary
    to call \l {qtwebview-initialize}{QtWebView::initialize}() right after creating the
    QGuiApplication instance.

    \note Due to platform limitations overlapping the WebView and other QML components
    is not supported.
*/

QuickWebView::QuickWebView(QQuickItem *parent)
    : QuickViewController(parent), m_webView(new QuodArcaWebView(this))
{
    setView(m_webView);
    qRegisterMetaType<QuickWebViewLoadRequest*>();
    connect(m_webView, &QuodArcaWebView::titleChanged, this, &QuickWebView::titleChanged);
    connect(m_webView, &QuodArcaWebView::urlChanged, this, &QuickWebView::urlChanged);
    connect(m_webView, &QuodArcaWebView::loadProgressChanged, this, &QuickWebView::loadProgressChanged);
    connect(m_webView, &QuodArcaWebView::loadingChanged, this, &QuickWebView::onLoadingChanged);
    connect(m_webView, &QuodArcaWebView::requestFocus, this, &QuickWebView::onFocusRequest);
    connect(m_webView, &QuodArcaWebView::javaScriptResult, this, &QuickWebView::onRunJavaScriptResult);
    connect(m_webView, &QuodArcaWebView::httpUserAgentChanged, this, &QuickWebView::httpUserAgentChanged);
    connect(m_webView, &QuodArcaWebView::javaScriptConsoleMessage, this, &QuickWebView::javaScriptConsoleMessage);
}

QuickWebView::~QuickWebView() { }

/*!
  \qmlproperty url QtWebView::WebView::httpUserAgent
  \since QtWebView 1.14
  The user agent in use.

  \note on WinRT, this property affects all WebViews of the application.
*/

void QuickWebView::setHttpUserAgent(const QString &userAgent)
{
    m_webView->setHttpUserAgent(userAgent);
}

QString QuickWebView::httpUserAgent() const
{
    return m_webView->httpUserAgent();
}

/*!
  \qmlproperty url QtWebView::WebView::url

  The URL of currently loaded web page. Changing this will trigger
  loading new content.

  The URL is used as-is. URLs that originate from user input should
  be parsed with QUrl::fromUserInput().

  \note The WebView does not support loading content through the Qt Resource system.
*/

void QuickWebView::setUrl(const QUrl &url)
{
    m_webView->setUrl(url);
}

/*!
  \qmlproperty string QtWebView::WebView::title
  \readonly

  The title of the currently loaded web page.
*/

QString QuickWebView::title() const
{
    return m_webView->title();
}

QUrl QuickWebView::url() const
{
    return m_webView->url();
}

/*!
  \qmlproperty bool QtWebView::WebView::canGoBack
  \readonly

  Holds \c true if it's currently possible to navigate back in the web history.
*/

bool QuickWebView::canGoBack() const
{
    return m_webView->canGoBack();
}

/*!
  \qmlproperty bool QtWebView::WebView::canGoForward
  \readonly

  Holds \c true if it's currently possible to navigate forward in the web history.
*/

bool QuickWebView::canGoForward() const
{
    return m_webView->canGoForward();
}

/*!
  \qmlproperty int QtWebView::WebView::loadProgress
  \readonly

  The current load progress of the web content, represented as
  an integer between 0 and 100.
*/
int QuickWebView::loadProgress() const
{
    return m_webView->loadProgress();
}

/*!
  \qmlproperty bool QtWebView::WebView::loading
  \readonly

  Holds \c true if the WebView is currently in the process of loading
  new content, \c false otherwise.

  \sa loadingChanged()
*/

/*!
  \qmlsignal QtWebView::WebView::loadingChanged(WebViewLoadRequest loadRequest)

  This signal is emitted when the state of loading the web content changes.
  By handling this signal it's possible, for example, to react to page load
  errors.

  The \a loadRequest parameter holds the \e url and \e status of the request,
  as well as an \e errorString containing an error message for a failed
  request.

  \sa WebViewLoadRequest
*/
bool QuickWebView::isLoading() const
{
    return m_webView->isLoading();
}

/*!
    \qmlmethod void QtWebView::WebView::goBack()

    Navigates back in the web history.
*/
void QuickWebView::goBack()
{
    m_webView->goBack();
}

/*!
    \qmlmethod void QtWebView::WebView::goForward()

    Navigates forward in the web history.
*/
void QuickWebView::goForward()
{
    m_webView->goForward();
}

/*!
    \qmlmethod void QtWebView::WebView::reload()

    Reloads the current \l url.
*/
void QuickWebView::reload()
{
    m_webView->reload();
}

/*!
    \qmlmethod void QtWebView::WebView::stop()

    Stops loading the current \l url.
*/
void QuickWebView::stop()
{
    m_webView->stop();
}

/*!
    \qmlmethod void QtWebView::WebView::loadHtml(string html, url baseUrl)

    Loads the specified \a html content to the web view.

    This method offers a lower-level alternative to the \l url property,
    which references HTML pages via URL.

    External objects such as stylesheets or images referenced in the HTML
    document should be located relative to \a baseUrl. For example, if \a html
    is retrieved from \c http://www.example.com/documents/overview.html, which
    is the base URL, then an image referenced with the relative url, \c diagram.png,
    should be at \c{http://www.example.com/documents/diagram.png}.

    \note The WebView does not support loading content through the Qt Resource system.

    \sa url
*/
void QuickWebView::loadHtml(const QString &html, const QUrl &baseUrl)
{
    m_webView->loadHtml(html, baseUrl);
}

/*!
    \qmlmethod void QtWebView::WebView::runJavaScript(string script, variant callback)

    Runs the specified JavaScript.
    In case a \a callback function is provided, it will be invoked after the \a script
    finished running.

    \badcode
    runJavaScript("document.title", function(result) { console.log(result); });
    \endcode
*/
void QuickWebView::runJavaScript(const QString &script, const QJSValue &callback)
{
    const int callbackId = callback.isCallable() ? callbacks->insertCallback(callback) : -1;
    runJavaScriptPrivate(script, callbackId);
}

void QuickWebView::runJavaScriptPrivate(const QString &script, int callbackId)
{
    m_webView->runJavaScriptPrivate(script, callbackId);
}

void QuickWebView::itemChange(ItemChange change, const ItemChangeData &value)
{
    if (change == QQuickItem::ItemActiveFocusHasChanged) {
        m_webView->setFocus(value.boolValue);
    }
    QQuickItem::itemChange(change, value);
}

void QuickWebView::onRunJavaScriptResult(int id, const QVariant &variant)
{
    if (id == -1)
        return;

    QJSValue callback = callbacks->takeCallback(id);
    if (callback.isUndefined())
        return;

    QQmlEngine *engine = qmlEngine(this);
    if (engine == 0) {
        qWarning("No JavaScript engine, unable to handle JavaScript callback!");
        return;
    }

    QJSValueList args;
    args.append(engine->toScriptValue(variant));
    callback.call(args);
}

void QuickWebView::onFocusRequest(bool focus)
{
    setFocus(focus);
}

void QuickWebView::onLoadingChanged(const QWebViewLoadRequestPrivate &loadRequest)
{
    QuickWebViewLoadRequest qqLoadRequest(loadRequest);
    Q_EMIT loadingChanged(&qqLoadRequest);
}

QJSValue QuickWebView::takeCallback(int id)
{
    return callbacks->takeCallback(id);
}

void _q_QQuickWebView_registerTypes() {
    qmlRegisterType<QuickWebView>("com.test", 1, 0, "QuodArcaWebView");
}

Q_COREAPP_STARTUP_FUNCTION(_q_QQuickWebView_registerTypes)
