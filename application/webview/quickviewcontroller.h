/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef QUICKVIEWCONTROLLER_H
#define QUICKVIEWCONTROLLER_H

#include <QQuickItem>

class NativeViewController;
class QuickViewChangeListener;

class QuickViewController : public QQuickItem
{
    Q_OBJECT
public:
    explicit QuickViewController(QQuickItem *parent = nullptr);
    ~QuickViewController();

public Q_SLOTS:
    void onWindowChanged(QQuickWindow *window);
    void onVisibleChanged();

protected:
    void componentComplete() override;
    void updatePolish() override;
    void geometryChange(const QRectF &newGeometry, const QRectF &oldGeometry) override;
    void setView(NativeViewController *view);

private:
    friend class QQuickWebView;
    NativeViewController *m_view;
    QScopedPointer<QuickViewChangeListener> m_changeListener;

private Q_SLOTS:
    void scheduleUpdatePolish();
    void onSceneGraphInvalidated();
};

#endif // QUICKVIEWCONTROLLER_H
