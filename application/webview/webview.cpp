/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "webview.h"

#include "webviewfactory.h"

QuodArcaWebView::QuodArcaWebView(QObject *p)
    : QObject(p)
    , d(WebViewFactory::createWebView())
    , m_progress(0)
{
    d->setParent(this);
    qRegisterMetaType<QWebViewLoadRequestPrivate>();
    connect(d, &AbstractWebView::titleChanged, this, &QuodArcaWebView::onTitleChanged);
    connect(d, &AbstractWebView::urlChanged, this, &QuodArcaWebView::onUrlChanged);
    connect(d, &AbstractWebView::loadingChanged, this, &QuodArcaWebView::onLoadingChanged);
    connect(d, &AbstractWebView::loadProgressChanged, this, &QuodArcaWebView::onLoadProgressChanged);
    connect(d, &AbstractWebView::httpUserAgentChanged, this, &QuodArcaWebView::onHttpUserAgentChanged);
    connect(d, &AbstractWebView::requestFocus, this, &QuodArcaWebView::requestFocus);
    connect(d, &AbstractWebView::javaScriptResult, this, &QuodArcaWebView::javaScriptResult);
    connect(d, &AbstractWebView::javaScriptConsoleMessage, this, &QuodArcaWebView::javaScriptConsoleMessage);
}

QuodArcaWebView::~QuodArcaWebView()
{
}

QString QuodArcaWebView::httpUserAgent() const
{
    if (m_httpUserAgent.isEmpty()){
        m_httpUserAgent = d->httpUserAgent();
    }
    return m_httpUserAgent;
}

void QuodArcaWebView::setHttpUserAgent(const QString &httpUserAgent)
{
    return d->setHttpUserAgent(httpUserAgent);
}

QUrl QuodArcaWebView::url() const
{
    return m_url;
}

void QuodArcaWebView::setUrl(const QUrl &url)
{
    d->setUrl(url);
}

bool QuodArcaWebView::canGoBack() const
{
    return d->canGoBack();
}

void QuodArcaWebView::goBack()
{
    d->goBack();
}

bool QuodArcaWebView::canGoForward() const
{
    return d->canGoForward();
}

void QuodArcaWebView::goForward()
{
    d->goForward();
}

void QuodArcaWebView::reload()
{
    d->reload();
}

void QuodArcaWebView::stop()
{
    d->stop();
}

QString QuodArcaWebView::title() const
{
    return m_title;
}

int QuodArcaWebView::loadProgress() const
{
    return m_progress;
}

bool QuodArcaWebView::isLoading() const
{
    return d->isLoading();
}

void QuodArcaWebView::setParentView(QObject *view)
{
    d->setParentView(view);
}

QObject *QuodArcaWebView::parentView() const
{
    return d->parentView();
}

void QuodArcaWebView::setGeometry(const QRect &geometry)
{
    d->setGeometry(geometry);
}

void QuodArcaWebView::setVisibility(QWindow::Visibility visibility)
{
    d->setVisibility(visibility);
}

void QuodArcaWebView::setVisible(bool visible)
{
    d->setVisible(visible);
}

void QuodArcaWebView::setFocus(bool focus)
{
    d->setFocus(focus);
}

void QuodArcaWebView::loadHtml(const QString &html, const QUrl &baseUrl)
{
    d->loadHtml(html, baseUrl);
}

void QuodArcaWebView::runJavaScriptPrivate(const QString &script,
                                    int callbackId)
{
    d->runJavaScriptPrivate(script, callbackId);
}

void QuodArcaWebView::onTitleChanged(const QString &title)
{
    if (m_title == title)
        return;
    m_title = title;
    Q_EMIT titleChanged();
}

void QuodArcaWebView::onUrlChanged(const QUrl &url)
{
    if (m_url == url)
        return;
    m_url = url;
    Q_EMIT urlChanged();
}

void QuodArcaWebView::onLoadProgressChanged(int progress)
{
    if (m_progress == progress)
        return;
    m_progress = progress;
    Q_EMIT loadProgressChanged();
}

void QuodArcaWebView::onLoadingChanged(const QWebViewLoadRequestPrivate &loadRequest)
{
    if (loadRequest.m_status == QuodArcaWebView::LoadFailedStatus)
        m_progress = 0;
    onUrlChanged(loadRequest.m_url);
    Q_EMIT loadingChanged(loadRequest);
}

void QuodArcaWebView::onHttpUserAgentChanged(const QString &httpUserAgent)
{
    if (m_httpUserAgent == httpUserAgent)
        return;
    m_httpUserAgent = httpUserAgent;
    Q_EMIT httpUserAgentChanged();
}

void QuodArcaWebView::init()
{
    d->init();
}
