/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#include "androidwebview.h"

#include <QGuiApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#include <QtCore/private/qandroidextras_p.h>

static const char qAndroidWebViewControllerClass[] = "com.test/QuodArcaAndroidWebViewController";

typedef QMap<quintptr, AndroidWebView *> WebViews;
Q_GLOBAL_STATIC(WebViews, g_webViews)

AndroidWebView::AndroidWebView(QObject *p)
    : AbstractWebView(p)
    , m_id(reinterpret_cast<quintptr>(this))
    , m_callbackId(0)
    , m_window(0)
{
    m_viewController = QJniObject(qAndroidWebViewControllerClass,
                                  "(Landroid/app/Activity;J)V",
                                  QNativeInterface::QAndroidApplication::context(),
                                  m_id);
    m_webView = m_viewController.callObjectMethod("getWebView",
                                                  "()Landroid/webkit/WebView;");

    m_window = QWindow::fromWinId(reinterpret_cast<WId>(m_webView.object()));
    g_webViews->insert(m_id, this);
    connect(qApp, &QGuiApplication::applicationStateChanged,
            this, &AndroidWebView::onApplicationStateChanged);
}

AndroidWebView::~AndroidWebView()
{
    g_webViews->take(m_id);
    if (m_window != 0) {
        m_window->setVisible(false);
        m_window->setParent(0);
        delete m_window;
    }

    m_viewController.callMethod<void>("destroy");
}

QString AndroidWebView::httpUserAgent() const
{
    return QString( m_viewController.callObjectMethod<jstring>("getUserAgent").toString());
}

void AndroidWebView::setHttpUserAgent(const QString &httpUserAgent)
{
    m_viewController.callMethod<void>("setUserAgent",
                                      "(Ljava/lang/String;)V",
                                      QJniObject::fromString(httpUserAgent).object());
    Q_EMIT httpUserAgentChanged(httpUserAgent);
}

QUrl AndroidWebView::url() const
{
    return QUrl::fromUserInput(m_viewController.callObjectMethod<jstring>("getUrl").toString());
}

void AndroidWebView::setUrl(const QUrl &url)
{
    m_viewController.callMethod<void>("loadUrl",
                                      "(Ljava/lang/String;)V",
                                      QJniObject::fromString(url.toString()).object());
}

void AndroidWebView::loadHtml(const QString &html, const QUrl &baseUrl)
{
    const QJniObject &htmlString = QJniObject::fromString(html);
    const QJniObject &mimeTypeString = QJniObject::fromString(QLatin1String("text/html;charset=UTF-8"));

    baseUrl.isEmpty() ? m_viewController.callMethod<void>("loadData",
                                                          "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
                                                          htmlString.object(),
                                                          mimeTypeString.object(),
                                                          0)

                      : m_viewController.callMethod<void>("loadDataWithBaseURL",
                                                          "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
                                                          QJniObject::fromString(baseUrl.toString()).object(),
                                                          htmlString.object(),
                                                          mimeTypeString.object(),
                                                          0,
                                                          0);
}

bool AndroidWebView::canGoBack() const
{
    return m_viewController.callMethod<jboolean>("canGoBack");
}

void AndroidWebView::goBack()
{
    m_viewController.callMethod<void>("goBack");
}

bool AndroidWebView::canGoForward() const
{
    return m_viewController.callMethod<jboolean>("canGoForward");
}

void AndroidWebView::goForward()
{
    m_viewController.callMethod<void>("goForward");
}

void AndroidWebView::reload()
{
    m_viewController.callMethod<void>("reload");
}

QString AndroidWebView::title() const
{
    return m_viewController.callObjectMethod<jstring>("getTitle").toString();
}

void AndroidWebView::setGeometry(const QRect &geometry)
{
    if (m_window == 0)
        return;

    m_window->setGeometry(geometry);
}

void AndroidWebView::setVisibility(QWindow::Visibility visibility)
{
    m_window->setVisibility(visibility);
}

void AndroidWebView::runJavaScriptPrivate(const QString &script,
                                                  int callbackId)
{
    if (QtAndroidPrivate::androidSdkVersion() < 19) {
        qWarning("runJavaScript() requires API level 19 or higher.");
        if (callbackId == -1)
            return;

        // Emit signal here to remove the callback.
        Q_EMIT javaScriptResult(callbackId, QVariant());
    }

    m_viewController.callMethod<void>("runJavaScript",
                                      "(Ljava/lang/String;J)V",
                                      static_cast<jstring>(QJniObject::fromString(script).object()),
                                      callbackId);
}

void AndroidWebView::setVisible(bool visible)
{
    m_window->setVisible(visible);
}

int AndroidWebView::loadProgress() const
{
    return m_viewController.callMethod<jint>("getProgress");
}

bool AndroidWebView::isLoading() const
{
    return m_viewController.callMethod<jboolean>("isLoading");
}

void AndroidWebView::setParentView(QObject *view)
{
    m_window->setParent(qobject_cast<QWindow *>(view));
}

QObject *AndroidWebView::parentView() const
{
    return m_window->parent();
}

void AndroidWebView::stop()
{
    m_viewController.callMethod<void>("stopLoading");
}

//void QAndroidWebViewPrivate::initialize()
//{
//    // TODO:
//}

void AndroidWebView::onApplicationStateChanged(Qt::ApplicationState state)
{
    if (QtAndroidPrivate::androidSdkVersion() < 11)
        return;

    if (state == Qt::ApplicationActive)
        m_viewController.callMethod<void>("onResume");
    else
        m_viewController.callMethod<void>("onPause");
}

static void c_onRunJavaScriptResult(JNIEnv *env,
                                    jobject thiz,
                                    jlong id,
                                    jlong callbackId,
                                    jstring result)
{
    Q_UNUSED(env);
    Q_UNUSED(thiz);

    const WebViews &wv = (*g_webViews);
    AndroidWebView *wc = static_cast<AndroidWebView *>(wv[id]);
    if (!wc)
        return;

    const QString &resultString = QJniObject(result).toString();

    // The result string is in JSON format, lets parse it to see what we got.
    QJsonValue jsonValue;
    const QByteArray &jsonData = "{ \"data\": " + resultString.toUtf8() + " }";
    QJsonParseError error;
    const QJsonDocument &jsonDoc = QJsonDocument::fromJson(jsonData, &error);
    if (error.error == QJsonParseError::NoError && jsonDoc.isObject()) {
        const QJsonObject &object = jsonDoc.object();
        jsonValue = object.value(QStringLiteral("data"));
    }

    Q_EMIT wc->javaScriptResult(int(callbackId),
                                jsonValue.isNull() ? resultString
                                                   : jsonValue.toVariant());
}

static void c_onPageFinished(JNIEnv *env,
                             jobject thiz,
                             jlong id,
                             jstring url)
{
    Q_UNUSED(env);
    Q_UNUSED(thiz);
    const WebViews &wv = (*g_webViews);
    AndroidWebView *wc = wv[id];
    if (!wc)
        return;

    QWebViewLoadRequestPrivate loadRequest(QUrl(QJniObject(url).toString()),
                                           QuodArcaWebView::LoadSucceededStatus,
                                           QString());
    Q_EMIT wc->loadingChanged(loadRequest);
}

static void c_onPageStarted(JNIEnv *env,
                            jobject thiz,
                            jlong id,
                            jstring url,
                            jobject icon)
{
    Q_UNUSED(env);
    Q_UNUSED(thiz);
    Q_UNUSED(icon);
    const WebViews &wv = (*g_webViews);
    AndroidWebView *wc = wv[id];
    if (!wc)
        return;
    QWebViewLoadRequestPrivate loadRequest(QUrl(QJniObject(url).toString()),
                                           QuodArcaWebView::LoadStartedStatus,
                                           QString());
    Q_EMIT wc->loadingChanged(loadRequest);

//    if (!icon)
//        return;

//    QImage image;
//    if (favIcon(env, icon, &image))
//        Q_EMIT wc->iconChanged(image);
}

static void c_onProgressChanged(JNIEnv *env,
                                jobject thiz,
                                jlong id,
                                jint newProgress)
{
    Q_UNUSED(env);
    Q_UNUSED(thiz);
    const WebViews &wv = (*g_webViews);
    AndroidWebView *wc = wv[id];
    if (!wc)
        return;

    Q_EMIT wc->loadProgressChanged(newProgress);
}

static void c_onReceivedIcon(JNIEnv *env,
                             jobject thiz,
                             jlong id,
                             jobject icon)
{
    Q_UNUSED(env);
    Q_UNUSED(thiz);
    Q_UNUSED(id);
    Q_UNUSED(icon);

    const WebViews &wv = (*g_webViews);
    AndroidWebView *wc = wv[id];
    if (!wc)
        return;

    if (!icon)
        return;

//    QImage image;
//    if (favIcon(env, icon, &image))
//        Q_EMIT wc->iconChanged(image);
}

static void c_onReceivedTitle(JNIEnv *env,
                              jobject thiz,
                              jlong id,
                              jstring title)
{
    Q_UNUSED(env);
    Q_UNUSED(thiz);
    const WebViews &wv = (*g_webViews);
    AndroidWebView *wc = wv[id];
    if (!wc)
        return;

    const QString &qTitle = QJniObject(title).toString();
    Q_EMIT wc->titleChanged(qTitle);
}

static void c_onReceivedError(JNIEnv *env,
                              jobject thiz,
                              jlong id,
                              jint errorCode,
                              jstring description,
                              jstring url)
{
    Q_UNUSED(env);
    Q_UNUSED(thiz);
    Q_UNUSED(errorCode);

    const WebViews &wv = (*g_webViews);
    AndroidWebView *wc = wv[id];
    if (!wc)
        return;
    QWebViewLoadRequestPrivate loadRequest(QUrl(QJniObject(url).toString()),
                                           QuodArcaWebView::LoadFailedStatus,
                                           QJniObject(description).toString());
    Q_EMIT wc->loadingChanged(loadRequest);
}

static void c_onConsoleMessage(JNIEnv *env,
                               jobject thiz,
                               jlong id,
                               jstring message)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)

    const WebViews &wv = (*g_webViews);
    AndroidWebView *wc = wv[id];
    if (!wc)
        return;

    const QString &qMessage = QJniObject(message).toString();
    Q_EMIT wc->javaScriptConsoleMessage(qMessage);
}

bool AndroidWebView::registerNatives(JNIEnv *env)
{
    jclass clazz = env->FindClass(qAndroidWebViewControllerClass);
    if (!clazz) return false;

    JNINativeMethod methods[] = {
        {"c_onPageFinished", "(JLjava/lang/String;)V", reinterpret_cast<void *>(c_onPageFinished)},
        {"c_onPageStarted", "(JLjava/lang/String;Landroid/graphics/Bitmap;)V", reinterpret_cast<void *>(c_onPageStarted)},
        {"c_onProgressChanged", "(JI)V", reinterpret_cast<void *>(c_onProgressChanged)},
        {"c_onReceivedIcon", "(JLandroid/graphics/Bitmap;)V", reinterpret_cast<void *>(c_onReceivedIcon)},
        {"c_onReceivedTitle", "(JLjava/lang/String;)V", reinterpret_cast<void *>(c_onReceivedTitle)},
        {"c_onRunJavaScriptResult", "(JJLjava/lang/String;)V", reinterpret_cast<void *>(c_onRunJavaScriptResult)},
        {"c_onReceivedError", "(JILjava/lang/String;Ljava/lang/String;)V", reinterpret_cast<void *>(c_onReceivedError)},
        {"c_onConsoleMessage", "(JLjava/lang/String;)V", reinterpret_cast<void *>(c_onConsoleMessage)}
    };
    const int nMethods = sizeof(methods) / sizeof(methods[0]);
    return env->RegisterNatives(clazz, methods, nMethods) == JNI_OK;
}
