/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef QUICKWEBVIEWLOADREQUEST_H
#define QUICKWEBVIEWLOADREQUEST_H

#include <QObject>

#include "quickwebview.h"

class QWebViewLoadRequestPrivate;

class QuickWebViewLoadRequest : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUrl url READ url CONSTANT)
    Q_PROPERTY(QuickWebView::LoadStatus status READ status CONSTANT)
    Q_PROPERTY(QString errorString READ errorString CONSTANT)

public:
    ~QuickWebViewLoadRequest();
    QUrl url() const;
    QuickWebView::LoadStatus status() const;
    QString errorString() const;

private:
    friend class QuickWebView;

    explicit QuickWebViewLoadRequest(const QWebViewLoadRequestPrivate &d);

    Q_DECLARE_PRIVATE(QWebViewLoadRequest)
    QScopedPointer<QWebViewLoadRequestPrivate> d_ptr;
};

#endif // QUICKWEBVIEWLOADREQUEST_H
