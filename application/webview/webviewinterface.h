/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef WEBVIEWINTERFACE_H
#define WEBVIEWINTERFACE_H

#include <QObject>
#include <QString>
#include <QUrl>

class QJSValue;

class WebViewInterface
{
public:
    virtual ~WebViewInterface() {}
    virtual QString httpUserAgent() const = 0;
    virtual void setHttpUserAgent(const QString &httpUserAgent) = 0;
    virtual QUrl url() const = 0;
    virtual void setUrl(const QUrl &url) = 0;
    virtual bool canGoBack() const = 0;
    virtual bool canGoForward() const = 0;
    virtual QString title() const = 0;
    virtual int loadProgress() const = 0;
    virtual bool isLoading() const = 0;

    // Q_SLOTS
    virtual void goBack() = 0;
    virtual void goForward() = 0;
    virtual void stop() = 0;
    virtual void reload() = 0;
    virtual void loadHtml(const QString &html, const QUrl &baseUrl = QUrl()) = 0;
    virtual void runJavaScriptPrivate(const QString &script,
                                      int callbackId) = 0;
};

#endif // WEBVIEWINTERFACE_H
