/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 - 2022  Test Ltd.                   *
 *   All Rights Reserved.                                                  *
 *                                                                         *
 *   https://www.test.com                                                 *
 *                                                                         *
 ***************************************************************************/
#ifndef QUICKWEBVIEW_H
#define QUICKWEBVIEW_H

#include "webviewinterface.h"
#include "webview.h"
#include "quickviewcontroller.h"

class QuickWebViewLoadRequest;
class QWebViewLoadRequestPrivate;

class QuickWebView : public QuickViewController, public WebViewInterface
{
    Q_OBJECT
    Q_PROPERTY(QString httpUserAgent READ httpUserAgent WRITE setHttpUserAgent NOTIFY httpUserAgentChanged)
    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(bool loading READ isLoading NOTIFY loadingChanged)
    Q_PROPERTY(int loadProgress READ loadProgress NOTIFY loadProgressChanged)
    Q_PROPERTY(QString title READ title NOTIFY titleChanged)
    Q_PROPERTY(bool canGoBack READ canGoBack NOTIFY loadingChanged)
    Q_PROPERTY(bool canGoForward READ canGoForward NOTIFY loadingChanged)

public:
    enum LoadStatus { // Changes here needs to be done in QWebView as well
        LoadStartedStatus,
        LoadStoppedStatus,
        LoadSucceededStatus,
        LoadFailedStatus
    };
    Q_ENUM(LoadStatus)

    QuickWebView(QQuickItem *parent = 0);
    ~QuickWebView();

    QString httpUserAgent() const override;
    void setHttpUserAgent(const QString &userAgent) override;
    QUrl url() const override;
    void setUrl(const QUrl &url) override;
    int loadProgress() const override;
    QString title() const override;
    bool canGoBack() const override;
    bool isLoading() const override;
    bool canGoForward() const override;

public Q_SLOTS:
    void goBack() override;
    void goForward() override;
    void reload() override;
    void stop() override;
    void loadHtml(const QString &html, const QUrl &baseUrl = QUrl()) override;
    void runJavaScript(const QString& script,
                                     const QJSValue &callback = QJSValue());
Q_SIGNALS:
    void titleChanged();
    void urlChanged();
    void loadingChanged(QuickWebViewLoadRequest *loadRequest);
    void loadProgressChanged();
    void httpUserAgentChanged();
    void javaScriptConsoleMessage(const QString &message);

protected:
    void itemChange(ItemChange change, const ItemChangeData &value) override;
    void runJavaScriptPrivate(const QString& script,
                              int callbackId) override;

private Q_SLOTS:
    void onRunJavaScriptResult(int id, const QVariant &variant);
    void onFocusRequest(bool focus);
    void onLoadingChanged(const QWebViewLoadRequestPrivate &loadRequest);

private:
    friend class QWebEngineWebViewPrivate;
    static QJSValue takeCallback(int id);
    QuodArcaWebView* m_webView;

};

#endif // QUICKWEBVIEW_H
