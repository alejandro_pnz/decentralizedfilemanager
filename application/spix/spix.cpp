
#include <QCoreApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickWindow>
#include <QQuickItem>
#include <QObject>
#include <QIODevice>
#include <QBuffer>
#include <QTime>
#include <QCommandLineParser>
#include <QNetworkInterface>

#include <Spix/QtQmlBot.h>
#include <Spix/AnyRpcServer.h>

#include "quodarcaapplication.h"

#if !defined(UI_TESTS_ENABLED)
    #error "UI_TESTS_ENABLED is not defined. Please check configuration."
#endif

class EventFilter : public QObject
{
public:
    EventFilter(QCoreApplication *app, QIODevice *device)
        : QObject(app),
          m_app(app)
    {
        if (device) {
            device->setParent(this);
            m_textStream.setDevice(device);
        }

        m_textStream << "#!/usr/bin/env python" << endl;
        m_textStream << "# ***************************************************************************" << endl;
        m_textStream << "# *                                                                         *" << endl;
        m_textStream << "# *   Copyright (C) 2019 - 2021  Test Ltd.                   *" << endl;
        m_textStream << "# *   All Rights Reserved.                                                  *" << endl;
        m_textStream << "# *                                                                         *" << endl;
        m_textStream << "# *   https://www.test.com                                                 *" << endl;
        m_textStream << "# *                                                                         *" << endl;
        m_textStream << "# ***************************************************************************" << endl;

        m_textStream << endl;

        m_textStream << "import time" << endl;
        m_textStream << "import xmlrpc.client" << endl;

        m_textStream << endl;

        m_textStream << "url = \"http://localhost:9000\"" << endl;

        m_textStream << "client = xmlrpc.client.ServerProxy(url)" << endl;

        m_textStream << endl;

        m_textStream << "def wait(millis):" << endl;

        begin();

        m_textStream << spaces() << "client.wait(millis)" << endl;

        m_textStream << spaces() << "client.sleep(millis / 1000)" << endl;

        end();

        m_textStream << endl;

        m_time.start();
    }

    ~EventFilter()
    {
        int elapsed = m_time.elapsed();

        m_textStream << spaces() << QString("wait(%1)").arg(QString::number(elapsed)) << endl;

        m_textStream << spaces() << "client.quit()" << endl;

        end();
    }

protected:

    void begin()
    {
        m_spacesCount += 4;
    }

    void end()
    {
        m_spacesCount -= 4;
    }

    QString spaces() const
    {
        return QString().fill(' ', m_spacesCount);
    }

    QQuickWindow *rootWindow()
    {
        if (!m_engine) {
            QuodArcaApplication *app = qobject_cast<QuodArcaApplication *>(m_app);
            Q_ASSERT(app);
            m_engine = app->engine();
        }

        return qobject_cast<QQuickWindow *>(m_engine->rootObjects().first());
    }

    QString getObjectName(QObject* object) const
    {
        if (object == nullptr) {
            return "";
        }

        // Allow to override id with objectName
        if (!object->objectName().isEmpty()) {
            return object->objectName();
        }
        QQmlContext* const context = qmlContext(object);
        if (context) {
            return context->nameForObject(object);
        }

        return object->objectName();
    }


    QString fullPath(QObject *obj) const
    {
        QString path;

        QObject *_obj = obj;
        while (_obj) {
            const QString objName = getObjectName(_obj);
            path.prepend(objName.isEmpty() ? QString("<Unknown ") + _obj->metaObject()->className() + ">" : objName);
            _obj = _obj->parent();
            if (_obj) {
                path.prepend("/");
            }
        }

        return path;
    }

    bool eventFilter(QObject *watched, QEvent *event) override
    {
        if (event->type() == QEvent::MouseButtonPress) {
            //qDebug() << event->type();
        }
        else if (event->type() == QEvent::MouseButtonRelease) {

            if (QQuickWindow *window = this->rootWindow()) {

                if (true /*watched == window->activeFocusItem() */) {

                    int elapsed = m_time.elapsed();

                    QString command = QString("wait(%1)").arg(QString::number(elapsed));

                    m_textStream << spaces() << command << endl;

                    qDebug().nospace().noquote() << spaces() << command;

                    m_time.restart();

                    QString itemPath = this->fullPath(watched);

                    command = QString("client.mouseClick(\"%1\")").arg(itemPath);

                    m_textStream << spaces() << command << endl;

                    qDebug().nospace().noquote() << spaces() << command;
                }
            }
        }
        else if (event->type() == QEvent::KeyPress) {
            //qDebug() << event->type();
        }
        else if (event->type() == QEvent::KeyRelease) {
            if (QQuickWindow *window = this->rootWindow()) {

                if (watched == window->activeFocusItem()) {

                    QKeyEvent *keyEvent = reinterpret_cast<QKeyEvent *>(event);

                    int elapsed = m_time.elapsed();

                    QString command = QString("wait(%1)").arg(QString::number(elapsed));

                    m_textStream << spaces() << command << endl;

                    qDebug().nospace().noquote() << spaces() << command;

                    m_time.restart();

                    QString itemPath = this->fullPath(watched);

                    QChar key(keyEvent->key());

                    if (key.isPrint() && !keyEvent->text().isEmpty()) {
                        command = QString("client.inputText(\"%1\", \"%2\")").arg(itemPath, keyEvent->text());
                    }
                    else {
                        command = QString("client.enterKey(\"%1\", %2, %3)").arg(itemPath, QString::number(keyEvent->key()), QString::number(keyEvent->modifiers()));
                    }

                    m_textStream << spaces() << command << endl;

                    qDebug().nospace().noquote() << spaces() << command;
                }
            }
        }
        else {
            //qDebug() << event->type();
        }

        return QObject::eventFilter(watched, event);
    }

    QCoreApplication *m_app{nullptr};

    QQmlApplicationEngine *m_engine{nullptr};

    QTextStream m_textStream;

    QTime m_time;

    int m_spacesCount = 0;
};

void startSpixServer() {
    qWarning() << "SPIX server starting.... Don't use in Production App.";

    QCoreApplication *app = QCoreApplication::instance();

    QCommandLineParser parser;

    const QCommandLineOption portOption({"p", "port"}, "Port to connect", "port", "9000");
    parser.addOption(portOption);

    const QCommandLineOption scriptFileOption({"f", "file"}, "Script file name", "file", "");
    parser.addOption(scriptFileOption);

    parser.addOptions({{{"g", "generate"}, "Generate new script"}});

    // Windows show message box on unknown cmd parameter, added this as workaround
    const QCommandLineOption urlOption({"u", "url"}, "Dummy url option", "url", "");
    parser.addOption(urlOption);

    parser.process(*app);

    QIODevice *outputDevice{nullptr};

    const bool generate = parser.isSet("generate");

    if (generate) {
        const QString filename = parser.value(scriptFileOption);

        outputDevice = new QFile(filename);

        outputDevice->open(QIODevice::WriteOnly);
    }
    else {
        outputDevice = new QBuffer();

        outputDevice->open(QIODevice::WriteOnly);
    }

    const QString port = parser.value(portOption);

    EventFilter *eventFilter = new EventFilter(app, outputDevice);
    app->installEventFilter(eventFilter);

    spix::AnyRpcServer *server = new spix::AnyRpcServer(port.toInt());
    auto bot = new spix::QtQmlBot(app);
    bot->runTestServer(*server);

    QList<QHostAddress> addresses;
    QList<QHostAddress> allAddresses = QNetworkInterface::allAddresses();
    std::copy_if(allAddresses.begin(), allAddresses.end(), std::back_inserter(addresses), [&](const QHostAddress &address) {
        return !address.isLoopback() && address.protocol() == QAbstractSocket::IPv4Protocol;
    });

    for (auto const &address : qAsConst(addresses)) {
        qWarning() << "SPIX server running on " + address.toString() + ":" + port;
    }
}

Q_COREAPP_STARTUP_FUNCTION(startSpixServer)
