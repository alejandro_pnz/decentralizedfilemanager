
ui-tests {
    warning("ui-tests configuration is enabled")

    DEFINES += \
        UI_TESTS_ENABLED

    INCLUDEPATH += \
        $$PWD/include

    SOURCES += \
        $$PWD/spix.cpp

    android {
        LIBS += -L$$PWD/libs/android_$${ANDROID_TARGET_ARCH} -lSpix -lanyrpc
    }
    ios {
        LIBS += -L$$PWD/libs/ios -lSpix -lanyrpc
    }
    macx {
        LIBS += -L$$PWD/libs/macx -lSpix -lanyrpc
    }
    win32 {
        CONFIG(release, debug|release): LIBS += -L$$PWD/libs/win/release -lSpix -lanyrpc
        else:CONFIG(debug, debug|release): LIBS += -L$$PWD/libs/win/debug -lSpix -lanyrpc

        LIBS += -lWs2_32
    }
}
