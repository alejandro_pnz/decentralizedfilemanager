#include "util.h"

QString getHttpStatusStr(HttpStatus status)
{
    auto it = httpStatusStrs.find(static_cast<int>(status));
    if (it == httpStatusStrs.end())
        return "";

    return it->second;
}

QByteArray gzipCompress(const QByteArray &data, int compressionLevel)
{
#ifndef QT_NO_COMPRESS
    QByteArray compressedData = qCompress(data, compressionLevel);
    // Data compressed with qCompress contain 4 bytes header with uncompressed data length.
    // This header should be removed to allow 3rd parties decompress the data
    return compressedData.remove(0, 4);
#else
    return data;
#endif
}

QByteArray gzipUncompress(const QByteArray &data)
{
#ifndef QT_NO_COMPRESS
    // To use qUncompress, data should contain 4 bytes header with expected data length
    QByteArray header;
    header.resize(4);
    const unsigned long nbytes = 1ul;
    header[0] = (nbytes & 0xff000000) >> 24;
    header[1] = (nbytes & 0x00ff0000) >> 16;
    header[2] = (nbytes & 0x0000ff00) >> 8;
    header[3] = (nbytes & 0x000000ff);

    QByteArray compressedData;
    compressedData.append(header);
    compressedData.append(data);

    return qUncompress(compressedData);
#else
    return data;
#endif
}
