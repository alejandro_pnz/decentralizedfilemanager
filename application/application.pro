QT += quick svg

CONFIG += c++17

TARGET = QuodArca

CONFIG -= debug_and_release build_all

QT += multimedia

QT += quickcontrols2

win32: QT += webview webchannel

win32:VERSION = 0.8.5.0 # major.minor.patch.build
else:VERSION = 0.8.5    # major.minor.patch

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
        src/RegistrationManager.h \
        src/document_editor/documenteditor.h \
        src/logger.h \
        src/requesthandler.h

SOURCES += \
        src/logger.cpp \
        src/requesthandler.cpp \
        src/RegistrationManager.cpp \
        src/document_editor/documenteditor.cpp \
        src/main.cpp

INCLUDEPATH += \
    $$PWD/src/document_editor

# Localization support
CONFIG += lrelease embed_translations
ios:CONFIG(release, debug|release):CONFIG += qtquickcompiler # Workaround for Qt.5.15.2

EXTRA_TRANSLATIONS += \
    $$PWD/i18n/QuodArca-de.ts \
    $$PWD/i18n/QuodArca-en.ts \
    $$PWD/i18n/QuodArca-es.ts \
    $$PWD/i18n/QuodArca-fr.ts \
    $$PWD/i18n/QuodArca-it.ts \
    $$PWD/i18n/QuodArca-ru.ts

win32 { # Windows specific configuration
    CONFIG += desktop
    CONFIG -= embed_manifest_exe embed_manifest_dll
    QMAKE_MANIFEST = $$PWD/windows/QuodArca.exe.manifest
}
macx { # MacOSX specific configuration
    CONFIG += desktop
}
ios { # iOS specific configuration
    CONFIG += mobile
}
android { # Android specific configuration
    CONFIG += mobile

    # function for generation unique code for gplay store
    defineReplace(droidVersionCode) {
        segments = $$split(1, ".")
        for (segment, segments): vCode = "$$first(vCode)$$format_number($$segment, width=3 zeropad)"
        contains(ANDROID_TARGET_ARCH, arm64-v8a): \
            suffix = 1
        else:contains(ANDROID_TARGET_ARCH, armeabi-v7a): \
            suffix = 0
        # add more cases as needed

        return($$first(vCode)$$first(suffix))
    }


    # Example: android-23
    ANDROID_NDK_PLATFORM = $$getenv(ANDROID_NDK_PLATFORM)

    # Example: android 23
    ANDROID_NDK_VERSION = $$split(ANDROID_NDK_PLATFORM, -)

    # Example: 23
    ANDROID_NDK_VERSION = $$last(ANDROID_NDK_VERSION)

    lessThan(ANDROID_NDK_VERSION, 23):error("Please use ANDROID_NDK_PLATFORM 23+. You can set the environment variable ANDROID_NDK_PLATFORM to android-23 in Qt Creator in the project tab.")

    ANDROID_VERSION_NAME = $$VERSION
    ANDROID_VERSION_CODE = $$droidVersionCode($$ANDROID_VERSION_NAME)
}

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

android {
    ANDROID_API_VERSION = 27
    ANDROID_TARGET_SDK_VERSION = 30
    ANDROID_MIN_SDK_VERSION = 24

    DISTFILES += \
        android/AndroidManifest.xml \
        android/build.gradle \
        android/gradle/wrapper/gradle-wrapper.jar \
        android/gradle/wrapper/gradle-wrapper.properties \
        android/gradlew \
        android/gradlew.bat \
        android/res/drawable/splashscreen.xml \
        android/res/mipmap-anydpi-v26/ic_launcher_round.xml \
        android/res/mipmap-anydpi-v26/ic_launcher.xml \
        android/res/values/apptheme.xml \
        android/res/values/ic_launcher_background.xml \
        android/res/drawable/ic_launcher_foreground.xml \
        android/res/drawable/ic_logo_vertical_22.xml

    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
}
# IOS and OSX common
mac {
    QMAKE_DEVELOPMENT_TEAM = 5HPQ92RG7Q

    QMAKE_BUNDLE = quodarca
    QMAKE_TARGET_BUNDLE_PREFIX = com.eisst
}
ios {
    QMAKE_ASSET_CATALOGS += $$PWD/ios/Media.xcassets
    QMAKE_INFO_PLIST = $$PWD/ios/Info.plist

    # iPhone and iPad
    QMAKE_APPLE_TARGETED_DEVICE_FAMILY = 1,2

    ENTITLEMENTS.name = CODE_SIGN_ENTITLEMENTS
    ENTITLEMENTS.value = $$PWD/ios/QuodArca.entitlements
    QMAKE_MAC_XCODE_SETTINGS += ENTITLEMENTS

    Q_ENABLE_BITCODE.name = ENABLE_BITCODE
    Q_ENABLE_BITCODE.value = NO
    QMAKE_MAC_XCODE_SETTINGS += Q_ENABLE_BITCODE

    launch_screen.files = $$PWD/ios/Launch_Screen.storyboard
    QMAKE_BUNDLE_DATA += launch_screen
}
macx {
    QMAKE_ASSET_CATALOGS += $$PWD/macx/Media.xcassets
    QMAKE_INFO_PLIST = $$PWD/macx/Info.plist
}
win32 {
    RC_ICONS = $$PWD/windows/Application.ico
}

RESOURCES += \
        $$PWD/qml/application.qrc \
        $$PWD/../tests/test_resources.qrc


# Only for UI tests
ui-tests {
    include($$PWD/spix/spix.pri)
}

# PDFTron.js
include($$PWD/pdfviewer/pdfviewer.pri)

# Markdown Editor
include($$PWD/mdeditor/mdeditor.pri)

# Common
include(../common/common.pri)

# App
include($$PWD/src/app/app.pri)

# Analytics
include($$PWD/analytics/analytics.pri)

# Http Server
include($$PWD/HttpServer/HttpServer.pri)

# Custom Webview
android|mac:include($$PWD/webview/webview.pri)
